# I-REACT Open Core Web Tool

This is the [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/) Web Tool repository.

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.

## Service dependencies

In order to work properly, the app needs the following services to be active and reachable:

- I-REACT Open Core Backend
- SOCIAL Analysis API
- Geonames.org subscription for reverse geocoding
- Mapbox subscription or Mapbox-compatible tile server

You must have access to a version of the [**I-REACT Open Core**](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/) backend deployed and reachable at a `https` URL.
Please download and deploy the [**I-REACT Open Core Backend**](https://bitbucket.org/mobilesolutionsismb/ioc-backend) from [this link](https://bitbucket.org/mobilesolutionsismb/ioc-backend/raw/997c9d7bf9b5119e113dbd5df132364ef2d71d7e/release/ioc-backend.zip)

In order to have the Social Analysis API working you must also have access to a version of the social analyisis tool from [CELI](https://www.celi.it/). Please [contact CELI](mailto:info@celi.it) and ask for purchasing their solution.

Optionally, you may want to deploy your own mapbox-compatible tile server for base layers, like http://tileserver.org/. Otherwise Mapbox subscription will do just fine.

Furthermore you may add Microsoft Application Insights in order to get stats and error reporting. See the comment in env.example file.

## Installation

```bash
npm install
```

## Coding

```bash
cp .env.example .env # then edit
npm start
```

Browse to `http://localhost:8080`  
Then edit your code. Webpack will reload it.

### Code Structure

This is how this repository code ~~is~~ was structured (this may be a bit outdated but generally holds).

![Alt text](src/assets/guide-images/i-react_frontend-project-structure.png 'Code Strutcure')

Now src `src/js` is divided into 4 folders:

- components: React views
- modules: Redux data source and providers/connectors
- startup: stuff needed at boot time only, which is seldomly updated
- utils: general purpose functions

## Build for production

```bash
npm run build
```

## Deploy to Azure

Just download the publishSettings to your PC from the azure webapp (slot), then run

```bash
npm run deploy <PATH_TO_publishSettings>
```

## Deploy to any other static server (nightlies)

Set `DEPLOY_PATH=/your/path/on/server` in the .env file if it's different from `'/'`,  
e.g. you want to deploy on `https://yourserver/yourpath/to/static/folders/`

```
# set DEPLOY_PATH=/yourpath/to/static/folders/ in .env
npm run build
```

The build is generated in `build_{year}_{month}_{day}_{hour}_{min}`, e.g. `build_2018_4_17_16_17`.
Copy the build folder into the static folders path on the server, and the build will be available at `https://yourserver/yourpath/to/static/folders/build_{year}_{month}_{day}_{hour}_{min}`

If `DEPLOY_PATH` is not set, the path will be `/` and the build will be generated in `dist` as default.
