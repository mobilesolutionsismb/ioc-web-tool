require('dotenv').config();
const { exec } = require('child_process');
const fs = require('fs-extra');

const { DEPLOY_USER, NIGHTLY_DEPLOY_REMOTE_PATH } = process.env;
if (!DEPLOY_USER || !NIGHTLY_DEPLOY_REMOTE_PATH) {
  console.error('Missing remote user or path');
}

// Guess most recent nightly build
const BUILD_FOLDER = fs.readdirSync('.').find(fname => fname.startsWith('build_'));
if (BUILD_FOLDER) {
  console.log(`Deploying a nightly build to ${DEPLOY_USER}:${NIGHTLY_DEPLOY_REMOTE_PATH}`);
  const cmd = `tar czv ${BUILD_FOLDER} | ssh ${DEPLOY_USER} 'cat | tar xz -C ${NIGHTLY_DEPLOY_REMOTE_PATH}'`;

  exec(cmd, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
  });
} else {
  console.error('No suitable build folder found');
}
