require('dotenv').config();

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');

const pkg = require('./package.json');
const eslintFormatter = require('react-dev-utils/eslintFormatter');
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const Mustache = require('mustache');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const ENVIRONMENT = process.env.NODE_ENV || 'development';
const IS_PRODUCTION = ENVIRONMENT === 'production';
const IS_CORDOVA = process.env.IS_CORDOVA === 'true';

const DEPLOY_PATH =
  process.env.DEPLOY_PATH && ENVIRONMENT !== 'dev-server' ? process.env.DEPLOY_PATH : 'default';
const buildDate = new Date();

function padInt(v, amount = 2, str = '0') {
  return `${v}`.padStart(amount, str);
}

const DEST =
  DEPLOY_PATH === 'default'
    ? 'dist'
    : `build_${buildDate.getFullYear()}_${padInt(buildDate.getMonth() + 1)}_${padInt(
        buildDate.getDate()
      )}_${padInt(buildDate.getHours())}_${padInt(buildDate.getMinutes())}`;

const OUTPUT_PATH = path.join(__dirname, IS_CORDOVA ? 'www' : DEST);

const PUBLIC_PATH = DEPLOY_PATH === 'default' ? '/' : `${DEPLOY_PATH}${DEST}/`;

const APP_MOUNT_ID = 'app';
const IREACT_BACKEND = process.env.IREACT_BACKEND;
const SOCIAL_BACKEND = process.env.SOCIAL_BACKEND;

if (!IREACT_BACKEND) {
  throw new Error('Missing process.env.IREACT_BACKEND');
}
if (!SOCIAL_BACKEND) {
  throw new Error('Missing process.env.SOCIAL_BACKEND');
}
// const vendorChunkName = 'vendor';

function getGeoserverURL(url) {
  return typeof url === 'string'
    ? /geoserver\/?$/.test(url)
      ? /\/$/.test(url)
        ? url
        : url + '/'
      : `${url.replace(/\/$/, '')}/geoserver/`
    : null;
}

// User Avatars
const NUMBER_OF_AVATARS = 16;
const AVATAR_FILE_NAMES = [...Array(NUMBER_OF_AVATARS).keys()].map(
  n => `/avatars/avatar-${('' + n).padStart(2, '0')}.svg`
);

const DEFINITIONS = new webpack.DefinePlugin({
  TITLE: JSON.stringify(pkg.title),
  VERSION: JSON.stringify(pkg.version),
  PKG_NAME: JSON.stringify(pkg.name),
  DESCRIPTION: JSON.stringify(pkg.description),
  ENVIRONMENT: JSON.stringify(ENVIRONMENT),
  IS_PRODUCTION: JSON.stringify(IS_PRODUCTION),
  BUILD_DATE: JSON.stringify(buildDate),
  APP_MOUNT_ID: JSON.stringify(APP_MOUNT_ID),
  IS_CORDOVA: JSON.stringify(IS_CORDOVA),
  //This tells which languages are supported by this app
  SUPPORTED_LANGUAGES: JSON.stringify(require('./src/locale/languages.json')),
  //Mapbox - OSM Tiles
  OSM_TILES_KEY: JSON.stringify(process.env.OSM_TILES_KEY),
  //Used by ReactJS to turn optimization on/off
  //Turn off prop check in cordova
  'process.env': {
    NODE_ENV: JSON.stringify(IS_CORDOVA ? 'production' : ENVIRONMENT)
  },
  IREACT_BACKEND: JSON.stringify(IREACT_BACKEND),
  SOCIAL_BACKEND: JSON.stringify(SOCIAL_BACKEND),
  RECAPTCHA_SITEKEY: JSON.stringify(pkg.config.RECAPTCHA_SITEKEY),
  MAP_SERVER_APIKEY: JSON.stringify(process.env.MAP_SERVER_APIKEY || pkg.config.MAP_SERVER_APIKEY),
  MAP_SERVER_URL: JSON.stringify(process.env.MAP_SERVER_URL || pkg.config.MAP_SERVER_URL),
  MAP_STYLES_URL: JSON.stringify(process.env.MAP_STYLES_URL || pkg.config.MAP_STYLES_URL),
  GEOSERVER_URL: JSON.stringify(
    getGeoserverURL(process.env.GEOSERVER_URL || pkg.config.GEOSERVER_URL)
  ), // this is now optional and IF SET will override the ones coming from server
  USE_WMTS: JSON.stringify(process.env.USE_WMTS === 'true'), // enable WMTS mode for raster layer - experimental
  APP_INSIGHTS_INSTR_KEY: JSON.stringify(process.env.APP_INSIGHTS_INSTR_KEY),
  PUBLIC_PATH: JSON.stringify(PUBLIC_PATH),
  AVATAR_FILE_NAMES: JSON.stringify(
    AVATAR_FILE_NAMES
    // AVATAR_FILE_NAMES.map(avatarFname => `${PUBLIC_PATH.replace(/\/$/, '')}${avatarFname}`)
  )
});

const CHROME_MOBILE_THEME_COLOR = '#000000';

const FAVICONS = [
  'apple-touch-icon.png',
  'favicon-32x32.png',
  'favicon-16x16.png',
  'manifest.json',

  //See manifest.json for these
  'android-chrome-36x36.png',
  'android-chrome-48x48.png',
  'android-chrome-72x72.png',
  'android-chrome-96x96.png',
  'android-chrome-144x144.png',
  'android-chrome-192x192.png',
  'android-chrome-256x256.png'
];

const PAGE_LOADER_LINKS = [
  {
    rel: 'stylesheet',
    href: PUBLIC_PATH + 'dataurl.css'
  }
];

//See http://realfavicongenerator.net/blog/new-favicon-package-less-is-more/
const FAVICONS_LINKS = [
  {
    rel: 'apple-touch-icon',
    sizes: '180x180',
    href: PUBLIC_PATH + 'apple-touch-icon.png'
  },
  {
    rel: 'icon',
    sizes: '32x32',
    href: PUBLIC_PATH + 'favicon-32x32.png'
  },
  {
    rel: 'icon',
    sizes: '16x16',
    href: PUBLIC_PATH + 'favicon-16x16.png'
  },
  {
    rel: 'manifest',
    href: PUBLIC_PATH + 'manifest.json'
  },
  {
    rel: 'twitter',
    href: PUBLIC_PATH + 'twitter.png'
  }
];

//See https://content-security-policy.com/
//Validate through http://www.cspplayground.com/csp_validator
const CSP_FILE_CONTENT = fs.readFileSync(path.join(__dirname, 'content-security-policy.txt'));

let SCRIPTS = [
  {
    src: PUBLIC_PATH + 'pace.min.js',
    type: 'text/javascript'
  },
  {
    src: PUBLIC_PATH + `${IS_PRODUCTION ? 'mapbox-gl.js' : 'mapbox-gl-dev.js'}`,
    type: 'text/javascript'
  }
];

if (IS_PRODUCTION) {
  const ApplicationInsightsScript = {
    src: IS_CORDOVA ? 'ai.js' : PUBLIC_PATH + `${'ai.js'}`,
    type: 'text/javascript'
  };
  SCRIPTS.splice(1, 0, ApplicationInsightsScript); // insert at 2nd place after pace
}

const HTML = new HtmlWebpackPlugin({
  inject: false,
  template: require('html-webpack-template'),
  appMountId: APP_MOUNT_ID,
  mobile: true,
  links: IS_CORDOVA
    ? ['mapbox-gl.css']
    : [...PAGE_LOADER_LINKS, PUBLIC_PATH + 'mapbox-gl.css', ...FAVICONS_LINKS],
  meta: [
    {
      'theme-color': CHROME_MOBILE_THEME_COLOR
    },
    {
      'http-equiv': 'Content-Security-Policy',
      content: CSP_FILE_CONTENT
    },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
    }
  ],
  lang: 'en',
  filename: 'index.html',
  favicon: path.join(__dirname, 'src', 'assets', 'icons', 'favicon.ico'),
  title: pkg.title,
  excludeChunks: ['tos', 'userAccount'],
  scripts: SCRIPTS,
  window: {
    // See https://github.com/HubSpot/pace/blob/master/pace.coffee
    paceOptions: {
      ajax: false, // disabled
      document: true, // disabled
      eventLag: false, // disabled
      restartOnPushState: false,
      restartOnRequestAfter: false,
      elements: {
        selectors: ['div[data-reactroot]']
      }
    },
    IREACT_BACKEND: IREACT_BACKEND,
    SOCIAL_BACKEND: SOCIAL_BACKEND
  }
});

const TOS_AND_PRIVACY = new HtmlWebpackPlugin({
  inject: false,
  template: require('html-webpack-template'),
  appMountId: 'tos',
  mobile: true,
  links: [...PAGE_LOADER_LINKS, ...FAVICONS_LINKS],
  meta: [
    {
      'theme-color': CHROME_MOBILE_THEME_COLOR
    },
    {
      'http-equiv': 'Content-Security-Policy',
      content: CSP_FILE_CONTENT
    },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
    }
  ],
  lang: 'en',
  filename: 'tos.html',
  favicon: path.join(__dirname, 'src', 'assets', 'icons', 'favicon.ico'),
  title: pkg.title,
  excludeChunks: ['main', 'userAccount'],
  scripts: [],
  window: {
    // See https://github.com/HubSpot/pace/blob/master/pace.coffee
    paceOptions: {
      ajax: false, // disabled
      document: true, // disabled
      eventLag: false, // disabled
      restartOnPushState: false,
      restartOnRequestAfter: false,
      elements: {
        selectors: ['div[data-reactroot]']
      }
    },
    IREACT_BACKEND: IREACT_BACKEND,
    SOCIAL_BACKEND: SOCIAL_BACKEND
  }
});

const PASSW_MANAGEMENT = new HtmlWebpackPlugin({
  inject: false,
  template: require('html-webpack-template'),
  appMountId: 'passw',
  mobile: true,
  links: [...PAGE_LOADER_LINKS, ...FAVICONS_LINKS],
  meta: [
    {
      'theme-color': CHROME_MOBILE_THEME_COLOR
    },
    {
      'http-equiv': 'Content-Security-Policy',
      content: CSP_FILE_CONTENT
    },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
    }
  ],
  lang: 'en',
  filename: 'account.html',
  favicon: path.join(__dirname, 'src', 'assets', 'icons', 'favicon.ico'),
  title: pkg.title,
  scripts: [],
  excludeChunks: ['main', 'tos'],
  window: {
    // See https://github.com/HubSpot/pace/blob/master/pace.coffee
    paceOptions: {
      ajax: false, // disabled
      document: true, // disabled
      eventLag: false, // disabled
      restartOnPushState: false,
      restartOnRequestAfter: false,
      elements: {
        selectors: ['div[data-reactroot]']
      }
    },
    IREACT_BACKEND: IREACT_BACKEND,
    SOCIAL_BACKEND: SOCIAL_BACKEND
  }
});

//Use copy webpack plugin only if it's needed to preserve static assets paths
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const PAGE_LOADER = ['dataurl.css', 'pace.min.js'];

const COPY_PAGE_LOADER = new CopyWebpackPlugin(
  PAGE_LOADER.map(fileName => {
    return {
      context: __dirname,
      from: `src/assets/loader/${fileName}`,
      to: `${OUTPUT_PATH}/${fileName}`
    };
  })
);

const LOGO = new CopyWebpackPlugin([
  {
    context: __dirname,
    from: 'src/assets/logo',
    to: `${OUTPUT_PATH}/logo`
  }
]);

// const TRANSLATIONS = new CopyWebpackPlugin([
//   {
//     context: __dirname,
//     from: 'src/locale',
//     to: `${OUTPUT_PATH}/locales`
//   }
// ]);

const HELP_SITE = new CopyWebpackPlugin([
  {
    context: __dirname,
    from: 'src/assets/help',
    to: `${OUTPUT_PATH}/help`
  }
]);

let MAPBOX_LIBRARY = ['mapbox-gl.css'];
MAPBOX_LIBRARY = [...MAPBOX_LIBRARY, IS_PRODUCTION ? 'mapbox-gl.js' : 'mapbox-gl-dev.js'];
if (!IS_CORDOVA && IS_PRODUCTION) {
  MAPBOX_LIBRARY = [...MAPBOX_LIBRARY, 'mapbox-gl.js.map'];
}

const AppInsightsLibrary = new CopyWebpackPlugin([
  {
    context: __dirname,
    from: 'node_modules/applicationinsights-js/dist/ai.js',
    to: `${OUTPUT_PATH}/ai.js`
  }
]);

const MapboxLibrary = new CopyWebpackPlugin(
  MAPBOX_LIBRARY.map(mapboxFileOrFolder => {
    return {
      context: __dirname,
      from: `node_modules/mapbox-gl/dist/${mapboxFileOrFolder}`,
      to: `${OUTPUT_PATH}/${mapboxFileOrFolder}`
    };
  })
);

//You may use package.json config.staticAssets = {}
const ICON_ASSETS = new CopyWebpackPlugin(
  FAVICONS.map(assetFileName => {
    return {
      context: __dirname,
      from: `src/assets/icons/${assetFileName}`,
      to: `${OUTPUT_PATH}/${assetFileName}`
    };
  })
);

const TWITTER_ICON_ASSETS = new CopyWebpackPlugin([
  {
    context: __dirname,
    from: 'src/assets/twitter-icon/twitter.png',
    to: `${OUTPUT_PATH}/twitter.png`
  }
]);

const WEB_CONFIG = new CopyWebpackPlugin([
  {
    context: __dirname,
    from: './web.config',
    to: `${OUTPUT_PATH}/web.config`
  },
  {
    context: __dirname, // For apache deploy
    from: './htaccess.mustache',
    toType: 'file',
    to: '.htaccess',
    transform(content, path) {
      return Mustache.render(content.toString('utf8'), { publicPath: PUBLIC_PATH });
    }
  }
]);

const SOURCE_MAPS = new webpack.SourceMapDevToolPlugin({
  filename: '[name].js.map',
  exclude: /(node_modules)/
});

let plugins = [
  TOS_AND_PRIVACY,
  PASSW_MANAGEMENT,
  HTML,
  DEFINITIONS,
  new webpack.NamedModulesPlugin(),
  new webpack.HashedModuleIdsPlugin(),
  AppInsightsLibrary,
  WEB_CONFIG,
  MapboxLibrary
];

if (!IS_PRODUCTION) {
  plugins = [...plugins, new CaseSensitivePathsPlugin()];
} else {
  plugins = [...plugins, SOURCE_MAPS];
}

if (!IS_CORDOVA) {
  plugins = [
    COPY_PAGE_LOADER,
    LOGO,
    // TRANSLATIONS,
    HELP_SITE,
    ICON_ASSETS,
    TWITTER_ICON_ASSETS,
    ...plugins
  ];
}

const entries = {
  tos: path.join(__dirname, 'src', 'tos.js'),
  userAccount: path.join(__dirname, 'src', 'userAccount.js'),
  main: path.join(__dirname, 'src', 'main.js')
};

const jsLoaders = [
  {
    test: /\.(js|jsx|mjs)$/,
    exclude: /(node_modules)/,
    use: [
      {
        options: {
          cacheDirectory: !IS_PRODUCTION
        },
        loader: 'babel-loader'
      }
    ]
  }
];

if (IS_PRODUCTION) {
  jsLoaders[0].use.push('strip-loader?strip[]=console.debug,strip[]=console.log');
  plugins.push(
    new TerserPlugin({
      sourceMap: IS_PRODUCTION,
      cache: IS_PRODUCTION,
      parallel: IS_PRODUCTION,
      terserOptions: { ecma: 8 }
    })
  );
} else {
  jsLoaders.push({
    test: /\.jsx?$/,
    include: /(node_modules)/,
    use: ['react-hot-loader/webpack']
  });
}

const devServer = {
  host: '0.0.0.0', //Any network interface,
  historyApiFallback: {
    rewrites: [{ from: /^\/*$/, to: '/index.html' }]
  },
  inline: true,
  compress: true,
  // clientLogLevel: 'none',
  hot: true,
  // overlay: false,
  // quiet: true,
  before(app) {
    // This lets us open files from the runtime error overlay.
    app.use(errorOverlayMiddleware());
  }
};

let webpackConfig = {
  context: path.join(__dirname, 'src'),
  entry: entries,
  devServer,
  output: {
    pathinfo: !IS_PRODUCTION,
    path: OUTPUT_PATH,
    filename: '[name].[hash].js',
    publicPath: IS_CORDOVA ? undefined : PUBLIC_PATH,
    sourceMapFilename: '[name].map',
    globalObject: 'this'
  },
  externals: {
    'mapbox-gl': 'window["mapboxgl"]'
  },
  resolve: {
    extensions: [
      '.js', // automatically in webpack 2
      '.jsx',
      '.json', // automatically in webpack 2
      '.scss',
      '.css'
    ],
    alias: {
      'ireact-frontend-api': IS_PRODUCTION
        ? 'ireact-frontend-api/dist/index.min.js'
        : 'ireact-frontend-api/dist/index.js'
    },
    modules: ['node_modules', path.resolve(__dirname, './node_modules'), 'src']
  },
  plugins,
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        vendor: {
          reuseExistingChunk: false,
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

            // npm package names are URL-safe, but some servers don't like @ symbols
            return `npm.${packageName.replace('@', '')}`;
          }
        }
      }
    }
  },
  mode: IS_PRODUCTION ? 'production' : 'development',
  devtool: IS_PRODUCTION ? 'source-map' : 'eval-source-map',
  module: {
    rules: [
      {
        test: /\.json$/i,
        type: 'javascript/auto',
        loader: 'json-loader'
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto'
      },
      {
        test: /\.(js|jsx|mjs)$/,
        enforce: 'pre',
        use: [
          {
            options: {
              formatter: eslintFormatter,
              eslintPath: require.resolve('eslint')
            },
            loader: require.resolve('eslint-loader')
          }
        ],
        include: /(src)/
      },
      {
        test: /\.(js|jsx|mjs)$/,
        use: ['source-map-loader'],
        exclude: /(src)/,
        enforce: 'pre'
      },
      ...jsLoaders,
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader?importLoaders=true', 'postcss-loader', 'sass-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?importLoaders=true', 'postcss-loader']
      },
      {
        test: /\.woff?(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=100000&mimetype=application/font-woff'
      },
      {
        test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=100000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=100000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.ico(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=100000&mimetype=image/svg+xml'
      },
      {
        test: /\.jpg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=250000&mimetype=image/jpeg'
      },
      {
        test: /\.png(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=250000&mimetype=image/png'
      }
    ]
  }
};

module.exports = webpackConfig;
