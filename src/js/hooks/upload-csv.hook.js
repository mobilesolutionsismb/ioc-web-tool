import { useState, useRef, /* useContext, */ useEffect } from 'react';
// import { logMain } from 'js/utils/log';

export function useFileReaders(
  fileReadMode,
  fileMultiple,
  onFileContentLoaded,
  onLoadStart,
  onLoadEnd,
  onError
) {
  const readers = useRef([]);
  const [reading, setReading] = useState(false);

  function clearReaders() {
    if (readers.current.length > 0) {
      readers.current.forEach((r, i) => {
        if (r.readyState !== FileReader.DONE) {
          r.abort();
        }
        r = null;
      });
      readers.current = [];
    }
    if (reading) {
      setReading(false);
    }
  }

  function readFile(file, index) {
    return new Promise((success, fail) => {
      const reader = new FileReader();
      readers.current[index] = reader;
      reader.onload = e => {
        success({
          content: e.target.result,
          file,
          status: 'done',
          event: e
        });
      };
      reader.onabort = e => {
        success({
          content: null,
          file,
          status: 'abort',
          event: e
        });
      };
      reader.onerror = e => {
        fail({
          content: null,
          file,
          status: 'error',
          event: e
        });
      };
      switch (fileReadMode) {
        case 'buffer':
          reader.readAsArrayBuffer(file);
          break;
        case 'binarystring':
          reader.readAsBinaryString(file);
          break;
        case 'dataurl':
          reader.readAsDataURL(file);
          break;
        case 'text':
        default:
          reader.readAsText(file);
          break;
      }
    });
  }

  useEffect(() => {
    reading ? onLoadStart() : onLoadEnd();
  }, [reading]);

  /**
   *
   * @param {Array<File>|FileList} files
   */
  async function _readFilesInner(files) {
    try {
      clearReaders();
      setReading(true);
      readers.current = Array(files.length).fill(null);
      const results = await Promise.all(Array.from(files).map(readFile));
      setReading(false);
      onFileContentLoaded(fileMultiple ? results : results[0]);
    } catch (err) {
      onError(err);
    }
  }

  /**
   * Read a file upon input event
   * @param {DOMEvent} e
   */
  async function onFileUpload(e) {
    const { files } = e.target;
    if (files.length > 0) {
      await _readFilesInner(files);
    }
  }

  /**
   *
   * @param {File} file
   */
  async function readFiles(files) {
    await _readFilesInner(files);
  }

  return { onFileUpload, readFiles };
}
