import { useState, useEffect } from 'react';
import isEqual from 'react-fast-compare';

/**
 * Callback for validating params
 *
 * @callback ParamValidatorFunction
 * @param {any} params - function params
 * @returns {boolean} - true if parameters are valid
 */

/**
 * Callback for calling an api (with axios)
 *
 * @callback APIFunction
 * @param {any} params - function params
 * @param {Function} setData - pass data
 * @param {Function} setError - pass errror if any
 * @param {Function} onLoad - loading: void (set loading state true or false)
 * @param {Function} setCancel - handle for passing cancel callback (see axios canceltoken)
 */

/**
 * Call an api (generic)
 * @param {APIFunction} apiFunction
 * @param {any} params
 * @param {any} [initialData=null] initial value for data
 * @param {ParamValidatorFunction} [paramValidator=params => true] params validator (won't trigger function)
 * @returns {[any, Error|null, boolean]} data, error and loading state
 */
export function useAPI(apiFunction, params, initialData = null, paramValidator = params => true) {
  const [cancel, setCancel] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [data, setData] = useState(initialData);
  const [storedParams, setStoredParams] = useState(params);

  function onLoad(isLoading) {
    if (typeof cancel == 'function') {
      setCancel(null);
    }
    setLoading(isLoading);
  }

  useEffect(() => {
    if (!isEqual(params, storedParams)) {
      setStoredParams(params);
    }
  }, [params]);

  useEffect(() => {
    return () => {
      if (typeof cancel == 'function') {
        cancel();
        setCancel(null);
      }
    };
  }, []);

  useEffect(() => {
    const validParams = paramValidator(storedParams);
    if (validParams) {
      if (loading === false) {
        apiFunction(storedParams, setData, setError, onLoad, setCancel);
      } else {
        if (typeof cancel == 'function') {
          cancel();
          apiFunction(storedParams, setData, setError, onLoad, setCancel);
        }
      }
    }
  }, [storedParams]);

  return [data, error, loading];
}
