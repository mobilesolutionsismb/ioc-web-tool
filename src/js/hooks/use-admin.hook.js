import { useRedux } from 'js/hooks/use-redux.hook';
import {
  quickUserAddSelector,
  quickUserAddActionCreators
} from 'js/modules/QuickUserAdd/dataProviders/withAdminQuickUserAdd';

export function useQuickUserAdd() {
  const [selector, actions, dispatch] = useRedux(quickUserAddSelector, quickUserAddActionCreators);
  return [selector, actions, dispatch];
}
