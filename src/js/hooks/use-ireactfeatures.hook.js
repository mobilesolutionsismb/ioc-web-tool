import { useRedux } from 'js/hooks/use-redux.hook';
import { ireactFeaturesSelector, ireactFeaturesActionCreators } from 'ioc-api-interface';
export function useIREACTFeatures() {
  const [selector, actions, dispatch] = useRedux(
    ireactFeaturesSelector,
    ireactFeaturesActionCreators
  );

  return [selector, actions, dispatch];
}
