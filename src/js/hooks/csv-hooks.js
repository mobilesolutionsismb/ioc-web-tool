import { useState, useEffect } from 'react';
import Papa from 'papaparse';

export function useCSV(initialString = '', cfg = { header: true }) {
  const [csvString, setCsvString] = useState(initialString);
  const [items, setItems] = useState({ data: [], errors: [], meta: {} });

  useEffect(() => {
    if (csvString.length) {
      const objs = Papa.parse(csvString, cfg);
      setItems(objs);
    }
  }, [csvString]);

  function reset() {
    setItems({ data: [], errors: [], meta: {} });
    setCsvString('');
  }

  function loadCsv(dataString) {
    setCsvString(dataString);
  }

  return [items, loadCsv, reset];
}
