import { FILTERS } from './filters';

const STATE = {
  // Filter checkboxes are initially unchecked
  filters: { ...FILTERS, reportType: [], hazard: [], temporalStatus: [] },
  sorter: '_date_time_desc'
};

export default STATE;
