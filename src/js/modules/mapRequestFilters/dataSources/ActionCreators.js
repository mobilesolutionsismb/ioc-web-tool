import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';
const DESELECT_MAP_REQ = 'api::ireact_features@DESELECT_FEATURE';

export function resetAllMapRequestFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addMapRequestFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

export function addMapRequestFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no MapRequest is currently selected
    dispatch({ type: DESELECT_MAP_REQ });
    dispatch(_addMapRequestFilter(filterCategory, filterName));
  };
}

function _removeMapRequestFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeMapRequestFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no MapRequest is currently selected
    dispatch({ type: DESELECT_MAP_REQ });
    dispatch(_removeMapRequestFilter(filterCategory, filterName));
  };
}

export function setMapRequestSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
