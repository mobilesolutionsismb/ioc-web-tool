/* REPORT TYPE FILTERS */
const _is_type_delineation = ['==', 'layerType', 'delineation'];
const _is_type_nowcast = ['==', 'layerType', 'nowcast'];
const _is_type_forecast = ['==', 'layerType', 'forecast'];
const _is_type_burned_area = ['==', 'layerType', 'burnedArea'];
const _is_type_risk_map = ['==', 'layerType', 'riskMap'];
export const reportType = {
  _is_type_delineation,
  _is_type_nowcast,
  _is_type_forecast,
  _is_type_burned_area,
  _is_type_risk_map
};
/* REPORT HAZARD FILTERS */
const _hazard_fire = ['==', 'hazard', 'fire'];
const _hazard_flood = ['==', 'hazard', 'flood'];
const _hazard_landslide = ['==', 'hazard', 'landslide'];
const _hazard_extremeWeather = ['==', 'hazard', 'extremeWeather'];
const _hazard_avalanches = ['==', 'hazard', 'avalanches'];
const _hazard_earthquake = ['==', 'hazard', 'earthquake'];
const _hazard_unknown = ['==', 'hazard', 'unknown'];
const _hazard_none = ['==', 'hazard', 'none'];
export const hazard = {
  _hazard_fire,
  _hazard_flood,
  _hazard_landslide,
  _hazard_extremeWeather,
  _hazard_avalanches,
  _hazard_earthquake,
  _hazard_unknown,
  _hazard_none
};

/* MAP REQUEST STATUS FILTERS */
const _is_status_ongoing = ['==', 'temporalStatus', 'ongoing'];
const _is_status_closed = ['!=', 'temporalStatus', 'ongoing'];
export const temporalStatus = {
  _is_status_ongoing,
  _is_status_closed
};

export const FILTERS = {
  reportType: Object.keys(reportType),
  hazard: Object.keys(hazard),
  temporalStatus: Object.keys(temporalStatus)
};

export const SORTERS = {
  _date_time_asc: (f1, f2) => {
    if (f1.properties.start !== f2.properties.start)
      return new Date(f1.properties.start).getTime() - new Date(f2.properties.start).getTime();
    else return f1.properties.id - f2.properties.id;
  },
  _date_time_desc: (f1, f2) => {
    if (f2.properties.start !== f1.properties.start)
      return new Date(f2.properties.start).getTime() - new Date(f1.properties.start).getTime();
    else return f2.properties.id - f1.properties.id;
  }
  // TODO add _share_desc, _share_asc, _upvote_desc, _upvote_asc, _downvote_desc, _downvote_asc
};

export const availableFilters = Object.keys(FILTERS);
export const availableMapReqSorters = Object.keys(SORTERS);
