import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, reportType, temporalStatus, hazard } from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';

import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  reportType,
  hazard,
  temporalStatus
};

const select = createStructuredSelector({
  mapRequestFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.mapRequestFilters.filters, FilterMap, FILTERS),
  mapRequestFilters: state => state.mapRequestFilters.filters,
  mapRequestSorter: state => state.mapRequestFilters.sorter
});

export default connect(select, ActionCreators);
