import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  statistics: state => state.socialApi.statistics,
  cloudScale: state => state.socialApi.cloudScale,
  hashtagCloud: state => state.socialApi.hashtagCloud
});

export default connect(select, ActionCreators);
