import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  eventDetection: state => state.socialApi.eventDetection,
  eventIndex: state => state.socialApi.eventIndex
});

export default connect(select, ActionCreators);
