import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  tweets: state => state.socialApi.tweets,
  tweetsGeolocated: state => state.socialApi.tweetsGeolocated,
  isFetching: state => state.socialApi.isFetching,
  pagination: state => state.socialApi.pagination,
  isFetchingGeolocated: state => state.socialApi.isFetchingGeolocated,
  paginationGeolocated: state => state.socialApi.paginationGeolocated,
  hasMore: state => state.socialApi.hasMore,
  hasMoreGeolocated: state => state.socialApi.hasMoreGeolocated,
  tweetFeedback: state => state.socialApi.tweetFeedback
});

export default connect(
  select,
  ActionCreators
);
