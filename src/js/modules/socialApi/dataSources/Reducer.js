import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  TWEETS_GET_SUCCESS,
  TWEETS_GEOLOCATED_GET_SUCCESS,
  STATISTICS_GET_SUCCESS,
  STATISTICS_PROFILES_SORT,
  SET_CLOUD_SCALE,
  TWEETS_GET_STARTED,
  TWEETS_GEOLOCATED_GET_STARTED,
  CLEAN_PAGINATION,
  EVENT_DETECION_GET_SUCCESS,
  EVENT_UPDATED,
  NEXT_EVENT,
  PREVIOUS_EVENT,
  CLEAN_GEOLOCATED_PAGINATION,
  SET_TWEET_FEEDBACK
} from './Actions';

const mutators = {
  [TWEETS_GET_SUCCESS]: {
    tweets: (action, state) => {
      let allTweets = state.tweets.concat(action.tweets);
      return [...new Set(allTweets.map(o => JSON.stringify(o)))].map(s => JSON.parse(s));
    },
    pagination: (action, state) => {
      if (action.total > 0) return action.offset + 50;
      return state.pagination;
    },
    hasMore: action => {
      if (action.total > 0) return true;
      return false;
    },
    isFetching: action => action.isFetching
  },
  [TWEETS_GEOLOCATED_GET_SUCCESS]: {
    tweetsGeolocated: (action, state) => {
      let allTweets = state.tweetsGeolocated.concat(action.tweetsGeolocated);
      return [...new Set(allTweets.map(o => JSON.stringify(o)))].map(s => JSON.parse(s));
    },
    paginationGeolocated: (action, state) => {
      if (action.total > 0) return action.offset + 300;
      return state.paginationGeolocated;
    },
    hasMoreGeolocated: action => {
      if (action.total > 0) return true;
      return false;
    },
    isFetchingGeolocated: action => action.isFetchingGeolocated
  },
  [CLEAN_PAGINATION]: {
    pagination: 0,
    paginationGeolocated: 0,
    tweets: [],
    tweetsGeolocated: [],
    hasMore: true,
    hasMoreGeolocated: true
  },
  [CLEAN_GEOLOCATED_PAGINATION]: {
    paginationGeolocated: 0,
    tweetsGeolocated: [],
    hasMoreGeolocated: true
  },
  [STATISTICS_GET_SUCCESS]: {
    statistics: (action, state) => {
      return Object.assign({}, state.statistics, action.statistics);
    },
    hashtagCloud: (action, state) => {
      if (action.hashtagCloud) return action.hashtagCloud;
      else return state.hashtagCloud;
    }
  },
  [STATISTICS_PROFILES_SORT]: {
    statistics: (action, state) => {
      let s = state.statistics;
      s.topProfiles = action.profiles;
      return s;
    }
  },
  [SET_CLOUD_SCALE]: {
    cloudScale: (action, state) => {
      return action.cloudScale;
    }
  },
  [TWEETS_GET_STARTED]: {
    isFetching: action => action.isFetching
  },
  [TWEETS_GEOLOCATED_GET_STARTED]: {
    isFetchingGeolocated: action => action.isFetchingGeolocated
  },
  [EVENT_DETECION_GET_SUCCESS]: {
    eventDetection: action => action.events,
    eventIndex: 0
  },
  [EVENT_UPDATED]: {
    eventDetection: (action, state) => {
      let events = state.eventDetection;
      events.splice(state.eventIndex, 1);
      return events;
    }
  },
  [PREVIOUS_EVENT]: {
    eventIndex: (action, state) => {
      return state.eventIndex - 1 >= 0 ? state.eventIndex - 1 : state.eventDetection.length - 1;
    }
  },
  [NEXT_EVENT]: {
    eventIndex: (action, state) => {
      return state.eventIndex + 1 < state.eventDetection.length ? state.eventIndex + 1 : 0;
    }
  },
  [SET_TWEET_FEEDBACK]: {
    tweetFeedback: (action, state) => {
      let feedBack = JSON.parse(JSON.stringify(state.tweetFeedback));
      feedBack[action.tweetId] = action.feedBack;
      return feedBack;
    }
  }
};

export default reduceWith(mutators, DefaultState);
