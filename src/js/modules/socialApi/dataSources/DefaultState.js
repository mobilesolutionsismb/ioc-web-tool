const state = {
  tweets: [],
  tweetsGeolocated: [],
  statistics: [],
  cloudScale: {},
  hashtagCloud: [],
  isFetching: false,
  pagination: 0,
  isFetchingGeolocated: false,
  paginationGeolocated: 0,
  hasMoreGeolocated: true,
  hasMore: true,
  eventDetection: [],
  eventIndex: 0,
  tweetFeedback: {}
};

export default state;
