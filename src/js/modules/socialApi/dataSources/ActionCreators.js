import {
  TWEETS_GET_SUCCESS,
  TWEETS_GEOLOCATED_GET_SUCCESS,
  STATISTICS_GET_SUCCESS,
  STATISTICS_PROFILES_SORT,
  SET_CLOUD_SCALE,
  TWEETS_GET_STARTED,
  TWEETS_GEOLOCATED_GET_STARTED,
  CLEAN_PAGINATION,
  EVENT_DETECION_GET_SUCCESS,
  EVENT_UPDATED,
  NEXT_EVENT,
  PREVIOUS_EVENT,
  CLEAN_GEOLOCATED_PAGINATION,
  SET_TWEET_FEEDBACK
} from './Actions';
import axios from 'axios';
import qs from 'qs';
import isPojo from 'is-pojo';
import { toGeoJson, compare, orderEvents, buildParams, buildEmergencyEventParams } from '../utils';

const baseUrl = SOCIAL_BACKEND;

export function dispatchIfAction(dispatch, action) {
  if (typeof action === 'function') {
    const resultAction = action();
    // Try to unwrap
    dispatchIfAction(dispatch, resultAction);
  } else if (isPojo(action) && typeof action.type === 'string') {
    dispatch(action);
  }
}

export function getTweets(
  filters,
  end,
  start,
  offset = 0,
  limit = -1,
  geolocated = null,
  onLoadStart = null,
  onLoadEnd = null
) {
  return async (dispatch, getState) => {
    dispatchIfAction(dispatch, onLoadStart);
    var params = {};

    /*Apply event filters or general filters*/
    if (getState().SocialFilters.eventFilter) {
      const { emergency, endDate, startDate } = getState().SocialFilters.eventFilter;
      params = buildParams(
        { emergency, retweet: false, language: getState().SocialFilters.language },
        endDate,
        startDate,
        geolocated,
        getState().SocialFilters.bboxToSearch
      );
    } else
      params = buildParams(filters, end, start, geolocated, getState().SocialFilters.bboxToSearch);
    /** */

    if (offset !== -1) params.offset = offset;
    if (limit !== -1) params.limit = limit;

    const parsed = qs.stringify(params, { indices: false }); //we managed to pass several parameters with the same key (emergency or infotype)
    const request = axios({
      method: 'get',
      baseURL: baseUrl,
      url: `/select?${parsed}`,
      responseType: 'json',
      headers: {
        common: {
          Accept: 'application/json',
          'Accept-Language': 'en'
        }
      }
    });
    let response = null;

    try {
      response = await request.then(
        response => {
          if (geolocated === true) {
            let result = response.data.map(tweet => toGeoJson(tweet));
            return dispatchIfAction(
              dispatch,
              getTweetsGeolocatedSuccess(result, response.data.length, offset)
            );
          } else if (geolocated === false)
            return dispatchIfAction(
              dispatch,
              getTweetsSuccess(response.data, response.data.length, offset)
            );
        },
        err => console.error('Cannot load tweets', err)
      );
      dispatchIfAction(dispatch, onLoadEnd);
    } catch (e) {
      return dispatch(getTweetsSuccess([], []));
    }
    return response;
  };
}

export function getStatistics(filters, start, end) {
  return async (dispatch, getState) => {
    //dispatchIfAction(dispatch, onLoadStart);
    var params = {};

    /*Apply event filters or general filters*/
    if (getState().SocialFilters.eventFilter) {
      const { emergency, endDate, startDate } = getState().SocialFilters.eventFilter;
      params = buildParams(
        { emergency, retweet: false, language: getState().SocialFilters.language },
        endDate,
        startDate
      );
    } else params = buildParams(filters, end, start);
    /** */

    const parsed = qs.stringify(params, { indices: false }); //we managed to pass several parameters with the same key (emergency or infotype)
    const statistics = [
      'general_statistics',
      'profiles_statistics',
      'infotype_statistics',
      'ht_statistics'
    ];
    let apiCalls = [];
    statistics.forEach(value => {
      apiCalls.push(
        axios({
          method: 'get',
          baseURL: baseUrl,
          url: `/${value}?${parsed}`,
          responseType: 'json',
          headers: {
            common: {
              Accept: 'application/json',
              'Accept-Language': 'en'
            }
          }
        })
      );
    });
    let response = [];
    try {
      apiCalls.forEach(async value => {
        response = await value.then(
          response => {
            return dispatch(getStatisticsSuccess(response.data));
          },
          err => console.error('Cannot load statistics', err)
        );
      });
    } catch (e) {
      return dispatch(getStatisticsSuccess({}));
    }
    return response;
  };
}

export function getEventDetection(filters, endDate) {
  return async dispatch => {
    //dispatchIfAction(dispatch, onLoadStart);
    const params = buildEmergencyEventParams(filters);
    let apiCalls = [];
    params.forEach(value => {
      const parsed = qs.stringify(value); //we managed to pass several parameters with the same key (emergency or infotype)
      apiCalls.push(
        axios({
          method: 'get',
          baseURL: baseUrl,
          url: `/selectEventDetections?${parsed}`,
          responseType: 'json',
          headers: {
            common: {
              Accept: 'application/json',
              'Accept-Language': 'en'
            }
          }
        })
      );
    });
    let response = [];
    try {
      apiCalls.forEach(async value => {
        response = await value.then(
          response => {
            return dispatch(getEventDetectionSuccess(response.data));
          },
          err => {
            console.error('Cannot load events', err);
            return dispatch(getEventDetectionSuccess({}));
          }
        );
      });
    } catch (e) {
      return dispatch(getEventDetectionSuccess({}));
    }
    return response;
  };
}

export function updateStatusOfEvent(event, acepted) {
  return async dispatch => {
    const eventParams = qs.stringify(event);
    const request = axios({
      method: 'post',
      baseURL: baseUrl,
      url: `sendEventDetectionFeedback?accepted=${acepted}&${eventParams}`,
      responseType: 'json',
      headers: {
        common: {
          Accept: 'application/json',
          'Accept-Language': 'en'
        }
      }
    });
    let response = null;
    try {
      response = await request.then(
        response => {
          dispatchIfAction(dispatch, nextEventDetection());
          return dispatchIfAction(dispatch, updateEvent(response));
        },
        err => {
          console.error('Cannot change the event status', err);
          return dispatch(getEventDetectionSuccess({}));
        }
      );
    } catch (e) {
      return dispatch(updateEvent(false));
    }
    return response;
  };
}

export function tweetFeedBackSuccess(feedBack, tweetId) {
  return {
    type: SET_TWEET_FEEDBACK,
    feedBack,
    tweetId
  };
}

export function sortProfiles(profiles) {
  return {
    type: STATISTICS_PROFILES_SORT,
    profiles
  };
}
export function setCloudScale(cloudScale) {
  return {
    type: SET_CLOUD_SCALE,
    cloudScale
  };
}

export function getTweetsStarted() {
  return {
    type: TWEETS_GET_STARTED,
    isFetching: true
  };
}

export function getTweetsGeolocatedStarted() {
  return {
    type: TWEETS_GEOLOCATED_GET_STARTED,
    isFetchingGeolocated: true
  };
}

function getTweetsGeolocatedSuccess(tweetsGeolocated, total, offset) {
  return {
    type: TWEETS_GEOLOCATED_GET_SUCCESS,
    isFetchingGelocated: false,
    tweetsGeolocated,
    total,
    offset
  };
}

function getTweetsSuccess(tweets, total, offset) {
  return {
    type: TWEETS_GET_SUCCESS,
    isFetching: false,
    tweets,
    total,
    offset
  };
}

export function cleanPagination() {
  return {
    type: CLEAN_PAGINATION
  };
}

export function cleanGeolocationPagination() {
  return {
    type: CLEAN_GEOLOCATED_PAGINATION
  };
}

function getStatisticsSuccess(statistics) {
  let hashtagCloud = statistics.topHashtags;
  if (hashtagCloud) {
    statistics.topHashtags = statistics.topHashtags.sort(compare);
  }
  return {
    type: STATISTICS_GET_SUCCESS,
    statistics,
    hashtagCloud
  };
}

function getEventDetectionSuccess(result) {
  let events = [];

  events = events
    .concat(result)
    .sort(orderEvents)
    .filter(a => a.status === 'PENDING');
  return {
    type: EVENT_DETECION_GET_SUCCESS,
    events
  };
}

function updateEvent(response) {
  return {
    type: EVENT_UPDATED,
    response
  };
}

export function nextEventDetection() {
  return {
    type: NEXT_EVENT
  };
}

export function previousEventDetection() {
  return {
    type: PREVIOUS_EVENT
  };
}
