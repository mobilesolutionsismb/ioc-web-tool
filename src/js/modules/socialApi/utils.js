import moment from 'moment-timezone';

export function buildParams(filters, endDate, startDate, geolocated, bbox) {
  var builtParams = Object.assign({}, filters);

  /*Build the date interval*/
  if (!startDate)
    builtParams.dateFrom = moment(endDate)
      .subtract(filters.dateTo.number, filters.dateTo.unity)
      .tz('Etc/GMT')
      .format('YYYYMMDDHHmm');
  else
    builtParams.dateFrom = moment(startDate)
      .tz('Etc/GMT')
      .format('YYYYMMDDHHmm');

  builtParams.dateTo = moment(endDate)
    .tz('Etc/GMT')
    .format('YYYYMMDDHHmm');

  /*Build the geolocalizedinput*/
  if (geolocated !== null) {
    builtParams.localized = geolocated;
    if (bbox !== null && geolocated)
      builtParams.bbox = [...bbox[0], ...bbox[2]].reduce(
        (valorAnterior, valorActual) => `${valorAnterior};${valorActual}`
      );
  }

  builtParams.emergency = filters.emergency.map(a => a.value);

  if (builtParams.infotype) builtParams.infotype = filters.infotype.map(a => a.value);

  if (builtParams.tags) {
    builtParams.informative = builtParams.tags.find(a => a.value.type === 'informative');
    if (!builtParams.informative) delete builtParams.informative;
    else builtParams.informative = builtParams.informative.value.value;

    builtParams.sentiment = builtParams.tags.find(a => a.value.type === 'sentiment');
    if (!builtParams.sentiment) delete builtParams.sentiment;
    else builtParams.sentiment = builtParams.sentiment.value.value;
  }

  if (builtParams.retweet === true) delete builtParams.retweet;
  builtParams.user = -1;

  delete builtParams.tags;
  if (geolocated) console.log(builtParams);
  return builtParams;
}

export function buildEmergencyEventParams(filters, endDate) {
  const dateFrom = moment(endDate)
    .subtract(filters.dateTo.number, filters.dateTo.unity)
    .tz('Etc/GMT')
    .format('YYYYMMDDHHmm');
  const dateTo = moment(endDate)
    .tz('Etc/GMT')
    .format('YYYYMMDDHHmm');
  var builtParams = [
    {
      //dateFrom: dateFrom, /* No events with this parameters */
      //dateTo: dateTo,
      language: filters.language
    }
  ];

  filters.emergency.forEach((emergency, index) => {
    builtParams[index] = {
      emergencyType: emergency.value,
      dateFrom: dateFrom,
      dateTo: dateTo,
      language: filters.language
    };
  });
  return builtParams;
}

export function toGeoJson(actualValue) {
  let coordinates = [];
  if (actualValue.geoLocation) {
    coordinates = [actualValue.geoLocation.longitude, actualValue.geoLocation.latitude];
  } else if (actualValue.bbox) {
    const xmin = actualValue.bbox.southWest.longitude,
      xmax = actualValue.bbox.northEast.longitude,
      ymin = actualValue.bbox.southWest.latitude,
      ymax = actualValue.bbox.northEast.latitude;
    coordinates = [xmin + (xmax - xmin) / 2, ymin + (ymax - ymin) / 2];
  } else if (actualValue.originalTweet.place) {
    const xmin = actualValue.originalTweet.place.boundingBoxCoordinates[0].longitude,
      xmax = actualValue.originalTweet.place.boundingBoxCoordinates[2].longitude,
      ymin = actualValue.originalTweet.place.boundingBoxCoordinates[0].latitude,
      ymax = actualValue.originalTweet.place.boundingBoxCoordinates[2].latitude;
    coordinates = [xmin + (xmax - xmin) / 2, ymin + (ymax - ymin) / 2];
  } else if (actualValue.originalTweet.place) {
    const xmin = actualValue.originalTweet.place.boundingBoxCoordinates[0].longitude,
      xmax = actualValue.originalTweet.place.boundingBoxCoordinates[2].longitude,
      ymin = actualValue.originalTweet.place.boundingBoxCoordinates[0].latitude,
      ymax = actualValue.originalTweet.place.boundingBoxCoordinates[2].latitude;
    coordinates = [xmin + (xmax - xmin) / 2, ymin + (ymax - ymin) / 2];
  } else if (actualValue.entities.length > 0) {
    coordinates = [actualValue.entities[0].longitude, actualValue.entities[0].latitude];
  }
  const geometry = {
    coordinates: coordinates,
    type: 'Point'
  };
  return {
    properties: actualValue,
    geometry: geometry
  };
}

export function compare(a, b) {
  //only needed because cloud word uses topHashtags field
  if (a.total > b.total) return -1;
  if (a.total < b.total) return 1;
  return 0;
}

export function orderEvents(a, b) {
  if (a.dateFrom > b.dateFrom) return -1;
  if (a.dateFrom < b.dateFrom) return 1;
  return 0;
}
