import Reducer from './dataSources/Reducer';

import withTweets from './dataProviders/withTweets';
import withStatistics from './dataProviders/withStatistics';
import withEventDetection from './dataProviders/withEventDetection';

export { Reducer, withTweets, withStatistics, withEventDetection };
export default Reducer;
