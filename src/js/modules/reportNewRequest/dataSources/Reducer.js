import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_END,
  RESET_ALL,
  SET_REPORTERS_ALLOWED,
  SET_VISIBILITY,
  SET_REPORTERS_ALLOWED_TEAMS,
  SET_REPORTERS_ALLOWED_LEVEL,
  REMOVE_CONTENT_CATEGORY,
  SET_CONTENT_TYPE,
  SET_CONTENT_CATEGORY,
  SET_CONTENT_MEASURES,
  FILL_REPORT_REQUEST
} from './Actions';

const mutators = {
  [SET_START_DATE]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.startDate = action.startDate;
      return newReportRequest;
    }
  },
  [SET_START_TIME]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.startTime = action.startTime;
      return newReportRequest;
    }
  },
  [SET_END_DATE]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.endDate = action.endDate;
      return newReportRequest;
    }
  },
  [SET_END_TIME]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.endTime = action.endTime;
      return newReportRequest;
    }
  },
  [SET_VISIBILITY]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.visibility = action.visibility;
      return newReportRequest;
    }
  },
  [RESET_END]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.endTime = null;
      newReportRequest.endDate = null;
      return newReportRequest;
    }
  },
  [FILL_REPORT_REQUEST]: {
    newReportRequest: (action, state) => {
      const repReq = action.reportRequest;
      let newReportRequest = {
        startDate: new Date(repReq.start),
        startTime: new Date(repReq.start),
        endDate: new Date(repReq.end),
        endTime: new Date(repReq.end),
        reportersAllowed: repReq.roles ? repReq.roles.map(item => parseInt(item.id, 10)) : [],
        teams: [],
        minimumLevel: '',
        contentType: repReq.contentType
          ? repReq.contentType
              .split(',')
              .map(item => item.trim())
              .sort(type => type === 'measure')
          : [],
        categoryType:
          repReq.measures && repReq.measures.length > 0
            ? {
                value: repReq.measures[0].category,
                label: repReq.measures[0].category.toUpperCase()
              }
            : { value: '', label: '' },
        roles: repReq.roles,
        measures: repReq.measures.map(function(item) {
          item.value = item.id;
          item.label = item.title;
          return item;
        }),
        id: repReq.id,
        address: repReq.address,
        visibility: repReq.visibility
      };
      return newReportRequest;
    }
  },
  //#region Reporters Allowed
  [SET_REPORTERS_ALLOWED]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      let reportersAllowed = newReportRequest.reportersAllowed;
      let index = reportersAllowed.indexOf(action.item);
      if (index >= 0) reportersAllowed.splice(index, 1);
      else reportersAllowed.push(action.item);

      return newReportRequest;
    }
  },
  [SET_REPORTERS_ALLOWED_TEAMS]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.teams = action.teams;
      return newReportRequest;
    }
  },
  [SET_REPORTERS_ALLOWED_LEVEL]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.minimumLevel = action.minimumLevel;
      return newReportRequest;
    }
  },
  //#endregion

  //#region  Content Type
  [SET_CONTENT_TYPE]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      let contentType = newReportRequest.contentType;
      let index = contentType.indexOf(action.item);

      if (index >= 0) contentType.splice(index, 1);
      else contentType.push(action.item);

      return newReportRequest;
    }
  },
  [SET_CONTENT_CATEGORY]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.categoryType = action.categoryType;
      newReportRequest.measures = [];
      return newReportRequest;
    }
  },
  [REMOVE_CONTENT_CATEGORY]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.categoryType = { value: '', label: '' };
      newReportRequest.measures = [];
      return newReportRequest;
    }
  },
  [SET_CONTENT_MEASURES]: {
    newReportRequest: (action, state) => {
      let newReportRequest = state.newReportRequest;
      newReportRequest.measures = action.measures;
      return newReportRequest;
    }
  },
  //#endregion
  [RESET_ALL]: {
    newReportRequest: (action, state) => {
      let newReportRequest = {
        startDate: null,
        startTime: null,
        endDate: null,
        endTime: null,
        reportersAllowed: [],
        contentType: [],
        teams: [],
        minimumLevel: '',
        measures: [],
        categoryType: { value: '', label: '' },
        id: 0,
        visibility: ''
      };
      return newReportRequest;
    }
  }
};

export default reduceWith(mutators, DefaultState);
