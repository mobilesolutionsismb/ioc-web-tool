export const CONTENT_TYPE = {
  damage: 'damage',
  measure: 'measure',
  resource: 'resource',
  people: 'people'
};
