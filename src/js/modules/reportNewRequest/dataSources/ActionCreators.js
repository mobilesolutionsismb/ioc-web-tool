import {
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_END,
  RESET_ALL,
  SET_CONTENT_TYPE,
  SET_CONTENT_CATEGORY,
  SET_VISIBILITY,
  REMOVE_CONTENT_CATEGORY,
  SET_CONTENT_MEASURES,
  SET_REPORTERS_ALLOWED,
  SET_REPORTERS_ALLOWED_TEAMS,
  SET_REPORTERS_ALLOWED_LEVEL,
  FILL_REPORT_REQUEST
  // TOGGLE_REPORT_REQUEST_DETAIL
} from './Actions';

function setReportNewRequestStartDate(startDate) {
  return {
    type: SET_START_DATE,
    startDate
  };
}

function setReportNewRequestStartTime(startTime) {
  return {
    type: SET_START_TIME,
    startTime
  };
}

function setReportNewRequestEndDate(endDate) {
  return {
    type: SET_END_DATE,
    endDate
  };
}

function setReportNewRequestEndTime(endTime) {
  return {
    type: SET_END_TIME,
    endTime
  };
}

function resetReportNewRequestEnd() {
  return {
    type: RESET_END
  };
}

function resetReportNewRequest() {
  return {
    type: RESET_ALL
  };
}

function setContentType(item) {
  return {
    type: SET_CONTENT_TYPE,
    item
  };
}

function setContentCategory(categoryType) {
  return {
    type: SET_CONTENT_CATEGORY,
    categoryType
  };
}

function setVisibility(visibility) {
  return {
    type: SET_VISIBILITY,
    visibility
  };
}

function removeContentCategory() {
  return {
    type: REMOVE_CONTENT_CATEGORY
  };
}

function setContentMeasures(measures) {
  return {
    type: SET_CONTENT_MEASURES,
    measures
  };
}

function setReportersAllowed(item) {
  return {
    type: SET_REPORTERS_ALLOWED,
    item
  };
}

function setReportersAllowedTeams(teams) {
  return {
    type: SET_REPORTERS_ALLOWED_TEAMS,
    teams
  };
}

function setReportersAllowedLevel(minimumLevel) {
  return {
    type: SET_REPORTERS_ALLOWED_LEVEL,
    minimumLevel
  };
}

function fillReportRequest(reportRequest) {
  return {
    type: FILL_REPORT_REQUEST,
    reportRequest
  };
}

export {
  setReportNewRequestStartDate,
  setReportNewRequestStartTime,
  setReportNewRequestEndDate,
  setReportNewRequestEndTime,
  resetReportNewRequestEnd,
  resetReportNewRequest,
  setReportersAllowed,
  setContentType,
  removeContentCategory,
  setContentCategory,
  setContentMeasures,
  setReportersAllowedTeams,
  setReportersAllowedLevel,
  fillReportRequest,
  setVisibility
};
