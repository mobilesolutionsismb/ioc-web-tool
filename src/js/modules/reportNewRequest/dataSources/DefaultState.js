const STATE = {
  newReportRequest: {
    startDate: null,
    startTime: null,
    endDate: null,
    endTime: null,
    reportersAllowed: [],
    teams: [],
    minimumLevel: '',
    contentType: [],
    categoryType: { value: '', label: '' },
    measures: [],
    id: 0,
    address: '',
    visibility: ''
  }
};

export default STATE;
