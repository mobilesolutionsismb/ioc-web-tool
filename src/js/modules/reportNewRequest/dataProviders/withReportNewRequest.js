import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const newReportRequest = state => state.reportNewRequest.newReportRequest;

const select = createStructuredSelector({
  newReportRequest
});

export default connect(select, ActionCreators);
