import Reducer from './dataSources/Reducer';
import withReportNewRequest from './dataProviders/withReportNewRequest';

export { withReportNewRequest, Reducer };
export {
  setReportNewRequestStartDate,
  setReportNewRequestStartTime,
  setReportNewRequestEndDate,
  setReportNewRequestEndTime,
  resetReportNewRequestEnd,
  resetReportNewRequest,
  setReportersAllowed,
  setContentType,
  setVisibility,
  removeContentCategory,
  setContentCategory,
  setContentMeasures,
  setReportersAllowedTeams,
  setReportersAllowedLevel,
  fillReportRequest
} from './dataSources/ActionCreators';

export { CONTENT_TYPE } from './dataSources/consts';

export default Reducer;
