import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, reportType, hazard, status, userType } from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  reportType,
  hazard,
  status,
  userType
};

const select = createStructuredSelector({
  reportFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.reportFilters.filters, FilterMap, FILTERS),
  reportFilters: state => state.reportFilters.filters,
  reportSorter: state => state.reportFilters.sorter
});

export default connect(select, ActionCreators);
