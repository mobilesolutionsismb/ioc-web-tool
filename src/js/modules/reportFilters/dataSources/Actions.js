const NS = 'REPORT_FILTERS';

export const RESET_ALL = `${NS}@RESET_ALL`;
export const SORTER_CHANGE = `${NS}@SORTER_CHANGE`;
export const ADD_FILTER = `${NS}@ADD_FILTER`;
export const REMOVE_FILTER = `${NS}@REMOVE_FILTER`;

// export const SET_DRAW_STEP = `${NS}@SET_DRAW_STEP`;
// export const SET_DRAW_POINT = `${NS}@SET_DRAW_POINT`;
// export const SET_DRAW_POLYGON = `${NS}@SET_DRAW_POLYGON`;
// export const RESET_DRAW = `${NS}@RESET_DRAW`;
// export const SET_DRAW_TRASH = `${NS}@SET_DRAW_TRASH`;
