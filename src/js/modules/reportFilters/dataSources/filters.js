/* REPORT TYPE FILTERS */
const _is_type_measure = ['==', 'type', 'measure'];
const _is_type_damage = ['==', 'type', 'damage'];
const _is_type_resource = ['==', 'type', 'resource'];
const _is_type_people = ['==', 'type', 'people'];
const _is_type_misc = ['==', 'type', 'misc'];
export const reportType = {
  _is_type_measure,
  _is_type_damage,
  _is_type_resource,
  _is_type_people,
  _is_type_misc
};

/* REPORT HAZARD FILTERS */
const _hazard_fire = ['==', 'hazard', 'fire'];
const _hazard_flood = ['==', 'hazard', 'flood'];
const _hazard_landslide = ['==', 'hazard', 'landslide'];
const _hazard_extremeWeather = ['==', 'hazard', 'extremeWeather'];
const _hazard_avalanches = ['==', 'hazard', 'avalanches'];
const _hazard_earthquake = ['==', 'hazard', 'earthquake'];
const _hazard_unknown = ['==', 'hazard', 'unknown'];
const _hazard_none = ['==', 'hazard', 'none'];
export const hazard = {
  _hazard_fire,
  _hazard_flood,
  _hazard_landslide,
  _hazard_extremeWeather,
  _hazard_avalanches,
  _hazard_earthquake,
  _hazard_unknown,
  _hazard_none
};

/* REPORT STATUS FILTERS */
const _is_status_validated = ['==', 'status', 'validated'];
const _is_status_inappropriate = ['==', 'status', 'inappropriate'];
const _is_status_inaccurate = ['==', 'status', 'inaccurate'];
const _is_status_submitted = ['==', 'status', 'submitted'];
export const status = {
  _is_status_validated,
  _is_status_inappropriate,
  _is_status_inaccurate,
  _is_status_submitted
};

/* USER TYPE FILTERS */
const _is_authority = ['!=', 'organization', null];
const _is_citizen = ['==', 'organization', null];
export const userType = {
  _is_authority,
  _is_citizen
};

export const FILTERS = {
  reportType: Object.keys(reportType),
  hazard: Object.keys(hazard),
  status: Object.keys(status),
  userType: Object.keys(userType)
};

export const SORTERS = {
  _date_time_asc: (f1, f2) => {
    if (f1.properties.userCreationTime !== f2.properties.userCreationTime)
      return (
        new Date(f1.properties.userCreationTime).getTime() -
        new Date(f2.properties.userCreationTime).getTime()
      );
    else return f1.properties.id - f2.properties.id;
  },
  _date_time_desc: (f1, f2) => {
    if (f2.properties.userCreationTime !== f1.properties.userCreationTime)
      return (
        new Date(f2.properties.userCreationTime).getTime() -
        new Date(f1.properties.userCreationTime).getTime()
      );
    else return f2.properties.id - f1.properties.id;
  }
  // TODO add _share_desc, _share_asc, _upvote_desc, _upvote_asc, _downvote_desc, _downvote_asc
};

export const availableFilters = Object.keys(FILTERS);
export const availableSorters = Object.keys(SORTERS);
