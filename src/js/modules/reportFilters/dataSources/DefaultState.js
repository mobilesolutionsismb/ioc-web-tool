import { FILTERS } from './filters';

const STATE = {
  // By setting userType to empty we have the same result of selecting both userType filters
  // But filter checkboxes are initially unchecked
  filters: { ...FILTERS, userType: [], reportType: [], hazard: [], status: [] },
  sorter: '_date_time_desc'
};

export default STATE;
