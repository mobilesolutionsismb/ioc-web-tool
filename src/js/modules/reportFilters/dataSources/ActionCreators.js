import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';
const DESELECT_FEATURE = 'api::ireact_features@DESELECT_FEATURE';

export function resetAllReportFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addReportFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

export function addReportFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no report is currently selected
    dispatch({ type: DESELECT_FEATURE });
    dispatch(_addReportFilter(filterCategory, filterName));
  };
}

function _removeReportFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeReportFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no report is currently selected
    dispatch({ type: DESELECT_FEATURE });
    dispatch(_removeReportFilter(filterCategory, filterName));
  };
}

export function setReportSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
