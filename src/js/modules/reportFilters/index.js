import Reducer from './dataSources/Reducer';
import withReportFilters from './dataProviders/withReportFilters';
import * as reportFiltersActions from './dataSources/ActionCreators';
export {
  availableSorters,
  availableFilters,
  FILTERS,
  SORTERS,
  reportType as REPORT_TYPE_FILTERS,
  hazard as HAZARD_FILTERS,
  status as STATUS_FILTERS,
  userType as USER_TYPE_FILTERS
} from './dataSources/filters';

export default Reducer;
export { Reducer, withReportFilters, reportFiltersActions };
