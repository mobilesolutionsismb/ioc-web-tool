const NS = 'map';
const UPDATE_MAP = `${NS}@UPDATE_MAP`;
const RESET_MAP = `${NS}@RESET_MAP`;

export { UPDATE_MAP, RESET_MAP };