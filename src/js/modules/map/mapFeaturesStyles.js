export const metersToPixelsAtMaxZoom = (meters, latitude) => meters / 0.075 / Math.cos(latitude * Math.PI / 180);

export function makeDefaultCircle(circleColor = '#0066EE', strokeColor = '#FFFFFF') {
    return {
        'circle-stroke-width': 2,
        'circle-radius': 10, //pixels
        'circle-blur': 0.15,
        'circle-color': circleColor,
        'circle-stroke-color': strokeColor,
        'circle-pitch-scale': 'map'
    };
}

export const DEFAULT_POSITION_CIRCLE_PAINT = makeDefaultCircle();

export function variableCircleInMeters(radiusInMeters, latitude, circleColor = '#0066EE', strokeColor = '#FFFFFF') {
    return {
        'circle-stroke-width': 2,
        'circle-radius': {
            stops: [
                [0, 0],
                [20, metersToPixelsAtMaxZoom(radiusInMeters, latitude)]
            ],
            base: 2
        },
        'circle-blur': 0.15,
        'circle-color': circleColor,
        'circle-opacity': 0.5,
        'circle-stroke-color': strokeColor,
        'circle-pitch-scale': 'map'
    };
}
