import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const hazard = state => state.mapRequestCreate.hazard;
const startDate = state => state.mapRequestCreate.startDate;
const startTime = state => state.mapRequestCreate.startTime;
const endDate = state => state.mapRequestCreate.endDate;
const endTime = state => state.mapRequestCreate.endTime;
const comment = state => state.mapRequestCreate.comment;
const eventStartDate = state => state.mapRequestCreate.eventStartDate;
const eventStartTime = state => state.mapRequestCreate.eventStartTime;
const eventExAreaDate = state => state.mapRequestCreate.eventExAreaDate;
const eventExAreaTime = state => state.mapRequestCreate.eventExAreaTime;
const origin_flow = state => state.mapRequestCreate.origin_flow;
const frequency = state => state.mapRequestCreate.frequency;
const limitTimeWindow = state => state.mapRequestCreate.limitTimeWindow;
const select = createStructuredSelector({
  hazard,
  startDate,
  startTime,
  endDate,
  endTime,
  comment,
  origin_flow,
  frequency,
  eventStartDate,
  eventStartTime,
  eventExAreaDate,
  eventExAreaTime,
  limitTimeWindow
});

export default connect(select, ActionCreators);
