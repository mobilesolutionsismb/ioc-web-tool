const STATE = {
  hazard: '',
  comment: '',
  startDate: null,
  startTime: null,
  endDate: null,
  endTime: null,
  origin_flow: '',
  eventStartDate: null,
  eventStartTime: null,
  eventExAreaDate: null,
  eventExAreaTime: null,
  limitTimeWindow: null,
  frequency: [
    { name: '_is_type_delineation', value: undefined, checked: false },
    { name: '_is_type_burned_area', value: undefined, checked: false },
    { name: '_is_type_nowcast', value: undefined, checked: false },
    { name: '_is_type_forecast', value: undefined, checked: false },
    { name: '_is_type_risk_map', value: undefined, checked: false }
  ]
};

export default STATE;
