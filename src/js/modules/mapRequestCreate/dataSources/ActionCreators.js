import {
  SET_HAZARD,
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_ALL_START_DATE,
  RESET_ALL_END_DATE,
  SET_ORIGIN_FLOW,
  RESET_DATE,
  RESET_ALL,
  SET_FREQUENCY,
  RESET_FREQUENCY,
  SET_EVENT_START_DATE,
  SET_EVENT_START_TIME,
  SET_EVENT_EX_AREA_DATE,
  SET_EVENT_EX_AREA_TIME,
  RESET_ALL_EVENT_START_DATE,
  RESET_ALL_EVENT_EX_AREA_DATE,
  RESET_STEP5
} from './Actions';

function setMapRequestCreateHazard(hazard) {
  return {
    type: SET_HAZARD,
    hazard
  };
}

function setMapRequestCreateStartDate(startDate) {
  return {
    type: SET_START_DATE,
    startDate
  };
}

function setMapRequestCreateStartTime(startTime) {
  return {
    type: SET_START_TIME,
    startTime
  };
}

function setMapRequestCreateEndDate(endDate) {
  return {
    type: SET_END_DATE,
    endDate
  };
}

function setMapRequestCreateEndTime(endTime) {
  return {
    type: SET_END_TIME,
    endTime
  };
}
function resetMapRequestCreateAllSDate() {
  return {
    type: RESET_ALL_START_DATE
  };
}
function resetMapRequestCreateAllEDate() {
  return {
    type: RESET_ALL_END_DATE
  };
}
function resetMapRequestCreate() {
  return {
    type: RESET_ALL
  };
}

function setMapRequestCreateEventStartDate(startDate) {
  return {
    type: SET_EVENT_START_DATE,
    startDate
  };
}

function setMapRequestCreateEventStartTime(startTime) {
  return {
    type: SET_EVENT_START_TIME,
    startTime
  };
}

function setMapRequestCreateExAreaDate(endDate) {
  return {
    type: SET_EVENT_EX_AREA_DATE,
    endDate
  };
}

function setMapRequestCreateExAreaTime(endTime) {
  return {
    type: SET_EVENT_EX_AREA_TIME,
    endTime
  };
}
function resetEventStartDate() {
  return {
    type: RESET_ALL_EVENT_START_DATE
  };
}
function resetEventExAreaDate() {
  return {
    type: RESET_ALL_EVENT_EX_AREA_DATE
  };
}
function setMapRequestCreateOF(originFlow) {
  return {
    type: SET_ORIGIN_FLOW,
    originFlow
  };
}

function resetDate() {
  return {
    type: RESET_DATE
  };
}

function setMapReqCreateFrequency(frequency) {
  return {
    type: SET_FREQUENCY,
    frequency
  };
}
function resetFrequency() {
  return {
    type: RESET_FREQUENCY
  };
}
function resetStep5() {
  return {
    type: RESET_STEP5
  };
}

export {
  setMapRequestCreateHazard,
  setMapRequestCreateStartDate,
  setMapRequestCreateStartTime,
  setMapRequestCreateEndDate,
  setMapRequestCreateEndTime,
  resetMapRequestCreateAllSDate,
  resetMapRequestCreateAllEDate,
  resetMapRequestCreate,
  setMapRequestCreateOF,
  resetDate,
  setMapReqCreateFrequency,
  resetFrequency,
  setMapRequestCreateEventStartDate,
  setMapRequestCreateEventStartTime,
  setMapRequestCreateExAreaDate,
  setMapRequestCreateExAreaTime,
  resetEventStartDate,
  resetEventExAreaDate,
  resetStep5
};
