import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  SET_HAZARD,
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_ALL_START_DATE,
  RESET_ALL_END_DATE,
  SET_ORIGIN_FLOW,
  RESET_DATE,
  RESET_ALL,
  SET_FREQUENCY,
  RESET_FREQUENCY,
  SET_EVENT_START_DATE,
  SET_EVENT_START_TIME,
  SET_EVENT_EX_AREA_DATE,
  SET_EVENT_EX_AREA_TIME,
  RESET_ALL_EVENT_START_DATE,
  RESET_ALL_EVENT_EX_AREA_DATE,
  RESET_STEP5
} from './Actions';

const mutators = {
  [SET_HAZARD]: {
    hazard: (action, state) => {
      return action.hazard;
    },
    frequency: (action, state) => {
      if (action.hazard === 'fire') {
        state.frequency[0] = { name: '_is_type_delineation', value: undefined, checked: false };
      } else if (action.hazard === 'flood') {
        state.frequency[1] = { name: '_is_type_burned_area', value: undefined, checked: false };
      }
      return state.frequency;
    },
    limitTimeWindow: (action, state) => {
      let time = null;
      if (
        (state.frequency[0].checked && state.frequency[0].value !== undefined) ||
        (state.frequency[1].checked && state.frequency[1].value !== undefined) ||
        (state.frequency[2].checked && state.frequency[2].value !== undefined)
      ) {
        time = 6;
      } else if (
        (state.frequency[3].checked && state.frequency[3].value !== undefined) ||
        (state.frequency[4].checked && state.frequency[4].value !== undefined)
      ) {
        time = 72;
      }
      return time;
    }
  },
  [SET_START_DATE]: {
    startDate: (action, state) => {
      return action.startDate;
    }
  },
  [SET_START_TIME]: {
    startTime: (action, state) => {
      return action.startTime;
    }
  },
  [SET_END_DATE]: {
    endDate: (action, state) => {
      return action.endDate;
    }
  },
  [SET_END_TIME]: {
    endTime: (action, state) => {
      return action.endTime;
    }
  },
  [RESET_ALL_START_DATE]: {
    startDate: null,
    startTime: null
  },
  [RESET_ALL_END_DATE]: {
    endDate: null,
    endTime: null
  },
  [SET_ORIGIN_FLOW]: {
    origin_flow: action => action.originFlow
  },
  [RESET_DATE]: {
    startDate: null,
    startTime: null,
    endDate: null,
    endTime: null
  },
  [RESET_ALL]: DefaultState,
  [SET_FREQUENCY]: {
    frequency: (action, state) => {
      let i = state.frequency.findIndex(e => e.name === action.frequency.name);
      if (i !== -1) {
        state.frequency[i] = action.frequency;
        if (
          action.frequency.name === '_is_type_risk_map' &&
          action.frequency.checked &&
          action.frequency.value
        ) {
          state.frequency[2].checked = true;
          state.frequency[2].value = action.frequency.value;
        } else if (
          action.frequency.name === '_is_type_nowcast' &&
          state.frequency[4].checked &&
          state.frequency[4].value
        ) {
          state.frequency[4].checked = action.frequency.checked;
          state.frequency[4].value = action.frequency.value;
        }
      }
      return state.frequency;
    },
    limitTimeWindow: (action, state) => {
      let time = null;
      if (
        (state.frequency[0].checked && state.frequency[0].value !== undefined) ||
        (state.frequency[1].checked && state.frequency[1].value !== undefined) ||
        (state.frequency[2].checked && state.frequency[2].value !== undefined)
      ) {
        time = 6;
      } else if (
        (state.frequency[3].checked && state.frequency[3].value !== undefined) ||
        (state.frequency[4].checked && state.frequency[4].value !== undefined)
      ) {
        time = 72;
      }
      return time;
    }
  },
  [RESET_FREQUENCY]: {
    frequency: (action, state) => {
      return [
        { name: '_is_type_delineation', value: undefined, checked: false },
        { name: '_is_type_burned_area', value: undefined, checked: false },
        { name: '_is_type_nowcast', value: undefined, checked: false },
        { name: '_is_type_forecast', value: undefined, checked: false },
        { name: '_is_type_risk_map', value: undefined, checked: false }
      ];
    }
  },
  [SET_EVENT_START_DATE]: {
    eventStartDate: (action, state) => {
      return action.startDate;
    }
  },
  [SET_EVENT_START_TIME]: {
    eventStartTime: (action, state) => {
      return action.startTime;
    }
  },
  [SET_EVENT_EX_AREA_DATE]: {
    eventExAreaDate: (action, state) => {
      return action.endDate;
    }
  },
  [SET_EVENT_EX_AREA_TIME]: {
    eventExAreaTime: (action, state) => {
      return action.endTime;
    }
  },
  [RESET_ALL_EVENT_START_DATE]: {
    eventStartDate: null,
    eventStartTime: null
  },
  [RESET_ALL_EVENT_EX_AREA_DATE]: {
    eventExAreaDate: null,
    eventExAreaTime: null
  },
  [RESET_STEP5]: {
    origin_flow: '',
    eventStartDate: null,
    eventStartTime: null,
    eventExAreaDate: null,
    eventExAreaTime: null
  }
};

export default reduceWith(mutators, DefaultState);
