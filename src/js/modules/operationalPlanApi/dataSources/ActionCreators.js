import {
  MY_OP_GET_SUCCESS,
  ANT_CONS_GET_SUCCESS,
  CREATE_RULE_SUCCESS,
  RESET_ERROR,
  RESTART_OP
} from './Actions';
import axios from 'axios';
import isPojo from 'is-pojo';

const baseUrl = 'https://backend.dev.ireact.answare-tech.com/api/v1.0';

export function dispatchIfAction(dispatch, action) {
  if (typeof action === 'function') {
    const resultAction = action();
    // Try to unwrap
    dispatchIfAction(dispatch, resultAction);
  } else if (isPojo(action) && typeof action.type === 'string') {
    dispatch(action);
  }
}

export function getMyProcedures(OrgId, onLoadStart = null, onLoadEnd = null) {
  return async dispatch => {
    dispatchIfAction(dispatch, restartOP());
    const request = axios({
      method: 'get',
      baseURL: baseUrl,
      url: `/organization/${OrgId}/rules/`,
      responseType: 'json',
      headers: {
        common: {
          Accept: 'application/json',
          'Accept-Language': 'en'
        }
      }
    });
    let response = null;

    try {
      response = await request.then(
        response => {
          return dispatchIfAction(dispatch, getMyOPSuccess(response.data));
        },
        err => {
          dispatch(getMyOPSuccess(undefined, true));
          console.error('Cannot load my operational procedures list', err);
        }
      );
      dispatchIfAction(dispatch, onLoadEnd);
    } catch (e) {
      return dispatch(getMyOPSuccess(undefined, true));
    }
    return response;
  };
}
export function getAntCons(onLoadStart = null, onLoadEnd = null) {
  return async dispatch => {
    dispatchIfAction(dispatch, onLoadStart);

    const request = axios({
      method: 'get',
      //baseURL: 'https://api.myjson.com',
      //url: '/bins/emzv0',
      baseURL: baseUrl,
      url: '/antecedentconsecuent/?last=true',
      responseType: 'json',
      headers: {
        common: {
          Accept: 'application/json',
          'Accept-Language': 'en'
        }
      }
    });
    let response = null;

    try {
      response = await request.then(
        response => {
          return dispatchIfAction(dispatch, getAntConsSuccess(response.data));
        },
        err => {
          dispatch(getAntConsSuccess({}, true));
          console.error('Cannot load antecedents & consequents', err);
        }
      );
      dispatchIfAction(dispatch, onLoadEnd);
    } catch (e) {
      return dispatch(getAntConsSuccess({}, true));
    }
    return response;
  };
}
export function createRule(rule, date, organization, onLoadStart = null, onLoadEnd = null) {
  return async dispatch => {
    dispatchIfAction(dispatch, onLoadStart);

    const request = axios({
      method: 'post',
      baseURL: baseUrl,
      url: `/organization/${organization}/rules/`,
      responseType: 'json',
      headers: {
        common: {
          'Content-Type': 'application/json'
        }
      },
      data: {
        created: date,
        value: rule
      }
    });
    let response = null;

    try {
      response = await request.then(
        response => {
          return dispatchIfAction(dispatch, createRulesuccess(response.data));
        },
        err => {
          throw err;
        }
      );
      dispatchIfAction(dispatch, onLoadEnd);
    } catch (e) {
      throw e;
    }
    return response;
  };
}
export function resetError() {
  return {
    type: RESET_ERROR
  };
}
function createRulesuccess(rule) {
  return {
    type: CREATE_RULE_SUCCESS,
    rule
  };
}
function getAntConsSuccess(antCons, error = false) {
  return {
    type: ANT_CONS_GET_SUCCESS,
    antCons,
    error
  };
}
export function restartOP() {
  return {
    type: RESTART_OP
  };
}
function getMyOPSuccess(lastProcedure, error = false) {
  return {
    type: MY_OP_GET_SUCCESS,
    lastProcedure,
    error
  };
}
