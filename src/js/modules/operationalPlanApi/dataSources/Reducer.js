import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  MY_OP_GET_SUCCESS,
  ANT_CONS_GET_SUCCESS,
  CREATE_RULE_SUCCESS,
  RESET_ERROR,
  RESTART_OP
} from './Actions';

const mutators = {
  [MY_OP_GET_SUCCESS]: {
    lastProcedure: (action, state) => {
      return action.lastProcedure;
    },
    errorGet: (action, state) => {
      return action.error;
    }
  },
  [ANT_CONS_GET_SUCCESS]: {
    antCons: (action, state) => {
      return action.antCons;
    },
    errorGet: (action, state) => {
      return action.error;
    }
  },
  [CREATE_RULE_SUCCESS]: {
    lastProcedure: (action, state) => {
      return action.rule;
    }
  },
  [RESET_ERROR]: {
    errorGet: false
  },
  [RESTART_OP]: {
    lastProcedure: undefined,
    antCons: undefined,
    errorGet: false
  }
};

export default reduceWith(mutators, DefaultState);
