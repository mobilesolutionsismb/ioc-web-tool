const NS = 'OP_PLAN_API';
export const MY_OP_GET_SUCCESS = `${NS}@MY_OP_GET_SUCCESS`;
export const ANT_CONS_GET_SUCCESS = `${NS}@ANT_CONS_GET_SUCCESS`;
export const CREATE_RULE_SUCCESS = `${NS}@CREATE_RULE_SUCCESS`;
export const RESET_ERROR = `${NS}@RESET_ERROR`;
export const RESTART_OP = `${NS}@RESTART_OP`;
