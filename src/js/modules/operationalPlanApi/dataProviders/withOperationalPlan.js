import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  antCons: state => state.operationalPlanApi.antCons,
  lastProcedure: state => state.operationalPlanApi.lastProcedure,
  errorGet: state => state.operationalPlanApi.errorGet
});

export default connect(select, ActionCreators);
