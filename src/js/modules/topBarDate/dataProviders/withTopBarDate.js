import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const startDateSelector = state => state.topBarDate.startDate;
const endDateSelector = state => state.topBarDate.endDate;

const select = createStructuredSelector({
  startDate: startDateSelector,
  endDate: endDateSelector
});

export default connect(select, ActionCreators);
