import { SELECT_DATE } from './Actions';

export function selectDate(start, end) {
  return {
    type: SELECT_DATE,
    start: new Date(start),
    end: new Date(end)
  };
}
