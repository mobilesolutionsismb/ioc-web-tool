import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import { SELECT_DATE } from './Actions';

const mutators = {
  [SELECT_DATE]: {
    startDate: action => action.start,
    endDate: action => action.end
  }
};

export default reduceWith(mutators, DefaultState);
