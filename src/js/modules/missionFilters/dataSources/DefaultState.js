import { FILTERS } from './filters';

const STATE = {
  // Filter checkboxes are initially unchecked
  filters: { ...FILTERS, missionStatus: [] },
  sorter: '_date_end_desc'
};

export default STATE;
