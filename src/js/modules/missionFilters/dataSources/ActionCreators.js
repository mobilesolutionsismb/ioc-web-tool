import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';
const DESELECT_MISSION = 'api::ireact_features@DESELECT_FEATURE';

export function resetAllMissionFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addMissionFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

export function addMissionFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no Mission is currently selected
    dispatch({ type: DESELECT_MISSION });
    dispatch(_addMissionFilter(filterCategory, filterName));
  };
}

function _removeMissionFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeMissionFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no Mission is currently selected
    dispatch({ type: DESELECT_MISSION });
    dispatch(_removeMissionFilter(filterCategory, filterName));
  };
}

export function setMissionSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
