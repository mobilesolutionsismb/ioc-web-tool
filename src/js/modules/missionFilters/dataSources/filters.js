/* MISSIONS STATUS FILTERS */
const _is_status_ongoing = ['==', 'temporalStatus', 'ongoing'];
const _is_status_open = ['==', 'temporalStatus', 'open'];
const _is_status_expired = ['==', 'temporalStatus', 'expired'];
const _is_status_completed = ['==', 'temporalStatus', 'completed'];
export const missionStatus = {
  _is_status_ongoing,
  _is_status_open,
  _is_status_expired,
  _is_status_completed
};

export const FILTERS = {
  missionStatus: Object.keys(missionStatus)
};

export const SORTERS = {
  _date_end_asc: (f1, f2) => {
    if (f1.properties.end !== f2.properties.end)
      return new Date(f1.properties.end).getTime() - new Date(f2.properties.end).getTime();
    else return f1.properties.id - f2.properties.id;
  },
  _date_end_desc: (f1, f2) => {
    if (f2.properties.end !== f1.properties.end)
      return new Date(f2.properties.end).getTime() - new Date(f1.properties.end).getTime();
    else return f2.properties.id - f1.properties.id;
  }
};

export const availableMissionFilters = Object.keys(FILTERS);
export const availableMissionSorters = Object.keys(SORTERS);
