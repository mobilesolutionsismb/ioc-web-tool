import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, missionStatus } from '../dataSources/filters';
import * as ActionCreators from '../dataSources/ActionCreators';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  missionStatus
};

const select = createStructuredSelector({
  missionFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.missionFilters.filters, FilterMap, FILTERS),
  missionFilters: state => state.missionFilters.filters,
  missionSorter: state => state.missionFilters.sorter
});

export default connect(select, ActionCreators);
