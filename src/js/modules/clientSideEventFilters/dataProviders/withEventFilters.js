import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, status, hazard } from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';

const FilterMap = {
  status,
  hazard
};

/**
 * Get filters definitions from set
 * Generate an array of filters (with logical OR) according to Mapbox style filters
 * @see https://www.mapbox.com/mapbox-gl-js/style-spec/#types-filter
 * @param {Array} stateFilters
 * @return filters or null
 */
function _getActiveFiltersDefinitions(stateFilters) {
  const definitions = ['all'];
  const filterCategories = Object.keys(FILTERS);
  for (const filterCategory of filterCategories) {
    if (stateFilters[filterCategory].length > 0) {
      const activeCategoryDefinitions = stateFilters[filterCategory].map(
        filterName => FilterMap[filterCategory][filterName]
      );
      if (activeCategoryDefinitions.length) {
        definitions.push(['any', ...activeCategoryDefinitions]);
      }
    }
  }
  return definitions.length === 1 ? null : definitions;
}

const select = createStructuredSelector({
  eventFiltersDefinition: state => _getActiveFiltersDefinitions(state.eventFilters.filters),
  feEventFilters: state => state.eventFilters.filters
});

export default connect(select, ActionCreators);
