import Reducer from './dataSources/Reducer';
import withEventFilters from './dataProviders/withEventFilters';
import * as eventFiltersActions from './dataSources/ActionCreators';
export {
  availableFilters,
  FILTERS,
  hazard as HAZARD_FILTERS,
  status as STATUS_FILTERS
} from './dataSources/filters';

export default Reducer;
export { Reducer, withEventFilters, eventFiltersActions };
