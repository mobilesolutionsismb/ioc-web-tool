import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import { RESET_ALL, ADD_FILTER, REMOVE_FILTER } from './Actions';
import { FILTERS } from './filters';

function isValidFilter(filterCategory, filterName) {
  return FILTERS.hasOwnProperty(filterCategory) && FILTERS[filterCategory].indexOf(filterName) > -1;
}

const mutators = {
  [RESET_ALL]: {
    ...DefaultState
  },
  [ADD_FILTER]: {
    filters: (action, state) => {
      const { filterCategory, filterName } = action;
      if (isValidFilter(filterCategory, filterName)) {
        // Convert array to set
        const currentFilterSet = new Set(state.filters[filterCategory]);
        currentFilterSet.add(filterName);
        // Convert set to array
        return { ...state.filters, [filterCategory]: Array.from(currentFilterSet) };
      } else {
        return state.filters;
      }
    }
  },
  [REMOVE_FILTER]: {
    filters: (action, state) => {
      const { filterCategory, filterName } = action;
      if (isValidFilter(filterCategory, filterName)) {
        // Convert array to set
        const currentFilterSet = new Set(state.filters[filterCategory]);
        if (currentFilterSet.delete(filterName)) {
          // Convert set to array
          return { ...state.filters, [filterCategory]: Array.from(currentFilterSet) };
        } else {
          return state.filters;
        }
      } else {
        return state.filters;
      }
    }
  }
};

export default reduceWith(mutators, DefaultState);
