import { RESET_ALL, ADD_FILTER, REMOVE_FILTER } from './Actions';

const DESELECT_EVENT = 'api@DESELECT_EVENT';
// const DEACTIVATE_EVENT = 'api@DEACTIVATE_EVENT';

export function resetAllEventFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addEventFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

export function addEventFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no report is currently selected
    dispatch({ type: DESELECT_EVENT });
    // dispatch({ type: DEACTIVATE_EVENT }); // NO GOOD IN THIS CASE! must call creator
    dispatch(_addEventFilter(filterCategory, filterName));
  };
}

function _removeFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeEventFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no report is currently selected
    dispatch({ type: DESELECT_EVENT });
    //dispatch({ type: DEACTIVATE_EVENT }); // NO GOOD IN THIS CASE! must call creator
    dispatch(_removeFilter(filterCategory, filterName));
  };
}
