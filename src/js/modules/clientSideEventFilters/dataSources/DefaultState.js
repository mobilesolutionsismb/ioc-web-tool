import { FILTERS } from './filters';

const STATE = {
  // By setting userType to empty we have to same result of selecting both userType filters
  // But filter checkboxes are initially unchecked
  filters: { ...FILTERS, status: [], hazard: [] }
};

export default STATE;
