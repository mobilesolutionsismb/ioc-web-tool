const NS = 'EVENT_FILTERS';

export const RESET_ALL = `${NS}@RESET_ALL`;
export const ADD_FILTER = `${NS}@ADD_FILTER`;
export const REMOVE_FILTER = `${NS}@REMOVE_FILTER`;
