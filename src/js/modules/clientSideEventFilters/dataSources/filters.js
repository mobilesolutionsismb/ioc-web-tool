/* EVENT STATUS FILTERS */
const _is_status_closed = ['==', 'isOngoing', false];
const _is_status_ongoing = ['==', 'isOngoing', true];
export const status = {
  _is_status_closed,
  _is_status_ongoing
};

/* REPORT HAZARD FILTERS */
const _hazard_fire = ['==', 'hazard', 'fire'];
const _hazard_flood = ['==', 'hazard', 'flood'];
const _hazard_landslide = ['==', 'hazard', 'landslide'];
const _hazard_extremeWeather = ['==', 'hazard', 'extremeWeather'];
const _hazard_avalanches = ['==', 'hazard', 'avalanches'];
const _hazard_earthquake = ['==', 'hazard', 'earthquake'];
const _hazard_unknown = ['==', 'hazard', 'unknown'];
const _hazard_none = ['==', 'hazard', 'none'];
export const hazard = {
  _hazard_fire,
  _hazard_flood,
  _hazard_landslide,
  _hazard_extremeWeather,
  _hazard_avalanches,
  _hazard_earthquake,
  _hazard_unknown,
  _hazard_none
};

export const FILTERS = {
  status: Object.keys(status),
  hazard: Object.keys(hazard)
};

export const availableFilters = Object.keys(FILTERS);
