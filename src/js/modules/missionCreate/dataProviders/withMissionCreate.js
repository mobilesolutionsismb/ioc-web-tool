import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const newMission = state => state.missionCreate.newMission;

const select = createStructuredSelector({
  newMission
});

export default connect(select, ActionCreators);
