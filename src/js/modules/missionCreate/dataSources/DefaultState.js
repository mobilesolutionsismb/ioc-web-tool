const STATE = {
  newMission: {
    areaOfInterest: '',
    coordinates: '',
    firstResponderId: null,
    tasks: [],
    id: 0,
    title: '',
    teamId: null,
    address: '',
    start: null,
    end: null
  }
};

export default STATE;
