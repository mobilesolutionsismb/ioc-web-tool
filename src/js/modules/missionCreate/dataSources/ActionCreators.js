import { RESET_ALL, SET_TITLE, SET_TEAM_ID, ADD_TASK, REMOVE_TASK } from './Actions';

function resetMission() {
  return {
    type: RESET_ALL
  };
}

function setMissionTitle(title) {
  return {
    type: SET_TITLE,
    title
  };
}

function setMissionTeamId(teamId) {
  return {
    type: SET_TEAM_ID,
    teamId
  };
}

function AddTaskToMission(task) {
  return {
    type: ADD_TASK,
    task
  };
}

function removeTaskFromMission(taskDescription) {
  return {
    type: REMOVE_TASK,
    taskDescription
  };
}

export { resetMission, setMissionTitle, setMissionTeamId, AddTaskToMission, removeTaskFromMission };
