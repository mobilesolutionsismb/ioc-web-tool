import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';

import { RESET_ALL, SET_TITLE, SET_TEAM_ID, ADD_TASK, REMOVE_TASK } from './Actions';

const mutators = {
  [SET_TITLE]: {
    newMission: (action, state) => {
      return { ...state.newMission, title: action.title };
    }
  },
  [SET_TEAM_ID]: {
    newMission: (action, state) => {
      return { ...state.newMission, teamId: action.teamId };
    }
  },
  [REMOVE_TASK]: {
    newMission: (action, state) => {
      let newTaskArray = state.newMission.tasks;
      let indexOfTaskToDelete = newTaskArray.findIndex(
        task => task.description === action.taskDescription
      );
      if (indexOfTaskToDelete >= 0) newTaskArray.splice(indexOfTaskToDelete, 1);

      return { ...state.newMission, tasks: newTaskArray };
    }
  },
  [ADD_TASK]: {
    newMission: (action, state) => {
      let newTaskArray = state.newMission.tasks;
      newTaskArray.push(action.task);
      return { ...state.newMission, tasks: newTaskArray };
    }
  },
  // [FILL_EM_COMM]: {
  //     newEmComm: (action, state) => {
  //         const emComm = action.emComm;
  //         let newEmComm = {
  //             startDate: new Date(emComm.start),
  //             startTime: new Date(emComm.start),
  //             endDate: emComm.end ? new Date(emComm.end) : undefined,
  //             endTime: emComm.end ? new Date(emComm.end) : undefined,
  //             areaOfInterest: emComm.areaOfInterest,
  //             targetAreaOfInterest: emComm.targetAreaOfInterest,
  //             location: emComm.location,
  //             targetLocation: emComm.targetLocation,
  //             id: emComm.id,
  //             address: emComm.address,
  //             targetAddress: emComm.targetAddress,
  //             type: emComm.type,
  //             level: emComm.type === 'warning' ? { value: emComm.level, label: '_emcomm_' + emComm.level } : { value: '', label: '' },
  //             hazard: { value: emComm.hazard, label: '_hazard_' + emComm.hazard },
  //             description: emComm.description,
  //             receivers: emComm.receivers
  //         };
  //         return newEmComm;
  //     }
  //},
  [RESET_ALL]: {
    newMission: (action, state) => {
      return { ...DefaultState.newMission, tasks: [] };
    }
  }
};

export default reduceWith(mutators, DefaultState);
