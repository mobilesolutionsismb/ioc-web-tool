const NS = 'MISSION_NEW';

export const RESET_ALL = `${NS}@RESET_ALL`;
export const SET_TITLE = `${NS}@SET_TITLE`;
export const SET_TEAM_ID = `${NS}@SET_TEAM_ID`;
export const ADD_TASK = `${NS}@ADD_TASK`;
export const REMOVE_TASK = `${NS}@REMOVE_TASK`;
