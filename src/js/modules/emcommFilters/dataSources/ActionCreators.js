import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';

const DESELECT_ECOMM = 'api@DESELECT_ECOMM';

export function resetAllReportFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addEmcommFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

export function addEmcommFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no report is currently selected
    dispatch({ type: DESELECT_ECOMM });
    dispatch(_addEmcommFilter(filterCategory, filterName));
  };
}

function _removeFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeEmcommFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no report is currently selected
    dispatch({ type: DESELECT_ECOMM });
    dispatch(_removeFilter(filterCategory, filterName));
  };
}

export function setEmcommSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
