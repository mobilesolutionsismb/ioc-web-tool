/* EMCOM TYPE FILTERS */
const _is_alert = ['==', 'type', 'alert'];
const _is_warning = ['==', 'type', 'warning'];
const _is_my_organization_unit = ['==', 'isMyOrganizationUnit', true];
export const emcommType = {
  _is_alert,
  _is_warning,
  _is_my_organization_unit
};

/* EMCOM HAZARD FILTERS */
const _hazard_fire = ['==', 'hazard', 'fire'];
const _hazard_flood = ['==', 'hazard', 'flood'];
const _hazard_landslide = ['==', 'hazard', 'landslide'];
const _hazard_extremeWeather = ['==', 'hazard', 'extremeWeather'];
const _hazard_avalanches = ['==', 'hazard', 'avalanches'];
const _hazard_earthquake = ['==', 'hazard', 'earthquake'];
const _hazard_unknown = ['==', 'hazard', 'unknown'];
const _hazard_none = ['==', 'hazard', 'none'];
export const hazard = {
  _hazard_fire,
  _hazard_flood,
  _hazard_landslide,
  _hazard_extremeWeather,
  _hazard_avalanches,
  _hazard_earthquake,
  _hazard_unknown,
  _hazard_none
};

/* EMCOM STATUS FILTERS */
const _is_status_ongoing = ['==', 'isOngoing', true];
const _is_status_closed = ['==', 'isOngoing', false];
export const emcommStatus = {
  _is_status_ongoing,
  _is_status_closed
};

export const FILTERS = {
  hazard: Object.keys(hazard),
  emcommType: Object.keys(emcommType),
  emcommStatus: Object.keys(emcommStatus)
};

export const SORTERS = {
  _date_time_asc: (f1, f2) => {
    if (f1.properties.start !== f2.properties.start)
      return new Date(f1.properties.start).getTime() - new Date(f2.properties.start).getTime();
    else return f1.properties.id - f2.properties.id;
  },
  _date_time_desc: (f1, f2) => {
    if (f2.properties.start !== f1.properties.start)
      return new Date(f2.properties.start).getTime() - new Date(f1.properties.start).getTime();
    else return f2.properties.id - f1.properties.id;
  }
};

export const availableEmcommFilters = Object.keys(FILTERS);
export const availableEmcommSorters = Object.keys(SORTERS);
