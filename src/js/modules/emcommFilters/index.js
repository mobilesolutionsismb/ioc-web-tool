import Reducer from './dataSources/Reducer';
import withEmergencyCommunicationFilters from './dataProviders/withEmergencyCommunicationFilters';
import * as emcommFiltersActions from './dataSources/ActionCreators';
export {
  availableEmcommSorters,
  availableEmcommFilters,
  FILTERS,
  SORTERS,
  emcommType as EMCOMM_TYPE_FILTERS,
  emcommStatus as EMCOMM_STATUS,
  hazard as HAZARD_FILTERS
} from './dataSources/filters';

export default Reducer;
export { Reducer, withEmergencyCommunicationFilters, emcommFiltersActions };
