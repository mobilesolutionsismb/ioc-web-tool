import { FILTERS } from './filters';

const STATE = {
  filters: { ...FILTERS }
};

export default STATE;
