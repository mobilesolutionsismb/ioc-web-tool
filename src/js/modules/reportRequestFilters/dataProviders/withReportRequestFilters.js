import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  FILTERS,
  reportRequestType,
  reportRequestStatus,
  reportRequestCategory
} from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  reportRequestType,
  reportRequestStatus,
  reportRequestCategory
};

const select = createStructuredSelector({
  reportRequestFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.reportRequestFilters.filters, FilterMap, FILTERS),
  reportRequestFilters: state => state.reportRequestFilters.filters,
  reportRequestSorter: state => state.reportRequestFilters.sorter
});

export default connect(select, ActionCreators);
