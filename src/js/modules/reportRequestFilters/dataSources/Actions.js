const NS = 'REPORT_REQUEST_FILTERS';

export const RESET_ALL = `${NS}@RESET_ALL`;
export const SORTER_CHANGE = `${NS}@SORTER_CHANGE`;
export const ADD_FILTER = `${NS}@ADD_FILTER`;
export const REMOVE_FILTER = `${NS}@REMOVE_FILTER`;
