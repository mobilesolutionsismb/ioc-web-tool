import { FILTERS } from './filters';

const STATE = {
  // By setting reportRequestType to empty we have the same result of selecting both userType filters
  // But filter checkboxes are initially unchecked
  filters: {
    ...FILTERS,
    reportRequestType: [],
    reportRequestStatus: [],
    reportRequestCategory: []
  },
  sorter: '_date_time_desc'
};

export default STATE;
