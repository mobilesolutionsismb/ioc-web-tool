import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';
const DESELECT_REPORT_REQUEST = 'api@DESELECT_REPORT_REQUEST';

export function resetAllReportRequestFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addReportRequestFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

export function addReportRequestFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no report request is currently selected
    dispatch({ type: DESELECT_REPORT_REQUEST });
    dispatch(_addReportRequestFilter(filterCategory, filterName));
  };
}

function _removeReportRequestFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeReportRequestFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no report request is currently selected
    dispatch({ type: DESELECT_REPORT_REQUEST });
    dispatch(_removeReportRequestFilter(filterCategory, filterName));
  };
}

export function setReportRequestSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
