/* REPORT REQUEST TYPE FILTERS */
const _is_type_measure = ['==', 'hasMeasure', true];
const _is_type_damage = ['==', 'hasDamage', true];
const _is_type_resource = ['==', 'hasResource', true];
const _is_type_people = ['==', 'hasPeople', true];
export const reportRequestType = {
  _is_type_measure,
  _is_type_damage,
  _is_type_resource,
  _is_type_people
};

/* REPORT REQUEST STATUS FILTERS */
const _is_status_ongoing = ['==', 'isOngoing', true];
const _is_status_closed = ['==', 'isOngoing', false];
export const reportRequestStatus = {
  _is_status_ongoing,
  _is_status_closed
};

/* REPORT REQUEST CATEGORY FILTERS */
const _cat_fire = ['==', 'hasFire', true];
const _cat_flood = ['==', 'hasFlood', false];
const _cat_weather = ['==', 'hasWeather', true];
const _cat_earthquake = ['==', 'hasEarthquake', true];
export const reportRequestCategory = {
  _cat_fire,
  _cat_flood,
  _cat_weather,
  _cat_earthquake
};

export const FILTERS = {
  reportRequestType: Object.keys(reportRequestType),
  reportRequestStatus: Object.keys(reportRequestStatus),
  reportRequestCategory: Object.keys(reportRequestCategory)
};

export const SORTERS = {
  _date_time_asc: (f1, f2) => {
    if (f1.properties.start !== f2.properties.start)
      return new Date(f1.properties.start).getTime() - new Date(f2.properties.start).getTime();
    else return f1.properties.id - f2.properties.id;
  },
  _date_time_desc: (f1, f2) => {
    if (f2.properties.start !== f1.properties.start)
      return new Date(f2.properties.start).getTime() - new Date(f1.properties.start).getTime();
    else return f2.properties.id - f1.properties.id;
  }
};

export const availableReportRequestFilters = Object.keys(FILTERS);
export const availableReportRequestSorters = Object.keys(SORTERS);
