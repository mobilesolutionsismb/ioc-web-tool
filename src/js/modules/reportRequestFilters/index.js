import Reducer from './dataSources/Reducer';
import withReportRequestFilters from './dataProviders/withReportRequestFilters';
import * as reportRequestFiltersActions from './dataSources/ActionCreators';
export {
  availableReportRequestSorters,
  availableReportRequestFilters,
  FILTERS,
  SORTERS,
  reportRequestType as REPORT_REQUEST_TYPE_FILTERS,
  reportRequestStatus as REPORT_REQUEST_STATUS_FILTERS,
  reportRequestCategory as REPORT_REQUEST_CATEGORY_FILTERS
} from './dataSources/filters';

export default Reducer;
export { Reducer, withReportRequestFilters, reportRequestFiltersActions };
