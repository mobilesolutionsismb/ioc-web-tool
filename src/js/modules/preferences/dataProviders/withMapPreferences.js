import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { MAP_STYLES } from '../mapStyles';

import {
  setMapStyleName,
  changeMapStyle,
  restoreDefaultMapPreferences
} from '../dataSources/ActionCreators';

const mapSettingsSelector = state => ({
  mapStyleName: state.preferences.mapStyleName,
  mapStyle: MAP_STYLES[state.preferences.mapStyleName]
});

const select = createStructuredSelector({
  preferences: mapSettingsSelector
});

export default connect(select, { setMapStyleName, changeMapStyle, restoreDefaultMapPreferences });
