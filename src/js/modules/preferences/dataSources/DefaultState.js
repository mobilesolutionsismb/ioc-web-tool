import { AVAILABLE_STYLES } from '../mapStyles';

const state = {
  mapStyleName: AVAILABLE_STYLES[0]
};

export default state;
