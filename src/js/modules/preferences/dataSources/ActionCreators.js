import { RESTORE_DEFAULT, SET_DEFAULT_MAP_STYLE, SET_MAP_STYLE } from './Actions';

import { MAP_STYLES_NAMES } from '../mapStyles';

// TODO into some general provider for preferences
export function restoreDefaultPreferences() {
  return {
    type: RESTORE_DEFAULT
  };
}

export function restoreDefaultMapPreferences() {
  return {
    type: SET_DEFAULT_MAP_STYLE
  };
}

export function setMapStyleName(mapStyleName) {
  return {
    type: SET_MAP_STYLE,
    mapStyleName
  };
}

export function changeMapStyle(mapStyleName) {
  return (dispatch, getState) => {
    const currentStyleName = getState().preferences.mapStyleName;
    // if it's not the same AND style is supported
    if (currentStyleName !== mapStyleName && MAP_STYLES_NAMES.indexOf(mapStyleName) > -1) {
      dispatch(setMapStyleName(mapStyleName));
    }
  };
}
