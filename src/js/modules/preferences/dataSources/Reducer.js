import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';

import { SET_MAP_STYLE, SET_DEFAULT_MAP_STYLE, RESTORE_DEFAULT } from './Actions';

const mutators = {
  [RESTORE_DEFAULT]: { ...DefaultState },
  [SET_DEFAULT_MAP_STYLE]: {
    mapStyleName: DefaultState.mapStyleName
  },
  [SET_MAP_STYLE]: {
    mapStyleName: action => action.mapStyleName
  }
};

export default reduceWith(mutators, DefaultState);
