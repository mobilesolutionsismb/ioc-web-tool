import settingsReducer from './dataSources/Reducer';
export { default as withMapPreferences } from './dataProviders/withMapPreferences';
export { MAP_STYLES, MAP_STYLES_NAMES } from './mapStyles';
export { settingsReducer as Reducer };

export default settingsReducer;
