import Reducer from './dataSources/Reducer';
import withTaskCreate from './dataProviders/withTaskCreate';

export { withTaskCreate, Reducer };
export {
  setTaskStartDate,
  setTaskStartTime,
  setTaskEndDate,
  setTaskEndTime,
  resetTask,
  setTaskDescription,
  setTaskAgentsNeeded,
  toggleTaskPopup,
  resetTaskDescription,
  resetTaskStart,
  resetTaskEnd,
  setTaskTools
} from './dataSources/ActionCreators';

export default Reducer;
