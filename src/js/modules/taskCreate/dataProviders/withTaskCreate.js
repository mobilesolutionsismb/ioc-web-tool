import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const newTask = state => state.taskCreate.newTask;
const isTaskPopupOpen = state => state.taskCreate.isTaskPopupOpen;

const select = createStructuredSelector({
  newTask,
  isTaskPopupOpen
});

export default connect(select, ActionCreators);
