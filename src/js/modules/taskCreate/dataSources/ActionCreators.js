import {
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_ALL,
  SET_DESCRIPTION,
  SET_AGENTS_NEEDED,
  TOGGLE_TASK_POPUP,
  RESET_DESCRIPTION,
  RESET_END,
  RESET_START,
  SET_TASK_TOOLS
} from './Actions';

export function setTaskStartDate(startDate) {
  return {
    type: SET_START_DATE,
    startDate
  };
}

export function setTaskStartTime(startTime) {
  return {
    type: SET_START_TIME,
    startTime
  };
}

export function setTaskEndDate(endDate) {
  return {
    type: SET_END_DATE,
    endDate
  };
}

export function setTaskEndTime(endTime) {
  return {
    type: SET_END_TIME,
    endTime
  };
}

export function resetTask() {
  return {
    type: RESET_ALL
  };
}

export function setTaskDescription(description) {
  return {
    type: SET_DESCRIPTION,
    description
  };
}

export function setTaskTools(taskTools) {
  return {
    type: SET_TASK_TOOLS,
    taskTools
  };
}

export function setTaskAgentsNeeded(numberOfNecessaryPeople) {
  return {
    type: SET_AGENTS_NEEDED,
    numberOfNecessaryPeople
  };
}

export function toggleTaskPopup(isTaskPopupOpen) {
  return {
    type: TOGGLE_TASK_POPUP,
    isTaskPopupOpen
  };
}

export function resetTaskDescription() {
  return {
    type: RESET_DESCRIPTION
  };
}

export function resetTaskStart() {
  return {
    type: RESET_START
  };
}

export function resetTaskEnd() {
  return {
    type: RESET_END
  };
}
