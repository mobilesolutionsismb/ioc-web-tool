const STATE = {
  newTask: {
    description: '',
    location: '',
    address: '',
    taskTools: '',
    start: null,
    end: null,
    startDate: null,
    startTime: null,
    endDate: null,
    endTime: null,
    numberOfNecessaryPeople: 0
  },
  isTaskPopupOpen: false
};

export default STATE;
