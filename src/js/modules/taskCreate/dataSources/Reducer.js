import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_ALL,
  SET_DESCRIPTION,
  SET_AGENTS_NEEDED,
  TOGGLE_TASK_POPUP,
  RESET_DESCRIPTION,
  RESET_END,
  RESET_START,
  SET_TASK_TOOLS
} from './Actions';

const mutators = {
  [SET_START_DATE]: {
    newTask: (action, state) => {
      return { ...state.newTask, startDate: action.startDate };
    }
  },
  [SET_START_TIME]: {
    newTask: (action, state) => {
      return { ...state.newTask, startTime: action.startTime };
    }
  },
  [SET_END_DATE]: {
    newTask: (action, state) => {
      return { ...state.newTask, endDate: action.endDate };
    }
  },
  [SET_END_TIME]: {
    newTask: (action, state) => {
      return { ...state.newTask, endTime: action.endTime };
    }
  },
  [TOGGLE_TASK_POPUP]: {
    isTaskPopupOpen: action => action.isTaskPopupOpen
  },
  // [FILL_REPORT_REQUEST]: {
  //     newReportRequest: (action, state) => {
  //         const repReq = action.reportRequest;
  //         let newReportRequest = {
  //             startDate: new Date(repReq.start),
  //             startTime: new Date(repReq.start),
  //             endDate: new Date(repReq.end),
  //             endTime: new Date(repReq.end),
  //             areaOfInterest: repReq.areaOfInterest,
  //             location: repReq.location,
  //             reportersAllowed: repReq.roles ? repReq.roles.map((item) => parseInt(item.id, 10)) : [],
  //             teams: [],
  //             minimumLevel: '',
  //             contentType: repReq.contentType ? repReq.contentType.split(',').map((item) => item.trim()).sort((type) => type === 'measure') : [],
  //             categoryType: repReq.measures && repReq.measures.length > 0 ? { value: repReq.measures[0].category, label: repReq.measures[0].category.toUpperCase() } : { value: '', label: '' },
  //             roles: repReq.roles,
  //             measures: repReq.measures.map(function (item) { item.value = item.id; item.label = item.title; return item; }),
  //             id: repReq.id,
  //             address: repReq.address
  //         };
  //         return newReportRequest;
  //     }
  // },
  [SET_DESCRIPTION]: {
    newTask: (action, state) => {
      return { ...state.newTask, description: action.description };
    }
  },
  [SET_TASK_TOOLS]: {
    newTask: (action, state) => {
      return { ...state.newTask, taskTools: action.taskTools };
    }
  },
  [SET_AGENTS_NEEDED]: {
    newTask: (action, state) => {
      return { ...state.newTask, numberOfNecessaryPeople: action.numberOfNecessaryPeople };
    }
  },
  [RESET_DESCRIPTION]: {
    newTask: (action, state) => {
      return { ...state.newTask, description: '' };
    }
  },
  [RESET_START]: {
    newTask: (action, state) => {
      return { ...state.newTask, start: null, startDate: null, startTime: null };
    }
  },
  [RESET_END]: {
    newTask: (action, state) => {
      return { ...state.newTask, end: null, endDate: null, endTime: null };
    }
  },
  [RESET_ALL]: {
    newTask: (action, state) => {
      return { ...DefaultState.newTask };
    }
  }
};

export default reduceWith(mutators, DefaultState);
