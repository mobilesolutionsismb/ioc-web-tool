const NS = 'TASK_NEW';

export const SET_DESCRIPTION = `${NS}@SET_DESCRIPTION`;
export const SET_TASK_TOOLS = `${NS}@SET_TASK_TOOLS`;
export const SET_START_DATE = `${NS}@SET_START_DATE`;
export const SET_START_TIME = `${NS}@SET_START_TIME`;
export const SET_END_DATE = `${NS}@SET_END_DATE`;
export const SET_END_TIME = `${NS}@SET_END_TIME`;
export const SET_AGENTS_NEEDED = `${NS}@SET_AGENTS_NEEDED`;
export const TOGGLE_TASK_POPUP = `${NS}@TOGGLE_TASK_POPUP`;

export const RESET_ALL = `${NS}@RESET_ALL`;
export const RESET_DESCRIPTION = `${NS}@RESET_DESCRIPTION`;
export const RESET_START = `${NS}@RESET_START`;
export const RESET_END = `${NS}@RESET_END`;
