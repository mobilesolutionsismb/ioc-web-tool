import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import { OPEN_PRIMARY, CLOSE_PRIMARY, OPEN_SECONDARY, CLOSE_SECONDARY } from './Actions';

const mutators = {
  [OPEN_PRIMARY]: {
    primaryOpen: true,
    primaryDrawerType: (action, state) => {
      return action.primaryDrawerType;
    }
  },
  [CLOSE_PRIMARY]: DefaultState,
  [OPEN_SECONDARY]: {
    primaryOpen: true,
    secondaryOpen: true,
    secondaryDrawerType: (action, state) => {
      return action.secondaryDrawerType;
    }
  },
  [CLOSE_SECONDARY]: {
    secondaryOpen: false,
    secondaryDrawerType: null
  }
};

export default reduceWith(mutators, DefaultState);
