import { OPEN_PRIMARY, CLOSE_PRIMARY, OPEN_SECONDARY, CLOSE_SECONDARY } from './Actions';

function setOpenPrimary(value, primaryDrawerType) {
  return {
    type: value === true ? OPEN_PRIMARY : CLOSE_PRIMARY,
    primaryDrawerType
  };
}

function setOpenSecondary(value, secondaryDrawerType) {
  return {
    type: value === true ? OPEN_SECONDARY : CLOSE_SECONDARY,
    secondaryDrawerType
  };
}

export { setOpenPrimary, setOpenSecondary };
