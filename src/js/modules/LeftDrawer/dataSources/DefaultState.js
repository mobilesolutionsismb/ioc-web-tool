const STATE = {
  primaryOpen: false,
  primaryDrawerType: null,
  secondaryDrawerType: null,
  secondaryOpen: false
};

export default STATE;
