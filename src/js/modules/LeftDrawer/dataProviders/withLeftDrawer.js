import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const openPrimarySelector = state => state.leftDrawer.primaryOpen;
const typePrimarySelector = state => state.leftDrawer.primaryDrawerType;
const openSecondarySelector = state => state.leftDrawer.secondaryOpen;
const typeSecondarySelector = state => state.leftDrawer.secondaryDrawerType;

const select = createStructuredSelector({
  isPrimaryOpen: openPrimarySelector,
  isSecondaryOpen: openSecondarySelector,
  primaryDrawerType: typePrimarySelector,
  secondaryDrawerType: typeSecondarySelector
});

export default connect(select, ActionCreators);
