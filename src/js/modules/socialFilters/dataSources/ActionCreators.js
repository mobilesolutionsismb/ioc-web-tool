import {
  RESET_ALL,
  ADD_FILTER,
  REMOVE_FILTER,
  CHANGE_SOCIAL_GEOLOCATED,
  ACTIVATE_EVENT_FILTERING,
  DEACTIVATE_EVENT_FILTERING,
  COLLAPSE_FILTERS,
  SET_BOUNDING_BOX,
  MAP_MOVED
} from './Actions';

export function resetAllSocialFilters() {
  return {
    type: RESET_ALL
  };
}

export function addSocialFilter(filterCategory, filterValue) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterValue
  };
}

export function removeSocialFilter(filterCategory) {
  return {
    type: REMOVE_FILTER,
    filterCategory
  };
}

export function changesocialGeolocated(newState) {
  return {
    type: CHANGE_SOCIAL_GEOLOCATED,
    newState
  };
}

export function activateEventFiltering(emergency) {
  return {
    type: ACTIVATE_EVENT_FILTERING,
    emergency
  };
}

export function deactivateEventFiltering() {
  return {
    type: DEACTIVATE_EVENT_FILTERING
  };
}

export function collapseFilters(collapse) {
  return {
    type: COLLAPSE_FILTERS,
    collapse
  };
}

export function setBoundingBox(bbox) {
  return {
    type: SET_BOUNDING_BOX,
    bbox
  };
}

export function moveMap() {
  return {
    type: MAP_MOVED
  };
}
