import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';

import {
  RESET_ALL,
  ADD_FILTER,
  REMOVE_FILTER,
  CHANGE_SOCIAL_GEOLOCATED,
  ACTIVATE_EVENT_FILTERING,
  DEACTIVATE_EVENT_FILTERING,
  COLLAPSE_FILTERS,
  SET_BOUNDING_BOX,
  MAP_MOVED
} from './Actions';

const mutators = {
  [RESET_ALL]: {
    ...DefaultState
  },
  [ADD_FILTER]: {
    filters: (action, state) => {
      const { filterCategory, filterValue } = action;
      const currentFilterSet = filterValue;
      return {
        ...state.filters,
        [filterCategory]: currentFilterSet
      };
    }
  },
  [REMOVE_FILTER]: {
    filters: (action, state) => {
      return {
        ...state.filters,
        [action.filterCategory]: DefaultState.filters[action.filterCategory]
      };
    }
  },
  [CHANGE_SOCIAL_GEOLOCATED]: {
    socialGeolocated: action => action.newState
  },
  [ACTIVATE_EVENT_FILTERING]: {
    eventFilter: action => action.emergency
  },
  [DEACTIVATE_EVENT_FILTERING]: {
    eventFilter: null
  },
  [COLLAPSE_FILTERS]: {
    collapsed: action => action.collapse
  },
  [SET_BOUNDING_BOX]: {
    bboxToSearch: action => action.bbox,
    mapMoved: false
  },
  [MAP_MOVED]: {
    mapMoved: true
  }
};

export default reduceWith(mutators, DefaultState);
