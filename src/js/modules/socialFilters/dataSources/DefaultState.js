const state = {
  filters: {
    language: null,
    dateFrom: null,
    dateTo: {
      label: '24h',
      number: 24,
      unity: 'hours'
    },
    emergency: [],
    infotype: [],
    tags: [],
    filterKw: [],
    filterKwNot: [],
    retweet: false
  },
  socialGeolocated: true,
  eventFilter: null,
  collapsed: false,
  mapMoved: false,
  bboxToSearch: null
};

export default state;
