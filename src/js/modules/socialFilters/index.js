import Reducer from './dataSources/Reducer';

import withSocialFilters from './dataProviders/withSocialFilters';
import * as SocialFiltersActions from './dataSources/ActionCreators';

export default Reducer;
export { Reducer, withSocialFilters, SocialFiltersActions };
