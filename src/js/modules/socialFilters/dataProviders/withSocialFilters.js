import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  filters: state => state.SocialFilters.filters,
  socialGeolocated: state => state.SocialFilters.socialGeolocated,
  eventFilter: state => state.SocialFilters.eventFilter,
  collapsed: state => state.SocialFilters.collapsed,
  mapMoved: state => state.SocialFilters.mapMoved,
  bboxToSearch: state => state.SocialFilters.bboxToSearch
});

export default connect(
  select,
  ActionCreators
);
