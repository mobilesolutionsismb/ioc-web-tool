import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';
// import { findLayerSettingByTaskId } from '../settingsTreeUtils';
import { findLayerSettingBySettingName, findCopernicusByName } from '../settingsTreeUtils';

const select = createStructuredSelector({
  remoteSettings: state => state.layersAndSettings.remoteSettings,
  remoteLayers: state => state.layersAndSettings.remoteLayers,
  remoteSettingsUpdating: state => state.layersAndSettings.remoteSettingsUpdating,
  activeIReactSettingNames: state => state.layersAndSettings.activeIReactSettingNames,
  // activeIReactTasks: state => state.layersAndSettings.activeIReactTasks,
  activeIReactTaskSettings: state => {
    if (
      Array.isArray(state.layersAndSettings.remoteSettings.children) &&
      state.layersAndSettings.activeIReactSettingNames.length
    ) {
      return state.layersAndSettings.activeIReactSettingNames.map(settingName =>
        findLayerSettingBySettingName(state.layersAndSettings.remoteSettings, settingName)
      );
    } else return [];
  },
  activeCopernicusNames: state => state.layersAndSettings.activeCopernicusNames,
  activeCopernicusLayers: state => {
    if (
      Array.isArray(state.api_layers.mapLayers.copernicusLayers) &&
      state.layersAndSettings.activeCopernicusNames.length
    ) {
      return state.layersAndSettings.activeCopernicusNames.map(copernicusName =>
        findCopernicusByName(state.api_layers.mapLayers.copernicusLayers, copernicusName, [])
      );
    } else return [];
  },
  itemEditedList: state => state.layersAndSettings.itemEditedList,
  activeLayersFromMapRequest: state => state.layersAndSettings.activeLayersFromMapRequest
});

export default connect(select, ActionCreators);
