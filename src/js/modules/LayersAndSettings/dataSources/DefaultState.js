const state = {
  remoteSettings: {},
  remoteLayers: {},
  remoteSettingsUpdating: true,
  activeIReactSettingNames: [],
  itemEditedList: [],
  activeCopernicusNames: [],
  activeLayersFromMapRequest: []
};

export default state;
