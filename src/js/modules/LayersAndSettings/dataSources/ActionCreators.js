import nop from 'nop';
import { apiCallActionCreatorFactory } from 'ioc-api-interface';
import {
  UPDATE_SETTINGS_OK,
  UPDATE_SETTINGS_FAIL,
  SETTINGS_UPDATING,
  SET_ACTIVE_IREACT_SETTINGNAMES,
  ADD_EDITED_SETTING,
  // SET_ACTIVE_IREACT_TASKS
  // SET_ACTIVE_IREACT_TASKS,
  UPDATE_LAYERS_OK,
  SET_ACTIVE_IREACT_COPERNICUS,
  SET_ACTIVE_LAYERS_FROM_MAP_REQUEST
} from './Actions';
import {
  computeSettingsTree,
  computeLayersTree,
  findLayerSettingByTaskId
} from '../settingsTreeUtils';

function _settingsUpdated(remoteSettings) {
  return {
    type: UPDATE_SETTINGS_OK,
    remoteSettings
  };
}

function _settingsUpdating() {
  return {
    type: SETTINGS_UPDATING
  };
}

function _layersWidgetUpdated(remoteLayers) {
  return {
    type: UPDATE_LAYERS_OK,
    remoteLayers
  };
}

function _settingsUpdateFailure() {
  return {
    type: UPDATE_SETTINGS_FAIL
  };
}

function _setActiveSettingNames(activeIReactSettingNames) {
  return {
    type: SET_ACTIVE_IREACT_SETTINGNAMES,
    activeIReactSettingNames
  };
}

function _setActiveCopernicusLayer(activeCopernicusNames) {
  return {
    type: SET_ACTIVE_IREACT_COPERNICUS,
    activeCopernicusNames
  };
}

async function _loadSettingsInner(dispatch, getState, onLoadStart, onLoadEnd) {
  dispatch(_settingsUpdating());
  let result;
  try {
    const errString = '_layes_and_settings_failure';
    const results = await Promise.all([
      apiCallActionCreatorFactory(
        // call for settings group (tree)
        onLoadStart,
        onLoadEnd,
        'generalSettings',
        'getDefinitionGroupTree',
        errString,
        nop,
        true
      )(dispatch, getState),
      apiCallActionCreatorFactory(
        // Call for settings (leafs)
        onLoadStart,
        onLoadEnd,
        'generalSettings',
        'getSettingDefinitionsByGroup',
        errString,
        nop,
        true
      )(dispatch, getState)
    ]);
    const groupTree = computeSettingsTree.apply(this, results);
    const layerTree = computeLayersTree.apply(this, results);

    dispatch(_settingsUpdated(groupTree));
    dispatch(_layersWidgetUpdated(layerTree));
  } catch (err) {
    dispatch(_settingsUpdateFailure());
    throw err;
  }
  return result;
}

/**
 * Call the backend in order to compute the settings tree
 */
export function loadSettings(onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    return await _loadSettingsInner(dispatch, getState, onLoadStart, onLoadEnd);
  };
}

export function updateSettingsOnServer(settingsDelta, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const done = async () => {
        const result = await _loadSettingsInner(dispatch, getState, onLoadStart, onLoadEnd);
        return result;
      };
      const response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'generalSettings',
        'updateSettings',
        '_general_setttings_update_failure',
        done,
        true,
        settingsDelta
      )(dispatch, getState);
      return response;
    } catch (err) {
      dispatch({
        type: UPDATE_SETTINGS_FAIL
      });
      console.warn('Update report Request Error', err);
      throw err;
    }
  };
}

export function activateIReactSettings(activeIReactSettingNames) {
  return _setActiveSettingNames(activeIReactSettingNames);
}

export function activateIReactSettingByName(settingName) {
  return (dispatch, getState) => {
    const { activeIReactSettingNames } = getState().layersAndSettings;
    if (activeIReactSettingNames.indexOf(settingName) === -1) {
      dispatch(_setActiveSettingNames([...activeIReactSettingNames, settingName]));
    }
  };
}

export function deactivateIReactSettingByName(settingName) {
  return (dispatch, getState) => {
    const { activeIReactSettingNames } = getState().layersAndSettings;
    const index = activeIReactSettingNames.indexOf(settingName);
    if (index > -1) {
      const nextSettingNames = [...activeIReactSettingNames]; // copy
      nextSettingNames.splice(index, 1);
      dispatch(_setActiveSettingNames(nextSettingNames));
    }
  };
}

export function activateCopernicusLayer(name) {
  return (dispatch, getState) => {
    const { activeCopernicusNames } = getState().layersAndSettings;
    if (activeCopernicusNames.indexOf(name) === -1) {
      dispatch(_setActiveCopernicusLayer([...activeCopernicusNames, name]));
    }
  };
}

export function deactivateCopernicusLayer(name) {
  return (dispatch, getState) => {
    const { activeCopernicusNames } = getState().layersAndSettings;
    const index = activeCopernicusNames.indexOf(name);
    if (index > -1) {
      const nextCopernicusLayers = [...activeCopernicusNames]; // copy
      nextCopernicusLayers.splice(index, 1);
      dispatch(_setActiveCopernicusLayer(nextCopernicusLayers));
    }
  };
}

export function deactivateMapRequestLayer(code) {
  return (dispatch, getState) => {
    const { activeLayersFromMapRequest } = getState().layersAndSettings;
    const index =
      activeLayersFromMapRequest && activeLayersFromMapRequest.layers.length > 0
        ? activeLayersFromMapRequest.layers.findIndex(
            a => a.extensionData.attributes.RequestCode === code
          )
        : -1;
    if (index > -1) {
      activeLayersFromMapRequest.layers.splice(index, 1);
    }
  };
}

export function clearAllIReactTasks() {
  return _setActiveSettingNames([]);
}

export function addSettingToBeUpdated(item) {
  return {
    type: ADD_EDITED_SETTING,
    item
  };
}

export function updateMapRequestLayers(layers = [], taskId = -1) {
  return (dispatch, getState) => {
    const state = getState();
    const setting =
      taskId > -1 ? findLayerSettingByTaskId(state.layersAndSettings.remoteSettings, taskId) : null;
    const settingName = setting ? setting.setting.settingDefinition.name : null;

    const activeLayersFromMapRequest = {
      layers,
      settingNames: settingName ? [settingName] : [],
      settings: setting ? [setting] : []
    };

    dispatch({
      type: SET_ACTIVE_LAYERS_FROM_MAP_REQUEST,
      activeLayersFromMapRequest
    });
  };
}
