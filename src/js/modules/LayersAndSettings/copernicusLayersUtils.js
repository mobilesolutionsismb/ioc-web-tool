export const COPERNICUS_TASK_IDS = {
  delineation: ['3102', '3104', '3111'],
  grading: ['3132', '3134', '3139', '3140', '3141'],
  reference: ['3164', '3167', '3171']
};

export const COPERNICUS_TYPES = Object.keys(COPERNICUS_TASK_IDS);

export const COPERNICUS_HAZARD_DISPLAY_NAMES = [
  'Drought',
  'Epidemic',
  'Extreme temperature',
  'Humanitarian',
  'Infestation',
  'Mass movement',
  'Storm',
  'Transportation accident',
  'Volcanic activity',
  'Wildfire',
  'Forest fire, wild fire',
  'Flood',
  'Flood (Riverine flood)',
  'Wind storm',
  'Earthquake',
  'Industrial accident',
  'Other'
];

/**
 * Returns true if taskId is in the range of IDs of type
 * @param {string} type - (delineation|gradient|reference)
 * @param {string} taskId - task id
 * @returns boolean
 */
function _isCopernicusType(type, taskId) {
  const taskIdsSet = COPERNICUS_TASK_IDS[type];
  if (Array.isArray(taskIdsSet) && taskIdsSet.length > 0) {
    return taskIdsSet.indexOf(taskId) > -1;
  } else {
    return false;
  }
}

/**
 * Returns true if layer belongs to hazardLabel (has Hazard_Type === hazardLabel in extension data)
 * @param {string} hazardLabel - Copernicus hazard name
 * @param {Object} layer - layer config coming from API
 * @return boolean
 */
function _filterLayerByHazard(hazardLabel, layer) {
  return (
    layer.extensionData &&
    layer.extensionData.attributes &&
    layer.extensionData.attributes['Hazard_Type'] === hazardLabel
  );
}

/**
 * Groups layers by hazardLabel ad taskId (which means type in delineation, grading, reference)
 * @param {Object} allLayers  - map <taskId: string>: <Array<LayerConfig>>
 * @param {string} hazardLabel - Copernicus hazard name
 * @param {Object} groupedCopernicusLayers - layerConfig to be generated (reduced)
 * @param {string} nextTaskId - task ID (key of allLayers)
 * @returns reduced groupedCopernicusLayers
 */
function _reduceCopernicusLayers(allLayers, hazardLabel, groupedCopernicusLayers, nextTaskId) {
  for (const type of COPERNICUS_TYPES) {
    if (_isCopernicusType(type, nextTaskId)) {
      const layersSet = allLayers[nextTaskId];
      const filtered = layersSet.filter(_filterLayerByHazard.bind(null, hazardLabel));
      if (filtered.length > 0) {
        groupedCopernicusLayers[type] = [...groupedCopernicusLayers[type], ...filtered];
      }
    }
  }

  return groupedCopernicusLayers;
}

/**
 * Returns true if remappedLayerConfig contains no information
 * @param {Object} remappedLayerConfig - object with layers divided into delineation, gradient, reference
 * @returns {boolean}
 */
function _isEmptyLayers(remappedLayerConfig) {
  const { delineation, grading, reference } = remappedLayerConfig;
  return delineation.length + grading.length + reference.length === 0;
}

/**
 * Map layers from GetLayers with Hazard labels
 * @param {Object} allLayers - map <taskId: string>: <Array<LayerConfig>>
 * @param {string} hazardDisplayName
 * @returns <Array<CopernicusGroup>> with meaningful information
 */
function _mapCopernicusLayersByHazard(allLayers, hazardDisplayName) {
  // A single group
  const groupedCopernicusLayersInitial = {
    delineation: [],
    grading: [],
    reference: []
  };

  const layers = Object.keys(allLayers).reduce(
    _reduceCopernicusLayers.bind(null, allLayers, hazardDisplayName),
    groupedCopernicusLayersInitial
  );

  const isEmpty = _isEmptyLayers(layers); //clean

  return isEmpty
    ? null
    : {
        displayName: hazardDisplayName,
        layers
      };
}

/**
 * Regroup Copernicus layer in a structure which is
 * <Array<CopernicusGroup>>
 * CopernicusGroup = {
 *    displayName: string // cop
 *    layers: {
 *          delineation: <Array<LayerDefinition>,
 *          grading: <Array<LayerDefinition>,
 *          reference: <Array<LayerDefinition>
 *    }
 * }
 *
 * LayerDefinition = original layer definition from GetLayers API
 * @param {Object} layers - map <taskId: string>: <Array<LayerConfig>>
 * @returns <Array<CopernicusGroup>>
 */
export function groupCopernicus(layers) {
  const result = COPERNICUS_HAZARD_DISPLAY_NAMES.map(
    _mapCopernicusLayersByHazard.bind(null, layers)
  ).filter(layerConfig => layerConfig !== null);

  return result;
}
