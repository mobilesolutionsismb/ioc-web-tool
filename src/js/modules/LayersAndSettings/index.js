// TODO check if this has to be moved into API
import Reducer from './dataSources/Reducer';
import withLayersAndSettings from './dataProviders/withLayersAndSettings';

export { withLayersAndSettings, Reducer };

export default Reducer;
