import {
  SET_READY_STATUS,
  RECEIVED,
  READ,
  /* ERROR, CLEAR_ERRORS,  */ UNSUBSCRIBE_AND_RESET
} from './Actions';
import nManager from '../RTNotificationsManager';

function _setNotificationsReady(ready = false) {
  return {
    type: SET_READY_STATUS,
    ready
  };
}

function _receiveNotification(notification) {
  return {
    type: RECEIVED,
    notification
  };
}

// function _getNotificationsError(error) {
//     return {
//         type: ERROR,
//         error
//     };
// }
// function _getNotificationsError(error) {
//     return {
//         type: ERROR,
//         error
//     };
// }
export function _unsubscribe() {
  return {
    type: UNSUBSCRIBE_AND_RESET
  };
}

export function initializeNotifications() {
  return async dispatch => {
    try {
      const notificationHandler = notification => {
        dispatch(_receiveNotification(notification));
      };
      const notificationReadyHandler = ready => {
        dispatch(_setNotificationsReady(ready));
      };
      const stopConnectionHandler = () => {
        dispatch(_unsubscribe());
      };
      const result = nManager.connect(
        notificationHandler,
        notificationReadyHandler,
        stopConnectionHandler
      );
      return result;
    } catch (err) {
      console.error(err);
      throw err;
    }
  };
}

export function setNotificationRead(notification) {
  console.warn('setNotificationRead', notification);
  return {
    type: READ,
    notification
  };
}

export function unsubscribeNotifications() {
  return async dispatch => {
    try {
      const result = await nManager.unsubscribeAll();
      dispatch(_unsubscribe());
      return result;
    } catch (err) {
      console.error(err);
      throw err;
    }
  };
}
