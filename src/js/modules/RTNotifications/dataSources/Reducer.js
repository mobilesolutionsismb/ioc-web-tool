import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  SET_READY_STATUS,
  RECEIVED,
  READ,
  ERROR,
  /*  CLEAR_ERRORS, */ UNSUBSCRIBE_AND_RESET
} from './Actions';

const mutators = {
  [SET_READY_STATUS]: {
    ready: action => action.ready
  },
  [RECEIVED]: {
    nReceived: (action, state) => {
      const notification = { ...action.notification, receivedTimeStamp: Date.now() };
      return [...state.nReceived, notification];
    }
  },
  [READ]: {
    nReceived: (action, state) => {
      const notification = action.notification;
      const receivedNs = [...state.nReceived];
      const index = receivedNs.findIndex(n => n.id === notification.id);
      if (index === -1) {
        return receivedNs;
      } else {
        //remove at index
        receivedNs.splice(index, 1);
        return receivedNs;
      }
    }
  },
  [ERROR]: {
    nError: action => action.error
  },
  [UNSUBSCRIBE_AND_RESET]: {
    ready: false,
    nReceived: [],
    nError: null
  }
};

export default reduceWith(mutators, DefaultState);
