import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const notificationsSelector = state => state.rtNotifications.nReceived;
const notificationsReadySelector = state => state.rtNotifications.ready;
const hasNotificationsSelector = state => state.rtNotifications.nReceived.length > 0;
const notificationsErrorSelector = state => state.rtNotifications.nError;

const select = createStructuredSelector({
  notifications: notificationsSelector,
  notificationsReady: notificationsReadySelector,
  hasNotifications: hasNotificationsSelector,
  notificationsError: notificationsErrorSelector
});

export default connect(select, ActionCreators);
