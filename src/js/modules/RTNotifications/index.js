import Reducer from './dataSources/Reducer';
import withNotification from './dataProviders/withNotification';

export { withNotification, Reducer };
export {
  initializeNotifications,
  setNotificationRead,
  unsubscribeNotifications
} from './dataSources/ActionCreators';

export default Reducer;
