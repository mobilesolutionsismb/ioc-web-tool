//import { hubConnection } from 'signalr-no-jquery';
import { default as $ } from 'jQuery';
import { API } from 'ioc-api-interface';
import axios from 'axios';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';

const api = API.getInstance();

// Print notifications to console (debug only)
function remapNotificationData(data) {
  return data.result.items
    .map(i => ({
      message: i.notification.data.message,
      state: i.state
    }))
    .forEach(n => console.log(`${n.state}: ${n.message} `));
}

const AppConsts = {
  remoteServiceBaseUrl: IREACT_BACKEND,
  auth: {
    tokenCookieName: 'Abp.AuthToken'
  }
};
/*


    AppConsts.remoteServiceBaseUrl = IREACT_BACKEND

    function initGetNotification() {
        $.connection.abpCommonHub.client.getNotification = function (notification) {
            console.log('abp.notifications.received');
        };
    }

    function connect() {

        $.connection.hub.url = AppConsts.remoteServiceBaseUrl + "/signalr";
        $.connection.hub.qs = "enc_auth_token=" + enc_token;
        $.connection.hub.logging = true;

        initGetNotification(); // set notification listener
        $.connection.hub.start({ transport: ['webSockets'], jsonp: true }).done(function () { // start signalr connection
            console.log("Connected, transport = " + $.connection.hub.transport.name);
            $.connection.abpCommonHub.server.register().done(function () { // call a function on the hub
                console.log('Registered to the SignalR server!');
            });
        })

    }
*/

class RTNotifications {
  connection = null;
  hubProxy = null;
  ready = false;
  notificationHandler = null;
  connecting = false;
  disconnecting = false;
  // Fetch messages on server (first 10)
  readMessagesFromServer = async () => {
    try {
      const res = await api.notifications.getUserNotifications(10, 0);
      remapNotificationData(res);
    } catch (err) {
      console.error('cannot read messages on server', err);
    }
  };

  // Send a message (that should echo back)
  sendMessage = async message => {
    try {
      await api.notifications.sendTestNotification(message, 'ignored');
    } catch (err) {
      console.error('cannot send message', err);
    }
  };

  // Register handlers - private
  _initGetNotification() {
    return new Promise(success => {
      $.connection.abpCommonHub.client.getNotification = message => {
        console.warn('%c Notification received! 🔔', 'background: gold; color: black', message);
        this.notificationHandler(message);
      };
      success();
    });
  }

  // connects to the hub
  connect = async (notificationHandler, notificationReadyHandler, stopConnectionHandler) => {
    // Check if service is available
    try {
      const res = await axios.get(`${IREACT_BACKEND}/signalr/hubs`);
      console.log('RES', res);
      if (!this.connecting) {
        this.connecting = true;
        this.notificationHandler = notificationHandler;
        this.notificationReadyHandler = notificationReadyHandler;
        this.stopConnectionHandler = stopConnectionHandler;
        if (api.encryptedAccessToken) {
          $.connection.hub.url = AppConsts.remoteServiceBaseUrl + '/signalr';
          $.connection.hub.qs = 'enc_auth_token=' + api.encryptedAccessToken;
          $.connection.hub.logging = true;
          return this._initGetNotification().then(() => {
            $.connection.hub
              .start({
                transport: ['webSockets'],
                jsonp: true
              })
              .done(() => {
                // start signalr connection
                console.log('Connected, transport = ' + $.connection.hub.transport.name);
                $.connection.abpCommonHub.server.register().done(() => {
                  // call a function on the hub
                  console.log(
                    '%c Now connected and registered',
                    'color: lime; background: whitesmoke'
                  );
                  this.notificationReadyHandler(true);
                  this.connecting = false;
                  this.connection = $.connection;
                  return Promise.resolve({
                    connected: false,
                    error: null
                  });
                });
              })
              .fail(e => {
                this.connecting = false;
                console.error('Could not connect', e);
                return Promise.resolve({
                  connected: true,
                  error: e
                });
              });
          });
        } else {
          this.connecting = false;
          console.warn('You must be logged in/invalid encryptedAccessToken');
          return Promise.resolve({
            connected: false,
            error: null
          });
        }
      }
    } catch (error) {
      console.log(
        '%c Cannot reach signalR hub on backend',
        'color: white; background: orangered;',
        error
      );
      appInsights.trackException(error, 'RTNotifications::connect', {}, {}, SeverityLevel.Error);
      return Promise.reject({
        connected: false,
        error
      });
    }
  };
  unsubscribeAll = async () => {
    this.disconnecting = true;
    //await this.connection.stop(true, false);
    console.log('Disconected from RTNotifications channel');
    //this.stopConnectionHandler();
    return Promise.resolve({
      connected: false,
      error: null
    });
  };
}
export default new RTNotifications();
