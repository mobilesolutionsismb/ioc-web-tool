import { RESET_ALL as AG_LOC_FILTERS_RESET } from './agentLocationFilters/dataSources/Actions';
import { RESET_ALL as EM_EVENT_FILTERS_RESET } from './clientSideEventFilters/dataSources/Actions';
import { RESET_ALL as EMCOMM_FILTERS_RESET } from './emcommFilters/dataSources/Actions';
import { RESET_ALL as EMCOMM_NEW_RESET } from './emCommNew/dataSources/Actions';
import { RESET_ALL as EM_EVENT_NEW_RESET } from './eventCreate/dataSources/Actions';
import { RESET_MAP as MAP_RESET } from './map/dataSources/Actions';
import { RESET_ALL as MAP_LAYERS_FILTERS_RESET } from './mapLayerFilters/dataSources/Actions';
import { RESET_ALL as MAP_REQ_CREATE } from './mapRequestCreate/dataSources/Actions';
import { RESET_ALL as MAP_REQ_FILTERS_RESET } from './mapRequestFilters/dataSources/Actions';
import { RESET_ALL as MISSION_CREATE_RESET } from './missionCreate/dataSources/Actions';
import { RESET_ALL as MISSION_FILTERS_RESET } from './missionFilters/dataSources/Actions';
import { RESTORE_DEFAULT as PREFERENCES_RESET } from './preferences/dataSources/Actions';
import { RESET as QUICK_USER_RESET } from './QuickUserAdd/dataSources/Actions';
import { RESET_ALL as REPORT_FILTERS_RESET } from './reportFilters/dataSources/Actions';
import { RESET_ALL as REP_REQ_CREATE_RESET } from './reportNewRequest/dataSources/Actions';
import { RESET_ALL as REP_REQ_FITLERS_RESET } from './reportRequestFilters/dataSources/Actions';
// import { UNSUBSCRIBE_AND_RESET as RTNOT_RESET } from './RTNotifications/dataSources/Actions';
import { RESET_ALL as SOCIAL_FILTERS_RESET } from './socialFilters/dataSources/Actions';
import { RESET_ALL as TASK_CREATE_RESET } from './taskCreate/dataSources/Actions';
import { CLEAR_SLIDERS as SLIDERS_RESET } from './TimeSliders/dataSources/Actions';

export const RESET_ACTIONS = [
  AG_LOC_FILTERS_RESET,
  EM_EVENT_FILTERS_RESET,
  EMCOMM_FILTERS_RESET,
  EMCOMM_NEW_RESET,
  EM_EVENT_NEW_RESET,
  MAP_RESET,
  MAP_LAYERS_FILTERS_RESET,
  MAP_REQ_CREATE,
  MAP_REQ_FILTERS_RESET,
  MISSION_CREATE_RESET,
  MISSION_FILTERS_RESET,
  PREFERENCES_RESET,
  QUICK_USER_RESET,
  REPORT_FILTERS_RESET,
  REP_REQ_CREATE_RESET,
  REP_REQ_FITLERS_RESET,
  // RTNOT_RESET,
  SOCIAL_FILTERS_RESET,
  TASK_CREATE_RESET,
  SLIDERS_RESET
];
