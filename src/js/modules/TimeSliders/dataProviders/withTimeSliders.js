import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  activeWidgets: state => state.timeSliders.activeWidgets
});

export default connect(select, ActionCreators);
