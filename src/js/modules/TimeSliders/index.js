import Reducer from './dataSources/Reducer';
import withTimeSliders from './dataProviders/withTimeSliders';

export { withTimeSliders, Reducer };

export default Reducer;
