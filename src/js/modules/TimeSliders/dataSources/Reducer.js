import reduceWith from 'js/startup/reduceWith';

import DefaultState from './State';
import { SET_SLIDERS, CLEAR_SLIDERS } from './Actions';

const mutators = {
  [SET_SLIDERS]: {
    activeWidgets: action => action.activeWidgets
  },
  [CLEAR_SLIDERS]: {
    activeWidgets: []
  }
};

export default reduceWith(mutators, DefaultState);
