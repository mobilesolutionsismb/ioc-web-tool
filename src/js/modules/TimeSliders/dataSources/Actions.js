const NS = 'ts:widget';

export const SET_SLIDERS = `${NS}@SET_SLIDERS`;
export const CLEAR_SLIDERS = `${NS}@CLEAR_SLIDERS`;
