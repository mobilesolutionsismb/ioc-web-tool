import { SET_SLIDERS, CLEAR_SLIDERS } from './Actions';

/* Provisional */
// import { getRandomActiveWidget } from './mocked-up-data';
/* Provisional */

export function clearSliders() {
  return {
    type: CLEAR_SLIDERS
  };
}

function _setSliders(activeWidgets) {
  return {
    type: SET_SLIDERS,
    activeWidgets
  };
}

/* Provisional */
// export function updateWidgetsRandom() {
//     return (dispatch, getState) => {
//         // const state = getState();
//         // TODO compute active widgets from state from ireact tasks with n.layers > 1
//         const activeWidgets = getRandomActiveWidget();
//         dispatch(_setSliders(activeWidgets));
//     };
// }
/* Provisional */

export function removeWidget(id) {
  return (dispatch, getState) => {
    const state = getState();
    const index = state.timeSliders.activeWidgets.findIndex(ts => ts.id === id);
    if (index > -1) {
      // TODO update map/layers/legend status coehrently
      const nextAw = [...state.timeSliders.activeWidgets];
      nextAw.splice(index, 1);
      dispatch(_setSliders(nextAw));
    }
  };
}
