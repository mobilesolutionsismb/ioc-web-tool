const x = [
  {
    tileJson: {
      tilejson: '2.2.0',
      name: 'i-react_test:t3302_20171113T132040_20170701T100000_Kritis_Finland',
      description: '',
      version: '1.0.0',
      attribution: "<a href='http://ireact.eu'>I-REACT</a>",
      legend: '',
      scheme: 'xyz',
      tiles: [
        'https://geoserver.ireact.cloud/geoserver/wms?format=image/png&bbox={bbox-epsg-3857}&service=Wms&request=GetMap&version=1.3.0&srs=EPSG:3857&width=256&height=256&layers=i-react_test:t3302_20171113T132040_20170701T100000_Kritis_Finland&transparent=true'
      ],
      minzoom: 0,
      maxzoom: 30
    },
    metadataId: 80798,
    start: '2017-03-31T00:00:00Z',
    end: '2017-03-31T00:00:00Z',
    leadTime: '2017-07-01T10:00:00Z'
  },
  {
    tileJson: {
      tilejson: '2.2.0',
      name: 'i-react_test:t3302_20171113T132040_20170702T100000_Kritis_Malta',
      description: '',
      version: '1.0.0',
      attribution: "<a href='http://ireact.eu'>I-REACT</a>",
      legend: '',
      scheme: 'xyz',
      tiles: [
        'https://geoserver.ireact.cloud/geoserver/wms?format=image/png&bbox={bbox-epsg-3857}&service=Wms&request=GetMap&version=1.3.0&srs=EPSG:3857&width=256&height=256&layers=i-react_test:t3302_20171113T132040_20170702T100000_Kritis_Malta&transparent=true'
      ],
      minzoom: 0,
      maxzoom: 30
    },
    metadataId: 80798,
    start: '2017-03-31T00:00:00Z',
    end: '2017-03-31T00:00:00Z',
    leadTime: '2017-07-02T10:00:00Z'
  },
  {
    tileJson: {
      tilejson: '2.2.0',
      name: 'i-react_test:t3302_20171113T132040_20170702T100000_Kritis_Italy',
      description: '',
      version: '1.0.0',
      attribution: "<a href='http://ireact.eu'>I-REACT</a>",
      legend: '',
      scheme: 'xyz',
      tiles: [
        'https://geoserver.ireact.cloud/geoserver/wms?format=image/png&bbox={bbox-epsg-3857}&service=Wms&request=GetMap&version=1.3.0&srs=EPSG:3857&width=256&height=256&layers=i-react_test:t3302_20171113T132040_20170702T100000_Kritis_Italy&transparent=true'
      ],
      minzoom: 0,
      maxzoom: 30
    },
    metadataId: 80798,
    start: '2017-03-31T00:00:00Z',
    end: '2017-03-31T00:00:00Z',
    leadTime: '2017-07-02T10:00:00Z'
  },
  {
    tileJson: {
      tilejson: '2.2.0',
      name: 'i-react_test:t3302_20171113T132040_20170702T100000_Kritis_Spain',
      description: '',
      version: '1.0.0',
      attribution: "<a href='http://ireact.eu'>I-REACT</a>",
      legend: '',
      scheme: 'xyz',
      tiles: [
        'https://geoserver.ireact.cloud/geoserver/wms?format=image/png&bbox={bbox-epsg-3857}&service=Wms&request=GetMap&version=1.3.0&srs=EPSG:3857&width=256&height=256&layers=i-react_test:t3302_20171113T132040_20170702T100000_Kritis_Spain&transparent=true'
      ],
      minzoom: 0,
      maxzoom: 30
    },
    metadataId: 80798,
    start: '2017-03-31T00:00:00Z',
    end: '2017-03-31T00:00:00Z',
    leadTime: '2017-07-02T10:00:00Z'
  },
  {
    tileJson: {
      tilejson: '2.2.0',
      name: 'i-react_test:t3302_20171113T132040_20170702T100000_Kritis_UK',
      description: '',
      version: '1.0.0',
      attribution: "<a href='http://ireact.eu'>I-REACT</a>",
      legend: '',
      scheme: 'xyz',
      tiles: [
        'https://geoserver.ireact.cloud/geoserver/wms?format=image/png&bbox={bbox-epsg-3857}&service=Wms&request=GetMap&version=1.3.0&srs=EPSG:3857&width=256&height=256&layers=i-react_test:t3302_20171113T132040_20170702T100000_Kritis_UK&transparent=true'
      ],
      minzoom: 0,
      maxzoom: 30
    },
    metadataId: 80798,
    start: '2017-03-31T00:00:00Z',
    end: '2017-03-31T00:00:00Z',
    leadTime: '2017-07-02T10:00:00Z'
  }
];
