import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const selectedTweetSelector = state => state.selectedTweet.selectedTweet;
const selectedGeometrySelector = state => state.selectedTweet.selectedGeometry;

const select = createStructuredSelector({
  selectedTweet: selectedTweetSelector,
  selectedGeometry: selectedGeometrySelector
});

export default connect(select, ActionCreators);
