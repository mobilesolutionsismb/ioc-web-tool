import Reducer from './dataSources/Reducer';
import withSelectedTweet from './dataProviders/withSelectedTweet';

export { withSelectedTweet, Reducer };
export { clickTweet, deleteClickTweet } from './dataSources/ActionCreators';

export default Reducer;
