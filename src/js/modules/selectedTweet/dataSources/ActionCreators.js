import { CLICK_TWEET, DELETE_CLICK_TWEET } from './Actions';

function clickTweet(tweetId, tweetGeometry) {
  // It's needed to pass the attributes too
  return {
    type: CLICK_TWEET,
    tweetId,
    tweetGeometry
  };
}

function deleteClickTweet() {
  return {
    type: DELETE_CLICK_TWEET
  };
}

export { clickTweet, deleteClickTweet };
