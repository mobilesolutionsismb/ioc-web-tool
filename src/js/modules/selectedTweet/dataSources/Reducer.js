import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import { CLICK_TWEET, DELETE_CLICK_TWEET } from './Actions';

const mutators = {
  [CLICK_TWEET]: {
    selectedTweet: action => action.tweetId,
    selectedGeometry: action => action.tweetGeometry
  },
  [DELETE_CLICK_TWEET]: {
    selectedTweet: null,
    selectedGeometry: null
  }
};

export default reduceWith(mutators, DefaultState);
