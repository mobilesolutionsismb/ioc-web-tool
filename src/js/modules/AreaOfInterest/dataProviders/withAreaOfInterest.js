import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as ActionCreators from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  draw: state => state.areaOfInterest.draw
});

export default connect(select, ActionCreators);
