import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import { drawerNames, EMPTY_DRAW_STATE } from './categories';
import wellknown from 'wellknown';
import {
  RESET_DRAW,
  SET_DRAW_POINT,
  SET_DRAW_POLYGON,
  SET_DRAW_STEP,
  SET_DRAW_TRASH,
  SET_AREA_OF_INTEREST /* , SET_ADD_SOURCE_LAYER */
} from './Actions';

function isValidCategory(category, currentCategories) {
  return Object.values(drawerNames).indexOf(category) > -1; // drawerNames.hasOwnProperty(category);
}

const mutators = {
  [RESET_DRAW]: {
    draw: (action, state) => {
      const { category } = action;
      if (isValidCategory(category, state.draw)) {
        return {
          ...state.draw,
          activeDraw: '',
          [category]: EMPTY_DRAW_STATE
        };
      }
    }
  },
  [SET_DRAW_POINT]: {
    draw: (action, state) => {
      const { category } = action;
      if (isValidCategory(category, state.draw)) {
        return {
          ...state.draw,
          activeDraw: category,
          [category]: {
            ...state.draw[category],
            point: action.point,
            currentStep: 2,
            currentDrawMode: 'draw_polygon',
            selectedReverseGeocode: action.selectedReverseGeocode
          }
        };
      }
    }
  },
  [SET_DRAW_POLYGON]: {
    draw: (action, state) => {
      const { category } = action;
      if (isValidCategory(category, state.draw)) {
        return {
          ...state.draw,
          activeDraw: category,
          [category]: {
            ...state.draw[category],
            polygon: action.polygon,
            currentStep: 3,
            currentDrawMode: 'simple_select'
          }
        };
      }
    }
  },
  [SET_DRAW_STEP]: {
    draw: (action, state) => {
      let currentDrawMode = '';
      switch (action.currentStep) {
        case 0:
          currentDrawMode = 'simple_select';
          break;
        case 1:
          currentDrawMode = 'draw_point';
          break;
        case 2:
          currentDrawMode = 'draw_polygon';
          break;
        default:
          currentDrawMode = 'simple_select';
          break;
      }
      const { category } = action;
      if (isValidCategory(category, state.draw)) {
        return {
          ...state.draw,
          activeDraw: category,
          [category]: {
            ...state.draw[category],
            currentStep: action.currentStep,
            currentDrawMode
          }
        };
      }
    }
  },
  [SET_DRAW_TRASH]: {
    draw: (action, state) => {
      const { category } = action;
      if (isValidCategory(category, state.draw)) {
        return {
          ...state.draw,
          activeDraw: category,
          [category]: {
            ...state.draw[category],
            trashAll: action.trashAll
          }
        };
      }
    }
  },
  [SET_AREA_OF_INTEREST]: {
    draw: (action, state) => {
      const { category, point, selectedReverseGeocode } = action;
      if (isValidCategory(category, state.draw)) {
        return {
          ...state.draw,
          activeDraw: category,
          [category]: {
            ...state.draw[category],
            point,
            //polygon: polygon(action.polygon),
            polygon: wellknown.parse(action.polygon),
            currentStep: 0,
            currentDrawMode: 'simple_select',
            selectedReverseGeocode
          }
        };
      }
    }
  }
};

export default reduceWith(mutators, DefaultState);
