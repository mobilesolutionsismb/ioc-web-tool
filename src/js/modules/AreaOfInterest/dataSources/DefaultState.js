import { EMPTY_DRAW_STATE } from './categories';

const STATE = {
  draw: {
    report_new_request: EMPTY_DRAW_STATE,
    report: EMPTY_DRAW_STATE,
    event_create: EMPTY_DRAW_STATE,
    event_edit: EMPTY_DRAW_STATE,
    events_filter: EMPTY_DRAW_STATE,
    reportRequest: EMPTY_DRAW_STATE,
    reports_requests: EMPTY_DRAW_STATE,
    map_request: EMPTY_DRAW_STATE,
    map_request_create: EMPTY_DRAW_STATE,
    map_request_event: EMPTY_DRAW_STATE,
    notifications: EMPTY_DRAW_STATE,
    emergencyCommunication: EMPTY_DRAW_STATE,
    notification_new: EMPTY_DRAW_STATE,
    target_aoi: EMPTY_DRAW_STATE,
    mission_create: EMPTY_DRAW_STATE,
    task_create: EMPTY_DRAW_STATE,
    mission_detail: EMPTY_DRAW_STATE,
    activeDraw: ''
  }
};

export default STATE;
