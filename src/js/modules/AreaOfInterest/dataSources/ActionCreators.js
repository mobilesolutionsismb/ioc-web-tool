import {
  RESET_DRAW,
  SET_DRAW_POINT,
  SET_DRAW_POLYGON,
  SET_DRAW_STEP,
  SET_DRAW_TRASH,
  SET_AREA_OF_INTEREST /* , SET_ADD_SOURCE_LAYER */
} from './Actions';
import { apiCallActionCreatorFactory } from 'ioc-api-interface';
import { LngLat } from 'mapbox-gl';

export function resetDrawState(category) {
  return {
    type: RESET_DRAW,
    category
  };
}

export function setDrawPoint(category, point, culture) {
  return async (dispatch, getState) => {
    let lat = null,
      lng = null;
    if (Array.isArray(point) && point.length === 2) {
      // Assume is geojson-like convention
      lat = point[1];
      lng = point[0];
    } else if (
      point instanceof LngLat ||
      (typeof point.lat === 'number' && typeof point.lng === 'number')
    ) {
      lat = point.lat;
      lng = point.lng;
    } else {
      throw new Error('Invalid point format!');
    }

    try {
      return apiCallActionCreatorFactory(
        null,
        null,
        'maps',
        'reverseGeocode',
        '_maps_reversegeocode_failure',
        data => {
          const result = data.result.resources;
          const selectedReverseGeocode = result && result.length > 0 ? result[0] : {};
          return Promise.resolve({
            type: SET_DRAW_POINT,
            category,
            point,
            selectedReverseGeocode
          });
        },
        true,
        culture,
        lat,
        lng
      )(dispatch, getState);
    } catch (ex) {
      return Promise.reject(ex);
    }
  };
}

export function setDrawPolygon(category, polygon) {
  return {
    type: SET_DRAW_POLYGON,
    category,
    polygon
  };
}

export function setDrawCurrentStep(category, currentStep) {
  return {
    type: SET_DRAW_STEP,
    category,
    currentStep
  };
}

export function setDrawTrash(category, trashAll) {
  return {
    type: SET_DRAW_TRASH,
    category,
    trashAll
  };
}

export function setAreaOfInterest(category, point, polygon, culture) {
  return async (dispatch, getState) => {
    let lat = null,
      lng = null;
    if (Array.isArray(point) && point.length === 2) {
      // Assume is geojson-like convention
      lat = point[1];
      lng = point[0];
    } else if (
      point instanceof LngLat ||
      (typeof point.lat === 'number' && typeof point.lng === 'number')
    ) {
      lat = point.lat;
      lng = point.lng;
    } else {
      throw new Error('Invalid point format!');
    }

    try {
      return apiCallActionCreatorFactory(
        null,
        null,
        'maps',
        'reverseGeocode',
        '_maps_reversegeocode_failure',
        data => {
          const result = data.result.resources;
          const selectedReverseGeocode = result && result.length > 0 ? result[0] : {};
          return Promise.resolve({
            type: SET_AREA_OF_INTEREST,
            category,
            point,
            polygon,
            selectedReverseGeocode
          });
        },
        true,
        culture,
        lat,
        lng
      )(dispatch, getState);
    } catch (ex) {
      return Promise.reject(ex);
    }
  };
}
