export const drawerNames = {
  report: 'reports',
  reportRequest: 'reports_requests',
  reportRequestFilter: 'reportRequest',
  reportFilter: 'report',
  reportNewRequest: 'report_new_request',
  event: 'events',
  eventFilter: 'events_filter',
  eventCreate: 'event_create',
  eventEdit: 'event_edit',
  mission: 'missions',
  missionCreate: 'mission_create',
  missionDetail: 'mission_detail',
  taskCreate: 'task_create',
  missionFilter: '',
  notification: 'notifications',
  notificationFilter: 'emergencyCommunication',
  notificationNew: 'notification_new',
  targetAOI: 'target_aoi',
  social: 'social',
  map: 'map',
  mapRequests: 'map_requests',
  mapRequestFilter: 'mapRequest',
  mapRequestCreate: 'map_request_create',
  mapRequestEvent: 'map_request_event',
  agentLocations: 'agent_locations'
};

export const EMPTY_DRAW_STATE = {
  point: null,
  polygon: null,
  currentStep: 0,
  currentDrawMode: 'simple_select',
  selectedGeocode: {},
  selectedReverseGeocode: {},
  trashAll: false
};
