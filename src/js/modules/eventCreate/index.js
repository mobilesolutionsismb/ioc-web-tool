import Reducer from './dataSources/Reducer';
import withEventCreate from './dataProviders/withEventCreate';

export { withEventCreate, Reducer };
export {
  setEventCreateHazard,
  setEventCreateStartDate,
  setEventCreateStartTime,
  setEventCreateEndDate,
  setEventCreateEndTime,
  setEventCreateTitle,
  setEventCreateComment,
  setEventCreatePA,
  setEventCreatePK,
  setEventCreateDamages,
  resetEventCreate,
  resetDate,
  resetDamages,
  fillEmergencyEvent
} from './dataSources/ActionCreators';

export default Reducer;
