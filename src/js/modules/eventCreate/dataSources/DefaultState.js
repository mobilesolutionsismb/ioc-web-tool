const STATE = {
  id: 0,
  hazard: { label: '', value: '' },
  title: '',
  comment: '',
  startDate: null,
  startTime: null,
  endDate: null,
  endTime: null,
  people_affected: '',
  people_killed: '',
  estimated_damage: '',
  damages: {},
  address: '',
  provinces: [],
  locations: [],
  countryCodes: [],
  countryNames: [],
  twitterSource: null
};

export default STATE;
