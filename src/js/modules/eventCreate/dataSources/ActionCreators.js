import {
  SET_HAZARD,
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_ALL_START_DATE,
  RESET_ALL_END_DATE,
  SET_TITLE,
  SET_COMMENT,
  SET_PEOPLE_AFFECTED,
  SET_PEOPLE_KILLED,
  SET_ESTIMATED_DAMAGES,
  RESET_DATE,
  RESET_DAMAGES,
  RESET_ALL,
  FILL_EMERGENCY_EVENT,
  TWITTER_SOURCE_EVENT
} from './Actions';

function setTwitterSourceEvent(event) {
  return {
    type: TWITTER_SOURCE_EVENT,
    event
  };
}

function setEventCreateHazard(hazard) {
  return {
    type: SET_HAZARD,
    hazard
  };
}

function setEventCreateStartDate(startDate) {
  return {
    type: SET_START_DATE,
    startDate
  };
}

function setEventCreateStartTime(startTime) {
  return {
    type: SET_START_TIME,
    startTime
  };
}

function setEventCreateEndDate(endDate) {
  return {
    type: SET_END_DATE,
    endDate
  };
}

function setEventCreateEndTime(endTime) {
  return {
    type: SET_END_TIME,
    endTime
  };
}
function resetEventCreateAllSDate() {
  console.log('Reset START date...');
  return {
    type: RESET_ALL_START_DATE
  };
}
function resetEventCreateAllEDate() {
  console.log('Reset END date...');
  return {
    type: RESET_ALL_END_DATE
  };
}
function resetEventCreate() {
  return {
    type: RESET_ALL
  };
}

function setEventCreateTitle(title) {
  return {
    type: SET_TITLE,
    title
  };
}

function setEventCreateComment(comment) {
  return {
    type: SET_COMMENT,
    comment
  };
}
function setEventCreatePA(affected) {
  return {
    type: SET_PEOPLE_AFFECTED,
    affected
  };
}
function setEventCreatePK(killed) {
  return {
    type: SET_PEOPLE_KILLED,
    killed
  };
}
function setEventCreateDamages(damages) {
  return {
    type: SET_ESTIMATED_DAMAGES,
    damages
  };
}
function resetDate() {
  return {
    type: RESET_DATE
  };
}
function resetDamages() {
  return {
    type: RESET_DAMAGES
  };
}

function fillEmergencyEvent(emergencyEvent) {
  return {
    type: FILL_EMERGENCY_EVENT,
    emergencyEvent
  };
}

export {
  setEventCreateHazard,
  setEventCreateStartDate,
  setEventCreateStartTime,
  setEventCreateEndDate,
  setEventCreateEndTime,
  resetEventCreateAllSDate,
  resetEventCreateAllEDate,
  resetEventCreate,
  setEventCreateTitle,
  setEventCreateComment,
  setEventCreatePA,
  setEventCreatePK,
  setEventCreateDamages,
  resetDate,
  resetDamages,
  fillEmergencyEvent,
  setTwitterSourceEvent
};
