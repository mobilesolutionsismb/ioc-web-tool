import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  SET_HAZARD,
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_ALL_START_DATE,
  RESET_ALL_END_DATE,
  SET_TITLE,
  SET_COMMENT,
  SET_PEOPLE_AFFECTED,
  SET_PEOPLE_KILLED,
  SET_ESTIMATED_DAMAGES,
  RESET_DATE,
  RESET_DAMAGES,
  RESET_ALL,
  FILL_EMERGENCY_EVENT,
  TWITTER_SOURCE_EVENT
} from './Actions';

const hazardMapping = {
  fire: {
    value: '_hazard_fire',
    label: 'Fire'
  },
  flood: {
    value: '_hazard_flood',
    label: 'Flood'
  },
  landslide: {
    value: '_hazard_landslide',
    label: 'Landslide'
  },
  extremeWeather: {
    value: '_hazard_extremeWeather',
    label: 'Extreme weather'
  },
  avalanches: {
    value: '_hazard_avalanches',
    label: 'Avalanches'
  },
  earthquake: {
    value: '_hazard_earthquake',
    label: 'Earthquake'
  }
};

const mutators = {
  [SET_HAZARD]: {
    hazard: action => action.hazard
  },
  [SET_START_DATE]: {
    startDate: (action, state) => {
      return action.startDate;
    }
  },
  [SET_START_TIME]: {
    startTime: (action, state) => {
      return action.startTime;
    }
  },
  [SET_END_DATE]: {
    endDate: (action, state) => {
      return action.endDate;
    }
  },
  [SET_END_TIME]: {
    endTime: (action, state) => {
      return action.endTime;
    }
  },
  [RESET_ALL_START_DATE]: {
    startDate: null,
    startTime: null
  },
  [RESET_ALL_END_DATE]: {
    endDate: null,
    endTime: null
  },
  [SET_TITLE]: {
    title: action => action.title
  },
  [SET_COMMENT]: {
    comment: action => action.comment
  },
  [SET_PEOPLE_AFFECTED]: {
    people_affected: action => action.affected
  },
  [SET_PEOPLE_KILLED]: {
    people_killed: action => action.killed
  },
  [SET_ESTIMATED_DAMAGES]: {
    estimated_damage: action => action.damages
  },
  [RESET_DATE]: {
    startDate: null,
    startTime: null,
    endDate: null,
    endTime: null
  },
  [RESET_DAMAGES]: {
    people_affected: '',
    people_killed: '',
    estimated_damage: ''
  },
  [FILL_EMERGENCY_EVENT]: {
    startDate: action => new Date(action.emergencyEvent.start),
    startTime: action => new Date(action.emergencyEvent.start),
    endDate: action => (action.emergencyEvent.end ? new Date(action.emergencyEvent.end) : null),
    endTime: action => (action.emergencyEvent.end ? new Date(action.emergencyEvent.end) : null),
    id: action => action.emergencyEvent.id,
    hazard: action =>
      action.emergencyEvent.hazard
        ? hazardMapping[action.emergencyEvent.hazard]
        : { label: '', value: '' },
    title: action => action.emergencyEvent.displayName,
    comment: action => action.emergencyEvent.comment,
    people_affected: action => action.emergencyEvent.extensionData.people_affected,
    people_killed: action => action.emergencyEvent.extensionData.people_killed,
    estimated_damage: action => action.emergencyEvent.extensionData.estimated_damage,
    damages: action => action.emergencyEvent.extensionData.damages,
    address: action => action.emergencyEvent.address,
    provinces: action => action.emergencyEvent.provinces,
    locations: action => action.emergencyEvent.locations,
    countryCodes: action => action.emergencyEvent.countryCodes,
    countryNames: action => action.emergencyEvent.countryNames
  },
  [RESET_ALL]: DefaultState,
  [TWITTER_SOURCE_EVENT]: {
    twitterSource: action => action.event
  }
};

export default reduceWith(mutators, DefaultState);
