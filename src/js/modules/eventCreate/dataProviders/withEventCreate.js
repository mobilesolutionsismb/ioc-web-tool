import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const id = state => state.eventCreate.id;
const hazard = state => state.eventCreate.hazard;
const startDate = state => state.eventCreate.startDate;
const startTime = state => state.eventCreate.startTime;
const endDate = state => state.eventCreate.endDate;
const endTime = state => state.eventCreate.endTime;
const title = state => state.eventCreate.title;
const comment = state => state.eventCreate.comment;
const people_affected = state => state.eventCreate.people_affected;
const people_killed = state => state.eventCreate.people_killed;
const estimated_damage = state => state.eventCreate.estimated_damage;
const address = state => state.eventCreate.address;
const provinces = state => state.eventCreate.provinces;
const locations = state => state.eventCreate.locations;
const countryCodes = state => state.eventCreate.countryCodes;
const countryNames = state => state.eventCreate.countryNames;
const twitterSource = state => state.eventCreate.twitterSource;

const select = createStructuredSelector({
  id,
  hazard,
  startDate,
  startTime,
  endDate,
  endTime,
  title,
  comment,
  people_affected,
  people_killed,
  estimated_damage,
  address,
  provinces,
  locations,
  countryCodes,
  countryNames,
  twitterSource
});

export default connect(select, ActionCreators);
