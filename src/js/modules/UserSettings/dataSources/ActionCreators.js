import {
  UPDATE_ALL_USER_SETTINGS,
  UPDATE_USER_SETTINGS,
  RESET_USER_SETTINGS_TO_MODIFY /* , UPDATE_URL_BEFORE */
} from './Actions';

function updateAllUserSettings(settings) {
  return {
    type: UPDATE_ALL_USER_SETTINGS,
    settings
  };
}

function updateLocalUserSettings(to, setting) {
  return {
    type: UPDATE_USER_SETTINGS,
    to,
    setting
  };
}

function resetUserSettingsToModify() {
  return {
    type: RESET_USER_SETTINGS_TO_MODIFY
  };
}

// REMOVED: we use history state
// See https://reacttraining.com/react-router/web/api/history
// function saveUrlBeforeSettings(url) {
//     return {
//         type: UPDATE_URL_BEFORE,
//         url
//     };
// }

export {
  updateAllUserSettings,
  updateLocalUserSettings,
  resetUserSettingsToModify /* , saveUrlBeforeSettings */
};
