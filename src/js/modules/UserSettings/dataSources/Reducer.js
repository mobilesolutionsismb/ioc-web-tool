import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  UPDATE_ALL_USER_SETTINGS,
  UPDATE_USER_SETTINGS,
  RESET_USER_SETTINGS_TO_MODIFY /* , UPDATE_URL_BEFORE */
} from './Actions';

const mutators = {
  [UPDATE_ALL_USER_SETTINGS]: {
    allUserSettings: action => action.settings
  },
  [UPDATE_USER_SETTINGS]: (state, action) => {
    let settings = state.allUserSettings;
    settings[action.to] = action.setting;

    let settingsToModify = state.settingsToModify;
    settingsToModify = settingsToModify.filter(a => a.key !== action.to);
    settingsToModify.push({
      key: action.to,
      option: action.setting
    });
    return {
      ...state,
      allUserSettings: settings,
      settingsToModify
    };
  },
  [RESET_USER_SETTINGS_TO_MODIFY]: {
    settingsToModify: []
  }
  // [UPDATE_URL_BEFORE]: {
  //     urlBeforeSettings: action => action.url
  // }
};

export default reduceWith(mutators, DefaultState);
