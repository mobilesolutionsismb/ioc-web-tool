import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const allUserSettingsSelector = state => state.userSettings.allUserSettings;
const settingsToModifySelector = state => state.userSettings.settingsToModify;
// const urlBeforeSettingsSelector = state => state.userSettings.urlBeforeSettings;

const select = createStructuredSelector({
  allUserSettings: allUserSettingsSelector,
  settingsToModify: settingsToModifySelector
  // urlBeforeSettings: urlBeforeSettingsSelector
});

export default connect(select, ActionCreators);
