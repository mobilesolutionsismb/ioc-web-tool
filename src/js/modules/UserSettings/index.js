import Reducer from './dataSources/Reducer';
import withUserSettings from './dataProviders/withUserSettings';

export { withUserSettings, Reducer };

export default Reducer;
