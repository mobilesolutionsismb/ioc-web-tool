import Reducer from './dataSources/Reducer';
import withAgentLocationFilters from './dataProviders/withAgentLocationFilters';
import * as AgentLocationFiltersActions from './dataSources/ActionCreators';
export {
  availableSorters,
  availableFilters,
  FILTERS,
  SORTERS,
  agentLocationDataOrigin,
  agentLocationCritical
} from './dataSources/filters';

export default Reducer;
export { Reducer, withAgentLocationFilters, AgentLocationFiltersActions };
