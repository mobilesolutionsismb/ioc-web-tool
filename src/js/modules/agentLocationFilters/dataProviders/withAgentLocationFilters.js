import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, agentLocationCritical, agentLocationDataOrigin } from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  agentLocationCritical,
  agentLocationDataOrigin
};

const select = createStructuredSelector({
  agentLocationFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.agentLocationFilters.filters, FilterMap, FILTERS),
  agentLocationFilters: state => state.agentLocationFilters.filters,
  agentLocationSorter: state => state.agentLocationFilters.sorter
});

export default connect(select, ActionCreators);
