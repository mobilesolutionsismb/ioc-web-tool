import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';
const DESELECT_FEATURE = 'api::ireact_features@DESELECT_FEATURE';

export function resetAllAgentLocationFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addAgentLocationFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

export function addAgentLocationFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no Agent Location is currently selected
    dispatch({ type: DESELECT_FEATURE });
    dispatch(_addAgentLocationFilter(filterCategory, filterName));
  };
}

function _removeAgentLocationFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeAgentLocationFilter(filterCategory, filterName) {
  return dispatch => {
    // ensure no Agent Location is currently selected
    dispatch({ type: DESELECT_FEATURE });
    dispatch(_removeAgentLocationFilter(filterCategory, filterName));
  };
}

export function setAgentLocationSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
