import { FILTERS } from './filters';

const STATE = {
  filters: { ...FILTERS, agentLocationDataOrigin: [], agentLocationCritical: [] },
  sorter: '_position_update'
};

export default STATE;
