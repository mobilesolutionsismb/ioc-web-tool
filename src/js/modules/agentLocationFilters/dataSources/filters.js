const _is_data_origin_mobile = ['==', 'dataOrigin', 'mobile'];
const _is_data_origin_wearable = ['==', 'dataOrigin', 'wearable'];

export const agentLocationDataOrigin = {
  _is_data_origin_mobile,
  _is_data_origin_wearable
};

const _is_ag_location_critical = ['==', 'oxygenLevel', 'critical'];

export const agentLocationCritical = {
  _is_ag_location_critical
};

export const FILTERS = {
  agentLocationDataOrigin: Object.keys(agentLocationDataOrigin),
  agentLocationCritical: Object.keys(agentLocationCritical)
};

export const SORTERS = {
  _position_update: (f1, f2) => {
    if (f1.properties.lastUpdated !== f2.properties.lastUpdated)
      return (
        new Date(f1.properties.lastUpdated).getTime() -
        new Date(f2.properties.lastUpdated).getTime()
      );
    else return f1.properties.id - f2.properties.id;
  }
};

export const availableFilters = Object.keys(FILTERS);
export const availableSorters = Object.keys(SORTERS);
