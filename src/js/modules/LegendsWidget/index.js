import Reducer from './dataSources/Reducer';
import withLegendsWidget from './dataProviders/withLegendsWidget';
import withLegendSelected from './dataProviders/withLegendSelected';
import withMapInfo from './dataProviders/withMapInfo';

export { withLegendsWidget, withLegendSelected, withMapInfo, Reducer };

export default Reducer;
