import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const openLegendsWidgetSelector = state => state.legendsWidget.isLegendsWidgetOpen;

const select = createStructuredSelector({
  isLegendsWidgetOpen: openLegendsWidgetSelector
});

export default connect(select, ActionCreators);
