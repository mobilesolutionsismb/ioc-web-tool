import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const legendSelectedSelector = state => state.legendsWidget.legendSelected;

const select = createStructuredSelector({
  legendSelected: legendSelectedSelector
});

export default connect(select, ActionCreators);
