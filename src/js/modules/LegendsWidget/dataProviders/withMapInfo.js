import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { getMapInfo, resetMapInfo } from '../dataSources/ActionCreators';

const mapInfoSelector = state => state.legendsWidget.mapInfo;

const select = createStructuredSelector({
  mapInfo: mapInfoSelector
});

export default connect(select, { getMapInfo, resetMapInfo });
