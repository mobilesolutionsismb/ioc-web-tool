const NS = 'LAYERSWIDGET';

const OPEN_LEGEND_WIDGET = `${NS}@OPEN_LEGEND_WIDGET`;
const CLOSE_LEGEND_WIDGET = `${NS}@CLOSE_LEGEND_WIDGET`;

const ADD_LEGEND = `${NS}@ADD_LEGEND`; // in the reducer: push action.layerName into state[to]
const REMOVE_LEGEND = `${NS}@REMOVE_LEGEND`; // in the reducer: splice action.layerName from state[to]
const GET_MAPINFO = `${NS}GET_MAPINFO`;
const RESET_MAPINFO = `${NS}RESET_MAPINFO`;

export {
  OPEN_LEGEND_WIDGET,
  CLOSE_LEGEND_WIDGET,
  ADD_LEGEND,
  REMOVE_LEGEND,
  GET_MAPINFO,
  RESET_MAPINFO
};
