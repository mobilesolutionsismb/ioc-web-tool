import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  OPEN_LEGEND_WIDGET,
  CLOSE_LEGEND_WIDGET,
  ADD_LEGEND,
  REMOVE_LEGEND,
  GET_MAPINFO,
  RESET_MAPINFO
} from './Actions';

const mutators = {
  [OPEN_LEGEND_WIDGET]: {
    isLegendsWidgetOpen: true
  },
  [CLOSE_LEGEND_WIDGET]: {
    isLegendsWidgetOpen: false
  },
  [ADD_LEGEND]: {
    legendSelected: (action, state) => {
      return action.legendName;
    }
  },
  [REMOVE_LEGEND]: {
    legendSelected: null
  },
  [GET_MAPINFO]: {
    mapInfo: (action, state) => {
      if (Array.isArray(action.result.features) && action.result.features.length > 0)
        return state.mapInfo.concat({
          title: action.title,
          properties: action.result.features[0].properties
        });
      else return state.mapInfo;
    }
  },
  [RESET_MAPINFO]: {
    mapInfo: []
  }
};

export default reduceWith(mutators, DefaultState);
