import {
  OPEN_LEGEND_WIDGET,
  CLOSE_LEGEND_WIDGET,
  ADD_LEGEND,
  REMOVE_LEGEND,
  GET_MAPINFO,
  RESET_MAPINFO
} from './Actions';
import axios from 'axios';

function setOpenLegendsWidget(value) {
  return {
    type: value === true ? OPEN_LEGEND_WIDGET : CLOSE_LEGEND_WIDGET
  };
}

function addLegend(legendName) {
  return {
    type: ADD_LEGEND,
    legendName
  };
}

function removeLegend(/* legendName */) {
  return {
    type: REMOVE_LEGEND /* ,
    legendName */
  };
}

function getMapInfo(
  point,
  url,
  canvas = {},
  bounds,
  layers,
  title = 'Map info',
  version = '1.1.0'
) {
  return async dispatch => {
    if (!url) {
      throw new Error('Missing required GeoServer URL');
    }
    let bbString = [];

    if (bounds) {
      const _southWest = bounds.getSouthWest();
      const _northEast = bounds.getNorthEast();
      bbString = [_southWest.lng, _southWest.lat, _northEast.lng, _northEast.lat].join(',');
    }
    const params = {
      request: 'GetFeatureInfo',
      service: 'WMS',
      srs: 'EPSG:4326',
      styles: '',
      transparent: true,
      version: version,
      format: 'image/png',
      bbox: bbString,
      height: canvas.clientHeight,
      width: canvas.clientWidth,
      layers: layers,
      query_layers: layers,
      info_format: 'application/json',
      exceptions: 'application/json'
    };
    params[params.version === '1.3.0' ? 'i' : 'x'] = point.x;
    params[params.version === '1.3.0' ? 'j' : 'y'] = point.y;
    const response = await axios({
      method: 'get',
      url: `${url.replace(/\/$/, '')}/gwc/service/`,
      params
    });
    dispatch({ type: GET_MAPINFO, result: response.data, title });
  };
}

function resetMapInfo() {
  return {
    type: RESET_MAPINFO
  };
}

export { setOpenLegendsWidget, addLegend, removeLegend, getMapInfo, resetMapInfo };
