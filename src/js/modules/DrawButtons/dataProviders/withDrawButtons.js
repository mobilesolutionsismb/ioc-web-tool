import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const showDrawButtons = state => state.drawButtons.showDrawButtons;

const select = createStructuredSelector({
  showDrawButtons: showDrawButtons
});

export default connect(select, ActionCreators);
