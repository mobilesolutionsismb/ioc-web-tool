import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import { TOGGLE_DRAW_BUTTONS } from './Actions';

const mutators = {
  [TOGGLE_DRAW_BUTTONS]: {
    showDrawButtons: (action, state) => {
      return !state.showDrawButtons;
    }
  }
};

export default reduceWith(mutators, DefaultState);
