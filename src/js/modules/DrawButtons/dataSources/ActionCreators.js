import { TOGGLE_DRAW_BUTTONS } from './Actions';

export function toggleDrawButtons() {
  return {
    type: TOGGLE_DRAW_BUTTONS
  };
}
