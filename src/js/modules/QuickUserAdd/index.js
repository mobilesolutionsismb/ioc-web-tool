import Reducer from './dataSources/Reducer';
import withAdminQuickUserAdd from './dataProviders/withAdminQuickUserAdd';

export { withAdminQuickUserAdd, Reducer };

export default Reducer;
