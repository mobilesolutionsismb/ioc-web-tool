var accounts = require('./accounts.json').records;
var organizations = require('./organizations.json');

const axios = require('axios');

const SERVER = 'http://localhost:22742/';

async function getToken() {
  const response = await axios.post(SERVER + '/api/TokenAuth/Authenticate', {
    userNameOrEmailAddress: 'admin@defaulttenant.com',
    password: '123qwe'
  });
  return response.data.result.accessToken;
}

async function createOrgUnit(authToken, orgUnit) {
  const response = await axios.post(
    SERVER + '/api/services/app/OrganizationUnit/CreateOrganizationUnit',
    {
      ParentId: null,
      OrganizationType: orgUnit.subType,
      AreaOfInterest: orgUnit.AreaOfInterest,
      DisplayName: orgUnit.organization
    }
  );
  return response.data.result.id;
}

async function createProUser(user, orgUnitId) {
  // console.log("CREATE PRO USER", user, orgUnitId)
  const response = await axios.post(SERVER + '/api/services/app/User/CreateOrUpdateUser', {
    user: {
      name: user.name,
      surname: user.surname,
      userName: user.username,
      emailAddress: user.email,
      phoneNumber: '123456789',
      password: 'ireact',
      isActive: true,
      shouldChangePasswordOnNextLogin: false,
      isTwoFactorEnabled: false,
      isLockoutEnabled: false,
      city: user.city,
      state: user.country,
      province: user.city
    },
    AssignedRoleNames: [],
    sendActivationEmail: false,
    setRandomPassword: false
  });
  const response2 = await axios.get(
    SERVER +
      '/api/services/app/User/GetUsers?Filter=' +
      user.username +
      '&MaxResultCount=1&SkipCount=0'
  );
  // console.log(response2)
  const response3 = await axios.post(
    SERVER + '/api/services/app/OrganizationUnit/AddUserToOrganizationUnit',
    {
      userId: response2.data.result.items[0].id,
      organizationUnitId: orgUnitId
    }
  );

  return response3;
}

async function createCitizenUser(user) {
  return axios.post(SERVER + '/api/services/app/Account/Register', {
    name: user.name,
    surname: user.surname,
    userName: user.username,
    emailAddress: user.email,
    phoneNumber: '123456789',
    password: 'ireact',
    city: user.city,
    country: user.country,
    province: user.city,
    bypassValidationToken: 'bypass',
    bypassCaptchaToken: 'bypass',
    captchaResponse: 'abc'
  });
}

async function start() {
  try {
    const authToken = await getToken();

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + authToken;

    organizations.forEach(async o => {
      console.log('CREATE ORGANIZATION', o.organization);
      let orgId = await createOrgUnit(authToken, o);
      let uu = accounts.filter(a => !!a.organization && a.organization === o.organization);
      uu.forEach(async u => {
        console.log('CREATE USER PRO', orgId, u.username);
        await createProUser(u, orgId);
      });
    });

    accounts.filter(a => !a.organization).forEach(async a => {
      console.log('CREATE USER CITIZEN', a.username);
      await createCitizenUser(a);
    });
  } catch (e) {
    throw e;
  }
}

start();
