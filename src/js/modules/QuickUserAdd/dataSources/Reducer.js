import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  RESET,
  RESET_ADD,
  FAILURE,
  LIST_ORGS /* LIST_ORGS_FAILURE, */,
  LIST_ORGS_USERS,
  LIST_TEAMS,
  CLEAR_TEAMS /*  LIST_TEAMS_FAILURE, */,
  ADDING_ORG,
  ADD_ORG,
  ADD_ORG_FAILURE,
  ADDING_TEAM,
  ADD_TEAM,
  ADD_TEAM_FAILURE,
  ADDING_PROFESSIONAL,
  ADD_PROFESSIONAL,
  ADD_PROFESSIONAL_FAILURE,
  ADDING_CITIZEN,
  ADD_CITIZEN,
  ADD_CITIZEN_FAILURE,
  LOADING
} from './Actions';

const mutators = {
  [RESET]: {
    ...DefaultState
  },
  [LOADING]: {
    loading: true
  },
  [RESET_ADD]: {
    orgAddState: 0,
    teamAddState: 0,
    citizenAddState: 0,
    professionalState: 0,
    error: null
  },
  [LIST_ORGS]: {
    organizations: action => action.organizations,
    lastUpdate: () => new Date(),
    loading: false
  },
  [LIST_ORGS_USERS]: {
    orgUsers: action => action.orgUsers,
    loading: false
  },
  [FAILURE]: {
    error: action => action.error,
    loading: false
  },
  [LIST_TEAMS]: {
    teams: action => action.teams,
    loading: false
  },
  [CLEAR_TEAMS]: {
    teams: []
  },
  [ADDING_ORG]: {
    orgAddState: 1,
    error: null
  },
  [ADD_ORG]: {
    orgAddState: 2,
    error: null
  },
  [ADD_ORG_FAILURE]: {
    orgAddState: -1,
    error: action => action.error
  },
  [ADDING_TEAM]: {
    teamAddState: 1,
    error: null
  },
  [ADD_TEAM]: {
    teamAddState: 2,
    error: null
  },
  [ADD_TEAM_FAILURE]: {
    teamAddState: -1,
    error: action => action.error
  },
  [ADDING_CITIZEN]: {
    citizenAddState: 1,
    error: null
  },
  [ADD_CITIZEN]: {
    citizenAddState: 2,
    error: null
  },
  [ADD_CITIZEN_FAILURE]: {
    citizenAddState: -1,
    error: action => action.error
  },
  [ADDING_PROFESSIONAL]: {
    professionalState: 1,
    error: null
  },
  [ADD_PROFESSIONAL]: {
    professionalState: 2,
    error: null
  },
  [ADD_PROFESSIONAL_FAILURE]: {
    professionalState: -1,
    error: action => action.error
  }
};

export default reduceWith(mutators, DefaultState);
