import { APIError } from 'ioc-api-interface';

export function _apiFailure(error, message = '_api_failure') {
  console.error('Quick Admin: _apiFailure', message, error);
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    const _err =
      error.response.data && error.response.data.error
        ? error.response.data.error
        : error.response.data;
    return new APIError(
      _err.message || message,
      error.response.status,
      error.response.statusText,
      _err
    );
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    return new APIError('_no_response');
  } else {
    // Something happened in setting up the request that triggered an Error
    return error;
  }
}

export function _listOrganizationUnits() {
  return {
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnits',
    method: 'get'
  };
}

export function _listTeams(orgId, cancelToken = null) {
  const req = {
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnitTeams',
    method: 'get'
  };
  if (orgId) {
    req['params'] = { ids: orgId };
  }
  if (cancelToken) {
    req['cancelToken'] = cancelToken;
  }
  return req;
}

export function _createOrganizationUnit(OrganizationType, AreaOfInterest, DisplayName) {
  return {
    url: '/api/services/app/OrganizationUnit/CreateOrganizationUnit',
    method: 'post',
    data: {
      ParentId: null,
      OrganizationType,
      AreaOfInterest,
      DisplayName
    }
  };
}

export function _updateOrganizationUnit(id, OrganizationType, AreaOfInterest, DisplayName) {
  return {
    url: '/api/services/app/OrganizationUnit/UpdateOrganizationUnit',
    method: 'put',
    data: {
      id,
      OrganizationType,
      AreaOfInterest,
      DisplayName
    }
  };
}

export function _deleteOrganizationUnit(id) {
  return {
    url: `/api/services/app/OrganizationUnit/DeleteOrganizationUnit/${id}`,
    method: 'delete'
  };
}

export function _updateTeamt(id, ParentId, OrganizationType, AreaOfInterest, DisplayName) {
  return {
    url: '/api/services/app/OrganizationUnit/UpdateTeam',
    method: 'put',
    data: {
      id,
      ParentId,
      OrganizationType,
      AreaOfInterest,
      DisplayName
    }
  };
}

export function _deleteTeam(id) {
  return {
    url: `/api/services/app/OrganizationUnit/DeleteTeam/${id}`,
    method: 'delete'
  };
}

export function _listUsers(orgId) {
  return {
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnitUsers',
    method: 'get',
    params: {
      id: orgId
    }
  };
}

export function _createTeam(AreaOfInterest, DisplayName, parentId) {
  return {
    url: '/api/services/app/OrganizationUnit/CreateTeam',
    method: 'post',
    data: {
      parentId,
      OrganizationType: 'FirstResponder', // all team are enforced FirstResponder
      AreaOfInterest,
      DisplayName
    }
  };
}

export function _createOrUpdateUser(
  name,
  surname,
  username,
  phone,
  email,
  city,
  country,
  roles = []
) {
  return {
    url: '/api/services/app/User/CreateOrUpdateUser',
    method: 'post',
    data: {
      user: {
        name: name,
        surname: surname,
        userName: username,
        emailAddress: email,
        phoneNumber: phone,
        password: 'ireact',
        isActive: true,
        shouldChangePasswordOnNextLogin: false,
        isTwoFactorEnabled: false,
        isLockoutEnabled: false,
        city: city,
        state: country,
        province: city
      },
      AssignedRoleNames: roles,
      sendActivationEmail: false,
      setRandomPassword: false
    }
  };
}

export function _getUserByUsername(username, MaxResultCount = 1, SkipCount = 0) {
  return {
    url: '/api/services/app/User/GetUsers',
    method: 'get',
    params: {
      Filter: username,
      MaxResultCount,
      SkipCount
    }
  };
}

export function _addUserToOrganizationUnit(userId, organizationUnitId) {
  return {
    url: '/api/services/app/OrganizationUnit/AddUserToOrganizationUnit',
    method: 'post',
    data: {
      userId,
      organizationUnitId
    }
  };
}

export function _addUserToTeam(userId, teamId) {
  return {
    url: '/api/services/app/OrganizationUnit/AddUserToTeam',
    method: 'post',
    data: {
      userId,
      teamId
    }
  };
}

export function _registerCitizen(
  name,
  surname,
  username,
  phone = '123456789',
  email,
  city,
  country
) {
  return {
    url: '/api/services/app/Account/Register',
    method: 'post',
    data: {
      name: name,
      surname: surname,
      userName: username,
      emailAddress: email,
      phoneNumber: phone,
      password: 'ireact',
      city: city,
      country: country,
      province: city,
      bypassCaptchaToken: 'bypass',
      bypassValidationToken: 'bypass',
      captchaResponse: 'abc'
    }
  };
}

export function _enableUser(Id) {
  return {
    url: '/api/services/app/Test/EnableUser',
    method: 'post',
    data: {
      Id
    }
  };
}

export function _defaultFailure() {
  return new Error('You must be logged in as admin in order to add user and organizations');
}
