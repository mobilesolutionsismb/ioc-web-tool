const state = {
  organizations: [],
  teams: [],
  orgUsers: {},
  orgAddState: 0,
  teamAddState: 0,
  citizenAddState: 0,
  professionalState: 0,
  error: null,
  loading: null,
  lastUpdate: new Date(0)
};

export default state;
