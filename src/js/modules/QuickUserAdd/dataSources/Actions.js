const NS = 'admin';

export const LIST_ORGS = `${NS}@LIST_ORGS`;
// export const LIST_ORGS_FAILURE = `${NS}@LIST_ORGS_FAILURE`;
export const LIST_ORGS_USERS = `${NS}@LIST_ORGS_USERS`;
export const LIST_TEAMS = `${NS}@LIST_TEAMS`;
export const CLEAR_TEAMS = `${NS}@CLEAR_TEAMS`;
// export const LIST_TEAMS_FAILURE = `${NS}@LIST_TEAMS_FAILURE`;

export const ADDING_ORG = `${NS}@ADDING_ORG`;
export const ADD_ORG = `${NS}@ADD_ORG`;
export const ADD_ORG_FAILURE = `${NS}@ADD_ORG_FAILURE`;

export const ADDING_TEAM = `${NS}@ADDING_TEAM`;
export const ADD_TEAM = `${NS}@ADD_TEAM`;
export const ADD_TEAM_FAILURE = `${NS}@ADD_TEAM_FAILURE`;

export const ADDING_PROFESSIONAL = `${NS}@ADDING_PROFESSIONAL`;
export const ADD_PROFESSIONAL = `${NS}@ADD_PROFESSIONAL`;
export const ADD_PROFESSIONAL_FAILURE = `${NS}@ADD_PROFESSIONAL_FAILURE`;

export const ADDING_CITIZEN = `${NS}@ADDING_CITIZEN`;
export const ADD_CITIZEN = `${NS}@ADD_CITIZEN`;
export const ADD_CITIZEN_FAILURE = `${NS}@ADD_CITIZEN_FAILURE`;

export const RESET = `${NS}@RESET`;
export const RESET_ADD = `${NS}@RESET_ADD`;
export const FAILURE = `${NS}@FAILURE`;
export const LOADING = `${NS}@LOADING`;
