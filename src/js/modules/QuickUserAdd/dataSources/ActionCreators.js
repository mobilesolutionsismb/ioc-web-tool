import { API } from 'ioc-api-interface';
import {
  _apiFailure,
  _listOrganizationUnits,
  _listTeams,
  _listUsers,
  _createOrganizationUnit,
  _createTeam,
  _createOrUpdateUser,
  _getUserByUsername,
  _addUserToOrganizationUnit,
  _addUserToTeam,
  _registerCitizen,
  _enableUser,
  _defaultFailure
} from './apiDefinitions';
import {
  RESET,
  RESET_ADD,
  LIST_ORGS,
  LIST_ORGS_USERS,
  LIST_TEAMS,
  FAILURE,
  ADDING_ORG,
  ADD_ORG,
  ADD_ORG_FAILURE,
  ADDING_TEAM,
  ADD_TEAM,
  ADD_TEAM_FAILURE,
  CLEAR_TEAMS,
  ADDING_PROFESSIONAL,
  ADD_PROFESSIONAL,
  ADD_PROFESSIONAL_FAILURE,
  ADDING_CITIZEN,
  ADD_CITIZEN,
  ADD_CITIZEN_FAILURE,
  LOADING
} from './Actions';

const api = API.getInstance();
const axiosInstance = api._axios;

export function cleanState() {
  return {
    type: RESET_ADD
  };
}

function setLoading() {
  return { type: LOADING };
}

function _cleanState(dispatch) {
  dispatch(cleanState());
}

async function _listOrganizationsInner(dispatch, getState) {
  const apiState = getState().api_authentication;
  if (apiState.logged_in && apiState.user.userName === 'admin') {
    dispatch(setLoading());
    _cleanState(dispatch);
    try {
      const response = await axiosInstance.request(_listOrganizationUnits());
      const { data } = response;
      // TODO dispatch
      dispatch({
        type: LIST_ORGS,
        organizations: data.result
      });
      return data.result;
    } catch (error) {
      dispatch({
        type: FAILURE,
        error
      });
    }
  } else {
    dispatch({
      type: FAILURE,
      error: _defaultFailure()
    });
  }
}

async function _listOrganizationsUsersInner(dispatch, getState, orgId) {
  const apiState = getState().api_authentication;
  if (apiState.logged_in && apiState.user.userName === 'admin') {
    dispatch(setLoading());
    _cleanState(dispatch);
    try {
      const response = await axiosInstance.request(_listUsers(orgId));
      const { data } = response;
      // TODO dispatch
      dispatch({
        type: LIST_ORGS_USERS,
        orgUsers: data.result
      });
      return data.result;
    } catch (error) {
      dispatch({
        type: FAILURE,
        error
      });
    }
  } else {
    dispatch({
      type: FAILURE,
      error: _defaultFailure()
    });
  }
}

export function listOrganizations() {
  return async (dispatch, getState) => {
    return await _listOrganizationsInner(dispatch, getState);
  };
}
export function listOrganizationsUsers(orgId) {
  return async (dispatch, getState) => {
    return await _listOrganizationsUsersInner(dispatch, getState, orgId);
  };
}

async function _listTeamsForOrganizationIdInner(dispatch, getState, orgId) {
  const apiState = getState().api_authentication;
  if (apiState.logged_in && apiState.user.userName === 'admin') {
    dispatch(setLoading());
    _cleanState(dispatch);
    try {
      const response = await axiosInstance.request(_listTeams(orgId));
      const { data } = response;
      // TODO dispatch
      dispatch({
        type: LIST_TEAMS,
        teams: data.result
      });
      return data.result;
    } catch (error) {
      dispatch({
        type: FAILURE,
        error
      });
    }
  } else {
    dispatch({
      type: FAILURE,
      error: _defaultFailure()
    });
  }
}

export function listTeamsForOrganizationId(orgId) {
  return async (dispatch, getState) => {
    return await _listTeamsForOrganizationIdInner(dispatch, getState, orgId);
  };
}

export function registerOrganizationUnit(OrganizationType, AreaOfInterest, DisplayName) {
  return async (dispatch, getState) => {
    _cleanState(dispatch);
    const apiState = getState().api_authentication;
    if (apiState.logged_in && apiState.user.userName === 'admin') {
      dispatch({
        type: ADDING_ORG
      });
      try {
        const config = _createOrganizationUnit(OrganizationType, AreaOfInterest, DisplayName);
        await axiosInstance.request(config);
        const res = await _listOrganizationsInner(dispatch, getState);
        dispatch({
          type: ADD_ORG
        });
        return res;
      } catch (error) {
        dispatch({
          type: ADD_ORG_FAILURE,
          error: _apiFailure(error, '_register_org_failure')
        });
      }
    } else {
      dispatch({
        type: ADD_ORG_FAILURE,
        error: _defaultFailure()
      });
    }
  };
}

export function registerTeam(AreaOfInterest, DisplayName, parentId) {
  return async (dispatch, getState) => {
    _cleanState(dispatch);
    const apiState = getState().api_authentication;
    if (apiState.logged_in && apiState.user.userName === 'admin') {
      dispatch({
        type: ADDING_TEAM
      });
      try {
        const config = _createTeam(AreaOfInterest, DisplayName, parentId);
        await axiosInstance.request(config);
        const res = await _listTeamsForOrganizationIdInner(dispatch, getState, parentId);
        dispatch({
          type: ADD_TEAM
        });
        return res;
      } catch (error) {
        dispatch({
          type: ADD_TEAM_FAILURE,
          error: _apiFailure(error, '_register_team_failure')
        });
      }
    } else {
      dispatch({
        type: ADD_TEAM_FAILURE,
        error: _defaultFailure()
      });
    }
  };
}

export function registerProfessional(
  name,
  surname,
  username,
  phone,
  email,
  city,
  country,
  organizationUnitId,
  teamIds,
  roles
) {
  return async (dispatch, getState) => {
    _cleanState(dispatch);
    if (organizationUnitId === -1 && teamIds.length === 0) {
      throw new Error(
        'Missing organization or teams: please provide a single organization id or one or more team id'
      ); // not both, though BE would support it
    }
    // if (
    //   (organizationUnitId === -1 && teamIds.length === 0) ||
    //   (organizationUnitId > -1 && teamIds.length > 0)
    // ) {
    //   throw new Error('Professional User must belong to an organization OR some teams'); // not both, though BE would support it
    // }

    const apiState = getState().api_authentication;
    if (apiState.logged_in && apiState.user.userName === 'admin') {
      dispatch({
        type: ADDING_PROFESSIONAL
      });
      try {
        const userCreateConfig = _createOrUpdateUser(
          name,
          surname,
          username,
          phone,
          email,
          city,
          country,
          roles
        );
        await axiosInstance.request(userCreateConfig);
        const userIdResponse = await axiosInstance.request(_getUserByUsername(username));
        const userId = userIdResponse.data.result.items[0].id;
        await axiosInstance.request(_enableUser(userId));
        // IF NO Team IDs are provided, add user to org
        if (/* organizationUnitId > -1 &&  */ teamIds.length === 0) {
          await axiosInstance.request(_addUserToOrganizationUnit(userId, organizationUnitId));
          dispatch({
            type: ADD_PROFESSIONAL
          }); // Else add user to teams
        } else if (teamIds.length > 0) {
          //(teamId > -1) {
          await Promise.all(
            teamIds.map(teamId => axiosInstance.request(_addUserToTeam(userId, teamId)))
          );
          dispatch({
            type: ADD_PROFESSIONAL
          });
        }
      } catch (error) {
        dispatch({
          type: ADD_PROFESSIONAL_FAILURE,
          error: _apiFailure(error, '_register_citizen_user_failure')
        });
      }
    } else {
      dispatch({
        type: ADD_PROFESSIONAL_FAILURE,
        error: _defaultFailure()
      });
    }
  };
}

export function registerCitizen(name, surname, username, phone, email, city, country) {
  return async (dispatch, getState) => {
    _cleanState(dispatch);
    const apiState = getState().api_authentication;
    if (apiState.logged_in && apiState.user.userName === 'admin') {
      dispatch({
        type: ADDING_CITIZEN
      });
      try {
        const config = _registerCitizen(name, surname, username, phone, email, city, country);
        await axiosInstance.request(config);
        const userIdResponse = await axiosInstance.request(_getUserByUsername(username));
        const userId = userIdResponse.data.result.items[0].id;
        await axiosInstance.request(_enableUser(userId));
        dispatch({
          type: ADD_CITIZEN
        });
      } catch (error) {
        dispatch({
          type: ADD_CITIZEN_FAILURE,
          error: _apiFailure(error, '_register_professional_user_failure')
        });
      }
    } else {
      dispatch({
        type: ADD_CITIZEN_FAILURE,
        error: _defaultFailure()
      });
    }
  };
}

export function resetUserAndOrganizationData() {
  return {
    type: RESET
  };
}

export function clearTeams() {
  return {
    type: CLEAR_TEAMS
  };
}
