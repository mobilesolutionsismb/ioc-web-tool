import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

function _getOperationState(state) {
  let s = 'unknown';
  switch (state) {
    case 0:
      s = 'nop';
      break;
    case 1:
      s = 'loading';
      break;
    case 2:
      s = 'success';
      break;
    case -1:
      s = 'error';
      break;
    default:
      break;
  }
  return s;
}

export const quickUserAddSelector = createStructuredSelector({
  organizationsList: state => state.adminQuickUserAdd.organizations,
  teamsList: state => state.adminQuickUserAdd.teams,
  orgUsers: state => state.adminQuickUserAdd.orgUsers,
  organization: state => _getOperationState(state.adminQuickUserAdd.orgAddState),
  team: state => _getOperationState(state.adminQuickUserAdd.teamAddState),
  citizen: state => _getOperationState(state.adminQuickUserAdd.citizenAddState),
  professional: state => _getOperationState(state.adminQuickUserAdd.professionalState),
  error: state => {
    const adminState = state.adminQuickUserAdd;
    const error = adminState.error;
    if (error) {
      let errorType = 'organizations_list_fetch';
      if (adminState.orgAddState < 0) {
        errorType = 'organization_add';
      }
      if (adminState.teamAddState < 0) {
        errorType = 'team_add';
      } else if (adminState.citizenAddState < 0) {
        errorType = 'citizen_add';
      } else if (adminState.professionalState < 0) {
        errorType = 'professional_add';
      }
      return {
        errorType,
        error
      };
    } else {
      return error;
    }
  },
  loading: state => state.adminQuickUserAdd.loading,
  lastUpdate: state => state.adminQuickUserAdd.lastUpdate
});

export const quickUserAddActionCreators = ActionCreators;

export default connect(
  quickUserAddSelector,
  ActionCreators
);
