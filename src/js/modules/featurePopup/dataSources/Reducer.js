import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import { TOGGLE_DIALOG } from './Actions';

const mutators = {
  [TOGGLE_DIALOG]: {
    isDialogOpen: action => action.value
  }
};

export default reduceWith(mutators, DefaultState);
