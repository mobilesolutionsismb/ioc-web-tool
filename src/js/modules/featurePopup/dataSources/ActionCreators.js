import { TOGGLE_DIALOG } from './Actions';

export function toggleDialog(value) {
  return {
    type: TOGGLE_DIALOG,
    value
  };
}
