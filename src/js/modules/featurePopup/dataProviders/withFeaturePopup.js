import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const isDialogOpen = state => state.featurePopup.isDialogOpen;

const select = createStructuredSelector({
  isDialogOpen
});

export default connect(select, ActionCreators);
