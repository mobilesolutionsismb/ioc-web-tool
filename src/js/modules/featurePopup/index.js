import Reducer from './dataSources/Reducer';
import withFeaturePopup from './dataProviders/withFeaturePopup';

export { withFeaturePopup, Reducer };
export { toggleDialog } from './dataSources/ActionCreators';

export default Reducer;
