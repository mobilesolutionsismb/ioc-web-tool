import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const errorSelector = state => {
  return state.ui.errors.length > 0 ? state.ui.errors[0] : null;
};

const messagesSelector = state => {
  let hasErrors = state.ui.errors.length > 0; //errors have priority
  return state.ui.messages.length > 0 && !hasErrors ? state.ui.messages[0] : null;
};

const select = createStructuredSelector({
  error: errorSelector,
  message: messagesSelector
});

export { select as uiSelector, ActionCreators as uiActionCreators };

export default connect(
  select,
  ActionCreators
);
