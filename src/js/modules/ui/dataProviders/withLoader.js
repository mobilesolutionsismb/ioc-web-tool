import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { loadingStart, loadingStop } from '../dataSources/ActionCreators';

const loadingMapState = state => ({
  status: state.ui.loading > 0
});

export const loadingSelector = createStructuredSelector({
  loading: loadingMapState
});

export const loadingActionCreators = { loadingStart, loadingStop };

export default connect(
  loadingSelector,
  loadingActionCreators
);
