import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const iReactTasksSelector = state => state.layersWidget.iReactTasks;

const select = createStructuredSelector({
  iReactTasks: iReactTasksSelector
});

export default connect(select, ActionCreators);
