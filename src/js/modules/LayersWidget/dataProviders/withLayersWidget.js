import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const openLayersWidgetSelector = state => state.layersWidget.layersWidgetOpen;

const select = createStructuredSelector({
  isLayersWidgetOpen: openLayersWidgetSelector
});

export default connect(select, ActionCreators);
