import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const layerWidgetSelector = state => {
  return makeArrayOfObjects(state.layersWidget.layerWidget);
};

function makeArrayOfObjects(layers) {
  layers = layers.map((a, i) => {
    const array = a.split('.');
    return toObject(array, 0);

    function toObject(obj, length) {
      let object = {};
      if (length < obj.length - 1) object[obj[length]] = toObject(obj, length + 1);
      else object = { name: `${obj[length]}` };
      return object;
    }
  });
  return layers;
}

const select = createStructuredSelector({
  layerWidget: layerWidgetSelector
});

export default connect(select, ActionCreators);
