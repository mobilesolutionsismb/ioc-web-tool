import Reducer from './dataSources/Reducer';
import withLayersWidget from './dataProviders/withLayersWidget';
import withLayersSelected from './dataProviders/withLayersSelected';
import withIReactTasks from './dataProviders/withIReactTasks';

export { withLayersWidget, withLayersSelected, withIReactTasks, Reducer };

export default Reducer;
