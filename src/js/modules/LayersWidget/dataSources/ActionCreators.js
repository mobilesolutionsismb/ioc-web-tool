import { OPEN_LAYERS_WIDGET, CLOSE_LAYERS_WIDGET, ADD_LAYER, REMOVE_LAYER } from './Actions';

function setOpenLayersWidget(value) {
  return {
    type: value === true ? OPEN_LAYERS_WIDGET : CLOSE_LAYERS_WIDGET
  };
}

function addLayer(layerName, iReactTasks) {
  return {
    type: ADD_LAYER,
    layerName,
    iReactTasks
  };
}

function removeLayer(layerName, iReactTasks) {
  return {
    type: REMOVE_LAYER,
    layerName,
    iReactTasks
  };
}

export { setOpenLayersWidget, addLayer, removeLayer };
