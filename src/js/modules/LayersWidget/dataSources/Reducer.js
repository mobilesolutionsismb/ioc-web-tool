import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import { OPEN_LAYERS_WIDGET, CLOSE_LAYERS_WIDGET, ADD_LAYER, REMOVE_LAYER } from './Actions';

const mutators = {
  [OPEN_LAYERS_WIDGET]: {
    layersWidgetOpen: true
  },
  [CLOSE_LAYERS_WIDGET]: {
    layersWidgetOpen: false
  },
  [ADD_LAYER]: (state, action) => {
    const s = new Set(state.layerWidget);
    s.add(action.layerName); //if already in, won't get added twice

    let tasks = state.iReactTasks ? state.iReactTasks : [];
    tasks = action.iReactTasks ? action.iReactTasks.concat(tasks) : tasks;

    return {
      ...state,
      layerWidget: Array.from(s),
      iReactTasks: tasks
    };
  },
  [REMOVE_LAYER]: (state, action) => {
    const s = new Set(state.layerWidget);
    s.delete(action.layerName); //returns false if no key such as action.layerName was found

    const iReactTasks = new Set(state.iReactTasks);
    if (action.iReactTasks) {
      action.iReactTasks.forEach(task => iReactTasks.delete(task));
    }

    return {
      ...state,
      layerWidget: Array.from(s),
      iReactTasks: Array.from(iReactTasks)
    };
  }
};

export default reduceWith(mutators, DefaultState);
