const NS = 'LAYERSWIDGET';

const OPEN_LAYERS_WIDGET = `${NS}@OPEN_LAYERS_WIDGET`;
const CLOSE_LAYERS_WIDGET = `${NS}@CLOSE_LAYERS_WIDGET`;

const ADD_LAYER = `${NS}@ADD_LAYER`; // in the reducer: push action.layerName into state[to]
const REMOVE_LAYER = `${NS}@REMOVE_LAYER`; // in the reducer: splice action.layerName from state[to]

export { OPEN_LAYERS_WIDGET, CLOSE_LAYERS_WIDGET, ADD_LAYER, REMOVE_LAYER };
