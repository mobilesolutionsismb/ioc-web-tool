export const EM_COMM_TYPE = {
  alert: '_emcomm_alert',
  warning: '_emcomm_warning'
};

export const EM_COMM_LEVEL = {
  bePrepared: '_emcomm_bePrepared',
  actionRequired: '_emcomm_actionRequired',
  dangerToLife: '_emcomm_dangerToLife'
};

export const CAP_STATUS = {
  actual: '_capStatus_actual',
  draft: '_capStatus_draft',
  exercise: '_capStatus_exercise',
  system: '_capStatus_system',
  test: '_capStatus_test'
};

export const CAP_URGENCY = {
  expected: '_capUrgency_expected',
  future: '_capUrgency_future',
  immediate: '_capUrgency_immediate',
  past: '_capUrgency_past',
  unknown: '_capUrgency_unknown'
};

export const CAP_WARNING_LEVEL = {
  minor: '_capSeverity_minor',
  moderate: '_capSeverity_moderate',
  severe: '_capSeverity_severe',
  extreme: '_capSeverity_extreme',
  unknown: '_capSeverity_unknown'
};

export const CAP_CATEGORY = {
  cBRNE: '_capUrgency_cbrne',
  env: '_capUrgency_env',
  fire: '_capUrgency_fire',
  geo: '_capUrgency_geo',
  health: '_capUrgency_health',
  infra: '_capUrgency_infra',
  met: '_capUrgency_met',
  other: '_capUrgency_other',
  rescue: '_capUrgency_rescue',
  safety: '_capUrgency_safety',
  security: '_capUrgency_security',
  transport: '_capUrgency_transport'
};

export const CAP_CERTAINTY = {
  alert: {
    unknown: '_capCertainty_unknown',
    unlikely: '_capCertainty_unlikely',
    possible: '_capCertainty_possible',
    likely: '_capCertainty_likely'
  },
  warning: {
    observed: '_capCertainty_observed'
  }
};

export const CAP_RESPONSE_TYPE = {
  allClear: '_capResponseType_allClear',
  assess: '_capResponseType_assess',
  avoid: '_capResponseType_avoid',
  evacuate: '_capResponseType_evacuate',
  execute: '_capResponseType_execute',
  monitor: '_capResponseType_monitor',
  none: '_capResponseType_none',
  prepare: '_capResponseType_prepare',
  shelter: '_capResponseType_shelter'
};

export const HAZARD_CAP_CATEGORY_MAPPING = {
  flood: 'met',
  fire: 'fire',
  extremeWeather: 'met',
  landslide: 'geo',
  avalanches: 'geo',
  earthquake: 'geo'
};
/*From label to backend */
export const HAZARD_MAPPING = {
  _ech_earthquake: 'Earthquake',
  _ech_tsunami: 'Tsunami',
  _ech_rock_fall: 'RockFall',
  _ech_landslide: 'Landslide',
  _ech_ash_fall: 'AshFall',
  _ech_lahar: 'Lahar',
  _ech_pyroclastic_flow: 'PyroclasticFlow',
  _ech_lava_flow: 'LavaFlow',
  _ech_extreme_weather: 'ExtremeWeather',
  _ech_fog: 'Fog',
  _ech_storm: 'Storm',
  _ech_wave_action: 'WaveAction',
  _ech_drought: 'Drought',
  _ech_glacial_lake_outburst: 'GlacialLakeOutburst',
  _ech_wildfire: 'Wildfire',
  _ech_epidemic: 'Epidemic',
  _ech_insect_infestation: 'InsectInfestation',
  _ech_animal_accident: 'AnimalAccident',
  _ech_extraterrestrial_impact: 'ExtraterrestrialImpact',
  _ech_space_weather: 'SpaceWeather',
  _ech_chemical_spill: 'ChemicalSpill',
  _ech_industrial_collapse: 'IndustrialCollapse',
  _ech_industrial_explosion: 'IndustrialExplosion',
  _ech_industrial_fire: 'IndustrialFire',
  _ech_gas_leak: 'GasLeak',
  _ech_poisoning: 'Poisoning',
  _ech_radiation: 'Radiation',
  _ech_oil_spill: 'OilSpill',
  _ech_industrial_accident: 'IndustrialAccident',
  _ech_air_accident: 'AirAccident',
  _ech_road_accident: 'RoadAccident',
  _ech_traffic: 'Traffic',
  _ech_rail_accident: 'RailAccident',
  _ech_water_accident: 'WaterAccident',
  _ech_collapse: 'Collapse',
  _ech_explosion: 'Explosion',
  _ech_fire: 'Fire',
  _ech_other: 'Other',
  _ech_extra_tropical_storm: 'ExtraTropicalStorm',
  _ech_tropical_storm: 'TropicalStorm',
  _ech_der_echo: 'Derecho',
  _ech_hail: 'Hail',
  _ech_thunderstorms: 'Thunderstorms',
  _ech_rain: 'Rain',
  _ech_tornado: 'Tornado',
  _ech_dust_storm: 'DustStorm',
  _ech_blizzard: 'Blizzard',
  _ech_storm_surge: 'StormSurge',
  _ech_wind: 'Wind',
  _ech_cold_wave: 'ColdWave',
  _ech_heat_wave: 'HeatWave',
  _ech_snow: 'Snow',
  _ech_ice: 'Ice',
  _ech_frost: 'Frost',
  _ech_coastal_event: 'CoastalEvent',
  _ech_coastal_erosion: 'CoastalErosion',
  _ech_flood: 'Flood',
  _ech_rain_flood: 'RainFlood',
  _ech_coastal_flood: 'CoastalFlood',
  _ech_riverine_flood: 'RiverineFlood',
  _ech_flash_flood: 'FlashFlood',
  _ech_ice_jam_flood: 'IceJamFlood',
  _ech_avalanches: 'Avalanches',
  _ech_debris: 'Debris',
  _ech_mudflow: 'Mudflow',
  _ech_forest_fire: 'ForestFire',
  _ech_land_fire: 'LandFire',
  _ech_pasture_fire: 'PastureFire',
  _ech_viral_disease: 'ViralDisease',
  _ech_bacterial_disease: 'BacterialDisease',
  _ech_parasitic_disease: 'ParasiticDisease',
  _ech_fungal_disease: 'FungalDisease',
  _ech_prion_disease: 'PrionDisease',
  _ech_grasshoper: 'Grasshoper',
  _ech_locust: 'Locust',
  _ech_airburst: 'Airburst',
  _ech_energetic_particles: 'EnergeticParticles',
  _ech_geomagnetic_storm: 'GeomagneticStorm',
  _ech_shockwave: 'Shockwave',
  _ech_none: 'None'
};

export function getTranslationByhazardName(val) {
  return Object.keys(HAZARD_MAPPING).find(
    key => HAZARD_MAPPING[key].toLowerCase() === val.toLowerCase()
  );
}
