const STATE = {
  newEmComm: {
    startDate: null,
    startTime: null,
    endDate: null,
    endTime: null,
    type: '',
    level: { value: '', label: '' },
    hazard: { value: '', label: '' },
    capCategory: { value: '', label: '' },
    capStatus: { value: '', label: '' },
    warningLevel: { value: '', label: '' },
    capCertainty: { value: '', label: '' },
    capUrgency: { value: '', label: '' },
    capResponseType: { value: '', label: '' },
    id: 0,
    address: '',
    description: '',
    title: '',
    instruction: '',
    receivers: [],
    web: '',
    visibility: ''
  }
};

export default STATE;
