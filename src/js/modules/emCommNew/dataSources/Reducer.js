import reduceWith from 'js/startup/reduceWith';

import DefaultState from './DefaultState';
import {
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_END,
  RESET_ALL,
  SET_TYPE,
  SET_LEVEL,
  SET_HAZARD,
  SET_CAP_CATEGORY,
  SET_CAP_STATUS,
  SET_CAP_WARNING_LEVEL,
  SET_CAP_CERTAINTY,
  SET_DESCRIPTION,
  SET_TITLE,
  SET_WEB,
  SET_VISIBILITY,
  SET_CAP_URGENCY,
  SET_CAP_RESPONSE_TYPE,
  SET_INSTRUCTION,
  UPDATE_RECEIVERS,
  REMOVE_RECEIVER,
  FILL_EM_COMM
} from './Actions';

const mutators = {
  [SET_START_DATE]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.startDate = action.startDate;
      return newEmComm;
    }
  },
  [SET_START_TIME]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.startTime = action.startTime;
      return newEmComm;
    }
  },
  [SET_END_DATE]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.endDate = action.endDate;
      return newEmComm;
    }
  },
  [SET_END_TIME]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.endTime = action.endTime;
      return newEmComm;
    }
  },
  [SET_TYPE]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.type = action.emCommType;
      return newEmComm;
    }
  },
  [SET_LEVEL]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.level = action.level;
      return newEmComm;
    }
  },
  [SET_CAP_STATUS]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.capStatus = action.capStatus;
      return newEmComm;
    }
  },
  [SET_CAP_WARNING_LEVEL]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.warningLevel = action.warningLevel;
      return newEmComm;
    }
  },
  [SET_CAP_CERTAINTY]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.capCertainty = action.capCertainty;
      return newEmComm;
    }
  },
  [SET_CAP_URGENCY]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.capUrgency = action.capUrgency;
      return newEmComm;
    }
  },
  [SET_CAP_RESPONSE_TYPE]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.capResponseType = action.capResponseType;
      return newEmComm;
    }
  },
  [SET_HAZARD]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.hazard = action.hazard;
      return newEmComm;
    }
  },
  [SET_CAP_CATEGORY]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.capCategory = action.capCategory;
      return newEmComm;
    }
  },
  [SET_DESCRIPTION]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.description = action.description;
      return newEmComm;
    }
  },
  [SET_TITLE]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.title = action.title;
      return newEmComm;
    }
  },
  [SET_WEB]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.web = action.web;
      return newEmComm;
    }
  },
  [SET_VISIBILITY]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.visibility = action.visibility;
      return newEmComm;
    }
  },
  [SET_INSTRUCTION]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.instruction = action.instruction;
      return newEmComm;
    }
  },
  [UPDATE_RECEIVERS]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.receivers = [...action.receivers];
      return newEmComm;
    }
  },
  [REMOVE_RECEIVER]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.receivers = newEmComm.receivers.filter(item => item.value !== action.receiver);
      return newEmComm;
    }
  },
  [RESET_END]: {
    newEmComm: (action, state) => {
      let newEmComm = state.newEmComm;
      newEmComm.endTime = null;
      newEmComm.endDate = null;
      return newEmComm;
    }
  },
  [FILL_EM_COMM]: {
    newEmComm: (action, state) => {
      const emComm = action.emComm;
      let newEmComm = {
        startDate: new Date(emComm.start),
        startTime: new Date(emComm.start),
        endDate: emComm.end ? new Date(emComm.end) : undefined,
        endTime: emComm.end ? new Date(emComm.end) : undefined,
        targetAreaOfInterest: emComm.targetAreaOfInterest,
        targetLocation: emComm.targetLocation,
        id: emComm.id,
        address: emComm.address,
        targetAddress: emComm.targetAddress,
        type: emComm.type,
        level:
          emComm.type === 'warning'
            ? { value: emComm.level, label: '_emcomm_' + emComm.level }
            : { value: '', label: '' },
        hazard: { value: '_ech_' + emComm.hazard, label: '_ech_' + emComm.hazard },
        description: emComm.description,
        receivers: emComm.receivers.map(item => {
          return { value: item.id, label: item.name };
        }),
        web: emComm.web,
        visibility: emComm.visibility,
        capCategory: { value: '_ecc_' + emComm.capCategory, label: emComm.capCategory },
        capCertainty: { value: emComm.capCertainty, label: '_capCertainty_' + emComm.capCertainty },
        capResponseType: emComm.capResponseType
          ? {
              value: emComm.capResponseType,
              label: '_capResponseType_' + emComm.capResponseType
            }
          : { value: '', label: '' },
        capStatus: { value: emComm.capStatus, label: '_capStatus_' + emComm.capStatus },
        capUrgency: { value: emComm.capUrgency, label: '_capUrgency_' + emComm.capUrgency },
        instruction: emComm.instruction,
        itemType: emComm.itemType,
        warningLevel: { value: emComm.warningLevel, label: '_capSeverity_' + emComm.warningLevel },
        title: emComm.title
      };
      return newEmComm;
    }
  },
  //#endregion
  [RESET_ALL]: {
    newEmComm: (action, state) => {
      let newEmComm = {
        startDate: null,
        startTime: null,
        endDate: null,
        endTime: null,
        type: '',
        level: { value: '', label: '' },
        hazard: { value: '', label: '' },
        capStatus: { value: '', label: '' },
        warningLevel: { value: '', label: '' },
        capCertainty: { value: '', label: '' },
        capUrgency: { value: '', label: '' },
        capResponseType: { value: '', label: '' },
        receivers: [],
        id: 0,
        address: '',
        description: '',
        instruction: '',
        title: '',
        web: '',
        visibility: ''
      };
      return newEmComm;
    }
  }
};

export default reduceWith(mutators, DefaultState);
