import {
  SET_START_DATE,
  SET_START_TIME,
  SET_END_DATE,
  SET_END_TIME,
  RESET_END,
  RESET_ALL,
  SET_TYPE,
  SET_LEVEL,
  SET_HAZARD,
  SET_CAP_CATEGORY,
  SET_CAP_STATUS,
  SET_CAP_WARNING_LEVEL,
  SET_CAP_CERTAINTY,
  SET_CAP_URGENCY,
  SET_DESCRIPTION,
  SET_TITLE,
  SET_WEB,
  SET_VISIBILITY,
  SET_CAP_RESPONSE_TYPE,
  SET_INSTRUCTION,
  UPDATE_RECEIVERS,
  REMOVE_RECEIVER,
  FILL_EM_COMM
} from './Actions';

function setEmCommStartDate(startDate) {
  return {
    type: SET_START_DATE,
    startDate
  };
}

function setEmCommStartTime(startTime) {
  return {
    type: SET_START_TIME,
    startTime
  };
}

function setEmCommEndDate(endDate) {
  return {
    type: SET_END_DATE,
    endDate
  };
}

function setEmCommEndTime(endTime) {
  return {
    type: SET_END_TIME,
    endTime
  };
}

function resetEmCommEnd() {
  return {
    type: RESET_END
  };
}

function resetEmComm() {
  return {
    type: RESET_ALL
  };
}

function setEmCommType(emCommType) {
  return {
    type: SET_TYPE,
    emCommType
  };
}

function setEmCommLevel(level) {
  return {
    type: SET_LEVEL,
    level
  };
}

function setEmCommCapStatus(capStatus) {
  return {
    type: SET_CAP_STATUS,
    capStatus
  };
}

function setEmCommCapWarningLevel(warningLevel) {
  return {
    type: SET_CAP_WARNING_LEVEL,
    warningLevel
  };
}

function setEmCommCapCertainty(capCertainty) {
  return {
    type: SET_CAP_CERTAINTY,
    capCertainty
  };
}

function setEmCommCapUrgency(capUrgency) {
  return {
    type: SET_CAP_URGENCY,
    capUrgency
  };
}

function setEmCommCapResponseType(capResponseType) {
  return {
    type: SET_CAP_RESPONSE_TYPE,
    capResponseType
  };
}

function setEmCommHazard(hazard) {
  return {
    type: SET_HAZARD,
    hazard
  };
}
function setCapCategory(capCategory) {
  return {
    type: SET_CAP_CATEGORY,
    capCategory
  };
}

function setEmCommDescription(description) {
  return {
    type: SET_DESCRIPTION,
    description
  };
}

function setEmCommTitle(title) {
  return {
    type: SET_TITLE,
    title
  };
}

function setEmCommWeb(web) {
  return {
    type: SET_WEB,
    web
  };
}

function setEmCommVisibility(visibility) {
  return {
    type: SET_VISIBILITY,
    visibility
  };
}

function setEmCommInstruction(instruction) {
  return {
    type: SET_INSTRUCTION,
    instruction
  };
}

function fillEmComm(emComm) {
  return {
    type: FILL_EM_COMM,
    emComm
  };
}

function updateEmCommReceivers(receivers) {
  return {
    type: UPDATE_RECEIVERS,
    receivers
  };
}

function removeEmCommReceiver(receiver) {
  return {
    type: REMOVE_RECEIVER,
    receiver
  };
}

export {
  setEmCommStartDate,
  setEmCommStartTime,
  setEmCommEndDate,
  setEmCommEndTime,
  resetEmCommEnd,
  resetEmComm,
  setEmCommType,
  setEmCommLevel,
  setEmCommHazard,
  setCapCategory,
  setEmCommCapStatus,
  setEmCommCapWarningLevel,
  setEmCommDescription,
  setEmCommTitle,
  setEmCommWeb,
  setEmCommVisibility,
  setEmCommCapCertainty,
  setEmCommCapUrgency,
  setEmCommInstruction,
  setEmCommCapResponseType,
  updateEmCommReceivers,
  removeEmCommReceiver,
  fillEmComm
};
