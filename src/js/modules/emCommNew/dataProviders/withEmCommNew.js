import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const newEmComm = state => state.emCommNew.newEmComm;

const select = createStructuredSelector({
  newEmComm
});

export default connect(select, ActionCreators);
