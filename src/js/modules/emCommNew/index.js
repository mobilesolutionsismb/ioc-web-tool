import Reducer from './dataSources/Reducer';
import withEmCommNew from './dataProviders/withEmCommNew';

export { withEmCommNew, Reducer };
export {
  resetEmComm,
  resetEmCommEnd,
  setEmCommEndDate,
  setEmCommEndTime,
  setEmCommStartDate,
  setEmCommStartTime,
  setEmCommLevel,
  setEmCommType,
  setEmCommHazard,
  setEmCommCapStatus,
  setEmCommCapWarningLevel,
  setEmCommDescription,
  setEmCommCapCertainty,
  setEmCommCapUrgency,
  setEmCommTitle,
  setEmCommWeb,
  setEmCommVisibility,
  setEmCommCapResponseType,
  setEmCommInstruction,
  updateEmCommReceivers,
  removeEmCommReceiver,
  fillEmComm
} from './dataSources/ActionCreators';

export {
  EM_COMM_TYPE,
  EM_COMM_LEVEL,
  CAP_STATUS,
  CAP_URGENCY,
  CAP_CATEGORY,
  CAP_WARNING_LEVEL,
  CAP_CERTAINTY,
  CAP_RESPONSE_TYPE,
  HAZARD_CAP_CATEGORY_MAPPING,
  HAZARD_MAPPING
} from './dataSources/consts';

export default Reducer;
