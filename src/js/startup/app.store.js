import React from 'react';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Reducer as GeolocationReducer } from 'ioc-geolocation';
import { Reducer as LocaleReducer } from 'ioc-localization';
import { Reducer as MapViewReducer } from 'js/modules/map';
import { frontendModules } from 'ioc-api-interface';
import * as modules from 'js/modules';
import {
  /* apiTransform, */ /* appVersionCheckTransform, */ localeTransform
} from 'js/startup/persistTransforms';
// import { CLEAR_USER_DATA } from '@state/auth/auth.actions';
import { persistStore, persistReducer } from 'redux-persist';
import { makePersistConfig } from 'js/utils/storageUtils';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const localModules = Object.entries(modules)
  .map(e => ({ [e[0]]: e[1].Reducer }))
  .reduce((ob, n) => {
    ob = { ...ob, ...n };
    return ob;
  }, {});

const appReducer = combineReducers({
  geolocation: GeolocationReducer,
  locale: LocaleReducer,
  map: MapViewReducer,
  ...frontendModules,
  ...localModules
});

// https://stackoverflow.com/questions/35622588/how-to-reset-the-state-of-a-redux-store
const rootReducer = (state, action) => {
  if (action.type === 'api:authentication@LOGOUT_SUCCESS') {
    state = undefined;
  }

  return appReducer(state, action);
};

const transforms = [/* appVersionCheckTransform, */ localeTransform /* , preferenceTransform */];

const persistingAPIModules = [
  //   'api_agentLocations',
  'api_authentication',
  'api_categories',
  //   'api_emergencyCommunications',
  //   'api_emergencyEvents',
  'api_enums',
  //   'api_ireactFeatures',
  'api_liveParams',
  'api_layers',
  //   'api_mapRequests',
  //   'api_missions',
  'api_organizationUnits',
  //   'api_reports',
  'api_roles',
  'api_settings'
]; // for now everything, then we'll remove unnecessary things

const whitelist = ['locale', 'geolocation', 'map', 'preferences', ...persistingAPIModules];

const persistConfig = makePersistConfig(whitelist, transforms);

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, composeEnhancers(applyMiddleware(thunk)));

export const StoreContext = React.createContext({ store });

export const getPersistor = onRehydrate => persistStore(store, null, onRehydrate);

export const StoreProvider = ({ store, children }) => (
  <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
);
