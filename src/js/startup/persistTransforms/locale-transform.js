import { createTransform } from 'redux-persist';

// We only want to save the locale

// Called lately (when saving state) several times
function serialize(inboundState, key) {
  console.log('%c serialize LOCALE', 'color: blue; background: white', inboundState, key);

  return inboundState;
}

// Called first (when loading state) at startup
function deserialize(outboundState, key) {
  console.log('%c DEserialize LOCALE', 'color: magenta; background: white', outboundState, key);
  // Remove dictionary so that it gets loaded at startup each time
  outboundState.dictionary = {
    isEmpty: true,
    locale: outboundState.dictionary.locale,
    supportedLanguages: [], //outboundState.dictionary.supportedLanguages,
    translations: {}
  };
  return outboundState;
}

const localeTransform = createTransform(
  // transform state coming from redux on its way to being serialized and stored
  (inboundState, key) => serialize(inboundState, key),
  // transform state coming from storage, on its way to be rehydrated into redux
  (outboundState, key) => deserialize(outboundState, key),
  // configuration options - a subset of the store whitelist
  { whitelist: ['locale'] }
);

export default localeTransform;
export { localeTransform };
