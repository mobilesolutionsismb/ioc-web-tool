import {
  Login,
  Logout,
  ToS,
  // Registration,
  Home,
  // PasswordForgot,
  Settings,
  Social,
  AdminPanel,
  UAVImage,
  ConfirmEmail,
  ResetPassword
  // ResetPassword
} from 'js/components';

export const DEFAULT_PAGE_AUTHENTICATED = '/home/map';
export const DEFAULT_PAGE_UNAUTHENTICATED = '/';

const popCSSTransition = {
  transition: {
    appear: 'slideDownIn-appear',
    appearActive: 'slideDownIn-appear-active',
    enter: 'slideDownIn-enter',
    enterActive: 'slideDownIn-enter-active',
    exit: 'slideUpOut-exit',
    exitActive: 'slideUpOut-exit-active'
  },
  transitionTimeout: 200
};

// Route configuration
const config = [
  {
    path: '/login',
    redirect: DEFAULT_PAGE_UNAUTHENTICATED
  },
  {
    path: DEFAULT_PAGE_UNAUTHENTICATED,
    component: Login,
    ...popCSSTransition,
    requirements: {
      loggedIn: false,
      roles: []
    }
  },
  {
    path: '/privacy',
    component: ToS,
    ...popCSSTransition,
    requirements: {
      loggedIn: false,
      roles: []
    }
  },
  {
    path: '/terms',
    component: ToS,
    ...popCSSTransition,
    requirements: {
      loggedIn: false,
      roles: []
    }
  },
  {
    path: '/confirm-email',
    component: ConfirmEmail,
    ...popCSSTransition,
    requirements: {
      loggedIn: false,
      roles: []
    }
  },
  {
    path: '/reset-password',
    component: ResetPassword,
    ...popCSSTransition,
    requirements: {
      loggedIn: false,
      roles: []
    }
  },
  // {
  //   path: '/registration',
  //   component: Registration,
  //   ...popCSSTransition,
  //   requirements: {
  //     loggedIn: false,
  //     roles: []
  //   }
  // },
  // {
  //   path: '/passwordForgot',
  //   component: PasswordForgot,
  //   ...popCSSTransition,
  //   requirements: {
  //     loggedIn: false,
  //     roles: []
  //   }
  // },
  {
    path: '/logout',
    component: Logout,
    ...popCSSTransition,
    requirements: {
      loggedIn: false,
      roles: []
    }
  },
  {
    path: '/home',
    redirect: '/home/map'
  },
  {
    path: '/home/',
    redirect: '/home/map'
  },
  {
    path: '/home/:activeItem?',
    component: Home,
    transition: 'fade',
    requirements: {
      loggedIn: true,
      roles: ['pro']
    }
  },
  // SOCIAL PANEL
  {
    path: '/social',
    component: Social,
    transition: 'fade',
    requirements: {
      loggedIn: true,
      roles: ['pro']
    }
  },
  // Preferences Panel
  {
    path: '/settings/:panel?/:subpanel?',
    component: Settings,
    requirements: {
      loggedIn: true,
      roles: ['pro']
    }
  },
  // ADMIN
  {
    path: '/admin',
    component: AdminPanel,
    transition: 'fade',
    requirements: {
      loggedIn: true,
      roles: ['admin']
    }
  },
  //UAV image
  {
    path: '/uavReport/:id',
    component: UAVImage,
    transition: 'fade',
    requirements: {
      loggedIn: true,
      roles: ['pro']
    }
  }
];

export default config;
