/**
 * Exception handling with Application Insight
 */
import nop from 'nop';
import { pushError } from 'js/modules/ui';
import { appInfo } from 'js/utils/getAppInfo';

// See https://github.com/Microsoft/ApplicationInsights-JS/blob/master/JavaScript/JavaScriptSDK.Interfaces/Contracts/Generated/SeverityLevel.ts
export const SeverityLevel = {
  Verbose: 0,
  Information: 1,
  Warning: 2,
  Error: 3,
  Critical: 4
};

function initAppInsights() {
  let appInsights = null;
  if (APP_INSIGHTS_INSTR_KEY && IS_PRODUCTION) {
    const params = {
      config: {
        instrumentationKey: APP_INSIGHTS_INSTR_KEY,
        enableDebug: false,
        disableTelemetry: false,
        enableSessionStorageBuffer: true
      }
    };
    const init = new Microsoft.ApplicationInsights.Initialization(params);
    appInsights = init.loadAppInsights();
    const { trackTrace, trackException } = appInsights;
    // https://github.com/Microsoft/ApplicationInsights-JS/blob/master/API-reference.md#trackexception
    // Log an exception you have caught. (Exceptions caught by the browser are also logged.)
    appInsights.trackException = (
      exception,
      handledAt,
      properties,
      measurements,
      severityLevel
    ) => {
      const _properties = { ...appInfo, ...properties };
      return trackException.apply(appInsights, [
        exception,
        handledAt,
        _properties,
        measurements,
        severityLevel
      ]);
    };
    // https://github.com/Microsoft/ApplicationInsights-JS/blob/master/API-reference.md#tracktrace
    // Log a diagnostic event such as entering or leaving a method.
    appInsights.trackTrace = (message, properties, severityLevel) => {
      const _properties = { ...appInfo, ...properties };
      return trackTrace.apply(appInsights, [message, _properties, severityLevel]);
    };
    window.appInsights = appInsights;
  } else {
    appInsights = {
      trackException: nop,
      trackTrace: nop,
      setAuthenticatedUserContext: nop,
      clearAuthenticatedUserContext: nop
      // TODO add other faked methods if needed
    };
  }
  return appInsights;
}

export const appInsights = initAppInsights();

export function globalErrorHandler(
  dispatch,
  messageOrEvent,
  source,
  lineNumber,
  columnNumber,
  error
) {
  const message = [
    'Message: ' + messageOrEvent,
    'Src/URL' + source,
    'Line: ' + lineNumber,
    'Column: ' + columnNumber //,
    //'Error object: ' + JSON.stringify(error)
  ].join(' - ');
  // Here all exception are critical because it means unhandled
  appInsights.trackException(
    error,
    `${source}, ${lineNumber} ${columnNumber}`,
    {},
    {},
    SeverityLevel.Critical
  );
  console.log('%cCatched! ' + message, 'background:darkred; color: whitesmoke', error);
  dispatch(pushError(error));
  return IS_PRODUCTION; //prevents crash
}

export function getReason(event) {
  let reason = 'Unknown reason';
  if (event) {
    if (event.hasOwnProperty('reason') || typeof event.reason !== 'undefined') {
      reason = event.reason;
    } else if (event.hasOwnProperty('details') || typeof event.details !== 'undefined') {
      if (event.details.hasOwnProperty('reason')) {
        reason = event.details.reason;
      } else {
        try {
          reason = JSON.stringify(event.details);
        } catch (ex) {
          console.warn('Cannot determine reason');
        }
      }
    } else {
      try {
        reason = JSON.stringify(event);
      } catch (ex) {
        console.warn('Cannot determine reason');
      }
    }
  }
  return reason;
}

export function globalPromiseErrorHandler(dispatch, event) {
  event.preventDefault();
  //    console.debug('uhp',event);
  //with bluebird it's in event.detail, on chrome native is in event
  const reason = getReason(event);
  const promise = event.promise ? event.promise : event.detail.promise;
  if (promise) {
    promise.catch(ex => {
      //Catched ex - solve promise status
      dispatch(pushError(reason));
      console.error('Unhandled Promise rejection event', event, reason);
      // Here all exception are critical because it means unhandled
      appInsights.trackException(ex, 'globalPromiseErrorHandler', {}, {}, SeverityLevel.Critical);
      return Promise.resolve();
    });
  } else {
    // Here all exception are critical because it means unhandled
    appInsights.trackException(event, 'globalPromiseErrorHandler', {}, {}, SeverityLevel.Critical);
    dispatch(pushError(reason));
    console.error('Unhandled Promise rejection event', event, reason);
  }
}
