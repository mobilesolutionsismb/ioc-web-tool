import React, { Component, lazy, Suspense, useContext } from 'react';
import { MuiThemeProvider, getMuiTheme } from 'material-ui/styles';
import { iReactTheme } from './iReactTheme';
import { ThemeProvider } from 'styled-components';
const Layout = lazy(() => import('js/components/app/Layout'));

const StateContext = {
  store: {}
};

export const StateContextContext = React.createContext(StateContext);

export const theme = getMuiTheme(iReactTheme);
export const ThemeContext = React.createContext({ theme });

export function useTheme() {
  const theme = useContext(ThemeContext);
  return theme && theme.theme ? theme.theme : theme;
}
// The app root component
class Root extends Component {
  state = {
    store: {}
  };

  componentDidMount() {
    this.setState({ store: this.props.store });
    document.querySelector('body').style.backgroundColor = theme.palette.canvasColor;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.store !== this.props.store) {
      this.setState({ store: this.props.store });
    }
  }

  onRouterError = err => {
    console.error('Router error', err);
  };

  render() {
    const { onRouterError, history } = this.props;
    return (
      <MuiThemeProvider muiTheme={theme}>
        <ThemeProvider theme={theme}>
          <StateContextContext.Provider value={this.state}>
            <ThemeContext.Provider value={theme}>
              <Suspense fallback={<div>Loading...</div>}>
                <Layout onRouterError={onRouterError} history={history} />
              </Suspense>
            </ThemeContext.Provider>
          </StateContextContext.Provider>
        </ThemeProvider>
      </MuiThemeProvider>
    );
  }
}

export default Root;
