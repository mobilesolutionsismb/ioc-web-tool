// Suggestion: use http://materialcolorize.com/convert-colors/
import { darkBaseTheme } from 'material-ui/styles';
import {
  black,
  grey400,
  grey900,
  grey800,
  deepOrangeA200,
  deepOrange900,
  // orange800,
  // purpleA400,
  redA700,
  // deepOrange500,
  // red900,
  // yellowA200,
  green800,
  white,
  indigoA700,
  amber50,
  // grey700,
  // purple800,
  // brown200,
  // indigo700,
  // lightBlue600,
  // lime400,
  // lightBlue100,
  // grey500,
  lightGreenA400,
  // indigo200,
  teal300,
  teal500,
  teal400,
  orangeA400,
  purpleA400,
  purple700,
  blue800,
  pink500,
  lightBlueA400,
  deepOrange500,
  yellowA200
} from 'material-ui/styles/colors';

export const OK_OR_COMPLETION_COLOR = lightGreenA400;
export const TWITTER_BLUE = '#00aced';
export const HAZARD_BANNER_COLOR = redA700;

export const ONGOING_EVT_COLOR = pink500;
export const CLOSED_EVT_COLOR = white;

// -------------   V2 colors

export const CLUSTER_CIRCLE_COLOR = amber50;
export const CLUSTER_COUNT_COLOR = black;

export const REPORTS_CLUSTERED_COLOR = teal400;
export const REPORT_REQUESTS_CLUSTERED_COLOR = blue800;
export const ALERTS_OR_WARNINGS_CLUSTERED_COLOR = grey900;
export const MISSIONS_CLUSTERED_COLOR = purple700;
export const AGENT_LOCATIONS_CLUSTERED_COLOR = orangeA400;
export const MAP_REQUESTS_CLUSTERED_COLOR = blue800;

export const CLUSTER_COLORS = {
  circle: CLUSTER_CIRCLE_COLOR,
  count: CLUSTER_COUNT_COLOR,
  // itemType --> clusteredCategory: color
  categories: {
    report: REPORTS_CLUSTERED_COLOR,
    reportRequest: REPORT_REQUESTS_CLUSTERED_COLOR,
    emergencyCommunication: ALERTS_OR_WARNINGS_CLUSTERED_COLOR,
    // alertOrWarning: ALERTS_OR_WARNINGS_CLUSTERED_COLOR,
    mission: MISSIONS_CLUSTERED_COLOR,
    agentLocation: AGENT_LOCATIONS_CLUSTERED_COLOR,
    mapRequest: MAP_REQUESTS_CLUSTERED_COLOR,
    tweet: TWITTER_BLUE
  }
};

// reports
export const REPORTS_STATUS_SUBMITTED_COLOR = teal400;
export const REPORTS_STATUS_VALIDATED_COLOR = teal400;
export const REPORTS_STATUS_INAPPROPRIATE_COLOR = redA700;
export const REPORTS_STATUS_INACCURATE_COLOR = deepOrange500;

export const REPORTS_REPORTER_TYPE_AUTHORITY_COLOR = orangeA400;
export const REPORTS_REPORTER_TYPE_CITIZEN_COLOR = lightBlueA400; // PROVISIONAL!

// Structure: propertyName - value - color
export const REPORT_COLORS = {
  status: {
    submitted: REPORTS_STATUS_SUBMITTED_COLOR,
    validated: REPORTS_STATUS_SUBMITTED_COLOR,
    inappropriate: REPORTS_STATUS_SUBMITTED_COLOR,
    inaccurate: REPORTS_STATUS_SUBMITTED_COLOR
  },
  reporterType: {
    authority: REPORTS_REPORTER_TYPE_AUTHORITY_COLOR,
    citizen: REPORTS_REPORTER_TYPE_CITIZEN_COLOR
  }
};

// emergency communications
export const EMCOMM_LEVEL_REPORT_REQUEST_COLOR = blue800;
export const EMCOMM_FEATURE_COLOR = grey900;
export const EMCOMM_LEVEL_ALERT_COLOR = redA700;
export const EMCOMM_LEVEL_BE_PREPARED_COLOR = yellowA200;
export const EMCOMM_LEVEL_ACTION_REQUIRED_COLOR = deepOrange500;
export const EMCOMM_LEVEL_DANGER_TO_LIFE_COLOR = redA700;

export const EMCOMM_COLORS = {
  //TODO simplify
  level: {
    reportRequest: EMCOMM_LEVEL_REPORT_REQUEST_COLOR,
    alert: EMCOMM_FEATURE_COLOR,
    bePrepared: EMCOMM_FEATURE_COLOR,
    actionRequired: EMCOMM_FEATURE_COLOR,
    dangerToLife: EMCOMM_FEATURE_COLOR
  }
};

export const EMCOMM_REPORT_REQUEST_ONGOING_COLOR = green800;
export const EMCOMM_REPORT_REQUEST_CLOSED_COLOR = 'transparent';

// missions
export const MISSION_COLOR = purple700;
export const MISSION_PROGRESS_COLOR = purpleA400;
export const MISSION_TASK_COLOR = purple700;

export const MISSION_COLORS = {
  temporalStatus: {
    completed: white,
    expired: redA700,
    ongoing: OK_OR_COMPLETION_COLOR,
    open: indigoA700
  }
};

// agent locations
export const AGENTLOCATION_COLOR = orangeA400;

// map requests
export const MAPREQUEST_COLOR = blue800;

// EVENTS
export const SELECTED_EVENT_LAYER_COLOR = white;
export const ACTIVE_EVENT_LAYER_COLOR = white;

export const EMCOMM_SEVERITY_UNKNOWN = grey400;
export const EMCOMM_SEVERITY_MINOR = green800;
export const EMCOMM_SEVERITY_MODERATE = yellowA200;
export const EMCOMM_SEVERITY_SEVERE = deepOrange500;
export const EMCOMM_SEVERITY_EXTREME = redA700;

const emcommSeverity = {
  unknown: EMCOMM_SEVERITY_UNKNOWN,
  minor: EMCOMM_SEVERITY_MINOR,
  moderate: EMCOMM_SEVERITY_MODERATE,
  severe: EMCOMM_SEVERITY_SEVERE,
  extreme: EMCOMM_SEVERITY_EXTREME
};

export const ireactPalette = {
  palette: {
    ...darkBaseTheme.palette,
    primary1Color: teal300,
    primary2Color: teal500,
    // primary3Color: grey600,
    accent1Color: deepOrangeA200,
    // accent2Color: grey100,
    // accent1Color: pinkA200,
    // accent2Color: pinkA400,
    // accent3Color: deepOrangeA200,
    accent2Color: deepOrangeA200,
    accent3Color: deepOrangeA200, //deepOrangeA200,
    accent4Color: deepOrange900,
    // CUSTOM KEYS
    appBarColor: grey900,
    tabsColor: grey800,
    backgroundColor: black,
    liveModeDisabledColor: grey400,
    // CUSTOMS
    // User types
    authorityColor: REPORTS_REPORTER_TYPE_AUTHORITY_COLOR,
    citizensColor: REPORTS_REPORTER_TYPE_CITIZEN_COLOR,
    // Mission status
    completedOrAccomplishedColor: OK_OR_COMPLETION_COLOR,
    // Validation statuses
    validatedColor: REPORTS_STATUS_VALIDATED_COLOR,
    submittedColor: REPORTS_STATUS_VALIDATED_COLOR,
    rejectedColor: REPORTS_STATUS_INAPPROPRIATE_COLOR,
    inaccurateColor: REPORTS_STATUS_INACCURATE_COLOR,
    // Hazard name banner (stripe)
    hazardBannerColor: HAZARD_BANNER_COLOR,
    // Emergency events
    closedEventsColor: CLOSED_EVT_COLOR,
    ongoingEventsColor: ONGOING_EVT_COLOR,
    // Alerts and Warnings
    bePreparedEmCommColor: EMCOMM_LEVEL_BE_PREPARED_COLOR,
    actionRequiredEmCommColor: EMCOMM_LEVEL_ACTION_REQUIRED_COLOR,
    dangerToLifeEmCommColor: EMCOMM_LEVEL_DANGER_TO_LIFE_COLOR,
    reportRequestEmCommColor: EMCOMM_LEVEL_REPORT_REQUEST_COLOR,
    // Unknown??
    ongoingReportRequestColor: EMCOMM_REPORT_REQUEST_ONGOING_COLOR,
    closedReportRequestColor: EMCOMM_LEVEL_DANGER_TO_LIFE_COLOR,
    // Missions/Tasks
    missionColor: MISSION_COLOR,
    missionProgressBarColor: MISSION_PROGRESS_COLOR,
    // agentLocation
    agentLocation: AGENTLOCATION_COLOR,
    // Map Request
    mapRequest: MAPREQUEST_COLOR,
    emcommSeverity
  },
  menuItem: {
    selectedTextColor: teal300 // Qnhu is happy now
  }
};
const iReactTheme = {
  ...darkBaseTheme,
  ...ireactPalette
};

export { iReactTheme };
export default iReactTheme;
