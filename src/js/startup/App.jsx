import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { checkStorageVersion } from 'js/utils/storageUtils';

import { PersistGate } from 'redux-persist/es/integration/react';

import { store as appStore, getPersistor as getAppPersistor, StoreProvider } from './app.store';
import { logMain } from 'js/utils/log';
/**
 * @param {Object} appInfo - dictionary with info about the app
 * @param {Function} onInit - steps to do before init, e.g. register reducers
 * @param {Function} onReady - steps to do before rendering dom - see https://github.com/rt2zz/redux-persist/issues/517#issuecomment-348632092
 */
const DEFAULT_CONFIG = {
  appInfo: {},
  onInit: null,
  onReady: null
};

class AppInnerComponent extends Component {
  state = {
    appError: null
  };

  componentDidCatch(error, info) {
    this.setState({ appError: { error, info } });
  }
  _resetError = () => {
    this.setState({ appError: null }, () => window.location.reload());
  };

  render() {
    const {
      store,
      onBeforeLift,
      persistor,
      historyEngine,
      onRouterError,
      LoadingComponent,
      RootComponent
    } = this.props;
    return (
      <Provider store={store}>
        <PersistGate
          loading={<LoadingComponent />}
          onBeforeLift={onBeforeLift}
          persistor={persistor}
        >
          <StoreProvider store={store}>
            <RootComponent
              store={store}
              onRouterError={onRouterError}
              history={historyEngine}
              appError={this.state.appError}
            />
          </StoreProvider>
        </PersistGate>
      </Provider>
    );
  }
}

const AppInner = hot(module)(AppInnerComponent);

export class App {
  /**
   * Creates an instance of Application.
   * @param {Object} config - defaults to DEFAULT_CONFIG - see that for parameters explanation
   * @memberOf Application
   */
  constructor(config) {
    const _config = { ...DEFAULT_CONFIG, ...config };
    const { appInfo, onInit, onReady } = _config;
    const _store = appStore;
    // const _persistor = appPersistor;
    this._persistor = null;

    // const _persistor = persistStore(_store);
    Object.defineProperties(this, {
      store: {
        enumerable: true,
        get: () => _store
      }
      // persistor: {
      //   enumerable: true,
      //   get: () => _persistor
      // }
    });

    this.info = { title: 'ismb-core-app', ...appInfo };
    if (!this._render) {
      throw new Error('onRenderCb must be a function');
    }

    this._onBeforeLift =
      typeof onReady === 'function'
        ? () => {
            return onReady(this);
          }
        : undefined;

    if (typeof onInit === 'function') {
      this._init = () =>
        new Promise((resolve, reject) => {
          onInit(this, resolve, reject);
        });
    }
  }

  /**
   * Starts the application on the specified HTML node
   *
   * @param {string|HTMLElement} selectorOrElement the HTML element selector
   * @param {node} RootComponent the main node of your app implementing all the mumbo jumbo
   * @param {node} LoadingComponent showed while loading
   * @memberOf Application
   */
  async start(selectorOrElement, RootComponent, LoadingComponent) {
    const root =
      selectorOrElement instanceof HTMLElement
        ? selectorOrElement
        : document.querySelector(selectorOrElement);
    if (!root) {
      throw new Error(`Cannot find node corresponding to selector: ${selectorOrElement}!`);
    }
    root.classList.add('app');
    if (
      !(
        React.isValidElement(RootComponent) ||
        RootComponent instanceof Function ||
        RootComponent instanceof React.Component
      )
    ) {
      throw new Error(`Root component ${RootComponent} is not a valid React node!`);
    }

    if (typeof this._init === 'function') {
      await this._init();
    }

    this._persistor = getAppPersistor(() => {
      logMain('Store rehydrated');
      this._onStateRestore(root, RootComponent, LoadingComponent);
    });
  }

  /**
   *
   */
  _onStateRestore = async (rootElement, RootComponent, LoadingComponent) => {
    await checkStorageVersion(this._persistor);
    const renderCb = this._render.bind(this, rootElement, RootComponent, LoadingComponent);

    renderCb();
  };

  /**
   * @param {HTMLElement} element
   * @param {node} RootComponent
   * @param {node} LoadingComponent
   * @param {*} onBeforeLift
   * @param {*} persistor
   */
  _render(rootElement, RootComponent, LoadingComponent) {
    const baseName = PUBLIC_PATH;
    const createHistory = createBrowserHistory;
    const historyEngine = createHistory({
      basename: baseName
    });
    ReactDOM.render(
      <AppInner
        store={this.store}
        onBeforeLift={this._onBeforeLift}
        persistor={this._persistor}
        historyEngine={historyEngine}
        onRouterError={this.onRouterError}
        LoadingComponent={LoadingComponent}
        RootComponent={RootComponent}
      />,
      rootElement
    );
  }
}

export default App;
