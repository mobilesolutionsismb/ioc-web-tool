import 'material-design-icons-iconfont/dist/material-design-icons.css';
// import 'mapbox-gl/dist/mapbox-gl.css';
import 'style/style.scss';
import { API } from 'ioc-api-interface';
import React from 'react';
import { App } from 'js/startup/App';
import { Root } from 'js/startup';
import { globalErrorHandler, globalPromiseErrorHandler } from 'js/startup/errorHandlingAndTracking';
import { pushError, pushMessage /* , pushModalMessage, popModalMessage */ } from 'js/modules/ui';

import { loadDictionary } from 'js/utils/getAssets';
import { setConfig } from 'react-hot-loader';
import { logMain } from 'js/utils/log';
import { ireactPalette } from './iReactTheme';
setConfig({ logLevel: 'error', ignoreSFC: true, pureRender: true });

const appInfo = {
  title: TITLE,
  packageName: PKG_NAME,
  description: DESCRIPTION,
  version: VERSION,
  buildDate: BUILD_DATE,
  environment: ENVIRONMENT
};

console.debug('%c APP INFO', 'color: white; background: dodgerblue;', appInfo);

/**
 * init the app
 * @param {App} app
 * @param {Function} done
 * @param {Function} fail
 */
async function onInit(app, done, fail) {
  try {
    // await checkStoreAndAppVersion(app);
    logMain(`Application '${appInfo.title}' is starting...`);
    done();
  } catch (err) {
    fail(err);
  }
}

function initApiAndStart(app) {
  //WARN - should be moved BEFORE this could need a polyfill on non-chrome browswers
  window.addEventListener(
    'unhandledrejection',
    globalPromiseErrorHandler.bind(null, app.store.dispatch)
  );
  window.onerror = globalErrorHandler.bind(null, app.store.dispatch);
  const api = API.getInstance();
  api.reduxStore = app.store;
  if (ENVIRONMENT.startsWith('debug') || ENVIRONMENT.startsWith('dev')) {
    app.pushError = (...params) => {
      app.store.dispatch(pushError(...params));
    };
    app.pushMessage = (...params) => {
      app.store.dispatch(pushMessage(...params));
    };
    app.api = api;
    if (!app.api) {
      const errMsg = 'I-REACT api not initialized!';
      console.warn(errMsg);
    }
    window.app = app;
  }
  window.Pace.stop();
  delete window.Pace;
}

/**
 * Run operations before rendering React DOM
 * Now integrated in redux persist
 * @see https://github.com/rt2zz/redux-persist/issues/517#issuecomment-348632092
 * @param {App} app
 * @returns {Promise<Boolean>}
 */
async function onReady(app) {
  let ready = false;
  const state = app.store.getState();
  const dispatch = app.store.dispatch;

  const localeState = state.locale;

  try {
    const { locale, translationLoader } = await loadDictionary(localeState.locale);
    translationLoader(translations => {
      console.debug('Locale loaded...');
      dispatch({
        type: 'LOCALE@LOCALE_CHANGED',
        locale: locale,
        translations
      });
      initApiAndStart(app);
    });
  } catch (err) {
    pushError(err);
    initApiAndStart(app);
  } finally {
    ready = true;
  }
  return ready;
}

// Loading is shown before store is loaded from IndexedDB (see https://github.com/rt2zz/redux-persist/blob/master/docs/PersistGate.md)
const Loading = () => (
  <div
    style={{
      width: 'vw',
      height: '100vh',
      color: ireactPalette.palette.textColor,
      fontSize: 48,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: ireactPalette.palette.canvasColor
    }}
  >
    Loading...
  </div>
);

export default function launch() {
  const app = new App({
    appInfo,
    onInit,
    onReady
  });

  app.start(`#${APP_MOUNT_ID}`, Root, Loading);
}
