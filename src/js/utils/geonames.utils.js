import axios from 'axios';
import nop from 'nop';

/**
 * @typedef {Object} Baseparams
 * @property {boolean} formatted
 * @property {string} lan localization
 * @property {string} style style
 * @property {string} username username (mandatory)
 */
export const baseParams = {
  formatted: true,
  lan: 'en',
  style: 'full',
  username: 'celi'
};

const baseURL = 'https://secure.geonames.org/';

const geonamesInstance = axios.create({ baseURL });

/**
 * Call geonames.org
 * @param {String} apiName see https://www.geonames.org/export/ws-overview.html
 * @param {Object} params any params fitting apiName
 * @param {Function} onData callback for data
 * @param {Function} onError callback for error
 * @param {Function} onLoad callback for loading state
 * @param {Function} onCancel callback for canceling request
 */
export async function geonames(
  apiName,
  params,
  onData = nop,
  onError = nop,
  onLoad = nop,
  onCancel = nop
) {
  onLoad(true);
  try {
    const response = await geonamesInstance.get(
      `/${apiName}JSON`,
      {
        params: {
          ...baseParams,
          ...params
        }
      },
      {
        // `cancelToken` specifies a cancel token that can be used to cancel the request
        cancelToken: new axios.CancelToken(onCancel)
      }
    );
    onData(response.data);
  } catch (err) {
    onError(err);
  } finally {
    onLoad(false);
  }
}

/**
 * @typedef {Object} FindNearbyPlaceNameParams
 * @extends {Baseparams}
 * @property {Number} lat latitude
 * @property {Number} lng longitude
 */

/**
 * Reverse geocoding
 * @param {FindNearbyPlaceNameParams} params
 * @param {Function} onData callback for data
 * @param {Function} onError callback for error
 * @param {Function} onLoad callback for loading state
 * @param {Function} onCancel callback for canceling request
 */
export function findNearbyPlaceName(params, onData, onError, onLoad, onCancel) {
  return geonames('findNearbyPlaceName', params, onData, onError, onLoad, onCancel);
}

/**
 * @typedef {Object} GeonamesSearchParam
 * @extends {Baseparams}
 * @property {string} q query
 */

/**
 * Geocode
 * @param {GeonamesSearchParam} params
 * @param {Function} onData callback for data
 * @param {Function} onError callback for error
 * @param {Function} onLoad callback for loading state
 * @param {Function} onCancel callback for canceling request
 */
export function search(params, onData, onError, onLoad, onCancel) {
  return geonames('search', params, onData, onError, onLoad, onCancel);
}
