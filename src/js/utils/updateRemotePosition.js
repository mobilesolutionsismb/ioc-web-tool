import distance from '@turf/distance';
import moment from 'moment-timezone';
import centroid from '@turf/centroid';
import { point } from '@turf/helpers';
import { getType } from '@turf/invariant';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { API, ActionCreators } from 'ioc-api-interface';
import { updatePosition } from 'ioc-geolocation';

const DISTANCE_THRESHOLD = 2; //km
const TIME_THRESHOLD = 24; //24 hours

export function areGeolocationsEqual(geolocation1 = null, geolocation2 = null) {
  if (!geolocation1 && !geolocation2) {
    return true;
  } else if ((geolocation1 && !geolocation2) || (!geolocation1 && geolocation2)) {
    return false;
  } else {
    return (
      geolocation1.coords.latitude === geolocation2.coords.latitude &&
      geolocation1.coords.longitude === geolocation2.coords.longitude &&
      geolocation1.coords.accuracy === geolocation2.coords.accuracy &&
      geolocation1.timestamp === geolocation2.timestamp
    );
  }
}

export function getTimeDistance(ts1, ts2 = Date.now(), unit = 'hours') {
  const t1 = ts2 >= ts1 ? ts1 : ts2;
  const t2 = ts2 >= ts1 ? ts2 : ts1;
  return moment(t2).diff(moment(t1), unit);
}

export function pointifyFeature(f) {
  if (typeof f.type !== 'undefined' && getType(f) === 'Point') {
    return f;
  } else {
    return point(f);
  }
}

export function getDistanceInKm(pointFeature1, pointFeature2) {
  return distance(pointifyFeature(pointFeature1), pointifyFeature(pointFeature2), {
    units: 'kilometres'
  });
}

/**
 * Update remote user position
 * @param {Geolocation} geolocation
 */
export async function updateRemotePosition(geolocation) {
  try {
    const api = API.getInstance();
    const { latitude, longitude } = geolocation.coords;
    const result = await api.account.updateUserLocation(`POINT(${longitude} ${latitude})`);
    return result;
  } catch (err) {
    console.error('Error notifying user position', err);
    throw err;
  }
}

// TO BE USED OUTSIDE REACT RENDERING
export async function updateRemoteUserDataIfNeeded(dispatch, getState) {
  try {
    const state = getState();
    const apiState = state.api_authentication;
    if (apiState.logged_in) {
      const storedPosition = state.geolocation.currentPosition;
      const updatedPosition = await updatePosition()(dispatch, getState);
      if (!areGeolocationsEqual(storedPosition, updatedPosition)) {
        const tooOld =
          getTimeDistance(storedPosition.timestamp, updatedPosition.timestamp) > TIME_THRESHOLD;

        const storedAOICentroid = apiState.liveAOI ? centroid(apiState.liveAOI.feature) : null;
        const positionFeature = point([
          storedPosition.coords.longitude,
          storedPosition.coords.latitude
        ]);
        const tooFar = apiState.liveAOI
          ? getDistanceInKm(positionFeature, storedAOICentroid) > DISTANCE_THRESHOLD
          : true;

        if (tooOld || tooFar) {
          await updateRemotePosition(updatedPosition);
          console.log('%c REMOTE geolocationPos', 'color: lightgreen;', updatedPosition);
        }
        if (tooFar) {
          await ActionCreators.setLiveAreaOfInterestFromUserPosition(updatedPosition)(
            dispatch,
            getState
          );
        }
      }
    }
  } catch (err) {
    console.error('Error updating remote user data', err);
    appInsights.trackException(
      err,
      'updateRemotePosition::updateRemoteUserDataIfNeeded',
      {},
      {},
      SeverityLevel.Error
    );
  }
  return Promise.resolve();
}
