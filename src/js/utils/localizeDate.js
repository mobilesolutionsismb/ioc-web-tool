import moment from 'moment-timezone';
const tz = moment.tz.guess();

/**
 * Return the date formatted in the selected locale
 * @param {Date} date the date to be formatted
 * @param {String} locale - string, eg, 'en'
 * @param {String} format - date formatting option, default to 'L LT' (see https://momentjs.com/docs/)
 * @return {String} formatted date
 */
export function localizeDate(date, locale, format = 'L LT') {
  if (locale && moment.locale() !== locale) {
    moment.locale(locale);
  } else if (!locale) {
    try {
      const loc = document.querySelector('html').getAttribute('lang');
      moment.locale(loc);
    } catch (e) {
      console.warn('Cannot determine locale', e);
    }
  }
  return moment(date)
    .tz(tz)
    .format(format);
}

/**
 * Returns a new date (as moment object) according to the params,
 * in the current time zone.
 * If no params is supplied, then the current date is returned
 * @param {Date|String|undefined} date
 * @param {Number} diff difference in 'unit' either in the past or in the future (positive)
 * @param {String} unit 'seconds', 'minutes', 'hours', 'days', ... (see https://momentjs.com/docs/)
 * @return {moment} the date
 */
export function getLocalDate(date, diff, unit = 'days') {
  let m = moment(
    date instanceof Date || typeof date === 'string' || (date && date.date._isValid)
      ? date
      : new Date()
  ).tz(tz);
  if (typeof diff === 'number') {
    m = diff > 0 ? m.add(diff, unit) : m.subtract(-diff, unit);
  }
  return m;
}

export function getDurationFormat(interval) {
  if (interval !== '') {
    var dur = moment.duration(interval);
    if (dur.asMonths() >= 1) {
      if (dur.asMonths() > 1) {
        return moment(dur._data).format('M [months]');
      } else {
        return moment(dur._data).format('M [month]');
      }
    } else if (dur.asDays() >= 1) {
      if (Math.round(dur.asDays()) === dur.asDays()) {
        if (dur.asDays() > 1) {
          return moment(dur._data).format('D [days]');
        } else {
          return moment(dur._data).format('D [day]');
        }
      } else {
        if (dur.asDays() >= 2) {
          return moment(dur._data).format('D [days and] H [hours]');
        } else {
          return moment(dur._data).format('D [day and] H [hours]');
        }
      }
    } else {
      return moment(dur._data).format('H [hours]');
    }
  }
  return '';
}

const MAX_GEO_AGING_SECONDS = ENVIRONMENT === 'production' ? 10 * 60 : 5 * 60; //seconds

export function isOldGeolocation(currentPosition, nextPosition = null) {
  const timeDiff = moment(
    typeof nextPosition === 'object' && typeof nextPosition.timestamp === 'number'
      ? new Date(nextPosition.timestamp)
      : undefined
  ).diff(currentPosition.timestamp, 'seconds');
  return timeDiff > MAX_GEO_AGING_SECONDS;
}

export function isFirstDateBeforeSecondDate(firstDate, secondDate) {
  const momentStart = getLocalDate(firstDate);
  const momentEnd = getLocalDate(secondDate);
  return momentStart.isBefore(momentEnd);
}

export function guessDate(possibleDateString, locale, format = 'L LT') {
  return moment(possibleDateString, moment.ISO_8601, true).format() === 'Invalid date'
    ? possibleDateString
    : localizeDate(possibleDateString, locale, format);
}
