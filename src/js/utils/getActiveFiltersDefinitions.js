/**
 * Get filters definitions from set
 * Generate an array of filters (with logical OR) according to Mapbox style filters
 * @see https://www.mapbox.com/mapbox-gl-js/style-spec/#types-filter
 * @param {Array} stateFilters
 * @return filters or undefined
 */
export function getActiveFiltersDefinitions(stateFilters, FilterMap, filterMap) {
  const definitions = ['all'];
  const filterCategories = Object.keys(filterMap);
  for (const filterCategory of filterCategories) {
    if (stateFilters[filterCategory].length > 0) {
      const activeCategoryDefinitions = stateFilters[filterCategory].map(
        filterName => FilterMap[filterCategory][filterName]
      );
      if (activeCategoryDefinitions.length) {
        definitions.push(['any', ...activeCategoryDefinitions]);
      }
    }
  }
  return definitions.length === 1 ? undefined : definitions;
}

export function compareFilterDefinitions(filterDef1, filterDef2) {
  if (filterDef1 !== null && filterDef2 !== null) {
    return filterDef1.length !== filterDef2.length
      ? false // different sizes means they're different
      : JSON.stringify(filterDef1) === JSON.stringify(filterDef2); // they could be different
  } else if (filterDef1 === null && filterDef2 === null) {
    return true;
  } else return false;
}

export function compareAoiFilters(prevCategory, nextCategory, filterPolygon) {
  if (prevCategory === nextCategory || categoryWhiteList.indexOf(nextCategory) === -1) return true;

  if (prevCategory !== nextCategory && nextCategory === 'none') return false;

  return !filterPolygon.id;
}

const categoryWhiteList = [
  'report',
  'reportRequest',
  'emergencyCommunication',
  'none',
  'mapRequest'
];
