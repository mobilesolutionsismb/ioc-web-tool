import moment from 'moment-timezone';
const tz = moment.tz.guess();

export function filterWithTimeWindow(items, start, end, locale) {
  return items.filter(item => {
    const momentStart = _getLocalDate(start);
    const momentEnd = end && end !== null ? _getLocalDate(end) : undefined;
    const momentItemDate = _getLocalDate(item.properties.userCreationTime);
    return momentEnd
      ? momentStart.isBefore(momentItemDate) && momentEnd.isAfter(momentItemDate)
      : momentStart.isBefore(momentItemDate);
  });
}

export function filterIREACTFeaturesWithTimeWindow(items, start, end, locale, itemType) {
  let oldItems = items.filter(item => item.properties.itemType !== itemType);
  let featuresToBeFiltered = items.filter(item => item.properties.itemType === itemType);
  let filteredFeatures = [];

  filteredFeatures = featuresToBeFiltered.filter(item => {
    const momentStart = _getLocalDate(start);
    const momentEnd = end && end !== null ? _getLocalDate(end) : undefined;
    const momentItemDate = _getLocalDate(item.properties.userCreationTime);
    return momentEnd
      ? momentStart.isBefore(momentItemDate) && momentEnd.isAfter(momentItemDate)
      : momentStart.isBefore(momentItemDate);
  });

  return [...oldItems, ...filteredFeatures];
}

export default filterWithTimeWindow;

function _getLocalDate(date, diff, unit = 'days') {
  let m = moment(date instanceof Date || typeof date === 'string' ? date : new Date()).tz(tz);
  if (typeof diff === 'number') {
    m = diff > 0 ? m.add(diff, unit) : m.subtract(-diff, unit);
  }
  return m;
}
