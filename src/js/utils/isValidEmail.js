export const EMAIL_RX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

export function isValidEmail(email) {
  return typeof email === 'string' && EMAIL_RX.test(email);
}

export const PHONE_RX = /^\+?([0-9]{2})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

export function isValidPhone(phone) {
  return typeof phone === 'string' && PHONE_RX.test(phone);
}
