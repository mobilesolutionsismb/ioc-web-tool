export function filterWithMeasures(items, filterMeasures) {
  let result = items.filter(item => {
    return (
      item.properties.measures.filter(itemMeasure => {
        return filterMeasures.map(measure => measure.id).indexOf(itemMeasure.id) >= 0;
      }).length > 0
    );
  });

  return result;
}

export function filterIREACTFeaturesWithActiveReportRequest(
  items,
  filterMeasures,
  filterPeople,
  filterDamages,
  filterResources,
  itemType
) {
  let oldItems = items.filter(item => item.properties.itemType !== itemType);
  let featuresToBeFiltered = items.filter(item => item.properties.itemType === itemType);
  let filteredFeatures = [];

  filteredFeatures = featuresToBeFiltered.filter(item => {
    return (
      item.properties.measures.filter(itemMeasure => {
        return filterMeasures.map(measure => measure.id).indexOf(itemMeasure.id) >= 0;
      }).length > 0 ||
      (item.properties.people && item.properties.people.length > 0 && filterPeople) ||
      (item.properties.resources && item.properties.resources.length > 0 && filterResources) ||
      (item.properties.damages && item.properties.damages.length > 0 && filterDamages)
    );
  });

  return [...oldItems, ...filteredFeatures];
}
