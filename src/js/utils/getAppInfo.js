const rxstart = /(\w|:|\/)+ireactbe-/i;
const rxend = /.azurewebsites.net$/i;

function getBackendEnv(beString) {
  if (rxstart.test(beString)) {
    return beString.replace(rxstart, '').replace(rxend, '');
  } else {
    return beString;
  }
}

/**
 * Information about this app as available from webpack and cordova
 */
const appInfo = {
  title: TITLE,
  packageName: PKG_NAME,
  description: DESCRIPTION,
  version: VERSION,
  buildDate: BUILD_DATE,
  environment: ENVIRONMENT,
  backendEnvironment: getBackendEnv(IREACT_BACKEND),
  geoServerURL: GEOSERVER_URL ? GEOSERVER_URL : 'from API',
  rasterLayersMode: USE_WMTS ? 'WMTS' : 'WMS'
};

export { appInfo };
