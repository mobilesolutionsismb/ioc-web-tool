export function hasPermissions(user, permissionName) {
  //return Object.keys(user.grantedPermissions).findIndex(p => p === permissionName) > -1;
  return user.grantedPermissions.hasOwnProperty(permissionName);
}

export function getUserType(user) {
  if (!user || !user.grantedPermissions) {
    return null;
  }
  const grantedPermissions = Object.keys(user.grantedPermissions);
  if (grantedPermissions.length > 0) {
    return 'authority';
    //TODO understand which are super pro and which are for simple pro, volunteers and so on
  } else {
    return 'citizen';
  }
}
