import moment from 'moment-timezone';
// import { localizeDate } from 'js/utils/localizeDate';

export function getCompleteDateTime(date, time, locale) {
  let newDate;
  if (date) {
    newDate = moment(date);
    if (time) {
      newDate.hour(time.getHours());
      newDate.minute(time.getMinutes());
    }
    return newDate;
  }
}

export default getCompleteDateTime;
