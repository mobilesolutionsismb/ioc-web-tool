import moment from 'moment-timezone';
// import { localizeDate } from 'js/utils/localizeDate';

export function getSimplifiedDateTime(date) {
  let newDate;
  if (date) {
    newDate = moment(date);
    return newDate.format('D/M/YYYY');
  } else {
    return 'dd/mm/yyyy';
  }
}

export default getSimplifiedDateTime;
