import axios from 'axios';
import remark from 'remark';
import reactRenderer from 'remark-react';

const rootDirectoryProvider = {};

Object.defineProperty(rootDirectoryProvider, 'rootDirectory', {
  get: () =>
    IS_CORDOVA && cordova.file && cordova.file.applicationDirectory
      ? `${cordova.file.applicationDirectory}www/`
      : PUBLIC_PATH
});

export async function localeLoader(locale) {
  let translation = { default: {} };
  switch (locale) {
    case 'de':
      translation = await import('locale/lang_de/translation.json');
      break;
    case 'es':
      translation = await import('locale/lang_es/translation.json');
      break;
    case 'fr':
      translation = await import('locale/lang_fr/translation.json');
      break;
    case 'fi':
      translation = await import('locale/lang_fi/translation.json');
      break;
    case 'it':
      translation = await import('locale/lang_it/translation.json');
      break;
    case 'en':
    default:
      translation = await import('locale/lang_en/translation.json');
      break;
  }
  return translation.default;
}

export async function loadDictionary(locale) {
  const translation = await localeLoader(locale);
  return {
    translationLoader: fn => {
      fn(translation);
    },
    locale
  }; // for backward comp with ioc-localization
}

export async function avatarLoader(index) {
  let avatar = null;
  switch (index) {
    case 0:
      avatar = await import('assets/avatars/avatar-00.svg');
      break;
    case 1:
      avatar = await import('assets/avatars/avatar-01.svg');
      break;
    case 2:
      avatar = await import('assets/avatars/avatar-02.svg');
      break;
    case 3:
      avatar = await import('assets/avatars/avatar-03.svg');
      break;
    case 4:
      avatar = await import('assets/avatars/avatar-04.svg');
      break;
    case 5:
      avatar = await import('assets/avatars/avatar-05.svg');
      break;
    case 6:
      avatar = await import('assets/avatars/avatar-06.svg');
      break;
    case 7:
      avatar = await import('assets/avatars/avatar-07.svg');
      break;
    case 8:
      avatar = await import('assets/avatars/avatar-08.svg');
      break;
    case 9:
      avatar = await import('assets/avatars/avatar-09.svg');
      break;
    case 10:
      avatar = await import('assets/avatars/avatar-10.svg');
      break;
    case 11:
      avatar = await import('assets/avatars/avatar-11.svg');
      break;
    case 12:
      avatar = await import('assets/avatars/avatar-12.svg');
      break;
    case 13:
      avatar = await import('assets/avatars/avatar-13.svg');
      break;
    case 14:
      avatar = await import('assets/avatars/avatar-14.svg');
      break;
    case 15:
      avatar = await import('assets/avatars/avatar-15.svg');
      break;
    default:
      break;
  }
  return avatar ? avatar.default : '';
}

/**
 * Transform a markdown text into raw html
 * @see https://github.com/mapbox/remark-react
 * @param {string} filename
 * @param {string} locale
 * @return html
 */
export async function getMarkdownTextAsHTML(filename, locale = 'en') {
  const pathname = `${rootDirectoryProvider.rootDirectory}locales/lang_${locale}/${filename}.md`;
  let markdownResponse;
  try {
    markdownResponse = await axios.get(pathname);
  } catch (err) {
    if (err.response && err.response.status === 404) {
      markdownResponse = await axios.get(
        `${rootDirectoryProvider.rootDirectory}locales/lang_en/${filename}.md`
      );
    } else {
      throw err;
    }
  }
  const text = markdownResponse.data;
  return (
    remark()
      // .use(reactRenderer)
      .processSync(text).contents
  );
}

/**
 * Transform a markdown text into a React Component
 * @see https://github.com/mapbox/remark-react
 * @param {string} filename
 * @param {string} locale
 * @return React.Component
 */
export async function getMarkdownTextAsComponent(filename, locale = 'en') {
  const pathname = `${rootDirectoryProvider.rootDirectory}locales/lang_${locale}/${filename}.md`;
  let markdownResponse;
  try {
    markdownResponse = await axios.get(pathname);
  } catch (err) {
    if (err.response && err.response.status === 404) {
      markdownResponse = await axios.get(
        `${rootDirectoryProvider.rootDirectory}locales/lang_en/${filename}.md`
      );
    } else {
      throw err;
    }
  }
  const text = markdownResponse.data;
  return remark()
    .use(reactRenderer)
    .processSync(text).contents;
}
