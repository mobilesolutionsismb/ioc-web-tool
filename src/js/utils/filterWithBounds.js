import within from '@turf/within';
import { featureCollection } from '@turf/helpers';
import bboxPolygon from '@turf/bbox-polygon';

export function filterWithBounds(items, bounds) {
  // TODO check that items is a FeatureCollection
  //Exception if Area of Interest is point instead of a polygon
  let result = false;
  try {
    result = within(
      featureCollection(items),
      featureCollection([Array.isArray(bounds) ? bboxPolygon(bounds) : bounds])
    );
  } catch (e) {
    console.log('%cError', 'color: darkRed; background: black', e);
  }

  return result;
}

export function filterIREACTFeaturesWithBounds(items, bounds, itemType) {
  let oldFeat = items.filter(
    item =>
      itemType === 'reportRequest'
        ? item.properties.type !== itemType
        : item.properties.itemType !== itemType || item.properties.type === 'reportRequest'
  );
  let featureToBeFiltered = items.filter(
    item =>
      itemType === 'reportRequest'
        ? item.properties.type === itemType
        : item.properties.itemType === itemType && item.properties.type !== 'reportRequest'
  );
  let result = [];
  try {
    result = within(
      featureCollection(featureToBeFiltered),
      featureCollection([Array.isArray(bounds) ? bboxPolygon(bounds) : bounds])
    );
  } catch (e) {
    console.log('%cError', 'color: darkRed; background: black', e);
  }

  return [...oldFeat, ...result.features];
}
