import booleanEqual from '@turf/boolean-equal';
import { point } from '@turf/helpers';
import isEqual from 'react-fast-compare';

/**
 *
 * @param {GeoJSON} ftA - ireact feature with unique(id, itemType)
 * @param {GeoJSON} ftB - ireact feature with unique(id, itemType)
 * @param {deep} false - compare using isEqual
 */
export function areFeaturesEqual(ftA = null, ftB = null, deep = false) {
  if (deep) {
    return isEqual(ftA, ftB);
  } else {
    if (ftA && ftB) {
      const sameProps =
        ftA.properties.id === ftB.properties.id &&
        ftA.properties.itemType === ftB.properties.itemType;

      return sameProps === false
        ? sameProps
        : booleanEqual(
            Array.isArray(ftA.coordinates) ? point(ftA.coordinates).geometry : ftA.geometry,
            Array.isArray(ftB.coordinates) ? point(ftB.coordinates).geometry : ftB.geometry
          );
    } else if (ftA === null && ftB === null) {
      return true;
    } else return false;
  }
}

/**
 *
 * @param {LngLatLike} ptA
 * @param {LngLatLike} ptB
 */
export function arePointsEqual(ptA = null, ptB = null) {
  if (ptA === null && ptB === null) {
    return true;
  } else if (ptA !== null && ptB !== null) {
    const pt1 = mapboxgl.LngLat.convert(ptA).toArray();
    const pt2 = mapboxgl.LngLat.convert(ptB).toArray();
    return pt1[0] === pt2[0] && pt1[1] === pt2[1];
  } else return false;
}
