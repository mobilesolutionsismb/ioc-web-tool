import moment from 'moment';

export function filterIREACTFeaturesWithDates(items, starDate, endDate, itemType) {
  let oldItems = items.filter(item => item.properties.itemType !== itemType);
  let featuresToBeFiltered = items.filter(item => item.properties.itemType === itemType);
  let filteredFeatures = [];

  filteredFeatures = featuresToBeFiltered.filter(item => {
    return (
      moment(starDate).isBefore(item.properties.userCreationTime, 'second') &&
      moment(endDate).isAfter(item.properties.userCreationTime, 'second')
    );
  });

  return [...oldItems, ...filteredFeatures];
}
