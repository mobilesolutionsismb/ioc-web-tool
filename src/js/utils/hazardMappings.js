import { HAZARD_MAPPING } from 'js/modules/emCommNew';
import { HAZARD_TYPES_LETTERS, HAZARD_TYPES_LABELS } from 'js/components/Home/components/commons';

export const capCategoryLabels = [
  '_ecc_geo',
  '_ecc_met',
  '_ecc_fire',
  '_ecc_health',
  '_ecc_cbrne',
  '_ecc_other',
  '_ecc_env',
  '_ecc_infra',
  '_ecc_transport'
];
export const hazardLabels = [
  '_ech_earthquake',
  '_ech_tsunami',
  '_ech_rock_fall',
  '_ech_landslide',
  '_ech_ash_fall',
  '_ech_lahar',
  '_ech_pyroclastic_flow',
  '_ech_lava_flow',
  '_ech_extreme_weather',
  '_ech_fog',
  '_ech_storm',
  '_ech_wave_action',
  '_ech_drought',
  '_ech_glacial_lake_outburst',
  '_ech_wildfire',
  '_ech_epidemic',
  '_ech_insect_infestation',
  '_ech_animal_accident',
  '_ech_extraterrestrial_impact',
  '_ech_space_weather',
  '_ech_chemical_spill',
  '_ech_industrial_collapse',
  '_ech_industrial_explosion',
  '_ech_industrial_fire',
  '_ech_gas_leak',
  '_ech_poisoning',
  '_ech_radiation',
  '_ech_oil_spill',
  '_ech_industrial_accident',
  '_ech_air_accident',
  '_ech_road_accident',
  '_ech_traffic',
  '_ech_rail_accident',
  '_ech_water_accident',
  '_ech_collapse',
  '_ech_explosion',
  '_ech_fire',
  '_ech_other',
  '_ech_extra_tropical_storm',
  '_ech_tropical_storm',
  '_ech_derecho',
  '_ech_hail',
  '_ech_thunderstorms',
  '_ech_rain',
  '_ech_tornado',
  '_ech_dust_storm',
  '_ech_blizzard',
  '_ech_storm_surge',
  '_ech_wind',
  '_ech_cold_wave',
  '_ech_heat_wave',
  '_ech_snow',
  '_ech_ice',
  '_ech_frost',
  '_ech_coastal_event',
  '_ech_coastal_erosion',
  '_ech_flood',
  '_ech_rain_flood',
  '_ech_coastal_flood',
  '_ech_riverine_flood',
  '_ech_flash_flood',
  '_ech_ice_jam_flood',
  '_ech_avalanches',
  '_ech_debris',
  '_ech_mudflow',
  '_ech_forest_fire',
  '_ech_land_fire',
  '_ech_pasture_fire',
  '_ech_viral_disease',
  '_ech_bacterial_disease',
  '_ech_parasitic_disease',
  '_ech_fungal_disease',
  '_ech_prion_disease',
  '_ech_grasshoper',
  '_ech_locust',
  '_ech_airburst',
  '_ech_energetic_particles',
  '_ech_geomagnetic_storm',
  '_ech_shockwave',
  '_ech_none'
];

const association = {
  _ecc_geo: [
    '_ech_earthquake',
    '_ech_tsunami',
    '_ech_rock_fall',
    '_ech_landslide',
    '_ech_ash_fall',
    '_ech_lahar',
    '_ech_pyroclastic_flow',
    '_ech_lava_flow',
    '_ech_wave_action',
    '_ech_coastal_erosion',
    '_ech_avalanches',
    '_ech_debris',
    '_ech_mudflow'
  ],
  _ecc_met: [
    '_ech_extreme_weather',
    '_ech_fog',
    '_ech_storm',
    '_ech_drought',
    '_ech_glacial_lake_outburst',
    '_ech_extra_tropical_storm',
    '_ech_tropical_storm',
    '_ech_derecho',
    '_ech_hail',
    '_ech_thunderstorms',
    '_ech_rain',
    '_ech_tornado',
    '_ech_dust_storm',
    '_ech_blizzard',
    '_ech_storm_surge',
    '_ech_wind',
    '_ech_cold_wave',
    '_ech_heat_wave',
    '_ech_snow',
    '_ech_ice',
    '_ech_frost',
    '_ech_coastal_event',
    '_ech_flood',
    '_ech_rain_flood',
    '_ech_coastal_flood',
    '_ech_riverine_flood',
    '_ech_flash_flood',
    '_ech_ice_jam_flood'
  ],
  _ecc_fire: [
    '_ech_wildfire',
    '_ech_industrial_fire',
    '_ech_fire',
    '_ech_forest_fire',
    '_ech_land_fire',
    '_ech_pasture_fire'
  ],
  _ecc_health: ['_ech_epidemic'],
  _ecc_cbrne: [
    '_ech_insect_infestation',
    '_ech_chemical_spill',
    '_ech_gas_leak',
    '_ech_poisoning',
    '_ech_radiation',
    '_ech_oil_spill',
    '_ech_viral_disease',
    '_ech_bacterial_disease',
    '_ech_parasitic_disease',
    '_ech_fungal_disease',
    '_ech_prion_disease',
    '_ech_grasshoper',
    '_ech_locust',
    '_ech_airburst'
  ],
  _ecc_other: ['_ech_animal_accident', '_ech_collapse', '_ech_explosion', '_ech_other'],
  _ecc_env: [
    '_ech_extraterrestrial_impact',
    '_ech_space_weather',
    '_ech_energetic_particles',
    '_ech_geomagnetic_storm',
    '_ech_shockwave'
  ],
  _ecc_infra: ['_ech_industrial_collapse', '_ech_industrial_explosion', '_ech_industrial_accident'],
  _ecc_transport: [
    '_ech_air_accident',
    '_ech_road_accident',
    '_ech_traffic',
    '_ech_rail_accident',
    '_ech_water_accident'
  ],
  _ecc_null: ['_ech_none']
};

export function filterHazards(capCategory) {
  let hazards;
  if (capCategory === '' || capCategory === null || capCategory === undefined) {
    hazards = hazardLabels;
  } else {
    hazards = association[capCategory];
  }
  return hazards;
}
export function getCapCategoryByHazard(hazard) {
  let capCategory = capCategoryLabels;
  if (hazard === '' || hazard === null || hazard === undefined) {
    capCategory = capCategoryLabels;
  } else {
    let keys = Object.keys(association);
    keys.forEach(element => {
      if (association[element].find(x => x === hazard) !== undefined) capCategory = [element];
    });
  }
  return capCategory;
}

export function getTranslationByhazardName(hazard) {
  return Object.keys(HAZARD_MAPPING).find(
    key => HAZARD_MAPPING[key].toLowerCase() === hazard.toLowerCase()
  );
}

export function getIconByHazardName(hazard) {
  return HAZARD_TYPES_LETTERS[HAZARD_TYPES_LABELS[getTranslationByhazardName(hazard)]];
}
