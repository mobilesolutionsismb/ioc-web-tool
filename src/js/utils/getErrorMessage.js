// Babel-generated sublclasses loose prototype info
// So we have to check on attributes
// https://github.com/babel/babel/issues/4485
export function getErrorMessage(error) {
  const err =
    error.details && error.details.serverDetails && error.details.serverDetails.error
      ? error.details.serverDetails.error
      : error;
  return err.details ? err.details : err.message;
}
