import localForage from 'localforage';
import { logSevereWarning } from 'js/utils/log';
import { API } from 'ioc-api-interface';

// Stores (part of) the redux state with redux persist
const ROOT_STORAGE_NAME = `root-storage-${PKG_NAME}`;
// Stores what's needed from the api
const AUTH_STORAGE_NAME = `app-storage-${PKG_NAME}`;

const authStorage = localForage.createInstance({
  name: AUTH_STORAGE_NAME,
  storeName: AUTH_STORAGE_NAME,
  description: 'storage for I-REACT authentication information'
});

const rootStorage = localForage.createInstance({
  name: ROOT_STORAGE_NAME,
  storeName: ROOT_STORAGE_NAME,
  description: 'storage for I-REACT data information'
});

export function makePersistConfig(whitelist, transforms) {
  const persistConfig = {
    key: ROOT_STORAGE_NAME,
    storage: rootStorage,
    version: makeVersion(VERSION),
    whitelist,
    transforms
  };
  return persistConfig;
}

function makeVersion() {
  return ENVIRONMENT === 'dev-server' ? `${ENVIRONMENT}-${BUILD_DATE}` : VERSION;
}

export const getAppStorage = () => authStorage;

function compareVersions(currentVersion, storedVersion) {
  let sameVersion = false;
  if (ENVIRONMENT === 'production') {
    // consider only major and minor number, leaving the patch number out
    let current = currentVersion
      .split('.')
      .slice(0, 2)
      .join('.');
    let stored = storedVersion
      .split('.')
      .slice(0, 2)
      .join('.');
    sameVersion = current === stored;
  } else {
    sameVersion = currentVersion === storedVersion;
  }
  return sameVersion;
}

export async function checkStorageVersion(persistor) {
  try {
    const storedVersion = await authStorage.getItem('version');
    if (storedVersion) {
      if (!compareVersions(storedVersion, makeVersion())) {
        // RESET
        try {
          await authStorage.clear();
          await rootStorage.clear();
          const api = API.getInstance();
          await api.clearInnerStorage();
          await persistor.purge();
        } catch (ex) {
          logSevereWarning('Error resetting storage', ex);
        } finally {
          window.location.reload();
        }
      } else {
      }
    } else {
      await authStorage.setItem('version', makeVersion());
    }
  } catch (ex) {
    logSevereWarning('Error accessing storage', ex);
  }
}
