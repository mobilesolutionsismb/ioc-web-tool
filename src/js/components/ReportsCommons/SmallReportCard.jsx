import './styles.scss';
import React, { Component } from 'react';
import { LikesCounter } from 'js/components/LikeButtons';
import {
  ValidatedIcon,
  RejectedIcon,
  ShareIcon,
  RoleColorDot,
  ReportContentStats,
  HazardBanner,
  GoToMapIcon
} from './commons';
import { localizeDate } from 'js/utils/localizeDate';
// import { withDictionary } from 'ioc-localization';

class SmallReportCard extends Component {
  static defaultProps = {
    showMapIcon: false,
    reportProperties: null
  };

  render() {
    const {
      locale,
      reportProperties,
      showMapIcon,
      onShareButtonClick,
      onMapButtonClick,
      /*dictionary, requesterId,*/ style
    } = this.props;

    if (reportProperties && reportProperties.user) {
      const reporter = reportProperties.user;
      const proReporter = reportProperties.organization !== null;
      // console.log('%c SmallReportCard', 'background: orangered; color: whitesmoke', reportProperties);
      //const reporterName = proReporter ? reporter.employeeId : reporter.nickname;
      //const displayedUserName = requesterId === reportProperties.creatorUserId ? dictionary._you : reporterName;
      const displayOrganizationName = reportProperties.organization; // != null ? reportProperties.organization.length > 34 ? reportProperties.organization.substring(0, 31) + '...' : reportProperties.organization : '';

      return (
        <div className="small-report-card" style={style}>
          <div
            className="report-popup-thumbnail"
            style={{ backgroundImage: `url(${reportProperties.thumbnail})` }}
          >
            <HazardBanner hazard={reportProperties.hazard} />
          </div>
          <div className="report-popup-info">
            <div className="content-info">
              <div className="contents">
                <ReportContentStats reportProperties={reportProperties} />
              </div>
              <div className="author">
                <RoleColorDot userRole={proReporter ? 'authority' : 'pro'} />
                {/* <span className="reporter">{proReporter ? `${reportProperties.organization}, ${displayedUserName}` : `${displayedUserName}${reporter.level ? ', ' + reporter.level : ''}`}</span> */}
                <span className="reporter line-clamp">
                  {proReporter
                    ? `${displayOrganizationName}`
                    : `${reporter.level ? reporter.level : ''}`}
                </span>
              </div>
            </div>
            <div className="date-votes-validation">
              <div className="date">
                {localizeDate(reportProperties.userCreationTime, locale, 'L LT')}
              </div>
              <div className="votes">
                {reportProperties.status === 'submitted' && (
                  <LikesCounter
                    size="xs"
                    style={{ width: 'calc(100% - 8px)' }}
                    upvotes={reportProperties.upvoteCount}
                    downvotes={reportProperties.downvoteCount}
                    alreadyVoted={reportProperties.alreadyVoted}
                  />
                )}
                {reportProperties.status === 'validated' && <ValidatedIcon tooltip="Validated" />}
                {reportProperties.status !== null &&
                  (reportProperties.status === 'inaccurate' ||
                    reportProperties.status === 'inappropriate') && (
                    <RejectedIcon tooltip={reportProperties.status} />
                  )}
              </div>
            </div>
            <div className="actions">
              <ShareIcon tooltip="Share" onClick={onShareButtonClick} />
              {showMapIcon && <GoToMapIcon tooltip="Show On Map" onClick={onMapButtonClick} />}
            </div>
          </div>
        </div>
      );
    } else {
      return <div className="small-report-card" />;
    }
  }
}

export default SmallReportCard;
