import React from 'react';
import { IconButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { white, red900 } from 'material-ui/styles/colors';
import { withDictionary } from 'ioc-localization';

export const DateIcon = ({ style }) => (
  <FontIcon style={style} className="material-icons">
    access_time
  </FontIcon>
);
export const PlaceIcon = ({ style }) => (
  <FontIcon style={style} className="material-icons">
    place
  </FontIcon>
);
export const InfoIcon = ({ style }) => (
  <FontIcon style={style} className="material-icons">
    info
  </FontIcon>
);

export const DeleteReportButton = ({ onClick, tooltip }) => (
  <IconButton
    className="menu-button"
    onClick={onClick}
    style={{ backgroundColor: red900, borderRadius: '100%' }}
  >
    <FontIcon className="material-icons" color={white}>
      clear
    </FontIcon>
  </IconButton>
);

// Blacklist hazard values which should not be selectable or displayed by the app
export const HAZARD_VALUES_BLACKLIST = ['all', 'other', 'unknown', 'none'];

export const CONTENT_TYPES_LETTERS = {
  measure: '\ue91a',
  damage: '\ue904', //!WRONG
  resource: '\ue929',
  people: '\ue91f'
};

//Export look up tables damage name -> iconName
export const DAMAGE_ICONS = {
  roads: '\ue909',
  bridges: '\ue900',
  underpass: '\ue90b',
  parks: '\ue906',
  other: '\ue905',
  school: '\ue90a',
  hospital: '\ue902',
  shopping_mall: '\ue903',
  private_house: '\ue908',
  farm: '\ue901',
  powerNetwork: '\ue907',
  power_network: '\ue907',
  waterSupply: '\ue90c',
  water_supply: '\ue90c'
};

//Export look up tables resource name -> iconName
export const RESOURCE_ICONS = {
  safe_place: '\ue926',
  'evacuation_area area': '\ue923',
  evacuation_area: '\ue923',
  drinkable_water: '\ue921',
  power_network: '\ue922',
  food: '\ue924',
  sleep: '\ue927',
  toilet: '\ue928',
  internet: '\ue925'
};

//Export look up tables people category -> iconName
export const PEOPLE_ICONS = {
  _injured: '\ue91d', // ic_sentiment_very_dissatisfied
  _to_be_rescued: '\ue920',
  _casualties: '\ue91b',
  _disabled: '\ue91c'
};

export const ContentTypeIcon = ({ iconSize, contentType, color }) => (
  <FontIcon
    style={{ lineHeight: iconSize * 0.6 + 'px', fontSize: iconSize * 0.6 }}
    className="ireact-icons"
    //className="material-icons"
    color={color}
  >
    {CONTENT_TYPES_LETTERS[contentType]}
  </FontIcon>
);

export const ValidatedIcon = muiThemeable()(({ muiTheme, tooltip, style, iconStyle }) => (
  <IconButton
    className="menu-button"
    style={{
      backgroundColor: muiTheme.palette.validatedColor,
      overflow: 'hidden',
      borderRadius: '100%',
      fontSize: '14px',
      width: 24,
      height: 24,
      margin: 0,
      padding: 0,
      lineHeight: '28px',
      ...style
    }}
    iconStyle={{ fontSize: '14px', ...iconStyle }}
    touch={true}
  >
    <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
      done
    </FontIcon>
  </IconButton>
));

export const RejectedIcon = muiThemeable()(({ muiTheme, tooltip, style, iconStyle }) => (
  <IconButton
    className="menu-button"
    style={{
      backgroundColor:
        tooltip === 'inappropriate'
          ? muiTheme.palette.rejectedColor
          : muiTheme.palette.inaccurateColor,
      overflow: 'hidden',
      borderRadius: '100%',
      fontSize: '14px',
      width: 24,
      height: 24,
      margin: 0,
      padding: 0,
      lineHeight: '28px',
      ...style
    }}
    iconStyle={{ fontSize: '14px', ...iconStyle }}
    touch={true}
  >
    <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
      clear
    </FontIcon>
  </IconButton>
));

export const GoToMapIcon = muiThemeable()(({ muiTheme, tooltip, onClick, style, iconStyle }) => (
  <IconButton
    className="menu-button"
    style={{ width: 24, height: 24, borderRadius: '100%', fontSize: '14px', padding: 0, ...style }}
    iconStyle={{ fontSize: '14px', ...iconStyle }}
    tooltip={tooltip}
    touch={true}
    tooltipStyles={{ top: 0 }}
    tooltipPosition="top-left"
    onClick={onClick}
  >
    <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
      place
    </FontIcon>
  </IconButton>
));

export const ShareIcon = muiThemeable()(({ muiTheme, tooltip, onClick, style, iconStyle }) => (
  <IconButton
    className="menu-button"
    style={{ width: 24, height: 24, borderRadius: '100%', fontSize: '14px', padding: 0, ...style }}
    iconStyle={{ fontSize: '14px', ...iconStyle }}
    tooltip={tooltip}
    touch={true}
    tooltipPosition="top-left"
    tooltipStyles={{ top: 0 }}
    onClick={onClick}
  >
    <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
      share
    </FontIcon>
  </IconButton>
));

export const RoleColorDot = muiThemeable()(({ userRole, muiTheme, style }) => (
  <div
    className={`role-color-dot role-${userRole}`}
    style={{
      ...style,
      background:
        userRole === 'authority' ? muiTheme.palette.authorityColor : muiTheme.palette.citizensColor
    }}
  />
));

/**
 * Extract unique category names from a set of measures
 * @param {Array} measures
 */
export function getUniqueCategories(measures) {
  if (Array.isArray(measures)) {
    return Array.from(new Set(measures.map(m => m.category)));
  } else {
    return [];
  }
}

export const ReportContentStats = withDictionary(
  ({ reportProperties, dictionary, hideMeasureIcons }) => {
    const { measures, damages, resources, people } = reportProperties;
    let count = 0;
    let type = 'unknown'; // override the server's type, for now
    let categories = [];
    if (measures.length > 0) {
      type = 'measures';
      count = measures.length;
      categories = getUniqueCategories(measures);
    } else if (damages.length > 0) {
      type = 'damages';
      count = damages.length;
    } else if (resources.length > 0) {
      type = 'resources';
      count = resources.length;
    } else if (people.length > 0) {
      type = 'people';
      count =
        people.length === 1
          ? people[0].quantity
          : people.reduce((a, b) => {
              let val1 = a.label ? (a.quantity > 0 ? a.quantity : 0) : a > 0 ? a : 0,
                val2 = b.label ? (b.quantity > 0 ? b.quantity : 0) : b > 0 ? b : 0;
              return val1 + val2;
            });
    } else if (reportProperties.type === 'misc') {
      type = 'misc';
    }

    return (
      <div className="report-stats">
        <span style={{ textTransform: 'uppercase' }}>
          {count === 0
            ? dictionary(`_r_stat_single_${type}`)
            : count === 1
              ? dictionary(`_r_stat_single_${type}`, count)
              : dictionary(`_r_stat_multi_${type}`, count)}
        </span>
        {type === 'measures' &&
          hideMeasureIcons !== true && (
            <span className="category-icons">
              {categories.map((c, i) => (
                <MeasureCategoryIcon key={i} style={{ fontSize: 18 }} category={c} />
              ))}
            </span>
          )}
      </div>
    );
  }
);

export const HazardBanner = withDictionary(
  muiThemeable()(
    ({ hazard, dictionary, muiTheme, style }) =>
      hazard && HAZARD_VALUES_BLACKLIST.indexOf(hazard) === -1 ? (
        <div
          className="hazard-banner"
          style={{
            color: muiTheme.palette.textColor,
            backgroundColor: muiTheme.palette.hazardBannerColor,
            ...style
          }}
        >
          {dictionary(`_${hazard}`)}
        </div>
      ) : null
  )
);

export const CATEGORY_MAP = {
  unknown: 'block-helper',
  fire: 'fire',
  water: 'water',
  landslide: 'terrain',
  weather: 'cloud'
};

export const MEASURE_UNITS = {
  degree: '°C',
  m: 'm',
  mm: 'mm',
  cm: 'cm',
  m3: 'm³'
};

export const MeasureCategoryIcon = ({ color, category, style }) => (
  <FontIcon color={color} style={style} className={`mdi mdi-${CATEGORY_MAP[category]}`} />
);
