import React from 'react';
import { Avatar, FontIcon } from 'material-ui';

import { withDictionary } from 'ioc-localization';
import muiThemeable from 'material-ui/styles/muiThemeable';

export const HAZARD_TYPES_LETTERS = {
  none: ' ',
  fire: '\ue905',
  flood: '\ue906',
  extremeWeather: '\ue904',
  landslide: '\ue907',
  avalanches: '\ue902',
  earthquake: '\ue903',
  unknown: '?'
};

export const HAZARD_TYPES_LABELS = {
  flood: '_flood',
  fire: '_fire',
  extremeWeather: '_extremeWeather',
  landslide: '_landslide',
  earthquake: '_earthquake',
  avalanches: '_avalanches'
};

function hazardNameToIcon(hazard) {
  let name = '_unknown';
  if (HAZARD_TYPES_LETTERS.hasOwnProperty(hazard)) {
    return HAZARD_TYPES_LETTERS[hazard];
  } else {
    return name;
  }
}

function hazardNameToLabel(hazard) {
  let name = '_unknown';
  if (HAZARD_TYPES_LABELS.hasOwnProperty(hazard)) {
    return HAZARD_TYPES_LABELS[hazard];
  } else {
    return name;
  }
}

export const HazardAvatar = ({ hazardName, backgroundColor, size }) => (
  <Avatar
    size={size}
    className="hazard-selection-list-item__avatar"
    backgroundColor={backgroundColor}
  >
    <FontIcon className="ireact-pinpoint-icons" style={{ fontSize: Math.round(size * 0.666) }}>
      {hazardNameToIcon(hazardName)}
    </FontIcon>
  </Avatar>
);

export const HazardLabel = withDictionary(({ hazardName, style, dictionary, labelStyle }) => (
  <div className="hazard-selection-list-item__label" style={style}>
    <div className="hazard-selection-list-item__label-inner" style={labelStyle}>
      {dictionary(hazardNameToLabel(hazardName))}
    </div>
  </div>
));

export const HazardSelectionButton = muiThemeable()(
  ({ hazardName, onClick, selected, dictionary, muiTheme }) => {
    const palette = muiTheme.palette;

    const backgroundColor = selected ? palette.accent1Color : undefined;
    const labelStyle = { color: selected ? palette.accent1Color : palette.textColor };
    return (
      <div
        className={
          selected === true ? 'hazard-selection-list-item selected' : 'hazard-selection-list-item'
        }
        onClick={onClick}
      >
        <HazardAvatar size={60} backgroundColor={backgroundColor} hazardName={hazardName} />
        <HazardLabel labelStyle={labelStyle} hazardName={hazardName} />
      </div>
    );
  }
);
