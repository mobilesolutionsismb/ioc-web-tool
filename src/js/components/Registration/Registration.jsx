// import './registration.scss';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withLogin } from 'ioc-api-interface';
import { withMessages, withLoader } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { compose } from 'redux';
import RegistrationForm from './RegistrationForm';

const enhance = compose(withDictionary, withLogin, withMessages, withLoader);

class Registration extends Component {
  onRegistrationFormSubmit = async data => {
    let { name, surname, user, email, phone, password, captchaResponse } = data;
    let response = await this.props.register(
      name,
      surname,
      user,
      email,
      phone,
      password,
      captchaResponse,
      this.props.loadingStart,
      this.props.loadingStop
    );
    console.log('%cRegistration success', 'color: white; background: lime', response);
    this.props.pushMessage('_register_success', 'success');
    this.props.history.goBack();
  };

  onBackButtonClick = e => {
    e.preventDefault();
    this.props.history.goBack();
  };

  render() {
    return (
      <div className="registration page">
        <RegistrationForm
          onSubmit={this.onRegistrationFormSubmit}
          onBackButtonClick={this.onBackButtonClick}
          dictionary={this.props.dictionary}
          locale={this.props.locale}
        />
      </div>
    );
  }
}

export default withRouter(enhance(Registration));
