import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextField, RaisedButton, FlatButton } from 'material-ui';
import Recaptcha from 'react-recaptcha';

const style = {
  button: {
    margin: 8
  },
  fullWidth: {
    width: '100%',
    marginTop: 8,
    marginBottom: 8
  }
};

const validate = (values, props) => {
  const errors = {};
  if (!values.user) {
    errors.user = props.dictionary._required;
  } else if (values.user.length > 32) {
    errors.user = props.dictionary._user_error_text;
  }

  if (!values.email) {
    errors.email = props.dictionary._required;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = props.dictionary._email_error_text;
  } else if (values.email.length > 32) {
    errors.email = props.dictionary._email_error_text;
  }

  if (!values.name) {
    errors.name = props.dictionary._required;
  } else if (values.name.length > 32) {
    errors.name = props.dictionary._name_error_text;
  }

  if (!values.surname) {
    errors.surname = props.dictionary._required;
  } else if (values.surname.length > 32) {
    errors.surname = props.dictionary._surname_error_text;
  }

  if (!values.password) {
    errors.password = props.dictionary._required;
  } else if (values.password.length > 32) {
    errors.password = props.dictionary._password_error_text;
  }
  if (!IS_CORDOVA) {
    if (!values.captchaResponse) {
      errors.captchaResponse = props.dictionary._required;
    }
  }

  return errors;
};

const callback = () => {
  console.log('Captcha Loaded');
};

// const onBackButtonClick = e => {
//   e.preventDefault();
//   this.props.onBackButtonClick();
// };

const renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => (
  <TextField
    hintText={label}
    fullWidth={true}
    floatingLabelText={label}
    errorText={touched && error}
    {...input}
    {...custom}
  />
);

const renderRecaptcha = ({ input, label, meta: { touched, error }, ...custom }) => (
  <Recaptcha
    sitekey={RECAPTCHA_SITEKEY}
    render="explicit"
    onloadCallback={callback}
    hl={custom.language}
    verifyCallback={value => input.onChange(value)}
  />
);

const RegistrationForm = props => {
  const {
    pristine,
    submitting,
    /*  reset, */ handleSubmit,
    onBackButtonClick,
    /* language, */ dictionary
  } = props;

  return (
    <form className="registration-form" onSubmit={handleSubmit}>
      <div style={style.fullWidth}>
        <Field name="name" component={renderTextField} label={dictionary._name} />
      </div>
      <div style={style.fullWidth}>
        <Field name="surname" component={renderTextField} label={dictionary._surname} />
      </div>
      <div style={style.fullWidth}>
        <Field name="user" component={renderTextField} label={dictionary._username} />
      </div>
      <div style={style.fullWidth}>
        <Field name="email" type="email" component={renderTextField} label={dictionary._email} />
      </div>
      <div style={style.fullWidth}>
        <Field name="phone" component={renderTextField} label={dictionary._phoneNumber} />
      </div>
      <div style={style.fullWidth}>
        <Field
          name="password"
          type="password"
          component={renderTextField}
          label={dictionary._password}
        />
      </div>
      {IS_CORDOVA ? null : (
        <div style={{ ...style.fullWidth, height: 72 }}>
          <Field name="captchaResponse" component={renderRecaptcha} />
        </div>
      )}
      <div style={style.fullWidth}>
        <RaisedButton
          type="submit"
          className="ui-button"
          disabled={pristine || submitting}
          label={dictionary._signup}
          primary={true}
          style={style.button}
        />
        <FlatButton className="ui-button" label={dictionary._back} onClick={onBackButtonClick} />
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'registrationForm', // a unique identifier for this form
  validate
})(RegistrationForm);
