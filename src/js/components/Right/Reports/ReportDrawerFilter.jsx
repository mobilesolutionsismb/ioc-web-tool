import React, { Component } from 'react';
import {
  Divider,
  List,
  ListItem,
  Subheader,
  Checkbox,
  Chip,
  Avatar,
  FlatButton
} from 'material-ui';
import {
  WarningIcon,
  LocationOnIcon,
  LocalSeeIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import SuperSelectField from 'material-ui-superselectfield';
import InputAOI from 'js/components/app/drawer/components/InputAOI';
import { withReportFilters, FILTERS } from 'js/modules/reportFilters';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { HAZARD_TYPES_LETTERS, HAZARD_TYPES_LABELS } from 'js/components/Home/components/commons';
import { compose } from 'redux';

const enhance = compose(withLeftDrawer, withReportFilters);

class ReportDrawerFilter extends Component {
  onReportTypeCheck = (event, isInputChecked) => {
    const filterCategory = 'reportType';
    const filterName = event.target.value;
    if (isInputChecked) this.props.addReportFilter(filterCategory, filterName);
    else this.props.removeReportFilter(filterCategory, filterName);
  };

  onReportStatusCheck = (event, isInputChecked) => {
    const filterCategory = 'status';
    const filterName = event.target.value;
    if (isInputChecked) this.props.addReportFilter(filterCategory, filterName);
    else this.props.removeReportFilter(filterCategory, filterName);
  };

  rightAction = () => {
    this.props.setOpenSecondary(false);
    this.props.history.push(`/home/${drawerNames.report}`);
  };

  updateSelectFilters = (selectedItems, args) => {
    const filterCategory = 'hazard';

    if (selectedItems.filter(item => item.value === '_hazard_fire').length > 0)
      this.props.addReportFilter(filterCategory, '_hazard_fire');
    else this.props.removeReportFilter(filterCategory, '_hazard_fire');

    if (selectedItems.filter(item => item.value === '_hazard_flood').length > 0)
      this.props.addReportFilter(filterCategory, '_hazard_flood');
    else this.props.removeReportFilter(filterCategory, '_hazard_flood');

    if (selectedItems.filter(item => item.value === '_hazard_landslide').length > 0)
      this.props.addReportFilter(filterCategory, '_hazard_landslide');
    else this.props.removeReportFilter(filterCategory, '_hazard_landslide');

    if (selectedItems.filter(item => item.value === '_hazard_extremeWeather').length > 0)
      this.props.addReportFilter(filterCategory, '_hazard_extremeWeather');
    else this.props.removeReportFilter(filterCategory, '_hazard_extremeWeather');

    if (selectedItems.filter(item => item.value === '_hazard_avalanches').length > 0)
      this.props.addReportFilter(filterCategory, '_hazard_avalanches');
    else this.props.removeReportFilter(filterCategory, '_hazard_avalanches');

    if (selectedItems.filter(item => item.value === '_hazard_earthquake').length > 0)
      this.props.addReportFilter(filterCategory, '_hazard_earthquake');
    else this.props.removeReportFilter(filterCategory, '_hazard_earthquake');
  };

  handleCustomDisplaySelectionsHazard = hazards => hazards =>
    hazards.length > 0 ? (
      hazards.map((item, i) => (
        <div key={i} style={{ display: 'flex', flexWrap: 'wrap' }}>
          <Chip key={i} style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteHazard(item)}>
            <Avatar key={i} size={30}>
              {HAZARD_TYPES_LETTERS[HAZARD_TYPES_LABELS[item.value]]}
            </Avatar>
            <span>{item.label}</span>
          </Chip>
        </div>
      ))
    ) : (
      <div key={0} style={{ minHeight: 42, lineHeight: '42px' }}>
        Select a Category
      </div>
    );

  onRequestDeleteHazard = itemToRemove => event => {
    this.props.removeReportFilter('hazard', itemToRemove.value);
  };

  render() {
    let {
      dictionary,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory
    } = this.props;

    const category = drawerNames.reportFilter;
    const address =
      drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const leftIconHazardType = <WarningIcon />;
    const leftIconAOI = <LocationOnIcon />;
    const leftIconContentType = <LocalSeeIcon />;
    const leftIconReportStatus = <LocalSeeIcon />;
    const subHeaderContentType = (
      <div style={{ display: 'flex' }}>
        <span>{leftIconContentType}</span>
        <span>{dictionary._content_type}</span>
      </div>
    );
    const subHeaderReportStatus = (
      <div style={{ display: 'flex' }}>
        <span>{leftIconReportStatus}</span>
        <span>{dictionary._status}</span>
      </div>
    );

    const hazardList = FILTERS.hazard
      .filter(item => item !== '_hazard_unknown' && item !== '_hazard_none')
      .map(item => {
        let hazard = {
          value: item,
          label: this.props.dictionary(item)
        };
        return hazard;
      });

    const hazardListNodes = hazardList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={category.label}>
          <span key={index}>{category.label}</span>
        </div>
      );
    });

    const hazardsSelected =
      this.props.reportFilters.hazard.length > 0
        ? this.props.reportFilters.hazard.map(item => {
            let hazard = {
              value: item,
              label: this.props.dictionary(item)
            };
            return hazard;
          })
        : [];

    const superSelectMenuCloseButton = (
      <FlatButton label={dictionary._close} backgroundColor={'red'} />
    );

    const drawerTitle = (
      <DrawerTitle
        dictionary={dictionary}
        locale={this.props.locale}
        title={dictionary._filters}
        rightIcon="close"
        rightAction={this.rightAction}
      />
    );
    const superSelectHazard = (
      <SuperSelectField
        key={0}
        style={{ width: '90%' }}
        name="selectHazardReport"
        multiple
        floatingLabel="Select the Hazard"
        onChange={this.updateSelectFilters}
        value={hazardsSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(hazardsSelected)}
        menuCloseButton={superSelectMenuCloseButton}
      >
        {hazardListNodes}
      </SuperSelectField>
    );

    return (
      <section>
        {drawerTitle}
        <div>
          <List>
            <ListItem
              key={0}
              disabled={true}
              leftIcon={leftIconHazardType}
              children={superSelectHazard}
            />
            <ListItem key={1} disabled={true} leftIcon={leftIconAOI}>
              <InputAOI
                category={category}
                address={address}
                drawPoint={drawPoint}
                drawPolygon={drawPolygon}
                getMapDraw={this.props.getMapDraw}
                resetHomeDrawState={resetHomeDrawState}
                setDrawPoint={setDrawPoint}
                setDrawPolygon={setDrawPolygon}
                setDrawCategory={setDrawCategory}
              />
            </ListItem>
          </List>
          <Divider />
          <List>
            <Subheader className="subHeader" children={subHeaderContentType} />
            {FILTERS.reportType ? (
              Object.keys(FILTERS.reportType).map((e, i) => (
                <ListItem
                  style={{ marginLeft: '10px' }}
                  leftCheckbox={
                    <Checkbox
                      checked={
                        this.props.reportFilters.reportType.indexOf(FILTERS.reportType[e]) >= 0
                      }
                      value={FILTERS.reportType[e]}
                      name={dictionary[FILTERS.reportType[e]]}
                      label={dictionary[FILTERS.reportType[e]]}
                      onCheck={this.onReportTypeCheck}
                    />
                  }
                  key={i}
                />
              ))
            ) : (
              <div />
            )}
          </List>
          <Divider />
          <List>
            <Subheader className="subHeader" children={subHeaderReportStatus} />
            {FILTERS.status ? (
              Object.keys(FILTERS.status).map((e, i) => (
                <ListItem
                  style={{ marginLeft: '10px' }}
                  leftCheckbox={
                    <Checkbox
                      checked={this.props.reportFilters.status.indexOf(FILTERS.status[e]) >= 0}
                      value={FILTERS.status[e]}
                      name={dictionary[FILTERS.status[e]]}
                      label={dictionary[FILTERS.status[e]]}
                      onCheck={this.onReportStatusCheck}
                    />
                  }
                  key={i}
                />
              ))
            ) : (
              <div />
            )}
          </List>
        </div>
      </section>
    );
  }
}

export default enhance(ReportDrawerFilter);
