import React, { Component } from 'react';
import { Divider, List, ListItem, Subheader, Checkbox } from 'material-ui';
import { LocationOnIcon, LocalSeeIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import InputAOI from 'js/components/app/drawer/components/InputAOI';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withReportRequestFilters, FILTERS } from 'js/modules/reportRequestFilters';

import { compose } from 'redux';
const enhance = compose(withLeftDrawer, withReportRequestFilters);

class ReportRequestDrawerFilter extends Component {
  rightAction = () => {
    this.props.setOpenSecondary(false);
    this.props.history.push(`/home/${drawerNames.report}?reportRequest`);
  };

  onReportRequestTypeCheck = (event, isInputChecked) => {
    const filterCategory = 'reportRequestType';
    const filterName = event.target.value;
    if (isInputChecked) this.props.addReportRequestFilter(filterCategory, filterName);
    else this.props.removeReportRequestFilter(filterCategory, filterName);
  };

  onReportRequestCategoryCheck = (event, isInputChecked) => {
    const filterCategory = 'reportRequestCategory';
    const filterName = event.target.value;
    if (isInputChecked) this.props.addReportRequestFilter(filterCategory, filterName);
    else this.props.removeReportRequestFilter(filterCategory, filterName);
  };

  render() {
    let {
      dictionary,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory
    } = this.props;

    const category = drawerNames.reportRequestFilter;
    const address =
      drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const leftIconAOI = <LocationOnIcon />;
    const leftIconDataNeeded = <LocalSeeIcon />;
    const subHeaderContentType = (
      <div style={{ display: 'flex' }}>
        <span>{leftIconDataNeeded}</span>
        <span>{dictionary._content_type}</span>
      </div>
    );
    // const subHeaderCategory = (<div style={{ display: 'flex' }}><span>{leftIconDataNeeded}</span><span>{dictionary._category}</span></div>);
    const drawerTitle = (
      <DrawerTitle
        dictionary={dictionary}
        locale={this.props.locale}
        title={dictionary._filters}
        rightIcon="close"
        rightAction={this.rightAction}
      />
    );
    return (
      <section>
        {drawerTitle}
        <div>
          <List>
            <ListItem disabled={true} leftIcon={leftIconAOI}>
              <InputAOI
                category={category}
                address={address}
                drawPoint={drawPoint}
                drawPolygon={drawPolygon}
                getMapDraw={this.props.getMapDraw}
                resetHomeDrawState={resetHomeDrawState}
                setDrawPoint={setDrawPoint}
                setDrawPolygon={setDrawPolygon}
                setDrawCategory={setDrawCategory}
              />
            </ListItem>
          </List>
          <Divider />
          <List>
            <Subheader className="subHeader" children={subHeaderContentType} />
            {FILTERS.reportRequestType ? (
              Object.keys(FILTERS.reportRequestType).map((e, i) => (
                <ListItem
                  style={{ marginLeft: '10px' }}
                  leftCheckbox={
                    <Checkbox
                      checked={
                        this.props.reportRequestFilters.reportRequestType.indexOf(
                          FILTERS.reportRequestType[e]
                        ) >= 0
                      }
                      name={e}
                      label={dictionary[FILTERS.reportRequestType[e]]}
                      onCheck={this.onReportRequestTypeCheck}
                      value={FILTERS.reportRequestType[i]}
                    />
                  }
                  key={i}
                />
              ))
            ) : (
              <div />
            )}
          </List>
          {/* <Divider />
                <List>
                  <Subheader className="subHeader" children={ subHeaderCategory }>
                  </Subheader>
                  { FILTERS.reportRequestCategory ? Object.keys(FILTERS.reportRequestCategory).map((e, i) => (
                        <ListItem 
                            style={ { marginLeft: '10px' } } 
                            leftCheckbox={ 
                                            <Checkbox checked={ this.props.reportRequestFilters.reportRequestCategory.indexOf(FILTERS.reportRequestCategory[e]) >= 0 }
                                                      name={ e } label={ dictionary[FILTERS.reportRequestCategory[e]] } onCheck={ this.onReportRequestCategoryCheck } 
                                                      value={ FILTERS.reportRequestCategory[i] }
                                            /> } 
                            key={ i } />                    
                    )) : <div /> }
                </List>                 */}
        </div>
      </section>
    );
  }
}

export default enhance(ReportRequestDrawerFilter);
