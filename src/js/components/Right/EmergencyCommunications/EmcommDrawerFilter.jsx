import React, { Component } from 'react';
import SuperSelectField from 'material-ui-superselectfield';
import {
  Divider,
  List,
  ListItem,
  Chip,
  Subheader,
  /* Checkbox, */ FlatButton,
  Avatar
} from 'material-ui';
import {
  WarningIcon,
  LocationOnIcon,
  DonutLargeIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import InputAOI from 'js/components/app/drawer/components/InputAOI';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withEmergencyCommunicationFilters, FILTERS } from 'js/modules/emcommFilters';
import { HAZARD_TYPES_LETTERS, HAZARD_TYPES_LABELS } from 'js/components/Home/components/commons';
import { redA700, green800 } from 'material-ui/styles/colors';

import { compose } from 'redux';
const enhance = compose(withLeftDrawer, withEmergencyCommunicationFilters);

const backgroundColorStyle = {
  _is_status_ongoing: green800,
  _is_status_closed: redA700
};

class EmcommDrawerFilter extends Component {
  onEmcommStatusCheck = (event, isInputChecked) => {
    const filterCategory = 'emcommStatus';
    const filterName = event.target.value;
    if (isInputChecked) this.props.addEmcommFilter(filterCategory, filterName);
    else this.props.removeEmcommFilter(filterCategory, filterName);
  };

  rightAction = () => {
    this.props.setOpenSecondary(false);
    this.props.history.push(`/home/${drawerNames.notification}`);
  };

  updateSelectFilters = (selectedItems, args) => {
    const filterCategory = 'hazard';

    if (selectedItems.filter(item => item.value === '_hazard_fire').length > 0)
      this.props.addEmcommFilter(filterCategory, '_hazard_fire');
    else this.props.removeEmcommFilter(filterCategory, '_hazard_fire');

    if (selectedItems.filter(item => item.value === '_hazard_flood').length > 0)
      this.props.addEmcommFilter(filterCategory, '_hazard_flood');
    else this.props.removeEmcommFilter(filterCategory, '_hazard_flood');

    if (selectedItems.filter(item => item.value === '_hazard_landslide').length > 0)
      this.props.addEmcommFilter(filterCategory, '_hazard_landslide');
    else this.props.removeEmcommFilter(filterCategory, '_hazard_landslide');

    if (selectedItems.filter(item => item.value === '_hazard_extremeWeather').length > 0)
      this.props.addEmcommFilter(filterCategory, '_hazard_extremeWeather');
    else this.props.removeEmcommFilter(filterCategory, '_hazard_extremeWeather');

    if (selectedItems.filter(item => item.value === '_hazard_avalanches').length > 0)
      this.props.addEmcommFilter(filterCategory, '_hazard_avalanches');
    else this.props.removeEmcommFilter(filterCategory, '_hazard_avalanches');

    if (selectedItems.filter(item => item.value === '_hazard_earthquake').length > 0)
      this.props.addEmcommFilter(filterCategory, '_hazard_earthquake');
    else this.props.removeEmcommFilter(filterCategory, '_hazard_earthquake');
  };

  updateStatusFilters = (selectedItems, args) => {
    const filterCategory = 'emcommStatus';

    if (selectedItems.filter(item => item.value === '_is_status_ongoing').length > 0)
      this.props.addEmcommFilter(filterCategory, '_is_status_ongoing');
    else this.props.removeEmcommFilter(filterCategory, '_is_status_ongoing');

    if (selectedItems.filter(item => item.value === '_is_status_closed').length > 0)
      this.props.addEmcommFilter(filterCategory, '_is_status_closed');
    else this.props.removeEmcommFilter(filterCategory, '_is_status_closed');
  };

  handleCustomDisplaySelectionsHazard = hazards => hazards =>
    hazards.length > 0 ? (
      hazards.map((item, i) => (
        <div key={i} style={{ display: 'flex', flexWrap: 'wrap' }}>
          <Chip style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteHazard(item)}>
            <Avatar key={i} size={30}>
              {HAZARD_TYPES_LETTERS[HAZARD_TYPES_LABELS[item.value]]}
            </Avatar>
            <span>{item.label}</span>
          </Chip>
        </div>
      ))
    ) : (
      <div key={0} style={{ minHeight: 42, lineHeight: '42px' }}>
        Select a Category
      </div>
    );

  handleCustomDisplaySelectionsStatus = statuses => statuses =>
    statuses.length > 0 ? (
      statuses.map((item, i) => (
        <div key={i} style={{ display: 'flex', flexWrap: 'wrap' }}>
          <Chip style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteStatus(item)}>
            <Avatar
              key={i}
              style={{ margin: 8, width: 16, height: 16 }}
              backgroundColor={backgroundColorStyle[item.value]}
            />
            <span>{item.label}</span>
          </Chip>
        </div>
      ))
    ) : (
      <div key={0} style={{ minHeight: 42, lineHeight: '42px' }}>
        Select a Status
      </div>
    );

  onRequestDeleteHazard = itemToRemove => event => {
    this.props.removeEmcommFilter('hazard', itemToRemove.value);
  };

  onRequestDeleteStatus = itemToRemove => event => {
    this.props.removeEmcommFilter('emcommStatus', itemToRemove.value);
  };

  render() {
    let {
      dictionary,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon
    } = this.props;

    const category = drawerNames.notificationFilter;
    const address =
      drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const leftIconHazardType = <WarningIcon />;
    const leftIconAOI = <LocationOnIcon />;
    const donutLargeIcon = <DonutLargeIcon />;

    const subHeaderStatus = (
      <div style={{ display: 'flex' }}>
        <span>{donutLargeIcon}</span>
        <span>{dictionary._status}</span>
      </div>
    );

    const drawerTitle = (
      <DrawerTitle
        dictionary={dictionary}
        locale={this.props.locale}
        title={dictionary._filters}
        rightIcon="close"
        rightAction={this.rightAction}
      />
    );

    const hazardList = FILTERS.hazard
      .filter(item => item !== '_hazard_unknown' && item !== '_hazard_none')
      .map(item => {
        let hazard = {
          value: item,
          label: this.props.dictionary(item)
        };
        return hazard;
      });

    const statusList = FILTERS.emcommStatus.map((item, i) => {
      let status = {
        value: item,
        label: this.props.dictionary(item)
      };
      return status;
    });

    const hazardListNodes = hazardList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={category.label}>
          <span>{category.label}</span>
        </div>
      );
    });

    const statusListNodes = statusList.map((status, index) => {
      return (
        <div key={index} value={status.value} label={status.label}>
          <span>{status.label}</span>
        </div>
      );
    });

    const hazardsSelected =
      this.props.emcommFilters.hazard.length > 0
        ? this.props.emcommFilters.hazard.map(item => {
            let hazard = {
              value: item,
              label: this.props.dictionary(item)
            };
            return hazard;
          })
        : [];

    const statusesSelected =
      this.props.emcommFilters.emcommStatus.length > 0
        ? this.props.emcommFilters.emcommStatus.map(item => {
            let status = {
              value: item,
              label: this.props.dictionary(item)
            };
            return status;
          })
        : [];

    const superSelectMenuCloseButton = (
      <FlatButton label={dictionary._close} backgroundColor={'red'} />
    );

    const superSelectHazard = (
      <SuperSelectField
        key={0}
        style={{ width: '90%' }}
        name="HazardEmComm"
        multiple
        floatingLabel="Select the Hazard"
        onChange={this.updateSelectFilters}
        value={hazardsSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(hazardsSelected)}
        menuCloseButton={superSelectMenuCloseButton}
      >
        {hazardListNodes}
      </SuperSelectField>
    );

    const superSelectStatus = (
      <SuperSelectField
        key={0}
        style={{ width: '90%', marginLeft: 20 }}
        name="StatusEmComm"
        multiple
        floatingLabel="Select the Status"
        onChange={this.updateStatusFilters}
        value={statusesSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsStatus(statusesSelected)}
        menuCloseButton={superSelectMenuCloseButton}
      >
        {statusListNodes}
      </SuperSelectField>
    );

    return (
      <section>
        {drawerTitle}
        <div>
          <List>
            <ListItem
              key={0}
              disabled={true}
              leftIcon={leftIconHazardType}
              children={superSelectHazard}
            />
          </List>
          <Divider />
          <List>
            <ListItem disabled={true} leftIcon={leftIconAOI}>
              <InputAOI
                category={category}
                address={address}
                drawPoint={drawPoint}
                drawPolygon={drawPolygon}
                getMapDraw={this.props.getMapDraw}
                resetHomeDrawState={resetHomeDrawState}
                setDrawPoint={setDrawPoint}
                setDrawPolygon={setDrawPolygon}
                setDrawCategory={setDrawCategory}
              />
            </ListItem>
          </List>
          <Divider />
          <List>
            <Subheader className="subHeader" children={subHeaderStatus} />
            <ListItem key={0} disabled={true} children={superSelectStatus} />
          </List>
        </div>
      </section>
    );
  }
}

export default enhance(EmcommDrawerFilter);
