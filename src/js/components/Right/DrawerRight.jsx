import './style.scss';
import React, { Component } from 'react';
import { Drawer } from 'material-ui';
import { withRouter } from 'react-router';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import ReportDrawerFilter from 'js/components/Right/Reports/ReportDrawerFilter';
import MapRequestDrawerFilter from 'js/components/Right/MapRequests/MapRequestDrawerFilter';
import EventDrawerFilter from 'js/components/Right/Events/EventDrawerFilter';
import ReportRequestDrawerFilter from 'js/components/Right/ReportRequests/ReportRequestDrawerFilter';
import EmcommDrawerFilter from 'js/components/Right/EmergencyCommunications/EmcommDrawerFilter';
import TaskCreateDrawer from 'js/components/Right/Missions/TaskCreateDrawer';

const enhance = compose(withDictionary, withLeftDrawer);

class DrawerRight extends Component {
  render() {
    const { dictionary, enums } = this.props;

    const {
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon
    } = this.props;

    let currentDrawerType;

    switch (this.props.primaryDrawerType) {
      case drawerNames.report:
        if (this.props.location.search === '?reportRequest')
          currentDrawerType = (
            <ReportRequestDrawerFilter
              drawPoint={drawPoint}
              drawPolygon={drawPolygon}
              setDrawPoint={setDrawPoint}
              setDrawPolygon={setDrawPolygon}
              setDrawCategory={setDrawCategory}
              resetHomeDrawState={resetHomeDrawState}
              getMapDraw={this.props.getMapDraw}
              history={this.props.history}
              dictionary={dictionary}
              enums={enums}
            />
          );
        else
          currentDrawerType = (
            <ReportDrawerFilter
              drawPoint={drawPoint}
              drawPolygon={drawPolygon}
              setDrawPoint={setDrawPoint}
              setDrawPolygon={setDrawPolygon}
              setDrawCategory={setDrawCategory}
              resetHomeDrawState={resetHomeDrawState}
              getMapDraw={this.props.getMapDraw}
              dictionary={dictionary}
              history={this.props.history}
              enums={enums}
            />
          );
        break;
      case drawerNames.event:
        currentDrawerType = (
          <EventDrawerFilter
            getMapDraw={this.props.getMapDraw}
            dictionary={dictionary}
            history={this.props.history}
            enums={enums}
          />
        );
        break;
      case drawerNames.notification:
        currentDrawerType = (
          <EmcommDrawerFilter
            drawPoint={drawPoint}
            drawPolygon={drawPolygon}
            setDrawPoint={setDrawPoint}
            setDrawPolygon={setDrawPolygon}
            setDrawCategory={setDrawCategory}
            resetHomeDrawState={resetHomeDrawState}
            getMapDraw={this.props.getMapDraw}
            dictionary={dictionary}
            history={this.props.history}
            enums={enums}
          />
        );
        break;
      case drawerNames.mission:
        currentDrawerType = (
          <TaskCreateDrawer
            drawPoint={drawPoint}
            drawPolygon={drawPolygon}
            resetHomeDrawState={resetHomeDrawState}
            getMapDraw={this.props.getMapDraw}
            dictionary={dictionary}
            history={this.props.history}
            enums={enums}
            setIsDrawing={this.props.setIsDrawing}
            setDrawTask={this.props.setDrawTask}
          />
        );
        break;
      case drawerNames.mapRequests:
        currentDrawerType = (
          <MapRequestDrawerFilter
            getMapDraw={this.props.getMapDraw}
            dictionary={dictionary}
            history={this.props.history}
            enums={enums}
            drawPoint={drawPoint}
            drawPolygon={drawPolygon}
            resetHomeDrawState={resetHomeDrawState}
            setIsDrawing={this.props.setIsDrawing}
            setDrawPoint={setDrawPoint}
            setDrawPolygon={setDrawPolygon}
            setDrawCategory={setDrawCategory}
          />
        );
        break;
      default:
        break;
    }

    return (
      <div>
        <Drawer
          ref="secondary"
          open={this.props.isSecondaryOpen}
          containerStyle={
            this.props.isSecondaryOpen
              ? {
                  left: '443px',
                  position: 'absolute',
                  boxShadow: 'none',
                  borderRight: '1px solid #F0F0F0',
                  height: 'calc(100%)',
                  width: '379px',
                  overflow: 'hidden'
                }
              : {}
          }
        >
          {currentDrawerType}
        </Drawer>
      </div>
    );
  }
}

export default withRouter(enhance(DrawerRight));
