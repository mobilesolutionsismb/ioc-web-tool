import React, { Component } from 'react';
import { Divider, List, ListItem, Chip, Avatar, FlatButton } from 'material-ui';
import { WarningIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { withEventFilters, FILTERS } from 'js/modules/clientSideEventFilters';
import { drawerNames, withAreaOfInterest } from 'js/modules/AreaOfInterest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import SuperSelectField from 'material-ui-superselectfield';
import { HAZARD_TYPES_LETTERS, HAZARD_TYPES_LABELS } from 'js/components/Home/components/commons';

import { compose } from 'redux';
const enhance = compose(withLeftDrawer, withEventFilters, withAreaOfInterest);

class EventDrawerFilter extends Component {
  rightAction = () => {
    this.props.setOpenSecondary(false);
    this.props.history.push(`/home/${drawerNames.event}`);
  };

  updateSelectFilters = selectedItems => {
    const filterCategory = 'hazard';
    if (selectedItems.filter(item => item.value === '_hazard_fire').length > 0)
      this.props.addEventFilter(filterCategory, '_hazard_fire');
    else this.props.removeEventFilter(filterCategory, '_hazard_fire');

    if (selectedItems.filter(item => item.value === '_hazard_flood').length > 0)
      this.props.addEventFilter(filterCategory, '_hazard_flood');
    else this.props.removeEventFilter(filterCategory, '_hazard_flood');

    if (selectedItems.filter(item => item.value === '_hazard_landslide').length > 0)
      this.props.addEventFilter(filterCategory, '_hazard_landslide');
    else this.props.removeEventFilter(filterCategory, '_hazard_landslide');

    if (selectedItems.filter(item => item.value === '_hazard_extremeWeather').length > 0)
      this.props.addEventFilter(filterCategory, '_hazard_extremeWeather');
    else this.props.removeEventFilter(filterCategory, '_hazard_extremeWeather');

    if (selectedItems.filter(item => item.value === '_hazard_avalanches').length > 0)
      this.props.addEventFilter(filterCategory, '_hazard_avalanches');
    else this.props.removeEventFilter(filterCategory, '_hazard_avalanches');

    if (selectedItems.filter(item => item.value === '_hazard_earthquake').length > 0)
      this.props.addEventFilter(filterCategory, '_hazard_earthquake');
    else this.props.removeEventFilter(filterCategory, '_hazard_earthquake');
  };

  handleCustomDisplaySelectionsHazard = hazards => hazards =>
    hazards.length > 0 ? (
      hazards.map((item, i) => (
        <div key={i} style={{ display: 'flex', flexWrap: 'wrap' }}>
          <Chip style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteHazard(item)}>
            <Avatar key={i} size={30}>
              {HAZARD_TYPES_LETTERS[HAZARD_TYPES_LABELS[item.value]]}
            </Avatar>
            <span>{item.label}</span>
          </Chip>
        </div>
      ))
    ) : (
      <div key={0} style={{ minHeight: 42, lineHeight: '42px' }}>
        Select a Category
      </div>
    );

  onRequestDeleteHazard = itemToRemove => event => {
    this.props.removeEventFilter('hazard', itemToRemove.value);
  };

  render() {
    let { dictionary } = this.props;
    const leftIconHazardType = <WarningIcon />;

    const drawerTitle = (
      <DrawerTitle
        dictionary={dictionary}
        locale={this.props.locale}
        title={dictionary._filters}
        rightIcon="close"
        rightAction={this.rightAction}
      />
    );
    const hazardList = FILTERS.hazard
      .filter(item => item !== '_hazard_unknown' && item !== '_hazard_none')
      .map(item => {
        let hazard = {
          value: item,
          label: this.props.dictionary(item)
        };
        return hazard;
      });

    const hazardListNodes = hazardList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={category.label}>
          <span>{category.label}</span>
        </div>
      );
    });

    const hazardsSelected =
      this.props.feEventFilters.hazard.length > 0
        ? this.props.feEventFilters.hazard.map(item => {
            let hazard = {
              value: item,
              label: this.props.dictionary(item)
            };
            return hazard;
          })
        : [];

    const superSelectMenuCloseButton = (
      <FlatButton label={dictionary._close} backgroundColor={'red'} />
    );

    const superSelectHazard = (
      <SuperSelectField
        key={0}
        style={{ width: '90%' }}
        name="selectHazardEvent"
        multiple
        floatingLabel="Select the Hazard"
        onChange={this.updateSelectFilters}
        value={hazardsSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(hazardsSelected)}
        menuCloseButton={superSelectMenuCloseButton}
      >
        {hazardListNodes}
      </SuperSelectField>
    );

    return (
      <section>
        {drawerTitle}
        <div>
          <List>
            <ListItem disabled={true} leftIcon={leftIconHazardType} children={superSelectHazard} />
          </List>
          <Divider />
        </div>
      </section>
    );
  }
}

export default enhance(EventDrawerFilter);
