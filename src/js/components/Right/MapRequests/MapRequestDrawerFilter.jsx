import React, { Component } from 'react';
import {
  Divider,
  List,
  ListItem,
  Subheader,
  Checkbox,
  Chip,
  Avatar,
  FlatButton
} from 'material-ui';
import {
  WarningIcon,
  LocationOnIcon,
  PeopleIcon,
  DonutLargeIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import SuperSelectField from 'material-ui-superselectfield';
import InputAOI from 'js/components/app/drawer/components/InputAOI';
import { withMapRequestFilters, FILTERS } from 'js/modules/mapRequestFilters';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { HAZARD_TYPES_LETTERS, HAZARD_TYPES_LABELS } from 'js/components/Home/components/commons';
import { redA700, green800 } from 'material-ui/styles/colors';
import { compose } from 'redux';

const enhance = compose(withLeftDrawer, withMapRequestFilters);
const backgroundColorStyle = {
  _is_status_ongoing: green800,
  _is_status_closed: redA700
};
class MapRequestDrawerFilter extends Component {
  onReportTypeCheck = (event, isInputChecked) => {
    const filterCategory = 'reportType';
    const filterName = event.target.value;
    if (isInputChecked) this.props.addMapRequestFilter(filterCategory, filterName);
    else this.props.removeMapRequestFilter(filterCategory, filterName);
  };

  onReportStatusCheck = (event, isInputChecked) => {
    const filterCategory = 'status';
    const filterName = event.target.value;
    if (isInputChecked) this.props.addMapRequestFilter(filterCategory, filterName);
    else this.props.removeMapRequestFilter(filterCategory, filterName);
  };

  rightAction = () => {
    this.props.setOpenSecondary(false);
    this.props.history.push(`/home/${drawerNames.mapRequests}`);
  };

  updateSelectFilters = (selectedItems, args) => {
    const filterCategory = 'hazard';

    if (selectedItems.filter(item => item.value === '_hazard_fire').length > 0)
      this.props.addMapRequestFilter(filterCategory, '_hazard_fire');
    else this.props.removeMapRequestFilter(filterCategory, '_hazard_fire');

    if (selectedItems.filter(item => item.value === '_hazard_flood').length > 0)
      this.props.addMapRequestFilter(filterCategory, '_hazard_flood');
    else this.props.removeMapRequestFilter(filterCategory, '_hazard_flood');

    if (selectedItems.filter(item => item.value === '_hazard_landslide').length > 0)
      this.props.addMapRequestFilter(filterCategory, '_hazard_landslide');
    else this.props.removeMapRequestFilter(filterCategory, '_hazard_landslide');

    if (selectedItems.filter(item => item.value === '_hazard_extremeWeather').length > 0)
      this.props.addMapRequestFilter(filterCategory, '_hazard_extremeWeather');
    else this.props.removeMapRequestFilter(filterCategory, '_hazard_extremeWeather');

    if (selectedItems.filter(item => item.value === '_hazard_avalanches').length > 0)
      this.props.addMapRequestFilter(filterCategory, '_hazard_avalanches');
    else this.props.removeMapRequestFilter(filterCategory, '_hazard_avalanches');

    if (selectedItems.filter(item => item.value === '_hazard_earthquake').length > 0)
      this.props.addMapRequestFilter(filterCategory, '_hazard_earthquake');
    else this.props.removeMapRequestFilter(filterCategory, '_hazard_earthquake');
  };

  updateStatusFilters = (selectedItems, args) => {
    const filterCategory = 'temporalStatus';

    if (selectedItems.filter(item => item.value === '_is_status_ongoing').length > 0)
      this.props.addMapRequestFilter(filterCategory, '_is_status_ongoing');
    else this.props.removeMapRequestFilter(filterCategory, '_is_status_ongoing');

    if (selectedItems.filter(item => item.value === '_is_status_closed').length > 0)
      this.props.addMapRequestFilter(filterCategory, '_is_status_closed');
    else this.props.removeMapRequestFilter(filterCategory, '_is_status_closed');
  };
  handleCustomDisplaySelectionsHazard = hazards => hazards =>
    hazards.length > 0 ? (
      hazards.map((item, i) => (
        <div key={i} style={{ display: 'flex', flexWrap: 'wrap' }}>
          <Chip key={i} style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteHazard(item)}>
            <Avatar key={i} size={30}>
              {HAZARD_TYPES_LETTERS[HAZARD_TYPES_LABELS[item.value]]}
            </Avatar>
            <span>{item.label}</span>
          </Chip>
        </div>
      ))
    ) : (
      <div key={0} style={{ minHeight: 42, lineHeight: '42px' }}>
        Select a Category
      </div>
    );

  handleCustomDisplaySelectionsStatus = statuses => statuses =>
    statuses.length > 0 ? (
      statuses.map((item, i) => (
        <div key={i} style={{ display: 'flex', flexWrap: 'wrap' }}>
          <Chip style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteStatus(item)}>
            <Avatar
              key={i}
              style={{ margin: 8, width: 16, height: 16 }}
              backgroundColor={backgroundColorStyle[item.value]}
            />
            <span>{item.label}</span>
          </Chip>
        </div>
      ))
    ) : (
      <div key={0} style={{ minHeight: 42, lineHeight: '42px' }}>
        Select a Status
      </div>
    );
  onRequestDeleteStatus = itemToRemove => event => {
    this.props.removeMapRequestFilter('temporalStatus', itemToRemove.value);
  };
  onRequestDeleteHazard = itemToRemove => event => {
    this.props.removeMapRequestFilter('hazard', itemToRemove.value);
  };
  isCheckDisabled = (item, hazardsSelected) => {
    if (item === '_is_type_delineation' && hazardsSelected.find(x => x.value === '_hazard_fire')) {
      if (this.props.mapRequestFilters.reportType.indexOf(item) >= 0) {
        this.props.removeMapRequestFilter('reportType', item);
      }
      return true;
    }
    if (item === '_is_type_burned_area' && hazardsSelected.find(x => x.value === '_hazard_flood')) {
      if (this.props.mapRequestFilters.reportType.indexOf(item) >= 0) {
        this.props.removeMapRequestFilter('reportType', item);
      }
      return true;
    }
    return false;
  };

  render() {
    let {
      dictionary,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon
    } = this.props;

    const category = drawerNames.mapRequestFilter;
    const address =
      drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const leftIconHazardType = <WarningIcon />;
    const leftIconAOI = <LocationOnIcon />;
    const leftIconContentType = <PeopleIcon />;
    const donutLargeIcon = <DonutLargeIcon />;
    const subHeaderContentType = (
      <div style={{ display: 'flex' }}>
        <span>{leftIconContentType}</span>
        <span>{dictionary._content_type}</span>
      </div>
    );
    const subHeaderReportStatus = (
      <div style={{ display: 'flex' }}>
        <span>{donutLargeIcon}</span>
        <span>{dictionary._status}</span>
      </div>
    );

    const hazardList = FILTERS.hazard
      .filter(item => item !== '_hazard_unknown' && item !== '_hazard_none')
      .map(item => {
        let hazard = {
          value: item,
          label: this.props.dictionary(item)
        };
        return hazard;
      });
    const statusList = FILTERS.temporalStatus.map((item, i) => {
      let temporalStatus = {
        value: item,
        label: this.props.dictionary(item)
      };
      return temporalStatus;
    });
    const statusesSelected =
      this.props.mapRequestFilters.temporalStatus.length > 0
        ? this.props.mapRequestFilters.temporalStatus.map(item => {
            let temporalStatus = {
              value: item,
              label: this.props.dictionary(item)
            };
            return temporalStatus;
          })
        : [];
    const hazardListNodes = hazardList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={category.label}>
          <span key={index}>{category.label}</span>
        </div>
      );
    });
    const statusListNodes = statusList.map((status, index) => {
      return (
        <div key={index} value={status.value} label={status.label}>
          <span>{status.label}</span>
        </div>
      );
    });
    const hazardsSelected =
      this.props.mapRequestFilters.hazard.length > 0
        ? this.props.mapRequestFilters.hazard.map(item => {
            let hazard = {
              value: item,
              label: this.props.dictionary(item)
            };
            return hazard;
          })
        : [];

    const superSelectMenuCloseButton = (
      <FlatButton label={dictionary._close} backgroundColor={'red'} />
    );
    const drawerTitle = (
      <DrawerTitle
        dictionary={dictionary}
        locale={this.props.locale}
        title={dictionary._filters}
        rightIcon="close"
        rightAction={this.rightAction}
      />
    );
    const superSelectHazard = (
      <SuperSelectField
        key={0}
        style={{ width: '90%' }}
        name="selectHazardReport"
        multiple
        floatingLabel="Select the Hazard"
        onChange={this.updateSelectFilters}
        value={hazardsSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(hazardsSelected)}
        menuCloseButton={superSelectMenuCloseButton}
      >
        {hazardListNodes}
      </SuperSelectField>
    );
    const superSelectStatus = (
      <SuperSelectField
        key={0}
        style={{ width: '90%', marginLeft: 20 }}
        name="StatusEmComm"
        multiple
        floatingLabel="Select the Status"
        onChange={this.updateStatusFilters}
        value={statusesSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsStatus(statusesSelected)}
        menuCloseButton={superSelectMenuCloseButton}
      >
        {statusListNodes}
      </SuperSelectField>
    );

    return (
      <section>
        {drawerTitle}
        <div>
          <List>
            <ListItem
              key={0}
              disabled={true}
              leftIcon={leftIconHazardType}
              children={superSelectHazard}
            />
            <ListItem key={1} disabled={true} leftIcon={leftIconAOI}>
              <InputAOI
                category={category}
                address={address}
                drawPoint={drawPoint}
                drawPolygon={drawPolygon}
                getMapDraw={this.props.getMapDraw}
                resetHomeDrawState={resetHomeDrawState}
                setDrawPoint={setDrawPoint}
                setDrawPolygon={setDrawPolygon}
                setDrawCategory={setDrawCategory}
              />
            </ListItem>
          </List>
          <Divider />
          <List>
            <Subheader className="subHeader" children={subHeaderContentType} />
            {FILTERS.reportType ? (
              Object.keys(FILTERS.reportType).map((e, i) => (
                <ListItem
                  style={{ marginLeft: '10px' }}
                  leftCheckbox={
                    <Checkbox
                      style={{ width: 200 }}
                      disabled={this.isCheckDisabled(FILTERS.reportType[e], hazardsSelected)}
                      checked={
                        this.props.mapRequestFilters.reportType.indexOf(FILTERS.reportType[e]) >= 0
                      }
                      value={FILTERS.reportType[e]}
                      name={dictionary[FILTERS.reportType[e]]}
                      label={dictionary[FILTERS.reportType[e]]}
                      onCheck={this.onReportTypeCheck}
                    />
                  }
                  key={i}
                />
              ))
            ) : (
              <div />
            )}
          </List>
          <Divider />
          <List>
            <Subheader className="subHeader" children={subHeaderReportStatus} />
            <ListItem key={0} disabled={true} children={superSelectStatus} />
          </List>
        </div>
      </section>
    );
  }
}

export default enhance(MapRequestDrawerFilter);
