import React, { Component } from 'react';
import { Divider, FlatButton } from 'material-ui';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { drawerNames, withAreaOfInterest } from 'js/modules/AreaOfInterest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withTaskCreate } from 'js/modules/taskCreate';
import { withMissionCreate } from 'js/modules/missionCreate';
import DrawerFooter from 'js/components/app/drawer/DrawerFooter';
import { withMessages, withLoader } from 'js/modules/ui';
import { MissionDetails } from 'js/components/Right/Missions/commons';
import TaskNewCard from 'js/components/TaskNewCard/TaskNewCard';
import wellknown from 'wellknown';
import { withMissions, withOrganizationUnits } from 'ioc-api-interface';
import { point, polygon } from '@turf/helpers';

import { compose } from 'redux';
const enhance = compose(
  withLeftDrawer,
  withAreaOfInterest,
  withTaskCreate,
  withMissionCreate,
  withMessages,
  withMissions,
  withLoader,
  withOrganizationUnits
);

class TaskCreateDrawer extends Component {
  rightAction = () => {
    this.props.setOpenSecondary(false);
    this.props.history.push(`/home/${drawerNames.mission}?mission_create`);
  };

  _abortTaskCreation = () => {
    this.props.resetTask();
    this.rightAction();
  };

  _deleteTask = taskDescription => {
    const mapDraw = this.props.getMapDraw();

    const id = this.props.newMission.tasks.find(task => task.description === taskDescription)
      .drawTask.id;
    if (mapDraw && id) mapDraw.delete(id);

    this.props.removeTaskFromMission(taskDescription);
  };

  _createMission = async () => {
    let newMission = Object.assign({}, this.props.newMission);

    if (newMission.title.length === 0) {
      this.props.pushMessage('Title mandatory', 'error', null);
      return;
    } else if (newMission.title.length > 50) {
      this.props.pushMessage('Title too long', 'error', null);
      return;
    } else newMission.title = newMission.title.toUpperCase();

    if (newMission.teamId == null && newMission.firstReponderId == null) {
      this.props.pushMessage('Assign To field mandatory', 'error', null);
      return;
    }

    if (newMission.tasks.length === 0) {
      this.props.pushMessage('Create at least a task', 'error', null);
      return;
    }

    if (
      !this.props.drawPolygon.id &&
      (newMission.areaOfInterest == null || newMission.areaOfInterest === '')
    ) {
      this.props.pushMessage('Area of interest mandatory', 'error', null);
      return;
    }
    if (newMission.id === 0 && this.props.drawPolygon.id)
      newMission.areaOfInterest = wellknown.stringify(polygon(this.props.drawPolygon.coordinates));

    if (newMission.id === 0 && (!newMission.coordinates || newMission.coordinates === null)) {
      newMission.coordinates = wellknown.stringify(point(this.props.drawPoint.coordinates));
    }

    try {
      if (newMission.id === 0) {
        let result = await this.props.createMission(
          newMission,
          this.props.loadingStart,
          this.props.loadingStop
        );
        if (result != null) {
          this.props.pushMessage('_new_mission_create', 'success', null);
          this.props.resetMission();
          this.props.resetHomeDrawState();
          const mapDraw = this.props.getMapDraw();
          if (mapDraw) mapDraw.deleteAll();

          this.props.setOpenSecondary(false);
          this.props.history.replace(`/home/${drawerNames.mission}`);
        } else this.props.pushError(new Error('Error: cannot create mission'));
      }
      // else {
      //   let result = await this.props.updateMission(
      //     newMission,
      //     this.props.loadingStart,
      //     this.props.loadingStop
      //   );
      //   if (result != null) this.props.pushMessage('_new_mission_update', 'success', null);
      //   else this.props.pushError(new Error('Error: cannot create mission'));
      // }
    } catch (err) {
      this.props.loadingStop();
      if (err && err.details && err.details.serverDetails && err.details.serverDetails.error)
        this.props.pushError(err.details.serverDetails.error.message);
      else this.props.pushError(err);
    }
  };

  _openTaskCreateDialog = () => {
    const mapDraw = this.props.getMapDraw();
    if (mapDraw)
      mapDraw.changeMode('task_mode', {
        toggleTaskPopup: this.props.toggleTaskPopup
      });
  };

  render() {
    let { dictionary, locale, newMission, teams } = this.props;

    const { teamId, firstResponderId } = newMission;
    const team = teams.find(team => team.id === teamId);
    const address =
      this.props.drawPoint && this.props.drawPoint.coordinates
        ? '[ ' +
          Math.round(this.props.drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(this.props.drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const assignToLabel = team ? team.displayName : firstResponderId ? 'First Responder Id' : '';
    const drawerTitle = (
      <DrawerTitle
        dictionary={dictionary}
        locale={this.props.locale}
        title={dictionary._new_mission}
        rightIcon="close"
        rightAction={this.rightAction}
      />
    );
    const footerDrawer = (
      <DrawerFooter
        entityId={this.props.newMission.id}
        leftLabel={dictionary._abort}
        rightLabel={dictionary._create}
        apply={this._createMission}
        // clear={this._abortTaskCreation}
        customStyle={{ backgroundColor: 'black' }}
      />
    );

    const missionDetail = (
      <MissionDetails
        address={address}
        mission={newMission}
        dictionary={dictionary}
        locale={locale}
        assignToLabel={assignToLabel}
      />
    );

    const taskHeader = (
      <div style={{ display: 'flex', justifyContent: 'space-between', margin: 10 }}>
        <span>
          TASKS: <span style={{ fontSize: 12 }}> {newMission.tasks.length} items</span>
        </span>
        <FlatButton
          label={dictionary._add_task}
          primary={true}
          onClick={this._openTaskCreateDialog}
          style={{ color: 'white', backgroundColor: 'royalblue' }}
        />
      </div>
    );

    const taskCardList = newMission.tasks.map((feature, i) => {
      return (
        <div key={i} style={{ marginBottom: 5 }}>
          <TaskNewCard
            locale={locale}
            dictionary={dictionary}
            feature={feature}
            palette={this.props.palette}
            history={this.props.history}
            deleteSelectedTask={descr => this._deleteTask(descr)}
          />
        </div>
      );
    });

    return (
      <section style={{ backgroundColor: 'black' }}>
        {drawerTitle}
        <div style={{ overflowY: 'auto', overflowX: 'hidden', height: '80vh' }}>
          {missionDetail}
          <Divider />
          {taskHeader}
          {taskCardList}
        </div>
        {footerDrawer}
      </section>
    );
  }
}

export default enhance(TaskCreateDrawer);
