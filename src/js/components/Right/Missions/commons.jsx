import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { PeopleIcon, LocationOnIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';

const iconStyle = { fontSize: '18px', color: 'lightgrey' };
const fontStyle = { width: 24, height: 24, padding: 0 };
const memberStyle = { display: 'flex', alignItems: 'center', fontSize: 12 };

export const MissionDetails = muiThemeable()(
  ({ dictionary, mission, locale, assignToLabel, address }) => (
    <div
      style={{
        margin: 10,
        wordBreak: 'break-all',
        display: 'flex',
        paddingTop: '20px',
        flexDirection: 'column',
        fontSize: 13
      }}
    >
      <span style={{ marginBottom: 20 }}>{mission.title}</span>
      <div style={memberStyle}>
        <LocationOnIcon style={fontStyle} iconStyle={iconStyle} />
        {address}
      </div>
      <div style={memberStyle}>
        <PeopleIcon style={fontStyle} iconStyle={iconStyle} />
        <span>{assignToLabel}</span>
      </div>
    </div>
  )
);
