import { Marker } from 'mapbox-gl';

class ClusterMarkers {
  constructor(iconSize, markerFactory, onMarkerClick) {
    const _iconOffset = [-iconSize.x * 0.5, -iconSize.y * 0.5];
    const _markers = [];
    this.markerFactory = markerFactory;
    this.onMarkerClick = onMarkerClick;
    this.clusters = [];

    Object.defineProperties(this, {
      iconOffset: {
        enumerable: true,
        configurable: false,
        get: () => _iconOffset
      },
      markers: {
        enumerable: true,
        configurable: false,
        get: () => _markers
      }
    });
  }

  addMarkers = (features, map) => {
    let count = features.length;
    if (count === 0) {
      return Promise.resolve();
    } else {
      return new Promise(done => {
        const onMarkerClick = this.onMarkerClick;
        this.clusters = features;
        features.forEach(feature => {
          const markerElement = this.markerFactory(
            feature.properties.categories,
            feature.properties.point_count
          );
          markerElement.addEventListener('click', function(e) {
            e.stopImmediatePropagation();
            e.stopPropagation();
            onMarkerClick(feature, map.project(feature.geometry.coordinates));
          });
          if (!IS_CORDOVA) {
            markerElement.addEventListener('mouseenter', () => {
              markerElement.style.cursor = 'pointer';
            });
            markerElement.addEventListener('mouseleave', () => {
              markerElement.style.cursor = '';
            });
          }
          const marker = new Marker(markerElement, { offset: this.iconOffset }).setLngLat(
            feature.geometry.coordinates
          );
          marker.addTo(map);
          this.markers.push(marker);
          count--;
          if (count === 0) {
            done();
          }
        });
      });
    }
  };

  removeMarkers = () => {
    let count = this.markers.length;
    if (count === 0) {
      return Promise.resolve();
    } else {
      return new Promise(done => {
        while (count--) {
          let m = this.markers.splice(count, 1);
          if (m.length) {
            m[0].remove();
          }
          if (count === 0) {
            done();
          }
        }
      });
    }
  };

  updateMarkers = (features, map) => {
    return this.removeMarkers().then(() => {
      return this.addMarkers(features, map);
    });
  };
  getClusters = () => {
    return this.clusters;
  };
}

export default ClusterMarkers;
