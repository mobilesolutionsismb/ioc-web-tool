import React, { Component } from 'react';
import { List } from 'material-ui';
import styled from 'styled-components';
import { guessDate } from 'js/utils/localizeDate';
import { logFatal, logMain } from 'js/utils/log';
import isPojo from 'is-pojo';
import decamelize from 'decamelize';

const Container = styled.div`
  max-width: 100%;
  max-height: 600px;
  overflow: auto;
`;

const MetadataListItem = styled.div`
  width: 100%;
  margin-bottom: 1em;
`;

const KeyItem = styled.span`
  text-transform: capitalize;
  font-weight: bold;
  width: 100%;
  color: ${props => props.palette.textColor};
`;

const ValueItem = styled.span`
  width: 100%;
  opacity: 0.85;
  padding-left: 0.5em;
`;

export class MetadataPanel extends Component {
  state = {
    metadataError: false
  };

  componentDidCatch(err) {
    logFatal(err);
    this.setState({ metadataError: true });
  }

  _getKeyName(key, prefix = null) {
    return typeof prefix === 'string' ? `${prefix}.${key}` : key;
  }

  _reduceMetadata(metadata, key = null) {
    return Object.keys(metadata).reduce((allItems, nextMetdataKey) => {
      const value = metadata[nextMetdataKey];
      const nextKeyName = this._getKeyName(nextMetdataKey, key);
      if (Array.isArray(value)) {
        const flat = value
          .map(v => this._reduceMetadata(v, nextKeyName))
          .reduce((pre, next) => {
            pre = { ...pre, ...next };
            return pre;
          }, {});
        allItems = { ...allItems, ...flat };
      } else if (isPojo(value)) {
        const flat = this._reduceMetadata(value, nextKeyName);
        allItems = { ...allItems, ...flat };
      } else {
        allItems[nextKeyName] = guessDate(value, undefined, 'LLLL Z');
      }
      return allItems;
    }, {});
  }

  _flattenMetadata(metadata) {
    const _metadata = Array.isArray(metadata) ? metadata : [metadata];
    return _metadata.map((meta, index) => Object.entries(this._reduceMetadata(meta, index)));
  }

  render() {
    const { metadata, metadataId, palette } = this.props;
    const items = this._flattenMetadata(metadata);
    logMain(metadata, items);
    if (this.state.metadataError) {
      return <div />;
    } else {
      return (
        <Container>
          {items.map((metadata, i) => (
            <List key={`m-${i}`} style={{ overflow: 'auto' }}>
              {metadata.map(entry => {
                const key = entry[0];
                const value = entry[1];
                return (
                  <MetadataListItem key={`m-${i}-${key}`} className="metadata-list-item">
                    <KeyItem palette={palette}>{decamelize(key, ' ')}</KeyItem>
                    <ValueItem>{value}</ValueItem>
                  </MetadataListItem>
                );
              })}
            </List>
          ))}
          {items.length === 0 && <h4>NO VALID METDATA FOUND FOR {metadataId}</h4>}
        </Container>
      );
    }
  }
}
