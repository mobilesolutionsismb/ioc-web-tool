// import React from 'react';
import styled from 'styled-components';
import { iReactTheme } from 'js/startup/iReactTheme';
import { fade } from 'material-ui/utils/colorManipulator';

export const TSWidth = 400; // px
export const TSHeight = 72; // px

export const TSWMargin = 8; // px
export const TSPadding = 4; // px

// Widget that contains all the time sliders
export const TSWidgetContainer = styled.div`
  position: absolute;
  left: ${$props => (!$props.isPrimaryOpen ? '72px' : '450px')};
  z-index: 100;
  padding: ${TSPadding / 2}px ${TSPadding}px;
  background: rgba(0, 0, 0, 0.3);
  transition: all 350ms cubic-bezier(0.165, 0.84, 0.44, 1);
`;

export const TSWidgetContainerInner = styled.div`
  overflow-x: auto;
  /*    overflow-y: hidden; */
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  /* justify-content: center; */
  width: 100%;
  height: 100%;
  max-width: 100%;
  max-height: 100%;
`;

// A single time slider
export const TSContainer = styled.div`
  margin: ${$props =>
    $props.lastItem ? `${TSPadding}px auto auto ${TSPadding}px` : `${TSPadding}px`};
  /* overflow: hidden; */
  width: ${TSWidth}px;
  height: ${TSHeight}px;
  max-width: ${TSWidth}px;
  max-height: ${TSHeight}px;
  min-width: ${TSWidth}px;
  min-height: ${TSHeight}px;
  /*     &:last-child{
        margin-bottom: auto;
        margin-right: auto;
    }
 */
`;

export const TSContainerInner = styled.div`
  color: ${props => props.theme.palette.textColor};
  background-color: ${props => props.theme.palette.backgroundColor};
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  user-select: none;
`;

const TSElement = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`;

// Map Request Indicator
const MRIndicatorWidth = 6; // px
export const TSMapRequestIndicator = styled(TSElement)`
  width: ${MRIndicatorWidth}px;
  background-color: ${props =>
    props.isMapRequest ? iReactTheme.palette.mapRequest : 'transparent'};
`;

export const TSHazardAndType = styled(TSElement)`
  width: calc(30% - ${MRIndicatorWidth}px);
  border-right: 1px solid ${fade(iReactTheme.palette.textColor, 0.4)};
  & span {
    width: 100%;
    box-sizing: border-box;
    text-align: right;
    padding: 0 8px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  & span.hazard {
    text-transform: uppercase;
  }
`;

export const TSControls = styled(TSElement)`
  width: 60%;
  border-right: 1px solid ${fade(iReactTheme.palette.textColor, 0.4)};
`;

export const TSActions = styled(TSElement)`
  width: 10%;
  justify-content: flex-start;
  & span.quantity {
    width: 100%;
    text-align: center;
    margin: 8px 0;
  }
`;

export const TSLayerControls = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 50%;
  padding: 0 4px;
  box-sizing: border-box;
`;

export const TSSliderContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: calc(100% - ${3 * 24}px);
  position: relative;
`;

// const TSThickBar = styled.div`
//     position: absolute;
//     width: 80%;
//     left: 10%;
//     height: 100%;
//     box-sizing: border-box;
// `;

// const TSThickBarThick = styled.span`
//     position: relative;
//     box-sizing: border-box;
//     width: 1px;
//     height: 20px;
//     background: white;
//     display: inline-block;
//     left: ${props => props.left}%;
// `;

// export const TSThicks = ({ thickPositions }) =>
//     <TSThickBar className="thicks-bar">{
//         thickPositions.map((tp, i) => <TSThickBarThick className="bar-thick" key={i} left={tp} />)
//     }</TSThickBar>;

export const TSLayerName = styled.span`
  width: 100%;
  box-sizing: border-box;
  overflow-x: hidden;
  padding: 0 4px;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: 0.75em;
  line-height: 1.5em;
  font-weight: 200;
  height: 25%;
  text-align: center;
`;

export const TSDatePlusContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  box-sizing: border-box;
  padding: 0 8px;
  height: 25%;
  font-size: 0.9em;
  align-items: center;
  justify-content: space-between;
`;

export const TSQuantity = styled(TSElement)`
  width: 100%;
  font-size: 0.8em;
  overflow-x: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;
