import React, { Component } from 'react';
import { IconButton, Slider, FontIcon } from 'material-ui';
import { CloseIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { tileJSONIfy } from 'js/utils/layerUtils';
import { LngLatBounds } from 'mapbox-gl';

import nop from 'nop';
import {
  TSContainer,
  TSContainerInner,
  TSMapRequestIndicator,
  TSHazardAndType,
  TSControls,
  TSLayerControls,
  TSDatePlusContainer,
  TSSliderContainer,
  // TSThicks,
  TSLayerName,
  TSQuantity,
  TSActions
} from './commons';
import { LocalizedDate } from './LocalizedDate';
import { iReactTheme } from 'js/startup/iReactTheme';
import wellknown from 'wellknown';
import bbox from '@turf/bbox';
import centroid from '@turf/centroid';
import intersect from '@turf/intersect';
import bboxPolygon from '@turf/bbox-polygon';
import booleanWithin from '@turf/boolean-within';
// import booleanContains from '@turf/boolean-contains'; //  True if the second geometry is completely contained by the first geometry
import { feature } from '@turf/helpers';
import { logError, logMain } from 'js/utils/log';
import { updateRasterLayersBBox } from 'js/components/Home2/components/Map/mapboxStyles/addFeatureLayers';

function toGeoJSONFeature() {
  return bboxPolygon([this.getWest(), this.getSouth(), this.getEast(), this.getNorth()]);
}

LngLatBounds.prototype.toGeoJSONFeature = toGeoJSONFeature;

// Enable experimental wmts by setting .env LAYER_MODE=true
// however currently not all layers render correcly
const LAYER_MODE = USE_WMTS ? 'wmts' : 'wms';
const SCHEME = USE_WMTS ? 'xyz' : 'tms';

const STYLES = {
  date: {
    color: iReactTheme.palette.canvasColor,
    backgroundColor: iReactTheme.palette.primary2Color,
    fontSize: 12,
    lineHeight: '16px',
    height: 16,
    textAlign: 'center',
    width: '80%'
  },
  icon: {
    boxSizing: 'border-box',
    padding: 2,
    width: 24,
    height: '100%'
  },
  legendIcon: {
    boxSizing: 'border-box',
    padding: 2,
    width: 18,
    height: 18,
    fontSize: 16
  },
  layerType: {
    fontSize: '0.725em'
  },
  num: {
    width: 'calc(100% - 8px)',
    textAlign: 'center',
    boxSizing: 'border-box'
  },
  slider: {
    zIndex: 10,
    width: '80%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  sliderBar: {
    margin: 0
  }
};

function parseLayerAOI(layerId, layerAOIWktString, mapCurrentBounds, center, mapMaxBounds = null) {
  let shouldUpdate = false;
  let zoomBounds = mapCurrentBounds;
  let layerAOIcenter = null;
  let layerAOIbounds = null;
  let layerAOIGeomPolygonFeature = null;
  const currentBoundsPolygonFeature = mapCurrentBounds.toGeoJSONFeature();

  try {
    const layerAOIGeom = wellknown.parse(layerAOIWktString);
    layerAOIcenter = centroid(layerAOIGeom);
    layerAOIbounds = bbox(layerAOIGeom);
    layerAOIGeomPolygonFeature = feature(layerAOIGeom, { layerId });
    // True if currentBoundsPolygonFeature is completely inside layerAOIGeomPolygonFeature
    const isAOIBiggerThanCurrentBounds = booleanWithin(
      currentBoundsPolygonFeature,
      layerAOIGeomPolygonFeature
    );
    const currentBoundsInters = intersect(currentBoundsPolygonFeature, layerAOIGeomPolygonFeature);
    const isAOIExternalToCurrentBounds = currentBoundsInters === null;

    if (!isAOIBiggerThanCurrentBounds || isAOIExternalToCurrentBounds) {
      if (mapMaxBounds) {
        const maxBoundPolygonFeature = mapMaxBounds.toGeoJSONFeature();
        const maxBoundsInters = intersect(maxBoundPolygonFeature, layerAOIGeomPolygonFeature);
        const isAOIBiggerThanMaxBounds = booleanWithin(
          maxBoundPolygonFeature,
          layerAOIGeomPolygonFeature
        );

        logMain('isAOIBiggerThanMaxBounds', isAOIBiggerThanMaxBounds, 'inters', maxBoundsInters);
        if (isAOIBiggerThanMaxBounds) {
          zoomBounds = mapCurrentBounds;
          shouldUpdate = false;
        } else if (maxBoundsInters) {
          const intersBbox = bbox(maxBoundsInters);
          zoomBounds = LngLatBounds.convert([
            [intersBbox[0], intersBbox[1]],
            [intersBbox[2], intersBbox[3]]
          ]);
          shouldUpdate = true;
        } else {
          zoomBounds = LngLatBounds.convert([
            [layerAOIbounds[0], layerAOIbounds[1]],
            [layerAOIbounds[2], layerAOIbounds[3]]
          ]);
          shouldUpdate = true;
        }
      } else {
        zoomBounds = LngLatBounds.convert([
          [layerAOIbounds[0], layerAOIbounds[1]],
          [layerAOIbounds[2], layerAOIbounds[3]]
        ]);
        shouldUpdate = false;
      }
    } else {
      shouldUpdate = false;
    }
  } catch (err) {
    logError(err);
  }

  return {
    bounds: layerAOIbounds,
    center: layerAOIcenter,
    zoomBounds,
    shouldUpdate,
    layerBoundsFeature: layerAOIGeomPolygonFeature
  };
}

// TODO understand if this is a param (explicit, or implicit from the data)
const UPDATE_INTERVAL = 10000; //10"

class TimeSlider extends Component {
  _interval = null;
  _unmounting = false;

  state = {
    playing: false, // controls only the autoplay
    playingIndex: 0 // controls the layer displayed
  };

  static defaultProps = {
    defaultGeoServerURL: null,
    openLegendByLayerName: nop,
    id: -1,
    isMapRequest: false,
    isCopernicus: false,
    layers: []
  };

  _startInterval = () => {
    if (this._interval === null) {
      this._interval = setInterval(this._next, UPDATE_INTERVAL);
    }
  };

  _stopInterval = () => {
    if (this._interval !== null) {
      clearInterval(this._interval);
      this._interval = null;
    }
  };

  componentWillUnmount() {
    this._unmounting = true;
    this._stopInterval();
    this._removeLayerFromMap(this.props.id);
  }

  componentDidMount() {
    if (!this.props.defaultGeoServerURL) {
      throw new Error('Missing defaultGeoServerURL!');
    }
    const layer = this.props.layers[this.state.playingIndex];
    this._updateLayerOnMap(layer, true);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.id !== this.props.id && this.props.id !== null) {
      //hope it's enough
      this._pause();
      this._removeLayerFromMap(prevProps.id);
      this.setState({ playingIndex: 0 });
      this._updateLayerOnMap(this.props.layers[0]);
    } else if (prevState.playingIndex !== this.state.playingIndex) {
      const layer = this.props.layers[this.state.playingIndex];
      this._updateLayerOnMap(layer);
    }
  }

  componentDidCatch(error) {
    logError(`TimeSlider-${this.props.id} error`, error);
    if (IS_PRODUCTION) {
      // log to app insight
      appInsights.trackException(
        error,
        'TimeSlider::componentDidCatch',
        this.props,
        {},
        SeverityLevel.Error
      );
    }
  }

  _makeSourceName = name => name.toLocaleLowerCase().replace(' ', '_');

  _removeLayerFromMap = name => {
    const map = this.props.map;
    if (map && map.getStyle()) {
      updateRasterLayersBBox(map, name, null);

      const layer = map.getLayer(name);
      if (layer) {
        // Add a source
        map.removeLayer(name).removeSource(name);
        this.props.onLayerUpdated();
      }
    }
  };

  _addOrUpdateLayerToMap = (
    layer,
    layerGroupId,
    hasMergedLayerNames = false,
    zoomToLayerIfNeeded = false
  ) => {
    const map = this.props.map;
    if (map) {
      const name = layerGroupId;
      const mapboxLayer = map.getLayer(name);
      const geoServerURL = GEOSERVER_URL
        ? GEOSERVER_URL
        : layer.url || this.props.defaultGeoServerURL;
      const boundsData = parseLayerAOI(
        name,
        layer.areaOfInterest,
        map.getBounds(),
        map.getCenter(),
        map.getMaxBounds()
      );

      updateRasterLayersBBox(map, name, boundsData.layerBoundsFeature);

      const layerConfig = tileJSONIfy(
        layer.name,
        geoServerURL.replace(/\/$/, ''),
        hasMergedLayerNames ? 'wms' : LAYER_MODE,
        hasMergedLayerNames ? 'tms' : SCHEME,
        boundsData.bounds,
        boundsData.center,
        map.getMinZoom(),
        map.getMaxZoom()
      );
      if (mapboxLayer) {
        // Add a source - only GeoJSON source has setData()
        map.removeLayer(name).removeSource(name);
      }
      map.addLayer(
        {
          id: name,
          type: 'raster',
          source: {
            type: 'raster',
            ...layerConfig,
            tileSize: 256
          }
        },
        'clusters'
      ); // add before the clusters
      if (zoomToLayerIfNeeded) {
        if (!map.isMoving() && boundsData.zoomBounds && boundsData.shouldUpdate) {
          map.fitBounds(boundsData.zoomBounds, { padding: 200, maxZoom: map.getMaxZoom() });
        }
      }
      this.props.onLayerUpdated();
    }
  };

  _updateLayerOnMap = (layer, firstTime = false) => {
    // TODO update map datasource for this layer
    //update layer source on this.props.map
    logMain(`TimeSlider-${this.props.id} layer updated`, layer);
    this._addOrUpdateLayerToMap(layer, this.props.id, this.props.isCopernicus, firstTime);
  };

  _play = () => {
    this.setState({ playing: true });
    this._next();
    logMain(`TimeSlider-${this.props.id} ▶️ Play`);
    this._startInterval();
  };

  _pause = () => {
    this.setState({ playing: false });
    logMain(`TimeSlider-${this.props.id} ⏹️ Paused`);
    this._stopInterval();
  };

  _togglePlay = () => {
    this.state.playing ? this._pause() : this._play();
  };

  // Implement next in a loop like logic
  _next = () => {
    if (this._unmounting === true) {
      return;
    }
    const max = this.props.layers.length - 1;
    this.setState(prevState => {
      const nextIndex = prevState.playingIndex + 1;
      return { playingIndex: nextIndex > max ? 0 : nextIndex };
    });
  };

  _onSliderChange = (event, value) => {
    // TODO implement a transform/reverse value for handling date/time?
    // this.setState({ slider: transform(value) });
    this.setState({ playingIndex: value });
  };

  // Implement next in a loop like logic
  _previous = () => {
    const min = 0;
    this.setState(prevState => {
      const nextIndex = prevState.playingIndex - 1;
      return { playingIndex: nextIndex < min ? this.props.layers.length - 1 : nextIndex };
    });
  };

  _close = () => {
    this._pause();
    if (typeof this.props.removeWidget === 'function') {
      let id = '';
      if (this.props.code && !this.props.isCopernicus) id = this.props.code;
      else id = this.props.id;
      this.props.removeWidget(id);
    }
    this.props.deselectFeature();
  };

  _showLegend = () => {
    const layer = this.props.layers[this.state.playingIndex];
    this.props.openLegendByLayerName(layer);
  };

  _fetchLayerMetadata = () => {
    const layer = this.props.layers[this.state.playingIndex];
    this.props.fetchLayerMetadata(layer.metadataId);
  };

  _copernicusName = (props, playingIndex) => {
    let name;
    if (props.isCopernicus) {
      const attributes = props.layers[playingIndex].extensionData.attributes;
      logMain('_copernicusName', attributes);
      const { Activation_Id, Location, Map_Type } = attributes;
      return `[${Activation_Id}]: ${Location}, ${Map_Type}`;
      // return `[${Activation_Id}]: ${Map_Type}`;
    } else {
      name = props.name;
    }
    return name;
  };

  render() {
    const { lastItem, style, layers, hazard, code, layerType, isMapRequest } = this.props;
    const { playingIndex, playing } = this.state;
    const numLayers = layers.length;
    const playStatus = playing ? 'pause' : 'play_arrow'; // circle and circle filled available
    const layer = layers[playingIndex];
    const name = this._copernicusName(this.props, this.state.playingIndex);
    if (!layer) {
      return null;
    }
    // TODO verify information
    const { leadTime, timeSpan, timespan } = layer;

    const disabled = numLayers === 1;

    return (
      <TSContainer className="time-slider" lastItem={lastItem} style={style}>
        <TSContainerInner>
          <TSMapRequestIndicator isMapRequest={isMapRequest} />
          <TSHazardAndType>
            <span className="hazard" style={STYLES.layerType}>
              {hazard}
            </span>
            <span className="group" style={STYLES.layerType}>
              {layerType}
            </span>
            <span className="group" style={STYLES.layerType}>
              {code}
            </span>
          </TSHazardAndType>
          <TSControls>
            <TSLayerName>{name}</TSLayerName>
            <TSDatePlusContainer>
              <LocalizedDate date={leadTime} timespan={timespan || timeSpan} style={STYLES.date} />
              <IconButton
                style={STYLES.legendIcon}
                iconStyle={STYLES.legendIcon}
                onClick={this._fetchLayerMetadata}
                tooltip="Show metadata"
                tooltipPosition="top-left"
              >
                <FontIcon style={STYLES.legendIcon} className="material-icons">
                  receipt
                </FontIcon>
              </IconButton>
              <IconButton
                style={STYLES.legendIcon}
                iconStyle={STYLES.legendIcon}
                onClick={this._showLegend}
                tooltip="Show Legend"
                tooltipPosition="top-right"
              >
                <FontIcon style={STYLES.legendIcon} className="material-icons">
                  add
                </FontIcon>
              </IconButton>
            </TSDatePlusContainer>
            <TSLayerControls>
              <IconButton
                style={STYLES.icon}
                iconStyle={STYLES.icon}
                onClick={this._togglePlay}
                disabled={disabled}
                iconClassName="material-icons"
                tooltip="play"
                tooltipPosition="top-center"
              >
                {playStatus}
              </IconButton>
              <TSSliderContainer className="slider-container">
                {/* <TSThicks thickPositions={this._getThicksPositions()} /> */}
                <Slider
                  style={STYLES.slider}
                  disabled={disabled}
                  onDragStart={e => {
                    e.stopPropagation();
                  }}
                  sliderStyle={STYLES.sliderBar}
                  min={0}
                  max={numLayers === 1 ? 1 : numLayers - 1}
                  onChange={this._onSliderChange}
                  // TODO implement a transform/reverse value for handling date/time?
                  step={1}
                  value={playingIndex}
                />
              </TSSliderContainer>
              <IconButton
                style={STYLES.icon}
                onClick={this._previous}
                disabled={disabled}
                tooltip="Previous"
                tooltipPosition="top-left"
              >
                <FontIcon style={STYLES.icon} className="material-icons">
                  skip_previous
                </FontIcon>
              </IconButton>
              <IconButton
                style={STYLES.icon}
                onClick={this._next}
                disabled={disabled}
                tooltip="Next"
                tooltipPosition="top-right"
              >
                <FontIcon style={STYLES.icon} className="material-icons">
                  skip_next
                </FontIcon>
              </IconButton>
            </TSLayerControls>
          </TSControls>
          <TSActions>
            {this.props.removeWidget !== null && (
              <CloseIcon
                onClick={this._close}
                style={STYLES.icon}
                iconStyle={{ ...STYLES.icon, fontSize: 18 }}
              />
            )}
            {this.props.removeWidget === null && <span />}
            <TSQuantity className="num-layers">
              <span style={{ ...STYLES.num, borderBottom: '1px solid' }}>{playingIndex + 1}</span>
              <span style={STYLES.num}>{numLayers}</span>
            </TSQuantity>
          </TSActions>
        </TSContainerInner>
      </TSContainer>
    );
  }
}

export default TimeSlider;
