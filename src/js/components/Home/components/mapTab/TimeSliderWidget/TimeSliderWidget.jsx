import React, { Component } from 'react';
import Draggable from 'react-draggable';
import TimeSlider from './TimeSlider';
import { compose } from 'redux';
import {
  TSWidgetContainer,
  TSWidgetContainerInner,
  TSWidth,
  TSHeight,
  TSWMargin,
  TSPadding
} from './commons';
import { withMessages } from 'js/modules/ui';
import { withMapLayers } from 'ioc-api-interface';
import { withLegendsWidget } from 'js/modules/LegendsWidget';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';
import { withMapInfo } from 'js/modules/LegendsWidget';
import { withLeftDrawer } from 'js/modules/LeftDrawer';

import { logError, logMain } from 'js/utils/log';
import { API } from 'ioc-api-interface';
import { MetadataPanel } from './MetadataPanel';
import muiThemeable from 'material-ui/styles/muiThemeable';

const enhance = compose(
  withLegendsWidget,
  withMapLayers,
  withLayersAndSettings,
  withMessages,
  withMapInfo,
  withLeftDrawer
);

function leadTimeSort(lcfg1, lcfg2) {
  const l1 = new Date(lcfg1.leadTime);
  const l2 = new Date(lcfg2.leadTime);
  return l1 - l2;
}

const MAX_ROWS = 2;
const MAX_VISIBLE_COLUMNS = 3;

class TimeSliderWidget extends Component {
  static defaultProps = {
    mapLayers: {},
    activeIReactSettingNames: [],
    activeIReactTaskSettings: [],
    remoteSettings: {},
    maxRows: MAX_ROWS,
    maxVisibleColumns: MAX_VISIBLE_COLUMNS,
    map: null
  };

  _fetchLayerMetadata = async metadataId => {
    try {
      const api = API.getInstance();

      // Set loading
      this.props.onMapInfoLoading();
      const response = await api.maps.getLayerInfo(metadataId);
      const title = `Metadata for metadataId=${metadataId}`;
      const metadata = response.result;
      logMain(title, metadata);
      this.props.onMapInfoLoaded();
      this.props.pushModalMessage(
        title,
        <MetadataPanel
          metadataId={metadataId}
          metadata={metadata}
          palette={this.props.muiTheme.palette}
        />,
        {
          _close: null
        }
      );
      // Unset loading
      //metadataFileId
    } catch (e) {
      this.props.onMapInfoLoaded();
      // unset loading
      logError(`Cannot fetch metadata with id ${metadataId}`, e);
      this.props.pushError(`Cannot fetch metadata with id ${metadataId}`);
    }
  };

  componentDidMount() {
    if (Object.keys(this.props.remoteSettings).length === 0) {
      this.props.loadSettings();
    }
  }

  _getStyle = nItems => {
    const { maxRows, maxVisibleColumns } = this.props;
    const maxVisibleItems = maxRows * maxVisibleColumns;
    const hFactor = Math.min(maxRows, Math.ceil(nItems / maxVisibleColumns));
    const height = hFactor * (TSHeight + 2 * TSPadding) + TSWMargin;
    const wFactor = nItems > maxVisibleColumns ? maxVisibleColumns : nItems;
    const width = wFactor * (TSWidth + 2 * TSPadding) + 3 * TSWMargin;
    return {
      container: {
        top: `calc(100% - ${height + 16}px)`,
        width,
        maxWidth: width,
        minHeight: height,
        maxHeight: height,
        height
      },
      inner: {
        flexDirection: nItems > maxVisibleItems ? 'column' : 'row'
      }
    };
  };

  /**
   *
   *
   * @param {Array<number>} taskIds - array of related task id
   * @param {Object} layers - map <taskId>:<Array<Layer>>
   * @memberof TimeSliderWidget
   */
  _remapLayerInfo(taskIds, layers) {
    return layers.filter(lcfg => taskIds.indexOf(lcfg.taskId) > -1).sort(leadTimeSort);
  }

  _getActiveLayers = selectedFeature => {
    const layersFromSettings = this._getActiveLayersFromSettings();
    const layersFromMapRequest = this._getActiveLayersFromMapRequest(selectedFeature);
    const copernicusLayers = this._remapCopernicusLayers();
    return [...layersFromSettings, ...copernicusLayers, ...layersFromMapRequest];
  };

  _remapLayersFromSettings = (
    layersConfigurationArray,
    activeIReactSettingNames = [],
    activeIReactTaskSettings = [],
    deactivateIReactSettingByName = null,
    isMapRequest = false
  ) => {
    const {
      // activeIReactSettingNames,
      // activeIReactTaskSettings,
      remoteSettings
    } = this.props;
    const noLayersAvailable =
      typeof layersConfigurationArray === 'undefined' || layersConfigurationArray.length === 0; // TODO Add also map requests
    if (
      activeIReactSettingNames.length === 0 ||
      noLayersAvailable ||
      Object.keys(remoteSettings).length === 0
    ) {
      return [];
    } else {
      return activeIReactSettingNames
        .map((settingName, index) => {
          const activeSetting = activeIReactTaskSettings[index];
          const taskIds = activeSetting.setting.settingDefinition.customData.relatedIreactTasks;
          if (!Array.isArray(taskIds)) {
            return null; // weird things may happen
          } else {
            const layers = this._remapLayerInfo(taskIds, layersConfigurationArray);
            if (layers.length === 0) {
              return null;
            } else {
              const removeWidget =
                typeof deactivateIReactSettingByName === 'function'
                  ? deactivateIReactSettingByName.bind(settingName)
                  : null;
              return {
                id: settingName,
                hazard: activeSetting.grandParentGroup
                  ? activeSetting.grandParentGroup.displayName
                  : 'Unknown Hazard', // See for Copernicus
                layerType: activeSetting.parentGroup.displayName,
                name: activeSetting.setting.settingDefinition.displayName,
                isMapRequest,
                isCopernicus: false,
                layers,
                removeWidget
              };
            }
          }
        })
        .filter(l => l !== null);
    }
  };

  _getActiveLayersFromMapRequest = selectedFeature => {
    return selectedFeature &&
      this.props.activeLayersFromMapRequest &&
      this.props.activeLayersFromMapRequest.layers &&
      this.props.activeLayersFromMapRequest.layers.length > 0
      ? [
          {
            ...selectedFeature.properties,
            isCopernicus: false,
            isMapRequest: true,
            id: selectedFeature.properties.code,
            removeWidget:
              typeof this.props.deactivateMapRequestLayer === 'function'
                ? this.props.deactivateMapRequestLayer.bind(selectedFeature.properties.code)
                : null,
            layers: this.props.activeLayersFromMapRequest.layers
          }
        ]
      : [];
  };

  _getActiveLayersFromSettings = () => {
    return this._remapLayersFromSettings(
      this.props.mapLayers.layers,
      this.props.activeIReactSettingNames,
      this.props.activeIReactTaskSettings,
      this.props.deactivateIReactSettingByName,
      false
    );
  };

  _remapCopernicusLayers = () => {
    const { activeCopernicusLayers, activeCopernicusNames, deactivateCopernicusLayer } = this.props;
    return activeCopernicusNames.map((copernicusName, index) => {
      const activeCopernicus = activeCopernicusLayers[index];
      const layers = activeCopernicus.layers
        .sort((a, b) => a.metadataId - b.metadataId)
        .sort(leadTimeSort);
      const removeWidget = deactivateCopernicusLayer.bind(copernicusName);
      return {
        id: copernicusName,
        hazard: activeCopernicus.hazard,
        layerType: activeCopernicus.layerType,
        code: activeCopernicus.code,
        isMapRequest: false,
        isCopernicus: true,
        layers,
        removeWidget
      };
    });
  };
  _showLegendForLayerConfig = cfg => {
    this.props.setOpenLegendsWidget(true);
    this.props.addLegend(cfg);
  };

  _removeLayerInfoPopupIfAny = () => {
    if (this.props.mapInfo.length) {
      this.props.resetMapInfo();
    }
  };

  render() {
    const {
      maxRows,
      maxVisibleColumns,
      map,
      mapLayers,
      selectedFeature,
      isPrimaryOpen
    } = this.props;

    const activeIreactLayers = this._getActiveLayers(selectedFeature);
    const nItems = activeIreactLayers.length;
    const style = this._getStyle(nItems);
    const maxVisibleItems = maxRows * maxVisibleColumns;

    if (nItems > 0) {
      return (
        // [
        <Draggable
          bounds=".map.tab"
          handle=".time-slider-container"
          position={null}
          grid={[25, 25]}
        >
          <TSWidgetContainer
            className="time-slider-container"
            style={style.container}
            isPrimaryOpen={isPrimaryOpen}
          >
            <TSWidgetContainerInner className="time-slider-container-inner" style={style.inner}>
              {activeIreactLayers.map((w, i) => (
                <TimeSlider
                  key={i}
                  defaultGeoServerURL={GEOSERVER_URL ? GEOSERVER_URL : mapLayers.baseUrl}
                  lastItem={nItems > maxVisibleItems && i === nItems - 1}
                  map={map}
                  openLegendByLayerName={this._showLegendForLayerConfig}
                  fetchLayerMetadata={this._fetchLayerMetadata}
                  {...w}
                  onLayerUpdated={this._removeLayerInfoPopupIfAny}
                  deselectFeature={this.props.deselectFeature}
                />
              ))}
            </TSWidgetContainerInner>
          </TSWidgetContainer>
        </Draggable>
      );
    } else {
      return (
        <TSWidgetContainer
          key={0}
          className="time-slider-container"
          style={{ width: 0, height: 0 }}
        />
      );
    }
  }
}

export default enhance(muiThemeable()(TimeSliderWidget));
