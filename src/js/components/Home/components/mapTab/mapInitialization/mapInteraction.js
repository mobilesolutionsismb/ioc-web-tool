/**
 *
 * @param {mapboxgl::Map} map
 * @param {HadlersMap} handlersMap {layerName:string : { handlerName:string : handler: Function}}
 */
export function attachHandlersToMap(map, handlersMap) {
  const handlersEntries = Object.entries(handlersMap);
  for (let entry of handlersEntries) {
    const layerName = entry[0];
    const handlers = entry[1];
    const handlerNames = Object.keys(handlers);
    for (let handlerName of handlerNames) {
      map.on(handlerName, layerName, handlers[handlerName]);
    }
  }
}
