import { point } from '@turf/helpers';
import {
  metersToPixelsAtMaxZoom,
  variableCircleInMeters,
  DEFAULT_POSITION_CIRCLE_PAINT
} from 'js/modules/map';
import { isOldGeolocation } from 'js/utils/localizeDate';

const POSITION_COLOR = DEFAULT_POSITION_CIRCLE_PAINT['circle-color'];
const POSITION_OLD_COLOR = '#555555';

export function initUserPosition(map, latitude, longitude, accuracy) {
  map.addSource('userposition', {
    type: 'geojson',
    data: point([longitude, latitude])
  });

  // User Position layers
  map.addLayer({
    id: 'position-marker-accuracy',
    type: 'circle',
    source: 'userposition',
    paint: { ...variableCircleInMeters(accuracy, latitude), 'circle-opacity': 0.25 }
  });

  map.addLayer({
    id: 'position-marker',
    type: 'circle',
    source: 'userposition',
    paint: DEFAULT_POSITION_CIRCLE_PAINT
  });

  return Promise.resolve();
}

export function updateUserPosition(map, currentGeolocation, nextGeolocation) {
  if (!map) {
    return;
  }
  const { coords } = currentGeolocation;
  const { longitude, latitude, accuracy } = coords;
  const nextCoords = nextGeolocation.coords;
  const isOld = isOldGeolocation(currentGeolocation, nextGeolocation);
  const isDifferentPosition =
    latitude !== nextCoords.latitude || longitude !== nextCoords.longitude;
  const isDifferentAccuracy = accuracy !== nextCoords.accuracy || latitude !== nextCoords.latitude;
  const repaintLayersEnabled =
    typeof map.getLayer('position-marker') !== 'undefined' &&
    typeof map.getLayer('position-marker-accuracy') !== 'undefined';

  if (repaintLayersEnabled) {
    if (isOld) {
      map.setPaintProperty('position-marker', 'circle-color', POSITION_OLD_COLOR);
      map.setPaintProperty('position-marker-accuracy', 'circle-color', POSITION_OLD_COLOR);
    } else {
      map.setPaintProperty('position-marker', 'circle-color', POSITION_COLOR);
      map.setPaintProperty('position-marker-accuracy', 'circle-color', POSITION_COLOR);
    }
  }

  if (isDifferentPosition) {
    const upSrc = map.getSource('userposition');
    if (upSrc) {
      upSrc.setData(point([nextCoords.longitude, nextCoords.latitude]));
    }
  }
  if (isDifferentAccuracy) {
    if (repaintLayersEnabled) {
      map.setPaintProperty('position-marker-accuracy', 'circle-radius', {
        stops: [[0, 0], [20, metersToPixelsAtMaxZoom(nextCoords.accuracy, nextCoords.latitude)]],
        base: 2
      });
    }
    //TODO set accuracy red if too big?
  }
}
