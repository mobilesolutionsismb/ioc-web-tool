import { getMapSettings /* , attachCursorEvents */ } from 'js/components/MapView';

import { initUserPosition, updateUserPosition } from './userPosition';
import {
  initIREACTFeatures,
  updateIreactDataSource,
  updateIreactDataSourceNoCluster,
  refreshIreactClusters
} from './ireactFeatures';
import {
  initEmergencyEventFeatures,
  updateEmergencyEventsDataSource,
  getFiltersForEventId
} from './emergencyEvents';

import * as staticFilters from './staticFiltersDefinitions';

export async function onMapLoad(event, doneCallback, hoverCallback, mouseLeaveCallback, rest) {
  const map = event.target;

  const coords = rest.geolocation.coords;
  const { longitude, latitude, accuracy } = coords;
  await initUserPosition(map, longitude, latitude, accuracy, rest.positionLayersHandlers);
  await initIREACTFeatures(
    map,
    rest.ireactClusteredFeatures,
    rest.ireactUnclusteredFeatures,
    rest.ireactLayersHandlers
  );
  await initEmergencyEventFeatures(map, rest.emergencyEvents, rest.emergencyEventsLayersHandlers);
  if (typeof doneCallback === 'function') {
    doneCallback(map, getMapSettings(map));
  }
}

export {
  updateUserPosition,
  updateIreactDataSource,
  updateIreactDataSourceNoCluster,
  refreshIreactClusters,
  updateEmergencyEventsDataSource,
  getFiltersForEventId,
  staticFilters
};
