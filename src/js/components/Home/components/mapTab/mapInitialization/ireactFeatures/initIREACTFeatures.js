import { getBorderLayers, getOffsetsForBorderDots } from './clusterUtils';
import { getMapSettings } from 'js/components/MapView';
import { CLUSTER_COLORS, IREACT_DATA_SOURCE_NAME, DEFAULT_ICON_SIZE } from './featureConstants';
import { IS_CLUSTER } from '../staticFiltersDefinitions';
import {
  unclusteredReports,
  unclusteredReportsIcons,
  unclusteredReportsBadge,
  unclusteredEmergencyCommunications,
  unclusteredEmergencyCommunicationsIcon,
  unclusteredEmergencyCommunicationsIconBadge,
  unclusteredMissions,
  unclusteredMissionIcons,
  unclusteredMissionTasks,
  unclusteredMissionTaskIcons,
  unclusteredAgentLocations,
  unclusteredAgentLocationIcons
} from './unclusteredFeatures';

import {
  selectedReport,
  selectedEmergencyCommunication,
  selectedEmergencyCommunicationAOI,
  selectedEmergencyCommunicationAOIOutline,
  selectedMission,
  selectedMissionAOI,
  selectedMissionAOIOutline,
  selectedMissionTask,
  selectedAgentLocation
} from './selectedFeatures';
import { featureCollection } from '@turf/helpers';

const CLUSTER_BORDER_RADIUS_PX = 3; // cluster border/2, pixel (each element is a circle)
const CLUSTER_RADIUS_PX = 20; // cluster radius, pixel
const CLUSTER_LAYER_BORDER_OFFSETS = getOffsetsForBorderDots(
  CLUSTER_RADIUS_PX,
  CLUSTER_BORDER_RADIUS_PX
);
const CLUSTER_MAX_ZOOM = 19;

// Inner circles of the cluster
const circleCluster = {
  id: 'clusters',
  type: 'circle',
  maxzoom: CLUSTER_MAX_ZOOM,
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  filter: IS_CLUSTER,
  paint: {
    'circle-color': CLUSTER_COLORS.circle,
    'circle-radius': CLUSTER_RADIUS_PX
  }
};

// Layer configurations for cluster borders (made by small circle)
const borderLayers = getBorderLayers(
  `${IREACT_DATA_SOURCE_NAME}-clustered`,
  CLUSTER_LAYER_BORDER_OFFSETS,
  CLUSTER_BORDER_RADIUS_PX,
  CLUSTER_COLORS.categories,
  0,
  CLUSTER_MAX_ZOOM
);

// Inner counter of features per cluster
const clusterCountLayer = {
  id: 'cluster-count',
  type: 'symbol',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  maxzoom: CLUSTER_MAX_ZOOM,
  filter: IS_CLUSTER,
  layout: {
    'text-field': '{point_count_abbreviated}',
    'text-font': ['Open Sans Bold'],
    'text-size': DEFAULT_ICON_SIZE,
    'text-allow-overlap': true
  },
  paint: {
    'text-color': CLUSTER_COLORS.count
  }
};

/**
 * Adds the data source and the layers related to I-REACT geojson features
 * @param {mapboxgl::Map} map
 * @param {GeoJSONFeatureCollection} data
 * @param {object} handlersMap - map<sting, object> layerName:handlers - handlers= event:function
 */
export function initIREACTFeatures(map, dataToBeClustered, dataUnclustered) {
  // Add source for all I-REACT geojson data
  map.addSource(`${IREACT_DATA_SOURCE_NAME}-clustered`, {
    type: 'geojson',
    data: featureCollection(dataToBeClustered)
  });

  map.addSource(`${IREACT_DATA_SOURCE_NAME}-noclustering`, {
    type: 'geojson',
    data: featureCollection(dataUnclustered)
  });

  // Add cluster circles
  map.addLayer(circleCluster);

  // circles of each cluster border
  for (let bl of borderLayers) {
    map.addLayer(bl);
  }

  // Cluster count
  map.addLayer(clusterCountLayer);

  // Unclustered layers
  map.addLayer(unclusteredReports);
  map.addLayer(unclusteredEmergencyCommunications);
  map.addLayer(unclusteredMissions);
  map.addLayer(unclusteredAgentLocations);
  //map.addLayer(unclusteredMapRequests);

  // Selected feature layers
  map.addLayer(selectedReport);

  map.addLayer(selectedEmergencyCommunicationAOI);
  map.addLayer(selectedEmergencyCommunicationAOIOutline);
  map.addLayer(selectedEmergencyCommunication);

  map.addLayer(selectedMissionAOI);
  map.addLayer(selectedMissionAOIOutline);
  map.addLayer(selectedMission);
  map.addLayer(unclusteredMissionTasks);
  map.addLayer(selectedMissionTask);
  map.addLayer(selectedAgentLocation);

  // Text Icons
  map.addLayer(unclusteredReportsIcons);
  map.addLayer(unclusteredReportsBadge);
  map.addLayer(unclusteredEmergencyCommunicationsIcon);
  map.addLayer(unclusteredEmergencyCommunicationsIconBadge);
  map.addLayer(unclusteredMissionIcons);
  map.addLayer(unclusteredMissionTaskIcons);
  map.addLayer(unclusteredAgentLocationIcons);
  //map.addLayer(unclusteredMapRequestIcons);

  return Promise.resolve();
}

// Update data source features of CLUSTERED features - i.e. data coming from worker
// export function updateIreactDataSource(map, clusteredFeatureCollection) {
//     const iReactDataSource = map.getSource(`${IREACT_DATA_SOURCE_NAME}-clustered`);
//     if (iReactDataSource) {
//         iReactDataSource.setData(clusteredFeatureCollection);
//     }
//     return Promise.resolve();
// }
export function updateIreactDataSource(map, clusteredFeaturesURL) {
  const iReactDataSource = map.getSource(`${IREACT_DATA_SOURCE_NAME}-clustered`);
  if (iReactDataSource) {
    iReactDataSource.setData(clusteredFeaturesURL);
  }
  return Promise.resolve();
}

export function updateIreactDataSourceNoCluster(map, featureCollection) {
  const iReactDataSource = map.getSource(`${IREACT_DATA_SOURCE_NAME}-noclustering`);
  if (iReactDataSource) {
    iReactDataSource.setData(featureCollection);
  }
  return Promise.resolve();
}

// Refresh items on map (by calling the worker)
export function refreshIreactClusters(map, reportClusterWorkerReady, reportClusterWorker) {
  const mapSettings = getMapSettings(map);
  if (mapSettings != null && reportClusterWorkerReady === true) {
    const bounds = mapboxgl.LngLatBounds.convert(mapSettings.bounds);
    const bbox = [bounds.getWest(), bounds.getSouth(), bounds.getEast(), bounds.getNorth()];
    const zoom = mapSettings.zoom;
    reportClusterWorker.postMessage({
      action: 'clusterize',
      bbox,
      pointsPerBorder: CLUSTER_LAYER_BORDER_OFFSETS.length,
      zoom: Math.round(zoom)
    });
  }
  return Promise.resolve();
}
