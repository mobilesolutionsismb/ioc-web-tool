import { white, brown400 } from 'material-ui/styles/colors';

// Colors for single features
export {
  CLUSTER_COLORS,
  REPORT_COLORS as REPORTS_SF_COLORS,
  EMCOMM_COLORS as EMCOMMS_SF_COLORS,
  MISSION_COLOR as MISSIONS_SF_COLOR,
  AGENTLOCATION_COLOR as AGENTLOCATION_SF_COLOR,
  MAPREQUEST_COLOR as MAPREQUEST_SF_COLOR,
  // Exception since it is reversed
  // MISSION_TASK_COLOR as MISSION_TASK_ICON_COLOR
  MISSION_TASK_COLOR as MISSION_TASK_SF_COLOR
} from 'js/startup/iReactTheme';

export const MISSION_TASK_ICON_COLOR = white;

// Text/Icon/Border colors for single features (except mission task)
export const REPORT_ICON_COLORS = {
  validated: white,
  submitted: white,
  inaccurate: white,
  inappropriate: white
};

export const EMCOMM_ICON_COLORS = {
  bePrepared: white,
  actionRequired: white,
  dangerToLife: white,
  alert: white,
  reportRequest: white
};

export const MISSION_ICON_COLOR = white;

export const AGENTLOCATION_ICON_COLOR = white;

export const MAPREQUEST_ICON_COLOR = white;

export const UNKNOW_FEATURE_TYPE_COLOR = brown400;

// Consts

export const IREACT_DATA_SOURCE_NAME = 'ireact-data';

export const DEFAULT_PINPOINT_SIZE = {
  base: 1.5,
  stops: [[0, 13], [12, 26], [20, 52], [24, 56]]
};

export const HAZARD_ICON_SIZE = {
  base: 1.5,
  stops: [[0, 20], [12, 26], [20, 36], [24, 40]]
};

export const DEFAULT_ICON_SIZE = {
  base: 1.5,
  stops: [[0, 20], [12, 26], [20, 36], [24, 40]]
};
