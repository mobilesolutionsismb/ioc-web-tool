import {
  IS_DESELECTED_REPORT,
  IS_DESELECTED_EMERGENCY_COMMUNICATION,
  IS_DESELECTED_EMERGENCY_COMMUNICATION_AOI,
  IS_DESELECTED_MISSION,
  IS_DESELECTED_MISSION_AOI,
  IS_DESELECTED_MISSION_TASK,
  IS_DESELECTED_AGENTLOCATION
} from '../staticFiltersDefinitions';
import {
  IREACT_DATA_SOURCE_NAME,
  REPORTS_SF_COLORS,
  EMCOMMS_SF_COLORS,
  MISSIONS_SF_COLOR,
  MISSION_TASK_SF_COLOR,
  AGENTLOCATION_SF_COLOR
  // MAP_REQUEST_SF_COLOR
} from './featureConstants';
import { darken } from 'material-ui/utils/colorManipulator';

/**
 * Layers for item selection
 * On selected item, change filters by using map.setFilter
 * On deselected item, restore filters by using map.setFilter
 */

/* Reports */
const REPORTS_SF_COLORS_STOPS = Object.entries(REPORTS_SF_COLORS.status);
// Colored halo circle around selected report
export const selectedReport = {
  id: 'selected-report',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'circle',
  paint: {
    'circle-stroke-width': 2,
    'circle-blur': 0.15,
    'circle-opacity': 0.25,
    'circle-pitch-scale': 'map',
    'circle-stroke-color': {
      type: 'categorical',
      property: 'status',
      stops: REPORTS_SF_COLORS_STOPS
    },
    'circle-color': {
      type: 'categorical',
      property: 'status',
      stops: REPORTS_SF_COLORS_STOPS
    },
    'circle-radius': {
      base: 1.5,
      stops: [[0, 16], [12, 26], [20, 104]]
    }
  },
  filter: IS_DESELECTED_REPORT
};

/* Emergency Communications */
// Show an enlarged pinpoint icon
export const selectedEmergencyCommunication = {
  id: 'selected-emcomm',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints_v4'],
    'text-field': '\ue90a',
    'text-size': 36,
    // 'text-rotate': 180,
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 2,
    'text-halo-color': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', darken(EMCOMMS_SF_COLORS.level.reportRequest, 0.15)],
        ['alert', darken(EMCOMMS_SF_COLORS.level.alert, 0.15)],
        ['bePrepared', darken(EMCOMMS_SF_COLORS.level.bePrepared, 0.15)],
        ['actionRequired', darken(EMCOMMS_SF_COLORS.level.actionRequired, 0.15)],
        ['dangerToLife', darken(EMCOMMS_SF_COLORS.level.dangerToLife, 0.15)]
      ],
      default: darken(EMCOMMS_SF_COLORS.level.reportRequest, 0.15)
    },
    'text-color': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', EMCOMMS_SF_COLORS.level.reportRequest],
        ['alert', EMCOMMS_SF_COLORS.level.alert],
        ['bePrepared', EMCOMMS_SF_COLORS.level.bePrepared],
        ['actionRequired', EMCOMMS_SF_COLORS.level.actionRequired],
        ['dangerToLife', EMCOMMS_SF_COLORS.level.dangerToLife]
      ]
    }
  },
  filter: IS_DESELECTED_EMERGENCY_COMMUNICATION
};

export const selectedEmergencyCommunicationAOI = {
  id: 'selected-emcomm-aoi',
  source: `${IREACT_DATA_SOURCE_NAME}-noclustering`,
  type: 'fill',
  paint: {
    'fill-color': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', EMCOMMS_SF_COLORS.level.reportRequest],
        ['alert', EMCOMMS_SF_COLORS.level.alert],
        ['bePrepared', EMCOMMS_SF_COLORS.level.bePrepared],
        ['actionRequired', EMCOMMS_SF_COLORS.level.actionRequired],
        ['dangerToLife', EMCOMMS_SF_COLORS.level.dangerToLife]
      ]
    },
    'fill-opacity': 0.2,
    'fill-outline-color': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', EMCOMMS_SF_COLORS.level.reportRequest],
        ['alert', EMCOMMS_SF_COLORS.level.alert],
        ['bePrepared', EMCOMMS_SF_COLORS.level.bePrepared],
        ['actionRequired', EMCOMMS_SF_COLORS.level.actionRequired],
        ['dangerToLife', EMCOMMS_SF_COLORS.level.dangerToLife]
      ]
    }
  },
  filter: IS_DESELECTED_EMERGENCY_COMMUNICATION_AOI
};

export const selectedEmergencyCommunicationAOIOutline = {
  id: 'selected-emcomm-aoi-outline',
  source: `${IREACT_DATA_SOURCE_NAME}-noclustering`,
  type: 'line',
  layout: {
    'line-cap': 'round',
    'line-join': 'round'
  },
  paint: {
    'line-color': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', EMCOMMS_SF_COLORS.level.reportRequest],
        ['alert', EMCOMMS_SF_COLORS.level.alert],
        ['bePrepared', EMCOMMS_SF_COLORS.level.bePrepared],
        ['actionRequired', EMCOMMS_SF_COLORS.level.actionRequired],
        ['dangerToLife', EMCOMMS_SF_COLORS.level.dangerToLife]
      ]
    },
    'line-dasharray': [0.2, 2],
    'line-width': 2
  },
  filter: IS_DESELECTED_EMERGENCY_COMMUNICATION_AOI
};

/* Missions and Tasks */

// Colored halo circle around selected mission
export const selectedMission = {
  id: 'selected-mission',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints_v4'],
    'text-field': '\ue90a',
    'text-size': 36,
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 2,
    'text-halo-color': darken(MISSIONS_SF_COLOR, 0.15),
    'text-color': MISSIONS_SF_COLOR
  },
  filter: IS_DESELECTED_MISSION
};

export const selectedMissionAOI = {
  id: 'selected-mission-aoi',
  source: `${IREACT_DATA_SOURCE_NAME}-noclustering`,
  type: 'fill',
  paint: {
    'fill-color': MISSIONS_SF_COLOR,
    'fill-opacity': 0.2,
    'fill-outline-color': MISSIONS_SF_COLOR
  },
  filter: IS_DESELECTED_MISSION_AOI
};

export const selectedMissionAOIOutline = {
  id: 'selected-mission-aoi-outline',
  source: `${IREACT_DATA_SOURCE_NAME}-noclustering`,
  type: 'line',
  layout: {
    'line-cap': 'round',
    'line-join': 'round'
  },
  paint: {
    'line-color': MISSIONS_SF_COLOR,
    'line-dasharray': [0.2, 2],
    'line-width': 2
  },
  filter: IS_DESELECTED_MISSION_AOI
};

// Colored halo circle around selected task
export const selectedMissionTask = {
  id: 'selected-missiontask',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints_v4'],
    'text-field': '\ue90a',
    'text-size': 36,
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 2,
    'text-halo-color': darken(MISSIONS_SF_COLOR, 0.15),
    'text-color': MISSION_TASK_SF_COLOR
  },
  filter: IS_DESELECTED_MISSION_TASK
};

// TODO?? add AOI layer for selected mission if selectedItemId == missionId or selectedItemId == taskId with missionId

export const selectedAgentLocation = {
  id: 'selected-agentlocation',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'circle',
  paint: {
    'circle-stroke-width': 2,
    'circle-blur': 0.15,
    'circle-opacity': 0.25,
    'circle-pitch-scale': 'map',
    'circle-stroke-color': AGENTLOCATION_SF_COLOR,
    'circle-color': AGENTLOCATION_SF_COLOR,
    'circle-radius': {
      base: 1.5,
      stops: [[0, 16], [12, 26], [20, 104]]
    }
  },
  filter: IS_DESELECTED_AGENTLOCATION
};
