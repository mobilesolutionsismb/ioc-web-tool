import {
  IS_UNCLUSTERED_REPORT,
  IS_UNCLUSTERED_EMERGENCY_COMMUNICATION,
  IS_UNCLUSTERED_MISSION,
  IS_UNCLUSTERED_MISSION_TASK,
  IS_UNCLUSTERED_AGENTLOCATION
} from '../staticFiltersDefinitions';
import { darken } from 'material-ui/utils/colorManipulator';

import {
  IREACT_DATA_SOURCE_NAME,
  REPORTS_SF_COLORS,
  REPORT_ICON_COLORS,
  EMCOMMS_SF_COLORS /* EMCOMM_ICON_COLORS, */,
  MISSIONS_SF_COLOR,
  MISSION_ICON_COLOR,
  MISSION_TASK_SF_COLOR,
  MISSION_TASK_ICON_COLOR,
  AGENTLOCATION_SF_COLOR,
  AGENTLOCATION_ICON_COLOR,
  // MAP_REQUEST_SF_COLOR, MAP_REQUEST_ICON_COLOR,
  DEFAULT_PINPOINT_SIZE,
  HAZARD_ICON_SIZE,
  DEFAULT_ICON_SIZE
} from './featureConstants';

/**
 * This file contains style definitions for unclustered features
 */

// Unclustered reports PINPOINTS
export const unclusteredReports = {
  id: 'unclustered-reports',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': '\ue91c',
    'text-size': DEFAULT_PINPOINT_SIZE,
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 2,
    'text-halo-color': darken(REPORTS_SF_COLORS.status.submitted, 0.15),
    'text-color': REPORTS_SF_COLORS.status.submitted
    // 'text-color': {
    //     type: 'categorical',
    //     property: 'status',
    //     stops: Object.entries(REPORTS_SF_COLORS.status)
    // }
  },
  filter: IS_UNCLUSTERED_REPORT
};

// Unclustered reports text/hazard icon
export const unclusteredReportsIcons = {
  id: 'unclustered-reports-icons',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': {
      type: 'categorical',
      property: 'hazard',
      stops: [
        ['none', ' '],
        ['fire', '\ue916'],
        ['flood', '\ue917'],
        ['extremeWeather', '\ue915'],
        ['landslide', '\ue913'],
        ['avalanches', '\ue913'], // WRONG THIS IS same as LANDSLIDE
        ['earthquake', '\ue914']
      ],
      default: ''
    },
    'text-transform': 'uppercase',
    'text-size': HAZARD_ICON_SIZE,
    'text-offset': [0, -0.1],
    'text-allow-overlap': true
  },
  paint: {
    'text-color': {
      type: 'categorical',
      property: 'status',
      stops: Object.entries(REPORT_ICON_COLORS)
    },
    'text-halo-width': 1,
    'text-halo-color': REPORTS_SF_COLORS.status.submitted
  },
  filter: IS_UNCLUSTERED_REPORT
};

export const unclusteredReportsBadge = {
  id: 'unclustered-reports-badge',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'icon-image': {
      type: 'categorical',
      property: 'status',
      stops: [
        ['validated', 'pinshape_validated_small'],
        ['inaccurate', 'pinshape_rejected'],
        ['inappropriate', 'pinshape_rejected'],
        ['submitted', '']
      ],
      default: ''
    },
    'icon-size': 0.75,
    'icon-offset': {
      base: 1.5,
      stops: [[0, [15, -18]], [12, [15, -23]], [20, [32, -32]], [24, [36, -36]]]
    }
  },
  filter: IS_UNCLUSTERED_REPORT
};

// Unclustered emcomms
export const unclusteredEmergencyCommunications = {
  id: 'unclustered-emcomms',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': '\ue91c',
    'text-size': DEFAULT_PINPOINT_SIZE,
    // 'text-rotate': 180,
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 2,
    'text-halo-color': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', darken(EMCOMMS_SF_COLORS.level.reportRequest, 0.15)],
        ['alert', darken(EMCOMMS_SF_COLORS.level.alert, 0.15)],
        ['bePrepared', darken(EMCOMMS_SF_COLORS.level.bePrepared, 0.15)],
        ['actionRequired', darken(EMCOMMS_SF_COLORS.level.actionRequired, 0.15)],
        ['dangerToLife', darken(EMCOMMS_SF_COLORS.level.dangerToLife, 0.15)]
      ],
      default: darken(EMCOMMS_SF_COLORS.level.reportRequest, 0.15)
    },
    'text-color': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', EMCOMMS_SF_COLORS.level.reportRequest],
        ['alert', EMCOMMS_SF_COLORS.level.alert],
        ['bePrepared', EMCOMMS_SF_COLORS.level.bePrepared],
        ['actionRequired', EMCOMMS_SF_COLORS.level.actionRequired],
        ['dangerToLife', EMCOMMS_SF_COLORS.level.dangerToLife]
      ],
      default: EMCOMMS_SF_COLORS.level.reportRequest
    }
  },
  filter: IS_UNCLUSTERED_EMERGENCY_COMMUNICATION
};

// Unclustered emcomms icon
export const unclusteredEmergencyCommunicationsIcon = {
  id: 'unclustered-emcomm-icons',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': {
      type: 'categorical',
      property: 'hazard',
      stops: [
        ['none', ' '],
        ['fire', '\ue916'],
        ['flood', '\ue917'],
        ['extremeWeather', '\ue915'],
        ['landslide', '\ue913'],
        ['avalanches', '\ue913'], // WRONG THIS IS same as LANDSLIDE
        ['earthquake', '\ue914']
      ],
      default: '\ue91b'
      // property: 'level',
      // stops: [
      //     ['reportRequest', '\ue91b'],
      //     // ['alert', '\ue91b'],
      //     // ['bePrepared', '\ue91b'],
      //     // ['actionRequired', '\ue91b'],
      //     // ['dangerToLife', '\ue91b']
      // ]
    },
    'text-transform': 'uppercase',
    'text-size': DEFAULT_ICON_SIZE,
    'text-offset': [0, -0.1],
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 1,
    'text-halo-color': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', EMCOMMS_SF_COLORS.level.reportRequest],
        ['alert', EMCOMMS_SF_COLORS.level.alert],
        ['bePrepared', EMCOMMS_SF_COLORS.level.bePrepared],
        ['actionRequired', EMCOMMS_SF_COLORS.level.actionRequired],
        ['dangerToLife', EMCOMMS_SF_COLORS.level.dangerToLife]
      ],
      default: EMCOMMS_SF_COLORS.level.reportRequest
    },
    // 'text-halo-color': '#999999',
    'text-color': '#FFFFFF'
    // 'text-color': {
    //     type: 'categorical',
    //     property: 'level',
    //     stops: [
    //         ['reportRequest', EMCOMM_ICON_COLORS.reportRequest],
    //         ['alert', EMCOMM_ICON_COLORS.alert],
    //         ['bePrepared', EMCOMM_ICON_COLORS.bePrepared],
    //         ['actionRequired', EMCOMM_ICON_COLORS.actionRequired],
    //         ['dangerToLife', EMCOMM_ICON_COLORS.dangerToLife]
    //     ]
    // }
  },
  filter: IS_UNCLUSTERED_EMERGENCY_COMMUNICATION
};

export const unclusteredEmergencyCommunicationsIconBadge = {
  id: 'unclustered-emcomm-badge',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'icon-image': {
      type: 'categorical',
      property: 'level',
      stops: [
        ['reportRequest', ''],
        ['alert', 'pinshape_warning_small'],
        ['bePrepared', ''],
        ['actionRequired', ''],
        ['dangerToLife', '']
      ],
      default: ''
    },
    'icon-size': 0.75,
    'icon-offset': {
      base: 1.5,
      stops: [[0, [15, -18]], [12, [15, -23]], [20, [32, -32]], [24, [36, -36]]]
    }
  },
  filter: IS_UNCLUSTERED_EMERGENCY_COMMUNICATION
};

// Unclustered mission
export const unclusteredMissions = {
  id: 'unclustered-missions',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': '\ue91c',
    'text-size': DEFAULT_PINPOINT_SIZE,
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 2,
    'text-halo-color': darken(MISSIONS_SF_COLOR, 0.15),
    'text-color': MISSIONS_SF_COLOR
  },
  filter: IS_UNCLUSTERED_MISSION
};

// Unclustered mission text/icon
export const unclusteredMissionIcons = {
  id: 'unclustered-missions-icons',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': '\ue919',
    'text-transform': 'uppercase',
    'text-size': DEFAULT_ICON_SIZE,
    'text-offset': [0, -0.1],
    'text-allow-overlap': true
  },
  paint: {
    'text-color': MISSION_ICON_COLOR,
    'text-halo-width': 1,
    'text-halo-color': MISSIONS_SF_COLOR
  },
  filter: IS_UNCLUSTERED_MISSION
};

// Unclustered mission task
export const unclusteredMissionTasks = {
  id: 'unclustered-missiontasks',
  source: `${IREACT_DATA_SOURCE_NAME}-noclustering`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': '\ue91c',
    'text-size': DEFAULT_PINPOINT_SIZE,
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 2,
    'text-halo-color': darken(MISSIONS_SF_COLOR, 0.15),
    'text-color': MISSION_TASK_SF_COLOR
  },
  filter: IS_UNCLUSTERED_MISSION_TASK
};

// Unclustered mission task text/icon
export const unclusteredMissionTaskIcons = {
  id: 'unclustered-missiontasks-icons',
  source: `${IREACT_DATA_SOURCE_NAME}-noclustering`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': '\ue912',
    'text-transform': 'uppercase',
    'text-size': DEFAULT_ICON_SIZE,
    'text-offset': [0, -0.1],
    'text-allow-overlap': true
  },
  paint: {
    'text-color': MISSION_TASK_ICON_COLOR,
    'text-halo-width': 1,
    'text-halo-color': MISSION_TASK_SF_COLOR
  },
  filter: IS_UNCLUSTERED_MISSION_TASK
};

// Agents locations
export const unclusteredAgentLocations = {
  id: 'unclustered-agentlocations',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': '\ue91c',
    'text-size': DEFAULT_PINPOINT_SIZE,
    'text-allow-overlap': true
  },
  paint: {
    'text-halo-width': 2,
    'text-halo-color': darken(AGENTLOCATION_SF_COLOR, 0.15),
    'text-color': AGENTLOCATION_SF_COLOR
  },
  filter: IS_UNCLUSTERED_AGENTLOCATION
};

// Unclustered reports text/hazard icon
export const unclusteredAgentLocationIcons = {
  id: 'unclustered-agentlocations-icons',
  source: `${IREACT_DATA_SOURCE_NAME}-clustered`,
  type: 'symbol',
  layout: {
    'text-font': ['I-REACT_Pinpoints'],
    'text-field': '\ue91a',
    'text-transform': 'uppercase',
    'text-size': DEFAULT_ICON_SIZE,
    'text-offset': [0, -0.1],
    'text-allow-overlap': true
  },
  paint: {
    'text-color': AGENTLOCATION_ICON_COLOR,
    'text-halo-width': 1,
    'text-halo-color': AGENTLOCATION_SF_COLOR
  },
  filter: IS_UNCLUSTERED_AGENTLOCATION
};
