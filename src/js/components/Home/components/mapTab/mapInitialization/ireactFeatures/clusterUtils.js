const { PI, cos, sin } = Math;

/**
 * Get number of dots of borderDotRadius radius size (pixel) which
 * will fill the circumference of a circle of clusterRadius radius size (pixel)
 * and returns the number of points and their offset displacements
 * @param {Number} clusterRadiusPx
 * @param {Number} borderDotRadiusPx
 * @return {Array} array of offsets of the centers of the dots
 */
export function getOffsetsForBorderDots(clusterRadiusPx = 16, borderDotRadiusPx = 2) {
  const nPoints = Math.ceil(2 * PI * clusterRadiusPx / borderDotRadiusPx);
  const anglesInRadians = [...Array(nPoints).keys()].map(x => x * 2 * PI / nPoints);
  const offsets = anglesInRadians.map(x => [clusterRadiusPx * cos(x), clusterRadiusPx * sin(x)]);
  return offsets;
}

/**
 * Get layers styles
 * @param {String} sourceName
 * @param {Array} layerSettings
 * @param {Number} radius
 * @param {Object} dataColors
 * @param {Number} minzoom
 * @param {Number} maxzoom
 * @return {Array} an array of mapboxgl style layers for drawing the borders
 */
export function getBorderLayers(
  sourceName,
  offsets,
  radius,
  dataColors,
  minzoom = 0,
  maxzoom = 24
) {
  const stops = [...Object.entries(dataColors), ['_pause_el', 'transparent']];

  return offsets.map((offset, i) => {
    return {
      id: `cluster-border-dot-${i}`,
      type: 'circle',
      source: sourceName,
      filter: ['has', 'point_count'],
      minzoom,
      maxzoom,
      paint: {
        'circle-blur': 0.5,
        'circle-color': {
          property: `_border-${i}`,
          type: 'categorical',
          stops
        },
        'circle-radius': radius,
        'circle-translate': offset
      }
    };
  });
}
