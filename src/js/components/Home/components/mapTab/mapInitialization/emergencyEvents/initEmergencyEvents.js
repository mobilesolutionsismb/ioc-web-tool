import { featureCollection } from '@turf/helpers';
import {
  selectedEvent,
  selectedEventIcon,
  selectedEventAOI,
  selectedEventAOIOutline,
  activeEvent,
  activeEventIcon,
  activeEventAOI,
  activeEventAOIOutline,
  getFiltersForEventId
} from './selectedFeatures';

import { EMERGENCY_EVENTS_DATASOURCE_NAME } from './featureConstants';

export { EMERGENCY_EVENTS_DATASOURCE_NAME, getFiltersForEventId };

/**
 *
 * @param {mapboxgl:Map} map
 * @param {Array} emergencyEvents
 */
export function initEmergencyEventFeatures(map, emergencyEvents) {
  // Add source for all I-REACT emergency events data
  map.addSource(EMERGENCY_EVENTS_DATASOURCE_NAME, {
    type: 'geojson',
    data: featureCollection(emergencyEvents)
  });
  map.addLayer(selectedEventAOI);
  map.addLayer(selectedEventAOIOutline);
  map.addLayer(activeEventAOI);
  map.addLayer(activeEventAOIOutline);

  map.addLayer(selectedEvent);
  map.addLayer(selectedEventIcon);
  map.addLayer(activeEvent);
  map.addLayer(activeEventIcon);
  return Promise.resolve();
}

/**
 *
 * @param {mapboxgl:Map} map
 * @param {Array} emergencyEvents
 */
export function updateEmergencyEventsDataSource(map, emergencyEvents, handlersMap = {}) {
  const emeDataSource = map.getSource(EMERGENCY_EVENTS_DATASOURCE_NAME);
  const emergencyEventsFeatureCollection = featureCollection(emergencyEvents);
  if (emeDataSource) {
    emeDataSource.setData(emergencyEventsFeatureCollection);
  }
  return Promise.resolve();
}
