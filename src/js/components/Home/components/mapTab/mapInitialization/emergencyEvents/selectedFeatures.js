import { white, /* black, */ grey500 } from 'material-ui/styles/colors';
import { darken } from 'material-ui/utils/colorManipulator';

import {
  EMERGENCY_EVENTS_DATASOURCE_NAME,
  DEFAULT_PINPOINT_SIZE,
  DEFAULT_ICON_SIZE,
  SELECTED_EVENT_LAYER_COLOR,
  ACTIVE_EVENT_LAYER_COLOR,
  ONGOING_EVT_COLOR
} from './featureConstants';

import {
  IS_EMERGENCY_EVENT,
  IS_EMERGENCY_EVENT_AOI,
  IS_DESELECTED_EMERGENCY_EVENT_POINT,
  IS_DESELECTED_EMERGENCY_EVENT_AOI
} from '../staticFiltersDefinitions';

export function getFiltersForEventId(eventId) {
  return {
    point: ['all', IS_EMERGENCY_EVENT, ['==', 'id', eventId]], // to be used with point features
    aoi: ['all', IS_EMERGENCY_EVENT_AOI, ['==', 'id', eventId]] // to be used with polygon features
  };
}

function layerFactory(layerId, color, textColor, pinPointBorderColor = grey500) {
  // Pinpoint
  const pointLayer = {
    id: layerId,
    source: EMERGENCY_EVENTS_DATASOURCE_NAME,
    type: 'symbol',
    layout: {
      'text-font': ['I-REACT_Pinpoints'],
      'text-field': '\ue91c',
      'text-size': DEFAULT_PINPOINT_SIZE,
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 2,
      'text-halo-color': {
        type: 'categorical',
        property: 'isOngoing',
        stops: [[true, darken(ONGOING_EVT_COLOR, 0.15)], [false, ONGOING_EVT_COLOR]]
      },
      'text-color': {
        type: 'categorical',
        property: 'isOngoing',
        stops: [[true, ONGOING_EVT_COLOR], [false, color]]
      }
    },
    filter: IS_DESELECTED_EMERGENCY_EVENT_POINT
  };
  // Pinpoint icon/text
  const iconLayer = {
    id: `${layerId}-icon`,
    type: 'symbol',
    source: EMERGENCY_EVENTS_DATASOURCE_NAME,
    layout: {
      'text-font': ['I-REACT_Pinpoints'],
      'text-field': {
        type: 'categorical',
        property: 'hazard',
        stops: [
          ['none', ' '],
          ['fire', '\ue916'],
          ['flood', '\ue917'],
          ['extremeWeather', '\ue915'],
          ['landslide', '\ue913'],
          ['avalanches', '\ue913'], // WRONG THIS IS same as LANDSLIDE
          ['earthquake', '\ue914']
        ],
        default: ''
      },
      'text-transform': 'uppercase',
      'text-size': DEFAULT_ICON_SIZE,
      'text-offset': [0, -0.1],
      'text-allow-overlap': true
    },
    paint: {
      'text-color': {
        type: 'categorical',
        property: 'isOngoing',
        stops: [[true, color], [false, ONGOING_EVT_COLOR]]
      },
      'text-halo-width': 1,
      'text-halo-color': {
        type: 'categorical',
        property: 'isOngoing',
        stops: [[true, ONGOING_EVT_COLOR], [false, color]]
      }
    },
    filter: IS_DESELECTED_EMERGENCY_EVENT_POINT
  };
  // Area (Polygon)
  const areaLayer = {
    id: `${layerId}-aoi`,
    type: 'fill',
    source: EMERGENCY_EVENTS_DATASOURCE_NAME,
    paint: {
      'fill-color': ONGOING_EVT_COLOR,
      'fill-opacity': 0.2,
      'fill-outline-color': ONGOING_EVT_COLOR
    },
    filter: IS_DESELECTED_EMERGENCY_EVENT_AOI
  };
  // Area Outline (Polygon dashed border)
  const areaOutlineLayer = {
    id: `${layerId}-aoi-outline`,
    type: 'line',
    source: EMERGENCY_EVENTS_DATASOURCE_NAME,
    paint: {
      'line-color': ONGOING_EVT_COLOR,
      'line-dasharray': [0.2, 2],
      'line-width': 2
    },
    filter: IS_DESELECTED_EMERGENCY_EVENT_AOI
  };

  return {
    pointLayer,
    iconLayer,
    areaLayer,
    areaOutlineLayer
  };
}

const selectedEventConfig = layerFactory(
  'selected-event',
  SELECTED_EVENT_LAYER_COLOR,
  white,
  ONGOING_EVT_COLOR
);
const activeEventConfig = layerFactory(
  'active-event',
  ACTIVE_EVENT_LAYER_COLOR,
  white,
  ONGOING_EVT_COLOR
);

export const selectedEvent = selectedEventConfig.pointLayer;
export const selectedEventIcon = selectedEventConfig.iconLayer;
export const selectedEventAOI = selectedEventConfig.areaLayer;
export const selectedEventAOIOutline = selectedEventConfig.areaOutlineLayer;

export const activeEvent = activeEventConfig.pointLayer;
export const activeEventIcon = activeEventConfig.iconLayer;
export const activeEventAOI = activeEventConfig.areaLayer;
export const activeEventAOIOutline = activeEventConfig.areaOutlineLayer;
