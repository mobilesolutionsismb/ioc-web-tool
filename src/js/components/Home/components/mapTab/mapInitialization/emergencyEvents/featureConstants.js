import {
  ONGOING_EVT_COLOR,
  SELECTED_EVENT_LAYER_COLOR,
  ACTIVE_EVENT_LAYER_COLOR
} from 'js/startup/iReactTheme';

export const EMERGENCY_EVENTS_DATASOURCE_NAME = 'emergency-events';

export const DEFAULT_PINPOINT_SIZE = {
  base: 1.5,
  stops: [[0, 13], [12, 26], [20, 52], [24, 56]]
};

export const DEFAULT_ICON_SIZE = {
  base: 1.5,
  stops: [[0, 20], [12, 26], [20, 36], [24, 40]]
};

export { ONGOING_EVT_COLOR, SELECTED_EVENT_LAYER_COLOR, ACTIVE_EVENT_LAYER_COLOR };
