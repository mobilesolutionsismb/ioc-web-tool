export const IS_CLUSTER = ['has', 'point_count'];

// IREACT FEATURES

export const IS_UNCLUSTERED_REPORT = ['==', 'itemType', 'report'];

export const IS_UNCLUSTERED_EMERGENCY_COMMUNICATION = ['==', 'itemType', 'emergencyCommunication'];
export const IS_UNCLUSTERED_EMERGENCY_COMMUNICATION_AOI = [
  '==',
  'itemType',
  'emergencyCommunicationAOI'
];

export const IS_UNCLUSTERED_MISSION = ['==', 'itemType', 'mission'];
export const IS_UNCLUSTERED_MISSION_AOI = ['==', 'itemType', 'missionAOI'];
export const IS_UNCLUSTERED_MISSION_TASK = [
  'all',
  ['==', 'itemType', 'missionTask'],
  ['==', 'missionId', -1]
];

export const IS_UNCLUSTERED_AGENTLOCATION = ['==', 'itemType', 'agentLocation'];

export const IS_DESELECTED_REPORT = ['all', IS_UNCLUSTERED_REPORT, ['==', 'id', -1]];
export const IS_DESELECTED_EMERGENCY_COMMUNICATION = [
  'all',
  IS_UNCLUSTERED_EMERGENCY_COMMUNICATION,
  ['==', 'id', -1]
];
export const IS_DESELECTED_EMERGENCY_COMMUNICATION_AOI = [
  'all',
  IS_UNCLUSTERED_EMERGENCY_COMMUNICATION_AOI,
  ['==', 'id', -1]
];

export const IS_DESELECTED_MISSION = ['all', IS_UNCLUSTERED_MISSION, ['==', 'id', -1]];
export const IS_DESELECTED_MISSION_AOI = ['all', IS_UNCLUSTERED_MISSION, ['==', 'id', -1]];
export const IS_DESELECTED_MISSION_TASK = ['all', IS_UNCLUSTERED_MISSION_TASK, ['==', 'id', -1]];

export const IS_DESELECTED_AGENTLOCATION = ['all', IS_UNCLUSTERED_AGENTLOCATION, ['==', 'id', -1]];

// EVENTS

export const IS_EMERGENCY_EVENT = ['==', 'itemType', 'emergencyEvent'];
export const IS_EMERGENCY_EVENT_AOI = ['==', 'itemType', 'emergencyEventAOI'];
export const IS_DESELECTED_EMERGENCY_EVENT_POINT = ['all', IS_EMERGENCY_EVENT, ['==', 'id', -1]];
export const IS_DESELECTED_EMERGENCY_EVENT_AOI = ['all', IS_EMERGENCY_EVENT_AOI, ['==', 'id', -1]];
