import React, { Component } from 'react';
import { IconButton, FontIcon, Toolbar, ToolbarGroup, ToolbarTitle, Paper } from 'material-ui';
import LayerPanel from './LayerPanel';
import { compose } from 'redux';

import Draggable from 'react-draggable';

import { withDictionary } from 'ioc-localization';
import { withLayersWidget } from 'js/modules/LayersWidget';

const enhance = compose(withDictionary, withLayersWidget);

class LayersSelectionWidget extends Component {
  render() {
    let { dictionary } = this.props;

    const header = (
      <Toolbar>
        <ToolbarGroup>
          <ToolbarTitle style={{ color: 'white' }} text={dictionary._layers} />
          <div style={{ textAlign: 'right' }}>
            <IconButton>
              <FontIcon className="material-icons">more_vert</FontIcon>
            </IconButton>
            <IconButton
              onClick={() => {
                this.props.setOpenLayersWidget(false);
              }}
            >
              <FontIcon className="material-icons">close</FontIcon>
            </IconButton>
          </div>
        </ToolbarGroup>
      </Toolbar>
    );

    return (
      <Draggable
        bounds=".map.tab"
        handle=".handle"
        defaultPosition={{ x: 0, y: 0 }}
        position={null}
        grid={[25, 25]}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}
      >
        <Paper
          zDepth={1}
          className="layerWidget"
          style={
            this.props.isLayersWidgetOpen
              ? {
                  minHeight: '200px',
                  maxHeight: '800px',
                  maxWidth: '500px'
                }
              : {
                  height: '0',
                  width: '0',
                  opacity: '0',
                  zIndex: '-1'
                }
          }
        >
          <div children={header} className="handle" />
          <LayerPanel mapView={this.props.mapView} dictionary={dictionary} />
        </Paper>
      </Draggable>
    );
  }
}

export default enhance(LayersSelectionWidget);
