import React, { Component } from 'react';
import {
  /* RadioButtonGroup, */ /* IconButton,
  FontIcon, */
  List,
  Subheader /* , Checkbox */
} from 'material-ui';
import { compose } from 'redux';
// import { withDictionary } from 'ioc-localization';
// import { withMap } from 'js/modules/map';
import { withLayersWidget } from 'js/modules/LayersWidget';
import { withRouter } from 'react-router';
import { withMapLayers, withGeneralSetting } from 'ioc-api-interface';
import LayerPanelItemsGroup from './LayerPanelItemsGroup';
import { ResizableBox } from 'react-resizable';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';
import PoiLayerWidget from './PoiLayerWidget';
const enhance = compose(
  /* withDictionary, */ withLayersWidget,
  withGeneralSetting,
  withMapLayers,
  withLayersAndSettings
);

const LayerPanelHeaderGroup = props => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: '#525252'
    }}
  >
    <Subheader> {props.title} </Subheader>
  </div>
);
class LayerPanel extends Component {
  constructor(props) {
    super(props);
    this.state = { layersTree: {} };
  }
  // _init = async props => {
  //   props.getDefinitionGroupTreeSettings();
  //   props.getSettingDefinitionsByGroup();

  //   await props.getLayers();
  //   await props.loadSettings();
  // };

  // componentDidMount() {
  //   this._init(this.props);
  // }

  _layersTreeConfig(tree, key) {
    if (!tree) {
      return;
    }

    for (var i = 0; i < tree.length; i++) {
      if (tree[i].name === key) {
        return tree[i];
      } else return this._layersTreeConfig(tree[i].children, key);
    }
  }

  _checkSettings = layer => {
    if (layer.relatedSettingsDefinitions) {
      const settingName = layer.relatedSettingsDefinitions.find(
        setting => setting.settingDefinitionType === 'showHide'
      );
      if (this.props.generalSettings.userSettings[settingName] === 'Hide') {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  };

  // showLayerInList = (item) => {
  //   let show = false;
  //   item.options.forEach(
  //     (child) => {
  //       if (child.options) {
  //         child.options.forEach(subChild => {
  //           if (subChild.options) {
  //             if (subChild.options.filter(suberChild => suberChild.showHide === 'Show').length > 0) show = true;
  //           }
  //           else {
  //             show = subChild.showHide === 'Show';
  //           }
  //         });
  //       }
  //       else {
  //         show = child.showHide === 'Show';
  //       }
  //     }
  //   );
  //   return show;
  // }

  render() {
    let { dictionary } = this.props;
    const layersTreeData = this._layersTreeConfig(
      this.props.remoteLayers.children,
      'App.UserSettingsGroups.Layers'
    );

    return (
      <ResizableBox
        className="box"
        width={300}
        height={400}
        minConstraints={[220, 200]}
        maxConstraints={[500, 747]}
      >
        <div className="text">
          {layersTreeData &&
            layersTreeData.children &&
            layersTreeData.children.map((groupLayer, index) => (
              <div key={index}>
                <div>
                  {groupLayer.displayName ? (
                    <LayerPanelHeaderGroup
                      {...groupLayer}
                      history={this.props.history}
                      title={groupLayer.displayName}
                    />
                  ) : null}
                  <List style={{ padding: '0' }}>
                    {groupLayer.children.map((son, indexSon) => (
                      <LayerPanelItemsGroup
                        mapView={this.props.mapView}
                        key={indexSon}
                        father={groupLayer}
                        item={son}
                        id={groupLayer.name}
                        layersTree={groupLayer}
                      />
                    ))}
                    {groupLayer.name === 'App.UserSettingsGroups.Layers.General' ? (
                      groupLayer.children.map((son, indexSon) => (
                        <PoiLayerWidget key={indexSon} dictionary={dictionary} />
                      ))
                    ) : (
                      <div />
                    )}
                  </List>
                </div>
                {/* {this.showLayerInList(this.state.layersTree[index]) ?
                  <div>
                    {this.state.layersTree[index].subheader ? <LayerPanelHeaderGroup {...this.state.layersTree[index]} history={this.props.history} title={this.state.layersTree[index].subheader} /> : null}
                    <List style={{ padding: '0' }}>
                      {this.state.layersTree[index].options.map(
                        (son, indexSon) => (
                          <LayerPanelItemsGroup mapView={this.props.mapView} key={indexSon} father={this.state.layersTree[index]} item={son} id={this.state.layersTree[index].id} layersTree={this.state.layersTree} />
                        )
                      )}
                    </List>
                  </div> :
                  null} */}
              </div>
            ))}
        </div>
      </ResizableBox>
    );
  }
}

export default withRouter(enhance(LayerPanel));
