export function checkAvailable(iReactTasks, availableLayers) {
  if (iReactTasks && iReactTasks.length > 0 && availableLayers) {
    return availableLayers.some(l => iReactTasks.indexOf(l) > -1);
  } else {
    return false;
  }
}

export function addLayerOnMap(mapView, layers) {
  //if (layers) layers.map((layer) => map.setLayerVisible(`layer-${layer}`));
  if (layers) {
    const map = mapView.getMap();
    if (map != null)
      layers.forEach(layer => {
        map.setLayoutProperty(`layer-${layer}`, 'visibility', 'visible');
        map.setPaintProperty(`layer-${layer}`, 'raster-opacity', 0.8);
      });
  }
}

export function removeLayerOnMap(mapView, layers, iReactTasks) {
  if (Array.isArray(layers)) {
    for (let layer of layers) {
      if (
        Object.keys(iReactTasks.layers).some(item => parseInt(item, 10) === parseInt(layer, 10))
      ) {
        const map = mapView.getMap();
        if (map != null) map.setLayoutProperty(`layer-${layer}`, 'visibility', 'none');
      }
    }
  }
}
