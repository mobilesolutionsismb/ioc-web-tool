import React, { Component } from 'react';
import { FontIcon, ListItem, Checkbox, /*Toggle,*/ IconButton } from 'material-ui';
import { compose } from 'redux';

import { withDictionary } from 'ioc-localization';
import { withLegendsWidget } from 'js/modules/LegendsWidget';
import { withMapLayers } from 'ioc-api-interface';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';

import { getDurationFormat } from 'js/utils/localizeDate';
import { checkAvailable } from './layersUtils';

import LayerOptionsGroup from './LayerOptionsGroup';
import CopernicusLayerWidget from './CopernicusLayerWidget';

const enhance = compose(
  withDictionary,
  withLayersAndSettings,
  withMapLayers,
  withLegendsWidget
);

class LayerPanelItemsGroup extends Component {
  item = this.props.item;
  id = this.props.id;
  father = this.props.father;
  child = [];

  state = {
    checked: false
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.activeIReactSettingNames !== this.props.activeIReactSettingNames ||
      prevProps.activeCopernicusNames !== this.props.activeCopernicusNames
    ) {
      if (this.props.id === 'App.UserSettingsGroups.Layers.ExternalServices')
        this.setState({ checked: this.props.activeCopernicusNames.length > 0 });
      else this.setState({ checked: this.getIsChecked() });
    }
  }

  removeLayerComponent = () => {
    if (this._isLastChild()) {
      this.props.setOpenLegendsWidget(false);
      this.props.addLegend(null);
      this.props.deactivateIReactSettingByName(this.item.name);
    } else {
      this.child.forEach((a, index) => this.child[index].removeLayerComponent());
    }
  };

  getIsChecked = () => {
    return this.item.children
      ? this.child.some(
          (a, index) => this.child[index] && this.child[index].getIsChecked() === true
        )
      : this.props.activeIReactSettingNames
      ? this.props.activeIReactSettingNames.some((a, i) => a === this.item.name)
      : false;
  };

  _onCheckPressed = event => {
    if (this.getIsChecked()) {
      this.removeLayerComponent();
    } else if (this._isLastChild()) {
      this._clearRadio();
      this._addLayer();
    }
  };

  _addLayer = () => {
    this._onLegendButtonClick(this.item);
    this.props.activateIReactSettingByName(this.item.name);
  };

  /** Obtain the layer info that match with the tasksIds selected
   * @param {Array<number>} taskIds array of related task id
   * @param {Array<Layer>} layers
   * @return filtered layers by taskIds
   * */
  _remapLayerInfo(taskIds, layers) {
    return layers.filter(e => taskIds.indexOf(parseInt(e.taskId, 10)) > -1); // remove layers whose taskId is not in taskIds
  }

  _onLegendButtonClick(item) {
    const layers = this._remapLayerInfo(item.iReactTasks, this.props.mapLayers.layers);
    this.props.setOpenLegendsWidget(true);
    this.props.addLegend(layers[0]);
  }

  /**
   * Access the last leaf of the tree and delete that layer from the active layer array
   */
  _clearRadio = () => {
    if (this.item.type === 'radio' || !this.item.type) {
      this.father.children.forEach(a => {
        if (a.children) {
          a.children.forEach(b => {
            if (b.children) {
              b.children.forEach(c => this.props.deactivateIReactSettingByName(c.name));
            } else {
              this.props.deactivateIReactSettingByName(b.name);
            }
          });
        } else {
          this.props.deactivateIReactSettingByName(a.name);
        }
      });
    }
  };

  _isLastChild = () => {
    if (this.item.children) {
      return false;
    } else {
      return true;
    }
  };

  /**
   * First checks the settings (showHide) and then verifies if the layers is in the available list
   * @param showHide bool
   * @param ireactTasks <Array<int>> list of ireact tasks in the layer
   * @return bool
   */
  _checkSettingsToShow(showHide, iReactTasks, name) {
    return (
      showHide === 'Show' && checkAvailable(iReactTasks, this.props.mapLayers.availableTaskIds)
    );
  }

  /**
   * Accesses the last leaf of the tree and checks the settings to show or not the layer
   * @return bool
   */
  _showLayerInList = () => {
    if (!this._isLastChild()) {
      let show = false;
      this.item.children.forEach(child => {
        if (child.children) {
          if (
            child.children.filter(subChild =>
              this._checkSettingsToShow(subChild.showHide, subChild.iReactTasks)
            ).length > 0
          ) {
            show = true;
          }
        } else {
          if (this._checkSettingsToShow(child.showHide, child.iReactTasks)) show = true;
        }
      });
      return show;
    } else {
      return this._checkSettingsToShow(this.item.showHide, this.item.iReactTasks);
    }
  };

  render() {
    let { item } = this.props;
    const onLegendButtonClick = this._onLegendButtonClick.bind(this, item);
    let children = null;
    if (this.props.id === 'App.UserSettingsGroups.Layers.ExternalServices') {
      children = [<CopernicusLayerWidget key={0} />];
    } else {
      children = item.children
        ? item.children.map((a, i) => (
            <LayerOptionsGroup
              onRef={ref => (this.child[i] = ref)}
              key={i}
              mapView={this.props.mapView}
              item={a}
              id={this.id}
              father={item}
              clearFatherRadio={this.clearRadio}
              ref={`child${i}`}
              layersTree={this.props.layersTree}
            />
          ))
        : [];
    }
    const groupItems = this._showLayerInList() ? (
      <div ref={c => (this.lisItem = c)}>
        <ListItem
          innerDivStyle={{ paddingLeft: '56px' }}
          primaryText={item.displayName}
          secondaryText={getDurationFormat(item.leadTime || '')}
          primaryTogglesNestedList={!this._isLastChild()}
          nestedListStyle={{ padding: 0 }}
          onNestedListToggle={() =>
            this.lisItem != null
              ? this.lisItem.scrollIntoView({
                  block: 'nearest',
                  inline: 'nearest',
                  behavior: 'smooth'
                })
              : {}
          }
          rightIconButton={
            this._isLastChild() ? (
              <IconButton
                onClick={onLegendButtonClick}
                style={{ padding: 0, color: '#fff' }}
                title="Legends"
              >
                <i className="material-icons" style={{ color: '#fff' }}>
                  add
                </i>
              </IconButton>
            ) : null
          }
          leftCheckbox={
            <Checkbox
              checked={this.state.checked}
              onCheck={event => this._onCheckPressed(event)}
              checkedIcon={<FontIcon className="material-icons">radio_button_checked</FontIcon>}
              uncheckedIcon={<FontIcon className="material-icons">radio_button_unchecked</FontIcon>}
            />
          }
          nestedItems={children}
        />
      </div>
    ) : null;
    return <div>{groupItems}</div>;
  }
}
export default enhance(LayerPanelItemsGroup);
