import React, { Component } from 'react';
import { ListItem, Checkbox, FontIcon, SelectField, MenuItem, IconButton } from 'material-ui';
import { compose } from 'redux';

import { withMapLayers } from 'ioc-api-interface';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';
import { withLegendsWidget } from 'js/modules/LegendsWidget';

const enhance = compose(
  withMapLayers,
  withLegendsWidget,
  withLayersAndSettings
);

class CopernicusLayerWidget extends Component {
  state = {
    hazardSelected: 0
  };

  listItem = [];

  _handleChange = (event, index, value) => this.setState({ hazardSelected: value });

  _onCheckLayer = async (checked, item, childrenChecked, father) => {
    if (checked) this.props.deactivateCopernicusLayer(item.id);
    else {
      await this._clearRadio(father);
      await this.props.activateCopernicusLayer(item.id);
      this.props.setOpenLegendsWidget(true);
      this.props.addLegend(item.layers[0]);
    }
  };
  _clearRadio(father) {
    Object.values(father.children).forEach(child => this.props.deactivateCopernicusLayer(child.id));
  }
  _onLegendButtonClick(item) {
    this.props.setOpenLegendsWidget(true);
    this.props.addLegend(item.layers[0]);
  }

  render() {
    return (
      <div>
        <SelectField
          floatingLabelText="Hazard"
          value={this.state.hazardSelected}
          onChange={this._handleChange}
          style={{
            paddingLeft: '30px',
            boxSizing: 'border-box',
            overflow: 'hidden',
            width: '100%'
          }}
        >
          {this.props.mapLayers.copernicusLayers.map((hazarType, index) => (
            <MenuItem key={index} value={index} primaryText={hazarType.displayName} />
          ))}
        </SelectField>
        {Object.keys(this.props.mapLayers.copernicusLayers[this.state.hazardSelected].children).map(
          (layer, index) => {
            const layerItem = this.props.mapLayers.copernicusLayers[this.state.hazardSelected]
              .children[layer];
            let childChecked = false;
            const children = Object.keys(layerItem.children).map((child, i) => {
              const item = layerItem.children[child];
              const checked = this.props.activeCopernicusNames.indexOf(item.id) > -1;
              const onLegendButtonClick = this._onLegendButtonClick.bind(this, item);

              childChecked =
                this.props.activeCopernicusNames.indexOf(item.id) > -1 ? true : childChecked;

              return item.layers.length > 0 ? (
                <ListItem
                  key={i}
                  primaryText={child.charAt(0).toUpperCase() + child.substr(1)}
                  nestedListStyle={{ padding: 0 }}
                  innerDivStyle={{ paddingLeft: '56px' }}
                  leftCheckbox={
                    <Checkbox
                      checked={checked}
                      onCheck={event => {
                        this._onCheckLayer(checked, item, childChecked, layerItem);
                      }}
                      checkedIcon={
                        <FontIcon className="material-icons">radio_button_checked</FontIcon>
                      }
                      uncheckedIcon={
                        <FontIcon className="material-icons">radio_button_unchecked</FontIcon>
                      }
                    />
                  }
                  rightIconButton={
                    <IconButton
                      onClick={onLegendButtonClick}
                      style={{ padding: 0, color: '#fff' }}
                      title="Legends"
                    >
                      <i className="material-icons" style={{ color: '#fff' }}>
                        add
                      </i>
                    </IconButton>
                  }
                />
              ) : null;
            });
            return (
              <div key={index} ref={c => (this.listItem[index] = c)}>
                <ListItem
                  innerDivStyle={{ paddingLeft: '56px' }}
                  primaryTogglesNestedList={true}
                  onNestedListToggle={() =>
                    this.lisItem != null
                      ? this.listItem[index].scrollIntoView({
                          block: 'nearest',
                          inline: 'nearest',
                          behavior: 'smooth'
                        })
                      : {}
                  }
                  primaryText={layerItem.displayName}
                  nestedListStyle={{ paddingLeft: '50px' }}
                  style={{ paddingLeft: '30px' }}
                  leftCheckbox={
                    <Checkbox
                      checked={childChecked}
                      onCheck={event => this._clearRadio(layerItem)}
                      checkedIcon={
                        <FontIcon className="material-icons">radio_button_checked</FontIcon>
                      }
                      uncheckedIcon={
                        <FontIcon className="material-icons">radio_button_unchecked</FontIcon>
                      }
                    />
                  }
                  nestedItems={children}
                />
              </div>
            );
          }
        )}
      </div>
    );
  }
}
export default enhance(CopernicusLayerWidget);
