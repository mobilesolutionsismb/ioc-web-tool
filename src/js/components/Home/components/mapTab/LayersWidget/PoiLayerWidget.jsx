import React, { Component } from 'react';
import { ListItem, Checkbox, FontIcon } from 'material-ui';
import { withMapLayerFilters } from 'js/modules/mapLayerFilters';

const MAP_LAYER_FILTER_NAMES = [
  '_is_layer_report_visible',
  '_is_layer_rr_visible',
  '_is_layer_emcomm_visible',
  '_is_layer_mission_visible',
  '_is_layer_agloc_visible',
  '_is_layer_maprequest_visible'
];

class PoiLayerWidget extends Component {
  _changeShowLayer(e, newToggleValue, layerSetting) {
    if (newToggleValue) {
      this.props.addMapLayerFilter('layersVisibility', layerSetting);
    } else {
      this.props.removeMapLayerFilter('layersVisibility', layerSetting);
    }
  }

  render() {
    const { dictionary } = this.props;

    return (
      <div>
        <div style={{ position: 'relative' }}>
          {MAP_LAYER_FILTER_NAMES.map((layerSetting, index) => (
            <ListItem
              key={index}
              innerDivStyle={{ paddingLeft: '56px' }}
              primaryText={dictionary(layerSetting)}
              nestedListStyle={{ padding: 0 }}
              leftCheckbox={
                <Checkbox
                  checked={this.props.mapLayerFilters.layersVisibility.indexOf(layerSetting) >= 0}
                  onCheck={(event, isInputChecked) =>
                    this._changeShowLayer(event, isInputChecked, layerSetting)
                  }
                  checkedIcon={<FontIcon className="material-icons">radio_button_checked</FontIcon>}
                  uncheckedIcon={
                    <FontIcon className="material-icons">radio_button_unchecked</FontIcon>
                  }
                />
              }
            />
          ))}
        </div>
      </div>
    );
  }
}
export default withMapLayerFilters(PoiLayerWidget);
