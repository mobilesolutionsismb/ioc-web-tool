import React, { Component } from 'react';
import { FontIcon, ListItem, Checkbox, IconButton } from 'material-ui';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withLegendsWidget } from 'js/modules/LegendsWidget';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';

import { withMapLayers } from 'ioc-api-interface';
import { checkAvailable } from './layersUtils';
import { getDurationFormat } from 'js/utils/localizeDate';

const enhance = compose(withDictionary, withMapLayers, withLegendsWidget, withLayersAndSettings);

class LayerOptions extends Component {
  componentDidMount() {
    this.props.onRef(this);
  }
  componentWillUnmount() {
    this.props.onRef(null);
  }

  getIsChecked = () => {
    return this.props.activeIReactSettingNames
      ? this.props.activeIReactSettingNames.some((a, i) => a === this.props.item.name)
      : false;
  };

  removeLayerComponent = () => {
    this.props.setOpenLegendsWidget(false);
    this.props.addLegend(null);
    this.props.deactivateIReactSettingByName(this.props.item.name);
  };

  _onCheckPressed = event => {
    if (this.getIsChecked()) {
      this.removeLayerComponent();
    } else {
      this._clearRadio();
      this._addLayerComponent();
    }
  };

  _clearRadio = () => {
    const { item, father, deactivateIReactSettingByName } = this.props;
    if (item.type === 'radio' || !item.type) {
      father.children.forEach(child => deactivateIReactSettingByName(child.name));
    }
  };

  /**
   * Obtain the layer info that match with the tasksIds selected
   * @param {Array<number>} taskIds - array of related task id
   * @param {Array<Layer>} layers
   * @memberof TimeSliderWidget
   */
  _remapLayerInfo(taskIds, layers) {
    return layers.filter(e => taskIds.indexOf(parseInt(e.taskId, 10)) > -1); // remove layers whose taskId is not in taskIds
  }

  _addLayerComponent = async () => {
    this._onLegendButtonClick(this.props.item);
    await this.props.activateIReactSettingByName(this.props.item.name);
  };

  _onLegendButtonClick(item) {
    const layers = this._remapLayerInfo(item.iReactTasks, this.props.mapLayers.layers);
    this.props.setOpenLegendsWidget(true);
    this.props.addLegend(layers[0]);
  }

  /**
   * First checks the settings (showHide) and then verifies if the layers is in the available list
   * @return bool
   */
  _showLayerInList = () => {
    return (
      this.props.item.showHide === 'Show' &&
      checkAvailable(this.props.item.iReactTasks, this.props.mapLayers.availableTaskIds)
    );
  };

  render() {
    const { item } = this.props;

    const onLegendButtonClick = this._onLegendButtonClick.bind(this, item);
    const groupItems = this._showLayerInList() ? (
      <div style={{ paddingLeft: '60px' }}>
        <ListItem
          innerDivStyle={{ paddingLeft: '56px' }}
          primaryText={item.displayName}
          secondaryText={getDurationFormat(item.leadTime || '')}
          rightIconButton={
            <IconButton
              onClick={onLegendButtonClick}
              style={{ padding: 0, color: '#fff' }}
              title="Legends"
            >
              <i className="material-icons" style={{ color: '#fff' }}>
                add
              </i>
            </IconButton>
          }
          leftCheckbox={
            <Checkbox
              checked={this.getIsChecked()}
              checkedIcon={<FontIcon className="material-icons">radio_button_checked</FontIcon>}
              uncheckedIcon={<FontIcon className="material-icons">radio_button_unchecked</FontIcon>}
              onCheck={event => this._onCheckPressed(event)}
            />
          }
        />
      </div>
    ) : null;
    return <div>{groupItems}</div>;
  }
}

export default enhance(LayerOptions);
