import React, { Component } from 'react';
import { FontIcon, ListItem, Checkbox, /* Toggle, */ IconButton } from 'material-ui';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withLegendsWidget } from 'js/modules/LegendsWidget';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';

import { withMapLayers } from 'ioc-api-interface';
import { checkAvailable } from './layersUtils';
import { getDurationFormat } from 'js/utils/localizeDate';
import LayerOptions from './LayerOptions';

// import * as _ from 'lodash';

const enhance = compose(
  withDictionary,
  withLayersAndSettings,
  withMapLayers,
  withLegendsWidget
);

class LayerOptionsGroup extends Component {
  item = this.props.item;
  id = this.props.id;
  father = this.props.father;
  child = [];

  state = {
    checked: false
  };

  componentDidMount() {
    this.props.onRef(this);
    this.setState({ checked: this.getIsChecked() });
  }
  componentWillUnmount() {
    this.props.onRef(null);
  }
  componentDidUpdate(prevProps) {
    if (prevProps.activeIReactSettingNames !== this.props.activeIReactSettingNames) {
      this.setState({ checked: this.getIsChecked() });
    }
  }

  /*
   * Check if in the state of the main group (e.g. meteorological Layers) contains
   * the name of the item.
   * We check if any objects of the gourp array cointains a name of the item at the second level
   * of the object, accessing with the fatherName name.
   */
  getIsChecked = () => {
    return this.item.children
      ? this._checkChilds()
      : this.props.activeIReactSettingNames
      ? this.props.activeIReactSettingNames.some((a, i) => a === this.item.name)
      : false;
  };

  removeLayerComponent = () => {
    if (this._isLastChild()) {
      this.props.setOpenLegendsWidget(false);
      this.props.addLegend(null);
      this.props.deactivateIReactSettingByName(this.item.name);
    } else {
      this.child.forEach((a, index) => this.child[index].removeLayerComponent());
    }
  };

  _onCheckPressed = event => {
    if (this.getIsChecked(this.item.name)) {
      this.removeLayerComponent();
    } else if (this._isLastChild()) {
      this._clearRadio();
      this._addLayerComponent();
    }
  };

  /**
   * Access the last leaf of the tree and delete that layer from the active layer array
   */
  _clearRadio = () => {
    if (this.item.type === 'radio' || !this.item.type) {
      this.father.children.forEach(a => {
        if (a.children) {
          a.children.forEach(b => {
            this.props.deactivateIReactSettingByName(b.name);
          });
        } else {
          this.props.deactivateIReactSettingByName(a.name);
        }
      });
    }
  };

  _addLayerComponent = async () => {
    this._onLegendButtonClick(this.item);
    this.props.activateIReactSettingByName(this.item.name);
  };

  _checkChilds = () => {
    return this.child.length > 0
      ? this.child.some(
          (a, index) => this.child[index] && this.child[index].getIsChecked() === true
        )
      : this.item.children.some((child, index) =>
          this.props.activeIReactSettingNames
            ? this.props.activeIReactSettingNames.some((a, i) => a === child.name)
            : false
        );
  };

  _isLastChild = () => {
    if (this.item.children) {
      return false;
    } else {
      return true;
    }
  };

  _remapLayerInfo(taskIds, layers) {
    return layers.filter(e => taskIds.indexOf(parseInt(e.taskId, 10)) > -1); // remove layers whose taskId is not in taskIds
  }

  _onLegendButtonClick(item) {
    const layers = this._remapLayerInfo(item.iReactTasks, this.props.mapLayers.layers);
    this.props.setOpenLegendsWidget(true);
    this.props.addLegend(layers[0]);
  }

  /**
   * First checks the settings (showHide) and then verifies if the layers is in the available list
   * @param showHide bool
   * @param ireactTasks <Array<int>> list of ireact tasks in the layer
   * @return bool
   */
  _checkSettingsToShow(showHide, iReactTasks) {
    return (
      showHide === 'Show' && checkAvailable(iReactTasks, this.props.mapLayers.availableTaskIds)
    );
  }

  /**
   * Accesses the last leaf of the tree and checks the settings to show or not the layer
   * @return bool
   */
  _showLayerInList = () => {
    if (!this._isLastChild()) {
      return (
        this.item.children.filter(child =>
          this._checkSettingsToShow(child.showHide, child.iReactTasks)
        ).length > 0
      );
    } else {
      return this._checkSettingsToShow(this.item.showHide, this.item.iReactTasks);
    }
  };

  render() {
    let { item } = this.props;
    const onLegendButtonClick = this._onLegendButtonClick.bind(this, item);
    const children = item.children
      ? item.children.map((a, i) => (
          <LayerOptions
            onRef={ref => (this.child[i] = ref)}
            key={i}
            mapView={this.props.mapView}
            item={a}
            id={this.id}
            father={item}
            clearFatherRadio={this.clearRadio}
          />
        ))
      : [];
    const groupItems = this._showLayerInList() ? (
      <div
        style={this._isLastChild() ? { paddingLeft: '30px' } : {}}
        ref={c => (this.lisItems = c)}
      >
        <ListItem
          onNestedListToggle={() =>
            this.lisItem != null
              ? this.lisItems.scrollIntoView({
                  block: 'nearest',
                  inline: 'nearest',
                  behavior: 'smooth'
                })
              : {}
          }
          innerDivStyle={{ paddingLeft: '56px' }}
          style={!this._isLastChild() ? { paddingLeft: '30px' } : null}
          primaryText={item.displayName}
          secondaryText={getDurationFormat(item.leadTime || '')}
          primaryTogglesNestedList={!this._isLastChild()}
          nestedListStyle={{ padding: 0 }}
          rightIconButton={
            this._isLastChild() ? (
              <IconButton
                onClick={onLegendButtonClick}
                style={{ padding: 0, color: '#fff' }}
                title="Legends"
              >
                <i className="material-icons" style={{ color: '#fff' }}>
                  add
                </i>
              </IconButton>
            ) : null
          }
          leftCheckbox={
            <Checkbox
              checked={this.state.checked}
              onCheck={event => this._onCheckPressed(event)}
              checkedIcon={<FontIcon className="material-icons">radio_button_checked</FontIcon>}
              uncheckedIcon={<FontIcon className="material-icons">radio_button_unchecked</FontIcon>}
            />
          }
          nestedItems={children}
        />
      </div>
    ) : null;
    return <div>{groupItems}</div>;
  }
}

export default enhance(LayerOptionsGroup);
