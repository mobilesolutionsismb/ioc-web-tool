import React, { Component } from 'react';
// import { compose } from 'redux';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';

import { withRouter } from 'react-router';
import { withDictionary } from 'ioc-localization';

import STYLES from './styles';

// const enhance = compose( withDictionary);

class GoogleSearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ''
    };
  }

  onTodoChange = value => {
    this.setState({
      searchText: value
    });
  };

  submitPosition = event => {
    event.preventDefault();
    console.log('A text was submitted: ', this.state.searchText);
  };
  render() {
    let { dictionary } = this.props;

    return (
      <div className="floating-controls" style={{ ...STYLES.controls, ...STYLES.googleSearchBox }}>
        <form onSubmit={this.submitPosition}>
          <input
            style={{
              padding: 10,
              boxShadow: 'rgba(0, 0, 0, 0.16) 0px 3px 10px, rgba(0, 0, 0, 0.23) 0px 3px 10px',
              border: 'none',
              width: '250px'
            }}
            placeholder={dictionary._search}
            onChange={e => this.onTodoChange(e.target.value)}
            value={this.state.searchText}
          />
          <IconButton
            type="submit"
            className="ui-button"
            iconStyle={{ fontSize: '12px', color: '#7b7b7b' }}
            style={STYLES.searchIcon}
          >
            <FontIcon className="material-icons">search</FontIcon>
          </IconButton>
        </form>
      </div>
    );
  }
}

export default withRouter(withDictionary(GoogleSearchBox));
