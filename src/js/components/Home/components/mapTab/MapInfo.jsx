import React, { Component } from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { CloseIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { withMapInfo } from 'js/modules/LegendsWidget';
import Draggable from 'react-draggable';
import { guessDate } from 'js/utils/localizeDate';

// const METADATA_WHITELIST = [
//   'count',
//   'mean',
//   'max',
//   'min',
//   'avg',
//   'std',
//   'pctl_90',
//   'pctl_10',
//   'quartile1',
//   'quartile2',
//   'quartile3',
//   'value',
//   'descr',
//   'lolimit',
//   'hilimit',
//   'class',
//   't_ref_st',
//   't_ref_ed',
//   't_ref_do_r',
//   't_ref_do_c',
//   'units',
//   'valper',
//   'season',
//   'sceclim',
//   'launch_date'
// ];

const METADATA_BLACKLIST = [
  'stroke',
  'stroke_width',
  'stroke_opacity',
  'fill',
  'fill_opacity',
  'shape_rendering',
  'geom'
];

class MapInfo extends Component {
  titleOfProperty = title => {
    const lowerCase = title.replace('_', ' ').toLowerCase();
    return lowerCase.charAt(0).toUpperCase() + lowerCase.slice(1);
  };
  render() {
    if (this.props.mapInfo.length > 0) {
      const mappedInfo = this.props.mapInfo.reduce((all, info) => {
        const c = Object.keys(info.properties)
          .filter(prName => METADATA_BLACKLIST.indexOf(prName) === -1)
          .reduce(
            (prev, k) => {
              const value = info.properties[k];
              if (value !== null && typeof value !== 'undefined') {
                prev.properties[k] = value;
              }
              return prev;
            },
            {
              properties: {}
            }
          );
        // let c = {
        //   properties: {}
        // };
        // for (let k of METADATA_WHITELIST) {
        //   if (info.properties[k]) {
        //     c.properties[k] = info.properties[k];
        //   }
        // }
        if (Object.keys(c.properties).length > 0) {
          all = [...all, { title: info.title, properties: c.properties }];
        }
        return all;
      }, []);
      if (mappedInfo.length > 0) {
        return (
          <Draggable
            // key={0}
            bounds=".map.tab"
            handle=".map-info"
            defaultPosition={{ x: 0, y: 0 }}
            position={null}
            grid={[25, 25]}
          >
            <div
              className="map-info"
              style={{
                top: this.props.position[1],
                left: this.props.position[0]
              }}
            >
              <div
                className="map-info-header"
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: 'calc(100% - 16px)'
                }}
              >
                <span>{`Values at ${this.props.coordinates[1].toFixed(
                  6
                )}, ${this.props.coordinates[0].toFixed(6)}`}</span>
                <CloseIcon
                  style={{
                    boxSizing: 'border-box',
                    padding: '0px 0px 2px 0px',
                    width: 20,
                    height: '100%'
                  }}
                  iconStyle={{ fontSize: '17px', color: 'lightgrey' }}
                  onClick={() => this.props.resetMapInfo()}
                />
              </div>
              <div
                style={{
                  width: '100%',
                  maxHeight: 600,
                  overflow: 'auto'
                }}
              >
                {mappedInfo.map((info, j) => [
                  <h5 style={{ paddingLeft: 8 }} key={`title-${j}`}>
                    {info.title}
                  </h5>,
                  Object.keys(info.properties).map((p, index) => (
                    <div key={`info-${j}-${index}`} className="map-info-content">
                      {p === 'fill' ? (
                        <div key={p}>
                          <b>{this.titleOfProperty(p)}: </b>
                          <span
                            style={{
                              paddingLeft: '10px',
                              fontWeight: '100',
                              backgroundColor: info.properties[p]
                            }}
                          />
                          {info.properties[p]}
                        </div>
                      ) : (
                        <div key={p}>
                          <b>{this.titleOfProperty(p)}: </b>
                          <span style={{ paddingLeft: '10px', fontWeight: '100' }}>
                            {guessDate(info.properties[p])}
                          </span>
                        </div>
                      )}
                    </div>
                  ))
                ])}
              </div>
            </div>
          </Draggable>
        );
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}

export default withMapInfo(muiThemeable()(MapInfo));
