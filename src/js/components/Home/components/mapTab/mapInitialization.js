// import { getMapSettings, attachCursorEvents } from 'js/components/MapView';
// import { point, featureCollection } from '@turf/helpers';
// import { variableCircleInMeters, DEFAULT_POSITION_CIRCLE_PAINT } from 'js/modules/map';
// import {
//     REPORT_COLORS, REPORT_CATEGORIES,
//     EMCOMM_COLORS, EMCOMM_CATEGORIES, EMCOMM_LETTER_COLORS,
//     REPORT_REQUEST_CATEGORIES, REPORT_REQUEST_COLORS, REPORT_REQUEST_LETTER_COLORS
// } from './layersConfig';
// import { STATUS_FILTERS } from 'js/modules/reportFilters';

// export const SELECTED_EVENT_LAYER_COLOR = '#999999';
// export const ACTIVE_EVENT_LAYER_COLOR = '#9B98DA';
// export const SELECTED_EMCOMM_LAYER_COLOR = '#7FFF00';
// export const SELECTED_REP_REQ_LAYER_COLOR = '#7FFFFF';

// export function onMapLoad(event, doneCallback, hoverCallback, mouseLeaveCallback, rest) {
//     const map = event.target;
//     const coords = rest.geolocation.coords;
//     const { longitude, latitude, accuracy } = coords;

//     //#region Source
//     map.addSource('selected-report', {
//         type: 'geojson',
//         data: point([0, 0])
//     });

//     map.addSource('userposition', {
//         type: 'geojson',
//         data: point([longitude, latitude])
//     });

//     map.addSource('point-like-items', {
//         type: 'geojson',
//         data: featureCollection([])
//     });

//     map.addSource('selected-emcomm', {
//         type: 'geojson',
//         data: featureCollection([])
//     });

//     map.addSource('selected-event', {
//         type: 'geojson',
//         data: featureCollection([])
//     });

//     map.addSource('active-event', {
//         type: 'geojson',
//         data: featureCollection([])
//     });

//     //#endregion

//     map.addLayer({
//         id: 'position-marker-accuracy',
//         type: 'circle',
//         source: 'userposition',
//         paint: {
//             ...variableCircleInMeters(accuracy, latitude),
//             'circle-opacity': 0.25
//         }
//     });

//     map.addLayer({
//         id: 'position-marker',
//         type: 'circle',
//         source: 'userposition',
//         paint: DEFAULT_POSITION_CIRCLE_PAINT
//     });

//     //#region Event
//     map.addLayer({
//         id: 'selected-event-area',
//         type: 'fill',
//         source: 'selected-event',
//         layout: {
//             visibility: 'none'
//         },
//         paint: {
//             'fill-color': SELECTED_EVENT_LAYER_COLOR,
//             'fill-opacity': 0.2,
//             'fill-outline-color': SELECTED_EVENT_LAYER_COLOR
//         },
//         filter: ['==', '$type', 'Polygon']
//     });

//     map.addLayer({
//         id: 'selected-event-outline',
//         type: 'line',
//         source: 'selected-event',
//         layout: {
//             visibility: 'none',
//             'line-cap': 'round',
//             'line-join': 'round'
//         },
//         paint: {
//             'line-color': SELECTED_EVENT_LAYER_COLOR,
//             'line-dasharray': [0.2, 2],
//             'line-width': 2
//         },
//         filter: ['==', '$type', 'Polygon']
//     });

//     map.addLayer({
//         id: 'selected-event-center',
//         type: 'symbol',
//         source: 'selected-event',
//         layout: {
//             visibility: 'none',
//             'text-font': ['IcoMoon-Free Regular'],
//             'text-field': '\ue906',
//             'text-size': 28,
//             'text-allow-overlap': true
//         },
//         paint: {
//             'text-halo-width': 2,
//             'text-halo-color': '#999999',
//             'text-color': SELECTED_EVENT_LAYER_COLOR,
//         },
//         filter: ['==', '$type', 'Point']
//     });

//     map.addLayer({
//         id: 'selected-event-text',
//         type: 'symbol',
//         source: 'selected-event',
//         layout: {
//             visibility: 'none',
//             'text-font': ['Open Sans Regular'],
//             'text-field': 'E',
//             'text-size': 22,
//             'text-offset': [0, -0.25],
//             'text-allow-overlap': true
//         },
//         paint: {
//             'text-color': '#000000',
//         },
//         filter: ['==', '$type', 'Point']
//     });

//     map.addLayer({
//         id: 'active-event-area',
//         type: 'fill',
//         source: 'active-event',
//         layout: {
//             visibility: 'none'
//         },
//         paint: {
//             'fill-color': ACTIVE_EVENT_LAYER_COLOR,
//             'fill-opacity': 0.4,
//             'fill-outline-color': ACTIVE_EVENT_LAYER_COLOR
//         },
//         filter: ['==', '$type', 'Polygon']
//     });

//     map.addLayer({
//         id: 'active-event-outline',
//         type: 'line',
//         source: 'active-event',
//         layout: {
//             visibility: 'none',
//             'line-cap': 'round',
//             'line-join': 'round'
//         },
//         paint: {
//             'line-color': ACTIVE_EVENT_LAYER_COLOR,
//             'line-dasharray': [0.2, 2],
//             'line-width': 2
//         },
//         filter: ['==', '$type', 'Polygon']
//     });

//     map.addLayer({
//         id: 'active-event-center',
//         type: 'symbol',
//         source: 'active-event',
//         layout: {
//             visibility: 'none',
//             'text-font': ['IcoMoon-Free Regular'],
//             'text-field': '\ue906',
//             'text-size': 28,
//             'text-allow-overlap': true
//         },
//         paint: {
//             'text-halo-width': 2,
//             'text-halo-color': '#999999',
//             'text-color': ACTIVE_EVENT_LAYER_COLOR,
//         },
//         filter: ['==', '$type', 'Point']
//     });

//     map.addLayer({
//         id: 'active-event-text',
//         type: 'symbol',
//         source: 'active-event',
//         layout: {
//             visibility: 'none',
//             'text-font': ['Open Sans Regular'],
//             'text-field': 'E',
//             'text-size': 22,
//             'text-offset': [0, -0.25],
//             'text-allow-overlap': true
//         },
//         paint: {
//             'text-color': '#000000',
//         },
//         filter: ['==', '$type', 'Point']
//     });
//     //#endregion

//     //#region Emergency communications
//     map.addLayer({
//         id: 'selected-emcomm-area',
//         type: 'fill',
//         source: 'selected-emcomm',
//         layout: {
//             visibility: 'none'
//         },
//         paint: {
//             'fill-color': SELECTED_EMCOMM_LAYER_COLOR,
//             'fill-opacity': 0.2,
//             'fill-outline-color': SELECTED_EMCOMM_LAYER_COLOR
//         },
//         filter: ['==', '$type', 'Polygon']
//     });

//     map.addLayer({
//         id: 'selected-emcomm-outline',
//         type: 'line',
//         source: 'selected-emcomm',
//         layout: {
//             visibility: 'none',
//             'line-cap': 'round',
//             'line-join': 'round'
//         },
//         paint: {
//             'line-color': SELECTED_EMCOMM_LAYER_COLOR,
//             'line-dasharray': [0.2, 2],
//             'line-width': 2
//         },
//         filter: ['==', '$type', 'Polygon']
//     });

//     //#endregion

//     //#region Report Requests
//     map.addLayer({
//         id: 'selected-report-request-area',
//         type: 'fill',
//         source: 'selected-emcomm',
//         layout: {
//             visibility: 'none'
//         },
//         paint: {
//             'fill-color': SELECTED_REP_REQ_LAYER_COLOR,
//             'fill-opacity': 0.2,
//             'fill-outline-color': SELECTED_REP_REQ_LAYER_COLOR
//         },
//         filter: ['==', '$type', 'Polygon']
//     });

//     map.addLayer({
//         id: 'selected-report-request-outline',
//         type: 'line',
//         source: 'selected-emcomm',
//         layout: {
//             visibility: 'none',
//             'line-cap': 'round',
//             'line-join': 'round'
//         },
//         paint: {
//             'line-color': SELECTED_REP_REQ_LAYER_COLOR,
//             'line-dasharray': [0.2, 2],
//             'line-width': 2
//         },
//         filter: ['==', '$type', 'Polygon']
//     });

//     //#endregion

//     //#region Em Communication categories
//     for (const emcomCategoryName of EMCOMM_CATEGORIES) {
//         const layerName = `unclustered-emcomm-${emcomCategoryName}`;
//         const textLayerName = `unclustered-emcomm-text-${emcomCategoryName}`;
//         const layer = {
//             id: layerName,
//             source: 'point-like-items',
//             'type': 'symbol',
//             'layout': {
//                 'text-font': ['IcoMoon-Free Regular'],
//                 'text-field': '\ue906',
//                 'text-size': 25,
//                 'text-rotate': 180,
//                 'text-allow-overlap': true
//             },
//             paint: {
//                 'text-halo-width': 2,
//                 'text-halo-color': '#333333',
//                 'text-color': EMCOMM_COLORS[emcomCategoryName],
//             }
//         };
//         const textLayer = {
//             id: textLayerName,
//             type: 'symbol',
//             source: 'point-like-items',
//             layout: {
//                 'text-font': ['Open Sans Regular'],
//                 'text-field': emcomCategoryName[0].toUpperCase(),
//                 'text-size': 14,
//                 'text-offset': [0, 0.25],
//                 'text-allow-overlap': true
//             },
//             paint: {
//                 'text-color': EMCOMM_LETTER_COLORS[emcomCategoryName],
//             }
//         };

//         let filter = [];
//         if (emcomCategoryName !== 'ecommUncategorized') {
//             if (emcomCategoryName === 'alert') {
//                 filter = ['all', ['!has', 'alreadyVoted'], ['==', 'type', 'alert']];
//             }
//             if (emcomCategoryName === 'warning_bePrepared') {
//                 filter = ['all', ['!has', 'alreadyVoted'], ['==', 'type', 'warning'], ['==', 'level', 'bePrepared']];
//             }
//             if (emcomCategoryName === 'warning_actionRequired') {
//                 filter = ['all', ['!has', 'alreadyVoted'], ['==', 'type', 'warning'], ['==', 'level', 'actionRequired']];
//             }
//             if (emcomCategoryName === 'warning_dangerToLife') {
//                 filter = ['all', ['!has', 'alreadyVoted'], ['==', 'type', 'warning'], ['==', 'level', 'dangerToLife']];
//             }
//         } else {
//             filter = ['all', ['!has', 'alreadyVoted'],
//                 ['any', ['!in', 'type', 'alert', 'warning'],
//                     ['all', ['==', 'type', 'warning'], ['!in', 'level', 'bePrepared', 'actionRequired', 'dangerToLife']]
//                 ]
//             ];
//         }
//         layer['filter'] = filter;
//         textLayer['filter'] = filter;

//         map.addLayer(layer);
//         map.addLayer(textLayer);
//         attachCursorEvents(map, layerName);
//         attachCursorEvents(map, textLayerName);
//     }

//     map.addLayer({
//         id: 'selected-emcomm-center',
//         type: 'symbol',
//         source: 'selected-emcomm',
//         layout: {
//             visibility: 'none',
//             'text-font': ['IcoMoon-Free Regular'],
//             'text-field': '\ue906',
//             'text-size': 28,
//             'text-rotate': 180,
//             'text-allow-overlap': true
//         },
//         paint: {
//             'text-halo-width': 2,
//             'text-halo-color': '#999999',
//             'text-color': SELECTED_EMCOMM_LAYER_COLOR,
//         },
//         filter: ['==', '$type', 'Point']
//     });

//     map.addLayer({
//         id: 'selected-emcomm-text',
//         type: 'symbol',
//         source: 'selected-emcomm',
//         layout: {
//             visibility: 'none',
//             'text-font': ['Open Sans Regular'],
//             'text-field': '!',
//             'text-size': 22,
//             'text-offset': [0, 0.25],
//             'text-allow-overlap': true
//         },
//         paint: {
//             'text-color': '#000000',
//         },
//         filter: ['==', '$type', 'Point']
//     });
//     //#endregion

//     //#region Report Request categories
//     for (const repReqCategoryName of REPORT_REQUEST_CATEGORIES) {
//         const layerName = `unclustered-emcomm-${repReqCategoryName}`;
//         const textLayerName = `unclustered-emcomm-text-${repReqCategoryName}`;
//         const layer = {
//             id: layerName,
//             source: 'point-like-items',
//             'type': 'symbol',
//             'layout': {
//                 'text-font': ['IcoMoon-Free Regular'],
//                 'text-field': '\ue906',
//                 'text-size': 25,
//                 'text-rotate': 180,
//                 'text-allow-overlap': true
//             },
//             paint: {
//                 'text-halo-width': 2,
//                 'text-halo-color': '#333333',
//                 'text-color': REPORT_REQUEST_COLORS[repReqCategoryName],
//             }
//         };

//         const textLayer = {
//             id: textLayerName,
//             type: 'symbol',
//             source: 'point-like-items',
//             layout: {
//                 'text-font': ['Open Sans Regular'],
//                 'text-field': repReqCategoryName[0].toUpperCase(),
//                 'text-size': 14,
//                 'text-offset': [0, 0.25],
//                 'text-allow-overlap': true
//             },
//             paint: {
//                 'text-color': REPORT_REQUEST_LETTER_COLORS[repReqCategoryName],
//             }
//         };
//         let filter = [];
//         if (repReqCategoryName !== 'ecommUncategorized') {
//             if (repReqCategoryName === 'reportRequest') {
//                 filter = ['all', ['!has', 'alreadyVoted'], ['==', 'type', 'reportRequest']];
//             }
//         }

//         layer['filter'] = filter;
//         textLayer['filter'] = filter;

//         map.addLayer(layer);
//         map.addLayer(textLayer);
//         attachCursorEvents(map, layerName);
//         attachCursorEvents(map, textLayerName);
//     }

//     map.addLayer({
//         id: 'selected-report-request-center',
//         type: 'symbol',
//         source: 'selected-emcomm',
//         layout: {
//             visibility: 'none',
//             'text-font': ['IcoMoon-Free Regular'],
//             'text-field': '\ue906',
//             'text-size': 28,
//             'text-rotate': 180,
//             'text-allow-overlap': true
//         },
//         paint: {
//             'text-halo-width': 2,
//             'text-halo-color': '#999999',
//             'text-color': SELECTED_REP_REQ_LAYER_COLOR,
//         },
//         filter: ['==', '$type', 'Point']
//     });

//     map.addLayer({
//         id: 'selected-report-request-text',
//         type: 'symbol',
//         source: 'selected-emcomm',
//         layout: {
//             visibility: 'none',
//             'text-font': ['Open Sans Regular'],
//             'text-field': '!',
//             'text-size': 22,
//             'text-offset': [0, 0.25],
//             'text-allow-overlap': true
//         },
//         paint: {
//             'text-color': '#000000',
//         },
//         filter: ['==', '$type', 'Point']
//     });
//     //#endregion

//     //#region Report
//     map.addLayer({
//         id: 'selected-report',
//         type: 'circle',
//         source: 'selected-report',
//         layout: {
//             visibility: 'none'
//         },
//         paint: {
//             'circle-stroke-width': 2,
//             'circle-radius': 40, //pixels
//             'circle-blur': 0.15,
//             'circle-opacity': 0.25,
//             'circle-pitch-scale': 'map',
//             'circle-color': '#000000',
//             'circle-stroke-color': '#000000'
//         }
//     });

//     for (const reportCategoryName of REPORT_CATEGORIES) {
//         const layerName = `unclustered-report-${reportCategoryName}`;
//         const layer = {
//             id: layerName,
//             source: 'point-like-items',
//             'type': 'symbol',
//             'layout': {
//                 'text-font': ['IcoMoon-Free Regular'],
//                 'text-field': '\ue906',
//                 'text-size': 25,
//                 'text-allow-overlap': true
//             },
//             paint: {
//                 'text-halo-width': 2,
//                 'text-halo-color': '#999999',
//                 'text-color': REPORT_COLORS[reportCategoryName],
//             }
//         };
//         let filter = [];
//         if (reportCategoryName !== 'reportUncategorized') {
//             filter = ['all', ['has', 'alreadyVoted'], STATUS_FILTERS[`_is_status_${reportCategoryName}`]];
//         } else {
//             filter = ['all', ['has', 'alreadyVoted'], ['!in', 'status', ...REPORT_CATEGORIES]];
//         }
//         layer['filter'] = filter;
//         map.addLayer(layer);
//         attachCursorEvents(map, layerName);
//     }
//     //#endregion

//     map.addControl(new mapboxgl.ScaleControl({
//         maxWidth: 80,
//         unit: 'metric',
//         float: 'left'
//     }));

//     if (typeof doneCallback === 'function') {
//         doneCallback(map, getMapSettings(map));
//     }
// }
