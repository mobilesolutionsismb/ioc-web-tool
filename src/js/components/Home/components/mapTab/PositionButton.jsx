import React, { Component } from 'react';
import { FloatingActionButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { compose } from 'redux';
import { withMapZoom } from 'js/modules/map';
import { withGeolocation } from 'ioc-geolocation';
import STYLES from './styles';

const enhance = compose(
  withMapZoom,
  withGeolocation
);

class PositionButton extends Component {
  onUpdatePositionClick = () => {
    this.props
      .updatePosition()
      .then(position => {
        let coords = this.props.geolocationPosition.coords;
        console.log('coords', [coords.longitude, coords.latitude]);
        this.props.zoomToFeature([coords.longitude, coords.latitude]);
      })
      .catch(e => {
        console.error('Cannot update position', e);
      });
  };

  render() {
    return (
      <div className="map-controls" style={{ ...STYLES.controls, ...STYLES.positionButton }}>
        <FloatingActionButton
          disabled={this.props.geolocationUpdating}
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          secondary={true}
          onClick={this.onUpdatePositionClick}
        >
          <FontIcon className="material-icons">my_location</FontIcon>
        </FloatingActionButton>
      </div>
    );
  }
}

export default enhance(muiThemeable()(PositionButton));
