import React, { Component } from 'react';
import { FloatingActionButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { withGeolocation } from 'ioc-geolocation';
import STYLES from './styles';

class ZoomButtons extends Component {
  onZoomPlusClick = () => {
    const map = this.props.map;
    if (map) {
      const zoom = map.getZoom();
      const maxZoom = map.getMaxZoom();
      const nextZoom = Math.round(zoom) + 1;
      if (nextZoom < maxZoom) {
        map.easeTo({ zoom: nextZoom });
      }
    }
  };

  onZoomMinusClick = () => {
    const map = this.props.map;

    if (map) {
      const zoom = map.getZoom();
      const minZoom = map.getMinZoom();
      const nextZoom = Math.round(zoom) - 1;
      if (nextZoom > minZoom) {
        map.easeTo({ zoom: nextZoom });
      }
    }
  };

  render() {
    return (
      <div className="map-controls" style={{ ...STYLES.controls, ...STYLES.zoomButton }}>
        <FloatingActionButton
          style={{ marginBottom: 2 }}
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          secondary={true}
          onClick={this.onZoomPlusClick}
        >
          <FontIcon className="material-icons">add</FontIcon>
        </FloatingActionButton>
        <FloatingActionButton
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          secondary={true}
          onClick={this.onZoomMinusClick}
        >
          <FontIcon className="material-icons">remove</FontIcon>
        </FloatingActionButton>
      </div>
    );
  }
}
export default withGeolocation(muiThemeable()(ZoomButtons));
