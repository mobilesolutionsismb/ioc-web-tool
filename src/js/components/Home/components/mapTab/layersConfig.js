import {
  VALIDATED_COLOR,
  NON_VALIDATED_COLOR,
  REJECTED_COLOR,
  EMCOMM_COLOR_REPORT_REQUEST,
  EMCOMM_COLOR_DANGER_TO_LIFE,
  EMCOMM_COLOR_ACTION_REQUIRED,
  EMCOMM_COLOR_BE_PREPARED
} from 'js/startup/iReactTheme';

import { white, black, brown900, brown400 } from 'material-ui/styles/colors';

export const REPORT_COLORS = {
  validated: VALIDATED_COLOR,
  submitted: NON_VALIDATED_COLOR,
  inaccurate: REJECTED_COLOR,
  inappropriate: REJECTED_COLOR,
  reportUncategorized: brown400 // light brown like dry s...
};

export const REPORT_LETTER_COLORS = {
  validated: white,
  submitted: white,
  inaccurate: white,
  inappropriate: white,
  reportUncategorized: white // light brown like dry s...
};

export const EMCOMM_COLORS = {
  warning_bePrepared: EMCOMM_COLOR_BE_PREPARED,
  warning_actionRequired: EMCOMM_COLOR_ACTION_REQUIRED,
  warning_dangerToLife: EMCOMM_COLOR_DANGER_TO_LIFE,
  alert: EMCOMM_COLOR_DANGER_TO_LIFE,
  //reportRequest: EMCOMM_COLOR_REPORT_REQUEST,
  ecommUncategorized: brown900 // dark brown like fresh s...
};

// Text colors which contrast emcomm colors adequately
export const EMCOMM_LETTER_COLORS = {
  warning_bePrepared: black,
  warning_actionRequired: white,
  warning_dangerToLife: white,
  alert: white,
  //reportRequest: white,
  ecommUncategorized: white // brown like s...
};

export const REPORT_REQUEST_COLORS = {
  reportRequest: EMCOMM_COLOR_REPORT_REQUEST
};

export const REPORT_REQUEST_LETTER_COLORS = {
  reportRequest: white
};

export const REPORT_CATEGORIES = Object.keys(REPORT_COLORS);
export const EMCOMM_CATEGORIES = Object.keys(EMCOMM_COLORS);
export const REPORT_REQUEST_CATEGORIES = Object.keys(REPORT_REQUEST_COLORS);

//CLUSTER MARKERS

const pi2 = Math.PI * 2;
export const ICON_SIZE = {
  x: 48,
  y: 48
};

const DATA_CATEGORIES = [...REPORT_CATEGORIES, ...EMCOMM_CATEGORIES, ...REPORT_REQUEST_CATEGORIES];
const DATA_COLORS = { ...REPORT_COLORS, ...EMCOMM_COLORS, ...REPORT_REQUEST_COLORS };
const DEFAULT_COLOR = brown900;

export function drawMarkerBalls(canvas, width, height, stats, population) {
  // let lol = 0;
  // let start = 0;
  let i = 0;
  for (let category of DATA_CATEGORIES) {
    i++;
    let size = stats[category] / population;
    if (size > 0) {
      canvas.beginPath();
      let angle = Math.PI / 4 * i;
      let posx = Math.cos(angle) * 18,
        posy = Math.sin(angle) * 18;
      //let xa = 0, xb = 1;
      let ya = 4,
        yb = 8;
      // let r = ya + (size - xa) * ((yb - ya) / (xb - xa));
      let r = ya + size * (yb - ya);
      //canvas.moveTo(posx, posy);
      canvas.arc(24 + posx, 24 + posy, r, 0, pi2);
      canvas.fillStyle = DATA_COLORS[category] || DEFAULT_COLOR;
      canvas.fill();
      canvas.closePath();
    }
  }
  canvas.beginPath();
  canvas.fillStyle = 'white';
  canvas.arc(24, 24, 16, 0, Math.PI * 2);
  canvas.fill();
  canvas.closePath();
  canvas.fillStyle = '#555';
  canvas.textAlign = 'center';
  canvas.textBaseline = 'middle';
  canvas.font = 'bold 12px sans-serif';
  canvas.fillText(population, 24, 24, 48);
}

export function drawMarkerBorders(canvas, width, height, stats, population) {
  let start = 0;
  for (let category of DATA_CATEGORIES) {
    let size = stats[category] / population;

    if (size > 0) {
      canvas.beginPath();
      canvas.moveTo(22, 22);
      canvas.fillStyle = DATA_COLORS[category] || DEFAULT_COLOR;
      let from = start + 0.14,
        to = start + size * pi2;
      if (to < from) {
        from = start;
      }
      canvas.arc(22, 22, 22, from, to);
      start += size * pi2;
      canvas.lineTo(22, 22);
      canvas.fill();
      canvas.closePath();
    }
  }
  canvas.beginPath();
  canvas.fillStyle = 'white';
  canvas.arc(22, 22, 18, 0, Math.PI * 2);
  canvas.fill();
  canvas.closePath();
  canvas.fillStyle = '#555';
  canvas.textAlign = 'center';
  canvas.textBaseline = 'middle';
  canvas.font = 'bold 12px sans-serif';
  canvas.fillText(population, 22, 22, 40);
}
