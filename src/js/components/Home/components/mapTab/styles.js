export const STYLES = {
  tab: {
    position: 'relative',
    width: '100%',
    height: '100%',
    overflow: 'hidden'
  },
  wrapper: {
    position: 'absolute',
    width: '100vw',
    height: 'calc(100vh - 112px)'
  },
  controls: {
    position: 'absolute',
    display: 'block',
    zIndex: 99
  },
  positionButton: {
    bottom: 110,
    right: 8
  },
  zoomButton: {
    bottom: 20,
    right: 8,
    display: 'flex',
    flexDirection: 'column'
  },
  // newReportButton: {
  //     bottom: 96,
  //     right: 8
  // },
  googleSearchBox: {
    top: 96,
    left: 88
  },
  compass: {
    top: 16,
    right: 8
  },
  notificationsButton: {
    top: 16,
    left: 8
  },
  mapSettingsButton: {
    top: 152,
    left: 88
  },
  searchIcon: {
    width: 36,
    height: 36,
    position: 'absolute',
    right: 0
  }
};

export default STYLES;
