import React, { Component } from 'react';
import { IconButton, FontIcon, Toolbar, ToolbarGroup, ToolbarTitle, Paper } from 'material-ui';
import { compose } from 'redux';
import axios from 'axios';

import Draggable from 'react-draggable';

import { withDictionary } from 'ioc-localization';
import { withLegendsWidget } from 'js/modules/LegendsWidget';
import { withLegendSelected } from 'js/modules/LegendsWidget';

import { withIReactTasks } from 'js/modules/LayersWidget';
import { withMapLayers } from 'ioc-api-interface';

const enhance = compose(
  withDictionary,
  withLegendsWidget,
  withIReactTasks,
  withMapLayers,
  withLegendSelected
);

class LegendsFrame extends Component {
  state = {
    legends: []
  };

  setLegend = b64images => {
    this.setState(prevState => ({
      legends: Array.isArray(b64images) ? b64images : []
    }));
  };

  _updateLegend = async () => {
    try {
      let imgs = [];
      // IF SET, GEOSERVER_URL takes prececedence over api GEOSERVER_URL
      if (Array.isArray(this.props.legendSelected.iReactTasks)) {
        //console.log(this.props.mapLayers.layers);
        // WHY loop? this is a bit complex and should be semplified and only accept layerName as string
        for (const task of this.props.legendSelected.iReactTasks) {
          const cfg = this.props.mapLayers.layers.find(layerCfg => layerCfg.taskId === task);
          const img = await this.getLegendGraphic(
            cfg.name,
            GEOSERVER_URL ? GEOSERVER_URL : cfg.url || this.props.mapLayers.baseUrl
          );
          imgs.push(img);
        }
      } else if (typeof this.props.legendSelected.name === 'string') {
        // short version with layerName config passed directly (e.g. from time slider)
        const cfg = this.props.legendSelected;
        const selectedLegendNames = cfg.name.split(',');
        for (let name of selectedLegendNames) {
          const img = await this.getLegendGraphic(
            name,
            GEOSERVER_URL ? GEOSERVER_URL : cfg.url || this.props.mapLayers.baseUrl
          );
          imgs.push(img);
        }
      }
      if (imgs.length) {
        this.setLegend(imgs);
      }
    } catch (err) {
      console.error('Error fetching legend', err);
      throw err;
    }
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.legendSelected !== prevProps.legendSelected &&
      this.props.legendSelected !== null
    ) {
      this._updateLegend();
    }
  }

  getLegendGraphic(
    layer,
    url /* = `${GEOSERVER_URL}/geoserver` */,
    format = 'image/png',
    WIDTH = 15,
    HEIGHT = 15,
    language = this.props.locale || 'en'
  ) {
    const params = {
      request: 'GetLegendGraphic',
      format,
      layer,
      exceptions: 'application/json',
      language,
      WIDTH,
      HEIGHT,
      legend_options: 'fontColor:ffffff;bgColor:0x303030;fontSize:14;forceLabels:on'
    };

    return axios({
      method: 'get',
      url: `${url.replace(/\/$/, '')}/wms`,
      params,
      responseType: 'arraybuffer'
    }).then(response => {
      let image = btoa(
        new Uint8Array(response.data).reduce((data, byte) => data + String.fromCharCode(byte), '')
      );
      return `data:${response.headers['content-type'].toLowerCase()};base64,${image}`;
    });
  }

  render() {
    let { dictionary } = this.props;

    const header = (
      <Toolbar>
        <ToolbarGroup>
          <ToolbarTitle style={{ color: 'white' }} text={dictionary._legends} />
          <div style={{ textAlign: 'right' }}>
            <IconButton
              onClick={() => {
                this.props.setOpenLegendsWidget(false);
              }}
            >
              <FontIcon className="material-icons">close</FontIcon>
            </IconButton>
          </div>
        </ToolbarGroup>
      </Toolbar>
    );

    return (
      <Draggable
        bounds=".map.tab"
        handle=".handle"
        defaultPosition={{ x: 0, y: 0 }}
        grid={[25, 25]}
      >
        <Paper
          zDepth={1}
          className="legendWidget"
          style={
            this.props.isLegendsWidgetOpen
              ? {
                  minHeight: '200px',
                  maxHeight: '550px',
                  maxWidth: '500px'
                }
              : {
                  height: '0',
                  width: '0',
                  opacity: '0',
                  zIndex: '-1'
                }
          }
        >
          <div children={header} className="handle" />
          <div
            style={{
              padding: '0 10px 10px 10px',
              maxHeight: 'calc(550px - 56px)',
              overflowY: 'auto',
              boxSizing: 'border-box'
            }}
          >
            <h4>{this.props.legendSelected ? this.props.legendSelected.label : ''}</h4>
            {this.state.legends.map((legend, index) => (
              <img key={index} src={legend} alt="Legend" />
            ))}
          </div>
        </Paper>
      </Draggable>
    );
  }
}

export default enhance(LegendsFrame);
