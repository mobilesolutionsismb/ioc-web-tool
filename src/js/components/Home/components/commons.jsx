import React from 'react';
import { FontIcon, Paper } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import nop from 'nop';

const CIRCLE_DIAMETER = 60;

export const STYLES = {
  icons: {
    color: 'lightgray'
  },
  bigCentralIcon: {
    color: 'black'
  },
  circularButton: {
    height: CIRCLE_DIAMETER,
    width: CIRCLE_DIAMETER,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center'
  },
  circularButtonLabel: {
    height: 20,
    paddingTop: 8,
    //width: CIRCLE_DIAMETER,
    textAlign: 'center',
    textTransform: 'capitalize',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    maxWidth: 160
  },
  circularButtonLetter: {
    height: CIRCLE_DIAMETER / 2,
    width: CIRCLE_DIAMETER / 2,
    padding: 15,
    fontSize: `${CIRCLE_DIAMETER / 2}px`,
    lineHeight: `${CIRCLE_DIAMETER / 2}px`,
    textAlign: 'center',
    color: 'rgba(0,0,0,1)',
    opacity: 1
  },
  circularButtonLetterDisabled: {
    borderRadius: '50%',
    backgroundColor: '#666',
    opacity: 0.87
  },
  circularButtonWrapper: {
    height: CIRCLE_DIAMETER + 28,
    width: CIRCLE_DIAMETER + 28,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '0 24px'
  }
};

export function addressLookUp(position) {
  //Should call a real geocoding service
  return 'Via Pier Carlo Boggio 61, 10138, Torino, Piemonte, Italia';
}

export const getIconStyle = props => ({
  ...STYLES.icons,
  ...(props.style || {})
});

export const DateIcon = props => (
  <FontIcon style={getIconStyle(props)} className="material-icons">
    access_time
  </FontIcon>
);
export const PlaceIcon = props => (
  <FontIcon style={getIconStyle(props)} className="material-icons">
    place
  </FontIcon>
);
export const InfoIcon = props => (
  <FontIcon style={getIconStyle(props)} className="material-icons">
    info
  </FontIcon>
);

class _CircularButton extends React.Component {
  render() {
    const props = this.props;
    const palette = this.props.muiTheme.palette;
    const disabled = props.disabled === true;
    const className = disabled ? 'circular-btn-icon disabled' : 'circular-btn-icon';
    return (
      <div
        className="circular-btn"
        style={{
          ...STYLES.circularButtonWrapper,
          ...props.wrapperStyle,
          opacity: disabled ? 0.5 : 1
        }}
      >
        <Paper
          className={className}
          style={{
            ...STYLES.circularButton,
            backgroundColor: palette.accent2Color,
            ...props.style
          }}
          zDepth={1}
          circle={true}
          onClick={disabled ? nop : props.onClick}
        >
          <span
            style={{
              ...STYLES.circularButtonLetter,
              color: palette.textColor,
              ...(disabled ? STYLES.circularButtonLetterDisabled : {})
            }}
          >
            {props.letter || 'L'}
          </span>
        </Paper>
        <span style={STYLES.circularButtonLabel} className="circular-btn-label">
          {props.label || 'label'}
        </span>
      </div>
    );
  }
}

export const CircularButton = muiThemeable()(_CircularButton);

const FACTOR = 1.75;

export const CircularPaper = muiThemeable()(props => (
  <div
    style={{
      ...STYLES.circularButtonWrapper,
      height: FACTOR * (CIRCLE_DIAMETER + 28),
      width: FACTOR * (CIRCLE_DIAMETER + 28),
      ...props.wrapperStyle
    }}
  >
    <Paper
      className={props.disabled ? 'circular-btn-icon big disabled' : 'circular-btn-icon big'}
      style={{
        ...STYLES.circularButton,
        height: CIRCLE_DIAMETER * FACTOR,
        width: CIRCLE_DIAMETER * FACTOR,
        backgroundColor: props.muiTheme.palette.accent2Color,
        ...props.style
      }}
      zDepth={1}
      circle={true}
      onClick={props.disabled === true ? nop : props.onClick}
    >
      <span
        style={{
          ...STYLES.circularButtonLetter,
          color: props.muiTheme.palette.textColor,
          height: FACTOR * (CIRCLE_DIAMETER / 2),
          width: FACTOR * (CIRCLE_DIAMETER / 2),
          fontSize: `${FACTOR * (CIRCLE_DIAMETER / 2)}px`,
          lineHeight: `${FACTOR * (CIRCLE_DIAMETER / 2)}px`
        }}
      >
        {props.letter || 'L'}
      </span>
    </Paper>
    <span
      style={{
        ...STYLES.circularButtonLabel,
        color: props.muiTheme.palette.accent2Color,
        height: 20 * FACTOR,
        lineHeight: `${20 * FACTOR}px`,
        width: 'auto',
        textTransform: 'uppercase'
      }}
      className="circular-btn-label big"
    >
      {props.label || 'label'}
    </span>
  </div>
));

export const HAZARD_TYPES_LETTERS = {
  flood: '💧',
  fire: '🔥',
  extremeWeather: '🌀️',
  landslide: '⛰️',
  earthquake: '🌋',
  avalanches: '🌨️',
  unknown: '❔',

  env: '',
  met: '',
  other: '',
  transport: '',
  cbrne: '',
  geo: '',
  health: '',
  infra: '',
  null: ''
};

export const CAP_CATEGORY_LABELS = {
  _ecc_geo: 'geo',
  _ecc_met: 'met',
  _ecc_fire: 'fire',
  _ecc_health: 'health',
  _ecc_cbrne: 'cbrne',
  _ecc_other: 'other',
  _ecc_env: 'env',
  _ecc_infra: 'infra',
  _ecc_transport: 'transport',
  _ecc_null: 'null'
};

export const HAZARD_TYPES_LABELS = {
  _ech_earthquake: 'earthquake',
  _ech_tsunami: 'tsunami',
  _ech_rock_fall: 'rockFall',
  _ech_landslide: 'landslide',
  _ech_ash_fall: 'ashFall',
  _ech_lahar: 'lahar',
  _ech_pyroclastic_flow: 'pyroclasticFlow',
  _ech_lava_flow: 'lavaFlow',
  _ech_extreme_weather: 'extremeWeather',
  _ech_fog: 'fog',
  _ech_storm: 'storm',
  _ech_wave_action: 'waveAction',
  _ech_drought: 'drought',
  _ech_glacial_lake_outburst: 'glacialLakeOutburst',
  _ech_wildfire: 'wildfire',
  _ech_epidemic: 'epidemic',
  _ech_insect_infestation: 'insectInfestation',
  _ech_animal_accident: 'animalAccident',
  _ech_extraterrestrial_impact: 'extraterrestrialImpact',
  _ech_space_weather: 'spaceWeather',
  _ech_chemical_spill: 'chemicalSpill',
  _ech_industrial_collapse: 'industrialCollapse',
  _ech_industrial_explosion: 'industrialExplosion',
  _ech_industrial_fire: 'industrialFire',
  _ech_gas_leak: 'gasLeak',
  _ech_poisoning: 'poisoning',
  _ech_radiation: 'radiation',
  _ech_oil_spill: 'oilSpill',
  _ech_industrial_accident: 'industrialAccident',
  _ech_air_accident: 'airAccident',
  _ech_road_accident: 'roadAccident',
  _ech_traffic: 'traffic',
  _ech_rail_accident: 'railAccident',
  _ech_water_accident: 'waterAccident',
  _ech_collapse: 'collapse',
  _ech_explosion: 'explosion',
  _ech_fire: 'fire',
  _ech_other: 'other',
  _ech_extra_tropical_storm: 'extraTropicalStorm',
  _ech_tropical_storm: 'tropicalStorm',
  _ech_der_echo: 'derecho',
  _ech_hail: 'hail',
  _ech_thunderstorms: 'thunderstorms',
  _ech_rain: 'rain',
  _ech_tornado: 'tornado',
  _ech_dust_storm: 'dustStorm',
  _ech_blizzard: 'blizzard',
  _ech_storm_surge: 'stormSurge',
  _ech_wind: 'wind',
  _ech_cold_wave: 'coldWave',
  _ech_heat_wave: 'heatWave',
  _ech_snow: 'snow',
  _ech_ice: 'ice',
  _ech_frost: 'frost',
  _ech_coastal_event: 'coastalEvent',
  _ech_coastal_erosion: 'coastalErosion',
  _ech_flood: 'flood',
  _ech_rain_flood: 'rainFlood',
  _ech_coastal_flood: 'coastalFlood',
  _ech_riverine_flood: 'riverineFlood',
  _ech_flash_flood: 'flashFlood',
  _ech_ice_jam_flood: 'iceJamFlood',
  _ech_avalanches: 'avalanches',
  _ech_debris: 'debris',
  _ech_mudflow: 'mudflow',
  _ech_forest_fire: 'forestFire',
  _ech_land_fire: 'landFire',
  _ech_pasture_fire: 'pastureFire',
  _ech_viral_disease: 'viralDisease',
  _ech_bacterial_disease: 'bacterialDisease',
  _ech_parasitic_disease: 'parasiticDisease',
  _ech_fungal_disease: 'fungalDisease',
  _ech_prion_disease: 'prionDisease',
  _ech_grasshoper: 'grasshoper',
  _ech_locust: 'locust',
  _ech_airburst: 'airburst',
  _ech_energetic_particles: 'energeticParticles',
  _ech_geomagnetic_storm: 'geomagneticStorm',
  _ech_shockwave: 'shockwave',
  _ech_none: ''
};

export const REPORT_TYPES_LETTERS = {
  measure: '📐',
  damage: '🚧',
  resources: '⛑️'
};

export const EMERGENCY_EVENT_CODE_VALIDATION_ICONS = {
  unknown: '❓',
  suggested: '⁉️',
  validated: '✅',
  all: '🚩'
};

export const MISSION_STATUS = {
  inProgress: <FontIcon className="material-icons orange">fiber_manual_record</FontIcon>,
  aborted: <FontIcon className="material-icons plum">fiber_manual_record</FontIcon>,
  completed: <FontIcon className="material-icons green">fiber_manual_record</FontIcon>,
  expired: <FontIcon className="material-icons grey">fiber_manual_record</FontIcon>
};

export const EMERGENCY_EVENT_STATUS = {
  validated: <FontIcon className="material-icons green">fiber_manual_record</FontIcon>,
  suggested: <FontIcon className="material-icons orange">fiber_manual_record</FontIcon>
};
