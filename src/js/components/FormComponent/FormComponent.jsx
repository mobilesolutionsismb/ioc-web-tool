import React, { Component } from 'react';
import nop from 'nop';
import { Attire } from 'react-attire';
import { trim as trimBoth, ltrim, rtrim } from 'validator';
import { TextField, FontIcon, IconButton } from 'material-ui';
import isEqual from 'react-fast-compare';
import { validate, reduceValidationErrors } from 'js/utils/fieldValidation';
import { logSevereWarning, logError } from 'js/utils/log';
import { ErrorContainer, Container, StyledInputWrapper } from 'js/components/app/commons';
import { red800 } from 'material-ui/styles/colors';

const defaultActionStyle = { position: 'absolute', right: 0 };

const ActionIcon = ({ style, color, text = 'cancel', clearAction = { nop } }) => (
  <IconButton
    style={{ ...defaultActionStyle, ...style }}
    onClick={clearAction}
    disableTouchRipple={true}
    tabIndex={-1}
  >
    {text && (
      <FontIcon className="material-icons" color={color}>
        {text}
      </FontIcon>
    )}
    {text === null && <span style={{ width: 24, height: 24 }} />}
  </IconButton>
);

// const TEXT_TYPES = ['text', 'password', 'number', 'email', 'tel'];

/*
Example config

interface IFormConfg {
    name: string,
    inputType: string, // (all those supported by TextField but there is no enum)
    label: string,
    rules: [],
    trim? : boolean || string // will trim state input value
    defaultValue?: any
}

const config: IFormConfg[] = [
    {
        name: 'email',
        inputType: 'email',
        label: 'Email',
        rules: [emailRule],
        trim: true,
        defaultValue: ''
    },
    {
        name: 'password',
        inputType: 'password',
        label: 'Password',
        rules: [passwordRule],
        trim: true,
        defaultValue: ''
    }
]


*/

const defaultInputStyle = {
  width: '100%' //'calc(100% - 48px)'
};

export class InputField extends Component {
  static defaultProps = {
    clearAction: nop,
    input: null,
    dictionary: n => n
  };

  state = {
    showPassword: false
  };

  _onFocus = e => {
    const target = e.target;
    if (target) {
      if (IS_CORDOVA && cordova.platformId === 'ios') {
        target.scrollIntoViewIfNeeded();
      } else {
        target.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
      }
    }
  };

  _togglePasswordShow = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  render() {
    const {
      input,
      clearAction,
      type,
      value,
      errorText,
      onChange,
      dictionary,
      style,
      wrapperStyle,
      inputStyle,
      underlineStyle,
      actionStyle,
      ...rest
    } = this.props;
    const isTextInput = type !== 'custom' || type !== 'hidden';
    const isPassword = type === 'password';
    const hasClearButton = isTextInput && value.length > 0;
    const hasErrorText =
      isTextInput && typeof errorText === 'string'
        ? errorText.length > 0
        : Array.isArray(errorText) && errorText.length > 0
        ? errorText[0].length > 0
        : false;
    const actionText = hasErrorText
      ? 'error'
      : isPassword && value.length > 0
      ? this.state.showPassword === false
        ? 'visibility'
        : 'visibility_off'
      : hasClearButton
      ? 'cancel'
      : null;

    const inputProps = {
      type: isPassword ? (this.state.showPassword === true ? 'text' : type) : type,
      value,
      onChange,
      underlineStyle,
      errorText: hasErrorText
        ? dictionary(Array.isArray(errorText) ? errorText[0] : errorText)
        : undefined,
      style,
      inputStyle: { ...defaultInputStyle, ...inputStyle },
      ...rest
    };
    const Input = input ? input : TextField;

    return type === 'hidden' ? (
      <input type={type} value={value} />
    ) : (
      <StyledInputWrapper style={wrapperStyle}>
        {!isTextInput && <Input value={value} onChange={onChange} onFocus={this._onFocus} />}
        {isTextInput && (
          <Input className={`form-input ${type}`} onFocus={this._onFocus} {...inputProps} />
        )}
        {isTextInput && (
          <ActionIcon
            style={actionStyle}
            clearAction={isPassword && !hasErrorText ? this._togglePasswordShow : clearAction}
            disableTouchRipple={true}
            text={actionText}
            color={hasErrorText ? red800 : undefined}
          />
        )}
      </StyledInputWrapper>
    );
  }
}

/**
 * Wraps a CardContent and CardActions inside a form with attire and validation
 */
export class FormComponent extends Component {
  static defaultProps = {
    onSubmit: nop,
    config: {},
    column: true,
    formComponent: 'form',
    innerComponent: Container,
    FormComponentProperties: {
      autoComplete: 'puff',
      autoCorrect: 'off',
      autoCapitalize: 'off',
      spellCheck: false
    },
    actionComponent: 'div',
    actions: []
  };

  constructor(props) {
    super(props);
    this.state = {
      formError: null,
      validationStatus: this._computeState(props.config),
      initialValues: this._computeInitialValue(props.config)
    };
  }

  _resetError = () => {
    this.setState({ formError: true });
  };

  // Compute validationStatus based on config - each input name corresponds to a string
  _computeState = config => {
    return config.reduce((validState, cfg) => {
      validState[cfg.name] = [];
      return validState;
    }, {});
  };

  // Compute validationStatus based on config - each input name corresponds to a default value
  _computeInitialValue = config => {
    return config.reduce((vs, cfg) => {
      const isTextInput = cfg.type !== 'number' && cfg.type !== 'custom'; //TEXT_TYPES.includes(cfg.tye);
      vs[cfg.name] =
        typeof cfg.defaultValue === 'undefined'
          ? isTextInput
            ? ''
            : cfg.type === 'number'
            ? cfg.minValue || ''
            : undefined
          : cfg.defaultValue;

      return vs;
    }, {});
  };

  // Update validationStatus based on config
  _recomputeComponentState = config => {
    this.setState({
      validationStatus: this._computeState(config),
      initialValues: this._computeInitialValue(config)
    });
  };

  // Reset validation state
  _resetValidationState = () => {
    this.setState({ validationStatus: this._computeState(this.props.config) });
  };

  // Clear field by name
  _clearField = (field, onChange) => {
    onChange(field, '');
    this.setState(prevState => {
      let nextState = { ...prevState };
      nextState.validationStatus[field] = '';
      return nextState;
    });
  };

  _extractValidationRuleObject = config => {
    return config.reduce((rules, cfg) => {
      if (Array.isArray(cfg.rules)) {
        rules[cfg.name] = reduceValidationErrors.bind(this, cfg.rules);
      }
      return rules;
    }, {});
  };

  _validate = async (state, partial = false) => {
    try {
      const rules = this._extractValidationRuleObject(this.props.config);
      const _state =
        partial === true
          ? Object.entries(state).reduce((partState, next) => {
              const key = next[0];
              const val = next[1];
              const initial = this.props.config.find(c => c.name === key);
              if (initial) {
                const initialValue = initial.defaultValue;
                if (!isEqual(val, initialValue)) {
                  partState[key] = state[key];
                }
              }
              return partState;
            }, {})
          : state;
      const _rules =
        partial === true
          ? Object.entries(rules).reduce((partRules, next) => {
              const key = next[0];
              const rule = next[1];
              if (typeof _state[key] !== 'undefined') {
                partRules[key] = rule;
              }
              return partRules;
            }, {})
          : rules;

      await validate(_rules)(_state);
      return true;
    } catch (validationStatus) {
      logSevereWarning('Validation errors', validationStatus);
      this.setState({ validationStatus });
      return false;
    }
  };

  _onFormSubmit = async (state, e) => {
    e.preventDefault();
    const isValid = await this._validate(state);
    if (isValid) {
      this.props.onSubmit(state);
    }
  };

  _onFormChange = async (state, e) => {
    e.preventDefault();
    const value = e.target.value;
    const name = e.target.name;
    await this._validate({ ...state, [name]: value }, true);
  };

  componentDidUpdate(prevProps) {
    if (!isEqual(prevProps.config, this.props.config)) {
      this._recomputeComponentState(this.props.config);
    }
  }

  componentDidCatch(err) {
    logError('Form render error', err, this.props);
    this.setState({ formError: err });
  }

  _getErrorText = (validationStatusObject, dictionary, label) => {
    if (!validationStatusObject) {
      return '';
    }
    if (Array.isArray(validationStatusObject)) {
      if (validationStatusObject.length === 0) {
        return '';
      } else {
        const { text, min, max, len } = validationStatusObject[0];
        return typeof len === 'undefined'
          ? dictionary(text, label, min, max)
          : dictionary(text, label, len);
      }
    }
  };

  render() {
    const {
      className,
      config,
      column,
      formComponent,
      FormComponentProperties,
      innerComponent,
      actionComponent,
      actions,
      dictionary
    } = this.props;
    const WrapperComponent = formComponent;
    const InputContainer = innerComponent;
    const ActionWrapperComponent = actionComponent;
    const hasConfig = config.length > 0;

    const error = this.state.formError;
    if (error) {
      return (
        <ErrorContainer className="form-error" onClick={this._resetError}>
          <h3>Form Card Content Error</h3>
          <span role="img" aria-label="Crying">
            😭
          </span>
          <p>{error.message}</p>
          <p>{error.stack}</p>
        </ErrorContainer>
      );
    }

    if (!hasConfig) {
      return null;
    } else {
      const _getErrorText = this._getErrorText;
      const initialState = this.state.initialValues;
      return (
        <Attire initial={initialState} onChange={this._resetValidationState}>
          {(state, onChange, reset) => {
            const hasValidationErrors = Object.entries(this.state.validationStatus).reduce(
              (hasErrors, nextEntry) => {
                hasErrors = hasErrors || (Array.isArray(nextEntry[1]) && nextEntry[1].length > 0);
                return hasErrors;
              },
              false
            );

            return (
              <WrapperComponent
                {...FormComponentProperties}
                className={className}
                onSubmit={this._onFormSubmit.bind(this, state)}
                onChange={this._onFormChange.bind(this, state)}
              >
                <InputContainer column={column ? 'true' : 'false'}>
                  {config.map((inputConfg, i) => {
                    const {
                      name,
                      inputType,
                      label,
                      trim,
                      input,
                      defaultValue,
                      ...rest
                    } = inputConfg;
                    const trimFn =
                      trim === true || trim === 'all'
                        ? trimBoth
                        : trim === 'left'
                        ? ltrim
                        : trim === 'right'
                        ? rtrim
                        : null;
                    const value = typeof trimFn === 'function' ? trimFn(state[name]) : state[name];
                    const type = inputType || 'text';
                    const _label = dictionary(label);
                    const errorText = _getErrorText(
                      this.state.validationStatus[name],
                      dictionary,
                      _label
                    );
                    const key = `${type}-${name}-${i}`;
                    return (
                      <InputField
                        id={key}
                        key={key}
                        name={name}
                        type={type}
                        input={input}
                        value={value}
                        autoComplete="puff"
                        autoCorrect="off"
                        autoCapitalize="off"
                        spellCheck={false}
                        dictionary={dictionary}
                        floatingLabelText={_label}
                        onChange={evt => {
                          const val =
                            typeof trimFn === 'function'
                              ? trimFn(evt.target.value)
                              : evt.target.value;
                          onChange(evt.target.name, val);
                        }}
                        errorText={errorText}
                        clearAction={() => this._clearField(name, onChange)}
                        {...rest}
                      />
                    );
                  })}
                </InputContainer>
                <ActionWrapperComponent key="actions">
                  {actions(reset, this._resetValidationState, state, hasValidationErrors)}
                </ActionWrapperComponent>
              </WrapperComponent>
            );
          }}
        </Attire>
      );
    }
  }
}

export default FormComponent;
