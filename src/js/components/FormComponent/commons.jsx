import styled from 'styled-components';

export const StyledForm = styled.form`
  width: 100%;
  height: auto;
`;

export const FormContentWrapper = styled.div`
  width: 100%;
  height: 172px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  overflow-y: auto;
  overflow-x: hidden;
`;

export const StyledFormActions = styled.div`
  width: 100%;
  height: auto;
  height: 142px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 8px 0;
`;
