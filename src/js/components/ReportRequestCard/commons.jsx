import React from 'react';
// import { IconButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {
  WarningIcon,
  ResourceIcon,
  /* MeasureIcon, */ PeopleIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';

const ROW_STYLE = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center'
  // paddingTop: 10,
  // paddingBottom: 10
};

const DISABLED_STYLE = {
  color: 'grey'
};

const iconStyle = {
  fontSize: 18
};

const firstButtonStyle = {
  paddingLeft: 0,
  paddingRight: 10,
  width: 24,
  textAlign: 'left'
};

const otherButtonStyle = {
  marginRight: 10,
  marginLeft: 10,
  width: 24,
  textAlign: 'left'
};

export const ReportRequestMeasureLine = muiThemeable()(
  ({ damages = false, resources = false, people = false, measures = [], dictionary }) => (
    <div style={ROW_STYLE}>
      <div style={!damages ? DISABLED_STYLE : {}}>
        <WarningIcon style={firstButtonStyle} iconStyle={iconStyle} disabled={!damages} />
        DAM
      </div>
      <div style={!resources ? DISABLED_STYLE : {}}>
        <ResourceIcon style={otherButtonStyle} iconStyle={iconStyle} disabled={!resources} />
        RES
      </div>
      <div style={!people ? DISABLED_STYLE : {}}>
        <PeopleIcon style={otherButtonStyle} iconStyle={iconStyle} disabled={!people} />
        PEO
      </div>
      <div
        style={
          measures.length === 0
            ? { ...DISABLED_STYLE, marginLeft: 10, marginTop: 8 }
            : { marginLeft: 10, marginTop: 8 }
        }
      >
        <span style={otherButtonStyle}>{measures ? measures.length : 0} MEA</span>
        {/* <MeasureIcon style={firstButtonStyle} iconStyle={ iconStyle } disabled={measures.length === 0} /> */}
      </div>
    </div>
  )
);

export const ReportRequestColorDot = muiThemeable()(({ status, muiTheme }) => (
  <div
    className={`status-report-request role-${status}`}
    style={{
      background: status
        ? muiTheme.palette.ongoingReportRequestColor
        : muiTheme.palette.closedReportRequestColor
    }}
  />
));
