// import './passwordForgot.scss';
import React, { Component } from 'react';
import { withDictionary } from 'ioc-localization';
import { withRouter } from 'react-router-dom';

import PasswordForgotForm from './PasswordForgotForm';

class PasswordForgot extends Component {
  onSubmit = async props => {
    let response = await this.props.passwordForgot(props.email);
    console.log('%cPasswordForgot success', 'color: white; background: lime', response);
    this.props.pushMessage('_password_update_link_sent', 'default', null, props.email);
  };

  onBackButtonClick = e => {
    e.preventDefault();
    this.props.history.goBack();
  };

  render() {
    return (
      <div className="password-forgot page">
        <PasswordForgotForm
          onSubmit={this.onSubmit}
          onBackButtonClick={this.onBackButtonClick}
          dictionary={this.props.dictionary}
          locale={this.props.locale}
        />
      </div>
    );
  }
}

export default withRouter(withDictionary(PasswordForgot));
