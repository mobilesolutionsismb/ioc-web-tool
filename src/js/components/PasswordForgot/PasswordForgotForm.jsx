import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextField, RaisedButton, FlatButton } from 'material-ui';
import { withDictionary } from 'ioc-localization';

const style = {
  button: {
    margin: 8
  },
  fullWidth: {
    width: '100%',
    marginTop: 8,
    marginBottom: 8
  }
};

const renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => (
  <TextField
    hintText={label}
    fullWidth={true}
    floatingLabelText={label}
    errorText={touched && error}
    {...input}
    {...custom}
  />
);

const validate = (values, props) => {
  const errors = {};

  if (!values.email) {
    errors.email = props.dictionary._required;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = props.dictionary._email_error_text;
  } else if (values.email.length > 32) {
    errors.email = props.dictionary._email_error_text;
  }
};

const PasswordForgotForm = props => {
  const {
    pristine,
    submitting,
    /* reset, */ handleSubmit,
    onBackButtonClick,
    /* language, */ dictionary
  } = props;

  return (
    <form className="password-forgot-form" onSubmit={handleSubmit}>
      <div style={style.fullWidth}>
        <Field name="email" type="email" component={renderTextField} label={dictionary._email} />
      </div>
      <div style={style.fullWidth}>
        <RaisedButton
          type="submit"
          className="ui-button"
          disabled={pristine || submitting}
          label={dictionary._submit}
          primary={true}
          style={style.button}
        />
        <FlatButton className="ui-button" label={dictionary._back} onClick={onBackButtonClick} />
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'passwordForgotForm', // a unique identifier for this form
  validate
})(withDictionary(PasswordForgotForm));
