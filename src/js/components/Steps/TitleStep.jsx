import './style.scss';
import React, { Component } from 'react';
import { TextField } from 'material-ui';

class Title extends Component {
  render() {
    const stepTitleContent = (
      <TextField name="title" onChange={this.props.handleTitleChange} value={this.props.title} />
    );
    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'flex-start',
          paddingTop: '25px'
        }}
      >
        {stepTitleContent}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default Title;
