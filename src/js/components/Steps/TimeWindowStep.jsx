import './style.scss';
import React, { Component } from 'react';
import { TimePicker, DatePicker } from 'material-ui';
import moment from 'moment';

class TimeWindowStep extends Component {
  render() {
    const { dictionary, isDisabledStart, isDisabledEnd } = this.props;

    return (
      <div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: '20px'
          }}
        >
          <div style={{ flexGrow: 1, width: '10%' }}>{dictionary._start + '*'}</div>
          <DatePicker
            disabled={isDisabledStart}
            value={this.props.startDate ? this.props.startDate : moment()}
            id="startDate"
            textFieldStyle={{ width: '100px' }}
            style={{ flexGrow: 1 }}
            ref="startDate"
            onChange={this.props.handleStartChange}
          />
          <TimePicker
            disabled={isDisabledStart}
            value={this.props.startTime ? this.props.startTime : moment()}
            id="startTime"
            textFieldStyle={{ width: '100px' }}
            style={{ flexGrow: 1 }}
            ref="startTime"
            onChange={this.props.handleStartTimeChange}
          />
        </div>
        <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
          <div style={{ flexGrow: 1, width: '10%' }}>{dictionary._end}</div>
          <DatePicker
            disabled={isDisabledEnd}
            value={this.props.endDate ? this.props.endDate : moment()}
            id="endDate"
            textFieldStyle={{ width: '100px' }}
            style={{ flexGrow: 1 }}
            ref="endDate"
            onChange={this.props.handleEndChange}
          />
          <TimePicker
            disabled={isDisabledEnd}
            value={this.props.endTime ? this.props.endTime : moment()}
            id="endTime"
            textFieldStyle={{ width: '100px' }}
            style={{ flexGrow: 1 }}
            ref="endTime"
            onChange={this.props.handleEndTimeChange}
          />
        </div>
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default TimeWindowStep;
