import './style.scss';
import React, { Component } from 'react';
import { ListItem, Checkbox, List, Chip, FlatButton, Avatar } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { CONTENT_TYPE } from 'js/modules/reportNewRequest';
import { MeasureCategoryIcon } from 'js/components/ReportsCommons/commons';

class ContentTypeStep extends Component {
  onRequestDelete = (measures, itemToRemove) => event => {
    this.props.handleContentTypeMeasureChange(
      measures.filter(measure => measure.value !== itemToRemove.value)
    );
  };

  onRequestDeleteCategoryType = categoryType => event => {
    this.props.handleContentTypeCategoryRemove();
  };

  handleCustomDisplaySelections = name => measures =>
    measures.length > 0 ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        {measures.map(
          (item, index) =>
            this.props.isDisabled ? (
              <Chip key={index} style={{ margin: 5 }}>
                {this.props.dictionary[item.label]}
              </Chip>
            ) : (
              <Chip
                key={index}
                style={{ margin: 5 }}
                onRequestDelete={this.onRequestDelete(measures, item)}
              >
                {this.props.dictionary[item.label]}
              </Chip>
            )
        )}
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select some measures</div>
    );

  handleCustomDisplaySelectionsCategories = categoryType => categoryType =>
    categoryType && categoryType.value !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap', textTransform: 'lowercase' }}>
        {this.props.isDisabled ? (
          <Chip style={{ margin: 5 }}>
            <Avatar size={30}>
              <MeasureCategoryIcon category={categoryType.value} />
            </Avatar>
            <span>{categoryType.label}</span>
          </Chip>
        ) : (
          <Chip
            style={{ margin: 5 }}
            onRequestDelete={this.onRequestDeleteCategoryType(categoryType)}
          >
            <Avatar size={30}>
              <MeasureCategoryIcon category={categoryType.value} />
            </Avatar>
            <span>{categoryType.label}</span>
          </Chip>
        )}
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select a Category</div>
    );

  render() {
    const { dictionary, measureList, categoryList } = this.props;

    const measureListNodes = measureList.map((measure, index) => {
      return (
        <div key={index} value={measure.value} label={dictionary(measure.title)}>
          <span>{dictionary(measure.title)}</span>
        </div>
      );
    });

    const categoryListNodes = categoryList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={category.name}>
          <span style={{ textTransform: 'lowercase' }}>{category.name}</span>
        </div>
      );
    });

    const superSelectMenuCloseButton = (
      <FlatButton label={dictionary._close} backgroundColor={'red'} />
    );

    const superSelCategories = (
      <div key={0} style={{ display: 'flex', alignItems: 'flex-end', marginTop: 10 }}>
        {this.props.contentType.filter(e => e === CONTENT_TYPE.measure).length > 0 && (
          <SuperSelectField
            disabled={this.props.isDisabled}
            style={{ width: '90%' }}
            name="selectContentType"
            floatingLabel="Select a category"
            onChange={this.props.handleContentTypeCategoryChange}
            value={this.props.categoryType}
            selectionsRenderer={this.handleCustomDisplaySelectionsCategories(
              this.props.categoryType
            )}
          >
            {categoryListNodes}
          </SuperSelectField>
        )}
      </div>
    );

    const superSelMeasures = (
      <div key={0} style={{ display: 'flex', alignItems: 'flex-end' }}>
        {this.props.contentType.filter(e => e === CONTENT_TYPE.measure).length > 0 &&
          this.props.categoryType &&
          this.props.categoryType.value !== '' && (
            <SuperSelectField
              disabled={this.props.isDisabled}
              style={{ width: '90%' }}
              name="selectMeasure"
              multiple={true}
              floatingLabel="Select measures"
              onChange={this.props.handleContentTypeMeasureChange}
              value={this.props.measures}
              selectionsRenderer={this.handleCustomDisplaySelections(this.props.measures)}
              menuCloseButton={superSelectMenuCloseButton}
            >
              {measureListNodes}
            </SuperSelectField>
          )}
      </div>
    );
    return (
      <div>
        <div>
          <List style={{ whiteSpace: 'noWrap' }}>
            <ListItem
              key={0}
              disabled={true}
              leftCheckbox={
                <Checkbox
                  key={0}
                  disabled={this.props.isDisabled}
                  checked={this.props.contentType.filter(e => e === CONTENT_TYPE.damage).length > 0}
                  value={CONTENT_TYPE.damage}
                  label={dictionary._damages}
                  onClick={this.props.handleContentTypeChange}
                />
              }
            />
            <ListItem
              key={1}
              disabled={true}
              leftCheckbox={
                <Checkbox
                  key={1}
                  disabled={this.props.isDisabled}
                  checked={
                    this.props.contentType.filter(e => e === CONTENT_TYPE.resource).length > 0
                  }
                  value={CONTENT_TYPE.resource}
                  label={dictionary._resources}
                  onClick={this.props.handleContentTypeChange}
                />
              }
            />
            <ListItem
              key={2}
              disabled={true}
              leftCheckbox={
                <Checkbox
                  key={2}
                  disabled={this.props.isDisabled}
                  checked={this.props.contentType.filter(e => e === CONTENT_TYPE.people).length > 0}
                  value={CONTENT_TYPE.people}
                  label={dictionary._people}
                  onClick={this.props.handleContentTypeChange}
                />
              }
            />
            <ListItem
              key={3}
              disabled={true}
              leftCheckbox={
                <Checkbox
                  key={3}
                  disabled={this.props.isDisabled}
                  checked={
                    this.props.contentType.filter(e => e === CONTENT_TYPE.measure).length > 0
                  }
                  value={CONTENT_TYPE.measure}
                  label={dictionary._measures}
                  onClick={this.props.handleContentTypeChange}
                />
              }
            />
            {this.props.showCategories && (
              <ListItem key={4} disabled={true} children={superSelCategories} />
            )}
            {this.props.showMeasures && (
              <ListItem key={5} disabled={true} children={superSelMeasures} />
            )}
          </List>
        </div>
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default ContentTypeStep;
