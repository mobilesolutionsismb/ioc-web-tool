import './style.scss';
import React, { Component } from 'react';
import { Chip, FlatButton } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { white } from 'material-ui/styles/colors';
import { InputAOIStep } from 'js/components/Steps';

class EmCommReceiversStep extends Component {
  handleCustomDisplaySelectionsRole = roles => roles =>
    roles.length > 0 ? (
      roles.map((item, i) => (
        <div key={i} style={{ display: 'flex', flexWrap: 'wrap' }}>
          <Chip
            key={i}
            style={{ margin: 5 }}
            deleteIconStyle={{ color: 'black', fill: 'black' }}
            backgroundColor={white}
            labelColor="black"
            onRequestDelete={this.onRequestDeleteRole(item)}
          >
            <span>{item.label}</span>
          </Chip>
        </div>
      ))
    ) : (
      <div key={0} style={{ minHeight: 42, lineHeight: '42px' }}>
        Select a Role
      </div>
    );

  onRequestDeleteRole = itemToRemove => event => {
    this.props.removeEmCommReceiver(itemToRemove.value);
  };

  updateRoles = (selectedItems, args) => {
    this.props.updateEmCommReceivers(selectedItems);
  };

  render() {
    const {
      dictionary,
      getMapDraw,
      isDisabled,
      category,
      address,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory,
      resetHomeDrawState,
      drawPoint,
      drawPolygon,
      resetCategory,
      roles,
      receivers
    } = this.props;

    const roleList = roles.map(item => {
      let role = {
        value: item.id,
        label: item.displayName
      };
      return role;
    });

    const receiverListNodes = roleList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={category.label}>
          <span key={index}>{category.label}</span>
        </div>
      );
    });
    const receiversSelected =
      receivers.length > 0
        ? receivers.map(item => {
            let receiver = {
              value: item.value,
              label: item.label
            };
            return receiver;
          })
        : [];

    const superSelectMenuCloseButton = (
      <FlatButton label={dictionary._close} backgroundColor={'red'} />
    );
    const superSelect = (
      <SuperSelectField
        key={0}
        style={{ width: '90%' }}
        name="selectRoles"
        multiple
        onChange={this.updateRoles}
        value={receiversSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsRole(receiversSelected)}
        menuCloseButton={superSelectMenuCloseButton}
      >
        {receiverListNodes}
      </SuperSelectField>
    );

    const aoiStep = (
      <InputAOIStep
        getMapDraw={getMapDraw}
        isDisabled={isDisabled}
        category={category}
        address={address}
        setDrawPoint={setDrawPoint}
        setDrawPolygon={setDrawPolygon}
        setDrawCategory={setDrawCategory}
        resetHomeDrawState={resetHomeDrawState}
        drawPoint={drawPoint}
        drawPolygon={drawPolygon}
        resetCategory={resetCategory}
      />
    );
    return (
      <div style={{ display: 'flex', flexDirection: 'column', marginTop: 10 }}>
        <span style={{ fontSize: 11, marginBottom: 5 }}>
          Select the audience will receive the notification
        </span>
        {superSelect}
        {aoiStep}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommReceiversStep;
