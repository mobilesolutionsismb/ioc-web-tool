import './style.scss';
import React, { Component } from 'react';
import { Chip, RadioButtonGroup, RadioButton } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { EM_COMM_TYPE, EM_COMM_LEVEL } from 'js/modules/emCommNew';
import { yellowA200, deepOrange500, redA700 } from 'material-ui/styles/colors';
import { EmCommColorSquare } from 'js/components/EmergencyCommunicationsCommons/commons';

const backgroundColorChip = {
  bePrepared: yellowA200,
  actionRequired: deepOrange500,
  dangerToLife: redA700
};

class EmCommTypeStep extends Component {
  handleCustomDisplaySelectionsHazard = level => level =>
    level && level.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip
          style={{ margin: 5 }}
          onRequestDelete={this.onRequestDeleteLevel(level)}
          deleteIconStyle={{ color: 'black', fill: 'black' }}
          labelColor="black"
          backgroundColor={backgroundColorChip[level.value]}
        >
          {this.props.dictionary[level.label]}
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Level</div>
    );

  onRequestDeleteLevel = itemToRemove => event => {
    this.props.setEmCommLevel({ label: '', value: '' });
  };

  setLevel = level => {
    this.props.setEmCommLevel(level);
  };

  setEmCommType = event => {
    this.props.setEmCommType(event.currentTarget.value);
  };

  render() {
    const { dictionary, newEmComm } = this.props;

    const levelListNodes = Object.keys(EM_COMM_LEVEL).map((e, i) => (
      <div key={i} value={e} label={dictionary[EM_COMM_LEVEL[e]]}>
        <div
          style={{
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <span>{dictionary[EM_COMM_LEVEL[e]]}</span>
          <EmCommColorSquare level={e} />
          <br />
        </div>
      </div>
    ));
    const levelSelected = newEmComm.level ? newEmComm.level : { label: '', value: '' };
    const superSelect = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectContentTypeStep"
        floatingLabel="Select the level"
        onChange={this.setLevel}
        value={levelSelected}
        elementHeight={58}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(levelSelected)}
      >
        {levelListNodes}
      </SuperSelectField>
    );
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ marginBottom: 20 }}>
          <RadioButtonGroup
            name="radioGroup"
            defaultSelected="alert"
            style={{ display: 'flex', flexDirection: 'column' }}
            valueSelected={newEmComm.type}
          >
            {Object.keys(EM_COMM_TYPE).map((e, i) => (
              <RadioButton
                value={e}
                label={dictionary[EM_COMM_TYPE[e]]}
                style={{ width: 'auto', marginLeft: '10px' }}
                onClick={this.setEmCommType}
                iconStyle={{ marginRight: '5px' }}
                key={i}
              />
            ))}
          </RadioButtonGroup>
        </div>
        {newEmComm.type === 'warning' && superSelect}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommTypeStep;
