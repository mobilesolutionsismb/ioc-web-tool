import React, { Component } from 'react';
import EmCommWarningLevelStep from './EmCommWarningLevelStep';
import EmCommCertaintyStep from './EmCommCertaintyStep';
import EmCommUrgencyStep from './EmCommUrgencyStep';

class EmCommCapDetailsStep extends Component {
  render() {
    const { newEmComm, dictionary, setWarningLevel, setCapCertainty, setCapUrgency } = this.props;

    const capWarinignLevel = (
      <EmCommWarningLevelStep
        dictionary={dictionary}
        newEmComm={newEmComm}
        setWarningLevel={setWarningLevel}
      />
    );

    const capCertainty = (
      <EmCommCertaintyStep
        dictionary={dictionary}
        newEmComm={newEmComm}
        setCapCertainty={setCapCertainty}
      />
    );

    const capUrgency = (
      <EmCommUrgencyStep
        dictionary={dictionary}
        newEmComm={newEmComm}
        setCapUrgency={setCapUrgency}
      />
    );

    return (
      <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
        <div style={{ marginBottom: 15 }}>{capWarinignLevel}</div>
        <div style={{ marginBottom: 15 }}>{capCertainty}</div>
        <div style={{ marginBottom: 15 }}>{capUrgency}</div>
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommCapDetailsStep;
