import './style.scss';
import React, { Component } from 'react';
import { Chip, Avatar } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { HAZARD_TYPES_LABELS, CAP_CATEGORY_LABELS } from 'js/components/Home/components/commons';
import { filterHazards, capCategoryLabels } from 'js/utils/hazardMappings';
import EMCOMM_HAZARD_ICONS from 'js/components/EmergencyCommunicationsCommons/ireact_pinpoint_emcomm_hazard_map';
import EMCOMM_CAP_CATEGORY_ICONS from 'js/components/EmergencyCommunicationsCommons/ireact_pinpoint_capcategory_hazard_map';

class HazardTypeStep extends Component {
  handleCustomDisplaySelectionsHazard = hazard => hazard =>
    hazard && hazard.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteHazard(hazard)}>
          <Avatar className="ireact-pinpoint-icons" backgroundColor={'white'} size={30}>
            {EMCOMM_HAZARD_ICONS[HAZARD_TYPES_LABELS[hazard.value]]}
          </Avatar>
          <span>{this.props.dictionary[hazard.label]}</span>
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Hazard</div>
    );

  handleCustomDisplaySelectionsCapCategory = capCategory => capCategory =>
    capCategory && capCategory.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteCapCategory(capCategory)}>
          <Avatar className="ireact-pinpoint-icons" backgroundColor={'white'} size={30}>
            {EMCOMM_CAP_CATEGORY_ICONS[CAP_CATEGORY_LABELS[capCategory.value]]}
          </Avatar>
          <span>{this.props.dictionary[capCategory.label]}</span>
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Cap Category</div>
    );

  onRequestDeleteHazard = itemToRemove => event => {
    this.props.setHazard({ label: '', value: '' });
  };
  onRequestDeleteCapCategory = itemToRemove => event => {
    this.props.setCapCategory({ label: '', value: '' });
  };
  setHazard = hazard => {
    this.props.setHazard(hazard);
  };
  setCapCategory = capCategory => {
    this.props.setCapCategory(capCategory);
    if (this.props.hazard.value !== '') {
      let acceptedHazards = filterHazards(capCategory.value);
      if (!acceptedHazards.find(x => x === this.props.hazard.value)) {
        this.props.setHazard({ label: '', value: '' });
      }
    }
  };

  render() {
    const { dictionary } = this.props;
    //.filter(item => item !== '_hazard_unknown' && item !== '_hazard_none')
    const hazardList = filterHazards(this.props.capCategory.value).map(item => {
      let hazard = {
        value: item,
        label: dictionary(item)
      };
      return hazard;
    });
    const capCategoryList = capCategoryLabels.map(item => {
      let hazard = {
        value: item,
        label: dictionary(item)
      };
      return hazard;
    });

    const hazardListNodes = hazardList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={dictionary[category.label]}>
          <span>{dictionary[category.label]}</span>
        </div>
      );
    });

    const capCategoryListNodes = capCategoryList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={dictionary[category.label]}>
          <span>{dictionary[category.label]}</span>
        </div>
      );
    });

    const hazardSelected = this.props.hazard;
    const capCategorySelected = this.props.capCategory;

    const superSelectHazard = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectHazardTypeStep"
        onChange={this.setHazard}
        value={hazardSelected}
        showAutocompleteThreshold={'always'}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(hazardSelected)}
      >
        {hazardListNodes}
      </SuperSelectField>
    );
    const superselectCapCategory = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectCapCategoryStep"
        onChange={this.setCapCategory}
        showAutocompleteThreshold={'always'}
        value={capCategorySelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsCapCategory(capCategorySelected)}
      >
        {capCategoryListNodes}
      </SuperSelectField>
    );

    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ marginBottom: 20 }}>
          {superselectCapCategory}
          {superSelectHazard}
        </div>
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default HazardTypeStep;
