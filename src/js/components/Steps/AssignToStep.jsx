import './style.scss';
import React, { Component } from 'react';
import { ListItem, List, Chip } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';

class AssignToStep extends Component {
  onRequestDeleteTeam = team => event => {
    this.props.handleTeamRemove();
  };

  handleCustomDisplaySelections = team => team =>
    team && team.value !== 0 ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        {this.props.isDisabled ? (
          <Chip style={{ margin: 5 }}>
            <span>{team.label}</span>
          </Chip>
        ) : (
          <Chip style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteTeam(team.value)}>
            <span>{team.label}</span>
          </Chip>
        )}
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select a team</div>
    );

  render() {
    const { teams, team } = this.props;

    const selectedTeam = team
      ? { value: team.id, label: team.displayName }
      : { value: 0, displayName: '' };
    const teamListNodes = teams.map((team, index) => {
      return (
        <div key={index} value={team.id} label={team.displayName}>
          <span>{team.displayName}</span>
        </div>
      );
    });

    const superSelTeams = (
      <div key={0} style={{ display: 'flex', alignItems: 'flex-end' }}>
        <SuperSelectField
          disabled={this.props.isDisabled}
          style={{ width: '90%' }}
          name="selectTeam"
          floatingLabel="Select a team"
          onChange={this.props.handleTeamChange}
          value={selectedTeam}
          selectionsRenderer={this.handleCustomDisplaySelections(teams)}
        >
          {teamListNodes}
        </SuperSelectField>
      </div>
    );
    return (
      <div>
        <div>
          <List style={{ whiteSpace: 'noWrap' }}>
            <ListItem key={4} disabled={true} primaryText="Team" />
            <ListItem key={5} disabled={true} children={superSelTeams} />
          </List>
        </div>
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default AssignToStep;
