import './style.scss';
import React, { Component } from 'react';
import { Chip, RadioButtonGroup, RadioButton } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { EM_COMM_TYPE, CAP_STATUS } from 'js/modules/emCommNew';
import { white } from 'material-ui/styles/colors';

class EmCommCapStatusStep extends Component {
  handleCustomDisplaySelectionsHazard = capStatus => capStatus =>
    capStatus && capStatus.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip
          style={{ margin: 5 }}
          onRequestDelete={this.onRequestDeleteCapStatus(capStatus)}
          deleteIconStyle={{ color: 'black', fill: 'black' }}
          labelColor="black"
          backgroundColor={white}
        >
          {this.props.dictionary[capStatus.label]}
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Cap Status</div>
    );

  onRequestDeleteCapStatus = itemToRemove => event => {
    this.props.setCapStatus({ label: '', value: '' });
  };

  setCapStatus = capStatus => {
    this.props.setCapStatus(capStatus);
  };

  setEmCommType = event => {
    this.props.setEmCommType(event.currentTarget.value);
  };

  render() {
    const { dictionary, newEmComm } = this.props;

    const capStatusListNodes = Object.keys(CAP_STATUS).map((e, i) => (
      <div key={i} value={e} label={dictionary[CAP_STATUS[e]]}>
        <div
          style={{
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <span>{dictionary[CAP_STATUS[e]]}</span>
          <br />
        </div>
      </div>
    ));
    const capStatusSelected = newEmComm.capStatus ? newEmComm.capStatus : { label: '', value: '' };
    const superSelect = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectCapStatusStep"
        floatingLabel="Select the Status"
        onChange={this.setCapStatus}
        value={capStatusSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(capStatusSelected)}
      >
        {capStatusListNodes}
      </SuperSelectField>
    );
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ marginBottom: 20 }}>
          <RadioButtonGroup
            name="radioGroup"
            defaultSelected="alert"
            style={{ display: 'flex', flexDirection: 'column' }}
            valueSelected={newEmComm.type}
          >
            {Object.keys(EM_COMM_TYPE).map((e, i) => (
              <RadioButton
                value={e}
                label={dictionary[EM_COMM_TYPE[e]]}
                style={{ width: 'auto', marginLeft: '10px' }}
                onClick={this.setEmCommType}
                iconStyle={{ marginRight: '5px' }}
                key={i}
              />
            ))}
          </RadioButtonGroup>
        </div>
        {superSelect}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommCapStatusStep;
