import './style.scss';
import React, { Component } from 'react';
import { RadioButtonGroup, RadioButton } from 'material-ui';

class EmCommVisibilityStep extends Component {
  setEmCommVisibility = event => {
    this.props.setEmCommVisibility(event.currentTarget.value);
  };

  render() {
    const { dictionary, visibility, visibilityList } = this.props;

    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ marginBottom: 20 }}>
          <RadioButtonGroup
            name="radioGroup"
            style={{ display: 'flex', flexDirection: 'column' }}
            valueSelected={visibility}
          >
            {Object.keys(visibilityList).map((e, i) => (
              <RadioButton
                value={e}
                label={dictionary['_' + e]}
                style={{ width: 'auto', marginLeft: '10px' }}
                onClick={this.setEmCommVisibility}
                iconStyle={{ marginRight: '5px' }}
                key={i}
              />
            ))}
          </RadioButtonGroup>
        </div>
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommVisibilityStep;
