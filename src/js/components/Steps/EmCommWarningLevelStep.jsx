import './style.scss';
import React, { Component } from 'react';
import { Chip } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { CAP_WARNING_LEVEL } from 'js/modules/emCommNew';
import { white, yellowA200, deepOrange500, redA700, green800 } from 'material-ui/styles/colors';
import { WarningLevelColorSquare } from 'js/components/EmergencyCommunicationsCommons/commons';

const backgroundColorChip = {
  unknown: white,
  minor: green800,
  moderate: yellowA200,
  severe: deepOrange500,
  extreme: redA700
};

class EmCommWarningLevelStep extends Component {
  handleCustomDisplaySelectionsHazard = warningLevel => warningLevel =>
    warningLevel && warningLevel.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip
          style={{ margin: 5 }}
          onRequestDelete={this.onRequestDeleteWarningLevel(warningLevel)}
          deleteIconStyle={{ color: 'black', fill: 'black' }}
          labelColor="black"
          backgroundColor={backgroundColorChip[warningLevel.value]}
        >
          {this.props.dictionary[warningLevel.label]}
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Severity</div>
    );

  onRequestDeleteWarningLevel = itemToRemove => event => {
    this.props.setWarningLevel({ label: '', value: '' });
  };

  setWarningLevel = warningLevel => {
    this.props.setWarningLevel(warningLevel);
  };

  render() {
    const { dictionary, newEmComm } = this.props;

    const capWarLevListNodes = Object.keys(CAP_WARNING_LEVEL).map((e, i) => (
      <div key={i} value={e} label={dictionary[CAP_WARNING_LEVEL[e]]}>
        <div
          style={{
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start'
          }}
        >
          <WarningLevelColorSquare warningLevel={e} />
          <span>{dictionary[CAP_WARNING_LEVEL[e]]}</span>
          <br />
        </div>
      </div>
    ));
    const warningLevelSelected = newEmComm.warningLevel
      ? newEmComm.warningLevel
      : { label: '', value: '' };
    const superSelect = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectWarningLevelStep"
        onChange={this.setWarningLevel}
        value={warningLevelSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(warningLevelSelected)}
      >
        {capWarLevListNodes}
      </SuperSelectField>
    );
    return (
      <div>
        {superSelect}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommWarningLevelStep;
