import './style.scss';
import React, { Component } from 'react';
import { Chip } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { CAP_RESPONSE_TYPE } from 'js/modules/emCommNew';
import { white } from 'material-ui/styles/colors';
import { TitleStep } from 'js/components/Steps';

class EmCommResponseTypeStep extends Component {
  handleCustomDisplaySelectionsHazard = capResponseType => capResponseType =>
    capResponseType && capResponseType.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip
          style={{ margin: 5 }}
          onRequestDelete={this.onRequestDeleteCapResponseType(capResponseType)}
          deleteIconStyle={{ color: 'black', fill: 'black' }}
          labelColor="black"
          backgroundColor={white}
        >
          {this.props.dictionary[capResponseType.label]}
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Response Type</div>
    );

  onRequestDeleteCapResponseType = itemToRemove => event => {
    this.props.setCapResponseType({ label: '', value: '' });
  };

  setCapResponseType = capResponseType => {
    this.props.setCapResponseType(capResponseType);
  };

  handleInstructionChange = e => {
    this.props.handleInstructionChange(e);
  };

  render() {
    const { dictionary, newEmComm } = this.props;

    const capResponseTypeListNodes = Object.keys(CAP_RESPONSE_TYPE).map((e, i) => (
      <div key={i} value={e} label={dictionary[CAP_RESPONSE_TYPE[e]]}>
        <div
          style={{
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start'
          }}
        >
          <span>{dictionary[CAP_RESPONSE_TYPE[e]]}</span>
          <br />
        </div>
      </div>
    ));
    const capResponseTypeSelected = newEmComm.capResponseType
      ? newEmComm.capResponseType
      : { label: '', value: '' };
    const superSelect = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectResponseTypeStep"
        onChange={this.setCapResponseType}
        value={capResponseTypeSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(capResponseTypeSelected)}
      >
        {capResponseTypeListNodes}
      </SuperSelectField>
    );

    const titleStep = (
      <TitleStep
        title={newEmComm.instruction}
        handleTitleChange={this.handleInstructionChange}
        dictionary={dictionary}
      />
    );

    return (
      <div>
        {superSelect}
        {titleStep}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommResponseTypeStep;
