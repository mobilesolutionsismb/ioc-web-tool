import './style.scss';
import React, { Component } from 'react';
import { FlatButton } from 'material-ui';

class TasksStep extends Component {
  _addTask = () => {
    this.props.toggleTaskDialog();
  };

  render() {
    const { dictionary } = this.props;
    return (
      <div>
        <div>
          <FlatButton
            label={dictionary._add}
            primary={true}
            onClick={this._addTask}
            style={{ color: 'white', backgroundColor: 'royalblue' }}
          />
        </div>
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default TasksStep;
