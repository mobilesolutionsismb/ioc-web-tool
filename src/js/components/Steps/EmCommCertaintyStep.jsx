import './style.scss';
import React, { Component } from 'react';
import { Chip } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { CAP_CERTAINTY } from 'js/modules/emCommNew';
import { white } from 'material-ui/styles/colors';

class EmCommCertaintyStep extends Component {
  handleCustomDisplaySelectionsHazard = capCertainty => capCertainty =>
    capCertainty && capCertainty.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip
          style={{ margin: 5 }}
          onRequestDelete={this.onRequestDeleteCapCertainty(capCertainty)}
          deleteIconStyle={{ color: 'black', fill: 'black' }}
          labelColor="black"
          backgroundColor={white}
        >
          {this.props.dictionary[capCertainty.label]}
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Certainty</div>
    );

  onRequestDeleteCapCertainty = itemToRemove => event => {
    this.props.setCapCertainty({ label: '', value: '' });
  };

  setCapCertainty = capCertainty => {
    this.props.setCapCertainty(capCertainty);
  };

  render() {
    const { dictionary, newEmComm } = this.props;
    const type = newEmComm.type === '' ? 'alert' : newEmComm.type;
    const capCertaintyListNodes = Object.keys(CAP_CERTAINTY[type]).map((e, i) => (
      <div key={i} value={e} label={dictionary[CAP_CERTAINTY[type][e]]}>
        <div
          style={{
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start'
          }}
        >
          <span>{dictionary[CAP_CERTAINTY[type][e]]}</span>
          <br />
        </div>
      </div>
    ));
    const capCertaintySelected = newEmComm.capCertainty
      ? newEmComm.capCertainty
      : { label: '', value: '' };
    const superSelect = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectCertaintyStep"
        onChange={this.setCapCertainty}
        value={capCertaintySelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(capCertaintySelected)}
      >
        {capCertaintyListNodes}
      </SuperSelectField>
    );
    return (
      <div>
        {superSelect}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommCertaintyStep;
