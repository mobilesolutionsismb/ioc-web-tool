import './style.scss';
import React, { Component } from 'react';
import { Chip } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';
import { CAP_URGENCY } from 'js/modules/emCommNew';
import { white } from 'material-ui/styles/colors';

class EmCommUrgencyStep extends Component {
  handleCustomDisplaySelectionsHazard = capUrgency => capUrgency =>
    capUrgency && capUrgency.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip
          style={{ margin: 5 }}
          onRequestDelete={this.onRequestDeleteCapUrgency(capUrgency)}
          deleteIconStyle={{ color: 'black', fill: 'black' }}
          labelColor="black"
          backgroundColor={white}
        >
          {this.props.dictionary[capUrgency.label]}
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Urgency</div>
    );

  onRequestDeleteCapUrgency = itemToRemove => event => {
    this.props.setCapUrgency({ label: '', value: '' });
  };

  setCapUrgency = capUrgency => {
    this.props.setCapUrgency(capUrgency);
  };

  render() {
    const { dictionary, newEmComm } = this.props;

    const capUrgencyListNodes = Object.keys(CAP_URGENCY).map((e, i) => (
      <div key={i} value={e} label={dictionary[CAP_URGENCY[e]]}>
        <div
          style={{
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start'
          }}
        >
          <span>{dictionary[CAP_URGENCY[e]]}</span>
          <br />
        </div>
      </div>
    ));
    const capUrgencySelected = newEmComm.capUrgency
      ? newEmComm.capUrgency
      : { label: '', value: '' };
    const superSelect = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectUrgencyStep"
        onChange={this.setCapUrgency}
        value={capUrgencySelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(capUrgencySelected)}
      >
        {capUrgencyListNodes}
      </SuperSelectField>
    );
    return (
      <div>
        {superSelect}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default EmCommUrgencyStep;
