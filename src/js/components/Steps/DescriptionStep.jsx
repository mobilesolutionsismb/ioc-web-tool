import './style.scss';
import React, { Component } from 'react';
import { TextField } from 'material-ui';

class DescriptionStep extends Component {
  render() {
    const stepCommentContent = (
      <TextField
        name="description"
        onChange={this.props.handleDescriptionChange}
        value={this.props.description}
        multiLine={true}
        rows={3}
      />
    );
    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'flex-start',
          paddingTop: '25px'
        }}
      >
        {stepCommentContent}
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default DescriptionStep;
