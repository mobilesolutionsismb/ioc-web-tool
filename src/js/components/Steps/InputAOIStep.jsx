import './style.scss';
import React, { Component } from 'react';
import InputAOI from 'js/components/app/drawer/components/InputAOI';

class InputAOIStep extends Component {
  render() {
    const {
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      category,
      address,
      isDisabled,
      getMapDraw,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory,
      setIsDrawing,
      resetCategory
    } = this.props;
    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'flex-start',
          paddingTop: '25px'
        }}
      >
        <InputAOI
          isDisabled={isDisabled}
          category={category}
          address={address}
          getMapDraw={getMapDraw}
          drawPoint={drawPoint}
          drawPolygon={drawPolygon}
          resetHomeDrawState={resetHomeDrawState}
          setDrawPoint={setDrawPoint}
          setDrawPolygon={setDrawPolygon}
          setDrawCategory={setDrawCategory}
          setIsDrawing={setIsDrawing}
          resetCategory={resetCategory}
        />
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default InputAOIStep;
