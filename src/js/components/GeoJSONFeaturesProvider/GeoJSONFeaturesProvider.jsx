import React from 'react';
import axios from 'axios';
import { featureFilter } from '@mapbox/mapbox-gl-style-spec';
import { filterIREACTFeaturesWithBounds } from 'js/utils/filterWithBounds';
import { filterIREACTFeaturesWithActiveReportRequest } from 'js/utils/filterWithActiveReportRequest';
import { filterIREACTFeaturesWithDates } from 'js/utils/filterWithDates';
import { IREACT_FEATURES_AOI_SRC_NAME } from 'js/components/Home2/components/Map/mapViewSettings';

class GeoJSONFeaturesProvider extends React.Component {
  static defaultProps = {
    sourceURL: null,
    filters: null, // filter function, array of mapbox defs or object : {fn: function, def: Array} for combining
    sorter: null
  };

  // See https://stackoverflow.com/questions/34544314/setstate-can-only-update-a-mounted-or-mounting-component-this-usually-mea/40969739#40969739
  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };

  state = {
    features: [],
    updating: false,
    error: null
  };

  _requestFeatures = async () => {
    try {
      if (this._autoRef) {
        this.setState({ error: null, updating: true });
      }
      let features = [];
      if (this.props.sourceURL !== null) {
        try {
          const response = await axios.get(this.props.sourceURL);
          features =
            response.data.type === 'FeatureCollection' ? response.data.features : response.data;
        } catch (err) {
          console.warn(`${this.props.itemType}: ${this.props.sourceURL} not valid`);
        }
      }
      if (this._autoRef) {
        this.setState({ features, updating: false });
      }
    } catch (error) {
      if (this._autoRef) {
        this.setState({ error, updating: false });
      }
    }
  };

  componentDidUpdate(prevProps) {
    if (prevProps.sourceURL !== this.props.sourceURL) {
      this._requestFeatures();
    }
  }

  componentDidMount() {
    this._requestFeatures();
  }

  _getFilterFn(filters) {
    if (!filters) {
      return null;
    }
    let filterFn =
      typeof filters === 'function'
        ? filters
        : typeof filters.fn === 'function'
          ? filters.fn
          : null;
    let mapboxFilterDefs = Array.isArray(filters)
      ? filters
      : Array.isArray(filters.def)
        ? filters.def
        : null;
    if (filterFn && mapboxFilterDefs) {
      // combine both
      const mapboxFilterFn = featureFilter(mapboxFilterDefs).bind(null, null);
      return feature => filterFn(feature) && mapboxFilterFn(feature);
    } else {
      return filterFn
        ? filterFn
        : mapboxFilterDefs
          ? featureFilter(mapboxFilterDefs).bind(null, null)
          : null;
    }
  }

  render() {
    const { children, filters, sorter } = this.props;
    const { features } = this.state;
    let _features = [...features];
    const filterFn = this._getFilterFn(filters);
    if (typeof filterFn === 'function') {
      _features = _features.filter(filterFn);
    }
    if (this.props.drawpolygon && this.props.drawcategory !== 'none') {
      const polygon = { ...this.props.drawpolygon };
      _features = filterIREACTFeaturesWithBounds(_features, polygon, this.props.drawcategory);
    }
    //if RR is active -> filter report according to RR aoi and measures
    if (
      this.props.itemType === 'report' &&
      this.props.selectedFeature &&
      this.props.selectedFeature.properties.itemType === 'reportRequest' &&
      _features.filter(item => item.properties.itemType === 'report').length > 0
    ) {
      const map = this.props.getMap();
      if (!map) {
        return;
      }
      const polygonsSRC = map.getSource(IREACT_FEATURES_AOI_SRC_NAME);
      if (polygonsSRC) {
        const aoi = polygonsSRC._data.features.find(
          item =>
            item.properties.itemType === 'reportRequest' &&
            item.properties.id === this.props.selectedFeature.properties.id
        );
        if (aoi) {
          _features = filterIREACTFeaturesWithBounds(_features, aoi.geometry, 'report');
          _features = filterIREACTFeaturesWithActiveReportRequest(
            _features,
            aoi.properties.measure,
            aoi.properties.hasPeople,
            aoi.properties.hasDamage,
            aoi.properties.hasResource,
            'report'
          );
          _features = filterIREACTFeaturesWithDates(
            _features,
            aoi.properties.startDate,
            aoi.properties.endDate,
            'report'
          );
        }
      }
    }
    if (typeof sorter === 'function') {
      _features = _features.sort(sorter); // TODO check if _.sort() is faster
    }

    return (
      <div ref={this._setAutoRef}>
        {children(
          _features,
          this.state.updating,
          this.state.error,
          filterFn !== null,
          sorter !== null
        )}
      </div>
    );
  }
}

export default GeoJSONFeaturesProvider;
