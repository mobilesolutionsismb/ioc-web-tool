import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { FlatButton } from 'material-ui';
import { Link } from 'react-router-dom';
import { drawerNames } from 'js/modules/AreaOfInterest';

const LINE_STYLE = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginTop: 15,
  marginBottom: 15
};

export const UpdateReportRequestLine = muiThemeable()(({ onEditEndDate, onClearSelection }) => (
  <div style={LINE_STYLE}>
    <FlatButton label="DESELECT" onClick={onClearSelection} labelStyle={{ fontSize: 15 }} />
    <Link to={`/home/${drawerNames.report}?${drawerNames.reportNewRequest}`}>
      <FlatButton label="EDIT END DATE" onClick={onEditEndDate} labelStyle={{ fontSize: 15 }} />
    </Link>
  </div>
));
