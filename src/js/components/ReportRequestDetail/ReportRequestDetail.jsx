import './style.scss';
import React, { Component } from 'react';
import { Card, CardText, CardTitle, CardMedia /* , FontIcon */ } from 'material-ui';
import { ReportDetailButtons, ReportDetailBoxedLine } from 'js/components/DetailCard/commons';
import { withReportNewRequest } from 'js/modules/reportNewRequest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { localizeDate } from 'js/utils/localizeDate';
import {
  LocationOnIcon,
  ScheduleIcon,
  FlightIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import {
  CONTENT_TYPES_LETTERS,
  // CATEGORY_MAP,
  MeasureCategoryIcon
} from 'js/components/ReportsCommons';
import { UpdateReportRequestLine } from './commons';
import { compose } from 'redux';
const enhance = compose(
  withReportNewRequest,
  withLeftDrawer
);

const AVATAR = require('assets/avatars/avatar-01.svg');
const AvatarContainer = ({ avatar }) => (
  <div className="avatarContainer">
    <div className="avatar" style={{ backgroundImage: `url(${avatar})`, height: 40, width: 30 }} />
  </div>
);

const EMPTY_USER = {
  name: '',
  userName: '',
  surname: '',
  avatar: AVATAR,
  score: 1200,
  rank: 23,
  currentLevel: 'Explorer',
  nextLevel: 'Guru',
  pointsToNextLevel: 30
};

const INLINE_ICON_STYLE = { fontSize: 13, lineHeight: '26px', marginRight: '4px' };

class ReportRequestDetail extends Component {
  closeCard = () => {
    setTimeout(() => {
      this.props.toggleDialog(false);
    }, 0);
  };

  shareReportRequest = () => {
    console.log('Share Report Request');
  };

  editEndDate = () => {
    if (this.props.feature) {
      this.props.setOpenSecondary(false);
      this.props.fillReportRequest(this.props.feature.properties);
    }
  };

  clearSelection = () => {
    this.props.deselectFeature();
  };

  render() {
    const { dictionary, locale, feature } = this.props;
    const reportRequest = feature.properties;
    const user = {
      ...EMPTY_USER
    };
    if (reportRequest && reportRequest !== null) {
      const address = reportRequest.address;
      const lineHeight = 35;

      const flightIcon = (
        <FlightIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
        />
      );
      const locationIcon = (
        <LocationOnIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
        />
      );
      const scheduleIcon = (
        <ScheduleIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
        />
      );
      const updateReportRequest = (
        <UpdateReportRequestLine
          onEditEndDate={this.editEndDate}
          onClearSelection={this.clearSelection}
        />
      );

      let listOfItems =
        reportRequest.measures.length > 0 ? (
          reportRequest.measures.map((item, index) => {
            let element = <div />;
            const icon = (
              <MeasureCategoryIcon
                category={item.category}
                style={INLINE_ICON_STYLE}
                color={'#FFFFFF'}
              />
              // <FontIcon className="material-icons" style={INLINE_ICON_STYLE}>
              //   {CATEGORY_MAP[item.category]}
              // </FontIcon>
            );
            if (index !== reportRequest.measures.length - 1)
              element = (
                <ReportDetailBoxedLine
                  customStyle={{ borderRight: 'none', borderBottom: 'none', borderTop: 'none' }}
                  leftIcon={icon}
                  key={index}
                  secondElement={dictionary(item.title)}
                  lineHeight={lineHeight}
                />
              );
            else
              element = (
                <ReportDetailBoxedLine
                  customStyle={{
                    borderRight: 'none',
                    borderBottom: 'none',
                    borderTop: 'none',
                    paddingBottom: 50
                  }}
                  leftIcon={icon}
                  key={index}
                  secondElement={dictionary(item.title)}
                  lineHeight={lineHeight}
                />
              );

            return element;
          })
        ) : (
          <div />
        );

      let avatarContainer = <AvatarContainer {...user} />;

      const lastRowSecondColumn = (
        <div style={{ display: 'flex' }}>
          <span>{reportRequest.sourceOfInformation || reportRequest.organization || ''}</span>
        </div>
      );

      let contentType = reportRequest.contentType
        ? reportRequest.contentType
            .split(',')
            .map(item => item.trim())
            .sort(type => type === 'measure')
        : [];
      const contentTypeList =
        contentType.length > 0 ? (
          contentType.map((item, index) => {
            let element = '';
            const customStyle = {
              borderRight: 'none',
              borderTop: 'none'
            };
            if (item === 'measure') {
              element = (
                <ReportDetailBoxedLine
                  customStyle={customStyle}
                  leftIcon={reportRequest.measures.length}
                  key={index}
                  secondElement={item.toUpperCase()}
                />
              );
            } else if (item === 'resource')
              element = (
                <ReportDetailBoxedLine
                  customStyle={customStyle}
                  leftIcon={CONTENT_TYPES_LETTERS[item]}
                  key={index}
                  secondElement={item.toUpperCase()}
                />
              );
            else if (item === 'damage')
              element = (
                <ReportDetailBoxedLine
                  customStyle={customStyle}
                  leftIcon={CONTENT_TYPES_LETTERS[item]}
                  key={index}
                  secondElement={item.toUpperCase()}
                />
              );
            else if (item === 'people')
              element = (
                <ReportDetailBoxedLine
                  customStyle={customStyle}
                  leftIcon={CONTENT_TYPES_LETTERS[item]}
                  key={index}
                  secondElement={item.toUpperCase()}
                />
              );

            return element;
          })
        ) : (
          <div />
        );

      return (
        <Card style={{ minHeight: 500 }}>
          <CardMedia>
            <ReportDetailButtons
              onCloseClick={this.closeCard}
              onShareClick={this.shareReportRequest}
            />
          </CardMedia>
          <CardTitle
            title="REQUEST:"
            titleStyle={{ fontSize: 15 }}
            style={{ paddingBottom: 0, paddingTop: 8 }}
          />
          <hr className="horizontalLineStyle" />
          <CardText style={{ display: 'flex', padding: 0, height: 400 }}>
            <div style={{ width: '60%', minWidth: 200, overflowY: 'auto', overflowX: 'hidden' }}>
              {contentTypeList}
              {listOfItems}
              <ReportDetailBoxedLine
                customStyle={{
                  borderTop: 'none',
                  borderRight: 'none',
                  borderBottom: 'none',
                  position: 'absolute',
                  bottom: 10
                }}
                leftIcon={avatarContainer}
                secondElement={lastRowSecondColumn}
              />
            </div>
            <div className="verticalLine" />
            <div style={{ minHeight: 140 }}>
              <div className="reportDetailSecondColumnInfo">
                <span>{locationIcon}</span>
                <span>{address}</span>
              </div>
              <div className="reportDetailSecondColumnInfo">
                <span>{scheduleIcon}</span>
                <span>
                  {dictionary._start}: {localizeDate(reportRequest.start, locale, 'L LT')}
                </span>
              </div>
              <div className="reportDetailSecondColumnInfo">
                <span style={{ width: 24 }} />
                <span>
                  {dictionary._end}:{' '}
                  {reportRequest.end ? localizeDate(reportRequest.end, locale, 'L LT') : ''}
                </span>
              </div>
              <div className="reportDetailSecondColumnInfo">
                <span style={{ width: 24 }} />
                <span>Status: {reportRequest.isOngoing ? 'Ongoing' : 'Closed'} </span>
              </div>
              <div className="reportDetailSecondColumnInfo">
                <span>{flightIcon}</span>
                <span>
                  Sent To:{' '}
                  {reportRequest.roles
                    ? reportRequest.roles.map((item, index) => {
                        let label = item.name;
                        if (index !== reportRequest.roles.length - 1) label = label + ', ';

                        return label;
                      })
                    : ''}
                </span>
              </div>

              <div style={{ position: 'absolute', bottom: 0 }}>{updateReportRequest}</div>
            </div>
          </CardText>
          <hr className="horizontalLineStyle" />
        </Card>
      );
    } else return <div className="report-request-detail-popup" style={{ padding: 0 }} />;
  }
}
export default enhance(ReportRequestDetail);
