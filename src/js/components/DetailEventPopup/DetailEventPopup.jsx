import './style.scss';
import React, { Component } from 'react';
import {
  Card,
  CardText,
  CardHeader,
  FontIcon,
  IconButton,
  Chip,
  Avatar,
  FlatButton,
  RaisedButton
} from 'material-ui';
import { withMessages } from 'js/modules/ui';
import { localizeDate } from 'js/utils/localizeDate';
import {
  LocationOnIcon,
  EventIcon,
  PersonIcon,
  MoneyIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { HAZARD_TYPES_LETTERS } from 'js/components/Home/components/commons';
import { getDisplayName } from 'js/components/EventCommons';
import { red900 } from 'material-ui/styles/colors';
import { compose } from 'redux';
import { withEventCreate } from 'js/modules/eventCreate';
import { Link } from 'react-router-dom';
import { withEmergencyEvents } from 'ioc-api-interface';
const enhance = compose(
  withMessages,
  withEventCreate,
  withEmergencyEvents
);

class DetailEventPopup extends Component {
  closeCard = () => {
    this.props.toggleDialog(false);
  };

  onEditCard = () => {
    if (this.props.feature != null) {
      this.props.fillEmergencyEvent(this.props.feature.properties);
    }
  };

  changeStatus = async event => {
    try {
      if (this.props.activeEventId !== event.id) {
        await this.props.activateEvent(event.id);
      } else {
        await this.props.deactivateEvent();
      }
    } catch (err) {
      console.error(`An error occurred while activating event ${event.id}:`, err);
    }
  };

  getAddress = event => {
    if (event.address) {
      return event.address;
    } else {
      const isValid = e => typeof e === 'string' && e.length;
      const locations = Array.isArray(event.locations)
        ? event.locations.filter(isValid).join(', ')
        : null;
      const provinces = Array.isArray(event.provinces)
        ? event.provinces.filter(isValid).join(', ')
        : null;
      const countryNames = Array.isArray(event.countryNames)
        ? event.countryNames
            .filter(isValid)
            .map((cn, i) => `${cn} (${event.countryCodes[i]})`)
            .join(', ')
        : null;
      return `${locations ? locations + ' - ' : ''}${provinces ? provinces + '  - ' : ''}${
        countryNames ? countryNames : ''
      }`;
    }
  };

  render() {
    const dict = this.props.dictionary;
    let eEvent = this.props.feature ? this.props.feature.properties : {};
    let extensionData;
    try {
      extensionData =
        eEvent !== null && eEvent.extensionData !== null ? eEvent.extensionData : null;
    } catch (e) {
      console.log(`An error occurred trying to get the extensionData of event ${eEvent.id}`);
    }

    const displayName = eEvent ? getDisplayName(eEvent) : '';
    const displayDescription =
      eEvent !== null && eEvent.comment ? (
        <div style={{ margin: 20 }}>
          {dict._description}: {eEvent.comment}
        </div>
      ) : (
        <div style={{ margin: 20 }}>{dict._description}: </div>
      );
    const displayOrganization =
      eEvent !== null && eEvent.organization === 'SET HERE MY ORGANIZATION' ? (
        <div />
      ) : (
        <div style={{ display: 'flex', alingItems: 'center', marginLeft: 20, marginTop: 10 }}>
          <FontIcon className="material-icons">volume_up</FontIcon>
          <span style={{ marginTop: 3, marginLeft: 10 }}>EMDAT</span>
        </div>
      );
    let address;
    if (eEvent !== null) address = this.getAddress(eEvent);
    const locationIcon = (
      <LocationOnIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '14px', color: 'lightgrey' }}
      />
    );
    const calendarIcon = (
      <EventIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '14px', color: 'lightgrey' }}
      />
    );
    // const pAffectedIcon = <PersonIcon style={{ width: 24, height: 24, padding: 0 }} iconStyle={{ fontSize: '14px', color: 'lightgrey' }} />;
    const causaltiesIcon = (
      <PersonIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '14px', color: 'lightgrey' }}
      />
    );
    const damagesIcon = (
      <MoneyIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '14px', color: 'lightgrey' }}
      />
    );

    return (
      <div className="detail-popup" style={{ padding: 0, paddingBottom: 0 }}>
        {eEvent === null ? (
          <span />
        ) : (
          <Card
            className="cardEventMap"
            style={{
              width: this.props.sizeWidth ? this.props.sizeWidth : 500,
              zIndex: 99,
              padding: 0,
              minHeight: 500
            }}
          >
            <CardHeader
              className="carEventHeader"
              style={{ padding: 0, marginBottom: 30, paddingBottom: 0 }}
            >
              <div
                style={{
                  float: 'left',
                  marginTop: 12,
                  marginLeft: 20,
                  paddingBottom: 0
                }}
              >
                <Chip style={{ height: 24 }}>
                  <Avatar
                    size={24}
                    style={{ height: 24, width: 24 }}
                    backgroundColor={eEvent.end === null ? red900 : 'black'}
                  >
                    {HAZARD_TYPES_LETTERS[eEvent.hazard]}
                  </Avatar>
                  <div className="eventCardHazzard">
                    <span>{this.props.dictionary['_hazard_' + eEvent.hazard]}</span>
                  </div>
                </Chip>
              </div>
              <IconButton style={{ float: 'right' }} onClick={this.closeCard}>
                <FontIcon className="material-icons">close</FontIcon>
              </IconButton>
            </CardHeader>
            <CardText style={{ padding: 0, display: 'flex', height: 450 }}>
              <div
                style={{ borderRight: '1px gray solid', borderTop: '1px gray solid', width: '55%' }}
              >
                <div style={{ marginTop: 12, marginLeft: 20, color: 'white', height: 50 }}>
                  {displayName}
                </div>
                <div style={{ borderTop: '1px gray solid', height: 330 }}>{displayDescription}</div>
                <div style={{ borderTop: '1px gray solid', height: 50 }}>{displayOrganization}</div>
              </div>
              <div style={{ borderTop: '1px gray solid', width: '45%' }}>
                <div
                  style={{
                    marginTop: 12,
                    marginLeft: 20,
                    marginRight: 12,
                    height: 380,
                    fontSize: '12px'
                  }}
                >
                  <div className="EventPopupData">
                    {locationIcon}
                    <span>{address}</span>
                  </div>
                  <div className="EventPopupData">
                    {calendarIcon}
                    <div>{dict._start}: </div>
                    <div style={{ paddingLeft: 2 }}>
                      {localizeDate(eEvent.start, this.props.locale, 'L LT', true)}{' '}
                    </div>
                  </div>
                  <div className="EventPopupData EventPopendDate">
                    <div>{dict._end}: </div>
                    <div style={{ paddingLeft: 2 }}>
                      {eEvent.end && eEvent.end !== null
                        ? localizeDate(eEvent.end, this.props.locale, 'L LT', true)
                        : dict._not_set}{' '}
                    </div>
                  </div>
                  <div
                    className="EventPopupData"
                    style={{
                      color:
                        !extensionData ||
                        extensionData == null ||
                        extensionData.people_affected == null
                          ? 'gray'
                          : 'white'
                    }}
                  >
                    {locationIcon}
                    <div style={{ paddingLeft: 2 }}>
                      {this.props.dictionary._people_affected}:{' '}
                      {extensionData &&
                      extensionData !== null &&
                      extensionData.people_affected !== null
                        ? extensionData.people_affected
                        : ''}{' '}
                    </div>
                  </div>
                  <div
                    className="EventPopupData"
                    style={{
                      color:
                        !extensionData ||
                        extensionData == null ||
                        extensionData.people_killed == null
                          ? 'gray'
                          : 'white'
                    }}
                  >
                    {causaltiesIcon}
                    <div style={{ paddingLeft: 2 }}>
                      {this.props.dictionary._causalties}:{' '}
                      {extensionData &&
                      extensionData !== null &&
                      extensionData.people_killed !== null
                        ? extensionData.people_killed
                        : ''}{' '}
                    </div>
                  </div>
                  <div
                    className="EventPopupData"
                    style={{
                      color:
                        !extensionData ||
                        extensionData == null ||
                        extensionData.estimated_damage == null
                          ? 'gray'
                          : 'white'
                    }}
                  >
                    {damagesIcon}
                    <div style={{ paddingLeft: 2 }}>
                      {this.props.dictionary._estimated_damages}:{' '}
                      {extensionData &&
                      extensionData !== null &&
                      extensionData.estimated_damage !== null
                        ? extensionData.estimated_damage
                        : ''}{' '}
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    height: 50,
                    display: 'Flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Link to={`/home/${drawerNames.event}?event_create`}>
                    <FlatButton label={dict._edit} onClick={this.onEditCard} />
                  </Link>
                  <RaisedButton
                    label={
                      this.props.activeEventId === eEvent.id ? dict._deactivate : dict._set_active
                    }
                    onClick={this.changeStatus.bind(this, eEvent)}
                    primary={true}
                  />
                </div>
              </div>
            </CardText>
          </Card>
        )}
      </div>
    );
  }
}
export default enhance(DetailEventPopup);
