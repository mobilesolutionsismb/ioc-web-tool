import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { CloseIcon, ShareIcon, FullScreenIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { IconButton, FontIcon } from 'material-ui';

export const ReportDetailButtons = muiThemeable()(
  ({ onCloseClick, onShareCLick, onFullScreenClick }) => (
    <div style={{ position: 'absolute', right: 5, top: 5, zIndex: 1 }}>
      {onFullScreenClick && (
        <FullScreenIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
          onClick={onFullScreenClick}
        />
      )}
      {onShareCLick && (
        <ShareIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
        />
      )}
      {onCloseClick && (
        <CloseIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
          onClick={onCloseClick}
        />
      )}
    </div>
  )
);

export const ChangeReportStatusButtons = muiThemeable()(
  ({ isValidated, onValidateClick, onRejectClick }) => (
    <div>
      {!isValidated ? (
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center',
            fontSize: 11,
            position: 'absolute',
            bottom: 0,
            right: 5
          }}
        >
          <span>Validate Report: </span>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'center',
              marginBottom: 5
            }}
          >
            <IconButton style={{ padding: 5 }} onClick={onValidateClick}>
              {' '}
              <i className="material-icons md-36 red">cancel</i>
            </IconButton>
            <IconButton style={{ padding: 5 }} onClick={onRejectClick}>
              {' '}
              <i className="material-icons md-36 green">check_circle</i>
            </IconButton>
          </div>
        </div>
      ) : (
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center',
            fontSize: 11,
            position: 'absolute',
            bottom: 0,
            right: 5
          }}
        >
          <IconButton style={{ padding: 5, marginBottom: 5 }} onClick={onRejectClick}>
            {' '}
            <i className="material-icons md-36 green">check_circle</i>
          </IconButton>
        </div>
      )}
    </div>
  )
);

export const ReportDetailBoxedLine = muiThemeable()(
  ({ customStyle, leftIcon, secondElement, thirdElement, content, lineWidth, lineHeight }) => (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        borderStyle: 'ridge',
        borderColor: 'grey',
        borderWidth: 1,
        width: lineWidth,
        height: lineHeight,
        padding: 0,
        ...customStyle
      }}
    >
      <span style={{ paddingLeft: 10, paddingRight: 20 }}>
        {isNaN(leftIcon) ? (
          <FontIcon className="material-icons" style={{ fontSize: 18 }}>
            {leftIcon}
          </FontIcon>
        ) : (
          <span style={{ padding: 5 }}>{leftIcon}</span>
        )}
      </span>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          width: '100%',
          height: '100%'
        }}
      >
        <span>{secondElement}</span>
        <span style={{ paddingRight: 20 }}>{thirdElement}</span>
      </div>
    </div>
  )
);
