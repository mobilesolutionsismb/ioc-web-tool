import React, { Component } from 'react';
import LeftDrawerFilterHeader from 'js/components/app/drawer/LeftDrawerFilterHeader';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import StatusHeaderFilter from 'js/components/app/drawer/StatusHeaderFilter';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import AddActionButton from 'js/components/app/drawer/AddActionButton';
import { compose } from 'redux';
// import { featureFilter as ff } from '@mapbox/mapbox-gl-style-spec';
import EventsInfiniteList from './EventsInfiniteList';
import { withEmergencyEvents } from 'ioc-api-interface';
import { withEventCreate } from 'js/modules/eventCreate';
const enhance = compose(
  withLeftDrawer,
  withEmergencyEvents,
  withEventCreate
);

const CHECKBOXES_CONFIG = [
  {
    label: 'Ongoing',
    type: 'status',
    value: '_is_status_ongoing',
    paletteColor: 'ongoingReportRequestColor'
  },
  {
    label: 'Closed',
    type: 'status',
    value: '_is_status_closed',
    paletteColor: 'closedReportRequestColor'
  }
];

class EmergencyEventDrawerInner extends Component {
  toggleSecondDrawer = () => {
    this.props.setOpenSecondary(!this.props.isSecondaryOpen, drawerNames.eventFilter);
  };

  createEmergencyEvent = () => {
    const isNewEventActive = this.props.search === '?event_create';
    this.props.setOpenSecondary(false);
    this.props.deselectEvent();
    //this.props.deactivateEvent();
    this.props.resetEventCreate();
    this.props.history.replace(
      `/home/${drawerNames.event}${!isNewEventActive ? '?event_create' : ''}`
    );
  };

  render() {
    const { dictionary, /*  filters, */ deselectEvent, isRrActive } = this.props;

    let titleDrawer = null,
      statusDrawer = null,
      filterHeader = null,
      listOfItems = null,
      addActionButton = null;

    titleDrawer = (
      <DrawerTitle
        key="title"
        category={drawerNames.event}
        title={dictionary._tab_header_events}
        rightIcon="filter_list"
        secondDrawerName={drawerNames.eventFilter}
        rightAction={this.toggleSecondDrawer}
      />
    );

    filterHeader = (
      <LeftDrawerFilterHeader
        checkboxes={CHECKBOXES_CONFIG}
        headerType="event"
        key="filters"
        deselectEvent={deselectEvent}
      />
    );

    statusDrawer = (
      <StatusHeaderFilter
        key="status"
        numberOfResults={this.props.emergencyEvents.totalCount}
        nameOfResults={dictionary._drawer_label_events}
        headerType="event"
        headerTypeSorter="eventSorter"
      />
    );

    listOfItems = (
      <EventsInfiniteList
        key="infiniteList"
        emergencyEvents={this.props.emergencyEvents}
        getEmergencyEvents={this.props.getEmergencyEvents}
        deselectFeature={this.props.deselectFeature}
        isRrActive={isRrActive}
      />
    );

    addActionButton = (
      <AddActionButton addAction={this.createEmergencyEvent} key="addActionButton" />
    );

    return [titleDrawer, filterHeader, statusDrawer, listOfItems, addActionButton];
  }
}

export default enhance(EmergencyEventDrawerInner);
