import React from 'react';
import nop from 'nop';
import { List } from 'react-virtualized';
import { InfiniteLoader } from 'react-virtualized';
import muiThemeable from 'material-ui/styles/muiThemeable';
import EventCard from 'js/components/EventCard/EventCard';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withEmergencyEvents } from 'ioc-api-interface';
import { withEventFilters } from 'js/modules/clientSideEventFilters';
const enhance = compose(
  withDictionary,
  withEventFilters,
  withEmergencyEvents
);

const EVENT_CARD_HEIGHT = 150;
const ITEMS_SPACING = 8;
const FIXED_HEADER_SIZE = 0; //255

const Loading = () => <h3>Loading...</h3>;

function getRowRenderer(list, isRowLoaded, cardProps, isRrActive) {
  // Render a list item or a loading indicator.
  const rowRenderer = ({ index, key, style }) => {
    let content;

    if (!isRowLoaded({ index })) {
      content = <Loading />;
    } else {
      const feature = list[index];
      if (!feature) {
        content = <Loading />;
      } else {
        content = <EventCard event={feature.properties} {...cardProps} isRrActive={isRrActive} />;
      }
    }

    return (
      <div key={key} style={style}>
        {content}
      </div>
    );
  };

  return rowRenderer;
}

class EventsInfiniteList extends React.Component {
  state = {
    height: Math.max(window.innerHeight - FIXED_HEADER_SIZE, 100) // we don't want negative numbers
  };

  updateHeight = () => {
    this.setState({ height: Math.max(window.innerHeight - FIXED_HEADER_SIZE, 100) });
  };

  componentDidMount = () => {
    window.addEventListener('resize', this.updateHeight);
    this._loadEvents();
  };

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateHeight);
  };

  _loadEvents = () => {
    const { skip, limit } = this.props.emergencyEvents;
    this.props.getEmergencyEvents(skip, limit);
  };

  render() {
    const {
      enums,
      locale,
      dictionary,
      dictIsEmpty,
      openSecondDrawer,
      history,
      muiTheme,
      deselectFeature,
      isRrActive
    } = this.props;
    const cardProps = {
      enums,
      locale,
      dictionary,
      dictIsEmpty,
      openSecondDrawer,
      history,
      deselectFeature
    };

    const { emergencyEvents } = this.props;
    const { hasMore, isFetching, items, totalCount, skip } = emergencyEvents;

    const loadMoreRows = isFetching ? nop : this._loadEvents;
    // Every row is loaded except for our loading indicator row.
    const isRowLoaded = ({ index }) => !hasMore || index < skip;

    return (
      <InfiniteLoader isRowLoaded={isRowLoaded} loadMoreRows={loadMoreRows} rowCount={totalCount}>
        {({ onRowsRendered, registerChild }) => (
          <List
            style={{ background: muiTheme.palette.backgroundColor }}
            ref={registerChild}
            onRowsRendered={onRowsRendered}
            rowRenderer={getRowRenderer(items, isRowLoaded, cardProps, isRrActive)}
            width={380}
            height={this.state.height}
            rowHeight={EVENT_CARD_HEIGHT + ITEMS_SPACING}
            rowCount={totalCount}
          />
        )}
      </InfiniteLoader>
    );
  }
}

export default muiThemeable()(enhance(EventsInfiniteList));
