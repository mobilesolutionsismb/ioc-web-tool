import './styles/_emcomm_card.scss';
import React, { Component } from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { localizeDate } from 'js/utils/localizeDate';
import { EmCommChip, EmCommLanguage, LevelBox } from 'js/components/EmergencyCommunicationsCommons';

const themed = muiThemeable();

class EmergencyCommunicationCard extends Component {
  static defaultProps = {
    showMapIcon: false,
    featureProperties: null,
    onCloseButtonClick: null
  };

  render() {
    const { featureProperties, dictionary, locale, muiTheme } = this.props;
    const {
      end,
      start,
      address,
      type,
      warningLevel,
      capStatus,
      organization,
      sourceOfInformation,
      isOngoing
    } = featureProperties;
    const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
    const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
    const emcommDatesString = `${dateStartedString}${end ? ' - ' + dateFinishedString : ''}`;
    const palette = muiTheme.palette;
    const severity = typeof warningLevel === 'string' ? warningLevel : 'unknown';
    const capStatusString = capStatus !== 'actual' && capStatus !== 0 ? '/' + capStatus : '';

    const statusColor = isOngoing
      ? palette.ongoingReportRequestColor
      : palette.closedReportRequestColor;

    if (featureProperties) {
      return (
        <div className="emcomm-card emcomm-details-body">
          <div className="emcomm-card__content">
            <div className="emcomm-card__info">
              <EmCommChip dictionary={dictionary} palette={palette} emComm={featureProperties} />
              {featureProperties.language && (
                <EmCommLanguage language={featureProperties.language} />
              )}
            </div>
            <div className="emcomm-card__emcdesc" style={{ display: 'flex' }}>
              <LevelBox severity={severity} size="18px" />
              <span className="emcomm-card__emctype">
                {dictionary(`_emcomm_${type}`).toLocaleUpperCase()}
                {capStatusString}
              </span>
            </div>
            <div className="emcomm-card__address">{address}</div>
            <div className="emcomm-card__dates">
              <span>{emcommDatesString}</span>
              <div className="emcomm-status" style={{ backgroundColor: statusColor }} />
            </div>
            <div className="emcomm-card__organization">
              <span>{sourceOfInformation || organization || 'EMDAT'}</span>
            </div>
          </div>
          {/* <div className="emcomm-card__actions">
            {!showMapIcon &&
              typeof onCloseButtonClick === 'function' && (
                <CloseButton
                  onClick={onCloseButtonClick}
                  style={{ padding: 0, width: 24, height: 24 }}
                />
              )}
            <ShareIcon tooltip="Share" onClick={onShareButtonClick} />
            {showMapIcon && <GoToMapIcon tooltip="Show On Map" onClick={onMapButtonClick} />}
          </div> */}
        </div>
      );
    } else {
      return <div className="emcomm-card reportrequest" />;
    }
  }
}

export default themed(EmergencyCommunicationCard);
