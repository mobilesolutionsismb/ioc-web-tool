import './styles/agent_location_style.scss';
import React, { Component } from 'react';
import { localizeDate } from 'js/utils/localizeDate';
import { MenuItem, IconButton, IconMenu, FontIcon } from 'material-ui';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { withDictionary } from 'ioc-localization';
import {
  PowerIcon,
  AccessibilityIcon,
  TollIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import { lightGreenA400, redA700 } from 'material-ui/styles/colors';
import { AGENT_ICONS } from 'js/components/Home2/components/Map/mapboxStyles/singleFeaturesLayers';

class AgentLocationCard extends Component {
  static defaultProps = {
    dataOrigin: 'mobile',
    critical: false,
    missions: [
      {
        id: 1,
        description: 'First mission for the user'
      },
      {
        id: 2,
        description: 'Second ongoing mission'
      }
    ]
  };

  render() {
    const { locale, featureProperties, style, dictionary } = this.props;
    const { name, surname, lastModified, dataOrigin, ongoingMissions } = featureProperties;

    const verticalDots = (
      <IconMenu
        iconButtonElement={
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        }
        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
        useLayerForClickAway={true}
        onClick={event => {
          event.stopPropagation();
        }}
        iconStyle={{ padding: 0 }}
      >
        <MenuItem
          id={featureProperties.id}
          primaryText="Call 012345678"
          onClick={event => {
            event.stopPropagation();
          }}
        />
      </IconMenu>
    );

    return (
      <div className="agent-location-card" style={style}>
        <div className="agent-icon">
          {dataOrigin === 'wearable' ? (
            <FontIcon style={{ color: 'orange' }} className="ireact-pinpoint-icons">
              {AGENT_ICONS['agentLocationWearable']}
            </FontIcon>
          ) : (
            <FontIcon style={{ color: 'orange' }} className="ireact-pinpoint-icons">
              {AGENT_ICONS['agentLocation']}
            </FontIcon>
          )}
        </div>
        <div className="agent-info">
          <div className="agent-props-top-line">
            <span className="agent-name">
              {name} {surname}
            </span>
            <span>{verticalDots}</span>
          </div>
          <div className="agent-props-middle-line">
            <span>
              <AgentLocationDataOrigin
                featureProperties={featureProperties}
                dictionary={dictionary}
              />
            </span>
          </div>
          <div className="agent-props-bottom-line">
            <span>{ongoingMissions.length} ongoing Missions</span>
            <span className="agent-lastUpdate">{localizeDate(lastModified, locale, 'L LT')}</span>
          </div>
        </div>
      </div>
    );
  }
}

const iconStyle = {
  fontSize: 18,
  padding: 0,
  width: 0,
  height: 20
};

const AgentLocationDataOrigin = ({ featureProperties, dictionary }) => {
  const { dataOrigin, wearableInfo } = featureProperties;
  let component = <div />;
  if (dataOrigin === 'wearable') {
    const { oxygenLevel, deviceStatus, fallDetection } = wearableInfo;
    const connectedColor = deviceStatus === 'off' ? redA700 : lightGreenA400;
    const hasOxygen = oxygenLevel && oxygenLevel.type;
    const oxygenColor =
      deviceStatus === 'off'
        ? 'grey'
        : hasOxygen
        ? oxygenLevel.type === 'low'
          ? redA700
          : lightGreenA400
        : '#fff';
    const fallDownColor =
      deviceStatus === 'off' ? 'grey' : fallDetection !== 'fallModerate' ? redA700 : lightGreenA400;

    component = (
      <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
        {dictionary._wearable}
        {hasOxygen && (
          <TollIcon style={{ width: 5 }} iconStyle={{ ...iconStyle, color: oxygenColor }} />
        )}
        {!hasOxygen && <span />}
        <AccessibilityIcon
          style={{ width: 5 }}
          iconStyle={{ ...iconStyle, color: fallDownColor }}
        />
        <PowerIcon style={{ width: 5 }} iconStyle={{ ...iconStyle, color: connectedColor }} />
      </div>
    );
  } else component = <div>{dictionary._mobile}</div>;

  return component;
};

export default withDictionary(AgentLocationCard);
