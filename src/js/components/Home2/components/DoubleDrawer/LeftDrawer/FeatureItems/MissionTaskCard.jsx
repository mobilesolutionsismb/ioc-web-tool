import './styles/miss_task_style.scss';
import React, { Component } from 'react';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { localizeDate } from 'js/utils/localizeDate';
import { withMissions } from 'ioc-api-interface';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { DeleteIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { TaskStatusIcon } from 'js/components/TaskDetailCard/commons';
import {
  MissionColorDot,
  CardContainer,
  InfoContainer,
  DetailsContainer,
  DescriptionContainer,
  DateStringContainer
} from 'js/components/MissionCard/commons';
import { withLoader, withMessages } from 'js/modules/ui';
import { drawerNames } from 'js/modules/AreaOfInterest';
import axios from 'axios';
import { capitalize } from 'js/utils/stringUtils';

const enhance = compose(
  withMissions,
  withLoader,
  withMessages
);

const MAX_TITLE_LENGTH_CARD = 35;

class MissionTaskCard extends Component {
  // _onDeleteButtonClick = async () => {
  //   if (!this.props.deleteSelectedTask) return;
  //   if (this.props.taskDescription)
  //     //when the task has no ID
  //     this.props.deleteSelectedTask(this.props.taskDescription);
  //   //task has to be deleted server-side
  //   else this.props.deleteSelectedTask(this.props.task.id);
  // };

  _onDeleteButtonClick = async () => {
    try {
      const mission = await this._loadMissionDetails();
      const taskId = this.props.featureProperties.id;
      const missionId = this.props.selectedMission.properties.id;

      if (taskId > 0 && missionId > 0) {
        if (mission.taskIds.length > 1) {
          await this.props.deleteTaskOfMission(
            taskId,
            missionId,
            this.props.loadingStart,
            this.props.loadingStop
          );
          this.props.pushMessage('_task_delete', 'success', null);
        } else {
          await this.props.deleteMission(
            missionId,
            this.props.loadingStart,
            this.props.loadingStop
          );
          this.props.pushMessage('_mission_delete', 'success', null);
        }

        this.props.deselectFeature();
      } else {
        this.props.pushError(new Error('Invalid Task/Mission Id'));
      }
    } catch (err) {
      this.props.loadingStop();
      this.props.pushError(err);
    }
  };

  _loadMissionDetails = async () => {
    if (this.props.missionFeaturesURL && this.props.selectedMission) {
      try {
        const response = await axios.get(this.props.missionFeaturesURL);
        const features =
          response.data.type === 'FeatureCollection' ? response.data.features : response.data;

        const mission = features.find(
          mission => mission.properties.id === this.props.selectedMission.properties.id
        );

        return mission ? mission.properties : {};
      } catch (err) {
        console.warn(`${this.props.itemType}: ${this.props.sourceURL} not valid`);
      }
    }
  };

  render() {
    const { featureProperties, dictionary, muiTheme, locale } = this.props;
    const { isCompleted, isTookOver } = featureProperties;
    const { palette } = muiTheme;
    const { temporalStatus, address, description, start, end } = featureProperties;

    const dateStartedString = start ? localizeDate(start, locale, 'L LT', true) : '';
    const dateFinishedString = end ? localizeDate(end, locale, 'L LT', true) : '';
    const taskDateString = `${dictionary(dateStartedString)}${
      featureProperties.end ? ' - ' + dictionary(dateFinishedString) : ''
    }`;

    const taskStatusIcon = <TaskStatusIcon isCompleted={isCompleted} isTookOver={isTookOver} />;

    const truncatedDescription =
      description && description.length > MAX_TITLE_LENGTH_CARD
        ? description.substring(0, MAX_TITLE_LENGTH_CARD) + '...'
        : description;

    return (
      <CardContainer palette={palette}>
        <InfoContainer>
          <DescriptionContainer className="task-description">
            <h3>{truncatedDescription}</h3>
            <Link to={`/home/${drawerNames.mission}`}>
              <DeleteIcon
                iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
                tooltip={dictionary._delete_task}
                onClick={this._onDeleteButtonClick}
              />
            </Link>
          </DescriptionContainer>
          <DetailsContainer className="task-details">
            <div>{address}</div>
            <DateStringContainer>
              <div style={{ display: 'flex' }}>
                {taskDateString}
                <MissionColorDot temporalStatus={temporalStatus}>
                  <span className="tooltiptext">
                    {dictionary(`_mission_filter_is${capitalize(temporalStatus)}`)}
                  </span>
                </MissionColorDot>
              </div>
              {taskStatusIcon}
            </DateStringContainer>
          </DetailsContainer>
        </InfoContainer>
      </CardContainer>
    );
  }
}

export default muiThemeable()(enhance(MissionTaskCard));
