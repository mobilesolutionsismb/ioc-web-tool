import './styles/mission_style.scss';
import React, { Component } from 'react';
import { LinearProgress } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {
  MissionColorDot,
  CardContainer,
  InfoContainer,
  DetailsContainer,
  BarContainer,
  DescriptionContainer,
  DateStringContainer
} from 'js/components/MissionCard/commons';
import { DeleteIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { localizeDate } from 'js/utils/localizeDate';
import { compose } from 'redux';
import { withMissions } from 'ioc-api-interface';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withLoader, withMessages } from 'js/modules/ui';
import { capitalize } from 'js/utils/stringUtils';

const enhance = compose(
  withMissions,
  withLeftDrawer,
  withLoader,
  withMessages
);

const ProgressContainer = props => (
  <div className="mission-progress">
    <LinearProgress
      className="progress-bar"
      color={props.palette.missionProgressBarColor}
      style={{ margin: '0px 0px auto 0px' }}
      mode="determinate"
      value={props.indicatorValue}
    />
  </div>
);

class MissionCard extends Component {
  _onDeleteButtonClick = async e => {
    try {
      e.stopPropagation();
      await this.props.deleteMission(
        this.props.featureProperties.id,
        this.props.loadingStart,
        this.props.loadingStop
      );
      this.props.pushMessage('_mission_delete', 'success', null);
    } catch (err) {
      this.props.loadingStop();
      this.props.pushError(err);
    }
  };

  render() {
    const { featureProperties, dictionary, locale, muiTheme } = this.props;
    const { palette } = muiTheme;

    if (featureProperties) {
      const {
        temporalStatus,
        numOfCompletedTask,
        taskIds,
        start,
        end,
        title,
        address
      } = featureProperties;

      const percentCompleted = (numOfCompletedTask / taskIds.length) * 100;

      const dateStartedString = start ? localizeDate(start, locale, 'L LT', true) : '';
      const dateFinishedString = end ? localizeDate(end, locale, 'L LT', true) : '';
      const missionDateString = dateStartedString + ' - ' + dateFinishedString;

      return (
        <CardContainer className="mission-card" palette={palette}>
          <InfoContainer className="mission-info">
            <BarContainer className="mission-bar">
              <span>{`${taskIds.length} TASKS`}</span>
              <ProgressContainer palette={palette} indicatorValue={percentCompleted} />
              <div>
                <DeleteIcon
                  style={{
                    width: 40,
                    paddingRight: 0
                  }}
                  iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
                  tooltip={dictionary._abort_mission}
                  onClick={this._onDeleteButtonClick}
                />
              </div>
            </BarContainer>
            <DescriptionContainer className="mission-description">
              <div className="mission-description-title">{title}</div>
            </DescriptionContainer>
            <DetailsContainer className="mission-details">
              <div>{address}</div>
              <DateStringContainer>
                <div>{missionDateString}</div>
                <MissionColorDot temporalStatus={temporalStatus}>
                  <span className="tooltiptext">
                    {dictionary(`_mission_filter_is${capitalize(temporalStatus)}`)}
                  </span>
                </MissionColorDot>
              </DateStringContainer>
            </DetailsContainer>
          </InfoContainer>
        </CardContainer>
      );
    } else {
      return <CardContainer className="mission-card" palette={palette} />;
    }
  }
}

export default muiThemeable()(enhance(MissionCard));
