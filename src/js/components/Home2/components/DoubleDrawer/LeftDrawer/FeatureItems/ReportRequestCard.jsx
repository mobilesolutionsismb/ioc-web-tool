import './styles/rep_req_style.scss';
import React, { Component } from 'react';
import { MenuItem, IconButton, IconMenu } from 'material-ui';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import {
  ReportRequestMeasureLine,
  ReportRequestColorDot
} from 'js/components/ReportRequestCard/commons';
import { ShareIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { localizeDate } from 'js/utils/localizeDate';
import { compose } from 'redux';
import { withReportNewRequest } from 'js/modules/reportNewRequest';
import { drawerNames, withAreaOfInterest } from 'js/modules/AreaOfInterest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withReportFilters } from 'js/modules/reportFilters';
import { Link } from 'react-router-dom';
const enhance = compose(
  withReportNewRequest,
  withAreaOfInterest,
  withLeftDrawer,
  withReportFilters
);

class ReportRequestCard extends Component {
  static defaultProps = {
    showDetailIcon: true
  };

  updateReportRequest = event => {
    const filterCategory = 'reportType';
    // if (event.currentTarget.id)
    //   this.props.selectFeature({ properties: this.props.featureProperties });
    this.props.removeReportFilter(filterCategory, '_is_type_damage');
    this.props.removeReportFilter(filterCategory, '_is_type_measure');
    this.props.removeReportFilter(filterCategory, '_is_type_people');
    this.props.removeReportFilter(filterCategory, '_is_type_resource');

    if (this.props.featureProperties.hasDamage)
      this.props.addReportFilter(filterCategory, '_is_type_damage');
    if (this.props.featureProperties.hasMeasure)
      this.props.addReportFilter(filterCategory, '_is_type_measure');
    if (this.props.featureProperties.hasPeople)
      this.props.addReportFilter(filterCategory, '_is_type_people');
    if (this.props.featureProperties.hasResource)
      this.props.addReportFilter(filterCategory, '_is_type_resource');

    this.props.fillReportRequest(this.props.featureProperties);
    this.props.setOpenSecondary(false);
  };

  render() {
    const {
      featureProperties,
      dictionary,
      locale,
      onShareButtonClick,
      showDetailIcon,
      style
    } = this.props;

    const verticalDots = (
      <IconMenu
        iconButtonElement={
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        }
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
        useLayerForClickAway={true}
        onClick={event => {
          event.stopPropagation();
        }}
      >
        <Link to={`/home/${drawerNames.report}?${drawerNames.reportNewRequest}`}>
          <MenuItem
            id={featureProperties.id}
            primaryText="Edit End Date"
            onClick={this.updateReportRequest}
          />
        </Link>
      </IconMenu>
    );
    let contentType = featureProperties.contentType
      ? featureProperties.contentType.split(',').map(item => item.trim())
      : [];
    const reportRequestMeasureLine = (
      <ReportRequestMeasureLine
        damages={contentType.indexOf('damage') >= 0}
        resources={contentType.indexOf('resource') >= 0}
        people={contentType.indexOf('people') >= 0}
        measures={featureProperties.measures}
        dictionary={dictionary}
      />
    );

    return (
      <div className="reportRequestCard" style={style}>
        <div className="reportRequestInfo">
          <div className="reportRequestBar">{reportRequestMeasureLine}</div>
          <div className="reportRequestDetails">
            <div style={{ marginBottom: 8 }}>{featureProperties.address}</div>
            <div style={{ display: 'flex' }}>
              <div style={{ marginRight: 10 }}>
                {localizeDate(featureProperties.start, locale, 'L LT', true)}
                {' - '}
                {featureProperties.end
                  ? localizeDate(featureProperties.end, locale, 'L LT', true)
                  : ''}
              </div>
              <ReportRequestColorDot status={featureProperties.isOngoing} />
            </div>
          </div>
        </div>
        <div className="reportRequestActions">
          <ShareIcon
            style={{ marginBottom: 20 }}
            iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
            tooltip="Share"
            onClick={onShareButtonClick}
          />
          {showDetailIcon && verticalDots}
        </div>
      </div>
    );
  }
}

export default enhance(ReportRequestCard);
