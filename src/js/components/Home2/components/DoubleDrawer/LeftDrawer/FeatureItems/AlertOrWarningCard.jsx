import React /* , { Component } */ from 'react';
import { EmCommChip, EmCommLanguage, Level } from 'js/components/EmergencyCommunicationsCommons';
import { localizeDate } from 'js/utils/localizeDate';
import { ReportRequestColorDot } from 'js/components/ReportRequestCard/commons';

const AlertOrWarningCard = ({ emComm, dictionary, locale, palette }) => {
  const {
    end,
    start,
    address,
    warningLevel,
    type,
    capStatus,
    organization,
    sourceOfInformation
  } = emComm;
  const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
  const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
  const emcommDatesString = `${dictionary(dateStartedString)}${
    end ? ' - ' + dictionary(dateFinishedString) : ''
  }`;
  return (
    <div className={`emcomm-card__content ${type}`}>
      <div className="emcomm-card__info">
        <EmCommChip dictionary={dictionary} palette={palette} emComm={emComm} />
        {emComm.language && <EmCommLanguage language={emComm.language} />}
      </div>
      <div className="emcomm-card__title">
        {/* <span className="emcomm-card__emcomm-type">{dictionary(`_emcomm_${type}`)}</span> */}
        <span className="emcomm-card__emctype">
          {dictionary(`_emcomm_${type}`).toLocaleUpperCase()}
          {capStatus !== 'actual' && '/'}
          {capStatus !== 'actual' && dictionary(`_emd_cap_stat_${capStatus}`)}
        </span>
        <Level
          dictionary={dictionary}
          isLarge={false}
          palette={palette}
          type={type}
          level={warningLevel}
        />
      </div>
      <div className="emcomm-card__address">{address}</div>
      <div className="emcomm-card__dates" style={{ display: 'flex', alignItems: 'center' }}>
        <div style={{ marginRight: 5 }}>{emcommDatesString}</div>
        <ReportRequestColorDot status={emComm.isOngoing} />
      </div>
      <div className="emcomm-card__organization">
        <span>{sourceOfInformation || organization || 'EMDAT'}</span>
      </div>
    </div>
  );
};

export default AlertOrWarningCard;
