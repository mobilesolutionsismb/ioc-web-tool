import './styles/map_request_style.scss';
import React, { Component } from 'react';
import { API, withMapRequests, withAPISettings } from 'ioc-api-interface';
import { localizeDate, getDurationFormat } from 'js/utils/localizeDate';
import {
  Chip,
  Avatar,
  // Toggle,
  IconButton,
  LinearProgress,
  Popover,
  Menu,
  MenuItem
} from 'material-ui';
import { compose } from 'redux';
import { HAZARD_TYPES_LETTERS } from 'js/components/ReportsCommons';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withMessages } from 'js/modules/ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
const api = API.getInstance();
const enhance = compose(
  withMessages,
  withLeftDrawer,
  withMapRequests,
  withAPISettings
);
const MapRequestColorDot = muiThemeable()(({ status, muiTheme }) => (
  <div
    className={`status-map-request status-${status}`}
    style={{
      background:
        status === 'ongoing'
          ? muiTheme.palette.ongoingReportRequestColor
          : muiTheme.palette.closedReportRequestColor
    }}
  />
));
class MapRequestCard extends Component {
  state = {
    open: false,
    anchorEl: null,
    toggled: false
  };
  componentDidMount() {
    if (this.props.featureProperties.status === 'contentAvailable') {
      this.setState({ toggled: true });
    }
  }

  _reimportMapReq = async (e, id) => {
    try {
      await api.mapRequest.reImport({ id: id });
      await this.props.updateMapRequests();
      this.setState({ open: false });
    } catch (e) {
      this.props.pushMessage('Reimport Map error', 'error', null);
    }
  };
  _applyTimeWindow = async () => {
    await this.props.setLiveTimeWindowWithDates(
      this.props.featureProperties.start,
      this.props.featureProperties.end,
      this.props.loadingStart,
      this.props.loadingStop
    );
    this.setState({ open: false });
  };

  _deleteMapReq = async (e, id) => {
    try {
      await api.mapRequest.deleteMapReq(id);
      await this.props.updateMapRequests(true);
      this.setState({ open: false });
    } catch (e) {
      this.props.pushMessage('Delete Map card error', 'error', null);
    }
  };

  openPopover = e => {
    e.stopPropagation();
    this.setState({
      open: true,
      anchorEl: e.currentTarget
    });
  };
  handleRequestClose = () => {
    this.setState({ open: false });
  };

  toggle = e => {
    e.stopPropagation();
    console.log(
      `map request with id ${this.props.featureProperties.id} toggle change to: ${!this.state
        .toggled}`
    );
    this.setState({
      toggled: !this.state.toggled
    });
  };

  render() {
    const mapReq = this.props.featureProperties;
    const dateStartedString = mapReq.start
      ? localizeDate(mapReq.start, this.props.locale, 'L LT', true)
      : '';
    const dateFinishedString = mapReq.end
      ? localizeDate(mapReq.end, this.props.locale, 'L LT', true)
      : '';

    const palette = this.props.muiTheme.palette;

    const displayName = mapReq.layerType.toUpperCase();
    const isOpen = typeof mapReq.end === 'undefined' || mapReq.end === null;
    let dur;
    if (mapReq.status === 'ContentAvailable') {
      let dateEnd = new Date(mapReq.layerExpirationTime).getTime();
      let today = new Date().getTime();
      const duration = dateEnd - today;
      dur = duration > 0 ? getDurationFormat(duration) : '';
    }

    const displaLayerCounter = mapReq && mapReq.layerCounter > 0 ? mapReq.layerCounter : '';

    return (
      <div className={'mapRequest-card'}>
        <div className="mapRequestButton">
          <div>{dur}</div>
          <IconButton onClick={this.openPopover}>
            <i className="material-icons md-18 md-light eventCardIcon">more_vert</i>
          </IconButton>
        </div>

        <div style={{ display: 'flex' }}>
          <div style={{ padding: 16, width: '100%' }}>
            <Chip style={{ height: 24 }}>
              <Avatar
                className="ireact-pinpoint-icons"
                size={24}
                style={{ height: 24, width: 24 }}
                color={isOpen ? palette.textColor : palette.canvasColor}
                backgroundColor={isOpen ? palette.ongoingEventsColor : palette.closedEventsColor}
              >
                {HAZARD_TYPES_LETTERS[mapReq.hazard]}
              </Avatar>
              <div className="mapRequestCardHazzard">
                <span>{this.props.dictionary['_hazard_' + mapReq.hazard]}</span>
              </div>
            </Chip>
            <div className="mapRequestCardTitle">
              <span>{displayName}</span>
              <span style={{ fontSize: 25, color: 'lightgray' }}>{displaLayerCounter}</span>
            </div>
            {(mapReq.status === 'requestSubmitted' || mapReq.status === 'reimportRequested') &&
              mapReq.temporalStatus !== 'expired' && (
                <div className="mapRequestLoader">
                  <LinearProgress mode="indeterminate" />
                </div>
              )}
            <div className="mapRequestCardDates">
              <div style={{ marginRight: 10 }}>
                {dateStartedString} - {dateFinishedString}
              </div>

              <MapRequestColorDot status={mapReq.temporalStatus} />
              {/* {(mapReq.status === 'contentAvailable' ||
                mapReq.status === 'contentNotAvailable') && (
                <Toggle
                  style={{
                    width: 40
                  }}
                  disabled={mapReq.status !== 'contentAvailable' ? false : true}
                  toggled={this.state.toggled}
                  onToggle={this.toggle}
                />
              )} */}
            </div>
          </div>
        </div>
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'right', vertical: 'center' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}
        >
          <Menu>
            <MenuItem primaryText="Apply timewindow" onClick={this._applyTimeWindow} />
            {mapReq.status === 'contentNotAvailable' && (
              <MenuItem
                primaryText="Re-import maps"
                onClick={event => {
                  this._reimportMapReq(event, mapReq.id);
                }}
              />
            )}
            {(mapReq.status === 'contentNotAvailable' || mapReq.status === 'ContentAvailable ') && (
              <MenuItem
                primaryText="Delete card"
                onClick={(event, id) => {
                  this._deleteMapReq(event, mapReq.id);
                }}
              />
            )}
          </Menu>
        </Popover>
      </div>
    );
  }
}

export default muiThemeable()(enhance(MapRequestCard));
/**
 * <p style={{ fontSize: '2em', color: 'gray' }}>{mapReq.frequency}</p>
              <div>
                <Toggle
                  onToggle={(event, isInputChecked) => {
                    this.toggle(event, isInputChecked, mapReq);
                  }}
                />
              </div>
 */
