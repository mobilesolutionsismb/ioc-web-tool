import React, { Component } from 'react';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import LeftDrawerFilterHeader from 'js/components/app/drawer/LeftDrawerFilterHeader';
import StatusHeaderFilter from 'js/components/app/drawer/StatusHeaderFilter';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { compose } from 'redux';
const enhance = compose(withLeftDrawer);

const CHECKBOXES_CONFIG = [
  {
    label: 'Only Mobile',
    type: 'agentLocationDataOrigin',
    value: '_is_data_origin_mobile',
    paletteColor: 'authorityColor'
  },
  {
    label: 'Wearable',
    type: 'agentLocationDataOrigin',
    value: '_is_data_origin_wearable',
    paletteColor: 'citizensColor'
  },
  {
    label: 'Critical',
    type: 'agentLocationCritical',
    value: '_is_ag_location_critical',
    paletteColor: 'validatedColor'
  }
];

class AgentLocationsHeader extends Component {
  static defaultProps = {
    mapRequests: [],
    featuresCount: 0
  };

  render() {
    const { dictionary, featuresCount, refreshItems, loading } = this.props;

    let statusDrawer = null;

    let titleDrawer = (
      <DrawerTitle
        key="title"
        mapLayerFilterName="_is_layer_agloc_visible"
        search={this.props.search}
        category={drawerNames.agentLocations}
        title={dictionary._tab_header_agent_locations}
      />
    );

    const filterHeader = (
      <LeftDrawerFilterHeader
        key="filters"
        checkboxes={CHECKBOXES_CONFIG}
        headerType="agentLocation"
        deselectFeature={this.props.deselectFeature}
        refreshItems={refreshItems}
        loading={loading}
      />
    );

    statusDrawer = (
      <StatusHeaderFilter
        key="status"
        numberOfResults={featuresCount}
        nameOfResults={dictionary._results}
        headerType="agentLocation"
        headerTypeSorter="agentLocation"
      />
    );

    return [titleDrawer, filterHeader, statusDrawer];
  }
}

export default enhance(AgentLocationsHeader);
