import React, { Component } from 'react';
import LeftDrawerFilterHeader from 'js/components/app/drawer/LeftDrawerFilterHeader';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import AddActionButton from 'js/components/app/drawer/AddActionButton';
import StatusHeaderFilter from 'js/components/app/drawer/StatusHeaderFilter';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { compose } from 'redux';
const enhance = compose(withLeftDrawer);

const CHECKBOXES_CONFIG = [
  {
    label: 'Ongoing',
    type: 'temporalStatus',
    value: '_is_status_ongoing',
    paletteColor: 'ongoingReportRequestColor'
  },
  {
    label: 'Closed',
    type: 'temporalStatus',
    value: '_is_status_closed',
    paletteColor: 'closedReportRequestColor'
  }
];

class MapRequestsHeader extends Component {
  static defaultProps = {
    mapRequests: [],
    featuresCount: 0
  };

  toggleSecondDrawer = () => {
    this.props.setOpenSecondary(!this.props.isSecondaryOpen, drawerNames.mapRequestFilter);
  };

  createMapRequest = () => {
    this.props.setOpenSecondary(false);
    this.props.history.replace(`/home/${drawerNames.mapRequests}?map_request_create`);
  };

  render() {
    const { dictionary, featuresCount, refreshItems, loading } = this.props;

    let statusDrawer = null,
      addActionButton = null;

    let titleDrawer = (
      <DrawerTitle
        key="title"
        mapLayerFilterName="_is_layer_maprequest_visible"
        search={this.props.search}
        category={drawerNames.mapRequests}
        title={dictionary._tab_header_map_requests}
        rightIcon="filter_list"
        rightAction={this.toggleSecondDrawer}
      />
    );
    let filterHeader = (
      <LeftDrawerFilterHeader
        key="filters"
        checkboxes={CHECKBOXES_CONFIG}
        headerType="mapRequest"
        deselectFeature={this.props.deselectFeature}
        refreshItems={refreshItems}
        loading={loading}
      />
    );

    statusDrawer = (
      <StatusHeaderFilter
        key="status"
        numberOfResults={featuresCount}
        nameOfResults={dictionary._results}
        headerType="mapRequest"
      />
    );
    addActionButton = <AddActionButton key="addActionButton" addAction={this.createMapRequest} />;
    return [titleDrawer, filterHeader, statusDrawer, addActionButton];
  }
}

export default enhance(MapRequestsHeader);
