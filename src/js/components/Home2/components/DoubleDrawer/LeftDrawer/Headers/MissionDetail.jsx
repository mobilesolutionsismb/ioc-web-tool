import React, { Component } from 'react';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withMissions } from 'ioc-api-interface';
import { withAreaOfInterest, drawerNames } from 'js/modules/AreaOfInterest';
import { MissionDetailHeader } from 'js/components/Left/Missions/commons';
import { compose } from 'redux';
import { MissionColorDot } from 'js/components/MissionCard/commons';
import { withMessages, withLoader } from 'js/modules/ui';
import axios from 'axios';
import DrawerFooter from 'js/components/app/drawer/DrawerFooter';
import { capitalize } from 'js/utils/stringUtils';

const enhance = compose(
  withMissions,
  withLeftDrawer,
  withAreaOfInterest,
  withMessages,
  withLoader
);

class MissionDetailsHeader extends Component {
  state = {
    missionDetail: {}
  };

  leftAction = () => {
    this.props.history.replace(`/home/${drawerNames.mission}`);
  };

  rightAction = () => {
    this.props.setOpenPrimary(false);
    this.props.history.replace('/home/map');
  };

  _abortMission = async () => {
    try {
      await this.props.deleteMission(
        this.props.selectedMission.properties.id,
        this.props.loadingStart,
        this.props.loadingStop
      );
      this.props.pushMessage('_mission_delete', 'success', null);
      this.props.history.replace(`/home/${drawerNames.mission}`);
      this.props.deselectMissionTask();
      this.props.deselectFeature();
    } catch (err) {
      this.props.loadingStop();
      this.props.pushError(err);
    }
  };

  _init = () => {
    if (!this.props.missionFeaturesURL) {
      this.props.history.replace(`/home/${drawerNames.mission}`);
    }

    /* const missionDetail = await */ this._loadMissionDetails();
    // this.setState({
    //   missionDetail
    // });
  };

  componentDidMount() {
    this._init();
  }

  _loadMissionDetails = async () => {
    if (this.props.missionFeaturesURL && this.props.selectedMission) {
      try {
        const response = await axios.get(this.props.missionFeaturesURL);
        const features =
          response.data.type === 'FeatureCollection' ? response.data.features : response.data;

        const mission = features.find(
          mission => mission.properties.id === this.props.selectedMission.properties.id
        );

        //return mission ? mission.properties : {};
        this.setState({ missionDetail: mission ? mission.properties : {} });
      } catch (err) {
        console.warn(`${this.props.itemType}: ${this.props.sourceURL} not valid`);
      }
    }
  };

  render() {
    const { dictionary, locale } = this.props;
    let titleDrawer = null;

    const missionProperties = this.state.missionDetail;

    const status = missionProperties.isCompleted
      ? 'isCompleted'
      : missionProperties.isExpired
      ? 'isExpired'
      : missionProperties.isOngoing
      ? 'isOngoing'
      : missionProperties.IsOpen
      ? 'isOpen'
      : '';

    const numberOfOngoingTask = missionProperties.numOfOngoingTask;
    const numOfTask = missionProperties.taskIds ? missionProperties.taskIds.length : 0;
    titleDrawer = (
      <DrawerTitle
        key="title"
        search={this.props.search}
        title={dictionary._tab_header_mission_detail}
        leftIcon="keyboard_backspace"
        rightIcon="close"
        rightAction={this.rightAction}
        leftAction={this.leftAction}
      />
    );

    const missionDetailHeader = (
      <MissionDetailHeader
        key="details"
        missionProperties={missionProperties}
        dictionary={dictionary}
        locale={locale}
      />
    );

    const taskHeader = (
      <div
        key="task-header"
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          margin: 10
        }}
      >
        <span style={{ fontWeight: 'bold' }}>{numOfTask} TASKS</span>
        <span style={{ display: 'flex', alignItems: 'center' }}>
          <MissionColorDot status={status}>
            <span className="tooltiptext">
              {dictionary(`_mission_filter_is${capitalize(status.replace(/^IS/i, ''))}`)}
            </span>
          </MissionColorDot>
          <span style={{ paddingLeft: 5 }}>{numberOfOngoingTask} ongoing</span>
        </span>
      </div>
    );

    const footerDrawer = (
      <DrawerFooter
        key="drawer-footer"
        category={this.props.primaryDrawerType}
        leftLabel={dictionary.abort}
        clear={this._abortMission}
        entityId={missionProperties.id}
        customStyle={{ backgroundColor: 'black' }}
      />
    );

    return [titleDrawer, missionDetailHeader, taskHeader, footerDrawer];
  }
}

export default enhance(MissionDetailsHeader);
