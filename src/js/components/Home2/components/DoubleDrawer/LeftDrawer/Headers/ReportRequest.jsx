import React, { Component } from 'react';
import LeftDrawerFilterHeader from 'js/components/app/drawer/LeftDrawerFilterHeader';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import StatusHeaderFilter from 'js/components/app/drawer/StatusHeaderFilter';
import AddActionButton from 'js/components/app/drawer/AddActionButton';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withReportNewRequest } from 'js/modules/reportNewRequest';
import { withReportRequests } from 'ioc-api-interface';
import { withReportFilters } from 'js/modules/reportFilters';

import { compose } from 'redux';
const enhance = compose(
  withLeftDrawer,
  withReportRequests,
  withReportNewRequest,
  withReportFilters
);

const CHECKBOXES_CONFIG = [
  {
    label: 'Ongoing',
    type: 'reportRequestStatus',
    value: '_is_status_ongoing',
    paletteColor: 'ongoingReportRequestColor'
  },
  {
    label: 'Closed',
    type: 'reportRequestStatus',
    value: '_is_status_closed',
    paletteColor: 'closedReportRequestColor'
  }
];

class ReportRequestsHeader extends Component {
  toggleSecondDrawer = () => {
    this.props.setOpenSecondary(!this.props.isSecondaryOpen, drawerNames.reportRequestFilter);
  };

  selectFirstDrawerClick = () => {
    const isReportRequestActive = this.props.search === '?reportRequest';
    this.props.setOpenSecondary(false);
    this.props.history.replace(
      `/home/${drawerNames.report}${!isReportRequestActive ? '?reportRequest' : ''}`
    );
  };

  createReportRequest = () => {
    this.props.setOpenSecondary(false);
    this.props.resetReportNewRequest();
    this.props.deselectFeature();

    const filterCategory = 'reportType';
    this.props.removeReportFilter(filterCategory, '_is_type_damage');
    this.props.removeReportFilter(filterCategory, '_is_type_measure');
    this.props.removeReportFilter(filterCategory, '_is_type_people');
    this.props.removeReportFilter(filterCategory, '_is_type_resource');

    this.props.history.replace(`/home/${drawerNames.report}?${drawerNames.reportNewRequest}`);
  };

  render() {
    const { dictionary, featuresCount, refreshItems, loading } = this.props;

    let statusDrawer = null,
      filterHeader = null,
      addActionButton = null;

    let titleDrawer = (
      <DrawerTitle
        key="title"
        mapLayerFilterName="_is_layer_rr_visible"
        category={drawerNames.reportRequest}
        title={[dictionary._tab_header_reports, dictionary._tab_header_reports_requests]}
        drawerNames={[drawerNames.report, drawerNames.reportRequest]}
        multiple={true}
        onSelectClick={this.selectFirstDrawerClick}
        rightIcon="filter_list"
        secondDrawerName={drawerNames.reportRequestFilter}
        rightAction={this.toggleSecondDrawer}
      />
    );

    addActionButton = (
      <AddActionButton key="addActionButton" addAction={this.createReportRequest} />
    );
    filterHeader = (
      <LeftDrawerFilterHeader
        key="filters"
        checkboxes={CHECKBOXES_CONFIG}
        headerType="reportRequest"
        deselectFeature={this.props.deselectFeature}
        refreshItems={refreshItems}
        loading={loading}
      />
    );

    statusDrawer = (
      <StatusHeaderFilter
        key="status"
        numberOfResults={featuresCount}
        nameOfResults={dictionary._drawer_label_reports_requests}
        headerType="reportRequest"
        orderBy={[
          { label: 'Start date Asc', value: 'dateTimeAsc' },
          { label: 'Start date Desc', value: 'dataTimeDesc' }
        ]}
      />
    );

    return [titleDrawer, filterHeader, statusDrawer, addActionButton];
  }
}

export default enhance(ReportRequestsHeader);
