import React, { Component } from 'react';
import LeftDrawerFilterHeader from 'js/components/app/drawer/LeftDrawerFilterHeader';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import StatusHeaderFilter from 'js/components/app/drawer/StatusHeaderFilter';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { compose } from 'redux';
const enhance = compose(withLeftDrawer);

const CHECKBOXES_CONFIG = [
  {
    label: 'Authorities',
    type: 'userType',
    value: '_is_authority',
    paletteColor: 'authorityColor'
  },
  { label: 'Citizens', type: 'userType', value: '_is_citizen', paletteColor: 'citizensColor' },
  {
    label: 'Validated',
    type: 'status',
    value: '_is_status_validated',
    paletteColor: 'validatedColor'
  }
];

class ReportsHeader extends Component {
  static defaultProps = {
    reports: [],
    featuresCount: 0
  };

  toggleSecondDrawer = () => {
    if (!this.props.isRrActive)
      this.props.setOpenSecondary(!this.props.isSecondaryOpen, drawerNames.reportFilter);
  };

  selectFirstDrawerClick = () => {
    const isReportRequestActive = this.props.search === '?reportRequest';
    this.props.setOpenSecondary(false);
    this.props.history.replace(
      `/home/${drawerNames.report}${!isReportRequestActive ? '?reportRequest' : ''}`
    );
  };

  render() {
    const { dictionary, featuresCount, refreshItems, loading } = this.props;

    const titleDrawer = (
      <DrawerTitle
        key="title"
        category={drawerNames.report}
        title={[dictionary._tab_header_reports, dictionary._tab_header_reports_requests]}
        drawerNames={[drawerNames.report, drawerNames.reportRequest]}
        multiple={true}
        onSelectClick={this.selectFirstDrawerClick}
        rightIcon="filter_list"
        secondDrawerName={drawerNames.reportFilter}
        rightAction={this.toggleSecondDrawer}
        mapLayerFilterName="_is_layer_report_visible"
      />
    );
    const filterHeader = (
      <LeftDrawerFilterHeader
        key="filters"
        checkboxes={CHECKBOXES_CONFIG}
        headerType="report"
        deselectFeature={this.props.deselectFeature}
        refreshItems={refreshItems}
        loading={loading}
      />
    );

    const statusDrawer = (
      <StatusHeaderFilter
        key="status"
        refreshItems={this.props.getReports}
        numberOfResults={featuresCount}
        nameOfResults={dictionary._drawer_label_reports}
        headerType="report"
        headerTypeSorter="sorter"
      />
    );

    return [titleDrawer, filterHeader, statusDrawer];
  }
}

export default enhance(ReportsHeader);
