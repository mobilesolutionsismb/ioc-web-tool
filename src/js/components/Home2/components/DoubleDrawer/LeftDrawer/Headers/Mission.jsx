import React, { Component } from 'react';
import LeftDrawerFilterHeader from 'js/components/app/drawer/LeftDrawerFilterHeader';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import StatusHeaderFilter from 'js/components/app/drawer/StatusHeaderFilter';
import AddActionButton from 'js/components/app/drawer/AddActionButton';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { compose } from 'redux';
const enhance = compose(withLeftDrawer);

const CHECKBOXES_CONFIG = [
  {
    label: 'Open',
    type: 'missionStatus',
    value: '_is_status_open',
    paletteColor: 'openMissionColor'
  },
  {
    label: 'Ongoing',
    type: 'missionStatus',
    value: '_is_status_ongoing',
    paletteColor: 'ongoingMissionColor'
  },
  {
    label: 'Completed',
    type: 'missionStatus',
    value: '_is_status_completed',
    paletteColor: 'completedMissionColor'
  },
  {
    label: 'Expired',
    type: 'missionStatus',
    value: '_is_status_expired',
    paletteColor: 'expiredMissionColor'
  }
];

class MissionsHeader extends Component {
  createMission = () => {
    this.props.setOpenSecondary(false);
    this.props.deselectFeature();
    this.props.history.replace(`/home/${drawerNames.mission}?${drawerNames.missionCreate}`);
  };

  render() {
    const { dictionary, featuresCount, refreshItems, loading } = this.props;
    let titleDrawer = null,
      statusDrawer = null,
      filterHeader = null;

    titleDrawer = (
      <DrawerTitle
        key="title"
        mapLayerFilterName="_is_layer_mission_visible"
        category={this.props.primaryDrawerType}
        title={dictionary._tab_header_missions}
      />
    );
    filterHeader = (
      <LeftDrawerFilterHeader
        key="filters"
        checkboxes={CHECKBOXES_CONFIG}
        headerType="mission"
        deselectFeature={this.props.deselectFeature}
        refreshItems={refreshItems}
        loading={loading}
      />
    );

    statusDrawer = (
      <StatusHeaderFilter
        key="status"
        numberOfResults={featuresCount}
        nameOfResults={dictionary._drawer_label_missions}
        headerType="mission"
        headerTypeSorter="sorter"
      />
    );
    const addActionButton = (
      <AddActionButton key="addActionButton" addAction={this.createMission} />
    );

    return [titleDrawer, filterHeader, statusDrawer, addActionButton];
  }
}

export default enhance(MissionsHeader);
