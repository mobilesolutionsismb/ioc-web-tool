import React, { Component } from 'react';
import LeftDrawerFilterHeader from 'js/components/app/drawer/LeftDrawerFilterHeader';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import StatusHeaderFilter from 'js/components/app/drawer/StatusHeaderFilter';
import AddActionButton from 'js/components/app/drawer/AddActionButton';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { withEmCommNew } from 'js/modules/emCommNew';
import { compose } from 'redux';
const enhance = compose(
  withLeftDrawer,
  withEmCommNew
);

const CHECKBOXES_CONFIG = [
  { label: 'Warnings', type: 'emcommType', value: '_is_warning', paletteColor: 'authorityColor' },
  { label: 'Alert', type: 'emcommType', value: '_is_alert', paletteColor: 'citizensColor' }
];

class EmergencyCommunication extends Component {
  static defaultProps = {
    featuresCount: 0
  };

  toggleSecondDrawer = () => {
    this.props.setOpenSecondary(!this.props.isSecondaryOpen, drawerNames.notificationFilter);
  };

  createEmComm = () => {
    this.props.setOpenSecondary(false);
    this.props.resetEmComm();
    this.props.deselectFeature();
    this.props.history.replace(`/home/${drawerNames.notification}?${drawerNames.notificationNew}`);
  };

  render() {
    const { dictionary, featuresCount, refreshItems, loading } = this.props;

    let statusDrawer = null,
      addActionButton = null;

    let titleDrawer = (
      <DrawerTitle
        key="title"
        mapLayerFilterName="_is_layer_emcomm_visible"
        search={this.props.search}
        category={drawerNames.notification}
        title={dictionary._tab_header_notifications}
        rightIcon="filter_list"
        rightAction={this.toggleSecondDrawer}
      />
    );
    let filterHeader = (
      <LeftDrawerFilterHeader
        key="filters"
        checkboxes={CHECKBOXES_CONFIG}
        headerType="notifications"
        deselectFeature={this.props.deselectFeature}
        refreshItems={refreshItems}
        loading={loading}
      />
    );

    statusDrawer = (
      <StatusHeaderFilter
        key="status"
        numberOfResults={featuresCount}
        nameOfResults={dictionary._results}
        headerType="notifications"
      />
    );
    addActionButton = <AddActionButton addAction={this.createEmComm} key="actionButton" />;
    return [titleDrawer, filterHeader, statusDrawer, addActionButton];
  }
}

export default enhance(EmergencyCommunication);
