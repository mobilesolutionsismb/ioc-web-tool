import React, { Component } from 'react';
import ReportCard from './FeatureItems/ReportCard';
import EmergencyCommunicationCard from './FeatureItems/EmergencyCommunicationCard';
import ReportRequestCard from './FeatureItems/ReportRequestCard';
import MissionCard from './FeatureItems/MissionCard';
import MissionTaskCard from './FeatureItems/MissionTaskCard';
import MapRequestCard from './FeatureItems/MapRequestCard';
import AgentLocationCard from './FeatureItems/AgentLocationCard';

class FeatureListItem extends Component {
  render() {
    const { properties, dictionary, selectedMission, deselectFeature, locale } = this.props;
    let Component = null;
    switch (properties.itemType) {
      case 'report':
        Component = ReportCard;
        break;
      case 'emergencyCommunication':
        Component = EmergencyCommunicationCard;
        break;
      case 'reportRequest':
        Component = ReportRequestCard;
        break;
      case 'mission':
        Component = MissionCard;
        break;
      case 'missionTask':
        Component = MissionTaskCard;
        break;
      case 'mapRequest':
        Component = MapRequestCard;
        break;
      case 'agentLocation':
        Component = AgentLocationCard;
        break;
      default:
        break;
    }
    if (Component) {
      return (
        <Component
          featureProperties={properties}
          locale={locale}
          dictionary={dictionary}
          selectedMission={selectedMission}
          deselectFeature={deselectFeature}
        />
      );
    } else {
      return null;
    }
  }
}

export default FeatureListItem;
