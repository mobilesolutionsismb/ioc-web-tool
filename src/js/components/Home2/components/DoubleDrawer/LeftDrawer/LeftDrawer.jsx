import React, { Component } from 'react';
import { compose } from 'redux';
import { Drawer } from 'material-ui';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import GeoJSONItemsList from './GeoJSONItemsList';
import { leftDrawerStyle } from 'js/components/app/styles';
import DrawerHeader from './DrawerHeader';
import { GeoJSONFeaturesProvider } from 'js/components/GeoJSONFeaturesProvider';
import {
  withReports,
  withAlertsAndWarnings,
  withReportRequests,
  withMissions,
  withMapRequests,
  withAgentLocations
} from 'ioc-api-interface';
import { withReportFilters, SORTERS as REPORT_SORTERS } from 'js/modules/reportFilters';
import {
  withReportRequestFilters,
  SORTERS as REPORT_REQUEST_SORTER
} from 'js/modules/reportRequestFilters';
import { withEventFilters } from 'js/modules/clientSideEventFilters';
import { withEmergencyCommunicationFilters, SORTERS } from 'js/modules/emcommFilters';
import { withMissionFilters, SORTERS as MISSION_SORTERS } from 'js/modules/missionFilters';
import {
  withAgentLocationFilters,
  SORTERS as AGENT_LOCATION_SORTERS
} from 'js/modules/agentLocationFilters';
import {
  withMapRequestFilters,
  SORTERS as MAP_REQUEST_SORTERS
} from 'js/modules/mapRequestFilters';
import { featureFilter as ff } from '@mapbox/mapbox-gl-style-spec';

import NewReportRequestDrawerInner from './CreateFeatures/NewReportRequestDrawerInner';
import NewMapRequestDrawerInner from './CreateFeatures/NewMapRequestDrawerInner';
import NewEmergencyEventDrawerInner from './CreateFeatures/NewEmergencyEventDrawerInner';
import NewMissionDrawerInner from './CreateFeatures/NewMissionDrawerInner';
import EmergencyEventDrawerInner from './EmergencyEvents/EmergencyEventDrawerInner';
import NewEmergencyCommunicationDrawerInner from './CreateFeatures/NewEmergencyCommunicationDrawerInner';
import { logMain } from 'js/utils/log';

const enhance = compose(
  withLeftDrawer,
  withReports,
  withAlertsAndWarnings,
  withReportRequests,
  withMissions,
  withMapRequests,
  withReportFilters,
  withEmergencyCommunicationFilters,
  withReportRequestFilters,
  withMapRequestFilters,
  withMissionFilters,
  withEventFilters,
  withAgentLocations,
  withAgentLocationFilters
);

const PROVISIONAL_GEOJSON_CONTAINER_STYLE = { position: 'relative', width: '100%', height: '100%' };
class LeftDrawer extends Component {
  static defaultProps = {
    emergencyCommunicationFeaturesURL: null,
    missionFeaturesURL: null,
    eventFeaturesURL: null, //particular case
    reportFeaturesURL: null,
    reportRequestFeaturesURL: null,
    mapRequestFeaturesURL: null
  };

  _compareFeatures = (ft1, ft2) => {
    return ft1 && ft2
      ? ft1.properties.id === ft2.properties.id &&
          ft1.properties.itemType === ft2.properties.itemType
      : false;
  };

  render() {
    const {
      dictionary,
      dictIsEmpty,
      getMap,
      // enums,  location, user, roles,*/
      roles,
      location,
      history,
      isPrimaryOpen,
      primaryDrawerType,

      selectFeature,
      deselectFeature,
      selectedFeature,
      mouseEnterFeature,
      mouseLeaveFeature,
      hoveredFeature,

      // Loading state
      reportsUpdating,
      agentLocationsUpdating,
      reportRequestsUpdating,
      mapRequestsUpdating,
      missionsUpdating,
      alertsAndWarningsUpdating,

      //mission task mgmt
      selectedMissionTask,
      selectMissionTask,
      deselectMissionTask,
      hoveredMissionTask,
      mouseEnterMissionTask,
      mouseLeaveMissionTask,

      //report management
      selectedReport,
      selectReport,
      deselectReport,
      hoveredReport,
      mouseEnterReport,
      mouseLeaveReport,

      emergencyCommunicationFeaturesURL,
      missionFeaturesURL,
      missionTaskFeaturesURL,
      // eventFeaturesURL, //particular case - may be NOT an URL
      reportFeaturesURL,
      reportRequestFeaturesURL,
      mapRequestFeaturesURL,
      agentLocationFeaturesURL,

      // UPDATERS
      updateAlertsAndWarnings,
      updateReportRequests,
      updateReports,
      updateAgentLocations,
      updateMapRequests,
      updateMissions,

      //FILTERS
      reportFiltersDefinition,
      emcommFiltersDefinition,
      reportRequestFiltersDefinition,
      missionFiltersDefinition,
      mapRequestFiltersDefinition,
      agentLocationFiltersDefinition,
      //eventFiltersDefinition,

      //SORTERS
      emcommSorter,
      reportSorter,
      reportRequestSorter,
      missionSorter,
      agentLocationSorter,
      mapRequestSorter,

      //Emergency Event
      deselectEvent,

      //Draw Props
      getMapDraw,
      drawCategory,
      drawPoint,
      drawPolygon,
      drawTargetPoint,
      drawTargetPolygon,
      drawEventPoint,
      drawEventArea,
      resetHomeDrawState,
      setDrawEventPoint,
      setDrawEventArea,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon,
      setDrawTargetPoint,
      setDrawTargetPolygon,
      setIsDrawing,
      setDrawTask,

      locale,
      enums
    } = this.props;

    const drawProps = {
      getMapDraw,
      drawCategory,
      drawPoint,
      drawPolygon,
      drawTargetPoint,
      drawTargetPolygon,
      drawEventPoint,
      drawEventArea,
      resetHomeDrawState,
      setDrawEventPoint,
      setDrawEventArea,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon,
      setDrawTargetPoint,
      setDrawTargetPolygon,
      setIsDrawing,
      setDrawTask
    };

    let updateFeaturesFn = null; //Feature specific updater
    let filters = null;
    let sorter = null;
    let featureURL = null;
    let loading = false;
    let isGeoJSONFeatures = false;
    let itemType = null;
    let innerPrimaryDrawerType = null; // e.g. reports or reportRequests
    const search = location.search;

    switch (primaryDrawerType) {
      case 'reports':
        if (search === '?reportRequest') {
          // report request list
          featureURL = reportRequestFeaturesURL;
          innerPrimaryDrawerType = 'reportRequests';
          isGeoJSONFeatures = true;
          filters = ['==', 'itemType', 'reportRequest'];
          if (reportRequestFiltersDefinition) {
            reportRequestFiltersDefinition.push(filters);
            filters = ff(reportRequestFiltersDefinition).bind(null, null);
          }
          sorter = REPORT_REQUEST_SORTER[reportRequestSorter];
          itemType = 'reportRequest';
          updateFeaturesFn = updateReportRequests;
          loading = reportRequestsUpdating;
        } else if (search === '?report_new_request') {
          // new report req
          featureURL = null;
          innerPrimaryDrawerType = 'newReportRequest';
          isGeoJSONFeatures = false;
        } else {
          //  reports list
          featureURL = reportFeaturesURL;
          innerPrimaryDrawerType = 'reports';
          isGeoJSONFeatures = true;
          filters = reportFiltersDefinition ? ff(reportFiltersDefinition).bind(null, null) : null;
          sorter = REPORT_SORTERS[reportSorter];
          itemType = 'report';
          updateFeaturesFn = updateReports;
          loading = reportsUpdating;
        }
        break;
      case 'notifications':
        if (search === '?notification_new') {
          featureURL = null;
          innerPrimaryDrawerType = 'newEmergencyNotification';
          isGeoJSONFeatures = false;
        } else {
          //  emergency communication list
          featureURL = emergencyCommunicationFeaturesURL;
          innerPrimaryDrawerType = 'emergencyCommunications';
          isGeoJSONFeatures = true;
          filters = ['==', 'itemType', 'emergencyCommunication'];
          if (emcommFiltersDefinition) {
            emcommFiltersDefinition.push(filters);
            filters = ff(emcommFiltersDefinition).bind(null, null);
          }
          sorter = SORTERS[emcommSorter];
          itemType = 'emergencyCommunication';
          updateFeaturesFn = updateAlertsAndWarnings;
          loading = alertsAndWarningsUpdating;
        }
        break;
      case 'missions':
        if (search === '?mission_detail') {
          featureURL = missionTaskFeaturesURL;
          innerPrimaryDrawerType = 'mission_detail';
          isGeoJSONFeatures = true;
          filters = missionTask =>
            selectedFeature
              ? missionTask.properties.missionId === selectedFeature.properties.id
              : false;
          itemType = 'missionTask';
          loading = missionsUpdating;
        } else if (search === '?mission_create') {
          featureURL = null;
          innerPrimaryDrawerType = 'newMission';
          isGeoJSONFeatures = false;
        } else {
          featureURL = missionFeaturesURL;
          innerPrimaryDrawerType = 'missions';
          isGeoJSONFeatures = true;
          filters = ['==', 'itemType', 'mission'];
          if (missionFiltersDefinition) {
            missionFiltersDefinition.push(filters);
            filters = ff(missionFiltersDefinition).bind(null, null);
          }
          sorter = MISSION_SORTERS[missionSorter];
          itemType = 'mission';
          updateFeaturesFn = updateMissions;
          loading = missionsUpdating;
        }
        break;
      case 'map_requests':
        if (search === '?map_request_create') {
          featureURL = null;
          innerPrimaryDrawerType = 'newMapRequest';
          isGeoJSONFeatures = false;
        } else {
          featureURL = mapRequestFeaturesURL;
          innerPrimaryDrawerType = 'mapRequests';
          isGeoJSONFeatures = true;
          filters = ['==', 'itemType', 'mapRequest'];
          if (mapRequestFiltersDefinition) {
            mapRequestFiltersDefinition.push(filters);
            filters = ff(mapRequestFiltersDefinition).bind(null, null);
          }
          sorter = MAP_REQUEST_SORTERS[mapRequestSorter];
          itemType = 'mapRequest';
          updateFeaturesFn = updateMapRequests;
          loading = mapRequestsUpdating;
        }
        break;
      case 'agent_locations':
        featureURL = agentLocationFeaturesURL;
        innerPrimaryDrawerType = 'agentLocations';
        isGeoJSONFeatures = true;
        filters = ['==', 'itemType', 'agentLocation'];
        if (agentLocationFiltersDefinition) {
          agentLocationFiltersDefinition.push(filters);
          filters = ff(agentLocationFiltersDefinition).bind(null, null);
        }
        sorter = AGENT_LOCATION_SORTERS[agentLocationSorter];
        itemType = 'agentLocation';
        updateFeaturesFn = updateAgentLocations;
        loading = agentLocationsUpdating;
        break;
      case 'events':
        if (search === '?event_create') {
          featureURL = null;
          innerPrimaryDrawerType = 'newEmergencyEvent';
          isGeoJSONFeatures = false;
        } else {
          isGeoJSONFeatures = false;
          itemType = 'emergencyEvent';
          filters = null;
          innerPrimaryDrawerType = 'emergencyEvent';
        }
        break;
      default:
        break;
    }

    const headerProps = {
      history,
      search,
      dictionary,
      dictIsEmpty,
      deselectFeature,
      deselectEvent,
      deselectMissionTask
    };

    const isRrActive =
      selectedFeature != null && selectedFeature.properties.itemType === 'reportRequest';

    // console.log('SS', search, innerPrimaryDrawerType, isGeoJSONFeatures);
    return (
      <Drawer
        className="drawer"
        ref="primary"
        open={isPrimaryOpen}
        containerClassName="drawer-container"
        containerStyle={isPrimaryOpen ? leftDrawerStyle : { left: '-120px' }}
      >
        {/* All List Views except Events */}
        {isGeoJSONFeatures && (
          <GeoJSONFeaturesProvider
            className="features-provider"
            style={PROVISIONAL_GEOJSON_CONTAINER_STYLE}
            filters={filters}
            sorter={sorter}
            sourceURL={featureURL}
            drawcategory={drawCategory}
            drawpolygon={drawPolygon}
            itemType={itemType}
            getMap={getMap}
            selectedFeature={selectedFeature}
          >
            {(features, featuresUpdating, featuresUpdateError, filtering, sorting) => [
              // First item: header
              <DrawerHeader
                key="drawer-header"
                primaryDrawerType={innerPrimaryDrawerType}
                featuresCount={features.length}
                selectedMission={itemType === 'missionTask' ? selectedFeature : {}}
                isRrActive={isRrActive}
                loading={loading}
                refreshItems={() =>
                  updateFeaturesFn(
                    itemType !== 'emergencyCommunication' && itemType !== 'reportRequest'
                  )
                }
                {...headerProps}
              />, // Second item: body
              featureURL === null ? (
                <div key="drawer-body" className="drawer-body" />
              ) : (
                <GeoJSONItemsList
                  key="list"
                  updateFeaturesFn={updateFeaturesFn}
                  sorting={sorting}
                  filtering={filtering}
                  features={features}
                  itemType={itemType}
                  featuresUpdating={featuresUpdating}
                  featuresUpdateError={featuresUpdateError}
                  hoveredFeature={
                    itemType === 'missionTask'
                      ? hoveredMissionTask
                      : itemType === 'report' && isRrActive
                      ? hoveredReport
                      : hoveredFeature
                  }
                  selectedMission={itemType === 'missionTask' ? selectedFeature : {}}
                  selectedReport={selectedReport}
                  selectedFeature={
                    itemType === 'missionTask'
                      ? selectedMissionTask
                      : itemType === 'report' && isRrActive
                      ? selectedReport
                      : selectedFeature
                  }
                  select={
                    itemType === 'missionTask'
                      ? selectMissionTask
                      : itemType === 'report' && isRrActive
                      ? selectReport
                      : isRrActive
                      ? () => {
                          logMain('There is an active Report Request. No selection allowed');
                        }
                      : selectFeature
                  }
                  deselect={
                    itemType === 'missionTask'
                      ? deselectMissionTask
                      : itemType === 'report' && isRrActive
                      ? deselectReport
                      : deselectFeature
                  }
                  mouseEnter={
                    itemType === 'missionTask'
                      ? mouseEnterMissionTask
                      : itemType === 'report' && isRrActive
                      ? mouseEnterReport
                      : mouseEnterFeature
                  }
                  mouseLeave={
                    itemType === 'missionTask'
                      ? mouseLeaveMissionTask
                      : itemType === 'report' && isRrActive
                      ? mouseLeaveReport
                      : mouseLeaveFeature
                  }
                  compareFn={this._compareFeatures}
                  dictionary={dictionary}
                  locale={locale}
                />
              )
            ]}
          </GeoJSONFeaturesProvider>
        )}
        {innerPrimaryDrawerType === 'newReportRequest' && (
          <NewReportRequestDrawerInner {...headerProps} {...drawProps} />
        )}
        {innerPrimaryDrawerType === 'newMapRequest' && (
          <NewMapRequestDrawerInner {...headerProps} {...drawProps} />
        )}
        {innerPrimaryDrawerType === 'emergencyEvent' && (
          <EmergencyEventDrawerInner
            isRrActive={isRrActive}
            {...headerProps}
            filters={filters}
            {...drawProps}
          />
        )}
        {innerPrimaryDrawerType === 'newEmergencyEvent' && (
          <NewEmergencyEventDrawerInner
            {...headerProps}
            {...drawProps}
            locale={this.props.locale}
          />
        )}
        {innerPrimaryDrawerType === 'newMission' && (
          <NewMissionDrawerInner {...headerProps} {...drawProps} />
        )}
        {innerPrimaryDrawerType === 'newEmergencyNotification' && (
          <NewEmergencyCommunicationDrawerInner
            {...headerProps}
            {...drawProps}
            roles={roles}
            locale={this.props.locale}
            enums={enums}
          />
        )}
      </Drawer>
    );
  }
}

export default enhance(LeftDrawer);
