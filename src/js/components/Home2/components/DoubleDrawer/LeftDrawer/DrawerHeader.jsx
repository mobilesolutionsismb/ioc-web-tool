import React, { Component } from 'react';
// import { Header } from './Headers';
import { default as ReportsHeader } from './Headers/Report';
import { default as ReportRequestsHeader } from './Headers/ReportRequest';
import { default as EmergencyCommunicationsHeader } from './Headers/EmergencyCommunication';
import { default as MissionsHeader } from './Headers/Mission';
import { default as MissionDetailsHeader } from './Headers/MissionDetail';
import { default as MapRequestsHeader } from './Headers/MapRequest';
import { default as AgentLocationsHeader } from './Headers/AgentLocation';
import nop from 'nop';

class DrawerHeader extends Component {
  static defaultProps = {
    refreshItems: nop,
    loading: false
  };

  render() {
    const { primaryDrawerType, refreshItems, loading, ...rest } = this.props;
    let Component = null;
    switch (primaryDrawerType) {
      case 'reports':
        Component = ReportsHeader;
        break;
      case 'reportRequests':
        Component = ReportRequestsHeader;
        break;
      case 'emergencyCommunications':
        Component = EmergencyCommunicationsHeader;
        break;
      case 'missions':
        Component = MissionsHeader;
        break;
      case 'mission_detail':
        Component = MissionDetailsHeader;
        break;
      case 'mapRequests':
        Component = MapRequestsHeader;
        break;
      case 'agentLocations':
        Component = AgentLocationsHeader;
        break;
      default:
        break;
    }
    if (Component) {
      return <Component refreshItems={refreshItems} loading={loading} {...rest} />;
    } else {
      return null;
    }
  }
}

export default DrawerHeader;
