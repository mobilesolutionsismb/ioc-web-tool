import React from 'react';
import { List } from 'react-virtualized';
import { lighten } from 'material-ui/utils/colorManipulator';
import muiThemeable from 'material-ui/styles/muiThemeable';
import FeatureListItem from './FeatureListItem';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';

const ITEMS_SPACING = 8;

const LIST_ITEMS_HEIGHTS = {
  report: 100,
  reportRequest: 120,
  emergencyCommunication: 120,
  mission: 120,
  missionTask: 110,
  agentLocation: 100,
  mapRequest: 128
};

const ITEMS_INNER_STYLE = {
  width: '100%',
  height: `calc(100% - ${ITEMS_SPACING}px)`,
  background: 'rgba(255, 255, 255, 0.3)'
};

function getRowRenderer(
  features,
  select,
  deselect,
  mouseEnter,
  mouseLeave,
  selectedFeature,
  hoveredFeature,
  compareFn,
  muiTheme,
  dictionary,
  selectedMission,
  selectedReport,
  locale
) {
  function rowRenderer({
    key, // Unique key within array of rows
    index, // Index of row within collection
    isScrolling, // The List is currently being scrolled
    isVisible, // This row is visible within the List (eg it is not an overscanned row)
    style // Style object to be applied to row (to position it)
  }) {
    const feature = Array.isArray(features) ? features[index] : null;
    const properties = feature ? feature.properties : null;
    // const address = properties ? properties.address : 'unk';
    // const id = properties ? properties.id : undefined;
    const isSelected = compareFn(feature, selectedFeature);
    const isHover = compareFn(feature, hoveredFeature);

    const selectedStyle = {
      background: lighten(muiTheme.palette.canvasColor, 0.5)
    };

    let innerStyle = isSelected ? { ...ITEMS_INNER_STYLE, ...selectedStyle } : ITEMS_INNER_STYLE;

    if (!selectedFeature && isHover)
      innerStyle = { ...innerStyle, background: lighten(muiTheme.palette.canvasColor, 0.3) };

    return (
      <div
        key={key}
        className={isSelected ? 'selected' : ''}
        style={style}
        onClick={() => (isSelected ? deselect() : select(feature))}
        onMouseEnter={() => {
          if (
            !selectedFeature ||
            (selectedFeature.properties.itemType === 'reportRequest' &&
              !selectedReport &&
              feature.properties.itemType === 'report')
          ) {
            mouseEnter(feature);
          }
        }}
        onMouseLeave={() => {
          if (
            !selectedFeature ||
            (selectedFeature.properties.itemType === 'reportRequest' && !selectedReport)
          ) {
            mouseLeave();
          }
        }}
      >
        {/* Render card depending on itemType - height and width should be 100% */}
        <div style={innerStyle}>
          <FeatureListItem
            key="feature-list-item"
            properties={properties}
            dictionary={dictionary}
            locale={locale}
            selectedMission={selectedMission}
            deselectFeature={deselect}
          />
        </div>
      </div>
    );
  }
  return rowRenderer;
}

const FIXED_HEADER_SIZE = 255; // all headers sum (64 + 64 + 48 + 79)

class GeoJSONItemsList extends React.Component {
  static defaultProps = {
    updateFeaturesFn: null
  };

  state = {
    height: Math.max(window.innerHeight - FIXED_HEADER_SIZE, 100) // we don't want negative numbers
  };

  updateHeight = () => {
    this.setState({ height: Math.max(window.innerHeight - FIXED_HEADER_SIZE, 100) });
  };

  componentDidMount = () => {
    window.addEventListener('resize', this.updateHeight);
    if (typeof this.props.updateFeaturesFn === 'function') {
      this.props.updateFeaturesFn();
    }
  };

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateHeight);
  };

  render() {
    // TODO use updating and error to display loader and error message
    const {
      features,
      /* updating, error, */ sorting,
      select,
      deselect,
      mouseEnter,
      mouseLeave,
      selectedFeature,
      selectedMission,
      hoveredFeature,
      compareFn,
      itemType,
      muiTheme,
      dictionary,
      selectedReport,
      locale
    } = this.props;
    const cardHeight = itemType ? LIST_ITEMS_HEIGHTS[itemType] || 20 : 20;
    const scrollToIndex = selectedFeature
      ? features.findIndex(f => areFeaturesEqual(f, selectedFeature))
      : undefined;

    const drawerHeight = itemType !== 'missionTask' ? this.state.height : this.state.height - 147;

    return (
      <List
        style={{ background: muiTheme.palette.backgroundColor }}
        sorting={sorting}
        width={380}
        height={drawerHeight}
        rowCount={features.length}
        rowHeight={cardHeight + ITEMS_SPACING}
        scrollToIndex={scrollToIndex}
        scrollToAlignment="auto"
        rowRenderer={getRowRenderer(
          features,
          select,
          deselect,
          mouseEnter,
          mouseLeave,
          selectedFeature,
          hoveredFeature,
          compareFn,
          muiTheme,
          dictionary,
          selectedMission,
          selectedReport,
          locale
        )}
      />
    );
  }
}

export default muiThemeable()(GeoJSONItemsList);
