import React, { Component } from 'react';
import DrawerFooter from 'js/components/app/drawer/DrawerFooter';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { drawerNames } from 'js/modules/AreaOfInterest';
import ReportNewRequest from 'js/components/Left/ReportRequests/ReportNewRequest';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withReportRequests } from 'ioc-api-interface';
import { withReportNewRequest } from 'js/modules/reportNewRequest';
import { withLoader, withMessages } from 'js/modules/ui';
import { getCompleteDateTime } from 'js/utils/getCompleteDateTime';
import wellknown from 'wellknown';
import { point, polygon } from '@turf/helpers';
import { withEnumsAndRoles } from 'ioc-api-interface';

import { compose } from 'redux';
const enhance = compose(
  withLeftDrawer,
  withReportRequests,
  withReportNewRequest,
  withLoader,
  withMessages,
  withEnumsAndRoles
);

class NewReportRequestDrawerInner extends Component {
  state = {};

  createReportRequest = async () => {
    let newReportRequest = Object.assign({}, this.props.newReportRequest);

    if (newReportRequest.contentType.length === 0) {
      this.props.pushMessage('Conten type mandatory', 'error', null);
      return;
    }

    // if (newReportRequest.reportersAllowed.length === 0) {
    //     this.props.pushMessage('Reporters allowed mandatory', 'error', null);
    //     return;
    // }
    // else{
    newReportRequest.roles = newReportRequest.reportersAllowed.map(item => parseInt(item, 10));
    //}

    if (newReportRequest.startDate) {
      newReportRequest.start = getCompleteDateTime(
        newReportRequest.startDate,
        newReportRequest.startTime,
        this.props.locale
      );
    } else {
      this.props.pushMessage('Start Date mandatory', 'error', null);
      return;
    }

    if (
      newReportRequest.contentType.indexOf('measure') >= 0 &&
      newReportRequest.measures.length === 0
    ) {
      this.props.pushMessage('Insert measures', 'error', null);
      return;
    }

    if (!newReportRequest.visibility || newReportRequest.visibility === '') {
      this.props.pushMessage('Visibility field mandatory', 'error', null);
      return;
    }

    if (newReportRequest.endDate) {
      newReportRequest.end = getCompleteDateTime(
        newReportRequest.endDate,
        newReportRequest.endTime,
        this.props.locale
      );
    }

    if (newReportRequest.end && newReportRequest.end.isBefore(newReportRequest.start)) {
      this.props.pushMessage('Start Date must be before the End date', 'error', null);
      return;
    }

    if (
      !this.props.drawPolygon.id &&
      newReportRequest.areaOfInterest === null &&
      newReportRequest.id === 0
    ) {
      this.props.pushMessage('Select the Area of interest', 'error', null);
      return;
    }
    if (newReportRequest.id === 0 && this.props.drawPolygon.id)
      newReportRequest.areaOfInterest = wellknown.stringify(
        polygon(this.props.drawPolygon.coordinates)
      );

    newReportRequest.isSuggested = false;
    newReportRequest.type = 'reportRequest';
    newReportRequest.measures = newReportRequest.measures.map(item => item.value);

    if (
      newReportRequest.id === 0 &&
      (!newReportRequest.location || newReportRequest.location === null)
    ) {
      //const lng, lat } = this.props.drawPoint.coordinates;
      //newReportRequest.location = wellknown.stringify(point([lng, lat]));
      newReportRequest.location = wellknown.stringify(point(this.props.drawPoint.coordinates));
    }

    try {
      if (newReportRequest.id === 0) {
        const res = await this.props.createReportRequest(
          newReportRequest,
          this.props.loadingStart,
          this.props.loadingStop
        );
        if (res != null) this.props.pushMessage('_new_report_request_create', 'success', null);
        else this.props.pushError(new Error('Error: cannot create report request'));
      } else {
        await this.props.updateReportRequest(
          newReportRequest,
          this.props.loadingStart,
          this.props.loadingStop
        );
        this.props.pushMessage('_new_report_request_update', 'success', null);
      }
    } catch (err) {
      let message = err;
      if (err && err.details && err.details.serverDetails) {
        let error = err.details.serverDetails.error;
        if (error.validationErrors)
          for (let i = 0; i < error.validationErrors.length; i++)
            this.props.pushError(error.validationErrors[i].message);
      } else {
        this.props.pushError(message);
      }
      this.props.loadingStop();
    } finally {
      this.props.history.replace(`/home/${drawerNames.report}?reportRequest`);
      this.props.resetReportNewRequest();
      const mapDraw = this.props.getMapDraw();
      if (this.props.drawPolygon.id) {
        const ids = [this.props.drawPoint.id, this.props.drawPolygon.id];
        if (mapDraw) mapDraw.delete(ids);
      } else {
        if (mapDraw) mapDraw.deleteAll();
      }
      this.props.resetHomeDrawState();
    }
  };

  clearReportRequest = () => {
    const isCreateReportRequestActive = this.props.search === '?reportNewRequest';
    this.props.history.replace(
      `/home/${drawerNames.report}${!isCreateReportRequestActive ? '?reportNewRequest' : ''}`
    );
  };

  selectFirstDrawerClick = () => {
    const isReportRequestActive = this.props.search === '?reportRequest';
    this.props.history.replace(
      `/home/${drawerNames.report}${!isReportRequestActive ? '?reportRequest' : ''}`
    );
  };

  resetReportNewRequest = () => {
    this.props.resetReportNewRequest();
    this.resetState();
  };

  leftAction = () => {
    this.props.resetReportNewRequest();
    this.resetState();
    this.props.history.push('/home/reports?reportRequest');
  };

  rightAction = () => {
    this.props.resetReportNewRequest();
    this.resetState();
    this.props.history.push('/home/map');
  };

  resetState = () => {
    const mapDraw = this.props.getMapDraw();
    if (this.props.drawPolygon.id) {
      const ids = [this.props.drawPoint.id, this.props.drawPolygon.id];
      if (mapDraw) mapDraw.delete(ids);
    } else {
      if (mapDraw) mapDraw.deleteAll();
    }
    this.props.resetHomeDrawState();
  };

  render() {
    const {
      dictionary,
      enums,
      roles,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory,
      getMapDraw
    } = this.props;
    let titleDrawer = null,
      statusDrawer = null,
      filterHeader = null,
      listOfItems = null,
      footerDrawer = null;

    const drawProps = {
      getMapDraw,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory
    };

    titleDrawer = (
      <DrawerTitle
        category={drawerNames.reportNewRequest}
        title={dictionary._new_request}
        leftIcon="keyboard_backspace"
        rightIcon="close"
        rightAction={this.rightAction}
        leftAction={this.leftAction}
      />
    );
    listOfItems = (
      <ReportNewRequest
        dictionary={dictionary}
        roles={roles}
        enums={enums}
        locale={this.props.locale}
        category={drawerNames.reportNewRequest}
        {...drawProps}
      />
    );
    footerDrawer = (
      <DrawerFooter
        entityId={this.props.newReportRequest.id}
        leftLabel={dictionary._clear}
        rightLabel={dictionary._save}
        apply={this.createReportRequest}
        clear={this.resetReportNewRequest}
      />
    );

    return (
      <div>
        {titleDrawer}
        {filterHeader}
        {statusDrawer}
        <div style={{ overflowY: 'auto', overflowX: 'hidden', height: '75vh' }}>{listOfItems}</div>
        {footerDrawer}
      </div>
    );
  }
}

export default enhance(NewReportRequestDrawerInner);
