import React, { Component } from 'react';
import DrawerFooter from 'js/components/app/drawer/DrawerFooter';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { drawerNames } from 'js/modules/AreaOfInterest';
import EmCommStepper from 'js/components/Left/EmergencyCommunication/EmCommStepper';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withEmCommNew } from 'js/modules/emCommNew';
import { withLoader, withMessages } from 'js/modules/ui';
import { getCompleteDateTime } from 'js/utils/getCompleteDateTime';
import { point, polygon } from '@turf/helpers';
import { API, withAlertsAndWarnings } from 'ioc-api-interface';
import wellknown from 'wellknown';
import moment from 'moment';
import { compose } from 'redux';
import { HAZARD_MAPPING } from 'js/modules/emCommNew';
import { CAP_CERTAINTY } from 'js/modules/emCommNew';

const api = API.getInstance();

const enhance = compose(
  withLeftDrawer,
  withLoader,
  withMessages,
  withEmCommNew,
  withAlertsAndWarnings
);

class NewEmergencyCommunicationDrawerInner extends Component {
  state = {};

  createEmComm = async () => {
    let newEmComm = { language: this.props.locale, ...this.props.newEmComm },
      areaOfInterest = '',
      location = '',
      targetAreaOfInterest = null,
      targetLocation = null,
      hazard = '',
      capCategory = '';

    if (newEmComm.type === '') {
      this.props.pushMessage('Emergency Communication Type mandatory', 'error', null);
      return;
    }
    if (newEmComm.capStatus.value === '') {
      this.props.pushMessage('Cap Status mandatory', 'error', null);
      return;
    }

    if (newEmComm.visibility === '') {
      this.props.pushMessage('Visibility mandatory', 'error', null);
      return;
    }

    if (newEmComm.hazard.value === '') {
      this.props.pushMessage('Hazard type mandatory', 'error', null);
      return;
    } else {
      hazard = HAZARD_MAPPING[newEmComm.hazard.value];
    }
    if (newEmComm.capCategory.value === '') {
      this.props.pushMessage('capCategory type mandatory', 'error', null);
      return;
    } else {
      var splitArray2 = newEmComm.capCategory.value.split('_ecc_');
      capCategory = splitArray2[1];
    }

    if (!this.props.drawPolygon && newEmComm.id === 0) {
      this.props.pushMessage('Select the Area of interest', 'error', null);
      return;
    } else {
      if (this.props.drawPolygon.id)
        areaOfInterest = wellknown.stringify(polygon(this.props.drawPolygon.coordinates));
    }

    if (newEmComm.id === 0 && this.props.drawPoint.id) {
      location = wellknown.stringify(point(this.props.drawPoint.coordinates));
    }

    if (this.props.drawTargetPolygon.id && newEmComm.id === 0) {
      targetAreaOfInterest = wellknown.stringify(polygon(this.props.drawTargetPolygon.coordinates));
    }

    if (this.props.drawTargetPoint.id && newEmComm.id === 0) {
      targetLocation = wellknown.stringify(point(this.props.drawTargetPoint.coordinates));
    }

    let start, end, startTmp, endTmp;
    if (newEmComm.startDate) {
      startTmp = getCompleteDateTime(newEmComm.startDate, newEmComm.startTime, this.props.locale);
      start = moment.utc(startTmp).format();
    } else {
      this.props.pushMessage('Start Date mandatory', 'error', null);
      return;
    }

    if (newEmComm.endDate) {
      endTmp = getCompleteDateTime(newEmComm.endDate, newEmComm.endTime, this.props.locale);
      end = moment.utc(endTmp).format();
    }

    if (endTmp && endTmp.isBefore(startTmp)) {
      this.props.pushMessage('Start Date must be before the End date', 'error', null);
      return;
    }

    if (newEmComm.warningLevel.value === '') {
      this.props.pushMessage('Severity mandatory', 'error', null);
      return;
    }
    if (newEmComm.capUrgency.value === '') {
      this.props.pushMessage('Urgency mandatory', 'error', null);
      return;
    }
    if (newEmComm.capCertainty.value === '') {
      this.props.pushMessage('Certainty mandatory', 'error', null);
      return;
    } else if (!CAP_CERTAINTY[newEmComm.type][newEmComm.capCertainty.value]) {
      this.props.pushMessage('Invalid value of CapCertainty', 'error', null);
      return;
    }
    if (newEmComm.title === '') {
      this.props.pushMessage('Title mandatory', 'error', null);
      return;
    }

    if (newEmComm.receivers.length === 0) {
      this.props.pushMessage('Receivers Field mandatory', 'error', null);
      return;
    }

    let newEmergencyCommunication = {
      id: newEmComm.id,
      type: newEmComm.type,
      capStatus: newEmComm.capStatus.value,
      hazard,
      capCategory,
      areaOfInterest,
      location,
      start,
      end,
      language: newEmComm.language,
      warningLevel: newEmComm.warningLevel.value,
      capCertainty: newEmComm.capCertainty.value,
      capUrgency: newEmComm.capUrgency.value,
      title: newEmComm.title,
      capResponseType: newEmComm.capResponseType.value,
      instruction: newEmComm.instruction,
      targetAreaOfInterest,
      targetLocation,
      description: newEmComm.description,
      receivers: newEmComm.receivers.map(item => item.value),
      web: newEmComm.web,
      visibility: newEmComm.visibility
    };
    try {
      this.props.loadingStart();
      let formData = new FormData();

      Object.keys(newEmergencyCommunication).forEach(item => {
        if (!item) return;
        if (Array.isArray(newEmergencyCommunication[item])) {
          newEmergencyCommunication[item].forEach((value, index) => {
            const key = item + '[' + index + ']';
            formData.append(key, value);
            return;
          });
        } else {
          if (newEmergencyCommunication[item])
            formData.append(item, newEmergencyCommunication[item]);
        }
      });
      if (newEmComm.id === 0)
        await api.emergencyCommunicationV2.createEmergencyCommunication(formData);
      else {
        await api.emergencyCommunicationV2.updateEmergencyCommunication(formData);
      }
      await this.props.updateAlertsAndWarnings(
        false,
        this.props.loadingStart,
        this.props.loadingStop
      );
      if (newEmComm.id === 0)
        this.props.pushMessage(this.props.dictionary._new_emcomm_create, 'success', null, null);
      else this.props.pushMessage(this.props.dictionary._emcomm_update, 'success', null, null);

      this.props.history.push('/home/notificaions');
      this.resetEmComm();
    } catch (err) {
      let message = err;
      if (err && err.details && err.details.serverDetails) {
        let error = err.details.serverDetails.error;
        if (error.validationErrors) {
          for (let i = 0; i < error.validationErrors.length; i++)
            this.props.pushError(error.validationErrors[i].message);
        } else if (error.message) this.props.pushError(error.message);
      } else {
        this.props.pushError(message);
      }
      //this.props.pushMessage(error, 'error', null, null);
    } finally {
      this.props.loadingStop();
    }
  };

  resetEmComm = () => {
    this.props.resetEmComm();
    this.resetState();
  };

  resetState = () => {
    const mapDraw = this.props.getMapDraw();
    if (this.props.drawPolygon.id) {
      const ids = [this.props.drawPoint.id, this.props.drawPolygon.id];
      if (mapDraw) mapDraw.delete(ids);
    } else {
      if (mapDraw) mapDraw.deleteAll();
    }
    if (this.props.drawTargetPolygon.id) {
      const ids = [this.props.drawTargetPoint.id, this.props.drawTargetPolygon.id];
      if (mapDraw) mapDraw.delete(ids);
    } else {
      if (mapDraw) mapDraw.deleteAll();
    }
    this.props.resetHomeDrawState('base');
    this.props.resetHomeDrawState('target');
  };

  leftAction = () => {
    this.props.resetEmComm();
    this.resetState();
    this.props.history.push('/home/notifications');
  };

  rightAction = () => {
    this.props.resetEmComm();
    this.resetState();
    this.props.history.push('/home/map');
  };

  render() {
    const {
      dictionary,
      enums,
      roles,
      drawPoint,
      drawPolygon,
      drawTargetPoint,
      drawTargetPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawCategory,
      setDrawPolygon,
      setDrawTargetPoint,
      setDrawTargetPolygon,
      getMapDraw
    } = this.props;
    let titleDrawer = null,
      statusDrawer = null,
      filterHeader = null,
      emCommStepper = null,
      footerDrawer = null;

    const drawProps = {
      getMapDraw,
      drawPoint,
      drawPolygon,
      drawTargetPoint,
      drawTargetPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory,
      setDrawTargetPoint,
      setDrawTargetPolygon
    };

    titleDrawer = (
      <DrawerTitle
        category={drawerNames.notificationNew}
        title={dictionary._new_em_comm}
        leftIcon="keyboard_backspace"
        rightIcon="close"
        rightAction={this.rightAction}
        leftAction={this.leftAction}
      />
    );
    emCommStepper = (
      <EmCommStepper
        dictionary={dictionary}
        enums={enums}
        roles={roles}
        locale={this.props.locale}
        category={drawerNames.notificationNew}
        {...drawProps}
      />
    );
    footerDrawer = (
      <DrawerFooter
        entityId={this.props.newEmComm.id}
        leftLabel={dictionary._clear}
        rightLabel={dictionary._save}
        apply={this.createEmComm}
        clear={this.resetEmComm}
      />
    );

    return (
      <div>
        {titleDrawer}
        {filterHeader}
        {statusDrawer}
        <div style={{ overflowY: 'auto', overflowX: 'hidden', height: '70vh' }}>
          {emCommStepper}
        </div>
        {footerDrawer}
      </div>
    );
  }
}

export default enhance(NewEmergencyCommunicationDrawerInner);
