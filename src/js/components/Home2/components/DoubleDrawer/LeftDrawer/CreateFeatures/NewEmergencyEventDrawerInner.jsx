import React, { Component } from 'react';
import {
  TextField,
  FlatButton,
  TimePicker,
  RaisedButton,
  DatePicker,
  IconButton,
  Chip,
  Avatar
} from 'material-ui';
import { Step, Stepper, StepButton, StepContent, StepLabel } from 'material-ui/Stepper';
import { withEmergencyEvents, API } from 'ioc-api-interface';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { withEventCreate } from 'js/modules/eventCreate';
import { compose } from 'redux';
import SuperSelectField from 'material-ui-superselectfield';
import { withMessages, withLoader } from 'js/modules/ui';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { InputAOIStep } from 'js/components/Steps';
import { FILTERS } from 'js/modules/reportFilters';
import wellknown from 'wellknown';
import { getCompleteDateTime } from 'js/utils/getCompleteDateTime';
import {
  WarningIcon,
  LocationOnIcon,
  EventIcon,
  PersonIcon,
  MoneyIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import Clear from 'material-ui/svg-icons/content/clear';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import DrawerFooter from 'js/components/app/drawer/DrawerFooter';
import { AOILabel } from 'js/components/app/drawer/components/commons';
import { HAZARD_TYPES_LETTERS, HAZARD_TYPES_LABELS } from 'js/components/Home/components/commons';
import { point, polygon } from '@turf/helpers';
import { LngLat } from 'mapbox-gl';
import moment from 'moment';

const api = API.getInstance();

const enhance = compose(
  withLeftDrawer,
  withLoader,
  withMessages,
  withEmergencyEvents,
  withEventCreate
);

class NewEmergencyEventDrawerInner extends Component {
  state = {
    stepIndex: 0
  };

  handleCustomDisplaySelectionsHazard = hazard => hazard =>
    hazard && hazard.label !== '' ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        <Chip style={{ margin: 5 }} onRequestDelete={this.onRequestDeleteHazard(hazard)}>
          <Avatar size={30}>{HAZARD_TYPES_LETTERS[HAZARD_TYPES_LABELS[hazard.value]]}</Avatar>
          <span>{hazard.label}</span>
        </Chip>
      </div>
    ) : (
      <div style={{ minHeight: 42, lineHeight: '42px' }}>Select the Hazard</div>
    );

  onRequestDeleteHazard = itemToRemove => event => {
    this.props.setEventCreateHazard({ label: '', value: '' });
  };

  handleTitleChange = (event, title) => {
    this.props.setEventCreateTitle(title);
  };

  handleCommentChange = (event, comment) => {
    this.props.setEventCreateComment(comment);
  };
  handlePeopleAffectedChange = (event, number) => {
    this.props.setEventCreatePA(number);
  };
  handleCausaltiesChange = (event, number) => {
    this.props.setEventCreatePK(number);
  };
  handleDamagesChange = (event, number) => {
    this.props.setEventCreateDamages(number);
  };

  handleNext = () => {
    const { stepIndex } = this.state;
    if (stepIndex < 5) {
      this.setState({
        stepIndex: stepIndex + 1
      });
    }
  };

  handlePrev = () => {
    const { stepIndex } = this.state;
    if (stepIndex > 0) {
      this.setState({
        stepIndex: stepIndex - 1
      });
    }
  };

  renderStepActions(step, enabled, clear) {
    return (
      <div style={{ margin: '12px 0' }}>
        {this.state.stepIndex === step &&
          step < 4 && (
            <RaisedButton
              disabled={!enabled}
              label={this.props.dictionary._next}
              disableTouchRipple={true}
              disableFocusRipple={true}
              primary={true}
              onClick={this.handleNext}
              style={{ marginRight: 12 }}
            />
          )}
        {this.state.stepIndex === step &&
          step === 4 && (
            <RaisedButton
              label="NEXT"
              disableTouchRipple={true}
              disableFocusRipple={true}
              primary={true}
              onClick={this.handleNext}
              style={{ marginRight: 12 }}
            />
          )}
        {this.state.stepIndex === step && (
          <FlatButton
            label={this.props.dictionary._cancel}
            disableTouchRipple={true}
            disableFocusRipple={true}
            onClick={clear}
          />
        )}
      </div>
    );
  }

  resetDate = fields => {
    if (fields === 'start') {
      this.props.resetEventCreateAllSDate();
    } else {
      this.props.resetEventCreateAllEDate();
    }
  };
  resetTitle = () => {
    this.props.setEventCreateTitle('');
  };
  resetComment = () => {
    this.props.setEventCreateTitle('');
  };
  resetHazard = () => {
    this.props.setEventCreateHazard({ value: '', label: '' });
  };
  resetAOI = () => {
    this.props.setDrawTrash(this.props.category, true);
  };
  resetAllDate = () => {
    this.props.resetDate();
  };
  resetComment = () => {
    this.props.setEventCreateComment('');
  };
  resetDamages = () => {
    this.props.resetDamages();
  };
  handleStartChange = (event, startDate) => {
    this.props.setEventCreateStartDate(startDate);
  };

  handleStartTimeChange = (event, startTime) => {
    this.props.setEventCreateStartTime(startTime);
  };

  handleEndChange = (event, endDate) => {
    this.props.setEventCreateEndDate(endDate);
  };

  handleEndTimeChange = (event, endTime) => {
    this.props.setEventCreateEndTime(endTime);
  };
  convertDate = date => {
    let formattedDate = '';
    if (date !== null) {
      const yyyy = date.getFullYear().toString();
      const mm = (date.getMonth() + 1).toString();
      const dd = date.getDate().toString();

      const mmChars = mm.split('');
      const ddChars = dd.split('');

      formattedDate =
        yyyy +
        '-' +
        (mmChars[1] ? mm : '0' + mmChars[0]) +
        '-' +
        (ddChars[1] ? dd : '0' + ddChars[0]);
    }
    return formattedDate;
  };
  formatAMPM = date => {
    let formattedTime = '';
    if (date !== null) {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      formattedTime = hours + ':' + minutes + ' ' + ampm;
    }
    return formattedTime;
  };

  setEventCreateHazard = hazard => {
    this.props.setEventCreateHazard(hazard);
  };

  resetEmergencyEvent = () => {
    if (this.props.id !== 0) return;
    this.props.resetEventCreate();
    this.props.resetDrawState(drawerNames.eventCreate);
  };

  leftAction = () => {
    this.props.resetEventCreate();
    this.resetState();
    this.props.history.push('/home/events');
  };

  resetState = () => {
    const mapDraw = this.props.getMapDraw();
    if (this.props.drawPolygon.id) {
      const ids = [this.props.drawPoint.id, this.props.drawPolygon.id];
      if (mapDraw) mapDraw.delete(ids);
    } else {
      if (mapDraw) mapDraw.deleteAll();
    }
    this.props.resetHomeDrawState();
  };

  rightAction = () => {
    this.props.resetEventCreate();
    this.resetState();
    this.props.history.push('/home/map');
  };

  _getReverseGeocode = async (category, point, culture = 'en') => {
    let lat = null,
      lng = null;
    if (Array.isArray(point) && point.length === 2) {
      // Assume is geojson-like convention
      lat = point[1];
      lng = point[0];
    } else if (
      point instanceof LngLat ||
      (typeof point.lat === 'number' && typeof point.lng === 'number')
    ) {
      lat = point.lat;
      lng = point.lng;
    } else {
      throw new Error('Invalid point format!');
    }

    return await api.maps.reverseGeocode(culture, lat, lng);
  };

  createEmergencyEvent = async () => {
    try {
      let category = drawerNames.eventCreate;
      let coordinates = '',
        areaOfInterest = '';
      let countryCodes, provinces, locations;
      if (this.props.id === 0) {
        if (this.props.drawPoint.id)
          coordinates = wellknown.stringify(point(this.props.drawPoint.coordinates));
        if (this.props.drawPolygon.id)
          areaOfInterest = wellknown.stringify(polygon(this.props.drawPolygon.coordinates));

        const response = await this._getReverseGeocode(
          category,
          this.props.drawPoint.coordinates,
          this.props.locale
        );
        const selectedReverseGeocode =
          response.result && response.result.resources.length > 0
            ? response.result.resources[0]
            : {};

        countryCodes = selectedReverseGeocode
          ? [selectedReverseGeocode.address.countryRegionIso2]
          : [];
        provinces = selectedReverseGeocode ? [selectedReverseGeocode.address.adminDistrict] : [];
        locations = selectedReverseGeocode ? [selectedReverseGeocode.address.adminDistrict2] : [];
      }
      // else {
      //   coordinates = this.props.selectedEvent.geometry.coordinates;
      //   countryCodes = this.props.countryCodes;
      //   provinces = this.props.provinces;
      //   locations = this.props.locations;
      // }

      let start, end;
      if (this.props.startDate) {
        start = getCompleteDateTime(this.props.startDate, this.props.startTime, this.props.locale);
      } else {
        this.props.pushMessage('Start Date mandatory', 'error', null);
        return;
      }

      //Start Date cannot be older than two weeks
      if (start.isBefore(moment().subtract(14, 'd'))) {
        this.props.pushMessage('Start Date cannot be older than two weeks', 'error', null);
        return;
      }

      if (this.props.endDate) {
        end = getCompleteDateTime(this.props.endDate, this.props.endTime, this.props.locale);
      }

      let hazardValue = '';
      if (this.props.hazard.value === '') {
        this.props.pushMessage('Hazard type mandatory', 'error', null);
        return;
      } else {
        var splitArray = this.props.hazard.value.split('_');
        hazardValue = splitArray[splitArray.length - 1];
      }

      let extensionData = {
        people_affected: this.props.people_affected + '',
        people_killed: this.props.people_killed + '',
        estimated_damage: this.props.estimated_damage + ''
      };

      let newEmergencyEvent = {};

      //Don't want to edit the AOI of the current Event
      if (this.props.id !== 0) {
        newEmergencyEvent = {
          id: this.props.id,
          displayName: this.props.title,
          hazard: hazardValue,
          comment: this.props.comment,
          start,
          end,
          extensionData: JSON.stringify({
            people_affected: this.props.people_affected,
            people_killed: this.props.people_killed,
            estimated_damage: this.props.estimated_damage
          })
        };
        delete newEmergencyEvent.areaOfInterest;
        await this.props.updateEmergencyEvent(
          newEmergencyEvent,
          this.props.loadingStart,
          this.props.loadingStop
        );
        if (this.props.twitterSource) {
          this.props.setTwitterSourceEvent(null);
        }
        this.props.pushMessage(
          this.props.dictionary._emergency_event_edit_success,
          'success',
          null,
          null
        );
      } else {
        newEmergencyEvent = {
          displayName: this.props.title,
          countryCodes,
          hazard: hazardValue,
          comment: this.props.comment,
          start,
          end,
          coordinates,
          areaOfInterest,
          provinces,
          locations,
          extensionData: JSON.stringify(extensionData)
        };

        await this.props.createEmergencyEvent(
          newEmergencyEvent,
          this.props.loadingStart,
          this.props.loadingStop
        );
        if (this.props.twitterSource) {
          this.props.setTwitterSourceEvent(null);
        }
        this.props.pushMessage(
          this.props.dictionary._emergency_event_create_success,
          'success',
          null,
          null
        );
      }

      this.props.resetEventCreate();
      this.props.deselectEvent();
      const mapDraw = this.props.getMapDraw();
      if (this.props.drawPolygon.id) {
        const ids = [this.props.drawPoint.id, this.props.drawPolygon.id];
        if (mapDraw) mapDraw.delete(ids);
      } else {
        if (mapDraw) mapDraw.deleteAll();
      }
      this.props.resetHomeDrawState();
      this.props.history.push('/home/map');
    } catch (err) {
      // this.props.pushMessage(error, 'error', null, null);
      // console.log(error);
      let message = err;
      if (err && err.details && err.details.serverDetails) {
        let error = err.details.serverDetails.error;
        if (error.validationErrors)
          for (let i = 0; i < error.validationErrors.length; i++)
            this.props.pushError(error.validationErrors[i].message);
      } else {
        this.props.pushError(message);
      }
      this.props.loadingStop();
    }
  };

  toggleFirstDrawer = (newValue, drawerType) => {
    this.props.setOpenPrimary(newValue, drawerType);
    if (drawerType !== '') this.props.history.push(`/home/${drawerType}`);
    else this.props.history.push('/home/map');
    if (!newValue) {
      //When the drawer is closed, I have to delete point and polygon from the map
      this.props.resetDrawState(drawerType);
      this.props.setDrawTrash(drawerType, true);
    }
    this.props.setOpenSecondary(false);
  };

  render() {
    let {
      dictionary,
      resetHomeDrawState,
      drawPoint,
      drawPolygon,
      getMapDraw,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon
    } = this.props;
    const { stepIndex } = this.state;

    const StepTitleChild = (
      <div>
        <span>{dictionary._title} *</span>
      </div>
    );

    const hazardList = FILTERS.hazard
      .filter(item => item !== '_hazard_unknown' && item !== '_hazard_none')
      .map(item => {
        let hazard = {
          value: item,
          label: this.props.dictionary(item)
        };
        return hazard;
      });

    const hazardListNodes = hazardList.map((category, index) => {
      return (
        <div key={index} value={category.value} label={category.label}>
          <span>{category.label}</span>
        </div>
      );
    });

    const hazardSelected = this.props.hazard;

    const superSelectHazard = (
      <SuperSelectField
        style={{ width: '90%' }}
        name="selectHazardEvent"
        floatingLabel="Select the Hazard"
        onChange={this.setEventCreateHazard}
        value={hazardSelected}
        selectionsRenderer={this.handleCustomDisplaySelectionsHazard(hazardSelected)}
      >
        {hazardListNodes}
      </SuperSelectField>
    );

    const StepTitleContent =
      stepIndex === 0 ? (
        <TextField
          name="position"
          hintText={this.props.dictionary._title_of_event}
          onChange={this.handleTitleChange}
          value={this.props.title}
        />
      ) : (
        <span style={{ fontSize: 14 }}>{this.props.title}</span>
      );

    const StepLabelChild = (
      <div>
        <span>{dictionary._hazard} *</span>
      </div>
    );
    const stepHCVisible = (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-start',
          marginTop: 20
        }}
      >
        <WarningIcon />
        {superSelectHazard}
      </div>
    );
    const StepHazardContent =
      stepIndex === 1 ? (
        stepHCVisible
      ) : this.props.hazard && this.props.hazard.label ? (
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
          <WarningIcon />
          <span style={{ fontSize: 14, marginTop: 15 }}>{this.props.hazard.label}</span>
        </div>
      ) : (
        <span />
      );

    const StepCommentChild = (
      <div>
        <span>{dictionary._description}</span>
      </div>
    );
    const StepDamagesChild = (
      <div>
        <span>{dictionary._damage}</span>
      </div>
    );
    const address =
      drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const StepAOIChild = <AOILabel text={address} dictionary={dictionary} />;

    const drawProps = {
      getMapDraw,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory
    };

    const stepAOIVisible =
      stepIndex === 2 ? (
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
          <LocationOnIcon style={{ paddingTop: 25 }} />
          <div style={{ maxWidth: 256 }}>
            <InputAOIStep
              isDisabled={this.props.id !== 0}
              category={this.props.category ? this.props.category : drawerNames.eventCreate}
              address={address}
              renderStepActions={this.renderStepActions(0)}
              {...drawProps}
              resetCategory="base"
            />
          </div>
        </div>
      ) : (
        <div />
      );

    const formattedStartDate = this.convertDate(this.props.startDate);
    const formattedEndDate = this.convertDate(this.props.endDate);
    const formattedStartTime = this.formatAMPM(this.props.startTime);
    const formattedEndTime = this.formatAMPM(this.props.endTime);
    const StepTimeChild = (
      <div>
        <div>
          <span>{dictionary._event_time} *</span>
        </div>
      </div>
    );
    const StepCommentContent =
      stepIndex === 4 ? (
        <TextField
          name="description"
          onChange={this.handleCommentChange}
          value={this.props.comment}
          multiLine={true}
          rows={3}
        />
      ) : (
        <span style={{ fontSize: 14 }}>{this.props.comment}</span>
      );

    const titleDrawer = (
      <DrawerTitle
        category={this.props.primaryDrawerType}
        title={dictionary._new_event}
        associatedFirstDrawerName={drawerNames.event}
        leftIcon="keyboard_backspace"
        backDrawerType={drawerNames.event}
        rightIcon="close"
        rightAction={this.rightAction}
        leftAction={this.leftAction}
      />
    );
    const footerDrawer = (
      <DrawerFooter
        category={this.props.primaryDrawerType}
        leftLabel={dictionary._clear}
        rightLabel={dictionary._save}
        apply={this.createEmergencyEvent}
        clear={this.resetEmergencyEvent}
        entityId={0}
      />
    );
    return (
      <section>
        {titleDrawer}
        <div className="drawerCreateEventContent">
          <Stepper activeStep={stepIndex} linear={false} orientation="vertical">
            <Step active={stepIndex >= 0} completed={stepIndex > 0 && this.props.title !== ''}>
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 0
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepTitleChild} />
              </StepButton>
              <StepContent>
                {StepTitleContent}
                {this.renderStepActions(0, this.props.title !== '', () => this.resetTitle())}
              </StepContent>
            </Step>
            <Step
              active={stepIndex >= 1}
              completed={
                stepIndex !== 1 && this.props.hazard && this.props.hazard.label ? true : false
              }
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 1
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepLabelChild} />
              </StepButton>
              <StepContent>
                {StepHazardContent}
                {this.renderStepActions(1, this.props.hazard && this.props.hazard.label, () =>
                  this.resetHazard()
                )}
              </StepContent>
            </Step>
            <Step
              active={stepIndex >= 2}
              completed={stepIndex !== 2 && address !== '' ? true : false}
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 2
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepAOIChild} />
              </StepButton>
              <StepContent>
                {stepAOIVisible}
                {this.renderStepActions(2, this.props.drawPolygon.id, () => this.resetAOI())}
              </StepContent>
            </Step>
            <Step
              active={stepIndex >= 3}
              completed={stepIndex !== 3 && formattedStartDate !== '' ? true : false}
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 3
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepTimeChild} />
              </StepButton>
              <StepContent style={{ width: '300px' }}>
                {stepIndex === 3 ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      paddingTop: '20px'
                    }}
                  >
                    <EventIcon />
                    <div style={{ flexGrow: 1 }}>Start*</div>
                    <DatePicker
                      value={this.props.startDate ? this.props.startDate : null}
                      id="startDate"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      style={{ flexGrow: 1 }}
                      ref="startDate"
                      onChange={this.handleStartChange}
                    />
                    <TimePicker
                      value={this.props.startTime ? this.props.startTime : null}
                      id="startTime"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      style={{ flexGrow: 1 }}
                      ref="startTime"
                      onChange={this.handleStartTimeChange}
                    />
                    <IconButton
                      onClick={() => this.resetDate('start')}
                      style={{ padding: '0px !important' }}
                      iconStyle={{ width: 12, height: 12 }}
                    >
                      <Clear />
                    </IconButton>
                  </div>
                ) : formattedStartDate !== '' && formattedStartTime !== '' ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      paddingTop: '20px'
                    }}
                  >
                    <EventIcon />
                    <div style={{ fontSize: 14, flexGrow: 1 }}>Start*</div>
                    <span style={{ fontSize: 14, flexGrow: 1 }}>{formattedStartDate}</span>
                    <span style={{ fontSize: 14, flexGrow: 1 }}>{formattedStartTime}</span>
                  </div>
                ) : (
                  <span />
                )}
                {stepIndex === 3 ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      marginLeft: 48
                    }}
                  >
                    <div style={{ flexGrow: 1 }}>End</div>
                    <DatePicker
                      value={this.props.endDate ? this.props.endDate : null}
                      id="endDate"
                      textFieldStyle={{ width: '90px', marginLeft: 18 }}
                      style={{ flexGrow: 1 }}
                      ref="endDate"
                      onChange={this.handleEndChange}
                    />
                    <TimePicker
                      value={this.props.endTime ? this.props.endTime : null}
                      id="endTime"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      style={{ flexGrow: 1 }}
                      ref="endTime"
                      onChange={this.handleEndTimeChange}
                    />
                    <IconButton
                      onClick={() => this.resetDate('end')}
                      style={{ padding: '0px !important' }}
                      iconStyle={{ width: 12, height: 12 }}
                    >
                      <Clear />
                    </IconButton>
                  </div>
                ) : formattedEndDate !== '' && formattedEndTime !== '' ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      marginLeft: 48
                    }}
                  >
                    <div style={{ flexGrow: 1 }}>End</div>
                    <span style={{ fontSize: 14, flexGrow: 1 }}>{formattedEndDate}</span>
                    <span style={{ fontSize: 14, flexGrow: 1 }}>{formattedEndTime}</span>
                  </div>
                ) : (
                  <span />
                )}
                {this.renderStepActions(3, formattedStartDate !== '', () => this.resetAllDate())}
              </StepContent>
            </Step>
            <Step
              active={stepIndex >= 4}
              completed={stepIndex !== 4 && this.props.comment !== '' ? true : false}
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 4
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepCommentChild} />
              </StepButton>
              <StepContent>
                <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
                  {StepCommentContent}
                </div>
                {this.renderStepActions(4, null, () => this.resetComment())}
              </StepContent>
            </Step>
            <Step active={stepIndex >= 5}>
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 5
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepDamagesChild} />
              </StepButton>
              <StepContent>
                <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
                  <span style={{ width: '100%', fontSize: 14 }}>
                    {this.props.dictionary._damages_info}
                  </span>
                  <PersonIcon />
                  <span style={{ width: '80%', marginTop: 15, flexGrow: 1 }}>
                    {this.props.dictionary._people_affected}
                  </span>
                  <div style={{ maxWidth: 120, marginLeft: 48, marginTop: 8, flexGrow: 1 }}>
                    {this.props.dictionary._affected}:
                  </div>
                  <TextField
                    type="number"
                    min={0}
                    name="affected"
                    onChange={this.handlePeopleAffectedChange}
                    value={this.props.people_affected}
                    style={{ maxWidth: 80, maxHeight: 35 }}
                  />
                </div>
                <div
                  style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'flex-start',
                    marginLeft: 48
                  }}
                >
                  <div style={{ maxWidth: 120, marginTop: 8, flexGrow: 1 }}>
                    {this.props.dictionary._causalties}:
                  </div>
                  <TextField
                    type="number"
                    min={0}
                    name="causalties"
                    onChange={this.handleCausaltiesChange}
                    value={this.props.people_killed}
                    style={{ maxWidth: 80, maxHeight: 35 }}
                  />
                </div>
                <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
                  <MoneyIcon />
                  <TextField
                    type="number"
                    min={0}
                    name="estimated"
                    onChange={this.handleDamagesChange}
                    value={this.props.estimated_damage}
                    hintText={this.props.dictionary._estimated_damages}
                  />
                </div>
                {this.renderStepActions(5, null, () => this.resetDamages())}
              </StepContent>
            </Step>
          </Stepper>
        </div>
        <p
          style={{
            position: 'fixed',
            top: '86%',
            fontSize: 10
          }}
        >
          *{this.props.dictionary._mandatory_field}
        </p>
        {footerDrawer}
      </section>
    );
  }
}

export default enhance(NewEmergencyEventDrawerInner);
