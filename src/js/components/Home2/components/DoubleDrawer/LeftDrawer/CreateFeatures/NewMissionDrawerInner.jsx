import React, { Component } from 'react';
import DrawerFooter from 'js/components/app/drawer/DrawerFooter';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import { drawerNames } from 'js/modules/AreaOfInterest';
import MissionCreateStepper from 'js/components/Left/Missions/MissionCreateStepper';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withMissionCreate } from 'js/modules/missionCreate';
import { withTaskCreate } from 'js/modules/taskCreate';
import { withLoader, withMessages } from 'js/modules/ui';

import { compose } from 'redux';
const enhance = compose(
  withLeftDrawer,
  withLoader,
  withMessages,
  withMissionCreate,
  withTaskCreate
);

class NewMissionDrawerInner extends Component {
  resetMission = () => {
    this.props.resetMission();
    this.props.resetTask();
    this.resetState();
  };

  resetState = () => {
    const mapDraw = this.props.getMapDraw();
    if (this.props.drawPolygon.id) {
      const ids = [this.props.drawPoint.id, this.props.drawPolygon.id];
      if (mapDraw) mapDraw.delete(ids);
    } else {
      if (mapDraw) mapDraw.deleteAll();
    }
    this.props.resetHomeDrawState();
  };

  leftAction = () => {
    this.props.resetMission();
    this.resetState();
    this.props.history.push('/home/missions');
  };

  rightAction = () => {
    this.props.resetMission();
    this.resetState();
    this.props.history.push('/home/map');
  };

  render() {
    const {
      dictionary,
      enums,
      roles,
      user,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      getMapDraw,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory
    } = this.props;

    const drawProps = {
      getMapDraw,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory
    };

    let titleDrawer = null,
      listOfItems = null,
      footerDrawer = null;

    titleDrawer = (
      <DrawerTitle
        category={drawerNames.missionCreate}
        title={dictionary._new_mission}
        leftIcon="keyboard_backspace"
        backDrawerType={drawerNames.mission}
        rightIcon="close"
        rightAction={this.rightAction}
        leftAction={this.leftAction}
      />
    );
    listOfItems = (
      <MissionCreateStepper
        dictionary={dictionary}
        roles={roles}
        enums={enums}
        locale={this.props.locale}
        category={drawerNames.missionCreate}
        user={user}
        {...drawProps}
      />
    );
    footerDrawer = (
      <DrawerFooter
        entityId={1}
        leftLabel={dictionary._clear}
        clear={this.resetMission}
        customStyle={{ backgroundColor: 'black' }}
      />
    );

    return (
      <div style={{ backgroundColor: 'black' }}>
        {titleDrawer}
        <div style={{ overflowY: 'auto', overflowX: 'hidden', height: '80vh' }}>{listOfItems}</div>
        {footerDrawer}
      </div>
    );
  }
}

export default enhance(NewMissionDrawerInner);
