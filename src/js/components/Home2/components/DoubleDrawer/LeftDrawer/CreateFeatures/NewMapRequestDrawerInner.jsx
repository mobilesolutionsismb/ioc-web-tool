import React, { Component } from 'react';
import {
  FlatButton,
  TimePicker,
  RaisedButton,
  DatePicker,
  IconButton,
  RadioButtonGroup,
  RadioButton,
  SelectField,
  MenuItem,
  Checkbox,
  TextField
} from 'material-ui';
import { Step, Stepper, StepButton, StepContent, StepLabel } from 'material-ui/Stepper';

import { drawerNames } from 'js/modules/AreaOfInterest';
import { withMapRequestCreate } from 'js/modules/mapRequestCreate';
import { compose } from 'redux';
import { withMessages, withLoader } from 'js/modules/ui';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { InputAOIStep } from 'js/components/Steps';
import wellknown from 'wellknown';
import { API, withMapRequests } from 'ioc-api-interface';
import { getCompleteDateTime } from 'js/utils/getCompleteDateTime';
import { LocationOnIcon, EventIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import Clear from 'material-ui/svg-icons/content/clear';
import DrawerTitle from 'js/components/app/drawer/DrawerTitle';
import DrawerFooter from 'js/components/app/drawer/DrawerFooter';
import { AOILabel } from 'js/components/app/drawer/components/commons';
import { point, polygon } from '@turf/helpers';

const api = API.getInstance();

const enhance = compose(
  withLeftDrawer,
  withLoader,
  withMessages,
  withMapRequestCreate,
  withMapRequests
);
const time = [
  { value: 15, primaryText: '15 min' },
  { value: 30, primaryText: '30 min' },
  { value: 60, primaryText: '1 hour' },
  { value: 120, primaryText: '2 hours' },
  { value: 720, primaryText: '12 hours' },
  { value: 1440, primaryText: '24 hours' },
  { value: 2880, primaryText: '48 hours' }
];
const validationRules = {
  //2: Mandatory , 1: Optional, 0:set null
  flood: {
    forecast: [2, 2, 1, 1, 1],
    nowcast: [1, 1, 1, 1, 0],
    riskmap: [0, 0, 1, 1, 0],
    delineation: [0, 0, 1, 1, 0]
  },
  fire: {
    forecast: [2, 2, 1, 1, 0],
    nowcast: [1, 1, 1, 1, 0],
    riskmap: [0, 0, 0, 0, 0],
    burnedarea: [0, 0, 1, 1, 0]
  }
};
const minDate = new Date();
let maxDate = new Date();
maxDate.setDate(maxDate.getDate() + 7);
maxDate.setHours(23, 59, 59);

class NewMapRequestDrawerInner extends Component {
  state = {
    stepIndex: 0
  };
  componentWillUnmount() {
    maxDate = new Date();
    maxDate.setDate(maxDate.getDate() + 7);
    maxDate.setHours(23, 59, 59);
  }
  componentDidUpdate(prevProps) {
    if (prevProps.limitTimeWindow !== this.props.limitTimeWindow) {
      maxDate = new Date();
      if (this.props.limitTimeWindow !== null) {
        maxDate.setHours(minDate.getHours() + this.props.limitTimeWindow || 0);
        let nextStartDate = this.props.startDate ? this.props.startDate.getTime() : undefined;
        let nextStartTime = this.props.startTime ? this.props.startTime.getTime() : undefined;
        let nextEndDate = this.props.endDate ? this.props.endDate.getTime() : undefined;
        let nextEndTime = this.props.endTime ? this.props.endTime.getTime() : undefined;

        if (
          nextStartDate &&
          nextEndDate &&
          nextEndDate - nextStartDate > this.props.limitTimeWindow * 3600000
        ) {
          //Update end date with the new time window
          let newEndDate = new Date(this.props.startDate);
          newEndDate.setHours(this.props.startDate.getHours() + this.props.limitTimeWindow);
          //this.props.setMapRequestCreateEndDate(newEndDate);
        }
        if (
          nextStartTime &&
          nextEndTime &&
          nextEndTime - nextStartTime > this.props.limitTimeWindow * 3600000
        ) {
          //update end time with the new time window
          let newEndTime = new Date(this.props.startTime);
          newEndTime.setHours(this.props.startTime.getHours() + this.props.limitTimeWindow);
          //this.props.setMapRequestCreateEndTime(newEndTime);
        }
      } else {
        maxDate.setDate(maxDate.getDate() + 7);
      }
    } else {
      let currentEndTime = prevProps.endTime ? prevProps.endTime.getHours() : undefined;
      let nextEndTime2 = this.props.endTime ? this.props.endTime.getHours() : undefined;
      if (
        nextEndTime2 &&
        currentEndTime !== nextEndTime2 &&
        this.props.startTime !== null &&
        this.props.limitTimeWindow != null
      ) {
        var timeDiff = this.props.endTime.getHours() - this.props.startTime.getHours();
        //maxDate.setHours(minDate.getHours() + nextProps.limitTimeWindow || 0);
        if (timeDiff > this.props.limitTimeWindow || timeDiff < 0) {
          let newEndTime = new Date(this.props.startTime);
          newEndTime.setHours(this.props.startTime.getHours() + this.props.limitTimeWindow);
          //this.props.setMapRequestCreateEndTime(newEndTime);
        }
      }
    }
  }
  selectHigestValue = (current, compare) => {
    if (compare) {
      var maxLength = Math.max(current.length, compare.length);
      var _arr = [];
      for (var i = 0; i < maxLength; i++) {
        if (current[i] && compare[i]) {
          _arr[i] = Math.max(current[i], compare[i]);
        } else if (current[i]) {
          _arr[i] = current[i];
        } else {
          _arr[i] = compare[i];
        }
      }
      return _arr;
    } else {
      return current;
    }
  };

  getlabelsStatus = () => {
    let arr = [0, 0, 0, 0, 0];

    let restrictions = this.props.hazard !== '' && validationRules[this.props.hazard];
    if (!restrictions) {
      //if there is no hazard selected, all fields are temporally optional
      return [0, 0, 0, 0, 0];
    }
    this.props.frequency.forEach(element => {
      if (element.checked && element.value) {
        switch (element.name) {
          case '_is_type_delineation':
            arr = this.selectHigestValue(arr, restrictions.delineation);
            break;
          case '_is_type_burned_area':
            arr = this.selectHigestValue(arr, restrictions.burnedarea);
            break;
          case '_is_type_nowcast':
            arr = this.selectHigestValue(arr, restrictions.nowcast);
            break;
          case '_is_type_forecast':
            arr = this.selectHigestValue(arr, restrictions.forecast);
            break;
          case '_is_type_risk_map':
            arr = this.selectHigestValue(arr, restrictions.riskmap);
            break;

          default:
            break;
        }
      }
    });
    return arr;
  };

  handleNext = () => {
    const { stepIndex } = this.state;
    if (stepIndex < 5) {
      this.setState({
        stepIndex: stepIndex + 1
      });
    }
  };

  renderStepActions(step, enabled, clear) {
    return (
      <div style={{ margin: '12px 0' }}>
        {this.state.stepIndex === step &&
          step < 4 && (
            <RaisedButton
              disabled={!enabled}
              label={this.props.dictionary._next}
              disableTouchRipple={true}
              disableFocusRipple={true}
              primary={true}
              onClick={this.handleNext}
              style={{ marginRight: 12 }}
            />
          )}

        {this.state.stepIndex === step && (
          <FlatButton
            label={this.props.dictionary._cancel}
            disableTouchRipple={true}
            disableFocusRipple={true}
            onClick={clear}
          />
        )}
      </div>
    );
  }

  resetDate = fields => {
    if (fields === 'start') {
      this.props.resetMapRequestCreateAllSDate();
    } else if (fields === 'end') {
      this.props.resetMapRequestCreateAllEDate();
    } else if (fields === 'event') {
      this.props.resetEventStartDate();
    } else if (fields === 'exArea') {
      this.props.resetEventExAreaDate();
    }
  };
  resetHazard = () => {
    this.props.setMapRequestCreateHazard('');
  };

  resetAOI = () => {
    this.props.setDrawTrash(this.props.category, true);
  };
  setHazard = (event, value) => {
    this.props.setMapRequestCreateHazard(value);
  };
  resetAllDate = () => {
    this.props.resetDate();
  };
  resetFrequency = () => {
    this.props.resetFrequency();
  };
  resetStep5 = () => {
    this.props.resetStep5();
    this.resetState();
  };
  handleStartChange = (event, startDate) => {
    this.props.setMapRequestCreateStartDate(startDate);
  };

  handleStartTimeChange = (event, startTime) => {
    this.props.setMapRequestCreateStartTime(startTime);
  };

  handleEndChange = (event, endDate) => {
    this.props.setMapRequestCreateEndDate(endDate);
  };

  handlestep5TimeChange = (event, time, type) => {
    if (type === 'event') {
      this.props.setMapRequestCreateEventStartTime(time);
    } else if (type === 'exArea') {
      this.props.setMapRequestCreateExAreaTime(time);
    }
  };
  handlestep5DateChange = (event, date, type) => {
    if (type === 'event') {
      this.props.setMapRequestCreateEventStartDate(date);
    } else if (type === 'exArea') {
      this.props.setMapRequestCreateExAreaDate(date);
    }
  };

  handleEndTimeChange = (event, endTime) => {
    this.props.setMapRequestCreateEndTime(endTime);
  };
  handleOriginFlowChange = (event, number) => {
    this.props.setMapRequestCreateOF(number);
  };
  convertDate = date => {
    let formattedDate = '';
    if (date !== null) {
      const yyyy = date.getFullYear().toString();
      const mm = (date.getMonth() + 1).toString();
      const dd = date.getDate().toString();

      const mmChars = mm.split('');
      const ddChars = dd.split('');

      formattedDate =
        yyyy +
        '-' +
        (mmChars[1] ? mm : '0' + mmChars[0]) +
        '-' +
        (ddChars[1] ? dd : '0' + ddChars[0]);
    }
    return formattedDate;
  };
  formatAMPM = date => {
    let formattedTime = '';
    if (date !== null) {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      formattedTime = hours + ':' + minutes + ' ' + ampm;
    }
    return formattedTime;
  };

  resetMapRequest = () => {
    this.props.resetMapRequestCreate();
    this.resetState();
  };

  leftAction = () => {
    this.resetState();
    this.props.history.push('/home/map_requests');
  };

  rightAction = () => {
    this.props.resetMapRequestCreate();
    this.resetState();
    this.props.history.push('/home/map');
  };

  resetState = () => {
    const mapDraw = this.props.getMapDraw();
    if (this.props.drawPolygon.id) {
      let ids = [this.props.drawPoint.id, this.props.drawPolygon.id];
      if (this.props.drawEventPoint.id) ids.push(this.props.drawEventPoint.id);
      if (this.props.drawEventArea.id) ids.push(this.props.drawEventArea.id);
      if (mapDraw) mapDraw.delete(ids);
    } else {
      if (mapDraw) mapDraw.deleteAll();
    }
    this.props.resetHomeDrawState();
  };

  createMapRequest = async restrictions => {
    try {
      let { translations } = this.props.dictionary;
      let areaOfInterest = '';

      if (!this.props.drawPolygon.id) {
        this.props.pushMessage('Select the Area of interest', 'error', null);
        return;
      } else {
        areaOfInterest = wellknown.stringify(polygon(this.props.drawPolygon.coordinates));
      }
      let start, end;
      if (this.props.startDate) {
        if (this.props.startTime) {
          start = getCompleteDateTime(
            this.props.startDate,
            this.props.startTime,
            this.props.locale
          );
        } else {
          this.props.pushMessage('Start Time mandatory', 'error', null);
          return;
        }
      } else {
        this.props.pushMessage('Start Date mandatory', 'error', null);
        return;
      }

      if (this.props.endDate) {
        if (this.props.endTime) {
          end = getCompleteDateTime(this.props.endDate, this.props.endTime, this.props.locale);
        } else {
          this.props.pushMessage('End Time mandatory', 'error', null);
          return;
        }
      } else {
        this.props.pushMessage('End Date mandatory', 'error', null);
        return;
      }

      if (end.isBefore(start)) {
        this.props.pushMessage('Start Date must be before the End date', 'error', null);
        return;
      } else if (end.diff(start, 'days') > 14) {
        this.props.pushMessage('The interval cannot be longer than 2 weeks', 'error', null);
        return;
      }

      if (this.props.hazard === '') {
        this.props.pushMessage('Hazard type mandatory', 'error', null);
        return;
      }
      let isOneEmpty = false;
      let noneSelected = true;
      let delineation, burnedArea, nowcast, forecast, riskMap;
      this.props.frequency.forEach(element => {
        if (element.checked) {
          if (element.value === undefined) {
            if (
              (this.props.hazard === 'flood' && element.name === '_is_type_burned_area') ||
              (this.props.hazard === 'fire' && element.name === '_is_type_delineation')
            ) {
            } else {
              this.props.pushMessage('The frequencies checked must be value', 'error', null);
              noneSelected = false;
              isOneEmpty = true;
            }

            return;
          } else if (element.value !== undefined) {
            switch (element.name) {
              case '_is_type_delineation':
                if (this.props.hazard === 'flood') {
                  delineation = element.value;
                  noneSelected = false;
                }
                break;
              case '_is_type_burned_area':
                if (this.props.hazard === 'fire') {
                  burnedArea = element.value;
                  noneSelected = false;
                }
                break;
              case '_is_type_nowcast':
                nowcast = element.value;
                noneSelected = false;
                break;
              case '_is_type_forecast':
                forecast = element.value;
                noneSelected = false;
                break;
              case '_is_type_risk_map':
                riskMap = element.value;
                noneSelected = false;
                break;
              default:
                break;
            }
          }
        }
      });
      if (noneSelected) {
        this.props.pushMessage('Layers and frequency mandatory', 'error', null);
        return;
      }
      if (isOneEmpty) {
        return;
      }
      let values = [
        this.getDateTimeOrNull(
          this.props.eventStartDate,
          this.props.eventStartTime,
          this.props.locale
        ),
        this.props.drawEventPoint.id
          ? wellknown.stringify(point(this.props.drawEventPoint.coordinates))
          : undefined,
        this.getDateTimeOrNull(
          this.props.eventExAreaDate,
          this.props.eventExAreaTime,
          this.props.locale
        ),
        this.props.drawEventArea.id &&
          wellknown.stringify(polygon(this.props.drawEventArea.coordinates)),
        this.props.origin_flow !== '' ? this.props.origin_flow : undefined
      ];
      for (let x = 0; x <= 4; x++) {
        switch (restrictions[x]) {
          case 0:
            values[x] = undefined;
            break;
          case 2:
            if (values[x] === undefined) {
              let label = '';
              switch (x) {
                case 0:
                  label = translations.event_start_time;
                  break;
                case 1:
                  label = translations.event_area;
                  break;
                case 2:
                  label = translations.event_expansion_time;
                  break;
                case 3:
                  label = translations.event_area;
                  break;
                case 4:
                  label = translations.origin_flow;
                  break;

                default:
                  break;
              }
              this.props.pushMessage(`${label} is mandatory`, 'error', null);
              return;
            }
            break;

          default:
            break;
        }
      }
      //let EventStartTime, EventInitialLocation, EventExpansionTime, EventExpansionArea, Originflow= undefined;
      //console.log(values);
      let newMapRequest = {
        areaOfInterest,
        start,
        end,
        hazard: this.props.hazard,
        delineation,
        burnedArea,
        nowcast,
        forecast,
        riskMap,
        eventInitialLocation: values[1],
        eventStartTime: values[0],
        eventExpansionArea: values[3],
        eventExpansionTime: values[2],
        originFlow: values[4]
      };
      //console.log(JSON.stringify(newMapRequest));
      await api.mapRequest.create(newMapRequest);
      await this.props.updateMapRequests(false, this.props.loadingStart, this.props.loadingStop);
      this.props.pushMessage(
        this.props.dictionary._map_request_create_success,
        'success',
        null,
        null
      );
      this.props.history.push('/home/map_requests');
      this.props.resetMapRequestCreate();
      this.resetState();
      this.props.resetFrequency();
    } catch (error) {
      this.props.pushMessage(error, 'error', null, null);
    }
  };
  getDateTimeOrNull = (date, time, locale) => {
    if (date && time) {
      return getCompleteDateTime(date, time, locale);
    } else {
      return undefined;
    }
  };
  handlecheck = (event, isInputChecked, element) => {
    element.checked = isInputChecked;
    this.props.setMapReqCreateFrequency(element);
  };
  handleChange = (value, element) => {
    element.value = value;
    this.props.setMapReqCreateFrequency(element);
  };
  isValidFrequency = () => {
    let num = this.props.frequency.findIndex(x => {
      return x.checked === true && x.value !== undefined;
    });
    return num !== -1;
  };
  render() {
    let restrictions = this.getlabelsStatus();
    //console.log(restrictions);
    let {
      dictionary,
      drawPoint,
      drawPolygon,
      drawEventPoint,
      drawEventArea,
      resetHomeDrawState,
      setDrawEventArea,
      setIsDrawing,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon,
      getMapDraw,
      setDrawEventPoint
    } = this.props;

    const drawProps = {
      getMapDraw,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory,
      drawEventPoint,
      drawEventArea,
      setDrawEventArea,
      setDrawEventPoint
    };
    const { stepIndex } = this.state;

    const address =
      drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const addressToDraw = stepIndex > 0 ? address : undefined;
    const StepAOIChild = <AOILabel text={addressToDraw} dictionary={dictionary} />;

    const stepAOIVisible =
      stepIndex === 0 ? (
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
          <LocationOnIcon style={{ paddingTop: 25 }} />
          <div style={{ maxWidth: 256 }}>
            <InputAOIStep
              isDisabled={drawPolygon.id !== undefined}
              category={this.props.category}
              address={address}
              {...drawProps}
              resetCategory="base"
            />
          </div>
        </div>
      ) : (
        <div />
      );

    const formattedStartDate = this.convertDate(this.props.startDate);
    const formattedEndDate = this.convertDate(this.props.endDate);
    const formattedStartTime = this.formatAMPM(this.props.startTime);
    const formattedEndTime = this.formatAMPM(this.props.endTime);
    const StepTimeChild = (
      <div>
        <div>
          <span>{dictionary._event_time} *</span>
        </div>
      </div>
    );
    const StepLabelChild = (
      <div>
        <span>{dictionary._hazard} *</span>
      </div>
    );
    const StemFrequencyChild = (
      <div>
        <div>
          <span>{dictionary.layers_and_desidered_frequency} *</span>
        </div>
      </div>
    );
    const Step5Child = (
      <div>
        <div>
          <span>{dictionary._event_details}</span>
        </div>
      </div>
    );
    const addresStep5 =
      drawEventPoint && drawEventPoint.coordinates
        ? '[ ' +
          Math.round(drawEventPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawEventPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';

    const titleDrawer = (
      <DrawerTitle
        category={this.props.primaryDrawerType}
        title={dictionary._new_map_request}
        associatedFirstDrawerName={drawerNames.mapRequests}
        leftIcon="keyboard_backspace"
        backDrawerType={drawerNames.mapRequests}
        rightIcon="close"
        rightAction={this.rightAction}
        leftAction={this.leftAction}
      />
    );
    const footerDrawer = (
      <DrawerFooter
        category={this.props.primaryDrawerType}
        leftLabel={dictionary._clear}
        rightLabel={dictionary._save}
        apply={() => this.createMapRequest(restrictions)}
        clear={this.resetMapRequest}
      />
    );
    return (
      <section>
        {titleDrawer}
        <div className="drawerCreateEventContent">
          <Stepper activeStep={stepIndex} linear={false} orientation="vertical">
            <Step
              active={stepIndex >= 0}
              completed={stepIndex !== 0 && address !== '' ? true : false}
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 0
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepAOIChild} />
              </StepButton>
              <StepContent>
                {stepAOIVisible}
                {this.renderStepActions(0, address !== '', () => this.resetAOI())}
              </StepContent>
            </Step>
            <Step
              active={stepIndex >= 1}
              completed={
                stepIndex !== 1 &&
                formattedStartDate !== '' &&
                formattedStartTime !== '' &&
                formattedEndDate !== '' &&
                formattedEndTime !== ''
                  ? true
                  : false
              }
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 1
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepTimeChild} />
              </StepButton>
              <StepContent style={{ width: '300px' }}>
                {stepIndex === 1 ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      paddingTop: '20px'
                    }}
                  >
                    <EventIcon />
                    <div style={{ flexGrow: 1 }}>Start*</div>
                    <DatePicker
                      // minDate={minDate}
                      // maxDate={maxDate}
                      value={this.props.startDate ? this.props.startDate : null}
                      id="startDate"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      style={{ flexGrow: 1 }}
                      ref="startDate"
                      onChange={this.handleStartChange}
                    />
                    <TimePicker
                      value={this.props.startTime ? this.props.startTime : null}
                      id="startTime"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      style={{ flexGrow: 1 }}
                      ref="startTime"
                      onChange={this.handleStartTimeChange}
                    />
                    <IconButton
                      onClick={() => this.resetDate('start')}
                      style={{ padding: '0px !important' }}
                      iconStyle={{ width: 12, height: 12 }}
                    >
                      <Clear />
                    </IconButton>
                  </div>
                ) : formattedStartDate !== '' && formattedStartTime !== '' ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      paddingTop: '20px'
                    }}
                  >
                    <EventIcon />
                    <div style={{ fontSize: 14, flexGrow: 1 }}>Start*</div>
                    <span style={{ fontSize: 14, flexGrow: 1 }}>{formattedStartDate}</span>
                    <span style={{ fontSize: 14, flexGrow: 1 }}>{formattedStartTime}</span>
                  </div>
                ) : (
                  <span />
                )}
                {stepIndex === 1 ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      marginLeft: 48
                    }}
                  >
                    <div style={{ flexGrow: 1 }}>End*</div>
                    <DatePicker
                      // minDate={minDate}
                      // maxDate={maxDate}
                      value={this.props.endDate ? this.props.endDate : null}
                      id="endDate"
                      textFieldStyle={{ width: '90px', marginLeft: 18 }}
                      style={{ flexGrow: 1 }}
                      ref="endDate"
                      onChange={this.handleEndChange}
                    />
                    <TimePicker
                      value={this.props.endTime ? this.props.endTime : null}
                      id="endTime"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      style={{ flexGrow: 1 }}
                      ref="endTime"
                      onChange={this.handleEndTimeChange}
                    />
                    <IconButton
                      onClick={() => this.resetDate('end')}
                      style={{ padding: '0px !important' }}
                      iconStyle={{ width: 12, height: 12 }}
                    >
                      <Clear />
                    </IconButton>
                  </div>
                ) : formattedEndDate !== '' && formattedEndTime !== '' ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      marginLeft: 48
                    }}
                  >
                    <div style={{ flexGrow: 1 }}>End*</div>
                    <span style={{ fontSize: 14, flexGrow: 1 }}>{formattedEndDate}</span>
                    <span style={{ fontSize: 14, flexGrow: 1 }}>{formattedEndTime}</span>
                  </div>
                ) : (
                  <span />
                )}
                {this.renderStepActions(
                  1,
                  formattedStartDate !== '' &&
                    formattedStartTime !== '' &&
                    formattedEndDate !== '' &&
                    formattedEndTime !== '',
                  () => this.resetAllDate()
                )}
              </StepContent>
            </Step>
            <Step
              active={stepIndex >= 2}
              completed={stepIndex !== 2 && this.props.hazard !== '' ? true : false}
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 2
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StepLabelChild} />
              </StepButton>
              <StepContent>
                {stepIndex === 2 ? (
                  <RadioButtonGroup
                    name="shipSpeed"
                    valueSelected={this.props.hazard}
                    onChange={this.setHazard}
                  >
                    <RadioButton value="flood" label={dictionary._hazard_flood} />
                    <RadioButton value="fire" label={dictionary._hazard_fire} />
                  </RadioButtonGroup>
                ) : (
                  <div>
                    {this.props.hazard !== '' && dictionary[`_hazard_${this.props.hazard}`]}
                  </div>
                )}

                {this.renderStepActions(2, this.props.hazard !== '', () => this.resetHazard())}
              </StepContent>
            </Step>
            <Step
              active={stepIndex >= 3}
              completed={stepIndex !== 3 && this.isValidFrequency() ? true : false}
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 3
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={StemFrequencyChild} />
              </StepButton>
              <StepContent style={{ width: '300px' }}>
                {stepIndex === 3 ? (
                  <div style={{ marginBottom: 50 }}>
                    {this.props.frequency.map(element => {
                      return (element.name === '_is_type_delineation' &&
                        this.props.hazard === 'fire') ||
                        (element.name === '_is_type_burned_area' &&
                          this.props.hazard === 'flood') ? (
                        <div key={element.name} />
                      ) : (
                        <div
                          key={element.name}
                          style={{ display: 'flex', justifyContent: 'space-around', height: 50 }}
                        >
                          <Checkbox
                            style={{ display: 'block', height: 30, marginTop: 42 }}
                            labelStyle={{ height: 30 }}
                            label={dictionary[element.name]}
                            checked={element.checked}
                            onCheck={(event, isInputChecked) => {
                              this.handlecheck(event, isInputChecked, element);
                            }}
                          />
                          <SelectField
                            floatingLabelText="Frequency"
                            disabled={!element.checked}
                            value={element.value}
                            onChange={(event, key, value) => this.handleChange(value, element)}
                          >
                            {time.map(item => {
                              return (
                                <MenuItem
                                  key={item.value}
                                  value={item.value}
                                  primaryText={item.primaryText}
                                />
                              );
                            })}
                          </SelectField>
                        </div>
                      );
                    })}
                  </div>
                ) : (
                  <div>
                    {this.props.frequency.map((element, i) => {
                      return (
                        element.checked &&
                        element.value && (
                          <div key={i}>
                            {dictionary[element.name]}{' '}
                            {time.find(x => x.value === element.value).primaryText}
                          </div>
                        )
                      );
                    })}
                  </div>
                )}
                {this.renderStepActions(3, this.isValidFrequency(), () => this.resetFrequency())}
              </StepContent>
            </Step>
            <Step
              active={stepIndex >= 4}
              completed={
                stepIndex !== 4 &&
                addresStep5 !== '' &&
                this.props.origin_flow !== '' &&
                this.props.origin_flow > 0 &&
                this.props.eventStartDate &&
                this.props.eventStartTime &&
                this.props.eventExAreaDate &&
                this.props.eventExAreaTime
                  ? true
                  : false
              }
            >
              <StepButton
                onClick={() =>
                  this.setState({
                    stepIndex: 4
                  })
                }
              >
                <StepLabel style={{ color: 'white' }} children={Step5Child} />
              </StepButton>
              <StepContent>
                <div>
                  <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
                    <div
                      style={{
                        color: restrictions[1] === 0 && restrictions[3] === 0 ? 'gray' : 'white'
                      }}
                    >
                      {dictionary.event_area}
                      {restrictions[1] === 2 || restrictions[3] === 2 ? '*' : ''}
                    </div>
                    <div style={{ maxWidth: 256 }}>
                      <InputAOIStep
                        isDisabled={restrictions[2] === 0 && restrictions[4] === 0}
                        getMapDraw={this.props.getMapDraw}
                        category={this.props.category}
                        drawPolygon={drawEventArea}
                        drawPoint={drawEventPoint}
                        resetHomeDrawState={resetHomeDrawState}
                        setDrawPoint={setDrawEventPoint}
                        setDrawPolygon={setDrawEventArea}
                        setDrawCategory={setDrawCategory}
                        setIsDrawing={setIsDrawing}
                        resetCategory="eventArea"
                        pushMessage={this.props.pushMessage}
                      />
                    </div>
                  </div>
                  <div style={{ color: restrictions[0] === 0 ? 'gray' : 'white' }}>
                    {dictionary.event_start_time}
                    {restrictions[0] === 2 ? '*' : ''}
                  </div>
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <DatePicker
                      disabled={restrictions[0] === 0}
                      value={this.props.eventStartDate ? this.props.eventStartDate : null}
                      id="eventStartDate"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      ref="eventStartDate"
                      onChange={(event, date) => {
                        this.handlestep5DateChange(event, date, 'event');
                      }}
                    />
                    <TimePicker
                      disabled={restrictions[0] === 0}
                      value={this.props.eventStartTime ? this.props.eventStartTime : null}
                      id="eventStartTime"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      ref="eventStartTime"
                      onChange={(event, time) => {
                        this.handlestep5TimeChange(event, time, 'event');
                      }}
                    />
                    <IconButton
                      disabled={restrictions[0] === 0}
                      onClick={() => this.resetDate('event')}
                      style={{ padding: '0px !important' }}
                      iconStyle={{ width: 12, height: 12 }}
                    >
                      <Clear />
                    </IconButton>
                  </div>
                  <div style={{ flexGrow: 1, color: restrictions[2] === 0 ? 'gray' : 'white' }}>
                    {dictionary.event_expansion_time}
                    {restrictions[2] === 2 ? '*' : ''}
                  </div>
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <DatePicker
                      disabled={restrictions[2] === 0}
                      value={this.props.eventExAreaDate ? this.props.eventExAreaDate : null}
                      id="eventExAreaDate"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      ref="eventExAreaDate"
                      onChange={(event, date) => {
                        this.handlestep5DateChange(event, date, 'exArea');
                      }}
                    />
                    <TimePicker
                      disabled={restrictions[2] === 0}
                      value={this.props.eventExAreaTime ? this.props.eventExAreaTime : null}
                      id="eventExAreaTime"
                      textFieldStyle={{ width: '90px', marginLeft: 5 }}
                      ref="eventExAreaTime"
                      onChange={(event, time) => {
                        this.handlestep5TimeChange(event, time, 'exArea');
                      }}
                    />
                    <IconButton
                      disabled={restrictions[2] === 0}
                      onClick={() => this.resetDate('exArea')}
                      style={{ padding: '0px !important' }}
                      iconStyle={{ width: 12, height: 12 }}
                    >
                      <Clear />
                    </IconButton>
                  </div>
                  <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
                    <div
                      style={{
                        maxWidth: 120,
                        marginTop: 8,
                        flexGrow: 1,
                        color: restrictions[4] === 0 ? 'gray' : 'white'
                      }}
                    >
                      {dictionary.origin_flow}
                      {restrictions[4] === 2 ? '*' : ''}
                    </div>
                    <TextField
                      disabled={restrictions[4] === 0}
                      type="number"
                      min={0}
                      name="originflow"
                      onChange={this.handleOriginFlowChange}
                      value={this.props.origin_flow}
                      style={{ maxWidth: 40, maxHeight: 35 }}
                    />
                  </div>
                </div>

                {this.renderStepActions(4, null !== '', () => this.resetStep5())}
              </StepContent>
            </Step>
          </Stepper>
        </div>
        <p
          style={{
            position: 'fixed',
            top: '86%',
            fontSize: 10
          }}
        >
          *{this.props.dictionary._mandatory_field}
        </p>
        {footerDrawer}
      </section>
    );
  }
}

export default enhance(NewMapRequestDrawerInner);
