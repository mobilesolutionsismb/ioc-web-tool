import React, { Component } from 'react';
import { compose } from 'redux';

import { LeftDrawer } from './LeftDrawer';
// import DrawerLeft from 'js/components/Left/DrawerLeft';
import DrawerRight from 'js/components/Right/DrawerRight';
import { withEnumsAndRoles, withEmergencyEvents } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';

const enhance = compose(
  withEnumsAndRoles,
  withDictionary,
  withEmergencyEvents
);

class DoubleDrawer extends Component {
  render() {
    const {
      dictionary,
      enums,
      roles,
      getMap,
      getMapDraw,
      activeEventId,
      deselectEvent,
      selectFeature,
      deselectFeature,
      selectedFeature,
      mouseEnterFeature,
      mouseLeaveFeature,
      hoveredFeature,
      /////////////////
      selectedMissionTask,
      selectMissionTask,
      deselectMissionTask,
      hoveredMissionTask,
      mouseEnterMissionTask,
      mouseLeaveMissionTask,
      /////////////////////
      selectedReport,
      selectReport,
      deselectReport,
      hoveredReport,
      mouseEnterReport,
      mouseLeaveReport,
      //////////////////////
      emergencyEvents,
      getEmergencyEvents,
      location,
      history,
      match,
      drawCategory,
      drawPoint,
      drawPolygon,
      drawTargetPoint,
      drawTargetPolygon,
      drawEventPoint,
      drawEventArea,
      resetHomeDrawState,
      setDrawEventPoint,
      setDrawEventArea,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon,
      setDrawTargetPoint,
      setDrawTargetPolygon,
      setDrawTask,
      setIsDrawing
    } = this.props;

    const featureProps = {
      getMap,
      getMapDraw,
      activeEventId,
      deselectEvent,
      selectFeature,
      deselectFeature,
      selectedFeature,
      mouseEnterFeature,
      mouseLeaveFeature,
      hoveredFeature,
      selectedMissionTask,
      selectMissionTask,
      deselectMissionTask,
      hoveredMissionTask,
      mouseEnterMissionTask,
      mouseLeaveMissionTask,
      selectedReport,
      selectReport,
      deselectReport,
      hoveredReport,
      mouseEnterReport,
      mouseLeaveReport
    };

    const routerProps = {
      location,
      history,
      match
    };

    const drawProps = {
      drawCategory,
      drawPoint,
      drawPolygon,
      drawTargetPoint,
      drawTargetPolygon,
      drawEventPoint,
      drawEventArea,
      resetHomeDrawState,
      setDrawEventPoint,
      setDrawEventArea,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon,
      setDrawTargetPoint,
      setDrawTargetPolygon,
      setIsDrawing,
      setDrawTask
    };

    return [
      <LeftDrawer
        key="dr-left"
        roles={roles}
        enums={enums}
        dictionary={dictionary}
        locale={this.props.locale}
        {...featureProps}
        {...routerProps}
        {...drawProps}
        getMap={getMap}
        getMapDraw={getMapDraw}
        emergencyEvents={emergencyEvents}
        getEmergencyEvents={getEmergencyEvents}
      />,
      <DrawerRight
        key="dr-right"
        enums={enums}
        dictionary={dictionary}
        getMap={getMap}
        getMapDraw={getMapDraw}
        activeEventId={activeEventId}
        {...drawProps}
        {...routerProps}
      />
    ];
  }
}

export default enhance(DoubleDrawer);
