import { search, findNearbyPlaceName } from 'js/utils/geonames.utils';
import { useAPI } from 'js/hooks/use-api.hook';
import { useLocale } from 'js/hooks/use-locale.hook';

function geocodingValidator({ q }) {
  return typeof q === 'string' && q.length >= 3;
}

export function useGeocoding(locationName = '') {
  const [{ locale }] = useLocale();

  const [data, error, loading] = useAPI(
    search,
    {
      q: locationName,
      maxRows: 10,
      lan: locale,
      lang: locale,
      searchlang: locale /* countryBias: 'should detect country' */
    },
    [],
    geocodingValidator
  );
  return [data, error, loading];
}

function reverseGeocodingValidator({ lat, lng }) {
  return !(isNaN(lng) || isNaN(lat));
}

export function useReverseGeocoding(lat = NaN, lng = NaN) {
  const [{ locale }] = useLocale();

  const [data, error, loading] = useAPI(
    findNearbyPlaceName,
    { lat, lng, lan: locale },
    [],
    reverseGeocodingValidator
  );
  return [data, error, loading];
}
