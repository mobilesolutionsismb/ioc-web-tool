import React from 'react';
import deburr from 'lodash/deburr';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from 'material-ui/TextField';
import { MenuItem } from 'material-ui/Menu';
import styled from 'styled-components';
import { useTheme } from 'js/startup/Root';

const PrimaryText = styled.div`
  height: 48px;
  width: 100%;
  div {
    line-height: 24px;
    font-size: 16px;
    height: 50%;
  }

  & .upper {
    .token {
      color: ${props => props.theme.palette.primary1Color};
    }
  }

  & .lower {
    font-weight: lighter;
    font-size: 12px;
  }
`;

export function renderInputComponent(inputProps) {
  return <TextField {...inputProps} />;
}

function SuggestionMenuItem({ text = '', secondaryText = '' }) {
  const theme = useTheme();
  return (
    <MenuItem
      primaryText={
        <PrimaryText theme={theme}>
          <div className="upper">{text}</div>
          <div className="lower">{secondaryText}</div>
        </PrimaryText>
      }
    />
  );
}

export function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion.name, query);
  const parts = parse(suggestion.name, matches);
  const text = parts.map((part, index) => (
    <span key={String(index)} className={part.highlight ? 'token' : ''}>
      {part.text}
    </span>
  ));

  return (
    <SuggestionMenuItem
      text={text}
      secondaryText={[suggestion.adminName2, suggestion.adminName1, suggestion.countryName]
        .filter(x => (typeof x === 'string' ? x.length > 0 : false))
        .join(', ')}
    />
  );
}

export function getSuggestions(suggestions) {
  return value => {
    const inputValue = deburr(value.trim()).toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    return inputLength === 0
      ? []
      : suggestions.filter(suggestion => {
          const keep =
            count < 5 && suggestion.name.slice(0, inputLength).toLowerCase() === inputValue;

          if (keep) {
            count += 1;
          }

          return keep;
        });
  };
}
