import React, { useState, useEffect, useRef } from 'react';
import { renderInputComponent, renderSuggestion, getSuggestions } from './geosearch.helpers';
import styled from 'styled-components';
import Autosuggest from 'react-autosuggest';
import { useGeocoding } from './geosearch.hooks';
import { logError, logWarning } from 'js/utils/log';
import Popover /* , { PopoverAnimationVertical }  */ from 'material-ui/Popover/Popover';
import { Menu } from 'material-ui/Menu';
import nop from 'nop';
import { useLocale } from 'js/hooks/use-locale.hook';

const GeosearchRoot = styled.div`
  height: 100%;
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 8px;
  background-color: ${props => props.theme.palette.canvasColor};
`;

const GeosearchWrapper = styled.div`
  position: relative;
  top: -40px;
  left: 48px;
  transition: width 0.35s cubic-bezier(0.755, 0.05, 0.855, 0.06);
  width: ${props => (props.expanded ? 290 : 0)}px;
`;

const StyledPopover = styled(Popover).attrs(() => ({ className: 'autosuggest-popover' }))`
  ul.react-autosuggest__suggestions-list {
    list-style-type: none;
    padding-inline-start: 0px;

    li.react-autosuggest__suggestion {
      display: block;
      text-align: initial;
    }
  }
`;

function GeosearchInner({ onLocationFound = nop }) {
  const [suggestions, setSuggestions] = useState([]);
  const [popper, setPopper] = useState('');
  const [{ dictionary }] = useLocale();

  const [value, setValue] = useState('');
  const [data, error, loading] = useGeocoding(value);
  const popperNode = useRef(null);

  function onBlur(event, { highlightedSuggestion }) {
    if (!highlightedSuggestion) {
      setPopper('');
    }
  }

  function onChange(event, { newValue }) {
    setPopper(newValue);
  }

  useEffect(() => {
    if (error) {
      logError(error);
    }
  }, [error]);

  useEffect(() => {
    if (loading) {
      logWarning('Loading names...');
    }
  }, [loading]);

  useEffect(() => {
    if (data) {
      console.log('DATA', data, 'FOR', value);
      const suggestions = Array.isArray(data.geonames) ? data.geonames : [];
      setSuggestions(getSuggestions(suggestions)(value));
    }
  }, [data]);

  function getSuggestionValue(suggestion) {
    const { name, lat, lng, bbox } = suggestion;
    onLocationFound({ name, lat: parseFloat(lat, 10), lng: parseFloat(lng, 10), bbox });
    return name;
  }

  function handleSuggestionsFetchRequested({ value }) {
    setValue(value);
  }

  function handleSuggestionsClearRequested() {
    setSuggestions([]);
  }

  const autosuggestProps = {
    renderInputComponent,
    suggestions,
    onSuggestionsFetchRequested: handleSuggestionsFetchRequested,
    onSuggestionsClearRequested: handleSuggestionsClearRequested,
    getSuggestionValue: getSuggestionValue,
    renderSuggestion
  };

  return (
    <GeosearchRoot>
      <Autosuggest
        {...autosuggestProps}
        inputProps={{
          type: 'search',
          hintText: `${dictionary._search}...`,
          value: popper,
          onChange,
          onBlur
        }}
        renderSuggestionsContainer={options => (
          <StyledPopover
            anchorEl={popperNode.current}
            open={popperNode.current !== null && Boolean(options.children)}
          >
            <Menu
              {...options.containerProps}
              style={{ width: popperNode.current ? popperNode.current.clientWidth : null }}
            >
              {options.children}
            </Menu>
          </StyledPopover>
        )}
      />
      <div ref={popperNode} />
    </GeosearchRoot>
  );
}

export function Geosearch({ expanded, onLocationFound }) {
  return (
    <GeosearchWrapper expanded={expanded}>
      {expanded && <GeosearchInner onLocationFound={onLocationFound} />}
    </GeosearchWrapper>
  );
}
