class PointMode {
  // When the mode starts this function will be called.
  // The `opts` argument comes from `draw.changeMode('lotsofpoints', {count:7})`.
  // The value returned should be an object and will be passed to all other lifecycle functions
  constructor(props) {
    this.pushMessage = props.pushMessage;
    this.setIsDrawing = props.setIsDrawing;
  }

  onSetup = function(opts) {
    var state = {
      point: null
    };
    this.setDrawPoint = opts.setDrawPoint;
    this.setDrawPolygon = opts.setDrawPolygon;
    this.setDrawCategory = opts.setDrawCategory;
    this.category = opts.category;
    this.setIsDrawing(true);
    this.pushMessage('Click on the center of AOI on the map', 'success', null);
    return state;
  };

  // Whenever a user clicks on the map, Draw will call `onClick`
  onClick = function(state, e) {
    // `this.newFeature` takes geojson and makes a DrawFeature
    if (!state.point) {
      const point = this.newFeature({
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'Point',
          coordinates: [e.lngLat.lng, e.lngLat.lat]
        }
      });

      state.point = point;
      this.addFeature(point); // puts the point on the map
      this.setDrawPoint(point);
      this.changeMode('polygon_mode', {
        pushMessage: this.pushMessage,
        setDrawPolygon: this.setDrawPolygon,
        setDrawCategory: this.setDrawCategory,
        setIsDrawing: this.setIsDrawing,
        category: this.category
      });
    }
  };

  toDisplayFeatures = function(state, geojson, display) {
    display(geojson);
  };
}

export default PointMode;
