import './styles.scss';
import React, { Component } from 'react';
import { compose } from 'redux';
import { withIREACTFeatures } from 'ioc-api-interface';
import { withMap, withMapZoom } from 'js/modules/map';
import { withMapPreferences } from 'js/modules/preferences';
import { withEventCreate } from 'js/modules/eventCreate';
import { getMapSettings } from 'js/components/MapView';
import { CircularProgress } from 'material-ui';
import { ZoomButtons, PositionButton, MapSettingsButton, Geosearchbutton } from './MapControls';
import { MapView, /* getMapSettings, */ attachCursorEvents } from 'js/components/MapView';
import styled from 'styled-components';
import MapboxDraw from '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw';
// TODO Move folders
import LayersSelectionWidget from 'js/components/Home/components/mapTab/LayersWidget/LayersSelectionWidget';
import LegendsFrame from 'js/components/Home/components/mapTab/LegendsWidget/LegendsFrame';
import { TimeSliderWidget } from 'js/components/Home/components/mapTab/TimeSliderWidget';
import MapInfo from 'js/components/Home/components/mapTab/MapInfo';
import { logMain, logSevereWarning } from 'js/utils/log';
import { areFeaturesEqual, arePointsEqual } from 'js/utils/ireactFeatureComparison';
import { toBBox } from 'js/utils/toBBox';
import { areGeolocationsEqual } from 'js/utils/updateRemotePosition';
import { compareFilterDefinitions, compareAoiFilters } from 'js/utils/getActiveFiltersDefinitions';
import debounce from 'lodash.debounce';
import { withTaskCreate } from 'js/modules/taskCreate';
import { TaskCreateDialog } from 'js/components/TaskCreateDialog';
import { WorkerWrapper } from './clusterWorker/WorkerWrapper';
import { withRouter } from 'react-router';

import {
  addFeatureLayers,
  addEmergencyEventLayers,
  addGeolocationPositionFeaturesLayers,
  updateUserPositionDataOnMap,
  addClickedPositionFeaturesLayers,
  updateClickedPoint,
  addSelectedFeatureLayers,
  updateSelectedFeatureLayers
} from './mapboxStyles/addFeatureLayers';
// import GoogleSearchBox from 'js/components/Home/components/mapTab/GoogleSearchBox';
import { PopupHandler } from './PopupHandler';
import {
  MIN_ZOOM,
  MAX_ZOOM,
  IREACT_FEATURES_SRC_NAME,
  IREACT_FEATURES_AOI_SRC_NAME,
  IREACT_MISSION_TASKS_SRC_NAME,
  IREACT_EMERGENCY_EVENTS_SRC_NAME,
  boundsHeight,
  boundsWidth,
  //drawControlsOptions,
  EmptyFeatureCollection,
  getMissionTaskFilterDefinition
} from './mapViewSettings';
import { withDictionary } from 'ioc-localization';
import { fetchFeatureAOI, flyToPolygonFeature } from './featuresInitialization/fetchFeatureAOI';
import PointMode from './PointMode';
import PolygonMode from './PolygonMode';
import TaskMode from './TaskMode';
import { withMessages } from 'js/modules/ui';
import { withFeaturePopup } from 'js/modules/featurePopup';
import axios from 'axios';

const IREACT_ALL_FEATURES_SRC_NAME = `${IREACT_FEATURES_SRC_NAME}_all_features`;

const IREACT_POINT_FEATURES_TYPES = ['report', 'missionTask', 'agentLocation'];

function isIREACTPointFeature(itemType) {
  return IREACT_POINT_FEATURES_TYPES.indexOf(itemType) !== -1;
}

const enhance = compose(
  withMap,
  withMapZoom,
  withMapPreferences,
  withIREACTFeatures,
  withDictionary,
  withMessages,
  withTaskCreate,
  withFeaturePopup,
  withEventCreate,
  withRouter
);

const MapContainer = styled.div`
  width: 100%;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.5);
`;

class MapInner extends Component {
  static defaultProps = {
    activeEventId: -1
  };

  state = {
    //mirrors of the ones in Home
    hoveredFeature: null,
    selectedFeature: null,
    mapInited: false,
    workerReady: false,
    featureLayersAdded: false,
    featureUpdating: false, // set true to hide popup when loading AOI
    // map initial style (base and stuff) loaded
    initialStyleLoaded: false,
    // all point features URL (well, except events)
    allFeaturesDataURL: null,
    // clustered point features URL (well, except events)
    clusteredFeaturesDataURL: null,
    filterByReportRequestActive: false
  };
  _draw = null;

  mapView = null;

  workerWrapper = null;

  _requestClusterUpdate = debounce(
    () => {
      if (!this.mapView) {
        return;
      }
      const map = this.mapView.getMap();
      const zoom = map.getZoom();
      const bbox = toBBox(map.getBounds());
      const center = map.getCenter().toArray();
      this.workerWrapper.send('getClusters', { bbox, zoom });
      return {
        zoom,
        bbox,
        center
      };
    },
    200,
    { leading: true }
  );

  _getMap = () => {
    return this.mapView ? this.mapView.getMap() : null;
    // return this.mapView && this.mapView.current ? this.mapView.current.getMap() : null;
  };

  _onWorkerInitOrUpdate = result => {
    logMain('Feature Inited Or Updated', result);
    this.setState({ allFeaturesDataURL: result.allFeaturesUrl, workerReady: result.ready });
  };

  _onWorkerGetClusters = result => {
    logMain('Worker getClusters', result);
    if (!result.clusteredFeaturesUrl) {
      return;
    }
    const map = this._getMap();
    if (!map) {
      return;
    }
    const featuresSrc = map.getSource(IREACT_FEATURES_SRC_NAME);
    // Update cluster features URL
    if (featuresSrc) {
      featuresSrc.setData(result.clusteredFeaturesUrl);
    } else {
      console.warn(`Main: cannot find source ${IREACT_FEATURES_SRC_NAME}`);
    }
  };

  _onWorkerZoomToFeature = ({ center, zoom, feature }) => {
    if (this.props.isDrawing === true) {
      return;
    }
    logMain('Worker zoom to feature', center, zoom, feature);
    const map = this._getMap();
    if (!map) {
      return;
    }
    if (feature && isIREACTPointFeature(feature.properties.itemType)) {
      if (map.isMoving()) {
        map.stop();
      }

      const zoom = map.getZoom();
      const container = map.getContainer();
      map.easeTo({
        center: feature.geometry.coordinates,
        // compensate drawer open
        offset: [
          this.props.location.pathname === '/home/map' ? 0 : 380 / 2,
          Math.round(container.clientHeight * 0.33)
        ],
        zoom,
        maxZoom: zoom
      });

      //   if (
      //     feature.properties.itemType === 'report' ||
      //     feature.properties.itemType === 'agentLocation'
      //   ) {
      //     center[1] -= 0.0002;
      //   }
      //   map.flyTo({ center, zoom, curve: 1 });
      // } else {
    } else if (feature == null && Array.isArray(center)) {
      map.easeTo({ center, zoom });
    }
  };

  _onWorkerGetChildren = result => {
    logMain('Worker Children', result);
  };

  _clearFeature = interactionType => {
    if (interactionType === 'hover') {
      this.setState({ hoveredFeature: null });
    } else if (interactionType === 'select') {
      this.setState({ selectedFeature: null });
    }
  };

  _updatePolygonSrc = polygonData => {
    const map = this._getMap();
    if (!map) {
      return;
    }
    const polygonsSRC = map.getSource(IREACT_FEATURES_AOI_SRC_NAME);
    if (polygonsSRC) {
      polygonsSRC.setData(polygonData);
    }
  };

  _fetchFeatureAOI = (id, itemType) => {
    if (!isIREACTPointFeature(itemType)) {
      this.setState({ featureUpdating: true }, async () => {
        this.props.onAOILoading();
        try {
          const featureAOI = await fetchFeatureAOI(id, itemType);
          if (this.props.selectedFeature && this.props.selectedFeature.properties.id === id) {
            this._updatePolygonSrc(featureAOI);
          }
          this.props.onAOILoaded();
        } catch (err) {
          this.props.onAOILoaded();
          this.props.onError(err);
        }
      });
    } else {
      this._clearFeatureAOI();
      return;
    }
  };

  _clearFeatureAOI = () => {
    const map = this._getMap();
    if (!map) {
      return;
    }
    const polygonsSRC = map.getSource(IREACT_FEATURES_AOI_SRC_NAME);
    if (polygonsSRC) {
      polygonsSRC.setData(EmptyFeatureCollection);
    }
  };

  _requestFeaturePosition = (feature, zoom, bbox, interactionType) => {
    const { id, itemType } = feature.properties;
    this.workerWrapper.send('findFeature', {
      interactionType: interactionType,
      featureId: id,
      itemType,
      zoom,
      bbox
    });
  };

  _onWorkerFindFeature = result => {
    logMain('Worker Find Feature', result);
    const { interactionType, feature, parentFeature } = result;
    if (interactionType === 'hover') {
      let _feature =
        parentFeature === null
          ? feature
          : {
              properties: feature.properties,
              geometry: parentFeature.geometry,
              type: feature.type
            };

      this.setState({ hoveredFeature: _feature });
    } else if (interactionType === 'select') {
      this.setState({ selectedFeature: feature });
    }
  };

  componentDidMount() {
    this.workerWrapper = new WorkerWrapper({
      initOrUpdate: this._onWorkerInitOrUpdate,
      getClusters: this._onWorkerGetClusters,
      zoomToFeature: this._onWorkerZoomToFeature,
      getChildren: this._onWorkerGetChildren,
      findFeature: this._onWorkerFindFeature
    });
  }

  _updateMissionTaskFeaturesURLOnMap = url => {
    const map = this._getMap();
    if (map) {
      const featureTasksSrc = map.getSource(IREACT_MISSION_TASKS_SRC_NAME);
      if (featureTasksSrc) {
        featureTasksSrc.setData(url ? url : EmptyFeatureCollection);
      }
    }
  };

  _setMapViewRef = mapView => (this.mapView = mapView);

  _updateMissionMapFilter = id => {
    const map = this._getMap();
    if (!map) {
      return;
    }
    map.setFilter('single-missiontasks-features', getMissionTaskFilterDefinition(id));
    map.setFilter('single-missiontasks-feature-icons', getMissionTaskFilterDefinition(id));
  };

  _zoomToSelectedFeatureIfNeeded = prevState => {
    if (this.props.isDrawing === true) {
      return;
    }
    if (this.state.selectedFeature !== null) {
      if (isIREACTPointFeature(this.state.selectedFeature.properties.itemType)) {
        this._clearFeatureAOI();
        // this.workerWrapper.send('zoomToFeature', {
        //   featureId: this.state.selectedFeature.properties.id,
        //   itemType: this.state.selectedFeature.properties.itemType,
        //   spiderBodyId: this.state.selectedFeature.properties.spiderBodyId || null
        // });
        // this.props.zoomToFeature(this.state.selectedFeature.geometry.coordinates);
        const map = this._getMap();
        if (map) {
          const zoom = map.getZoom();
          const container = map.getContainer();
          map.easeTo({
            center: this.state.selectedFeature.geometry.coordinates,
            // compensate drawer open
            offset: [
              this.props.location.pathname === '/home/map' ? 0 : 380 / 2,
              Math.round(container.clientHeight * 0.33)
            ],
            zoom,
            maxZoom: zoom
          });
        }
      } else {
        // Fetch AOI
        this._fetchFeatureAOI(
          this.state.selectedFeature.properties.id,
          this.state.selectedFeature.properties.itemType
        );
        if (this.state.selectedFeature.properties.itemType === 'mission') {
          this._updateMissionMapFilter(this.state.selectedFeature.properties.id);
        } else {
          this._updateMissionMapFilter(-1);
        }
        if (this.state.selectedFeature.properties.itemType === 'mapRequest') {
          this.props.onMapRequestSelected(null);
          setTimeout(() => {
            const map = this._getMap();
            const polygonsSRC = map.getSource(IREACT_FEATURES_AOI_SRC_NAME);
            let mapRequestFeature = null;
            if (polygonsSRC && this.props.selectedFeature) {
              mapRequestFeature = polygonsSRC._data.features.find(
                rr =>
                  rr.properties.itemType === 'mapRequest' &&
                  rr.properties.id === this.props.selectedFeature.properties.id
              );
            }
            this.props.onMapRequestSelected({
              ...this.state.selectedFeature,
              layers: mapRequestFeature ? mapRequestFeature.properties.layers : []
            });
          }, 1000);
        } else {
          this.props.onMapRequestSelected(null);
        }
      }
    } else {
      this._clearFeature('select');
      this._updateMissionMapFilter(-1);
      // Clear AOI
      this._clearFeatureAOI();
      if (prevState.selectedFeature.properties.itemType === 'mapRequest') {
        this.props.onMapRequestSelected(null);
      }
    }
    if (this.mapView) {
      updateSelectedFeatureLayers(this._getMap(), this.state.selectedFeature);
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.featureLayersAdded === false &&
      this.state.featureLayersAdded === true &&
      this.props.ireactFeaturesIsUpdating === false
    ) {
      this.workerWrapper.send('init', {
        filterDefinitions: this.props.allFeatureFiltersDefinitions
      });
    }

    if (prevState.workerReady === false && this.state.workerReady === true) {
      this._requestClusterUpdate();
      if (this.state.mapInited === false) {
        this.setState({ mapInited: true });
      }
    }
    // TODO find some more accurate predicate property since this is flagged even if diff update returned 0 features
    const map = this._getMap();
    if (!map) {
      return;
    }
    const polygonsSRC = map.getSource(IREACT_FEATURES_AOI_SRC_NAME);
    let reportRequestFeature = null;
    if (polygonsSRC && this.props.selectedFeature) {
      reportRequestFeature = polygonsSRC._data.features.find(
        rr =>
          rr.properties.itemType === 'reportRequest' &&
          rr.properties.id === this.props.selectedFeature.properties.id
      );
    }
    if (
      prevProps.ireactFeaturesLastUpdated !== this.props.ireactFeaturesLastUpdated ||
      /* (prevProps.ireactFeaturesIsUpdating === true &&
        this.props.ireactFeaturesIsUpdating === false) */ !compareFilterDefinitions(
        // client-side filters changed
        prevProps.allFeatureFiltersDefinitions,
        this.props.allFeatureFiltersDefinitions
      ) ||
      !compareAoiFilters(prevProps.drawCategory, this.props.drawCategory, this.props.drawPolygon) ||
      ((reportRequestFeature && !this.state.filterByReportRequestActive) ||
        (prevProps.selectedFeature && !this.props.selectedFeature))
    ) {
      if (reportRequestFeature) this.setState({ filterByReportRequestActive: true });
      if (this.state.featureLayersAdded) {
        //update features on map
        this.workerWrapper.send('update', {
          filterDefinitions: this.props.allFeatureFiltersDefinitions,
          aoiFilter: { drawPolygon: this.props.drawPolygon, drawCategory: this.props.drawCategory },
          reportRequestFeature
        });
      }
    }
    if (
      (!this.props.selectedFeature ||
        this.props.selectedFeature.properties.itemType !== 'reportRequest') &&
      prevProps.selectedFeature &&
      prevProps.selectedFeature.properties.itemType === 'reportRequest'
    )
      this.setState({ filterByReportRequestActive: false });

    if (!areFeaturesEqual(prevState.selectedFeature, this.state.selectedFeature)) {
      this._zoomToSelectedFeatureIfNeeded(prevState);
    }
    if (prevState.allFeaturesDataURL !== this.state.allFeaturesDataURL) {
      const map = this._getMap();
      if (map) {
        const allFeaturesSrc = map.getSource(IREACT_ALL_FEATURES_SRC_NAME);
        if (allFeaturesSrc && this.state.allFeaturesDataURL) {
          allFeaturesSrc.setData(this.state.allFeaturesDataURL);
        }
      }
    }
    let forceTaskUpdateOnMap = false;
    if (!areFeaturesEqual(prevProps.selectedFeature, this.props.selectedFeature, true)) {
      if (this.props.selectedFeature !== null) {
        forceTaskUpdateOnMap = this.props.selectedFeature.itemType === 'mission';
        let bounds = [];
        if (!this.props.bounds) {
          const mapSettings = getMapSettings(map);
          if (mapSettings) bounds = mapboxgl.LngLatBounds.convert(mapSettings.bounds);
        }
        this._requestFeaturePosition(
          this.props.selectedFeature,
          this.props.zoom,
          this.props.bounds ? this.props.bounds : bounds,
          'select'
        );
        // // Fetch AOI
        // this._fetchFeatureAOI(
        //   this.props.selectedFeature.properties.id,
        //   this.props.selectedFeature.properties.itemType
        // );
        // if (this.props.selectedFeature.properties.itemType === 'mission') {
        //   this._updateMissionMapFilter(this.props.selectedFeature.properties.id);
        // } else {
        //   this._updateMissionMapFilter(-1);
        // }
      } else {
        this._clearFeature('select');
        this._updateMissionMapFilter(-1);
        // Clear AOI
        this._clearFeatureAOI();
      }
    }
    if (!areFeaturesEqual(prevProps.hoveredFeature, this.props.hoveredFeature)) {
      if (this.props.hoveredFeature !== null) {
        this._requestFeaturePosition(
          this.props.hoveredFeature,
          this.props.zoom,
          this.props.bounds,
          'hover'
        );
      } else {
        this._clearFeature('hover');
      }
    }
    if (
      prevProps.missionTaskFeaturesURL !== this.props.missionTaskFeaturesURL ||
      forceTaskUpdateOnMap
    ) {
      this._updateMissionTaskFeaturesURLOnMap(this.props.missionTaskFeaturesURL);
    }
    if (!areFeaturesEqual(prevProps.selectedEvent, this.props.selectedEvent)) {
      this._updateSelectedEventData(
        'selected',
        this.props.selectedEvent,
        this.props.selectedEventBounds ? this.props.selectedEventBounds.feature : null
      );
    }
    if (!areFeaturesEqual(prevProps.activeEvent, this.props.activeEvent)) {
      this._updateSelectedEventData(
        'active',
        this.props.activeEvent,
        this.props.activeEventBounds ? this.props.activeEventBounds.feature : null
      );
    }
    if (!arePointsEqual(prevProps.clickedPoint, this.props.clickedPoint) && !this.props.isDrawing) {
      if (this.mapView) {
        updateClickedPoint(this._getMap(), this.props.clickedPoint);
      }
    }
    if (prevProps.geolocationPosition.timestamp !== this.props.geolocationPosition.timestamp) {
      if (!areGeolocationsEqual(prevProps.geolocationPosition, this.props.geolocationPosition)) {
        const map = this._getMap();
        if (map) {
          if (prevProps.geolocationPosition.timestamp === 0) {
            // only first update
            updateUserPositionDataOnMap(
              map,
              this.props.geolocationPosition,
              this.props.geolocationPosition
            );
            const coords = this.props.geolocationPosition.coords;
            map.easeTo({ center: [coords.longitude, coords.latitude], zoom: 16 });
          } else {
            updateUserPositionDataOnMap(
              map,
              prevProps.geolocationPosition,
              this.props.geolocationPosition
            );
          }
        }
      }
    }
  }

  _updateSelectedEventData = (type, eventFeature, eventFeatureAOI) => {
    if (eventFeature !== null) {
      this.props.setClickedPoint(null);
    }
    const map = this._getMap();
    if (map) {
      const eventsSrc = map.getSource(`${IREACT_EMERGENCY_EVENTS_SRC_NAME}_${type}`);
      if (eventsSrc) {
        this.setState({ featureUpdating: true }, () => {
          const featureCollection = {
            type: 'FeatureCollection',
            features: eventFeature === null ? [] : [eventFeature, eventFeatureAOI]
          };
          eventsSrc.setData(featureCollection);
        });
      }
    }
  };

  _deselectAnyContent = (clickedPoint = null) => {
    logMain('Will have to deselect anything');
    if (
      this.props.selectedFeature === null ||
      this.props.selectedFeature.properties.itemType !== 'reportRequest'
    ) {
      const p = this.props.selectedEvent || this.props.activeEvent ? null : clickedPoint;
      this.props.deselectFeature(p);
      this.props.deselectEvent();
      this._clearFeatureAOI();
      // this.props.deselectTask();
    } else {
      this.props.deselectReport();
    }
  };

  _onMouseEnterFeature = e => {
    if (this.props.isDrawing === true) {
      return;
    }
    const feature = Array.isArray(e.features) && e.features.length > 0 ? e.features[0] : null;
    logMain('Mouse Enter feature', feature);
    if (feature && this.props.selectedFeature === null) {
      this.props.mouseEnterFeature(feature);
    } else if (
      this.props.selectedFeature &&
      feature &&
      feature.properties.itemType === 'missionTask'
    ) {
      this.props.mouseEnterMissionTask(feature);
    } else if (feature && feature.properties.itemType === 'report' && !this.props.selectedReport) {
      this.props.mouseEnterReport(feature);
    }
  };

  _onMouseLeaveFeature = e => {
    if (this.props.isDrawing === true) {
      return;
    }
    if (!this.props.selectedReport && this.props.hoveredReport) this.props.mouseLeaveReport();
    else this.props.mouseLeaveFeature();
  };

  onUpdatePositionClick = () => {
    if (this.props.isDrawing === true) {
      return;
    }
    const coords = this.props.geolocationPosition.coords;
    const map = this.mapView ? this.mapView.getMap() : null;
    if (map) {
      map.easeTo({ center: [coords.longitude, coords.latitude], zoom: 16 });
    }

    this._deselectAnyContent();
    this.props.updatePosition();
  };

  _onMapStyleLoad = async () => {
    logMain('Map style loaded');
    if (this.state.initialStyleLoaded === false) {
      this.setState({ initialStyleLoaded: true });
    } else {
      const map = this._getMap();
      if (!map) {
        return;
      }
      await addGeolocationPositionFeaturesLayers(
        map,
        MIN_ZOOM,
        MAX_ZOOM,
        this.props.geolocationPosition
      );
      await addFeatureLayers(
        map,
        IREACT_FEATURES_SRC_NAME,
        IREACT_FEATURES_AOI_SRC_NAME,
        MIN_ZOOM,
        MAX_ZOOM,
        IREACT_MISSION_TASKS_SRC_NAME,
        this.props.missionTaskFeaturesURL
      );
      await addEmergencyEventLayers(map, IREACT_EMERGENCY_EVENTS_SRC_NAME, MAX_ZOOM);
      await addSelectedFeatureLayers(
        map,
        IREACT_FEATURES_SRC_NAME,
        // IREACT_ALL_FEATURES_SRC_NAME,
        // this.state.allFeaturesDataURL,
        MIN_ZOOM
      );
      await addClickedPositionFeaturesLayers(map, MIN_ZOOM, MAX_ZOOM);
      map.on('sourcedata', e => {
        if (e.isSourceLoaded) {
          if (e.sourceId === IREACT_FEATURES_SRC_NAME) {
            if (this.props.selectedFeature !== null) {
              this._requestFeaturePosition(
                this.props.selectedFeature,
                this.props.zoom,
                this.props.bounds,
                'select'
              );
            }

            if (this.props.hoveredFeature !== null) {
              this._requestFeaturePosition(
                this.props.hoveredFeature,
                this.props.zoom,
                this.props.bounds,
                'hover'
              );
            }
          } else if (e.sourceId === IREACT_FEATURES_AOI_SRC_NAME) {
            if (e.source.data.features.length > 0) {
              const feature = e.source.data.features[0];
              const _onMoveEnd = () => {
                map.off('moveend', _onMoveEnd);
                this.setState({ featureUpdating: false });
              };
              map.on('moveend', _onMoveEnd);
              flyToPolygonFeature(map, feature, this.props.location);
            }
          } else if (e.sourceId.startsWith(IREACT_EMERGENCY_EVENTS_SRC_NAME)) {
            if (e.source.data.features.length > 0) {
              const polygonFeature = e.source.data.features.filter(
                f => f.geometry.type !== 'Point'
              )[0];
              if (polygonFeature) {
                const _onMoveEnd = () => {
                  map.off('moveend', _onMoveEnd);
                  this.setState({ featureUpdating: false });
                };
                map.on('moveend', _onMoveEnd);
                flyToPolygonFeature(map, polygonFeature);
              }
            }
          } else if (e.sourceId.startsWith(IREACT_MISSION_TASKS_SRC_NAME)) {
            const searchTask = this.props.selectedMissionTask
              ? this.props.selectedMissionTask
              : this.props.hoveredMissionTask
              ? this.props.hoveredMissionTask
              : null;
            const selectionFn = this.props.selectedMissionTask
              ? this.props.selectMissionTask
              : this.props.hoveredMissionTask
              ? this.props.mouseEnterMissionTask
              : null;
            if (searchTask) {
              const selectedTaskOnMap = map.querySourceFeatures(e.sourceId, {
                filter: ['==', 'id', searchTask.properties.id]
              });
              logMain(
                'IREACT_MISSION_TASKS_SRC_NAME',
                IREACT_MISSION_TASKS_SRC_NAME,
                'loaded or updated',
                selectedTaskOnMap
              );
              selectionFn(
                Array.isArray(selectedTaskOnMap) ? selectedTaskOnMap[0] : selectedTaskOnMap
              );
            }
          }
        }
      });
      this._updateSelectedEventData(
        'selected',
        this.props.selectedEvent,
        this.props.selectedEventBounds ? this.props.selectedEventBounds.feature : null
      );
      this._updateSelectedEventData(
        'active',
        this.props.activeEvent,
        this.props.activeEventBounds ? this.props.activeEventBounds.feature : null
      );
      map.on('contextmenu', this.props.onMapContextMenuClick);
      this.setState({ featureLayersAdded: true });
      if (this.state.workerReady) {
        this._requestClusterUpdate();
      } else if (!this.state.workerReady && this.state.featureLayersAdded) {
        //update features on map
        this.workerWrapper.send('update', {
          filterDefinitions: this.props.allFeatureFiltersDefinitions
        });
      }
    }
  };

  _addMapboxGlDraw = map => {
    if (map) {
      //    onst draw = new MapboxDraw(drawControlsOptions);
      this._draw = new MapboxDraw({
        touchEnabled: false,
        displayControlsDefault: false,
        //defaultMode: 'point_mode',
        // Adds custom modes to the built-in set of modes
        modes: Object.assign(
          {
            point_mode: new PointMode({
              pushMessage: this.props.pushMessage,
              // setDrawPoint: drawPoint => this.props.setDrawPoint(drawPoint),
              // setDrawPolygon: drawPolygon => this.props.setDrawPolygon(drawPolygon),
              // setDrawCategory: drawCategory => this.props.setDrawCategory(drawCategory),
              setIsDrawing: isDrawing => this.props.setIsDrawing(isDrawing)
            }),
            polygon_mode: PolygonMode,
            task_mode: new TaskMode({
              pushMessage: this.props.pushMessage,
              setDrawTask: drawTask => this.props.setDrawTask(drawTask),
              setIsDrawing: isDrawing => this.props.setIsDrawing(isDrawing)
            })
          },
          MapboxDraw.modes
        )
      });
      map.addControl(this._draw);
      map.on('draw.create', e => {
        logMain('CREATED FEATURE', e.features);
        setTimeout(() => {
          this.props.setIsDrawing(false);
        }, 600);
      });
      this.props.setMapDrawReference(this._draw);
      this.props.loadEvents(this._draw);
    }
  };

  _removeMapboxGlDraw = map => {
    if (map && this._draw !== null) {
      this.props.setMapDrawReference(null);
      map.removeControl(this._draw);
    }
    this._draw = null;
  };

  _onMapLoadEnd = map => {
    logMain('Map load end');
    this.props.setMapReference(map);
    this._onMapStyleLoad();
    attachCursorEvents(
      map,
      'single-features',
      this._onMouseEnterFeature,
      this._onMouseLeaveFeature
      //this.props.mouseLeaveFeature
    );
    attachCursorEvents(
      map,
      'single-missiontasks-features',
      this._onMouseEnterFeature,
      this.props.mouseLeaveMissionTask
    );
  };

  _onMapLoad = map => {
    this._addMapboxGlDraw(map);
  };
  _onMapMoveEnd = (map, mapSettings) => {
    logMain('Map move end', mapSettings);
    if (this.state.workerReady) {
      this._requestClusterUpdate();
    }
  };

  _findAndSelectFromCollection = async (id, itemType, collectionUrl) => {
    if (collectionUrl) {
      try {
        const response = await axios.get(collectionUrl);
        const features =
          response.data.type === 'FeatureCollection' ? response.data.features : response.data;
        const feature = features.find(
          f => f.properties.id === id && f.properties.itemType === itemType
        );
        if (feature) {
          if (
            !this.props.selectedFeature ||
            (this.props.selectedFeature &&
              this.props.selectedFeature.properties.id !== feature.properties.id)
          )
            this.props.selectFeature(feature);
          // else if (this.props.selectedFeature && !this.props.isDialogOpen)
          //   this.props.toggleDialog(true);
        }
      } catch (err) {
        logSevereWarning('_findAndSelectFromCollection failure', err);
      }
    }
  };

  _onMapClick = e => {
    if (this.props.isDrawing === true) {
      return;
    }
    const map = e.target;
    const pixelPoint = e.point;
    const bounds = [
      [pixelPoint.x - boundsWidth / 2, pixelPoint.y - boundsHeight / 2],
      [pixelPoint.x + boundsWidth / 2, pixelPoint.y + boundsHeight / 2]
    ];
    const features = map.queryRenderedFeatures(bounds, {
      layers: ['clusters', 'single-features', 'single-missiontasks-features']
    });

    const aoiFeatures = map.queryRenderedFeatures(bounds, { layers: ['feature-polygon'] });
    const pointFeatures = features.filter(f => f.geometry.type === 'Point');
    let toggleDialog = true;
    if (pointFeatures.length === 0) {
      if (aoiFeatures.length === 0) {
        this._deselectAnyContent(e.lngLat.toArray());
      } else if (aoiFeatures[0].properties.itemType === 'mission') {
        this.props.deselectMissionTask();
      }
      // this.props.setClickedPoint(e.lngLat.toArray());
    } else if (pointFeatures[0].properties.cluster === true) {
      this.props.setClickedPoint(null);
      // Cluster Feature Click
      logMain('Cluster feature click', pointFeatures[0]);
      // Will make cluster expand
      this.workerWrapper.send('zoomToFeature', {
        featureId: pointFeatures[0].properties.cluster_id,
        itemType: 'cluster'
      });
    } else {
      const isRrActive =
        this.state.selectedFeature &&
        this.state.selectedFeature.properties.itemType === 'reportRequest';

      this.props.setClickedPoint(null);
      const properties = pointFeatures[0].properties;
      logMain('Single Feature click', properties);
      if (properties.itemType === 'missionTask' && !isRrActive) {
        setTimeout(() => this.props.selectMissionTask(pointFeatures[0]), 0);
      } else if (
        this.props.selectedFeature &&
        this.props.selectedFeature.properties.itemType === 'reportRequest' &&
        properties.itemType === 'report'
      ) {
        if (
          !this.props.selectedReport ||
          this.props.selectedReport.properties.id !== pointFeatures[0].properties.id
        ) {
          let reportFeature = [...pointFeatures];
          reportFeature.map(item => {
            item.properties.damages = JSON.parse(properties.damages);
            item.properties.measures = JSON.parse(properties.measures);
            item.properties.people = JSON.parse(properties.people);
            item.properties.resources = JSON.parse(properties.resources);
            item.properties.user = JSON.parse(properties.user);
            return item;
          });

          setTimeout(() => this.props.selectReport(reportFeature[0]), 0);
        }
      } else {
        if (!isRrActive) {
          if (properties.itemType === 'mission') {
            this.props.deselectMissionTask();
          } else if (properties.itemType === 'reportRequest') {
            this.props.mouseLeaveReport();
            this.props.deselectReport();
          }
          const { id, itemType } = pointFeatures[0].properties;
          this._findAndSelectFromCollection(id, itemType, this.state.allFeaturesDataURL);
          // this.props.selectFeature(pointFeatures[0]);
        } else {
          if (
            this.props.selectedFeature &&
            this.props.selectedFeature.properties.itemType === 'reportRequest' &&
            this.props.selectedFeature.properties.id !== pointFeatures[0].properties.id
          )
            toggleDialog = false;
        }
      }

      if (toggleDialog) {
        setTimeout(() => {
          this.props.toggleDialog(true);
        }, 0);
      }
    }
  };

  onGeosearchLocationFound = loc => {
    const map = this.mapView.getMap();
    if (map) {
      logMain('Geosearch location', loc);
      const { lat, lng, bbox } = loc;
      this.props.setClickedPoint([lng, lat]);
      const { west, south, east, north } = bbox;
      const bboxArray = [west, south, east, north];
      const isValidBBox = bboxArray.reduce((prev, next) => {
        prev = prev && typeof next === 'number';
        return prev;
      }, true);
      // fitBounds if bbox
      if (isValidBBox) {
        map.fitBounds(bboxArray, { padding: 100, linear: true });
      } else {
        map.setCenter([lng, lat]);
      }
    }
  };

  componentWillUnmount = () => {
    this.workerWrapper.terminate();
    this._removeMapboxGlDraw(this._getMap());
  };

  render() {
    const {
      preferences,
      center,
      zoom,
      pitch,
      bearing,
      updateMapSettings,
      dictionary,
      getMapDraw,
      selectFeature,
      deselectFeature,
      deselectEvent,
      selectedMissionTask,
      hoveredMissionTask,
      selectedEvent,
      activeEvent,
      selectMissionTask,
      deselectMissionTask,
      selectedReport,
      selectReport,
      deselectReport,
      hoveredReport,
      onAOILoading,
      onAOILoaded,
      isDialogOpen,
      toggleDialog,
      drawTask
    } = this.props;

    if (center[0] === 0 && center[1] === 0) {
      return <div className="invalid-position" />;
    }

    const mapSettings = {
      style: preferences.mapStyle,
      center,
      zoom,
      pitch,
      bearing,
      minZoom: MIN_ZOOM,
      maxZoom: MAX_ZOOM
    };

    return (
      <MapContainer className="map tab">
        <MapView
          ref={this._setMapViewRef}
          mapSettings={mapSettings}
          onSettingsUpdate={updateMapSettings}
          onStyleLoad={this._onMapStyleLoad}
          onMapLoaded={this._onMapLoad}
          onMapLoadEnd={this._onMapLoadEnd}
          onMapMoveEnd={this._onMapMoveEnd}
          onMapClick={this._onMapClick}
          mapLoadingMessage={<CircularProgress />}
        />
        <PopupHandler
          mapView={this.mapView}
          selectedFeature={this.state.selectedFeature}
          hoveredFeature={this.state.hoveredFeature}
          selectedMissionTask={selectedMissionTask}
          hoveredMissionTask={hoveredMissionTask}
          selectedReport={selectedReport}
          hoveredReport={hoveredReport}
          selectedEvent={selectedEvent}
          activeEvent={activeEvent}
          dictionary={dictionary}
          getMapDraw={getMapDraw}
          selectFeature={selectFeature}
          deselectFeature={deselectFeature}
          hidePopup={this.state.featureUpdating}
          deselectEvent={deselectEvent}
          selectMissionTask={selectMissionTask}
          deselectMissionTask={deselectMissionTask}
          selectReport={selectReport}
          deselectReport={deselectReport}
          isDialogOpen={isDialogOpen}
          toggleDialog={toggleDialog}
        />
        <TaskCreateDialog
          mapView={this.mapView}
          getMapDraw={getMapDraw}
          dictionary={dictionary}
          locale={this.props.locale}
          mapBoxDraw={this.draw}
          isTaskPopupOpen={this.props.isTaskPopupOpen}
          drawTask={drawTask}
          setDrawTask={this.props.setDrawTask}
        />
        {/* <GoogleSearchBox /> */}
        {this.props.activeEventId === -1 && (
          <PositionButton
            onClick={this.onUpdatePositionClick}
            geolocationUpdating={this.props.geolocationUpdating}
          />
        )}
        <ZoomButtons map={this.mapView ? this.mapView.getMap() : null} />
        <LayersSelectionWidget mapView={this.mapView} />
        <LegendsFrame />
        <MapInfo {...this.props.mapInfoProperties} />
        {this.state.mapInited && (
          <TimeSliderWidget
            map={this.mapView ? this.mapView.getMap() : null}
            onMapInfoLoading={onAOILoading}
            onMapInfoLoaded={onAOILoaded}
            selectedFeature={this.state.selectedFeature}
            deselectFeature={deselectFeature}
          />
        )}
        <MapSettingsButton />
        <Geosearchbutton onLocationFound={this.onGeosearchLocationFound} />
      </MapContainer>
    );
  }
}

export default enhance(MapInner);
