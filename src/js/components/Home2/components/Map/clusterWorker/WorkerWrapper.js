import { logMain, logError } from 'js/utils/log';

/* eslint-disable */
// ESLint complains about !
import ClustersWorker from 'worker-loader?inline&fallback=false!js/components/Home2/components/Map/clusterWorker/clusterWorker';
//const ClustersWorker = require('worker-loader?name=clusterWorker.js&inline=false&fallback=false!js/components/Home2/components/Map/clusterWorker/clusterWorker');
/* eslint-enable */

const defaultWorkerErrorHandler = e => logError('WorkerWrapper - Error', e);

export class WorkerWrapper {
  constructor(handlersMap = {}, onError = defaultWorkerErrorHandler) {
    this._worker = new ClustersWorker();
    this._handlersMap = handlersMap;

    this._onWorkerMessage = e => {
      const messageType = e.data.messageType;
      if (!messageType) {
        logMain(`WorkerWrapper - Unknown message: "${e.data}"`);
        return;
      }
      const handler = this._handlersMap[messageType];
      if (!handler) {
        logMain(`WorkerWrapper - Unhandled messageType: "${messageType}"`);
        return;
      }
      handler(e.data.payload);
    };
    this._worker.onerror = onError;
    this._worker.onmessage = this._onWorkerMessage;
  }

  send = (messageType, payload) => {
    this._worker.postMessage({ messageType, payload });
  };

  terminate = () => {
    this._worker.terminate();
  };
}
