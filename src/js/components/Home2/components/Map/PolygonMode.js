import Constants from '@mapbox/mapbox-gl-draw/src/constants';
import DrawPolygon from '@mapbox/mapbox-gl-draw/src/modes/draw_polygon';
import rewind from '@turf/rewind';

const PolygonMode = DrawPolygon;

PolygonMode.onSetup = function(opts) {
  const polygon = this.newFeature({
    type: Constants.geojsonTypes.FEATURE,
    properties: {},
    geometry: {
      type: Constants.geojsonTypes.POLYGON,
      coordinates: [[]]
    }
  });

  this.addFeature(polygon);

  this.clearSelectedFeatures();
  this.map.doubleClickZoom.disable(this);
  this.updateUIClasses({ mouse: Constants.cursors.ADD });
  this.activateUIButton(Constants.types.POLYGON);
  this.setActionableState({
    trash: true
  });
  this.setDrawPolygon = opts.setDrawPolygon;
  this.setDrawCategory = opts.setDrawCategory;
  this.setIsDrawing = opts.setIsDrawing;
  this.pushMessage = opts.pushMessage;
  this.category = opts.category;
  this.pushMessage('Draw a polygon on the map', 'success', null);
  this.setIsDrawing(true);
  this._ctx.options.modes.simple_select.dragMove = () => {};
  return {
    polygon,
    currentVertexPosition: 0,
    point: opts.point
  };
};

DrawPolygon.onStop = function(state) {
  this.updateUIClasses({ mouse: Constants.cursors.NONE });

  // check to see if we've deleted this feature
  if (this.getFeature(state.polygon.id) === undefined) return;

  //remove last added coordinate
  //state.polygon.removeCoordinate('0.' + state.currentVertexPosition);
  if (state.polygon.isValid()) {
    //var last = state.polygon.coordinates[0].pop();
    //state.polygon.coordinates[0].unshift(last);
    var first = state.polygon.coordinates[0][0];
    state.polygon.coordinates[0].pop();
    state.polygon.coordinates[0].push(first);
    const polygon = rewind(state.polygon);
    polygon.id = state.polygon.id;
    this.setDrawPolygon(polygon);
    this.setDrawCategory(this.category);
    // this.setIsDrawing(false);
    this.pushMessage('Operation Completed', 'success', null);
    this.map.fire(Constants.events.CREATE, {
      features: [state.polygon.toGeoJSON()]
    });
  } else {
    this.deleteFeature([state.polygon.id], { silent: true });
    this.changeMode(Constants.modes.SIMPLE_SELECT, {}, { silent: true });
  }
};

export default PolygonMode;
