import './styles.scss';
import React, { Component } from 'react';
import MapInner from './MapInner';
import nop from 'nop';
import { logError } from 'js/utils/log';
import { withMessages } from 'js/modules/ui';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { withMissions, withEmergencyEvents } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';
import { withGeolocation } from 'ioc-geolocation';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';
import { withMapInfo } from 'js/modules/LegendsWidget';

import { withAllFeatureFilters } from 'js/modules/AllFeatureFilters';
import { logMain } from 'js/utils/log';

import { compose } from 'redux';
import { NOTIFICATION_STYLES } from 'js/components/app/SnackBar';
const enhance = compose(
  withGeolocation,
  withDictionary,
  withMessages,
  withMissions,
  withEmergencyEvents,
  // FEATURE FILTERS
  withAllFeatureFilters,
  // Layers + info
  withLayersAndSettings,
  withMapInfo
);

const MOCKED_UP_LAYERS = false; //TODO dev only! remove once map requests are allright

const THE_MOCKED_UP_LAYERS = [
  {
    taskId: 4201,
    name: 'i-react_dev:t4201_20171030T094040_20100301T000000_temp_TG_rcp45_2010-2039_MAM',
    areaOfInterest: 'MULTIPOLYGON (((3.41 40.47, 3.41 42.94, 0.13 42.94, 0.13 40.47, 3.41 40.47)))',
    metadataId: 79674,
    metadataFileId: 93307,
    start: '2010-01-01T00:00:00Z',
    end: '2039-12-31T23:59:00Z',
    leadTime: '2010-03-01T00:00:00Z',
    url: 'https://geoserver-dev2.ireact.cloud/geoserver/'
  },
  {
    taskId: 4201,
    name: 'i-react_dev:t4201_20171030T094040_20100601T000000_temp_TG_rcp45_2010-2039_JJA',
    areaOfInterest: 'MULTIPOLYGON (((3.41 40.47, 3.41 42.94, 0.13 42.94, 0.13 40.47, 3.41 40.47)))',
    metadataId: 79674,
    metadataFileId: 93308,
    start: '2010-01-01T00:00:00Z',
    end: '2039-12-31T23:59:00Z',
    leadTime: '2010-06-01T00:00:00Z',
    url: 'https://geoserver-dev2.ireact.cloud/geoserver/'
  },
  {
    taskId: 4201,
    name: 'i-react_dev:t4201_20171030T094040_20100901T000000_temp_TG_rcp45_2010-2039_SON',
    areaOfInterest: 'MULTIPOLYGON (((3.41 40.47, 3.41 42.94, 0.13 42.94, 0.13 40.47, 3.41 40.47)))',
    metadataId: 79674,
    metadataFileId: 93309,
    start: '2010-01-01T00:00:00Z',
    end: '2039-12-31T23:59:00Z',
    leadTime: '2010-09-01T00:00:00Z',
    url: 'https://geoserver-dev2.ireact.cloud/geoserver/'
  }
];

const EXTRACT_GEOSERVER_URL_RX = /^https:\/\/[\w|\d|/.-]+\/geoserver\//;

const MR_NOTIFICATION_STYLE = { ...NOTIFICATION_STYLES.default, width: 400 };

const UPDATE_POS_INTERVAL_MS = ENVIRONMENT === 'production' ? 5 * 60 * 1000 : 1 * 60 * 1000;

class Map extends Component {
  static defaultProps = {
    // selection
    selectedFeature: null,
    selectFeature: nop,
    deselectFeature: nop,
    // Mission task (additional to the above)
    missionTaskFeaturesURL: null,
    selectedMissionTask: null,
    selectMissionTask: nop,
    deselectMissionTask: nop,
    hoveredMissionTask: null,
    mouseEnterMissionTask: nop,
    mouseLeaveMissionTask: nop,
    // hovering
    hoveredFeature: null,
    mouseEnterFeature: nop,
    mouseLeaveFeature: nop,
    // Emergency events
    deselectEvent: nop,
    selectedEvent: null,
    selectedEventBounds: null,
    activeEvent: null,
    activeEventBounds: null,
    // MAP REQUEST
    updateMapRequestLayers: nop
  };

  state = {
    // Map Feature Info
    mapInfoProperties: { position: [0, 0], coordinates: [NaN, NaN] }
  };

  _positionUpdateInterval = null;

  _startPositionUpdateInterval = () => {
    if (this._positionUpdateInterval !== null) {
      this._clearPositionUpdateInterval(this._positionUpdateInterval);
    }
    this.props.updatePosition();
    this._positionUpdateInterval = setInterval(() => {
      this.props.updatePosition();
      logMain('Position update requested');
    }, UPDATE_POS_INTERVAL_MS);
  };

  _clearPositionUpdateInterval = () => {
    clearInterval(this._positionUpdateInterval);
    this._positionUpdateInterval = null;
  };

  componentDidMount() {
    this._startPositionUpdateInterval();
  }

  componentWillUnmount() {
    this._clearPositionUpdateInterval();
  }

  updatePosition = async () => {
    try {
      const newGeolocation = await this.props.updatePosition();
      return newGeolocation;
    } catch (posError) {
      logError('Cannot update position', posError);
      if (posError.code === 1) {
        appInsights.trackException(
          posError,
          'Login::_updatePosition',
          { geolocation: 'updatePosition', error: 'Geolocation permission denied' },
          {},
          SeverityLevel.Error
        );
        const dictionary = this.props.dictionary;
        const title = dictionary._geolocation_permission_denied;
        const message = dictionary._geolocation_permission_denied_message;
        this.props.pushModalMessage(title, message, {
          _close: null
        });
      }
      return posError;
    }
  };

  _requestInfo = async (map, src, point, coordinates, displayName) => {
    const geoserverUrlMatch = GEOSERVER_URL
      ? GEOSERVER_URL
      : src._options.tiles[0].match(EXTRACT_GEOSERVER_URL_RX);
    const geoserverURL = GEOSERVER_URL ? GEOSERVER_URL : geoserverUrlMatch[0];
    if (geoserverUrlMatch) {
      await this.props.getMapInfo(
        point,
        geoserverURL,
        map.getCanvas(),
        map.getBounds(),
        src._options.name,
        displayName
      );
      this.setState({
        mapInfoProperties: { position: [point.x, point.y], coordinates }
      });
    } else return Promise.resolve();
  };

  onMapContextMenuClick = async e => {
    const map = e.target;
    const point = e.point;
    const coords = e.lngLat.toArray();
    logMain(
      'Map::nMapContextMenuClick',
      point,
      map,
      this.props.activeIReactSettingNames,
      this.props.activeCopernicusNames
    );
    for (let activeSetting of this.props.activeIReactTaskSettings) {
      const setting = activeSetting.setting;
      const src = map.getSource(setting.settingDefinition.name);
      if (src) {
        await this._requestInfo(map, src, point, coords, setting.settingDefinition.displayName);
      }
    }
    for (let activeCopernicusname of this.props.activeCopernicusNames) {
      const src = map.getSource(activeCopernicusname);
      if (src) {
        await this._requestInfo(map, src, point, coords, 'Copernicus');
      }
    }
    if (this.props.activeLayersFromMapRequest && this.props.activeLayersFromMapRequest.layers) {
      for (let activeMapRequestLayer of this.props.activeLayersFromMapRequest.layers) {
        const src = map.getSource(activeMapRequestLayer.extensionData.attributes.RequestCode);
        if (src) {
          await this._requestInfo(map, src, point, coords, 'MapLayer');
        }
      }
    }
  };

  _onAOILoading = () => {
    this.props.setFeatureDetailsLoading(true);
  };

  _onAOILoaded = () => {
    this.props.setFeatureDetailsLoading(false);
  };

  _handleSelectedMapRequestSelection = mapRequestFeature => {
    if (!mapRequestFeature) {
      this.props.updateMapRequestLayers([]);
    } else {
      if (MOCKED_UP_LAYERS) {
        //TODO dev only! remove once map requests are allright
        this.props.pushMessage(
          `Adding ${THE_MOCKED_UP_LAYERS.length} map request layers`,
          MR_NOTIFICATION_STYLE
        );
        this.props.updateMapRequestLayers(THE_MOCKED_UP_LAYERS, THE_MOCKED_UP_LAYERS[0].taskId);
      } else {
        // THIS should be working with REAL map requests
        if (mapRequestFeature.properties.layerCounter > 0) {
          this.props.pushMessage(
            `Adding ${mapRequestFeature.properties.layerCounter} map request layers`,
            MR_NOTIFICATION_STYLE
          );
          this.props.updateMapRequestLayers(
            mapRequestFeature.layers,
            mapRequestFeature.properties.iReactTask
          );
        } else {
          this.props.pushMessage(
            `Map Request with code ${mapRequestFeature.properties.code} for Task ID ${
              mapRequestFeature.properties.iReactTask
            } has not yet any layer ready`,
            MR_NOTIFICATION_STYLE
          );
          this.props.updateMapRequestLayers([]);
        }
      }
    }
  };

  _onError = err => this.props.pushError(err);

  componentDidCatch(err) {
    this.props.pushError(err);
  }

  render() {
    const {
      selectedFeature,
      selectFeature,
      deselectFeature,
      hoveredFeature,
      mouseEnterFeature,
      mouseLeaveFeature,
      // TASK
      missionTaskFeaturesURL,
      selectedMissionTask,
      selectMissionTask,
      deselectMissionTask,
      hoveredMissionTask,
      mouseEnterMissionTask,
      mouseLeaveMissionTask,

      //Report
      selectedReport,
      selectReport,
      deselectReport,
      hoveredReport,
      mouseEnterReport,
      mouseLeaveReport,

      //Emergency event
      deselectEvent,
      selectedEvent,
      selectedEventBounds,
      activeEvent,
      activeEventBounds,
      // FILTERS
      allFeatureFiltersDefinitions,
      // REST
      geolocationPosition,
      getMapDraw,
      setMapReference,
      setMapDrawReference,
      clickedPoint,
      setClickedPoint,

      //Draw Props
      drawCategory,
      drawPoint,
      drawPolygon,
      drawTask,
      isDrawing
    } = this.props;

    const mapProps = {
      selectedFeature,
      selectFeature,
      deselectFeature,
      hoveredFeature,
      mouseEnterFeature,
      mouseLeaveFeature,
      missionTaskFeaturesURL,
      selectedMissionTask,
      selectMissionTask,
      deselectMissionTask,
      hoveredMissionTask,
      mouseEnterMissionTask,
      mouseLeaveMissionTask,
      selectedReport,
      selectReport,
      deselectReport,
      hoveredReport,
      mouseEnterReport,
      mouseLeaveReport,
      deselectEvent,
      selectedEvent,
      selectedEventBounds,
      activeEvent,
      activeEventBounds,
      geolocationPosition,
      clickedPoint,
      setClickedPoint,
      allFeatureFiltersDefinitions,
      drawCategory,
      drawPoint,
      drawPolygon,
      drawTask,
      isDrawing
    };

    return (
      <MapInner
        {...mapProps}
        updatePosition={this.updatePosition}
        loadEvents={this.props.loadEvents}
        getMapDraw={getMapDraw}
        onError={this._onError}
        mapInfoProperties={this.state.mapInfoProperties}
        onMapContextMenuClick={this.onMapContextMenuClick}
        setMapDrawReference={setMapDrawReference}
        setMapReference={setMapReference}
        onAOILoaded={this._onAOILoaded}
        onAOILoading={this._onAOILoading}
        onMapRequestSelected={this._handleSelectedMapRequestSelection}
        setDrawPoint={this.props.setDrawPoint}
        setDrawPolygon={this.props.setDrawPolygon}
        setDrawCategory={this.props.setDrawCategory}
        setIsDrawing={this.props.setIsDrawing}
        setDrawTask={this.props.setDrawTask}
      />
    );
  }
}

export default enhance(Map);
