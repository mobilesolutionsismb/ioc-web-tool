import Constants from '@mapbox/mapbox-gl-draw/src/constants';

class TaskMode {
  // When the mode starts this function will be called.
  // The `opts` argument comes from `draw.changeMode('lotsofpoints', {count:7})`.
  // The value returned should be an object and will be passed to all other lifecycle functions
  constructor(props) {
    this.pushMessage = props.pushMessage;
    this.setDrawTask = props.setDrawTask;
    this.setIsDrawing = props.setIsDrawing;
  }

  onSetup = function(opts) {
    var state = {
      point: null
    };
    this.setIsDrawing(true);
    this.toggleTaskPopup = opts.toggleTaskPopup;
    this.toggleTaskPopup(false);
    this.pushMessage('Click to set the location of the task', 'success', null);
    return state;
  };

  // Whenever a user clicks on the map, Draw will call `onClick`
  onClick = function(state, e) {
    // `this.newFeature` takes geojson and makes a DrawFeature
    if (!state.point) {
      const point = this.newFeature({
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'Point',
          coordinates: [e.lngLat.lng, e.lngLat.lat]
        }
      });

      state.point = point;
      this.addFeature(point); // puts the point on the map
      this.setDrawTask(point);
      setTimeout(() => {
        // this.setIsDrawing(false);
        this.toggleTaskPopup(true);
      }, 0);
      this.pushMessage('Position set', 'success', null);
      this.changeMode(Constants.modes.SIMPLE_SELECT);
    }
  };

  toDisplayFeatures = function(state, geojson, display) {
    display(geojson);
  };
}

export default TaskMode;
