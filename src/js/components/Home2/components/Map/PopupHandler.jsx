import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import styled from 'styled-components';
import { logMain } from 'js/utils/log';
import { DetailCard } from 'js/components/DetailCard';
import { TaskDetailCard } from 'js/components/TaskDetailCard';
import { ReportRequestDetail } from 'js/components/ReportRequestDetail';
import { EmergencyCommunicationDetails } from 'js/components/EmergencyCommunicationDetails';
import { AgentLocationDetail } from 'js/components/AgentLocationDetail';
// import { AgentLocationCard } from 'js/components/AgentLocationCard';
// import { TaskDetailCard } from 'js/components/TaskDetailCard';
import { DetailEventPopup } from 'js/components/DetailEventPopup';
import FeatureListItem from '../DoubleDrawer/LeftDrawer/FeatureListItem';
import { withDictionary } from 'ioc-localization';
import { logWarning } from 'js/utils/log';

const { Popup } = mapboxgl;

const SelectionPopupContainer = styled.div`
  width: 500px;
  height: 500px;
`;

const HoverPopupContainer = styled.div`
  width: auto;
  min-width: 380px;
  height: auto;
`;

class PopupContentComponent extends Component {
  static defaultProps = {
    hidePopup: false,
    id: -1,
    itemType: 'none',
    popupType: 'none' // or 'select' or 'hover'
  };

  _getHoverCard = (type, feature) => {
    logMain('Hovered', type, feature.properties.id);
    return (
      <FeatureListItem
        key="feature-list-item"
        properties={feature.properties}
        dictionary={this.props.dictionary}
        locale={this.props.locale}
      />
    );
  };

  _getSelectionCard = (type, feature) => {
    logMain('Selected', type, feature.properties.id);
    let component = <div />;
    // const draw = this.props.getMapDraw();
    switch (type) {
      case 'emergencyEvent':
        component = (
          <DetailEventPopup
            locale={this.props.locale}
            dictionary={this.props.dictionary}
            feature={feature}
            deselectFeature={this.props.deselectFeature}
            deselectEvent={this.props.deselectEvent}
            toggleDialog={this.props.toggleDialog}
          />
        );
        break;
      case 'report':
        component = (
          <DetailCard
            locale={this.props.locale}
            dictionary={this.props.dictionary}
            feature={feature}
            deselectFeature={
              this.props.selectedFeature &&
              this.props.selectedFeature.properties.itemType === 'reportRequest'
                ? this.props.deselectReport
                : this.props.deselectFeature
            }
            toggleDialog={this.props.toggleDialog}
          />
        );
        break;
      case 'reportRequest':
        component = (
          <ReportRequestDetail
            locale={this.props.locale}
            dictionary={this.props.dictionary}
            feature={feature}
            deselectFeature={this.props.deselectFeature}
            toggleDialog={this.props.toggleDialog}
          />
        );
        break;
      case 'emergencyCommunication':
        component = (
          <EmergencyCommunicationDetails
            locale={this.props.locale}
            dictionary={this.props.dictionary}
            feature={feature}
            deselectFeature={this.props.deselectFeature}
            toggleDialog={this.props.toggleDialog}
          />
        );
        break;
      // case 'mapRequest':
      //   component = <div>{`MAP REQUEST ${feature.properties.id}`}</div>;
      //   break;
      case 'agentLocation':
        component = (
          <AgentLocationDetail
            locale={this.props.locale}
            dictionary={this.props.dictionary}
            feature={feature}
            deselectFeature={this.props.deselectFeature}
            toggleDialog={this.props.toggleDialog}
          />
        );
        break;
      case 'missionTask':
        component = (
          <TaskDetailCard
            locale={this.props.locale}
            dictionary={this.props.dictionary}
            feature={feature}
            deselectFeature={this.props.deselectFeature}
            selectMissionTask={this.props.selectMissionTask}
            deselectMissionTask={this.props.deselectMissionTask}
            toggleDialog={this.props.toggleDialog}
            mission={this.props.selectedFeature}
          />
        );
        break;
      default:
        break;
    }
    return component;
  };

  _getInnerComponentToShow = (type, feature, popupType) => {
    if (!feature) {
      return null;
    }
    return popupType === 'select'
      ? this._getSelectionCard(type, feature)
      : this._getHoverCard(type, feature);
  };

  render() {
    const { itemType, popupType, feature } = this.props;
    if (popupType === 'none') {
      return <div />;
    }
    let Container =
      popupType === 'select' && itemType !== 'agentLocation'
        ? SelectionPopupContainer
        : HoverPopupContainer;
    let componentToShow = this._getInnerComponentToShow(itemType, feature, popupType);
    return <Container>{componentToShow}</Container>;
  }
}

const PopupContent = withDictionary(PopupContentComponent);

const NO_SELECT_POPUP_TYPES = ['mission', 'mapRequest'];

class PopupHandler extends Component {
  mapPopup = new Popup({
    closeButton: false
  });
  element = null;

  static defaultProps = {
    mapView: null,
    selectedFeature: null,
    hoveredFeature: null,
    selectedMissionTask: null,
    hoveredMissionTask: null,
    selectedEvent: null,
    // activeEmergenctEvent: null,
    hidePopup: false
  };

  constructor(props) {
    super(props);
    this.element = document.createElement('div');
  }

  _addPopup = (mapView, feature, popupType) => {
    if (
      !feature ||
      (!Array.isArray(feature.coordinates) && !Array.isArray(feature.geometry.coordinates))
    ) {
      logWarning('Invalid feature', feature);
      return;
    }
    const itemType = feature.properties.itemType;
    if (
      popupType === 'hover' ||
      (popupType === 'select' && NO_SELECT_POPUP_TYPES.indexOf(itemType) === -1)
    ) {
      const map = mapView.getMap();
      const coordinates = feature.coordinates ? feature.coordinates : feature.geometry.coordinates;
      this.mapPopup
        .setDOMContent(this.element)
        .setLngLat(coordinates)
        .addTo(map);
    } else {
      this._removePopup();
    }
  };

  _removePopup = () => {
    this.mapPopup.remove();
  };

  // Custom logic which determines priority content to be displayed
  // since we have to make the react declarative synta with mapbox imperative api
  // nested ternaries look worse but are more efficient
  _getPopupConfiguration = (
    selectedFeature,
    hoveredFeature,
    selectedMissionTask,
    hoveredMissionTask,
    selectedEvent,
    activeEvent,
    isDialogOpen,
    selectedReport,
    hoveredReport
  ) => {
    const regularConfig = !isDialogOpen
      ? ['none', null, null]
      : selectedFeature
      ? selectedFeature.properties.itemType === 'mission'
        ? selectedMissionTask
          ? ['select', selectedMissionTask, 'missionTask']
          : hoveredMissionTask
          ? ['hover', hoveredMissionTask, 'missionTask']
          : hoveredFeature && hoveredFeature.properties.itemType === 'mission'
          ? ['hover', hoveredFeature, 'mission']
          : ['none', selectedFeature, 'mission']
        : selectedFeature.properties.itemType === 'reportRequest'
        ? selectedReport
          ? ['select', selectedReport, 'report']
          : hoveredReport
          ? ['hover', hoveredReport, 'report']
          : ['select', selectedFeature, 'reportRequest']
        : ['select', selectedFeature, selectedFeature.properties.itemType]
      : hoveredFeature
      ? ['hover', hoveredFeature, hoveredFeature.properties.itemType]
      : ['none', null, null];

    const config = activeEvent
      ? ['select', activeEvent, activeEvent.properties.itemType]
      : selectedEvent
      ? ['select', selectedEvent, selectedEvent.properties.itemType]
      : regularConfig;
    return config;
  };

  componentDidUpdate(prevProps) {
    if (!this.props.mapView) {
      return; // no mapview no party
    }

    if (this.props.hidePopup === false && prevProps.hidePopup === true) {
      const {
        selectedFeature,
        hoveredFeature,
        selectedMissionTask,
        hoveredMissionTask,
        selectedEvent,
        activeEvent,
        isDialogOpen,
        selectedReport,
        hoveredReport
      } = this.props;
      const currentConfig = this._getPopupConfiguration(
        selectedFeature,
        hoveredFeature,
        selectedMissionTask,
        hoveredMissionTask,
        selectedEvent,
        activeEvent,
        isDialogOpen,
        selectedReport,
        hoveredReport
      );
      const feature = currentConfig[1];
      if (feature) {
        this._addPopup(this.props.mapView, feature, currentConfig[0]);
      }
    }
    // else if (this.props.hidePopup === true && prevProps.hidePopup === false) {
    //   // hide
    //   this._removePopup();
    // }
    else if (
      !areFeaturesEqual(prevProps.selectedFeature, this.props.selectedFeature, true) ||
      !areFeaturesEqual(prevProps.hoveredFeature, this.props.hoveredFeature) ||
      !areFeaturesEqual(prevProps.selectedMissionTask, this.props.selectedMissionTask, true) ||
      !areFeaturesEqual(prevProps.hoveredMissionTask, this.props.hoveredMissionTask) ||
      !areFeaturesEqual(prevProps.selectedReport, this.props.selectedReport, true) ||
      !areFeaturesEqual(prevProps.hoveredReport, this.props.hoveredReport) ||
      !areFeaturesEqual(prevProps.selectedEvent, this.props.selectedEvent) ||
      !areFeaturesEqual(prevProps.activeEvent, this.props.activeEvent)
    ) {
      const config = this._getPopupConfiguration(
        this.props.selectedFeature,
        this.props.hoveredFeature,
        this.props.selectedMissionTask,
        this.props.hoveredMissionTask,
        this.props.selectedEvent,
        this.props.activeEvent,
        this.props.isDialogOpen,
        this.props.selectedReport,
        this.props.hoveredReport
      );
      if (config[1] !== null) {
        this._addPopup(this.props.mapView, config[1], config[0]);
      } else {
        this._removePopup();
      }
    } else if (this.props.isDialogOpen && !prevProps.isDialogOpen) {
      const myConfig = this._getPopupConfiguration(
        this.props.selectedFeature,
        this.props.hoveredFeature,
        this.props.selectedMissionTask,
        this.props.hoveredMissionTask,
        this.props.selectedEvent,
        this.props.activeEvent,
        this.props.isDialogOpen,
        this.props.selectedReport,
        this.props.hoveredReport
      );
      if (myConfig[1] !== null) {
        this._addPopup(this.props.mapView, myConfig[1], myConfig[0]);
      } else {
        this._removePopup();
      }
    } else if (this.props.isDialogOpen === false && prevProps.isDialogOpen) {
      this._removePopup();
    }
  }

  render() {
    const {
      selectedFeature,
      hoveredFeature,
      dictionary,
      getMapDraw, //TODO check if needed!
      selectFeature,
      deselectFeature,
      hidePopup,
      selectedMissionTask,
      hoveredMissionTask,
      selectedEvent,
      activeEvent,
      deselectEvent,
      selectMissionTask,
      deselectMissionTask,
      isDialogOpen,
      selectedReport,
      hoveredReport,
      selectReport,
      deselectReport
    } = this.props;

    const config = this._getPopupConfiguration(
      selectedFeature,
      hoveredFeature,
      selectedMissionTask,
      hoveredMissionTask,
      selectedEvent,
      activeEvent,
      isDialogOpen,
      selectedReport,
      hoveredReport
    );
    const popupType = config[0];
    const feature = config[1];

    return ReactDOM.createPortal(
      <PopupContent
        id={feature ? feature.properties.id : -1}
        itemType={feature ? feature.properties.itemType : undefined}
        selectedFeature={selectedFeature}
        popupType={popupType}
        feature={feature}
        dictionary={dictionary}
        getMapDraw={getMapDraw}
        selectFeature={selectFeature}
        deselectFeature={deselectFeature}
        hidePopup={hidePopup}
        deselectEvent={deselectEvent}
        selectMissionTask={selectMissionTask}
        deselectMissionTask={deselectMissionTask}
        selectReport={selectReport}
        deselectReport={deselectReport}
        toggleDialog={this.props.toggleDialog}
      />,
      this.element
    );
  }
}

export { PopupHandler };
