import React, { Component, useState, useCallback } from 'react';
import { FloatingActionButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { withLayersWidget } from 'js/modules/LayersWidget';
import styled from 'styled-components';
import { useTheme } from 'js/startup/Root';
import { Geosearch } from 'js/components/Home2/components/Geosearch/Geosearch';

const ControlsContainer = styled.div`
  position: absolute;
  display: block;
  z-index: 99;
`;

const ZoomButtonsContainer = styled(ControlsContainer)`
  bottom: 20px;
  right: 8px;
  display: flex;
  flex-direction: column;
`;

const PositionButtonContainer = styled(ControlsContainer)`
  bottom: 110px;
  right: 8px;
`;

const MapSettingsButtonContainer = styled(ControlsContainer)`
  top: 152px;
  left: 88px;
`;

const GeosearchButtonContainer = styled(ControlsContainer)`
  top: 104px;
  left: 88px;
`;

const themeable = muiThemeable();
class ZoomButtonsComponent extends Component {
  onZoomPlusClick = () => {
    const map = this.props.map;
    if (map) {
      const zoom = map.getZoom();
      const maxZoom = map.getMaxZoom();
      const nextZoom = Math.round(zoom) + 1;
      if (nextZoom < maxZoom) {
        map.easeTo({ zoom: nextZoom });
      }
    }
  };

  onZoomMinusClick = () => {
    const map = this.props.map;

    if (map) {
      const zoom = map.getZoom();
      const minZoom = map.getMinZoom();
      const nextZoom = Math.round(zoom) - 1;
      if (nextZoom > minZoom) {
        map.easeTo({ zoom: nextZoom });
      }
    }
  };

  render() {
    return (
      <ZoomButtonsContainer className="zoom-controls">
        <FloatingActionButton
          style={{ marginBottom: 2 }}
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          secondary={true}
          onClick={this.onZoomPlusClick}
        >
          <FontIcon className="material-icons">add</FontIcon>
        </FloatingActionButton>
        <FloatingActionButton
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          secondary={true}
          onClick={this.onZoomMinusClick}
        >
          <FontIcon className="material-icons">remove</FontIcon>
        </FloatingActionButton>
      </ZoomButtonsContainer>
    );
  }
}

export const ZoomButtons = themeable(ZoomButtonsComponent);

/* class PositionButtonComponent extends Component {
  onUpdatePositionClick = () => {
    this.props
      .updatePosition()
      .then(position => {
        let coords = this.props.geolocationPosition.coords;
        console.log('coords', [coords.longitude, coords.latitude]);
        this.props.zoomToFeature([coords.longitude, coords.latitude]);
      })
      .catch(e => {
        console.error('Cannot update position', e);
      });
  };

  render() {
    return (
      <PositionButtonContainer className="position-controls">
        <FloatingActionButton
          disabled={this.props.geolocationUpdating}
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          secondary={true}
          onClick={this.onUpdatePositionClick}
        >
          <FontIcon className="material-icons">my_location</FontIcon>
        </FloatingActionButton>
      </PositionButtonContainer>
    );
  }
}

export const PositionButton = enhance(themeable(PositionButtonComponent));
 */
export function PositionButton({ onClick, geolocationUpdating }) {
  const theme = useTheme();

  return (
    <PositionButtonContainer className="position-controls">
      <FloatingActionButton
        disabled={geolocationUpdating}
        mini={true}
        iconStyle={{ color: theme.palette.textColor }}
        secondary={true}
        onClick={onClick}
      >
        <FontIcon className="material-icons">my_location</FontIcon>
      </FloatingActionButton>
    </PositionButtonContainer>
  );
}

class MapSettingsButtonComponent extends Component {
  onClick = () => {
    this.props.setOpenLayersWidget(!this.props.isLayersWidgetOpen);
  };

  render() {
    return (
      <MapSettingsButtonContainer className="map-settings-controls">
        <FloatingActionButton
          mini={true}
          secondary={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          onClick={this.onClick}
        >
          <FontIcon className="material-icons">layers</FontIcon>
        </FloatingActionButton>
      </MapSettingsButtonContainer>
    );
  }
}

export const MapSettingsButton = withLayersWidget(themeable(MapSettingsButtonComponent));

export function Geosearchbutton({ onLocationFound }) {
  const theme = useTheme();
  const [expandedGeosearch, setExpandedGeosearch] = useState(false);
  const toggleExpanded = useCallback(() => {
    setExpandedGeosearch(!expandedGeosearch);
  }, [expandedGeosearch]);

  return (
    <GeosearchButtonContainer>
      <FloatingActionButton
        mini={true}
        secondary={true}
        iconStyle={{ color: theme.palette.textColor }}
        onClick={toggleExpanded}
      >
        <FontIcon className="material-icons">search</FontIcon>
      </FloatingActionButton>
      <Geosearch expanded={expandedGeosearch} onLocationFound={onLocationFound} />
    </GeosearchButtonContainer>
  );
}
