import {
  getByItemType,
  FEATURE_BORDER_COLORS,
  FEATURE_COLORS,
  NOR_CLUSTER_NOR_LEG,
  DEFAULT_ICON_PAINT,
  iconTextFieldExpr,
  iconImageExpr
} from './singleFeaturesLayers';
// import { EmptyFeatureCollection } from '../mapViewSettings';

export async function addSelectedFeatureLayers(
  map,
  featuresSrcName,
  /* featureDataURL, */ mapMinZoom
) {
  // map.addSource(featuresSrcName, {
  //   type: 'geojson',
  //   data: featureDataURL ? featureDataURL : EmptyFeatureCollection,
  //   tolerance: 0,
  //   cluster: false
  // });

  const filter = ['all', NOR_CLUSTER_NOR_LEG, ['==', 'id', -1], ['==', 'itemType', 'undefined']];

  const selectedFeaturePinpoint = {
    id: 'selected-feature-pin',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: featuresSrcName,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-field': '\ue90a',
      'text-size': 36,
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 3,
      'text-color': getByItemType(FEATURE_COLORS),
      'text-halo-color': getByItemType(FEATURE_BORDER_COLORS)
    },
    filter
  };

  const selectedFeaturePinpointIcon = {
    id: 'selected-feature-pin-icon',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: featuresSrcName,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-size': 36,
      'text-offset': [0, -0.1],
      'text-field': iconTextFieldExpr,
      'text-allow-overlap': true,
      'icon-image': iconImageExpr,
      'icon-size': 1,
      'icon-offset': [18, -28]
    },
    paint: DEFAULT_ICON_PAINT,
    filter
  };

  map.addLayer(selectedFeaturePinpoint);
  map.addLayer(selectedFeaturePinpointIcon);
  return Promise.resolve();
}

export async function updateSelectedFeatureLayers(map, selectedFeature) {
  const filter = selectedFeature
    ? [
        'all',
        NOR_CLUSTER_NOR_LEG,
        ['==', 'id', selectedFeature.properties.id],
        ['==', 'itemType', selectedFeature.properties.itemType]
      ]
    : ['all', NOR_CLUSTER_NOR_LEG, ['==', 'id', -1], ['==', 'itemType', 'undefined']];
  map.setFilter('selected-feature-pin', filter);
  map.setFilter('selected-feature-pin-icon', filter);
  return Promise.resolve();
}
