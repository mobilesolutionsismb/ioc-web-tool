import { addSingleFeatureLayers } from './singleFeaturesLayers';
import { addAOIFeatureLayers } from './aoiLayersStyle';

import { CLUSTER_CIRCLE_COLOR, CLUSTER_COUNT_COLOR, CLUSTER_COLORS } from './featureColors';
import {
  EmptyFeatureCollection,
  SHOW_CATEGORY_LAYERS_ON_CLUSTERS,
  CLUSTER_BORDER_RADIUS_PX,
  CLUSTER_RADIUS_PX,
  CLUSTER_LAYER_BORDER_OFFSETS,
  getBorderLayers
} from '../mapViewSettings';
import { addEmergencyEventLayers as _addEmergencyEventLayers } from './emergencyEventLayers';

const RASTER_LAYER_BBOX_SRC_NAME = 'raster-layer-bboxes';

function addRasterLayersBBox(map, minzoom, maxzoom) {
  const featuresSrcName = RASTER_LAYER_BBOX_SRC_NAME;
  map.addSource(featuresSrcName, {
    type: 'geojson',
    data: EmptyFeatureCollection,
    maxzoom,
    tolerance: 0,
    cluster: false
  });

  map.addLayer({
    id: featuresSrcName,
    source: featuresSrcName,
    type: 'line',
    layout: {
      'line-cap': 'round',
      'line-join': 'round'
    },
    paint: {
      'line-width': 2,
      'line-color': '#444'
    }
  });

  return Promise.resolve();
}

/**
 * Update Raster layersbbox
 * @param {mapboxgl.Map} map
 * @param {string} layerId
 * @param {GeoJSON.Feature} data
 */
export function updateRasterLayersBBox(map, layerId, feature = null) {
  const src = map.getSource(RASTER_LAYER_BBOX_SRC_NAME);
  if (src) {
    // Quick and dirty deep clone
    const data = JSON.parse(JSON.stringify({ ...src._data })); // risky because _data is undocumented
    const featureIndex = data.features.findIndex(f => f.properties.layerId === layerId);
    if (feature === null) {
      //attempt remove
      data.features.splice(featureIndex, 1);
    } else {
      // check if feature exists and update it
      if (featureIndex > -1) {
        data.features[featureIndex] = feature;
      } else {
        data.features.push(feature);
      }
    }
    src.setData(data);
  }
  return Promise.resolve();
}

function addSizeBasedClusterLayers(map, featuresSrcName, minzoom, maxzoom) {
  // Add clusters layer
  // Each circle represents a cluster
  // Cluster size depends on point count
  map.addLayer({
    id: 'clusters',
    type: 'circle',
    minzoom,
    maxzoom,
    source: featuresSrcName,
    filter: ['==', 'cluster', true],
    paint: {
      'circle-color': CLUSTER_CIRCLE_COLOR,
      'circle-radius': ['interpolate', ['linear'], ['get', 'point_count'], 10, 10, 50, 20, 100, 30],
      'circle-stroke-width': 1
    }
  });

  // Add cluster count for each cluster
  // Using abbreviated will transform 1000 into '1k'
  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    minzoom,
    maxzoom,
    source: featuresSrcName,
    filter: ['has', 'point_count'],
    paint: {
      'text-color': CLUSTER_COUNT_COLOR
    },
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['Open Sans Regular'],
      'text-size': ['interpolate', ['linear'], ['get', 'point_count'], 10, 10, 50, 20, 100, 30]
    }
  });

  return Promise.resolve();
}

function addCategorizedClusterLayers(map, featuresSrcName, minzoom, maxzoom) {
  // Inner circles of the cluster
  const circleCluster = {
    id: 'clusters',
    type: 'circle',
    minzoom,
    maxzoom,
    source: featuresSrcName,
    filter: ['has', 'point_count'],
    paint: {
      'circle-color': CLUSTER_CIRCLE_COLOR,
      'circle-radius': CLUSTER_RADIUS_PX
    }
  };

  const borderLayers = getBorderLayers(
    featuresSrcName,
    CLUSTER_LAYER_BORDER_OFFSETS,
    CLUSTER_BORDER_RADIUS_PX,
    CLUSTER_COLORS.categories,
    0,
    maxzoom
  );

  // Inner counter of features per cluster
  const clusterCountLayer = {
    id: 'cluster-count',
    type: 'symbol',
    source: featuresSrcName,
    maxzoom,
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-font': ['Open Sans Bold'],
      'text-size': {
        base: 1.5,
        stops: [[0, 20], [12, 26], [20, 36], [24, 40]]
      },
      'text-allow-overlap': true
    },
    paint: {
      'text-color': CLUSTER_COUNT_COLOR
    }
  };

  map.addLayer(circleCluster);

  // circles of each cluster border
  for (let bl of borderLayers) {
    map.addLayer(bl);
  }

  // Cluster count
  map.addLayer(clusterCountLayer);

  return Promise.resolve();
}

export async function addFeatureLayers(
  map,
  featuresSrcName,
  aoiFeaturesSrcName,
  mapMinZoom,
  mapMaxZoom,
  missionTasksSrc,
  missionTasksDataURL = null
) {
  await addRasterLayersBBox(map, mapMinZoom, mapMaxZoom);
  // Add a geojson point source.
  // Source for all I-REACT Features
  map.addSource(featuresSrcName, {
    type: 'geojson',
    data: EmptyFeatureCollection,
    maxzoom: mapMaxZoom,
    tolerance: 0,
    cluster: false
    // clusterMaxZoom: 23, // Max zoom to cluster points on
    // clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
  });

  map.addSource(missionTasksSrc, {
    type: 'geojson',
    data: missionTasksDataURL ? missionTasksDataURL : EmptyFeatureCollection,
    maxzoom: mapMaxZoom,
    tolerance: 0,
    cluster: false
  });

  map.addSource(aoiFeaturesSrcName, {
    type: 'geojson',
    data: EmptyFeatureCollection,
    maxzoom: mapMaxZoom,
    tolerance: 0.75,
    cluster: false
  });

  map.addLayer({
    id: 'spider-body',
    type: 'circle',
    minzoom: mapMaxZoom,
    maxzoom: mapMaxZoom + 1,
    source: featuresSrcName,
    filter: ['==', 'cluster', true],
    paint: { 'circle-color': '#333333', 'circle-radius': 8 }
  });

  map.addLayer({
    id: 'spider-legs',
    minzoom: mapMaxZoom,
    maxzoom: mapMaxZoom + 1,
    type: 'line',
    source: featuresSrcName,
    filter: ['==', 'isLeg', true],
    paint: {
      'line-color': '#333333',
      'line-width': 2
    }
  });

  if (SHOW_CATEGORY_LAYERS_ON_CLUSTERS) {
    await addCategorizedClusterLayers(map, featuresSrcName, mapMinZoom, mapMaxZoom);
  } else {
    await addSizeBasedClusterLayers(map, featuresSrcName, mapMinZoom, mapMaxZoom);
  }

  await addSingleFeatureLayers(map, featuresSrcName, missionTasksSrc, mapMinZoom, mapMaxZoom);
  await addAOIFeatureLayers(map, aoiFeaturesSrcName, mapMinZoom, mapMaxZoom);
  return true;
}

export async function addEmergencyEventLayers(map, featuresSrcName, mapMaxZoom) {
  await _addEmergencyEventLayers(map, featuresSrcName, mapMaxZoom, 'selected');
  await _addEmergencyEventLayers(map, featuresSrcName, mapMaxZoom, 'active');
  return Promise.resolve();
}

export {
  addGeolocationPositionFeaturesLayers,
  updateUserPositionDataOnMap,
  addClickedPositionFeaturesLayers,
  updateClickedPoint
} from './addOtherFeaturesLayers';

export {
  addSelectedFeatureLayers,
  updateSelectedFeatureLayers
} from './selectedFeaturesPinpointLayers';
