/**
 * MapView Settings + Cluster Utils
 */
export const MIN_ZOOM = 1;
export const MAX_ZOOM = 22;
export const IREACT_FEATURES_SRC_NAME = 'features';
export const IREACT_FEATURES_AOI_SRC_NAME = 'feature-polygons';
export const IREACT_MISSION_TASKS_SRC_NAME = 'missiontasks-features';

export const IREACT_EMERGENCY_EVENTS_SRC_NAME = 'emergency_events';
// Query rendered features bounds in px
export const boundsWidth = 40;
export const boundsHeight = 40;

export const drawControlsOptions = {
  displayControlsDefault: false
};

export const EmptyFeatureCollection = {
  features: [],
  type: 'FeatureCollection'
};

export function getMissionTaskFilterDefinition(missionId = -1) {
  return ['all', ['==', 'itemType', 'missionTask'], ['==', 'missionId', missionId]];
}

const { PI, cos, sin } = Math;

/**
 * Get number of dots of borderDotRadius radius size (pixel) which
 * will fill the circumference of a circle of clusterRadius radius size (pixel)
 * and returns the number of points and their offset displacements
 * @param {Number} clusterRadiusPx
 * @param {Number} borderDotRadiusPx
 * @return {Array} array of offsets of the centers of the dots
 */
export function getOffsetsForBorderDots(clusterRadiusPx = 16, borderDotRadiusPx = 2) {
  const nPoints = Math.ceil((2 * PI * clusterRadiusPx) / borderDotRadiusPx);
  const anglesInRadians = [...Array(nPoints).keys()].map(x => (x * 2 * PI) / nPoints);
  const offsets = anglesInRadians.map(x => [clusterRadiusPx * cos(x), clusterRadiusPx * sin(x)]);
  return offsets;
}

/**
 * Get layers styles
 * @param {String} sourceName
 * @param {Array} layerSettings
 * @param {Number} radius
 * @param {Object} dataColors
 * @param {Number} minzoom
 * @param {Number} maxzoom
 * @return {Array} an array of mapboxgl style layers for drawing the borders
 */
export function getBorderLayers(
  sourceName,
  offsets,
  radius,
  dataColors,
  minzoom = 0,
  maxzoom = 24
) {
  const stops = [...Object.entries(dataColors), ['_pause_el', 'transparent']];

  return offsets.map((offset, i) => {
    return {
      id: `cluster-border-dot-${i}`,
      type: 'circle',
      source: sourceName,
      filter: ['has', 'point_count'],
      minzoom,
      maxzoom,
      paint: {
        'circle-blur': 0.5,
        'circle-color': {
          property: `_border-${i}`,
          type: 'categorical',
          stops
        },
        'circle-radius': radius,
        'circle-translate': offset
      }
    };
  });
}

// If false, it will not show colored dots but clusters sized proportionally
// to the number of items they contain
export const SHOW_CATEGORY_LAYERS_ON_CLUSTERS = true;

export const CLUSTER_BORDER_RADIUS_PX = 3; // cluster_border/2, pixel (each element is a circle)
export const CLUSTER_RADIUS_PX = 20; // cluster radius, pixel
// Offset for each cluster border colored dot
export const CLUSTER_LAYER_BORDER_OFFSETS = getOffsetsForBorderDots(
  CLUSTER_RADIUS_PX,
  CLUSTER_BORDER_RADIUS_PX
);

// no. of dots for category colors
export const DOTS_PER_BORDER = CLUSTER_LAYER_BORDER_OFFSETS.length;
