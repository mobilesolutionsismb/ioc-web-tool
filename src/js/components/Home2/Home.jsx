import React, { Component } from 'react';
import { compose } from 'redux';
import { StatusBar } from 'js/components/app/statusBar';
import NavigationDrawer from 'js/components/app/drawer/NavigationDrawer';
import STYLES, { LoaderContainer } from 'js/components/app/styles';
// import { withNotification } from 'js/modules/RTNotifications';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';
import { withRouter } from 'react-router';
import { DoubleDrawer, Map } from './components';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { LinearProgress } from 'material-ui';
import { withFeaturePopup } from 'js/modules/featurePopup';
import { withEventCreate } from 'js/modules/eventCreate';
import { Redirect } from 'react-router-dom';

import {
  withLogin,
  withEnumsAndRoles,
  withAPISettings,
  withMapLayers,
  withGeneralSetting,
  withEmergencyEvents
} from 'ioc-api-interface';

const enhance = compose(
  // API moudule
  withLogin,
  withEnumsAndRoles,
  withAPISettings,
  withMapLayers,
  withGeneralSetting,
  // local
  // withNotification,
  withLayersAndSettings,
  withEmergencyEvents,
  withFeaturePopup,
  withEventCreate
);

class Home extends Component {
  state = {
    featureDetailsLoading: 0,
    clickedPoint: null, // Array[lng, lat]
    // only id: number and itemType: string wrapped in properties: {} for GeoJSON compatibility
    // plus coordinates for displaying on maps and so on
    hoveredFeature: null,
    selectedFeature: null,
    // Mission task Management
    hoveredMissionTask: null,
    selectedMissionTask: null,

    //Report Management (when report Request is selected)
    hoveredReport: null,
    selectedReport: null,

    //Map Draw
    drawPoint: {},
    drawPolygon: {},
    drawTargetPoint: {},
    drawTargetPolygon: {},
    drawTask: {},
    drawEventPoint: {},
    drawEventArea: {},
    drawCategory: 'none',
    isDrawing: false
  };

  _map = null;
  _draw = null;

  _getMap = () => this._map;

  _setMap = map => {
    return (this._map = map);
  };

  _getMapDraw = () => this._draw;

  _loadEvents = draw => {
    if (this.props.location.search === '?event_create' && this.props.twitterSource) {
      draw.changeMode('simple_select', {});

      draw.add(this.props.twitterSource.polygon);
      draw.add(this.props.twitterSource.point);

      this._setDrawPolygon(this.props.twitterSource.polygon);
      this._setDrawPoint(this.props.twitterSource.point);
      this._setDrawCategory(drawerNames.eventCreate);

      console.log(draw);
    }
  };

  _setMapDraw = draw => {
    return (this._draw = draw);
  };

  componentDidMount() {
    this._init(this.props);
  }

  _init = async props => {
    if (props.loggedIn) {
      // if (!props.notificationsReady) {
      //   props
      //     .initializeNotifications()
      //     .then(() => console.log('RTN initialized'))
      //     .catch(err => console.error('Error attempting RTN registration', err));
      // }
      await props.loadSettings();
    }
  };

  // _updateLayersAndUserSettings(props) {
  //   if (props.activeEventAOI !== null || props.liveAOI !== null) {
  //     props.getLayers();
  //     props.getAllUserSettings();
  //   }
  // }

  // componentDidUpdate(prevProps) {
  //   if (
  //     prevProps.liveAOI !== this.props.liveAOI ||
  //     this.props.activeEventAOI !== prevProps.activeEventAOI
  //   ) {
  //     console.log('%c Update layer and user settings', 'color: whitesmoke; background: orangered');
  //     this._updateLayersAndUserSettings(this.props);
  //   }
  // }

  selectFeature = feature => {
    this.props.toggleDialog(true);
    if (this.props.selectedEvent !== null) this.props.deselectEvent();
    this.setState(previousState => {
      const { id, itemType } = feature.properties;
      const { coordinates } = feature.geometry;
      let selectedMissionTask = null;
      let hoveredMissionTask = null;

      if (itemType === 'mission') {
        const isMissionActive = this.props.search === '?mission_detail';
        this.props.history.replace(
          `/home/${drawerNames.mission}${!isMissionActive ? '?mission_detail' : ''}`
        );
        if (
          previousState.selectedMissionTask &&
          previousState.selectedMissionTask.properties.missionId === id
        ) {
          selectedMissionTask = previousState.selectedMissionTask;
        }
        if (
          previousState.hoveredMissionTask &&
          previousState.hoveredMissionTask.properties.missionId === id
        ) {
          hoveredMissionTask = previousState.hoveredMissionTask;
        }
      }
      return {
        selectedFeature: { properties: { id, itemType }, coordinates },
        hoveredFeature: null,
        selectedMissionTask,
        hoveredMissionTask,
        clickedPoint: null
      };
      // this.setState();
    });
  };

  _determineClickedPointFromPreviousState = (point, previousState) =>
    previousState.clickedPoint ? null : Array.isArray(point) && point.length === 2 ? point : null;

  setClickedPoint = point => {
    this.setState(previousState => {
      const clickedPoint = this._determineClickedPointFromPreviousState(point, previousState);
      return { ...previousState, clickedPoint };
    });
  };

  deselectFeature = point => {
    this.props.toggleDialog(false);
    this.setState(previousState => {
      if (previousState.selectedFeature == null) {
        const clickedPoint = this._determineClickedPointFromPreviousState(point, previousState);
        return { ...previousState, clickedPoint };
      } else {
        return { ...previousState, selectedFeature: null };
      }
    });
    // this.setState({ selectedFeature: null });
  };

  selectMissionTask = taskFeature => {
    if (this.state.selectedFeature) {
      const { /* id, */ itemType, missionId } = taskFeature.properties;
      if (itemType === 'missionTask' && missionId === this.state.selectedFeature.properties.id) {
        const { coordinates } = taskFeature.geometry;
        this.setState({
          selectedMissionTask: { properties: taskFeature.properties, coordinates }
        });
      }
    }
  };

  selectReport = repoortFeature => {
    if (
      this.state.selectedFeature &&
      this.state.selectedFeature.properties.itemType === 'reportRequest'
    ) {
      const { coordinates } = repoortFeature.geometry;
      this.setState({
        selectedReport: { properties: repoortFeature.properties, coordinates },
        hoveredReport: null
      });
      this.props.toggleDialog(true);
    }
  };

  deselectMissionTask = () => {
    this.setState({ selectedMissionTask: null });
  };
  deselectReport = () => {
    this.setState({ selectedReport: null, hoveredReport: null });
    this.props.toggleDialog(false);
  };

  mouseEnterMissionTask = taskFeature => {
    if (this.state.selectedFeature) {
      const { /* id, */ itemType, missionId } = taskFeature.properties;
      if (itemType === 'missionTask' && missionId === this.state.selectedFeature.properties.id) {
        const { coordinates } = taskFeature.geometry;
        this.setState({
          hoveredMissionTask: { properties: taskFeature.properties, coordinates }
        });
      }
    }
  };

  mouseEnterReport = reportFeature => {
    if (
      this.state.selectedFeature &&
      this.state.selectedFeature.properties.itemType === 'reportRequest'
    ) {
      const { coordinates } = reportFeature.geometry;
      this.setState({
        hoveredReport: { properties: reportFeature.properties, coordinates }
      });
      this.props.toggleDialog(true);
    }
  };

  mouseLeaveMissionTask = () => {
    this.setState({ hoveredMissionTask: null });
  };
  mouseLeaveReport = () => {
    this.setState({ hoveredReport: null });
    this.props.toggleDialog(false);
  };

  mouseEnterFeature = feature => {
    this.props.toggleDialog(true);
    const { id, itemType } = feature.properties;
    const { coordinates } = feature.geometry;
    this.setState({ hoveredFeature: { properties: { id, itemType }, coordinates } });
  };

  mouseLeaveFeature = () => {
    this.setState({ hoveredFeature: null });
  };

  setFeatureDetailsLoading = loading => {
    this.setState(prevState => ({
      featureDetailsLoading: loading ? prevState.loading + 1 : Math.max(0, prevState.loading - 1)
    }));
  };

  _setDrawPoint = drawPoint => {
    this.setState({ drawPoint });
  };

  _setDrawPolygon = drawPolygon => {
    this.setState({ drawPolygon });
  };

  _setDrawTargetPoint = drawTargetPoint => {
    this.setState({ drawTargetPoint });
  };

  _setDrawTargetPolygon = drawTargetPolygon => {
    this.setState({ drawTargetPolygon });
  };

  _setDrawEventPoint = drawEventPoint => {
    this.setState({ drawEventPoint });
  };

  _setDrawEventArea = drawEventArea => {
    this.setState({ drawEventArea });
  };

  _setDrawCategory = drawCategory => {
    this.setState({ drawCategory });
  };

  _setIsDrawing = isDrawing => {
    this.setState({ isDrawing });
  };

  _setDrawTask = drawTask => {
    this.setState({ drawTask });
  };

  resetHomeDrawState = category => {
    switch (category) {
      case 'base':
        this.setState({
          drawPoint: {},
          drawPolygon: {},
          drawCategory: 'none',
          isDrawing: false
        });
        break;
      case 'target':
        this.setState({
          drawTargetPoint: {},
          drawTargetPolygon: {},
          drawCategory: 'none',
          isDrawing: false
        });
        break;
      case 'eventArea':
        this.setState({
          drawEventPoint: {},
          drawEventArea: {},
          drawCategory: 'none',
          isDrawing: false
        });
        break;
      default:
        this.setState({
          drawPoint: {},
          drawPolygon: {},
          drawTargetPoint: {},
          drawTargetPolygon: {},
          drawTask: {},
          drawEventArea: {},
          drawCategory: 'none',
          isDrawing: false
        });
        break;
    }
  };

  render() {
    const { location, history, match, loggedIn, activeEventAOI, liveAOI } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" />;
    } else if (loggedIn && (activeEventAOI === null && liveAOI === null)) {
      return <div />;
    }
    const featureSelection = {
      selectedFeature: this.state.selectedFeature,
      selectFeature: this.selectFeature,
      deselectFeature: this.deselectFeature
    };

    const featureHover = {
      hoveredFeature: this.state.hoveredFeature,
      mouseEnterFeature: this.mouseEnterFeature,
      mouseLeaveFeature: this.mouseLeaveFeature
    };

    const missionTaskSelection = {
      selectedMissionTask: this.state.selectedMissionTask,
      selectMissionTask: this.selectMissionTask,
      deselectMissionTask: this.deselectMissionTask
    };

    const missionTaskHover = {
      hoveredMissionTask: this.state.hoveredMissionTask,
      mouseEnterMissionTask: this.mouseEnterMissionTask,
      mouseLeaveMissionTask: this.mouseLeaveMissionTask
    };

    const reportSelection = {
      selectedReport: this.state.selectedReport,
      selectReport: this.selectReport,
      deselectReport: this.deselectReport
    };

    const reportHover = {
      hoveredReport: this.state.hoveredReport,
      mouseEnterReport: this.mouseEnterReport,
      mouseLeaveReport: this.mouseLeaveReport
    };

    const routerProps = {
      location,
      history,
      match
    };

    const drawProps = {
      drawCategory: this.state.drawCategory,
      drawPoint: this.state.drawPoint,
      drawPolygon: this.state.drawPolygon,
      drawTargetPoint: this.state.drawTargetPoint,
      drawTargetPolygon: this.state.drawTargetPolygon,
      drawTask: this.state.drawTask,
      drawEventPoint: this.state.drawEventPoint,
      drawEventArea: this.state.drawEventArea,
      resetHomeDrawState: this.resetHomeDrawState,
      isDrawing: this.state.isDrawing,
      setDrawTask: this._setDrawTask,
      setDrawEventPoint: this._setDrawEventPoint,
      setDrawEventArea: this._setDrawEventArea,
      setDrawCategory: this._setDrawCategory,
      setDrawPoint: this._setDrawPoint,
      setDrawPolygon: this._setDrawPolygon,
      setDrawTargetPoint: this._setDrawTargetPoint,
      setDrawTargetPolygon: this._setDrawTargetPolygon,
      setIsDrawing: this._setIsDrawing
    };

    return (
      <div className="home page">
        <StatusBar
          style={STYLES.statusBarStyle}
          transparent={true}
          clickedPoint={this.state.clickedPoint}
          deselectFeature={featureSelection.deselectFeature}
        />
        {this.state.featureDetailsLoading > 0 && (
          <LoaderContainer>
            <LinearProgress mode="indeterminate" />
          </LoaderContainer>
        )}
        <NavigationDrawer />
        <DoubleDrawer
          getMap={this._getMap}
          getMapDraw={this._getMapDraw}
          {...drawProps}
          {...featureSelection}
          {...featureHover}
          {...missionTaskSelection}
          {...missionTaskHover}
          {...routerProps}
          {...reportSelection}
          {...reportHover}
        />
        <Map
          {...featureSelection}
          {...featureHover}
          {...missionTaskSelection}
          {...missionTaskHover}
          {...reportSelection}
          {...reportHover}
          {...drawProps}
          clickedPoint={this.state.clickedPoint}
          setClickedPoint={this.setClickedPoint}
          setFeatureDetailsLoading={this.setFeatureDetailsLoading}
          loadEvents={this._loadEvents}
          getMapDraw={this._getMapDraw}
          setMapDrawReference={this._setMapDraw}
          setMapReference={this._setMap}
        />
      </div>
    );
  }
}

export default withRouter(enhance(Home));
