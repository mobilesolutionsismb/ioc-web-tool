/**
 * Temporary fix the layer naming and groups for the Implementation Review Demo (June 2017)
 */

const emsr192nameRX = /^emsr192_(([a-zA-Z_,\s\d])+)(_v(\d){1,2}_(\d)+_)/i;

// remap wms requests to request all features for a group
function remapURLSForGroups(groups) {
  const groupNames = Object.keys(groups);
  const groupedSources = {};
  for (const groupName of groupNames) {
    const group = groups[groupName];
    const layerNames = Object.keys(group);
    const layer0 = group[layerNames[0]];
    // Only for those layers where there are many features
    if (layerNames.length > 1) {
      const layersToRequire = layerNames
        .map(function(l) {
          return groupName + '_' + l;
        })
        .join(',');
      const newURLString = layer0.source.tiles[0].replace(
        /layers=([\w\d_,])+&/,
        `layers=${layersToRequire}&`
      );
      const source = { ...layer0.source, id: groupName };
      const newTileJSON = { ...source, name: groupName, scheme: 'xyz', tiles: [newURLString] };
      groupedSources[groupName] = {
        id: groupName,
        source: newTileJSON,
        type: 'raster'
      };
    } else if (layerNames.length === 1) {
      groupedSources[groupName] = layer0;
    }
  }
  return groupedSources;
}

// regroup layers
export function regroupLayers(layers) {
  let groupName = null;
  const groupedLayers = layers.reduce(function(groups, nextLayer) {
    // first time no group name
    if (!groupName) {
      const match = nextLayer.id.match(emsr192nameRX);
      // if match, it's a group
      groupName = match === null ? nextLayer.id : match[0];
    }
    // the id matches
    if (nextLayer.id.indexOf(groupName) === 0) {
      const gname = groupName.replace(/_$/, ''); //remove trailing _
      let layerName = nextLayer.id.replace(groupName, '');
      layerName = layerName === '' ? 'layer' : layerName;
      if (!groups.hasOwnProperty(gname)) {
        groups[gname] = {};
      }
      groups[gname][layerName] = nextLayer;
    }
    // next group name: move to next group name
    else {
      const match = nextLayer.id.match(emsr192nameRX);
      // if match, it's a group
      groupName = match === null ? nextLayer.id : match[0];
      const gname = groupName.replace(/_$/, ''); //remove trailing _
      let layerName = nextLayer.id.replace(groupName, '');
      layerName = layerName === '' ? 'layer' : layerName;
      if (!groups.hasOwnProperty(gname)) {
        groups[gname] = {};
      }
      groups[gname][layerName] = nextLayer;
    }
    return groups;
  }, {});

  return remapURLSForGroups(groupedLayers);
}
