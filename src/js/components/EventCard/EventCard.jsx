import './style.scss';
import React, { Component } from 'react';
import { localizeDate } from 'js/utils/localizeDate';
import { Chip, Avatar, IconButton, Card } from 'material-ui';
import { withRouter } from 'react-router';
import { compose } from 'redux';
import { getDisplayName } from 'js/components/EventCommons';
import { HAZARD_TYPES_LETTERS } from 'js/components/ReportsCommons';
import { withEmergencyEvents } from 'ioc-api-interface';
//import { withEventPopup } from 'js/modules/eventPopup';
import { withFeaturePopup } from 'js/modules/featurePopup';
import { lighten } from 'material-ui/utils/colorManipulator';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withEventCreate } from 'js/modules/eventCreate';
// import { withTopBarDate } from 'js/modules/topBarDate';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { withMessages } from 'js/modules/ui';
import { withMapZoom } from 'js/modules/map';
import { withGeolocation } from 'ioc-geolocation';
import muiThemeable from 'material-ui/styles/muiThemeable';

const enhance = compose(
  withEventCreate,
  withMessages,
  withEmergencyEvents,
  withRouter,
  withGeolocation,
  withMapZoom,
  withLeftDrawer,
  withFeaturePopup
);
class EventCard extends Component {
  _element = null;

  onCardClick = event => {
    if (this.props.activeEventId !== -1 || this.props.isRrActive) return;

    this.props.deactivateEvent();
    if (this.props.selectedEventId === event.id) {
      //deselect
      this.props.deselectEvent();
      this.props.toggleDialog(false);
    } else {
      this.props.selectEvent(event.id);
      this.props.mouseLeaveEvent();
      this.props.toggleDialog(true);
      //this.props.deselectFeature();
    }
  };

  onMouseEnter = e => {
    if (
      this.props.selectedEventId === -1 &&
      this.props.activeEventId === -1 &&
      !this.props.isRrActive
    ) {
      this.props.mouseEnterEvent(this.props.event.id);
    }
  };

  onMouseLeave = () => {
    this.props.mouseLeaveEvent();
  };

  onEditCard = (event, e) => {
    e.stopPropagation();
    const selected =
      this.props.selectedEventId !== -1 &&
      event.id === this.props.selectedEventId &&
      event.id !== this.props.activeEventId;
    if (!selected) {
      this.props.selectEvent(this.props.event.id);
    }

    this.props.fillEmergencyEvent(this.props.event);
    this.props.toggleDialog(false);
    this.props.setOpenSecondary(false);
    this.props.mouseLeaveEvent();
    this.props.history.replace(`/home/${drawerNames.event}?event_create`);
  };

  componentDidMount(prevProps) {
    if (this._element && this.props.event.id === this.props.selectedEventId) {
      this._element.scrollIntoViewIfNeeded();
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this._element &&
      this.props.event.id === this.props.selectedEventId &&
      prevProps.selectedEventId !== this.props.selectedEventId
    ) {
      this._element.scrollIntoViewIfNeeded();
    }
  }

  getAddress = event => {
    if (event.address) {
      return event.address;
    } else {
      const isValid = e => typeof e === 'string' && e.length;
      const locations = Array.isArray(event.locations)
        ? event.locations.filter(isValid).join(', ')
        : null;
      const provinces = Array.isArray(event.provinces)
        ? event.provinces.filter(isValid).join(', ')
        : null;
      const countryNames = Array.isArray(event.countryNames)
        ? event.countryNames
            .filter(isValid)
            .map((cn, i) => `${cn} (${event.countryCodes[i]})`)
            .join(', ')
        : null;
      return `${locations ? locations + ' - ' : ''}${provinces ? provinces + '  - ' : ''}${
        countryNames ? countryNames : ''
      }`;
    }
  };
  _setCardRef = e => (this._element = e);

  render() {
    const event = this.props.event;
    const dateStartedString = event.start
      ? localizeDate(event.start, this.props.locale, 'L LT')
      : '';
    const dateFinishedString = event.end ? localizeDate(event.end, this.props.locale, 'L LT') : '';
    //const selected = this.props.selectedEventId !== null && event.id === this.props.selectedEventId;
    const palette = this.props.muiTheme.palette;
    const selected = this.props.selectedEventId !== -1 && event.id === this.props.selectedEventId;
    const isActive = this.props.activeEventId !== -1 && event.id === this.props.activeEventId;
    const hovered = this.props.hoveredEventId === event.id && !this.props.isRrActive;
    let innerStyleBg = palette.canvasColor;
    if (hovered) {
      innerStyleBg = lighten(palette.canvasColor, 0.3);
    }
    if (selected || isActive) {
      innerStyleBg = lighten(palette.canvasColor, 0.5);
    }

    const innerStyle = {
      backgroundColor: innerStyleBg,
      marginBottom: 5,
      fontWeight: 300,
      maxHeight: '100%'
    };
    let address;
    if (event !== null) address = this.getAddress(event);
    const displayName = getDisplayName(event);
    const isOpen = typeof event.end === 'undefined' || event.end === null;

    // TODO selected style properly
    return (
      <div
        className={`event-card${selected ? ' selected' : ''}${hovered ? ' hovered' : ''}`}
        ref={this._setCardRef}
        onClick={this.onCardClick.bind(this, event)}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
        style={{ height: 150 }}
      >
        <Card style={innerStyle}>
          <div className="eventCardButtons">
            <IconButton onClick={this.onEditCard.bind(this, event)}>
              <i className="material-icons md-18 md-light eventCardIcon">create</i>
            </IconButton>
            {/*<IconButton onClick={this.onCardClick.bind(this, event)}>
                                      <i className="material-icons md-18 md-light eventCardIcon">location_on</i>
                                    </IconButton> */}
          </div>
          <div style={{ padding: 16 }}>
            <Chip style={{ height: 24 }}>
              <Avatar
                className="ireact-pinpoint-icons"
                size={24}
                style={{ height: 24, width: 24 }}
                color={isOpen ? palette.textColor : palette.canvasColor}
                backgroundColor={isOpen ? palette.ongoingEventsColor : palette.closedEventsColor}
              >
                {HAZARD_TYPES_LETTERS[event.hazard]}
              </Avatar>
              <div className="eventCardHazzard">
                <span>{this.props.dictionary['_hazard_' + event.hazard]}</span>
              </div>
            </Chip>
            <div className="eventCardTitle">
              <span>{displayName}</span>
            </div>
            <div className="eventCardAddress">{address ? address : ''}</div>
            <div className="eventCardDates">
              {event.start ? dateStartedString + ' ' : ''} -{event.end ? dateFinishedString : ''}
            </div>
          </div>
        </Card>
      </div>
    );
  }
}

export default muiThemeable()(enhance(EventCard));
