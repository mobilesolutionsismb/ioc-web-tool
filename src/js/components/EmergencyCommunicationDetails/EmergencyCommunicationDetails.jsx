import './styles.scss';
import React, { Component } from 'react';
import { Card, CardMedia } from 'material-ui';
import { EmCommChip, EmCommLanguage, LevelBox } from 'js/components/EmergencyCommunicationsCommons';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { compose } from 'redux';
import { localizeDate } from 'js/utils/localizeDate';
import { logMain } from 'js/utils/log';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withEmCommNew } from 'js/modules/emCommNew';
import { EmCommButtons, UpdateAlertOrWarningLine } from './commons';

const enhance = compose(
  withLeftDrawer,
  withEmCommNew
);

class EmergencyCommunicationDetails extends Component {
  _goToMap = () => {
    logMain('EmergencyCommunicationDetails::_goToMap');
    // this.props.history.replace('/home/map');
  };

  clearSelection = () => {
    logMain('EmergencyCommunicationDetails::clearSelection');
    // this.props.history.push('/home/notifications');
    // this.props.deselectAlertOrWarning();
    // const mapView = this.props.mapView.getMap();
    // if (mapView !== null)
    //   mapView.setLayoutProperty('selected-emcomm', 'visibility', 'none');
  };

  edit = () => {
    if (this.props.feature) {
      this.props.fillEmComm(this.props.feature.properties);
      // this.props.history.push('/home/notifications?notification_new');
      this.props.setOpenSecondary(false);
    }
  };

  closeDetail = () => {
    this.props.toggleDialog(false);
  };

  render() {
    const { dictionary, locale, feature, muiTheme } = this.props;
    const palette = muiTheme.palette;

    const emComm = feature ? feature.properties : {};
    const { end, start, address, type } = emComm;
    const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
    const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';

    const {
      warningLevel,
      description,
      receivers,
      isOngoing,
      capStatus,
      capUrgency,
      capCertainty,
      capResponseType,
      lastModified,
      title,
      instruction,
      web,
      capIdentifier
    } = emComm;
    const hasReceivers = Array.isArray(receivers) && receivers.length > 0;
    const receiversListString = hasReceivers ? receivers.map(r => r.name).join(', ') : '';
    const lastUpdate = localizeDate(lastModified, locale, 'L LT');

    const severity = typeof warningLevel === 'string' ? warningLevel : 'unknown';

    const buttonLine = <EmCommButtons onCloseClick={this.closeDetail} />;
    const capStatusString = capStatus !== 'actual' && capStatus !== 0 ? '/' + capStatus : '';

    const organizationRow = (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <span style={{ fontSize: 12, paddingLeft: 5 }}>
          <span>{emComm.sourceOfInformation || emComm.organization || ''}</span>
        </span>
      </div>
    );

    const cardMediaChildren = (
      <div
        style={{
          padding: 10,
          borderStyle: 'ridge',
          borderWidth: 1,
          borderTop: 'hidden',
          borderLeft: 'hidden',
          borderRight: 'hidden'
        }}
      >
        <div className="emcomm-details-title">
          <EmCommChip dictionary={dictionary} palette={palette} emComm={emComm} />
          {emComm.language && <EmCommLanguage language={emComm.language} />}
        </div>
        {buttonLine}
      </div>
    );

    const secondColumnFooter = <UpdateAlertOrWarningLine onEdit={this.edit} />;

    return (
      <Card style={{ minHeight: 500, height: '100%' }}>
        <CardMedia style={{ height: '100%' }} children={cardMediaChildren} />
        <div style={{ display: 'flex', padding: 10 }}>
          <div style={{ width: '65%', display: 'flex', flexDirection: 'column' }}>
            <div className="emcomm-card__emcdesc">
              <LevelBox severity={severity} size="18px" />
              <span className="emcomm-card__emctype">
                {dictionary(`_emcomm_${type}`).toLocaleUpperCase()}
                {capStatusString}
              </span>
            </div>
            <div style={{ overflowY: 'auto', height: 330 }}>
              <div className="emComm-info-group">
                <div className="emCommFirtsColumnInfo">
                  <span style={{ fontWeight: 100 }}>{dictionary._last_update}</span>
                </div>
                <div className="emCommFirtsColumnInfo">
                  <span>{lastUpdate}</span>
                </div>
              </div>
              <hr className="emcomm-horizontalLineStyle" />

              <div className="emComm-info-group">
                <div className="emCommFirtsColumnInfo">
                  <span style={{ fontWeight: 100 }}>{dictionary._title}</span>
                </div>
                <div className="emCommFirtsColumnInfo">
                  <span>{title}</span>
                </div>
              </div>
              <hr className="emcomm-horizontalLineStyle" />
              <div className="emComm-info-group">
                <div className="emCommFirtsColumnInfo">
                  <span style={{ fontWeight: 100 }}>Web Url</span>
                </div>
                <div className="emCommFirtsColumnInfo">
                  <span>
                    <a
                      href={web}
                      style={{ color: 'lightblue' }}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {web}
                    </a>
                  </span>
                </div>
              </div>
              <hr className="emcomm-horizontalLineStyle" />
              <div className="emComm-info-group">
                <div className="emCommFirtsColumnInfo">
                  <span style={{ fontWeight: 100 }}>{dictionary._emcomm_cap_response_type}</span>
                </div>
                <div className="emCommFirtsColumnInfo">
                  <span>{capResponseType}</span>
                </div>
              </div>
              <div className="emComm-info-group">
                <div className="emCommFirtsColumnInfo">
                  <span style={{ fontWeight: 100 }}>{dictionary._instructions}</span>
                </div>
                <div className="emCommFirtsColumnInfo">
                  <span>{instruction}</span>
                </div>
              </div>
              <hr className="emcomm-horizontalLineStyle" />
              <div className="emComm-info-group">
                <div className="emCommFirtsColumnInfo">
                  <span style={{ fontWeight: 100 }}>{dictionary._description}</span>
                </div>
                <div className="emCommFirtsColumnInfo">
                  <span>{description}</span>
                </div>
              </div>
              <div style={{ position: 'absolute', bottom: 0 }}>
                <div className="emComm-info-group">
                  <div className="emCommFirtsColumnInfo">
                    <span>{organizationRow}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="emcomm-verticalLine" />
          <div style={{ width: '35%', padding: 10, height: 350, overflowY: 'auto' }}>
            <div className="emComm-info-group">
              <div className="emCommSecondColumnInfo">
                <span>{dictionary._cap_severity}: </span>
                <span>{severity}</span>
              </div>
              <div className="emCommSecondColumnInfo">
                <span>{dictionary._urgency}: </span>
                <span>{capUrgency}</span>
              </div>
              <div className="emCommSecondColumnInfo">
                <span>{dictionary._certainty}: </span>
                <span>{capCertainty}</span>
              </div>
            </div>
            <hr className="emcomm-horizontalLineStyle" />
            <div className="emComm-info-group">
              <div className="emCommSecondColumnInfo">
                <span>{address}</span>
              </div>
            </div>
            <hr className="emcomm-horizontalLineStyle" />
            <div className="emComm-info-group">
              <div className="emCommSecondColumnInfo">
                <span>{dateStartedString}</span>
              </div>
              <div className="emCommSecondColumnInfo">
                <span>{dateFinishedString}</span>
              </div>
              <div className="emCommSecondColumnInfo">
                <span>
                  Status: {isOngoing ? dictionary._is_status_ongoing : dictionary._is_status_closed}
                </span>
              </div>
            </div>
            <hr className="emcomm-horizontalLineStyle" />
            <div className="emComm-info-group">
              <div className="emCommSecondColumnInfo">
                <span>Sent To: {receiversListString}</span>
              </div>
            </div>
            <hr className="emcomm-horizontalLineStyle" />
            <div className="emComm-info-group">
              <div className="emCommSecondColumnInfo">
                <span>Code: {capIdentifier}</span>
              </div>
            </div>
            <div style={{ position: 'absolute', bottom: 0 }}>{secondColumnFooter}</div>
          </div>
        </div>
      </Card>
    );
  }
}

export default muiThemeable()(enhance(EmergencyCommunicationDetails));
