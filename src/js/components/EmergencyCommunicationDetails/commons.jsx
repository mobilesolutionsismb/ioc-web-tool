import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { FlatButton } from 'material-ui';
import { CloseIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { Link } from 'react-router-dom';

const LINE_STYLE = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginTop: 15,
  marginBottom: 15
};

export const UpdateAlertOrWarningLine = muiThemeable()(({ onEdit, onClearSelection }) => (
  <div style={LINE_STYLE}>
    <Link to={`/home/${drawerNames.notification}?${drawerNames.notificationNew}`}>
      <FlatButton label="EDIT" onClick={onEdit} labelStyle={{ fontSize: 12 }} />
    </Link>
  </div>
));

export const EmCommButtons = muiThemeable()(({ onCloseClick }) => (
  <div style={{ position: 'absolute', right: 5, top: 5, zIndex: 1 }}>
    {onCloseClick && (
      <CloseIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
        onClick={onCloseClick}
      />
    )}
  </div>
));
