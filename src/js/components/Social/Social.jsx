import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';
import { StatusBar } from 'js/components/app/statusBar';
import NavigationDrawer from 'js/components/app/drawer/NavigationDrawer';
import { Map, FiltersBar, EventDetection } from './components';
import { TweetsList } from './components/TweetsList';
import { Statistics } from './components/Statistics';
import { withLogin } from 'ioc-api-interface';
import { withTweets } from 'js/modules/socialApi';
import { withDictionary } from 'ioc-localization';
import { withSocialFilters } from 'js/modules/socialFilters';
import { compose } from 'redux';

import STYLES from './styles';

const enhance = compose(
  withLogin,
  withDictionary,
  withTweets,
  withSocialFilters
);

class Social extends Component {
  clickTweetCard = id => {
    this._mapcomponent.addClickTweet(id, 'tweet-marker-click');
    this._mapcomponent.addClickTweet(id, 'spiderify-tweet-marker-click');
  };
  noClickTweetCard = () => {
    this._mapcomponent.deleteClickTweet();
  };
  showEventOnMap = event => {
    this._mapcomponent.drawEvent(event);
  };

  goToEventOnMap = event => {
    this._mapcomponent.goToEventBoundingBox(event);
  };

  _setMap = map => {
    this._mapcomponent = map;
  };

  _applyBoundingBoxFilter = () => {
    this._mapcomponent._repeatResearchNewLocation();
  };

  render() {
    if (!this.props.loggedIn) {
      return <Redirect to="/" />;
    }
    return (
      <div className="social page">
        <StatusBar style={STYLES.statusBarStyle} socialPanel={true} />
        <FiltersBar
          style={STYLES.filtersBarStyle}
          applyBoundingBoxFilter={() => this._applyBoundingBoxFilter()}
        />
        <NavigationDrawer />
        <div style={STYLES.threeColumns}>
          <TweetsList
            style={{
              ...STYLES.tweetsListStyle,
              ...(!this.props.socialGeolocated ? { width: '50%' } : {})
            }}
            tweetsGeolocated={this.props.tweetsGeolocated}
            type="geolocation"
            clickTweetCard={this.clickTweetCard}
            noClickTweetCard={this.noClickTweetCard}
          />
          <div style={STYLES.middleColumn}>
            <Map onRef={this._setMap} style={STYLES.mapStyle} />
            <EventDetection
              style={STYLES.eventCardStyle}
              drawOnMap={event => this.showEventOnMap(event)}
              goToEventOnMap={event => this.goToEventOnMap(event)}
              applyBoundingBoxFilter={() => this._applyBoundingBoxFilter()}
            />
            <Statistics style={STYLES.cardsStyle} />
          </div>
          <TweetsList
            style={{
              ...STYLES.tweetsListStyle,
              ...(!this.props.socialGeolocated ? { width: '0', padding: '0', opacity: '0' } : {})
            }}
            tweets={this.props.tweets}
            type="non_geolocation"
            clickTweetCard={this.clickTweetCard}
            noClickTweetCard={this.noClickTweetCard}
          />
        </div>
      </div>
    );
  }
}
export default withRouter(enhance(Social));
