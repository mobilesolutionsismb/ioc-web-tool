import React, { Component } from 'react';
import { compose } from 'redux';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { iReactTheme } from 'js/startup/iReactTheme';

import SuperSelectField from 'material-ui-superselectfield';
import {
  FontIcon,
  SelectField,
  MenuItem,
  FlatButton,
  RaisedButton,
  IconButton,
  Chip,
  Avatar,
  Toggle,
  Checkbox,
  TextField
} from 'material-ui';
import {
  SOCIAL_HAZARDS,
  HAZADR_SOCIAL_ICONS,
  HAZARD_LABELS,
  SOCIAL_INFOTYPE,
  SOCIAL_INFOTYPE_LABELS,
  SOCIAL_TAGS,
  LANGUAGES
} from './socialTagsConfig.js';

import { withLoader } from 'js/modules/ui';
import { withTweets } from 'js/modules/socialApi';
import { withSocialFilters } from 'js/modules/socialFilters';
import { withDictionary } from 'ioc-localization';
import { withMap } from 'js/modules/map';
import { withAPISettings } from 'ioc-api-interface';

// import STYLES from './styles';

// const FIRST_OPENED_WIDTH = 380; //px
// const SECOND_OPENED_WIDTH = 665; //px

const enhance = compose(
  withDictionary,
  withSocialFilters,
  withTweets,
  withMap,
  withAPISettings,
  withLoader
);

const hazards = SOCIAL_HAZARDS;
const infotype = SOCIAL_INFOTYPE;
const tags = SOCIAL_TAGS;

const STYLES = {
  selectedSocialTimeRangeFilter: {
    backgroundColor: iReactTheme.palette.primary1Color,
    color: iReactTheme.palette.appBarColor
  }
};

const times = [
  {
    label: '30m',
    number: 30,
    unity: 'minutes'
  },
  {
    label: '1h',
    number: 1,
    unity: 'hours'
  },
  {
    label: '2h',
    number: 2,
    unity: 'hours'
  },
  {
    label: '4h',
    number: 4,
    unity: 'hours'
  },
  {
    label: '6h',
    number: 6,
    unity: 'hours'
  },
  {
    label: '12h',
    number: 12,
    unity: 'hours'
  },
  {
    label: '24h',
    number: 24,
    unity: 'hours'
  }
];

class FiltersBar extends Component {
  state = {
    collapse: false,
    keyword: '',
    unwantedKeyword: ''
  };
  componentDidMount = () => {
    if (!this.props.filters.language) {
      this._selectDefaultLanguage();
    }
  };

  _selectDefaultLanguage = () => {
    if (LANGUAGES[this.props.locale]) this.props.addSocialFilter('language', this.props.locale);
    else this.props.addSocialFilter('language', 'en');
  };

  languageChange = (event, index, language) => {
    this.props.addSocialFilter('language', language);
  };

  hazardsChange = hazards => {
    this.props.addSocialFilter('emergency', hazards);
  };

  infotypeChange = infotype => {
    this.props.addSocialFilter('infotype', infotype);
  };

  tagsChange = tags => {
    const newTag = tags.find(obj => this.props.filters.tags.indexOf(obj) === -1);
    if (newTag === undefined) {
      const deleteTag = this.props.filters.tags.find(obj => tags.indexOf(obj) === -1);
      let newArray = this.props.filters.tags.filter(
        tag => tag.value.label !== deleteTag.value.label
      );
      this.props.addSocialFilter('tags', newArray);
    } else {
      let newArray = this.props.filters.tags.filter(tag => tag.value.type !== newTag.value.type);
      newArray.push(newTag);
      this.props.addSocialFilter('tags', newArray);
    }
  };

  timeRangeChange = timeRange => {
    if (this.props.filters.dateTo.label === timeRange.label) {
      this.props.removeSocialFilter('dateTo');
    } else {
      this.props.addSocialFilter('dateTo', timeRange);
    }
  };

  updateCheck(event, isChecked) {
    this.props.addSocialFilter('retweet', isChecked);
  }

  resetEventFilter = async () => {
    await this.props.deactivateEventFiltering();
    await this.props.applyBoundingBoxFilter();
    this.applyFilters();
  };

  applyFilters = async () => {
    await this.props.deactivateEventFiltering();
    await this.props.cleanPagination();
    await this.props.applyBoundingBoxFilter();

    this.props.getTweetsGeolocatedStarted();
    this.props.getTweets(
      this.props.filters,
      this.props.liveTW.dateEnd,
      null,
      this.props.paginationGeolocated,
      300,
      true,
      this.props.loadingStart,
      this.props.loadingStop
    );
    this.props.getTweetsStarted();
    this.props.getTweets(
      this.props.filters,
      this.props.liveTW.dateEnd,
      null,
      this.props.pagination,
      -1,
      false,
      this.props.loadingStart,
      this.props.loadingStop
    );
    this.props.getStatistics(this.props.filters, null, this.props.liveTW.dateEnd);
    this.props.getEventDetection(this.props.filters, null, this.props.liveTW.dateEnd);
  };

  resetFilters = async () => {
    await this.props.resetAllSocialFilters();
    if (!this.props.filters.language) {
      this._selectDefaultLanguage();
    }
  };

  handleCustomDisplaySelectionsHazards = values => values => {
    if (values.length > 0) {
      return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {values.map(({ value, label }, index) => (
            <Chip
              key={index}
              style={{ margin: '1px 5px 0px 5px' }}
              labelStyle={{
                fontSize: '12px',
                paddingLeft: '10px',
                paddingRight: '10px',
                lineHeight: '22px'
              }}
              deleteIconStyle={{
                margin: '0',
                height: '22px',
                width: '22px'
              }}
              onRequestDelete={() => this.handleRequestDelete(value, values, 'emergency')}
            >
              <Avatar size={22} style={{ height: '22px', width: '22px' }}>
                {HAZADR_SOCIAL_ICONS[value]}
              </Avatar>
              {this.props.collapsed ? '' : this.props.dictionary[label]}
            </Chip>
          ))}
        </div>
      );
    } else {
      return <div>Hazards</div>;
    }
  };
  handleCustomDisplaySelectionsinfotype = values => values => {
    if (values.length > 0) {
      return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {values.map(({ value, label }, index) => (
            <Chip
              key={index}
              style={{ margin: '1px 5px 0px 5px' }}
              labelStyle={{
                fontSize: '12px',
                paddingLeft: '10px',
                paddingRight: '10px',
                lineHeight: '22px'
              }}
              deleteIconStyle={{ margin: '0', height: '22px', width: '22px' }}
              onRequestDelete={() => this.handleRequestDelete(value, values, 'informative')}
            >
              <Avatar size={22} style={{ height: '22px', width: '22px' }}>
                <FontIcon style={{ lineHeight: '22px' }} className="material-icons">
                  info_outline
                </FontIcon>
              </Avatar>
              {this.props.dictionary[label]}
            </Chip>
          ))}
        </div>
      );
    } else {
      return <div>Info types</div>;
    }
  };
  handleCustomDisplaySelectionsTags = values => values => {
    if (values.length > 0) {
      return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {values.map(({ label, value }, index) => (
            <Chip
              key={index}
              style={{ margin: '1px 5px 0px 5px' }}
              labelStyle={{
                fontSize: '12px',
                paddingLeft: '10px',
                paddingRight: '10px',
                lineHeight: '22px'
              }}
              deleteIconStyle={{ margin: '0', height: '22px', width: '22px' }}
              onRequestDelete={() => this.handleRequestDelete(value, values, 'tags')}
            >
              <Avatar size={22} style={{ height: '22px', width: '22px' }}>
                <span style={{ color: value.color, lineHeight: '25px' }}>⬤</span>
              </Avatar>
              {this.props.dictionary[label]}
            </Chip>
          ))}
        </div>
      );
    } else {
      return <div>Tags</div>;
    }
  };
  collapseExpand = collapse => {
    this.props.collapseFilters(collapse);
  };

  handleRequestDelete = (key, values, type) => {
    let arrayOfItems = values;
    const chipToDelete = arrayOfItems.map(chip => chip.value).indexOf(key);
    arrayOfItems.splice(chipToDelete, 1);
    this.props.addSocialFilter(type, arrayOfItems);
  };

  _handleKeywords = event => {
    this.setState({
      keyword: event.target.value
    });
  };

  _handleUnwantedKeywords = event => {
    this.setState({
      unwantedKeyword: event.target.value
    });
  };

  _addKeyword = keyword => {
    const newArray = [...this.props.filters.filterKw, keyword];
    this.props.addSocialFilter('filterKw', newArray);
    this.setState({
      keyword: ''
    });
  };

  _addUnwantedKeyword = unwantedKeyword => {
    const newArray = [...this.props.filters.filterKwNot, unwantedKeyword];
    this.props.addSocialFilter('filterKwNot', newArray);
    this.setState({
      unwantedKeyword: ''
    });
  };

  _deleteKeyword = name => {
    const newArray = this.props.filters.filterKw.filter(keyword => keyword !== name);
    this.props.addSocialFilter('filterKw', newArray);
  };

  _deleteUnwantedKeyword = name => {
    const newArray = this.props.filters.filterKwNot.filter(
      unwantedKeyword => unwantedKeyword !== name
    );
    this.props.addSocialFilter('filterKwNot', newArray);
  };

  render() {
    const { style, muiTheme } = this.props;
    const aspectStyle = {
      color: muiTheme.palette.textColor
    };

    const hazardsNodeListHazards = hazards.map((hazard, index) => {
      return (
        <div key={index} value={hazard} label={HAZARD_LABELS[hazard]}>
          <div style={{ marginRight: 10 }}>
            <span>{this.props.dictionary[HAZARD_LABELS[hazard]]}</span>
            <br />
          </div>
        </div>
      );
    });
    const infotypeNodeList = infotype.map((info, index) => {
      return (
        <div key={index} value={info} label={SOCIAL_INFOTYPE_LABELS[info]}>
          <div style={{ marginRight: 10 }}>
            <span>{this.props.dictionary[SOCIAL_INFOTYPE_LABELS[info]]}</span>
            <br />
          </div>
        </div>
      );
    });

    const tagsNodeList = tags.map((tag, index) => {
      const label = tag['label'];
      return (
        <div key={index} value={tag} label={label}>
          <div style={{ marginRight: 10 }}>
            <span>{this.props.dictionary[label]}</span>
            <br />
          </div>
        </div>
      );
    });
    return (
      <section style={{ ...style, ...aspectStyle, ...{ position: 'relative' } }}>
        <div
          className="filterBar"
          style={this.props.eventFilter ? { cursor: 'not-allowed' } : { cursor: 'inherit' }}
        >
          <div className="groupFilters">
            <div className="socialFilter socialFilterFirstColumn">
              <div style={{ alignSelf: 'flex-end', padding: '5px', paddingRight: '10px' }}>
                <FontIcon
                  className="material-icons socialFilterLabel"
                  color={this.props.eventFilter !== null ? '#6f6f6f' : 'white'}
                >
                  chat
                </FontIcon>
              </div>
              <SelectField
                style={{
                  overflow: 'hidden',
                  width: '100%'
                }}
                value={this.props.filters.language}
                onChange={this.languageChange}
                disabled={this.props.eventFilter !== null}
              >
                {Object.keys(LANGUAGES).map(lang => (
                  <MenuItem key={lang} value={lang} primaryText={LANGUAGES[lang]} />
                ))}
              </SelectField>
            </div>
            <div className="socialFilter socialFilterSecondColumn">
              <Toggle
                disabled={this.props.eventFilter !== null}
                labelStyle={{ fontWeight: 'bolder' }}
                style={{
                  width: 'auto',
                  marginTop: 'auto',
                  padding: '5px 5px 3px 5px'
                }}
                label="Non-geolocalized"
                toggled={this.props.socialGeolocated}
                onToggle={() => this.props.changesocialGeolocated(!this.props.socialGeolocated)}
              />
            </div>
            <div className="socialFilter socialFilterThirdColumn">
              <SuperSelectField
                name="hazards"
                multiple
                hintText="hazards"
                onSelect={this.hazardsChange}
                value={this.props.filters.emergency}
                selectionsRenderer={this.handleCustomDisplaySelectionsHazards(
                  this.props.filters.emergency
                )}
                style={{ width: '100%', marginTop: 0 }}
                disabled={this.props.eventFilter !== null}
              >
                {hazardsNodeListHazards}
              </SuperSelectField>
            </div>
            <div className="socialFilter socialFilterButtons">
              <RaisedButton
                label="Clear all"
                onClick={this.resetFilters.bind(this)}
                style={{ margin: '5px' }}
                disabled={this.props.eventFilter !== null}
              />
              <RaisedButton
                label="Apply"
                primary={true}
                onClick={this.applyFilters.bind(this)}
                style={{ margin: '5px' }}
                disabled={this.props.eventFilter !== null}
              />
              <IconButton
                tooltip="expand/collapse filters"
                onClick={this.collapseExpand.bind(this, !this.props.collapsed)}
              >
                {this.props.collapsed ? (
                  <FontIcon className="material-icons keyboard-arrow">keyboard_arrow_down</FontIcon>
                ) : (
                  <FontIcon className="material-icons keyboard-arrow">keyboard_arrow_up</FontIcon>
                )}
              </IconButton>
            </div>
          </div>
          {this.props.collapsed ? null : (
            <div>
              <div className="groupFilters">
                <div className="socialFilter socialFilterFirstColumn">
                  <div style={{ marginTop: 0, paddingRight: '10px' }}>
                    <FontIcon
                      className="material-icons socialFilterLabel"
                      color={this.props.eventFilter !== null ? '#6f6f6f' : 'white'}
                    >
                      access_time
                    </FontIcon>
                  </div>
                  <div
                    style={{
                      display: 'inline-flex',
                      overflow: 'hidden',
                      paddingLeft: '5px',
                      width: '100%'
                    }}
                  >
                    {times.map(item => (
                      <FlatButton
                        disabled={this.props.eventFilter !== null}
                        key={item.label}
                        label={item.label}
                        primary
                        labelStyle={{
                          textTransform: 'lowercase',
                          paddingLeft: '10px',
                          paddingRight: '10px'
                        }}
                        style={{
                          ...{
                            minWidth: 0,
                            border: '1px solid',
                            borderRadius: 'none',
                            margin: '0px 1px'
                          },
                          ...(this.props.filters.dateTo.label === item.label
                            ? STYLES.selectedSocialTimeRangeFilter
                            : {})
                        }}
                        onClick={this.timeRangeChange.bind(this, item)}
                      />
                    ))}
                  </div>
                </div>
                <div className="socialFilter socialFilterSecondColumn">
                  <SuperSelectField
                    disabled={this.props.eventFilter !== null}
                    name="tags"
                    multiple
                    hintText="tags"
                    onSelect={a => this.tagsChange(a)}
                    value={this.props.filters.tags}
                    selectionsRenderer={this.handleCustomDisplaySelectionsTags(
                      this.props.filters.tags
                    )}
                    style={{ width: '310px', marginTop: 0 }}
                  >
                    {tagsNodeList}
                  </SuperSelectField>
                </div>
                <div className="socialFilter socialFilterThirdColumn">
                  <SuperSelectField
                    disabled={this.props.eventFilter !== null}
                    name="info"
                    multiple
                    hintText="info types"
                    onSelect={a => this.infotypeChange(a)}
                    value={this.props.filters.infotype}
                    selectionsRenderer={this.handleCustomDisplaySelectionsinfotype(
                      this.props.filters.infotype
                    )}
                    style={{ width: '100%', marginTop: 0 }}
                  >
                    {infotypeNodeList}
                  </SuperSelectField>
                </div>
                <div className="socialFilter socialFilterButtons">
                  <Checkbox
                    disabled={this.props.eventFilter !== null}
                    label="Retweets"
                    style={{ paddingLeft: '15px', boxSizing: 'border-box' }}
                    onCheck={(event, isInputChecked) => this.updateCheck(event, isInputChecked)}
                    checked={this.props.filters.retweet}
                  />
                </div>
              </div>
              <div className="groupFilters">
                <div className="socialFilter socialFilterFirstColumn" />
                <div className="socialFilter socialFilterSecondColumn">
                  <div
                    style={{
                      position: 'relative',
                      flexDirection: 'row',
                      display: 'flex',
                      alignItems: 'baseline',
                      flexWrap: 'wrap',
                      width: '310px'
                    }}
                  >
                    {this.props.filters.filterKw.map((keyword, index) => (
                      <Chip
                        key={index}
                        style={{ margin: '1px 5px 0px 5px' }}
                        labelStyle={{
                          fontSize: '12px',
                          paddingLeft: '10px',
                          paddingRight: '10px',
                          lineHeight: '22px'
                        }}
                        deleteIconStyle={{ margin: '0', height: '22px', width: '22px' }}
                        onRequestDelete={() => this._deleteKeyword(keyword, index)}
                      >
                        {keyword}
                      </Chip>
                    ))}
                    <div style={{ position: 'relative', flex: 1 }}>
                      <TextField
                        disabled={this.props.eventFilter !== null}
                        value={this.state.keyword}
                        hintText="Keywords"
                        onChange={this._handleKeywords}
                        style={{ width: '100%' }}
                      />
                      <IconButton
                        style={{
                          position: 'absolute',
                          right: 0,
                          top: 10,
                          padding: 0,
                          width: 'auto',
                          height: 'auto'
                        }}
                        onClick={() => {
                          this._addKeyword(this.state.keyword);
                        }}
                      >
                        <FontIcon color={'#777777'} className="material-icons">
                          add_circle_outline
                        </FontIcon>
                      </IconButton>
                    </div>
                  </div>
                </div>
                <div className="socialFilter socialFilterThirdColumn">
                  <div
                    style={{
                      position: 'relative',
                      flexDirection: 'row',
                      display: 'flex',
                      alignItems: 'baseline',
                      flexWrap: 'wrap'
                    }}
                  >
                    {this.props.filters.filterKwNot.map((unwantedKeyword, index) => (
                      <Chip
                        key={index}
                        style={{ margin: '1px 5px 0px 5px' }}
                        labelStyle={{
                          fontSize: '12px',
                          paddingLeft: '10px',
                          paddingRight: '10px',
                          lineHeight: '22px'
                        }}
                        deleteIconStyle={{ margin: '0', height: '22px', width: '22px' }}
                        onRequestDelete={() => this._deleteUnwantedKeyword(unwantedKeyword, index)}
                      >
                        {unwantedKeyword}
                      </Chip>
                    ))}
                    <div style={{ position: 'relative' }}>
                      <TextField
                        disabled={this.props.eventFilter !== null}
                        value={this.state.unwantedKeyword}
                        hintText="Unwanted keywords"
                        onChange={this._handleUnwantedKeywords}
                        style={{ width: '310px' }}
                      />
                      <IconButton
                        style={{
                          position: 'absolute',
                          right: 0,
                          top: 10,
                          padding: 0,
                          width: 'auto',
                          height: 'auto'
                        }}
                        onClick={() => {
                          this._addUnwantedKeyword(this.state.unwantedKeyword);
                        }}
                      >
                        <FontIcon color={'#777777'} className="material-icons">
                          add_circle_outline
                        </FontIcon>
                      </IconButton>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
        {this.props.eventFilter !== null && (
          <div
            style={{
              position: 'absolute',
              top: '0',
              width: '100%',
              height: '100%',
              background: 'rgba(33, 33, 33, 0.8)',
              alignItems: 'center',
              display: 'grid',
              textAlign: 'center'
            }}
          >
            <div>
              Tweets are filtered by a detected event, click{' '}
              <span
                style={{
                  color: iReactTheme.palette.primary1Color,
                  cursor: 'pointer',
                  textDecoration: 'underline'
                }}
                onClick={this.resetEventFilter.bind(this)}
              >
                here
              </span>{' '}
              to reset
            </div>
          </div>
        )}
      </section>
    );
  }
}

export default enhance(muiThemeable()(FiltersBar));
