export const SOCIAL_HAZARDS = [
  'drought',
  'earthquake',
  'extreme_weather_conditions',
  'wildfires',
  'floods',
  'landslide',
  'storms',
  'snow'
];

export const SOCIAL_INFOTYPE = [
  'caution_advice',
  'donations',
  'volunteering',
  'affected_individuals',
  'infrastructures',
  'other_info'
];

export const SOCIAL_TAGS = [
  {
    type: 'sentiment',
    value: 'none',
    label: '_no_panic',
    color: 'green'
  },
  {
    type: 'sentiment',
    value: 'panic',
    label: '_panic',
    color: 'red'
  },
  {
    type: 'informative',
    value: true,
    label: '_informative',
    color: 'green'
  },
  {
    type: 'informative',
    value: false,
    label: '_non_informative',
    color: 'red'
  }
];

export const HAZADR_SOCIAL_ICONS = {
  floods: '💧',
  fire: '🔥',
  extreme_weather_conditions: '🌀️',
  landslide: '⛰️',
  earthquake: '🌋',
  drought: '🌵',
  storms: '⚡️',
  snow: '❄️',
  wildfires: '🔥'
};

export const HAZARD_TYPES_FOR_EVENTS_EQUIVALENCY = {
  extreme_weather_conditions: '_hazard_extremeWeather',
  drought: '_hazard_extremeWeather',
  earthquake: '_hazard_earthquake',
  wildfires: '_hazard_fire',
  floods: '_hazard_flood',
  landslide: '_hazard_landslide',
  storms: '_hazard_extremeWeather',
  snow: '_hazard_extremeWeather'
};

export const HAZARD_TYPES_FOR_ICON_EQUIVALENCY = {
  extreme_weather_conditions: 'extremeWeather',
  drought: 'extremeWeather',
  earthquake: 'earthquake',
  wildfires: 'fire',
  floods: 'flood',
  landslide: 'landslide',
  storms: 'extremeWeather',
  snow: 'extremeWeather'
};

export const HAZARD_LABELS = {
  drought: '_drought',
  earthquake: '_hazard_earthquake',
  extreme_weather_conditions: '_hazard_extremeWeather',
  wildfires: '_hazard_fire',
  floods: '_flood',
  landslide: '_hazard_landslide',
  storms: '_storm',
  snow: '_snow'
};

export const SOCIAL_INFOTYPE_LABELS = {
  caution_advice: '_caution_and_advice',
  donations: '_donation',
  volunteering: '_volunteering',
  affected_individuals: '_affected_individuals',
  infrastructures: '_infraestructures',
  other_info: '_other_info'
};

export const LANGUAGES = {
  en: 'English',
  es: 'Español',
  fi: 'Finnish',
  it: 'Italiano',
  de: 'Deutsch',
  fr: 'Français'
};
