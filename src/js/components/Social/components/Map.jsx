import React, { Component } from 'react';
import { compose } from 'redux';

import { featureCollection, polygon } from '@turf/helpers';

// import bbox from '@turf/bbox';
import { MapView } from 'js/components/MapView';
import { onMapLoad, clusterMaxZoom } from './mapInitialization';
import MapboxDraw from '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw';

import { withTweets } from 'js/modules/socialApi';
import { withSocialFilters } from 'js/modules/socialFilters';
// import { withMap } from 'js/modules/map';
import { withGeolocation } from 'ioc-geolocation';
import { withMapPreferences } from 'js/modules/preferences';
import { withTopBarDate } from 'js/modules/topBarDate';
import { withDictionary } from 'ioc-localization';
import { withSelectedTweet } from 'js/modules/selectedTweet';
import { withLoader } from 'js/modules/ui';
import { withAPISettings } from 'ioc-api-interface';
import { withEventDetection } from 'js/modules/socialApi';
import { Spiderifier } from './spiderifier';
import { RaisedButton } from 'material-ui/';
import { compareBbox } from './utilsMapSearching';
// import { featureFilter as ff } from '@mapbox/mapbox-gl-style-spec';

/* eslint-disable */
// on Windows won't work with import (check with latest version)
/* eslint-enable */
const enhance = compose(
  withAPISettings,
  // withMap,
  withMapPreferences,
  withGeolocation,
  withTweets,
  withSocialFilters,
  withTopBarDate,
  withDictionary,
  withSelectedTweet,
  withLoader,
  withEventDetection
);

const SB_SIZE = 10; // pixel

const controlsOpts = {
  displayControlsDefault: false
};
const MIN_ZOOM = 1;
const MAX_ZOOM = 22;
const DEFAULT_ZOOM = 10;

class Map extends Component {
  constructor(props) {
    super(props);
    this.mapView = null;
    this.draw = null;
    const coords = this.props.geolocationPosition.coords;
    const center =
      typeof coords.longitude === 'number' && typeof coords.latitude === 'number'
        ? [coords.longitude, coords.latitude]
        : [0, 0];
    this.state = {
      dialogVisible: false,
      mapState: {
        center,
        zoom: DEFAULT_ZOOM,
        pitch: 0,
        bearing: 0,
        bounds: undefined
      },
      spiderifierDone: false
    };
  }
  spiderifier = new Spiderifier();

  componentWillUnmount() {
    this.props.onRef(undefined);
  }

  componentDidMount = async () => {
    this.props.onRef(this);
  };
  componentDidUpdate = async prevProps => {
    if (this.props.tweetsGeolocated !== prevProps.tweetsGeolocated) {
      if (this.mapView) {
        const map = this.mapView.getMap();
        if (map) {
          const hideTweetsSrc = map.getSource('hide-tweets');
          const tweetsSrc = map.getSource('tweets');
          if (hideTweetsSrc && tweetsSrc) {
            tweetsSrc.setData(featureCollection(this.props.tweetsGeolocated));
            hideTweetsSrc.setData(featureCollection(this.props.tweetsGeolocated));
            if (this.state.spiderifierDone) this._spiderify(map);
          }
        }
      }
    }
    if (this.props.bboxToSearch !== prevProps.bboxToSearch) {
      if (this.mapView) {
        const map = this.mapView.getMap();
        if (map) {
          const bboxSrc = map.getSource('bounding-box');
          if (bboxSrc)
            bboxSrc.setData(
              featureCollection([
                {
                  geometry: { type: 'Polygon', coordinates: [this.props.bboxToSearch] }
                }
              ])
            );
        }
      }
    }
  };
  _updateMapSettings = newSettings => {
    const { center, zoom, pitch, bearing, bounds } = newSettings;
    this.setState(prevState => {
      const mapState = { ...prevState.mapState, center, zoom, pitch, bearing, bounds };
      return { mapState };
    });
  };
  _onMapLoadEnd = (map, mapSettings) => {
    this.draw = new MapboxDraw(controlsOpts);
    const tweetsSrc = map.getSource('tweets');
    const hideTweetsSrc = map.getSource('hide-tweets');
    const bboxSrc = map.getSource('bounding-box');

    if (bboxSrc) {
      bboxSrc.setData(
        featureCollection([
          {
            geometry: { type: 'Polygon', coordinates: [this.props.bboxToSearch] }
          }
        ])
      );
    }

    if (tweetsSrc) {
      tweetsSrc.setData(featureCollection(this.props.tweetsGeolocated));
    }
    if (hideTweetsSrc) {
      hideTweetsSrc.setData(featureCollection(this.props.tweetsGeolocated));
    }
    this.drawEvent(this.props.eventDetection[this.props.eventIndex]);
    map.addControl(this.draw);
  };

  _onMapMove = a => {
    const map = this.mapView.getMap();
    const event = this.props.eventDetection[this.props.eventIndex];
    let bbox;
    if (this.props.eventFilter && event) {
      const x1 = event.boundingBox.southWest.longitude; //the lowest x coordinate
      const y1 = event.boundingBox.southWest.latitude; //the lowest y coordinate
      const x2 = event.boundingBox.northEast.longitude; //the highest x coordinate
      const y2 = event.boundingBox.northEast.latitude; //the highest y coordinate
      bbox = [[x2, y1], [x1, y1], [x1, y2], [x2, y2], [x2, y1]];
    } else {
      bbox = [
        [map.getBounds().getWest(), map.getBounds().getSouth()],
        [map.getBounds().getEast(), map.getBounds().getSouth()],
        [map.getBounds().getEast(), map.getBounds().getNorth()],
        [map.getBounds().getWest(), map.getBounds().getNorth()],
        [map.getBounds().getWest(), map.getBounds().getSouth()]
      ];
    }

    if (!compareBbox(bbox, this.props.bboxToSearch)) this.props.moveMap();
    this.props.updatePosition().catch(e => {
      console.error('Cannot update position', e);
    });
  };

  addClickTweet = (id, layer) => {
    if (this.mapView) {
      const map = this.mapView.getMap();
      if (map) {
        map.setFilter(layer, ['==', 'tweetId', id]);
        const featureCoords = this.props.selectedGeometry.coordinates;
        const currentZoom = map.getZoom();
        const nextZoom = 14;
        const zoomDifference = nextZoom - currentZoom;
        const duration = zoomDifference > 4 ? 1200 : 1000;
        const animParams = {
          center: featureCoords,
          zoom: nextZoom,
          bearing: 0,
          pitch: 0,
          duration,
          offset: [0, 0]
        };
        map.flyTo(animParams);
      }
    }
  };

  deleteClickTweet = () => {
    if (this.mapView) {
      const map = this.mapView.getMap();
      if (map) {
        map.setFilter('tweet-marker-click', ['==', 'name', '']);
        map.setFilter('spiderify-tweet-marker-click', ['==', 'name', '']);
      }
    }
  };

  _onMapClick = e => {
    const map = e.target;
    const point = e.point;

    const bb = [
      [point.x - SB_SIZE / 2, point.y - SB_SIZE / 2],
      [point.x + SB_SIZE / 2, point.y + SB_SIZE / 2]
    ];
    const clusters = map.queryRenderedFeatures(bb, { layers: ['clusters'] });
    const features = map.queryRenderedFeatures(bb, { layers: ['tweet-marker'] });
    const spiderFeatures = map.queryRenderedFeatures(bb, { layers: ['spiderify-tweet-marker'] });

    if (clusters && clusters.length) {
      map.flyTo({
        center: e.lngLat,
        zoom: map.getZoom() + 2
      });
    }

    if (features && features.length) {
      const feature = features[0];
      this._selectTweet(feature.properties.tweetId, feature.geometry, 'tweet-marker-click');
    } else if (spiderFeatures && spiderFeatures.length) {
      const feature = spiderFeatures[0];
      this._selectTweet(
        feature.properties.tweetId,
        feature.geometry,
        'spiderify-tweet-marker-click'
      );
    } else {
      this.props.deleteClickTweet();
      this.deleteClickTweet();
    }
  };

  _selectTweet = async (tweetId, geometry, layer) => {
    if (this.props.selectedTweet !== tweetId) {
      await this.props.clickTweet(tweetId, geometry);
      this.addClickTweet(tweetId, layer);
    } else {
      this.props.deleteClickTweet();
      this.deleteClickTweet();
    }
  };

  drawEvent = event => {
    if (this.mapView) {
      const map = this.mapView.getMap();
      if (map) {
        if (event) {
          const x1 = event.boundingBox.southWest.longitude; //the lowest x coordinate
          const y1 = event.boundingBox.southWest.latitude; //the lowest y coordinate
          const x2 = event.boundingBox.northEast.longitude; //the highest x coordinate
          const y2 = event.boundingBox.northEast.latitude; //the highest y coordinate

          const eventSrc = map.getSource('event-detection');
          if (eventSrc) {
            const bbox = [[x2, y1], [x1, y1], [x1, y2], [x2, y2], [x2, y1]];
            const data = polygon([bbox]);

            eventSrc.setData(data);
            // const duration = Math.round(map.getZoom() + 1 / 3) * 200;
            // map.fitBounds([bbox[1], bbox[3]], {
            //   duration,
            //   padding: 24,
            //   maxZoom: map.getMaxZoom()
            // });
          }
          const eventPointSrc = map.getSource('event-detection-point');

          if (eventPointSrc) {
            let center = {};
            center.x = x1 + (x2 - x1) / 2;
            center.y = y1 + (y2 - y1) / 2;

            eventPointSrc.setData({
              type: 'Feature',
              properties: { hazard: event.eventType },
              geometry: {
                type: 'Point',
                coordinates: [center.x, center.y]
              }
            });
          }
        } else {
          const eventSrc = map.getSource('event-detection');
          if (eventSrc) {
            eventSrc.setData(featureCollection([]));
            const eventPointSrc = map.getSource('event-detection-point');
            if (eventPointSrc) {
              eventPointSrc.setData(featureCollection([]));
            }
          }
        }
      }
    }
  };

  goToEventBoundingBox = event => {
    if (this.mapView) {
      const map = this.mapView.getMap();
      if (map && event) {
        const x1 = event.boundingBox.southWest.longitude; //the lowest x coordinate
        const y1 = event.boundingBox.southWest.latitude; //the lowest y coordinate
        const x2 = event.boundingBox.northEast.longitude; //the highest x coordinate
        const y2 = event.boundingBox.northEast.latitude; //the highest y coordinate
        const bbox = [[x2, y1], [x1, y1], [x1, y2], [x2, y2], [x2, y1]];

        const duration = Math.round(map.getZoom() + 1 / 3) * 200;

        map.fitBounds([bbox[1], bbox[3]], {
          duration,
          padding: 24,
          maxZoom: map.getMaxZoom()
        });
      }
    }
  };

  _spiderify = map => {
    /**
     * Get the tweets presents on the visible bounding box
     * Its needed a hide layer ('hide-tweets-layer') cause the visible layer is
     * clustered and doesn't returns the entire feature's info
     */
    let features = map.queryRenderedFeatures({ layers: ['hide-tweets-layer'] });
    features = [...new Set(features.map(o => JSON.stringify(o)))].map(s => JSON.parse(s));
    if (features && features.length > 1) {
      /* It extracts the tweets on the same position and calculate their new states*/
      const spiderFeatureColection = this.spiderifier.getSpiderifiedFeatures(features);
      if (spiderFeatureColection != null && spiderFeatureColection.length > 0) {
        /* It hides the modified tweets*/
        map.setLayoutProperty('tweet-marker-click', 'visibility', 'none');
        spiderFeatureColection.forEach(spider => {
          map.setFilter('tweet-marker', [
            'all',
            ['!has', 'point_count'],
            ...spider.originalFeatures.map(f => ['!=', 'tweetId', f.properties.tweetId])
          ]);

          const spiderifierSrc = map.getSource('spiderify-tweets');
          const centerSpiderifierSrc = map.getSource('center-spiderify-tweets');
          if (spiderifierSrc) {
            spiderifierSrc.setData(featureCollection(spider.spider));
            centerSpiderifierSrc.setData(featureCollection(spider.center));
          }
        });
      }
    }
  };

  _onMapZoom = async e => {
    const map = this.mapView.getMap();
    // spiderifier
    if (map.getZoom() >= clusterMaxZoom + 1 && !this.state.spiderifierDone) {
      this.setState({ spiderifierDone: true });
      this._spiderify(map);
    }
    // unspiderifier
    else if (map.getZoom() <= clusterMaxZoom + 1 && this.state.spiderifierDone) {
      const spiderifierSrc = map.getSource('spiderify-tweets');
      const centerSpiderifierSrc = map.getSource('center-spiderify-tweets');

      centerSpiderifierSrc.setData(featureCollection([]));
      spiderifierSrc.setData(featureCollection([]));

      map.setFilter('tweet-marker', ['!has', 'point_count']);
      map.setLayoutProperty('tweet-marker-click', 'visibility', 'visible');
      this.setState({ spiderifierDone: false });
    }
  };

  _repeatResearchNewLocation = () => {
    const map = this.mapView.getMap();
    const bbox = [
      [map.getBounds().getWest(), map.getBounds().getSouth()],
      [map.getBounds().getEast(), map.getBounds().getSouth()],
      [map.getBounds().getEast(), map.getBounds().getNorth()],
      [map.getBounds().getWest(), map.getBounds().getNorth()],
      [map.getBounds().getWest(), map.getBounds().getSouth()]
    ];
    this.props.setBoundingBox(bbox);
  };

  _loadMoreTweetsGeolocated = () => {
    this.props.getTweets(
      this.props.filters,
      this.props.liveTW.dateEnd,
      null,
      this.props.paginationGeolocated,
      300,
      true,
      this.props.loadingStart,
      this.props.loadingStop
    );
  };

  render() {
    const mapSettings = {
      social: true,
      style: this.props.preferences.mapStyle,
      center: this.state.mapState.center,
      zoom: this.state.mapState.zoom,
      pitch: this.state.mapState.pitch,
      bearing: this.state.mapState.bearing,
      bounds: this.state.mapState.bounds,
      // maxBounds: this.props.activeEventAOI
      //   ? this.props.activeEventAOI.bounds
      //   : this.props.liveAOI.bounds,
      minZoom: MIN_ZOOM,
      maxZoom: MAX_ZOOM
    };
    // const dict = this.props.dictionary;
    return (
      <div ref={div => (this.mapDiv = div)} className="map bound" style={{ ...this.props.style }}>
        <div className="buttonResearch">
          {(this.props.tweetsGeolocated.length > 0 &&
            this.props.tweetsGeolocated.length >= this.props.paginationGeolocated) ||
          this.props.mapMoved ? (
            <RaisedButton
              label={this.props.mapMoved ? 'Repeat research here' : 'Show more'}
              labelStyle={{ textTransform: 'none' }}
              primary={true}
              onClick={() => {
                this.props.mapMoved
                  ? this._repeatResearchNewLocation()
                  : this._loadMoreTweetsGeolocated();
              }}
              style={{ margin: '5px' }}
            />
          ) : null}
        </div>
        <MapView
          ref={mapView => (this.mapView = mapView)}
          geolocation={this.props.geolocationPosition}
          mapSettings={mapSettings}
          onSettingsUpdate={this._updateMapSettings}
          onMapLoad={onMapLoad}
          onMapZoom={this._onMapZoom}
          onMapError={err => console.error('Map Err', err)}
          onMapLoadEnd={this._onMapLoadEnd}
          onMapMoveEnd={this._onMapMove}
          onMapClick={this._onMapClick}
          onMapHover={() => {}}
          onMapMouseLeave={() => {}}
        />
      </div>
    );
  }
}
export default enhance(Map);
