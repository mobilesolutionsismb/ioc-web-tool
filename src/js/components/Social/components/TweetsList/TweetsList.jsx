import React, { Component } from 'react';
import TweetCard from './TweetCard';
import { compose } from 'redux';

import muiThemeable from 'material-ui/styles/muiThemeable';
import { FontIcon } from 'material-ui/';
import { getLocalDate } from 'js/utils/localizeDate';
import { InfiniteLoader } from 'react-virtualized';
import { List, CellMeasurer, CellMeasurerCache, AutoSizer } from 'react-virtualized';
import { withGeolocation } from 'ioc-geolocation';

import { withTweets } from 'js/modules/socialApi';
import { withLoader } from 'js/modules/ui';
import { withSocialFilters } from 'js/modules/socialFilters';
import { withSelectedTweet } from 'js/modules/selectedTweet';

import { withAPISettings } from 'ioc-api-interface';
import { withTopBarDate } from 'js/modules/topBarDate';
import { withDictionary } from 'ioc-localization';
import { LANGUAGES } from '../socialTagsConfig';
import { withTweetFeedback, withLogin } from 'ioc-api-interface';

const typeList = {
  geolocation: {
    title: 'Geolocalized',
    icon: 'location_on'
  },
  non_geolocation: {
    title: 'Non-geolocalized',
    icon: 'location_off'
  }
};

const cache = new CellMeasurerCache({
  defaultHeight: 230,
  fixedWidth: true
});

const geoCache = new CellMeasurerCache({
  defaultHeight: 230,
  fixedWidth: true
});

const enhance = compose(
  withSocialFilters,
  withTweets,
  withAPISettings,
  withLoader,
  withTopBarDate,
  withDictionary,
  withTweetFeedback,
  withLogin,
  withSelectedTweet,
  withGeolocation
);

class TweetsList extends Component {
  _container = null;
  _tweetCards = [];
  listGeolocated = null;
  list = null;

  state = {
    //height: Math.max(window.innerHeight - FIXED_HEADER_SIZE, 100) // we don't want negative numbers
    scrollToColumn: 0,
    scrollToRow: 0
  };

  componentDidMount = async () => {
    const boundingBox = [
      [
        this.props.geolocationPosition.coords.longitude - 0.4,
        this.props.geolocationPosition.coords.latitude - 0.2
      ],
      [
        this.props.geolocationPosition.coords.longitude - 0.4,
        this.props.geolocationPosition.coords.latitude + 0.2
      ],
      [
        this.props.geolocationPosition.coords.longitude + 0.4,
        this.props.geolocationPosition.coords.latitude + 0.2
      ],
      [
        this.props.geolocationPosition.coords.longitude + 0.4,
        this.props.geolocationPosition.coords.latitude - 0.2
      ],
      [
        this.props.geolocationPosition.coords.longitude - 0.4,
        this.props.geolocationPosition.coords.latitude - 0.2
      ]
    ];
    await this.props.setBoundingBox(boundingBox);
    await this.props.cleanPagination();
    this._loadMore();
    this._loadMoreGeolocated();
  };
  componentDidUpdate = async prevProps => {
    if (prevProps.selectedTweet !== this.props.selectedTweet) {
      const selected =
        this.props.selectedTweet !== null && this.props.tweetId === this.props.selectedTweet;
      if (selected && this._card) {
        typeof this._card.scrollIntoViewIfNeeded === 'function' //webkit broweser, does better
          ? this._card.scrollIntoViewIfNeeded()
          : this._card.scrollIntoView(); // other sad browsers
      }
    }
    if (prevProps.tweetsGeolocated !== this.props.tweetsGeolocated) {
      /** We clean the cache height of virtualized list */
      geoCache.clearAll();
    }
    if (prevProps.tweets !== this.props.tweets) {
      cache.clearAll();
    }
    if (
      this.props.bboxToSearch !== prevProps.bboxToSearch &&
      this.props.tweetsGeolocated.length > 0
    ) {
      await this.props.cleanGeolocationPagination();
      this._loadMoreGeolocated();
    }
    if (this.props.tweetFeedback !== prevProps.tweetFeedback) {
      cache.clearAll();

      if (this.listGeolocated) this.listGeolocated.forceUpdateGrid();
      if (this.list) this.list.forceUpdateGrid();
    }
    if (this.props.socialGeolocated !== prevProps.socialGeolocated) {
      geoCache.clearAll();
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.tweetFeedback !== nextProps.tweetFeedback ||
      nextProps.tweetsGeolocated !== this.props.tweetsGeolocated ||
      nextProps.tweets !== this.props.tweets ||
      this.props.bboxToSearch !== nextProps.bboxToSearch ||
      nextProps.selectedTweet !== this.props.selectedTweet ||
      this.props.socialGeolocated !== nextProps.socialGeolocated ||
      this.props.collapsed !== nextProps.collapsed
    );
  }

  _selectDefaultLanguage = () => {
    if (LANGUAGES[this.props.locale]) this.props.addSocialFilter('language', this.props.locale);
    else this.props.addSocialFilter('language', 'en');
  };

  _loadMore = async a => {
    this.props.getTweetsStarted();
    if (!this.props.filters.language) {
      await this._selectDefaultLanguage();
    }
    if (!this.props.startDate) {
      await this.props.selectDate(getLocalDate(null, -7, 'days').toDate(), getLocalDate().toDate());
    }
    return this.props.getTweets(
      this.props.filters,
      this.props.liveTW.dateEnd,
      null,
      this.props.pagination,
      -1,
      false,
      this.props.loadingStart,
      this.props.loadingStop
    );
  };

  _loadMoreGeolocated = async e => {
    this.props.getTweetsGeolocatedStarted();
    if (!this.props.filters.language) {
      await this._selectDefaultLanguage();
    }
    if (!this.props.startDate) {
      await this.props.selectDate(getLocalDate(null, -7, 'days').toDate(), getLocalDate().toDate());
    }
    this.props.getTweets(
      this.props.filters,
      this.props.liveTW.dateEnd,
      null,
      this.props.paginationGeolocated,
      300,
      true,
      this.props.loadingStart,
      this.props.loadingStop
    );
  };

  _setContainerRef = e => (this._container = e);

  isRowLoaded = ({ index }) => {
    return !!this.props.tweets[index];
  };

  /** */
  _rowRenderer = ({ index, isScrolling, key, parent, style }) => {
    return (
      <CellMeasurer cache={cache} columnIndex={0} key={key} parent={parent} rowIndex={index}>
        <div style={style}>
          <TweetCard
            feedBack={this.props.tweetFeedback[this.props.tweets[index].tweetId]}
            tweet={this.props.tweets[index]}
          />
        </div>
      </CellMeasurer>
    );
  };
  _rowRendererGeolocated = ({ index, isScrolling, key, parent, style }) => {
    return (
      <CellMeasurer cache={geoCache} columnIndex={0} key={key} parent={parent} rowIndex={index}>
        <div style={style}>
          <TweetCard
            feedBack={
              this.props.tweetFeedback[this.props.tweetsGeolocated[index].properties.tweetId]
            }
            geometry={this.props.tweetsGeolocated[index].geometry}
            clickTweetCard={this.props.clickTweetCard}
            noClickTweetCard={this.props.noClickTweetCard}
            tweet={this.props.tweetsGeolocated[index].properties}
          />
        </div>
      </CellMeasurer>
    );
  };

  render() {
    const {
      style,
      muiTheme,
      hasMore,
      selectedTweet,
      tweetsGeolocated /*, dictionary*/
    } = this.props;
    const aspectStyle = {
      color: muiTheme.palette.textColor,
      backgroundColor: muiTheme.palette.appBarColor
    };

    /** Scrolling to the selected tweet on the map */
    let scrollToIndex = selectedTweet
      ? tweetsGeolocated.findIndex(tweet => selectedTweet === tweet.properties.tweetId)
      : undefined;

    const isRowLoaded = ({ index }) => !hasMore || index < this.props.tweets.length;

    return (
      <section
        ref={this._setContainerRef}
        style={{ ...style, ...aspectStyle, ...{ position: 'relative' } }}
        className="container tweets-lazy-scroll"
      >
        {this.props.type === 'non_geolocation' ? (
          <div className="container posts-lazy-scroll">
            <div
              style={{
                boxSizing: 'border-box',
                padding: '15px',
                position: 'sticky',
                top: 0,
                zIndex: '99',
                width: '100%'
              }}
            >
              <FontIcon className="material-icons" style={{ fontSize: '18px' }}>
                {typeList[this.props.type].icon}
              </FontIcon>
              {this.props.tweets ? this.props.tweets.length : ' 0'}{' '}
              {typeList[this.props.type].title}
            </div>
            <InfiniteLoader
              isRowLoaded={isRowLoaded}
              loadMoreRows={this._loadMore}
              rowCount={this.props.tweets.length + 1}
            >
              {({ onRowsRendered, registerChild }) => (
                <AutoSizer>
                  {({ height, width }) => {
                    cache.clearAll();
                    return (
                      <List
                        ref={c => {
                          this.list = c;
                          registerChild(c);
                        }}
                        onRowsRendered={onRowsRendered}
                        rowRenderer={this._rowRenderer}
                        height={window.innerHeight - (this.props.collapsed ? 240 : 346)}
                        width={window.innerWidth * (this.props.socialGeolocated ? 0.26 : 0) - 25} //0.26 'cause the tweet list is the 26% of the screen
                        rowHeight={cache.rowHeight}
                        rowCount={this.props.tweets.length}
                        deferredMeasurementCache={cache}
                      />
                    );
                  }}
                </AutoSizer>
              )}
            </InfiniteLoader>
            <div className="row posts-lazy-scroll__messages">
              {this.props.isFetching && (
                <div className="alert alert-info"> Loading more posts... </div>
              )}

              {!this.props.hasMore && (
                <div className="alert alert-success">
                  All the tweets has been loaded successfully.
                </div>
              )}
            </div>
          </div>
        ) : (
          <div className="container posts-lazy-scroll">
            <div
              style={{
                boxSizing: 'border-box',
                padding: '15px',
                position: 'sticky',
                top: 0,
                zIndex: '99',
                width: '100%'
              }}
            >
              <FontIcon className="material-icons" style={{ fontSize: '18px' }}>
                {typeList[this.props.type].icon}
              </FontIcon>
              {tweetsGeolocated ? tweetsGeolocated.length : ' 0'} {typeList[this.props.type].title}
            </div>
            {tweetsGeolocated && tweetsGeolocated.length > 0 && (
              <AutoSizer>
                {({ height, width }) => {
                  geoCache.clearAll();
                  return (
                    <List
                      ref={c => (this.listGeolocated = c)}
                      rowRenderer={this._rowRendererGeolocated}
                      height={window.innerHeight - (this.props.collapsed ? 240 : 346)}
                      width={window.innerWidth * (this.props.socialGeolocated ? 0.26 : 0.5) - 25}
                      rowHeight={geoCache.rowHeight}
                      rowCount={tweetsGeolocated.length}
                      deferredMeasurementCache={geoCache}
                      scrollToRow={scrollToIndex}
                      scrollToIndex={scrollToIndex}
                    />
                  );
                }}
              </AutoSizer>
            )}
            <div className="row posts-lazy-scroll__messages">
              {this.props.isFetchingGeolocated && (
                <div className="alert alert-info"> Loading more tweets... </div>
              )}

              {!this.props.hasMoreGeolocated && (
                <div className="alert alert-success">
                  All the tweets has been loaded successfully.
                </div>
              )}
            </div>
          </div>
        )}
      </section>
    );
  }
}

export default enhance(muiThemeable()(TweetsList));
