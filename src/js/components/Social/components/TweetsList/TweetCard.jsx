import React, { Component } from 'react';
import { compose } from 'redux';

import {
  Card,
  CardHeader,
  CardMedia,
  CardText,
  IconButton,
  FontIcon,
  Chip,
  Avatar
} from 'material-ui';
import moment from 'moment';
import { withDictionary } from 'ioc-localization';
import { withSelectedTweet } from 'js/modules/selectedTweet';
import { iReactTheme } from 'js/startup/iReactTheme';
import { withMessages } from 'js/modules/ui';
import EditTag from './EditTag/EditTag';
import { withTweetFeedback, withLogin } from 'ioc-api-interface';
import { withTweets } from 'js/modules/socialApi';

import {
  SOCIAL_TAGS,
  HAZADR_SOCIAL_ICONS,
  HAZARD_LABELS,
  SOCIAL_INFOTYPE_LABELS
} from '../socialTagsConfig.js';

import Linkify from 'react-linkify';

const STYLES = {
  triangleTopleft: {
    width: 0,
    height: 0,
    borderTop: '40px solid',
    borderRight: '40px solid transparent',
    borderTopColor: iReactTheme.palette.primary1Color,
    position: 'absolute',
    zIndex: 99,
    filter: 'drop-shadow(2px 2px 2px rgba(1, 1, 1, 0.5))'
  }
};
const enhance = compose(
  withMessages,
  withSelectedTweet,
  withDictionary,
  withTweetFeedback,
  withLogin,
  withTweets
);

const TwitterIcon = props => (
  <svg
    id="Capa_1"
    x="0px"
    y="0px"
    width={props.width}
    height={props.height}
    viewBox="0 0 512.002 512.002"
    style={{ enableBackground: 'new 0 0 512.002 512.002' }}
  >
    <g>
      <path
        d="M512.002,97.211c-18.84,8.354-39.082,14.001-60.33,16.54c21.686-13,38.342-33.585,46.186-58.115   c-20.299,12.039-42.777,20.78-66.705,25.49c-19.16-20.415-46.461-33.17-76.674-33.17c-58.011,0-105.042,47.029-105.042,105.039   c0,8.233,0.929,16.25,2.72,23.939c-87.3-4.382-164.701-46.2-216.509-109.753c-9.042,15.514-14.223,33.558-14.223,52.809   c0,36.444,18.544,68.596,46.73,87.433c-17.219-0.546-33.416-5.271-47.577-13.139c-0.01,0.438-0.01,0.878-0.01,1.321   c0,50.894,36.209,93.348,84.261,103c-8.813,2.399-18.094,3.687-27.674,3.687c-6.769,0-13.349-0.66-19.764-1.888   c13.368,41.73,52.16,72.104,98.126,72.949c-35.95,28.176-81.243,44.967-130.458,44.967c-8.479,0-16.84-0.496-25.058-1.471   c46.486,29.807,101.701,47.197,161.021,47.197c193.211,0,298.868-160.062,298.868-298.872c0-4.554-0.104-9.084-0.305-13.59   C480.111,136.775,497.92,118.275,512.002,97.211z"
        fill={props.color}
      />
    </g>
  </svg>
);

const Tags = props => {
  return (
    <div
      style={{
        display: 'inline-flex',
        alignItems: 'center',
        margin: '0 10px 10px',
        width: '70%',
        flexWrap: 'wrap'
      }}
    >
      {props.tags
        ? props.tags.map((item, i) => (
            <Chip
              style={{ margin: '2.5px' }}
              labelStyle={{
                fontSize: '12px',
                paddingLeft: '10px',
                paddingRight: '10px',
                lineHeight: '22px'
              }}
              key={i}
            >
              <Avatar size={22} style={{ height: '22px', width: '25px' }}>
                {HAZADR_SOCIAL_ICONS[item] ? (
                  HAZADR_SOCIAL_ICONS[item]
                ) : (
                  <FontIcon className="material-icons" style={{ lineHeight: '22px' }}>
                    info_outline
                  </FontIcon>
                )}
              </Avatar>
              {HAZARD_LABELS[item]
                ? props.dictionary[HAZARD_LABELS[item]]
                : SOCIAL_INFOTYPE_LABELS[item]
                ? props.dictionary[SOCIAL_INFOTYPE_LABELS[item]]
                : item}
            </Chip>
          ))
        : null}
      {props.entities
        ? props.entities.map((item, i) => (
            <Chip
              style={{ margin: '2.5px' }}
              labelStyle={{
                fontSize: '12px',
                paddingLeft: '10px',
                paddingRight: '10px',
                lineHeight: '22px'
              }}
              key={i}
            >
              <Avatar size={22} style={{ height: '22px', width: '25px' }}>
                <FontIcon
                  className="material-icons"
                  style={{ lineHeight: '22px', fontSize: '20px' }}
                >
                  location_city
                </FontIcon>
              </Avatar>
              {item.name ? item.name : item.label}
            </Chip>
          ))
        : null}
      <Chip
        style={{ margin: '2.5px' }}
        labelStyle={{
          fontSize: '12px',
          paddingLeft: '10px',
          paddingRight: '10px',
          lineHeight: '22px'
        }}
      >
        <Avatar size={22} style={{ height: '22px', width: '25px' }}>
          <span
            style={
              props.info
                ? { color: 'green', lineHeight: '22px' }
                : { color: 'red', lineHeight: '22px' }
            }
          >
            ⬤
          </span>
        </Avatar>
        {
          props.dictionary[
            SOCIAL_TAGS.find(a => a.value === props.info && a.type === 'informative').label
          ]
        }
      </Chip>
      <Chip
        style={{ margin: '2.5px' }}
        labelStyle={{
          fontSize: '12px',
          paddingLeft: '10px',
          paddingRight: '10px',
          lineHeight: '22px'
        }}
      >
        <Avatar size={22} style={{ height: '22px', width: '25px' }}>
          <span
            style={
              props.panic === 'panic'
                ? { color: 'red', lineHeight: '22px' }
                : { color: 'green', lineHeight: '22px' }
            }
          >
            ⬤
          </span>
        </Avatar>
        {
          props.dictionary[
            SOCIAL_TAGS.find(a => a.value === props.panic && a.type === 'sentiment').label
          ]
        }
      </Chip>
    </div>
  );
};

class TweetCard extends Component {
  state = { hover: false };

  addDefaultSrc(ev) {
    ev.target.src = '';
  }

  onHover = e => {
    this.setState({ hover: true });
  };
  onMouseLeave = () => {
    this.setState({ hover: false });
  };

  onClick = async e => {
    if (this.props.selectedTweet !== this.props.tweet.tweetId) {
      await this.props.clickTweet(this.props.tweet.tweetId, this.props.geometry);
      if (typeof this.props.clickTweetCard === 'function')
        this.props.clickTweetCard(this.props.tweet.tweetId);
    } else {
      this.props.deleteClickTweet();
      if (typeof this.props.noClickTweetCard === 'function') this.props.noClickTweetCard();
    }
  };
  tagParser(tag) {
    const parsed_tag = tag.replace('_', ' ');
    return parsed_tag.charAt(0).toUpperCase() + parsed_tag.slice(1);
  }
  _setCardRef = e => {
    this._card = e;
  };

  _validateTweet = info => {
    this.props
      .updateTweetFeedback(this.props.tweet.tweetId.toString(), -1, this.props.tweet, {
        informativeNewValue: info
      })
      .then(a => {
        this.props.tweetFeedBackSuccess({}, this.props.tweet.tweetId);
      })
      .catch(
        // Registrar la razón del rechazo
        function(reason) {
          console.log('Manejar promesa rechazada (' + reason + ') aquí.');
        }
      );
  };

  _setModalRef = e => (this._modal = e);

  render() {
    const tags =
      this.props.feedBack && this.props.feedBack.classes
        ? this.props.feedBack.classes
        : this.props.tweet.classes;
    const entities =
      this.props.feedBack && this.props.feedBack.entities
        ? this.props.feedBack.entities
        : this.props.tweet.entities;
    const info =
      this.props.feedBack && this.props.feedBack.informative
        ? this.props.feedBack.informative
        : this.props.tweet.informative;
    const panic =
      this.props.feedBack && this.props.feedBack.sentiment
        ? this.props.feedBack.sentiment
        : this.props.tweet.sentiment;

    return (
      <div ref={this._setCardRef}>
        <Card
          style={{
            ...{ position: 'relative', margin: '0 15px 15px', cursor: 'pointer' },
            ...(this.props.selectedTweet === this.props.tweet.tweetId &&
              !this.state.edit_mode && {
                backgroundColor: '#666666',
                position: 'relative',
                margin: '0 15px 15px',
                cursor: 'pointer'
              }),
            ...(this.state.hover && {
              backgroundColor: '#464646',
              position: 'relative',
              margin: '0 15px 15px',
              cursor: 'pointer'
            })
          }}
          onMouseEnter={this.onHover}
          onMouseLeave={this.onMouseLeave}
          onClick={this.onClick}
        >
          {this.props.tweet.originalTweet.retweet === true && (
            <div>
              <div style={STYLES.triangleTopleft} />
              <div style={{ top: 0, position: 'absolute', margin: '4px', zIndex: '99' }}>
                <FontIcon className="material-icons social-share-icon">repeat</FontIcon>
              </div>
            </div>
          )}
          <IconButton style={{ position: 'absolute', right: 0, zIndex: '99' }} onClick={() => {}}>
            <FontIcon className="material-icons social-share-icon">share</FontIcon>
          </IconButton>
          <IconButton
            style={{ position: 'absolute', right: '30px', zIndex: '99' }}
            onClick={() => {
              window.open(`https://twitter.com/statuses/${this.props.tweet.tweetId}`, '_blank');
            }}
          >
            <TwitterIcon width="16px" height="16px" color="#FFFFFF" />
          </IconButton>
          <CardHeader
            style={{ padding: '10px 10px 10px' }}
            title={this.props.tweet.author}
            subtitle={
              <span>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={`https://twitter.com/${this.props.tweet.originalTweet.user.screenName}`}
                  style={{ color: iReactTheme.palette.primary1Color }}
                >
                  @{this.props.tweet.originalTweet.user.screenName}
                </a>
                · {moment(this.props.tweet.timestamp).format('L LT')}
              </span>
            }
            avatar={
              <Avatar
                src={this.props.tweet.originalTweet.user.profileImageURLHttps}
                onError={this.addDefaultSrc}
              />
            }
          />
          <CardText style={{ padding: '0 15px 0' }}>
            <Linkify
              properties={{ target: '_blank', style: { color: iReactTheme.palette.primary1Color } }}
            >
              {this.props.tweet.content}
            </Linkify>
            {this.props.tweet.originalTweet.place ? (
              <div style={{ color: 'rgba(255, 255, 255, 0.54)', marginTop: '10px' }}>
                {this.props.tweet.originalTweet.place.name},{' '}
                {this.props.tweet.originalTweet.place.countryCode}
              </div>
            ) : this.props.tweet.entities.length > 0 ? (
              <div style={{ color: 'rgba(255, 255, 255, 0.54)', marginTop: '10px' }}>
                {this.props.tweet.entities[0].name}, {this.props.tweet.entities[0].country}
              </div>
            ) : null}
          </CardText>
          {this.props.tweet.originalTweet.photo ? (
            <CardMedia>
              <img src={this.props.tweet.originalTweet.photo.mediaURLHttps} alt="" />
            </CardMedia>
          ) : null}
          <hr />
          <div>
            <Tags
              edit_mode={this.state.edit_mode}
              tags={tags}
              entities={entities}
              info={info}
              tagParser={this.tagParser}
              panic={panic}
              dictionary={this.props.dictionary}
              deleteChip={this.handleRequestDelete}
            />
            {this.props.feedBack || this.props.tweet.tweetOrigin === 'professional' ? (
              <div
                style={{
                  position: 'absolute',
                  right: 0,
                  bottom: 0,
                  padding: '28px 24px 24px 24px'
                }}
              >
                <FontIcon
                  className="material-icons"
                  color="green"
                  style={{ background: 'white', borderRadius: '100%' }}
                >
                  check_circle
                </FontIcon>
              </div>
            ) : (
              <div style={{ position: 'absolute', right: 0, bottom: 0 }}>
                <IconButton onClick={() => {}}>
                  <FontIcon
                    className="material-icons social-share-icon"
                    onClick={() => {
                      this.props.pushModalMessage(
                        this.props.dictionary._edit,
                        <EditTag
                          tweetId={this.props.tweet.tweetId}
                          tweet={this.props.tweet}
                          tags={tags}
                          info={info}
                          panic={panic}
                          entities={entities}
                          tweetContent={this.props.tweet.content}
                          onRef={this._setModalRef}
                          dictionary={this.props.dictionary}
                          tweetFeedBackSuccess={this.props.tweetFeedBackSuccess}
                          updateTweetFeedback={this.props.updateTweetFeedback}
                        />,
                        {
                          _cancel: () => {},
                          _save: () => {
                            this._modal.saveChanges();
                          }
                        }
                      );
                    }}
                  >
                    mode_edit
                  </FontIcon>
                </IconButton>
                <IconButton
                  onClick={() => {
                    this._validateTweet(info);
                  }}
                >
                  <FontIcon className="material-icons social-share-icon">check</FontIcon>
                </IconButton>
              </div>
            )}
          </div>
        </Card>
      </div>
    );
  }
}

export default enhance(withDictionary(TweetCard));
