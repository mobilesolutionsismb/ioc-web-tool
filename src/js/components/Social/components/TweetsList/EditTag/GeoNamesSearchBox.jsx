import React, { Component } from 'react';
import { compose } from 'redux';

import { AutoComplete, FontIcon, IconButton } from 'material-ui';
import { withDictionary } from 'ioc-localization';
import { withSocialFilters } from 'js/modules/socialFilters';

import axios from 'axios';

const enhance = compose(
  withDictionary,
  withSocialFilters
);

const dataSourceConfig = {
  text: 'label',
  value: 'geonameId'
};

class GeoNamesSearchBox extends Component {
  state = {
    dataSource: [],
    searchText: '',
    newLocation: {},
    updating: false
  };
  _selectLocation = (location, index) => {
    this.setState({
      newLocation: this.state.dataSource[index === -1 ? 0 : index]
    });
    this.setState({
      searchText: ''
    });
    this.props.addLocation(this.state.dataSource[index === -1 ? 0 : index]);
  };

  handleUpdateInput = async value => {
    this.setState({
      searchText: value
    });
    if (!this.state.updating) {
      this.setState({ updating: true });
      setTimeout(() => {
        const request = axios({
          method: 'get',
          baseURL: 'https://secure.geonames.net',
          url: `/searchJSON?q=${this.state.searchText}&maxRows=3&lang=${
            this.props.filters.language
          }&username=celi&token=ireact`,
          responseType: 'json',
          headers: {
            common: {
              Accept: 'application/json',
              'Accept-Language': 'en'
            }
          }
        });
        request.then(
          response => {
            this.setState({ updating: false });
            this.setState({
              dataSource: response.data.geonames.map(item => {
                return {
                  label: item.name,
                  id: item.geonameId
                };
              })
            });
          },
          err => {
            this.state.setState({ updating: false });
            console.error('Cannot load geonames', err);
          }
        );
      }, 500);
    }
  };

  _addGeoName(location) {
    this.refs['amountComp'].focus();
  }

  render() {
    return (
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <AutoComplete
          ref="amountComp"
          hintText="Type a location"
          searchText={this.state.searchText}
          dataSource={this.state.dataSource}
          onUpdateInput={this.handleUpdateInput}
          dataSourceConfig={dataSourceConfig}
          filter={AutoComplete.noFilter}
          onNewRequest={this._selectLocation}
        />

        <IconButton
          style={{
            position: 'absolute',
            right: 0,
            top: 10,
            padding: 0,
            width: 'auto',
            height: 'auto'
          }}
          onClick={() => {
            this.state.dataSource.length > 0
              ? this._selectLocation(null, 0)
              : this._addGeoName(this.state.newLocation);
          }}
        >
          <FontIcon color={'#777777'} className="material-icons">
            add_circle_outline
          </FontIcon>
        </IconButton>
      </div>
    );
  }
}

export default enhance(GeoNamesSearchBox);
