import React, { Component } from 'react';
import {
  Chip,
  Avatar,
  FontIcon,
  IconButton,
  IconMenu,
  MenuItem,
  Card,
  CardHeader,
  CardText
} from 'material-ui';
import {
  SOCIAL_HAZARDS,
  HAZADR_SOCIAL_ICONS,
  HAZARD_LABELS,
  SOCIAL_INFOTYPE,
  SOCIAL_INFOTYPE_LABELS,
  SOCIAL_TAGS
} from '../../socialTagsConfig.js';
// import GeoNamesSearchBox from './GeoNamesSearchBox';

const hazards = SOCIAL_HAZARDS;
const infotype = SOCIAL_INFOTYPE;

const CustomChip = props => (
  <Chip
    style={{
      ...{ margin: '10px 5px 10px 0' },
      ...(props.active ? {} : { border: '1.5px solid #656565' })
    }}
    backgroundColor={props.active ? '' : 'rgb(54,54,54)'}
    labelStyle={{
      fontSize: '12px',
      paddingLeft: '10px',
      paddingRight: '10px',
      lineHeight: '22px'
    }}
    deleteIconStyle={{
      ...(props.active ? { height: '22px', width: '22px' } : { height: '0', width: '0' }),
      ...{ margin: '0' }
    }}
    onRequestDelete={() => {
      return props.active ? props.handleRequestDeleteTag() : false;
    }}
    onClick={() => {
      return !props.active ? props.handleChangeTag() : null;
    }}
  >
    <Avatar
      size={22}
      style={{ height: '22px', width: '25px' }}
      backgroundColor={props.active ? '' : 'rgb(54,54,54)'}
    >
      <span
        style={
          props.active
            ? { color: props.tag.color, lineHeight: '22px' }
            : { color: 'gray', lineHeight: '22px' }
        }
      >
        ⬤
      </span>
    </Avatar>
    {props.dictionary[props.tag.label]}
  </Chip>
);

class EditTags extends Component {
  state = {
    fixTags: [],
    tags: [],
    locations: [],
    panic: 'none',
    informative: false,
    newLocation: ''
  };

  componentWillUnmount = () => {
    this.props.onRef(null);
  };

  handleRequestDelete = key => {
    let nextValues = this.state.tags;
    const chipToDelete = nextValues.indexOf(key);
    nextValues.splice(chipToDelete, 1);
    this.setState({
      tags: nextValues
    });
  };
  handleRequestDeleteLocation = key => {
    let nextValues = this.state.locations;
    const chipToDelete = nextValues.indexOf(key);
    nextValues.splice(chipToDelete, 1);
    this.setState({
      locations: nextValues
    });
  };

  handleRequestDeleteTag = (tags, type) => {
    let actual = tags.find(tag => tag.type === type);
    let future = SOCIAL_TAGS.find(tag => tag.value !== actual.value && tag.type === type);
    this._changeToOppositeTag(future);
  };

  handleChangeMultiple = (event, value) => {
    this.setState({
      tags: value
    });
  };

  handleChangeTag = (selected, type) => {
    //Deleting duplicate items
    this._changeToOppositeTag(selected);
  };

  _changeToOppositeTag(newTag) {
    var newArray = this.state.fixTags.filter(tag => tag.type !== newTag.type);
    newArray.push(newTag);
    this.setState({ fixTags: newArray });
  }

  saveChanges = () => {
    let properties = {
      classes: this.state.tags,
      entities: this.state.locations
    };

    this.state.fixTags.forEach(tag => {
      properties[tag.type] = tag.value;
    });

    const originalEntities = this.props.entities.map(item => this._parseEntitie(item));
    const newEntities = properties.entities.map(item => this._parseEntitie(item));

    let feedBack = {
      AddedClasses: properties.classes.filter(function(n) {
        return !this.has(n);
      }, new Set(this.props.tags)),
      AddedEntities: newEntities.filter(function(n) {
        return !this.has(n.id);
      }, new Set(originalEntities.map(item => this._arrayIdEntities(item)))),
      InformativeNewValue: properties.informative,
      SentimentNewValue: properties.sentiment,
      RemovedClasses: properties.classes.filter(function(n) {
        return !this.has(n);
      }, new Set(this.state.tags)),
      RemovedEntities: originalEntities.filter(function(n) {
        return !this.has(n.id);
      }, new Set(newEntities.map(item => this._arrayIdEntities(item))))
    };

    if (feedBack.AddedClasses.length <= 0) {
      delete feedBack.AddedClasses;
    }

    if (feedBack.AddedEntities.length <= 0) {
      delete feedBack.AddedEntities;
    }
    if (feedBack.RemovedClasses.length <= 0) {
      delete feedBack.RemovedClasses;
    }
    if (feedBack.RemovedEntities.length <= 0) {
      delete feedBack.RemovedEntities;
    }
    if (feedBack.InformativeNewValue === this.props.info) {
      delete feedBack.InformativeNewValue;
    }
    if (feedBack.SentimentNewValue === this.props.panic) {
      delete feedBack.SentimentNewValue;
    }

    this.props
      .updateTweetFeedback(this.props.tweetId.toString(), -1, this.props.tweet, feedBack)
      .then(a => {
        this.props.tweetFeedBackSuccess(properties, this.props.tweetId);
      })
      .catch(
        // Registrar la razón del rechazo
        function(reason) {
          console.log('Manejar promesa rechazada (' + reason + ') aquí.');
        }
      );
  };

  _parseEntitie = entitie => {
    return {
      label: entitie.name ? entitie.name : entitie.label,
      iri: entitie.iri,
      id: entitie.iri ? entitie.iri.replace('http://sws.geonames.org/', '') : entitie.id.toString()
    };
  };

  _arrayIdEntities = entitie => {
    return entitie.id;
  };
  _addLocation = location => {
    let news = this.state.locations;
    news.push(location);
    this.setState({ locations: news });
  };

  componentDidMount() {
    this.props.onRef(this);
    this.setState({
      tags: this.props.tags,
      fixTags: [
        { value: this.props.panic, type: 'sentiment' },
        { value: this.props.info, type: 'informative' }
      ],
      locations: JSON.parse(JSON.stringify(this.props.entities))
    });
  }
  render() {
    return (
      <div
        style={{
          display: 'inline-grid',
          width: '100%',
          padding: '10px 0',
          paddingTop: '14px',
          boxSizing: 'border-box'
        }}
      >
        {this.state.fixTags.length > 0 && (
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between'
            }}
          >
            <Card
              style={{ marginBottom: '20px', backgroundColor: 'rgb(54,54,54)', width: '49%' }}
              containerStyle={{ padding: 0 }}
            >
              <CardHeader style={{ paddingBottom: 0 }} title="Informativeness" />
              <CardText
                style={{
                  display: 'inline-flex',
                  flexFlow: 'row wrap',
                  alignItems: 'center',
                  paddingTop: 0
                }}
              >
                {SOCIAL_TAGS.filter(tag => tag.type === 'informative').map((tag, index) => {
                  let active =
                    this.state.fixTags.find(tag => tag.type === 'informative').value === tag.value;
                  return (
                    <CustomChip
                      key={index}
                      state={this.state}
                      active={active}
                      tag={tag}
                      handleRequestDeleteTag={() =>
                        this.handleRequestDeleteTag(this.state.fixTags, 'informative')
                      }
                      handleChangeTag={() => this.handleChangeTag(tag, 'informative')}
                      dictionary={this.props.dictionary}
                    />
                  );
                })}
              </CardText>
            </Card>
            <Card
              style={{ marginBottom: '20px', backgroundColor: 'rgb(54,54,54)', width: '49%' }}
              containerStyle={{ padding: 0 }}
            >
              <CardHeader style={{ paddingBottom: 0 }} title="Panic" />
              <CardText
                style={{
                  display: 'inline-flex',
                  flexFlow: 'row wrap',
                  alignItems: 'center',
                  paddingTop: 0
                }}
              >
                {SOCIAL_TAGS.filter(tag => tag.type === 'sentiment').map((tag, index) => {
                  let active =
                    this.state.fixTags.find(tag => tag.type === 'sentiment').value === tag.value;
                  return (
                    <CustomChip
                      key={index}
                      state={this.state}
                      active={active}
                      tag={tag}
                      handleRequestDeleteTag={() =>
                        this.handleRequestDeleteTag(this.state.fixTags, 'sentiment')
                      }
                      handleChangeTag={() => this.handleChangeTag(tag, 'sentiment')}
                      dictionary={this.props.dictionary}
                    />
                  );
                })}
              </CardText>
            </Card>
          </div>
        )}
        <Card
          style={{ marginBottom: '20px', backgroundColor: 'rgb(54,54,54)' }}
          containerStyle={{ padding: 0 }}
        >
          <CardHeader style={{ paddingBottom: 0 }} title="Named locations" />
          <CardText
            style={{
              display: 'inline-flex',
              flexFlow: 'row wrap',
              alignItems: 'center',
              paddingTop: 0
            }}
          >
            {this.state.locations.length > 0
              ? this.state.locations.map((item, i) => (
                  <Chip
                    style={{ margin: '15px 10px 5px 0' }}
                    labelStyle={{
                      fontSize: '12px',
                      paddingLeft: '10px',
                      paddingRight: '10px',
                      lineHeight: '22px'
                    }}
                    key={i}
                    onRequestDelete={() => {
                      this.handleRequestDeleteLocation(item);
                    }}
                    deleteIconStyle={{ margin: '0', height: '22px', width: '22px' }}
                  >
                    <Avatar size={22} style={{ height: '22px', width: '25px' }}>
                      <FontIcon
                        className="material-icons"
                        style={{ lineHeight: '22px', fontSize: '20px' }}
                      >
                        location_city
                      </FontIcon>
                    </Avatar>
                    {item.name ? item.name : item.label}
                  </Chip>
                ))
              : null}

            {/* <GeoNamesSearchBox addLocation={this._addLocation} /> */}
          </CardText>
        </Card>
        <Card
          style={{ marginBottom: '20px', backgroundColor: 'rgb(54,54,54)' }}
          containerStyle={{ padding: 0 }}
        >
          <CardHeader style={{ paddingBottom: 0 }} title="Hazards" />
          <CardText
            style={{
              display: 'inline-flex',
              flexFlow: 'row wrap',
              alignItems: 'center',
              paddingTop: 0
            }}
          >
            {this.state.tags.length > 0 ? (
              this.state.tags.map((item, i) =>
                HAZADR_SOCIAL_ICONS[item] ? (
                  <Chip
                    style={{ margin: '10px 5px 10px 0' }}
                    labelStyle={{
                      fontSize: '12px',
                      paddingLeft: '10px',
                      paddingRight: '10px',
                      lineHeight: '22px'
                    }}
                    key={i}
                    onRequestDelete={() => {
                      this.handleRequestDelete(item);
                    }}
                    deleteIconStyle={{ margin: '0', height: '22px', width: '22px' }}
                  >
                    <Avatar size={22} style={{ height: '22px', width: '25px' }}>
                      {HAZADR_SOCIAL_ICONS[item]}
                    </Avatar>
                    {this.props.dictionary[HAZARD_LABELS[item]]}
                  </Chip>
                ) : null
              )
            ) : (
              <span
                style={{
                  color: '#656565'
                }}
              >
                Add a label
              </span>
            )}
            <IconMenu
              multiple={true}
              onChange={this.handleChangeMultiple}
              value={this.state.tags}
              clickCloseDelay={0}
              iconButtonElement={
                <IconButton onClick={() => {}}>
                  <FontIcon color={'#777777'} className="material-icons">
                    add_circle_outline
                  </FontIcon>
                </IconButton>
              }
            >
              {hazards.map(hazard => (
                <MenuItem
                  key={hazard}
                  insetChildren={true}
                  checked={this.state.tags.indexOf(hazard) > -1}
                  value={hazard}
                  primaryText={this.props.dictionary[HAZARD_LABELS[hazard]]}
                />
              ))}
            </IconMenu>
          </CardText>
        </Card>
        <Card style={{ backgroundColor: 'rgb(54,54,54)' }} containerStyle={{ padding: 0 }}>
          <CardHeader style={{ paddingBottom: 0 }} title="Info types" />
          <CardText
            style={{
              display: 'inline-flex',
              flexFlow: 'row wrap',
              alignItems: 'center',
              paddingTop: 0
            }}
          >
            {this.state.tags.some(a => !HAZADR_SOCIAL_ICONS[a]) ? (
              this.state.tags.map((item, i) =>
                !HAZADR_SOCIAL_ICONS[item] ? (
                  <Chip
                    style={{ margin: '10px 5px 10px 0' }}
                    labelStyle={{
                      fontSize: '12px',
                      paddingLeft: '10px',
                      paddingRight: '10px',
                      lineHeight: '22px'
                    }}
                    key={i}
                    onRequestDelete={() => {
                      this.handleRequestDelete(item);
                    }}
                    deleteIconStyle={{ margin: '0', height: '22px', width: '22px' }}
                  >
                    <Avatar size={22} style={{ height: '22px', width: '25px' }}>
                      <FontIcon className="material-icons" style={{ lineHeight: '22px' }}>
                        info_outline
                      </FontIcon>
                    </Avatar>
                    {this.props.dictionary[SOCIAL_INFOTYPE_LABELS[item]]}
                  </Chip>
                ) : null
              )
            ) : (
              <span
                style={{
                  color: '#656565'
                }}
              >
                Add a label
              </span>
            )}
            <IconMenu
              multiple={true}
              onChange={this.handleChangeMultiple}
              value={this.state.tags}
              clickCloseDelay={0}
              iconButtonElement={
                <IconButton onClick={() => {}}>
                  <FontIcon color={'#777777'} className="material-icons">
                    add_circle_outline
                  </FontIcon>
                </IconButton>
              }
            >
              {infotype.map(type => (
                <MenuItem
                  key={type}
                  insetChildren={true}
                  checked={this.state.tags.indexOf(type) > -1}
                  value={type}
                  primaryText={this.props.dictionary[SOCIAL_INFOTYPE_LABELS[type]]}
                />
              ))}
            </IconMenu>
          </CardText>
        </Card>
      </div>
    );
  }
}
export default EditTags;
