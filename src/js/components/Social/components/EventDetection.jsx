import React, { Component } from 'react';
import { compose } from 'redux';
import moment from 'moment';

import muiThemeable from 'material-ui/styles/muiThemeable';
import { HAZARD_TYPES_LETTERS } from 'js/components/ReportsCommons';
import {
  HAZARD_TYPES_FOR_EVENTS_EQUIVALENCY,
  LANGUAGES,
  HAZARD_TYPES_FOR_ICON_EQUIVALENCY
} from './socialTagsConfig';
import { Avatar, FlatButton, IconButton, FontIcon, RaisedButton } from 'material-ui';
import { withEventDetection } from 'js/modules/socialApi';
import { withTopBarDate } from 'js/modules/topBarDate';
import { withDictionary } from 'ioc-localization';
import { withSocialFilters } from 'js/modules/socialFilters';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { withRouter } from 'react-router';
import { withEventCreate } from 'js/modules/eventCreate';
import { iReactTheme } from 'js/startup/iReactTheme';
import { withTweets } from 'js/modules/socialApi';
import { withAPISettings } from 'ioc-api-interface';

import { getLocalDate } from 'js/utils/localizeDate';
import './style.scss';

const enhance = compose(
  withTopBarDate,
  withEventDetection,
  withDictionary,
  withSocialFilters,
  withRouter,
  withEventCreate,
  withTweets,
  withAPISettings
);

class EventDetection extends Component {
  componentDidMount = async () => {
    if (!this.props.filters.language) {
      if (LANGUAGES[this.props.locale])
        await this.props.addSocialFilter('language', this.props.locale);
      else await this.props.addSocialFilter('language', 'en');
    }
    if (!this.props.startDate) {
      await this.props.selectDate(getLocalDate(null, -7, 'days').toDate(), getLocalDate().toDate());
    }
    await this.props.getEventDetection(
      this.props.filters,
      this.props.startDate,
      this.props.endDate
    );
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.eventDetection[this.props.eventIndex] !==
      prevProps.eventDetection[prevProps.eventIndex]
    ) {
      this.props.drawOnMap(this.props.eventDetection[this.props.eventIndex]);
    }
  }
  applyFilters = async (emergency, startDate, endDate) => {
    let filters, end, start;
    if (this.props.eventFilter) {
      await this.props.deactivateEventFiltering();
      await this.props.applyBoundingBoxFilter();
      filters = this.props.filters;
      end = this.props.liveTW.dateEnd;
      start = null;
    } else {
      await this.props.activateEventFiltering({ emergency, startDate, endDate });
      this._goToBoundingbox();
      this._searchOnEventBoundingBox();
      filters = { emergency, retweet: false, language: this.props.filters.language };
      end = endDate;
      start = startDate;
    }
    await this.props.cleanPagination();

    this.props.getTweetsGeolocatedStarted();
    this.props.getTweets(
      filters,
      end,
      start,
      this.props.paginationGeolocated,
      300,
      true,
      this.props.loadingStart,
      this.props.loadingStop
    );

    this.props.getTweetsStarted();
    this.props.getTweets(
      filters,
      end,
      start,
      this.props.pagination,
      50,
      false,
      this.props.loadingStart,
      this.props.loadingStop
    );
    this.props.getStatistics(filters, start, end);
  };

  /**
   * @param {Object} event
   * @param {boolean} option
   */
  createEvent = async (event, option) => {
    const x1 = event.boundingBox.southWest.longitude; //the lowest x coordinate
    const y1 = event.boundingBox.southWest.latitude; //the lowest y coordinate
    const x2 = event.boundingBox.northEast.longitude; //the highest x coordinate
    const y2 = event.boundingBox.northEast.latitude; //the highest y coordinate
    let center = {};
    center.x = x1 + (x2 - x1) / 2;
    center.y = y1 + (y2 - y1) / 2;

    const eventToUpdate = {
      msgId: event.msgId,
      language: event.language,
      emergencyType: event.eventType
    };
    if (option) {
      await this.props.updateStatusOfEvent(eventToUpdate, option);
      eventToUpdate.polygon = {
        coordinates: [[[x1, y1], [x1, y2], [x2, y2], [x2, y1], [x1, y1]]],
        id: event.msgId,
        type: 'Polygon'
      };
      eventToUpdate.point = {
        coordinates: [center.x, center.y],
        id: event.msgId,
        type: 'Point'
      };

      this.props.setTwitterSourceEvent(eventToUpdate);
      this.props.setEventCreateTitle(
        `Twitter: ${
          this.props.dictionary[HAZARD_TYPES_FOR_EVENTS_EQUIVALENCY[event.eventType]]
        } detection`
      );
      this.props.setEventCreateHazard({
        value: HAZARD_TYPES_FOR_EVENTS_EQUIVALENCY[event.eventType],
        label: this.props.dictionary[HAZARD_TYPES_FOR_EVENTS_EQUIVALENCY[event.eventType]]
      });

      this.props.setEventCreateStartDate(new Date(event.startDate));
      this.props.setEventCreateStartTime(new Date(event.startDate));
      this.props.setEventCreateEndDate(new Date(event.endDate));
      this.props.setEventCreateEndTime(new Date(event.endDate));

      this.props.history.push(`/home/${drawerNames.event}?${drawerNames.eventCreate}`, {
        from: this.props.location.pathname
      });
    } else {
      this.props.updateStatusOfEvent(eventToUpdate, option);
    }
  };

  _goToBoundingbox() {
    this.props.goToEventOnMap(this.props.eventDetection[this.props.eventIndex]);
  }

  _searchOnEventBoundingBox() {
    const event = this.props.eventDetection[this.props.eventIndex];
    const x1 = event.boundingBox.southWest.longitude; //the lowest x coordinate
    const y1 = event.boundingBox.southWest.latitude; //the lowest y coordinate
    const x2 = event.boundingBox.northEast.longitude; //the highest x coordinate
    const y2 = event.boundingBox.northEast.latitude; //the highest y coordinate
    const bbox = [[x2, y1], [x1, y1], [x1, y2], [x2, y2], [x2, y1]];
    this.props.setBoundingBox(bbox);
  }

  render() {
    const { dictionary, style, eventDetection, eventIndex } = this.props;
    const eventSelected = eventDetection[eventIndex];
    return (
      <div style={{ ...style, backgroundColor: iReactTheme.palette.appBarColor }}>
        {eventDetection && eventDetection.length > 0 ? (
          <div
            className="first"
            style={{
              zIndex: '10',
              backgroundColor: iReactTheme.palette.accent1Color,
              padding: '10px 0px',
              boxSizing: 'border-box'
            }}
            ref={c => (this.card = c)}
          >
            <div
              style={{
                display: 'flex',
                width: '9%',
                borderRight: '1px solid white',
                height: '100%',
                alignItems: 'center'
              }}
            >
              <IconButton
                onClick={() => {
                  this._goToBoundingbox();
                }}
              >
                <Avatar
                  className="ireact-pinpoint-icons ireact-pinpoint-icons-social"
                  style={{ height: 'auto', width: 'auto', fontSize: '28px', margin: 'auto' }}
                  backgroundColor={'transparent'}
                  color={'white'}
                >
                  {HAZARD_TYPES_LETTERS[HAZARD_TYPES_FOR_ICON_EQUIVALENCY[eventSelected.eventType]]}
                </Avatar>
              </IconButton>
            </div>
            <div
              style={{
                textAlign: 'initial',
                width: '15%',
                borderRight: '1px solid white',
                padding: '0px 10px',
                height: '100%',
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <div style={{ margin: 'auto', width: 'min-content' }}>
                {dictionary[HAZARD_TYPES_FOR_EVENTS_EQUIVALENCY[eventSelected.eventType]]}
              </div>
            </div>
            <div
              style={{
                display: 'flex',
                width: '46%',
                borderRight: '1px solid white',
                padding: '0px 5px',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center'
                }}
              >
                <FontIcon style={{ margin: '0 0 0 5px' }} className="material-icons">
                  access_time
                </FontIcon>
                <div style={{ display: 'grid', textAlign: 'left', margin: '0 5px' }}>
                  <span style={{ fontSize: '10px' }}>From</span>
                  {moment(eventSelected.startDate).format('L')}
                  <span style={{ fontSize: '12px' }}>
                    {moment(eventSelected.startDate).format('HH:mm')}
                  </span>
                </div>
                <div style={{ display: 'grid', textAlign: 'left', margin: '0 5px' }}>
                  <span style={{ fontSize: '10px' }}>To</span>
                  {moment(eventSelected.endDate).format('L')}
                  <span style={{ fontSize: '12px' }}>
                    {moment(eventSelected.endDate).format('HH:mm')}
                  </span>
                </div>
              </div>
              <RaisedButton
                label={this.props.eventFilter ? 'Reset' : 'Apply'}
                backgroundColor="white"
                labelColor={iReactTheme.palette.accent1Color}
                onClick={this.applyFilters.bind(
                  this,
                  [{ value: eventSelected.eventType }],
                  eventSelected.startDate,
                  eventSelected.endDate
                )}
                style={{ margin: '5px', minWidth: 0 }}
              />
            </div>
            <div
              style={{
                width: '20%',
                borderRight: '1px solid white',
                padding: '0px 10px',
                height: '100%'
              }}
            >
              <div style={{ textAlign: 'left', fontSize: '12px' }}>Confirm?</div>
              <div style={{ display: 'flex' }}>
                <FlatButton
                  onClick={() => this.createEvent(eventSelected, true)}
                  label={dictionary._yes}
                  style={{
                    minWidth: 0,
                    border: 'solid 1px white',
                    borderRadius: 0,
                    height: '30px',
                    lineHeight: '30px'
                  }}
                />
                <FlatButton
                  onClick={() => this.createEvent(eventSelected, false)}
                  label={dictionary._no}
                  style={{
                    minWidth: 0,
                    border: 'solid 1px white',
                    borderRadius: 0,
                    height: '30px',
                    lineHeight: '30px',
                    marginLeft: '5px'
                  }}
                />
              </div>
            </div>
            <div
              style={{
                display: 'flex',
                width: '10%',
                padding: '0px 5px',
                height: '100%',
                alignItems: 'center'
              }}
            >
              <IconButton style={{ padding: 0 }}>
                <FontIcon
                  className="material-icons"
                  onClick={() => {
                    this.props.previousEventDetection();
                    this.props.deactivateEventFiltering();
                  }}
                >
                  keyboard_arrow_left
                </FontIcon>
              </IconButton>
              <IconButton style={{ padding: 0 }}>
                <FontIcon
                  className="material-icons"
                  onClick={() => {
                    this.props.nextEventDetection();
                    this.props.deactivateEventFiltering();
                  }}
                >
                  keyboard_arrow_right
                </FontIcon>
              </IconButton>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default enhance(muiThemeable()(EventDetection));
