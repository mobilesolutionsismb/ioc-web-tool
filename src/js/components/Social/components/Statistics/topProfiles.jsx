import React, { Component } from 'react';
// import muiThemeable from 'material-ui/styles/muiThemeable';
import { iReactTheme } from 'js/startup/iReactTheme';
import {
  IconButton,
  FontIcon,
  Toolbar,
  ToolbarGroup,
  ToolbarTitle,
  Paper,
  Avatar,
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui';
import '../style.scss';

class Topprofiles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sort: null,
      order: null,
      tableData: null
    };
  }
  changeSort = (sort, order) => {
    if (this.state.sort !== sort) {
      this.setState({
        sort: sort,
        order: 'asc'
      });
    } else {
      let newOrder = this.state.order === 'asc' ? 'desc' : 'asc';
      this.setState({
        sort: sort,
        order: newOrder
      });
    }
  };
  componentDidUpdate(prevProps, prevState) {
    if (prevState.sort !== this.state.sort || prevState.order !== this.state.order) {
      this.props.sort(this.props.data.sort(this._compare));
    }
    if (prevProps.data === undefined && this.props.data !== undefined) {
      this.setState({
        tableData: this.props.data
      });
    }
  }
  _compare = (a, b) => {
    let res = 0;
    switch (this.state.order) {
      case 'asc':
        if (this.state.sort === 'numTweets') {
          if (a[this.state.sort] < b[this.state.sort]) res = -1;
          if (a[this.state.sort] > b[this.state.sort]) res = 1;
        } else {
          if (a.userProfile[this.state.sort] < b.userProfile[this.state.sort]) res = -1;
          if (a.userProfile[this.state.sort] > b.userProfile[this.state.sort]) res = 1;
        }

        res = 0;
        break;
      case 'desc':
        if (this.state.sort === 'numTweets') {
          if (a[this.state.sort] > b[this.state.sort]) res = -1;
          if (a[this.state.sort] < b[this.state.sort]) res = 1;
        } else {
          if (a.userProfile[this.state.sort] > b.userProfile[this.state.sort]) res = -1;
          if (a.userProfile[this.state.sort] < b.userProfile[this.state.sort]) res = 1;
        }

        res = 0;
        break;
      default:
        break;
    }
    return res;
  };

  render() {
    const colors = {
      background: '#242424',
      content: '#303030',
      header: '#3c3c3c'
    };
    const profilesHeader = (
      <Toolbar className="toolbar">
        <ToolbarGroup>
          <ToolbarTitle style={{ color: 'white' }} text={this.props.dictionary._top_profiles} />
          <div style={{ textAlign: 'right' }}>
            <IconButton>
              <FontIcon className="material-icons">more_vert</FontIcon>
            </IconButton>
          </div>
        </ToolbarGroup>
      </Toolbar>
    );

    return this.props.data !== null ? (
      <div className={'rightDiv'}>
        <Paper>
          <div children={profilesHeader} />
          <div className="cardContent">
            <Table className={'cardTopProf'} height={'242px'} fixedHeader={true}>
              <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
                style={{ color: 'blue !important' }}
              >
                <TableRow>
                  <TableHeaderColumn
                    style={{
                      boxSizing: 'border-box',
                      paddingLeft: 0,
                      paddingRight: 0,
                      width: '12%',
                      color: 'white'
                    }}
                  />
                  <TableHeaderColumn
                    style={{
                      boxSizing: 'border-box',
                      paddingLeft: 0,
                      paddingRight: 0,
                      width: '19%',
                      color: 'white'
                    }}
                  />
                  <TableHeaderColumn
                    style={{
                      boxSizing: 'border-box',
                      paddingLeft: 0,
                      paddingRight: 0,
                      width: '13%',
                      color: 'white',
                      alignItems: 'center'
                    }}
                  >
                    <div
                      style={{
                        marginTop: 5,
                        float: 'left',
                        cursor: 'pointer',
                        maxWidth: 'calc(100% - 25px)',
                        textOverflow: 'ellipsis',
                        overflow: 'hidden',
                        whiteSpace: 'nowrap'
                      }}
                      onClick={() => this.changeSort('numTweets', 'asc')}
                      title={this.props.dictionary._posts.toUpperCase()}
                    >
                      {this.props.dictionary._posts.toUpperCase()}
                    </div>
                    <IconButton
                      style={{ width: 16, height: 16, padding: 0 }}
                      iconStyle={{
                        fontSize: '10px !important',
                        color: this.state.sort === 'numTweets' ? 'white' : colors.header
                      }}
                      onClick={() => this.changeSort('numTweets', 'asc')}
                    >
                      <FontIcon className="material-icons">
                        {this.state.sort === 'numTweets'
                          ? this.state.order === 'desc'
                            ? 'keyboard_arrow_up'
                            : 'keyboard_arrow_down'
                          : 'keyboard_arrow_down'}
                      </FontIcon>
                    </IconButton>
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    style={{
                      boxSizing: 'border-box',
                      paddingLeft: 0,
                      paddingRight: 0,
                      width: '13%',
                      color: 'white',
                      alignItems: 'center'
                    }}
                  >
                    <div
                      style={{
                        marginTop: 5,
                        float: 'left',
                        cursor: 'pointer',
                        maxWidth: 'calc(100% - 25px)',
                        textOverflow: 'ellipsis',
                        overflow: 'hidden',
                        whiteSpace: 'nowrap'
                      }}
                      onClick={() => this.changeSort('statusesCount', 'asc')}
                      title="RET"
                    >
                      RET
                    </div>
                    <IconButton
                      style={{ width: 16, height: 16, padding: 0 }}
                      iconStyle={{
                        fontSize: '10px !important',
                        color: this.state.sort === 'statusesCount' ? 'white' : colors.header
                      }}
                      onClick={() => this.changeSort('statusesCount', 'asc')}
                    >
                      <FontIcon className="material-icons">
                        {this.state.sort === 'statusesCount'
                          ? this.state.order === 'desc'
                            ? 'keyboard_arrow_up'
                            : 'keyboard_arrow_down'
                          : 'keyboard_arrow_down'}
                      </FontIcon>
                    </IconButton>
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    style={{
                      boxSizing: 'border-box',
                      paddingLeft: 0,
                      paddingRight: 0,
                      width: '21%',
                      color: 'white',
                      alignItems: 'center'
                    }}
                  >
                    <div
                      style={{
                        marginTop: 5,
                        float: 'left',
                        cursor: 'pointer',
                        maxWidth: 'calc(100% - 25px)',
                        textOverflow: 'ellipsis',
                        overflow: 'hidden',
                        whiteSpace: 'nowrap'
                      }}
                      onClick={() => this.changeSort('friendsCount', 'asc')}
                      title={this.props.dictionary._following.toUpperCase()}
                    >
                      {this.props.dictionary._following.toUpperCase()}
                    </div>
                    <IconButton
                      style={{ width: 16, height: 16, padding: 0 }}
                      iconStyle={{
                        fontSize: '10px !important',
                        color: this.state.sort === 'friendsCount' ? 'white' : colors.header
                      }}
                      onClick={() => this.changeSort('friendsCount', 'asc')}
                    >
                      <FontIcon className="material-icons">
                        {this.state.sort === 'friendsCount'
                          ? this.state.order === 'desc'
                            ? 'keyboard_arrow_up'
                            : 'keyboard_arrow_down'
                          : 'keyboard_arrow_down'}
                      </FontIcon>
                    </IconButton>
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    style={{
                      boxSizing: 'border-box',
                      paddingLeft: 0,
                      paddingRight: 0,
                      width: '22%',
                      color: 'white',
                      alignItems: 'center'
                    }}
                  >
                    <div
                      style={{
                        marginTop: 5,
                        float: 'left',
                        cursor: 'pointer',
                        maxWidth: 'calc(100% - 25px)',
                        textOverflow: 'ellipsis',
                        overflow: 'hidden',
                        whiteSpace: 'nowrap'
                      }}
                      onClick={() => this.changeSort('followersCount', 'asc')}
                      title={this.props.dictionary._followers.toUpperCase()}
                    >
                      {this.props.dictionary._followers.toUpperCase()}
                    </div>
                    <IconButton
                      style={{ width: 16, height: 16, padding: 0 }}
                      iconStyle={{
                        fontSize: '10px !important',
                        color: this.state.sort === 'followersCount' ? 'white' : colors.header
                      }}
                      onClick={() => this.changeSort('followersCount', 'asc')}
                    >
                      <FontIcon className="material-icons">
                        {this.state.sort === 'followersCount'
                          ? this.state.order === 'desc'
                            ? 'keyboard_arrow_up'
                            : 'keyboard_arrow_down'
                          : 'keyboard_arrow_down'}{' '}
                      </FontIcon>
                    </IconButton>
                  </TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                {Array.isArray(this.props.data) &&
                  this.props.data.map((row, index) => {
                    return (
                      <TableRow key={index}>
                        <TableRowColumn
                          style={{
                            boxSizing: 'border-box',
                            paddingLeft: '2%',
                            paddingRight: '2%',
                            width: '12%'
                          }}
                        >
                          <Avatar src={row.userProfile.profileImageURLHttps} />
                        </TableRowColumn>
                        <TableRowColumn
                          style={{
                            boxSizing: 'border-box',
                            width: '18%',
                            paddingLeft: '5px',
                            paddingRight: '5px'
                          }}
                        >
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href={`https://twitter.com/${row.userProfile.screenName}`}
                            style={{ color: iReactTheme.palette.primary1Color }}
                            title={`@${row.userProfile.screenName}`}
                          >
                            @{row.userProfile.screenName}
                          </a>
                        </TableRowColumn>
                        <TableRowColumn
                          style={{
                            boxSizing: 'border-box',
                            paddingLeft: 0,
                            paddingRight: 0,
                            width: '13%'
                          }}
                          title={this.props.shortnumber(row.numTweets, 1)}
                        >
                          {this.props.shortnumber(row.numTweets, 1)}
                        </TableRowColumn>
                        <TableRowColumn
                          style={{ boxSizing: 'border-box', width: '13%', paddingLeft: 0 }}
                          title={this.props.shortnumber(row.userProfile.statusesCount, 0)}
                        >
                          {this.props.shortnumber(row.userProfile.statusesCount, 0)}
                        </TableRowColumn>
                        <TableRowColumn
                          style={{ boxSizing: 'border-box', width: '21%', paddingLeft: 0 }}
                          title={this.props.shortnumber(row.userProfile.friendsCount, 0)}
                        >
                          {this.props.shortnumber(row.userProfile.friendsCount, 0)}
                        </TableRowColumn>
                        <TableRowColumn
                          style={{ boxSizing: 'border-box', width: '22%', paddingLeft: 0 }}
                          title={this.props.shortnumber(row.userProfile.followersCount, 1)}
                        >
                          {this.props.shortnumber(row.userProfile.followersCount, 1)}
                        </TableRowColumn>
                      </TableRow>
                    );
                  }, this)}
              </TableBody>
            </Table>
          </div>
        </Paper>
      </div>
    ) : (
      <div />
    );
  }
}

export default Topprofiles;
