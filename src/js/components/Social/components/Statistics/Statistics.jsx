import React, { Component } from 'react';
import { compose } from 'redux';

import muiThemeable from 'material-ui/styles/muiThemeable';
import { LinearProgress } from 'material-ui';
import { withStatistics } from 'js/modules/socialApi';
import { withEventDetection } from 'js/modules/socialApi';
import { withSocialFilters } from 'js/modules/socialFilters';
import { withTopBarDate } from 'js/modules/topBarDate';
import { withDictionary } from 'ioc-localization';
import { getLocalDate } from 'js/utils/localizeDate';
import { iReactTheme } from 'js/startup/iReactTheme';

import Tagcloud from './tagCloud';
import Topprofiles from './topProfiles';
import Tophashtags from './topHashtags';
import Topgraph from './topGraph';
import InfoTypeGrap from './infoTypeGraph';
import '../style.scss';
import { LANGUAGES } from '../socialTagsConfig';

const enhance = compose(
  withSocialFilters,
  withStatistics,
  withTopBarDate,
  withEventDetection,
  withDictionary
);

class Statistics extends Component {
  componentDidMount = async () => {
    if (!this.props.filters.language) {
      if (LANGUAGES[this.props.locale])
        await this.props.addSocialFilter('language', this.props.locale);
      else await this.props.addSocialFilter('language', 'en');
    }
    if (!this.props.startDate) {
      await this.props.selectDate(getLocalDate(null, -7, 'days').toDate(), getLocalDate().toDate());
    }
    this.props.getStatistics(this.props.filters, this.props.startDate, this.props.endDate);
    this.props.getEventDetection(this.props.filters, this.props.startDate, this.props.endDate);
  };
  shortnumber(n, decimals) {
    let precision = Math.pow(10, decimals || 0);
    let result = n / 1000;
    let number = n;
    if (result >= 1) {
      //unless K
      precision = Math.pow(10, 2 || 0);
      let result2 = result / 1000;
      if (result2 >= 1) {
        //M
        if (precision > 1) {
          number = `${Math.round(result2 * precision) / precision}M`;
        } else {
          number = `${Math.trunc(result2)}M`;
        }
      } else {
        if (precision > 1) {
          number = `${Math.round(result * precision) / precision}K`;
        } else {
          number = `${Math.trunc(result)}K`;
        }
      }
    }
    return number;
  }

  render() {
    const { style, statistics, dictionary } = this.props;
    const colors = {
      background: iReactTheme.palette.appBarColor,
      content: iReactTheme.palette.tabsColor,
      header: iReactTheme.palette.liveModeDisabledColor
    };

    const panic = statistics.panic === undefined ? 0 : Math.round(statistics.panic * 1000) / 10;
    const informativeness =
      statistics.informativeness === undefined
        ? 0
        : Math.round(statistics.informativeness * 1000) / 10;
    return (
      <section style={{ ...style, backgroundColor: colors.background }}>
        {statistics.panic !== undefined ? (
          <div className={'root'}>
            <div className={'second'}>
              <div className={'smallCardTitle'}>{dictionary._volume}</div>
              <div className={'smallCardContainer'}>
                <div className={'smallCardBigNumber'} />
                <div className={'smallCardBigNumber'}>{this.shortnumber(statistics.volume)}</div>
              </div>
            </div>

            <div className={'second'}>
              <div className={'smallCardTitle'}>{dictionary._unique_authors}</div>
              <div className={'smallCardContainer'}>
                <div className={'smallCardBigNumber'} />
                <div className={'smallCardBigNumber'}>
                  {this.shortnumber(statistics.uniqueAuthors)}
                </div>
              </div>
            </div>

            <div className={'second'}>
              <div className={'smallCardTitle'}>{dictionary._panic}</div>
              <div className={'smallCardTopLegend'}>
                <div>{panic.toFixed(2)}%</div>
                <div style={{ color: 'black' }}>{(100 - panic).toFixed(2)}%</div>
              </div>
              <LinearProgress
                className={'smallCardProgBar'}
                mode="determinate"
                color={'#4192d8'}
                value={panic}
              />
              <div className={'smallCardProgLegend'}>
                <div>YES</div>
                <div style={{ color: 'black' }}>NO</div>
              </div>
            </div>

            <div className={'second'}>
              <div className={'smallCardTitle'}>{dictionary._informativeness}</div>
              <div className={'smallCardTopLegend'}>
                <div>{informativeness.toFixed(2)}%</div>
                <div style={{ color: 'black' }}>{(100 - informativeness).toFixed(2)}%</div>
              </div>
              <LinearProgress
                className={'smallCardProgBar'}
                mode="determinate"
                color={'#4192d8'}
                value={informativeness}
              />
              <div className={'smallCardProgLegend'}>
                <div>YES</div>
                <div style={{ color: 'black' }}>NO</div>
              </div>
            </div>
          </div>
        ) : (
          <div />
        )}

        <div className={'root'}>
          <Tagcloud
            dictionary={dictionary}
            data={this.props.hashtagCloud}
            cloudScale={this.props.cloudScale}
            setCloudScale={cloudScale => this.props.setCloudScale(cloudScale)}
          />

          <Topprofiles
            dictionary={dictionary}
            data={statistics.topProfiles}
            sort={profiles => this.props.sortProfiles(profiles)}
            shortnumber={(n, decimals) => this.shortnumber(n, decimals)}
          />
        </div>

        <div className={'root'}>
          <Tophashtags dictionary={dictionary} data={statistics.topHashtags} />

          <Topgraph dictionary={dictionary} data={statistics.topHashtags} />
        </div>
        <div className={'root'}>
          <InfoTypeGrap data={statistics.infoTypeByHazard} />
        </div>
      </section>
    );
  }
}

export default enhance(muiThemeable()(Statistics));
