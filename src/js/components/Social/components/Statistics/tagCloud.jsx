import React, { Component } from 'react';
import { IconButton, FontIcon, Toolbar, ToolbarGroup, ToolbarTitle, Paper } from 'material-ui';
import { withStatistics } from 'js/modules/socialApi';
import { TagCloud } from 'react-tagcloud';
import '../style.scss';

class Tagcloud extends Component {
  cloudHashtagRender(tag, size, color) {
    let hunit = (this.props.cloudScale.max - this.props.cloudScale.min) / 100;
    if (hunit === 0) hunit = 0.1;
    let lhunit = (40 - 1) / 100;
    let lh = (tag.total - this.props.cloudScale.min) / hunit * lhunit + 1;

    let fsunit = 20 / 100;
    let fsize = (tag.total - this.props.cloudScale.min) / hunit * fsunit + 13;

    let cunit = (255 - 123) / 100;
    let csize = Math.round((tag.total - this.props.cloudScale.min) / hunit * cunit + 100);
    let finalcolor = `rgb(${csize},${csize},${csize})`;
    if (tag.hashtag.length % 2 === 0) {
      lh = 0;
    }

    return (
      <span
        key={tag.hashtag}
        className={`tag-${size}`}
        style={{
          fontSize: `${fsize}px`,
          margin: '3px',
          padding: '3px',
          display: 'inline-block',
          color: `${finalcolor}`,
          maxWidth: '100%',
          lineHeight: `${lh}px`,
          wordWrap: 'break-word'
        }}
      >
        {tag.hashtag}
      </span>
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.data.length !== 0 && this.props.statistics !== prevProps.statistics) {
      var lowest = 0;
      var highest = 100000000;
      var tmp;
      for (var i = this.props.data.length - 1; i >= 0; i--) {
        tmp = this.props.data[i].total;
        if (tmp > lowest) lowest = tmp;
        if (tmp < highest) highest = tmp;
      }
      if (this.props.cloudScale.min !== highest || this.props.cloudScale.max !== lowest) {
        this.props.setCloudScale({
          min: highest,
          max: lowest
        });
      }
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.statistics !== this.props.statistics ||
      this.props.cloudScale !== nextProps.cloudScale
    );
  }
  render() {
    const cloudHeader = (
      <Toolbar className="toolbar">
        <ToolbarGroup>
          <ToolbarTitle style={{ color: 'white' }} text={this.props.dictionary._hashtags_cloud} />
          <div style={{ textAlign: 'right' }}>
            <IconButton>
              <FontIcon className="material-icons">more_vert</FontIcon>
            </IconButton>
          </div>
        </ToolbarGroup>
      </Toolbar>
    );
    return this.props.data.length !== 0 ? (
      <div className={'leftDiv'}>
        <Paper>
          <div children={cloudHeader} />
          <div className="cardContent" style={{ display: 'flex' }}>
            <div className={'hashtagCloudContainer'}>
              <TagCloud
                minSize={12}
                maxSize={20}
                tags={this.props.data}
                style={{ textAlign: 'center' }}
                disableRandomColor={true}
                renderer={this.cloudHashtagRender.bind(this)}
              />
            </div>
          </div>
        </Paper>
      </div>
    ) : (
      <div />
    );
  }
}

export default withStatistics(Tagcloud);
