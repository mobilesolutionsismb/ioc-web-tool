import React, { Component } from 'react';
import { IconButton, FontIcon, Toolbar, ToolbarGroup, ToolbarTitle, Paper } from 'material-ui';
import { withDictionary } from 'ioc-localization';

import { VictoryChart, VictoryAxis, VictoryStack, VictoryBar } from 'victory';
import '../style.scss';
import { HAZARD_LABELS, SOCIAL_INFOTYPE_LABELS } from '../socialTagsConfig.js';

class InfoTypeGraph extends Component {
  render() {
    const colors = {
      background: '#242424',
      content: '#303030',
      header: '#3c3c3c'
    };
    // const topColors = ['#41cedc', '#1dc339', '#51ffc7', '#ffeb00', '#ff8d00'];
    const titlegraph = (
      <div style={{ display: 'flex', alignItems: 'baseline' }}>{'Info Type'} </div>
    );
    const graphHeader = (
      <Toolbar className="toolbar">
        <ToolbarGroup>
          <ToolbarTitle style={{ color: 'white' }} text={titlegraph} />
          <div style={{ textAlign: 'right' }}>
            <IconButton>
              <FontIcon className="material-icons">more_vert</FontIcon>
            </IconButton>
          </div>
        </ToolbarGroup>
      </Toolbar>
    );

    let data = [];
    let auxObject = {};
    let tickFormat = [];
    let tickValues = [];
    let auxY = [];
    let auxColors = ['#ac2424', '#d3570e', 'green', 'yellow', 'gray', 'white'];
    let objectsPerHazard = [];
    let countAux = 0;
    let num = 0;
    let divHeight = 280;
    let victoryHeight = 400;
    if (this.props.data !== undefined) {
      num = this.props.data.length;
      divHeight = num === 2 ? 120 : num > 5 ? 280 : 180;
      victoryHeight = num === 2 ? 160 : num > 5 ? 400 : 250;
      data = this.props.data.map((row, index) => {
        tickValues[index] = index + 1;
        tickFormat[index] = this.props.dictionary[HAZARD_LABELS[row.hazard]];
        auxObject = {};
        countAux = 0;
        auxObject['hazard'] = this.props.dictionary[HAZARD_LABELS[row.hazard]];
        if (index === 0) {
          row.infoTypes.reduce((countAux, row, index) => {
            auxY[index] = row.infoType;
            auxObject[row.infoType] = row.frequency;
            countAux = countAux + row.frequency;
            return countAux;
          }, countAux);
        } else {
          row.infoTypes.reduce((countAux, row, index) => {
            auxObject[row.infoType] = row.frequency;
            countAux = countAux + row.frequency;
            return countAux;
          }, countAux);
        }
        objectsPerHazard[index] = countAux;
        return auxObject;
      });
      //percentage
      /*data.map((row, index) =>{
        for (var property in row) {
            if (row.hasOwnProperty(property)) {
                if(property!='hazard'){

                  data[index][property] = data[index][property]> 0 ?(data[index][property] /objectsPerHazard[index]) *100 : 0;
                }
            }
        }
      });*/
      /*console.log("Final value");
      console.log(data);*/
    }
    const { dictionary } = this.props;
    return data.length !== 0 ? (
      <div className={'unique'}>
        <Paper>
          <div children={graphHeader} />
          <div
            style={{
              backgroundColor: colors.content,
              height: divHeight,
              paddingTop: num === 2 ? 30 : 0
            }}
          >
            <VictoryChart
              domainPadding={20}
              width={800}
              height={victoryHeight}
              className="victorybarmain"
            >
              <VictoryAxis
                style={{
                  axis: { stroke: 'white' },
                  axisLabel: { fontSize: 25, padding: 30 },
                  ticks: {
                    size: tick => {
                      const tickSize = tick % 2 === 0 ? 15 : 7;
                      return tickSize;
                    },
                    stroke: 'white',
                    strokeWidth: 1
                  },
                  tickLabels: { fontSize: 17, padding: 5, fill: 'white' }
                }}
              />
              <VictoryStack horizontal>
                {auxY.map((row, index) => {
                  return (
                    <VictoryBar
                      key={index}
                      data={data}
                      x="hazard"
                      y={row}
                      style={{
                        data: { fill: auxColors[index] }
                      }}
                    />
                  );
                })}
              </VictoryStack>
              <VictoryAxis
                dependentAxis
                tickValues={tickValues}
                tickFormat={tickFormat}
                style={{
                  axis: { stroke: 'white' },
                  axisLabel: { fontSize: 25, padding: 30 },
                  ticks: { stroke: 'white', size: 20 },
                  tickLabels: { fontSize: 20, padding: 5, fill: 'white' }
                }}
              />
            </VictoryChart>
          </div>
          <div
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              justifyContent: 'space-around',
              paddingTop: '5px'
            }}
          >
            {auxY.map((row, index) => {
              return (
                <div
                  key={index}
                  style={{
                    display: 'flex',
                    padding: '5px'
                  }}
                >
                  <div
                    style={{
                      height: 20,
                      width: 20,
                      backgroundColor: auxColors[index],
                      marginRight: 2
                    }}
                  />
                  <div>
                    {SOCIAL_INFOTYPE_LABELS[row] ? dictionary[SOCIAL_INFOTYPE_LABELS[row]] : row}
                  </div>
                </div>
              );
            })}
          </div>
        </Paper>
      </div>
    ) : (
      <div />
    );
  }
}

export default withDictionary(InfoTypeGraph);
