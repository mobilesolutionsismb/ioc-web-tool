import React, { Component } from 'react';
import { IconButton, FontIcon, Toolbar, ToolbarGroup, ToolbarTitle, Paper } from 'material-ui';
import '../style.scss';

class Tophashtags extends Component {
  render() {
    const colors = {
      background: '#242424',
      content: '#303030',
      header: '#3c3c3c'
    };
    const topColors = ['#41cedc', '#1dc339', '#51ffc7', '#ffeb00', '#ff8d00'];
    const tophashtagsHeader = (
      <Toolbar
        style={{ padding: 0, backgroundColor: colors.header, display: 'block', paddingLeft: 20 }}
      >
        <ToolbarGroup>
          <ToolbarTitle style={{ color: 'white' }} text={this.props.dictionary._top_hashtags} />
          <div style={{ textAlign: 'right' }}>
            <IconButton>
              <FontIcon className="material-icons">more_vert</FontIcon>
            </IconButton>
          </div>
        </ToolbarGroup>
      </Toolbar>
    );

    return this.props.data !== undefined ? (
      <div className={'leftDiv'}>
        <Paper>
          <div children={tophashtagsHeader} />
          <div className="cardContent" style={{ paddingLeft: 20, overflowY: 'scroll' }}>
            <div style={{ margin: 20 }}>
              {this.props.data.map((row, index) => {
                return (
                  <div
                    key={index}
                    style={{ marginBottom: 30, overflow: 'hidden', textOverflow: 'ellipsis' }}
                    title={`#${row.hashtag}`}
                  >
                    <span
                      className="topCircle"
                      style={{ color: index <= 4 ? topColors[index] : '#303030' }}
                    >
                      ⬤
                    </span>
                    <span style={{ marginRight: 15 }}>{index + 1}</span>
                    #{row.hashtag}
                  </div>
                );
              }, this)}
            </div>
          </div>
        </Paper>
      </div>
    ) : (
      <div />
    );
  }
}

export default Tophashtags;
