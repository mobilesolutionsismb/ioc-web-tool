import React, { Component } from 'react';
import { IconButton, FontIcon, Toolbar, ToolbarGroup, ToolbarTitle, Paper } from 'material-ui';
import moment from 'moment';
import { VictoryChart, VictoryLine, VictoryAxis } from 'victory';
import { withAPISettings } from 'ioc-api-interface';
import '../style.scss';

class Topgraph extends Component {
  render() {
    const colors = {
      background: '#242424',
      content: '#303030',
      header: '#3c3c3c'
    };
    const topColors = ['#41cedc', '#1dc339', '#51ffc7', '#ffeb00', '#ff8d00'];
    const titlegraph = (
      <div style={{ display: 'flex', alignItems: 'baseline' }}>
        {this.props.dictionary._top_hashtags_trend}{' '}
      </div>
    );
    const graphHeader = (
      <Toolbar className="toolbar">
        <ToolbarGroup>
          <ToolbarTitle style={{ color: 'white' }} text={titlegraph} />
          <div style={{ textAlign: 'right' }}>
            <IconButton>
              <FontIcon className="material-icons">more_vert</FontIcon>
            </IconButton>
          </div>
        </ToolbarGroup>
      </Toolbar>
    );
    let dataGraph = [];
    // let auxLine = [];
    if (this.props.data !== undefined) {
      dataGraph = this.props.data.slice(0, 5).map((row, index) => {
        const auxLine = row.serie.map((row, index) => ({
          x: moment(row.endTimestamp, 'YYYYMMDDHHmm').format('HH:mm'),
          y: row.frequency
        }));
        return auxLine;
      });
      return this.props.data !== undefined ? (
        <div className={'rightDiv'}>
          <Paper>
            <div children={graphHeader} />
            <div style={{ backgroundColor: colors.content, height: 300 }}>
              <VictoryChart width={800} height={400}>
                <VictoryAxis
                  scale="time"
                  style={{
                    axis: { stroke: 'white' },
                    axisLabel: { fontSize: 20, padding: 30 },
                    ticks: { stroke: 'white', size: 15 },
                    tickLabels: { fontSize: 15, padding: 5, fill: 'white', angle: -45 }
                  }}
                  standalone={false}
                />
                <VictoryAxis
                  dependentAxis
                  style={{
                    axis: { stroke: 'white' },
                    axisLabel: { fontSize: 20, padding: 30 },
                    ticks: { stroke: 'white', size: 15 },
                    tickLabels: { fontSize: 15, padding: 5, fill: 'white' }
                  }}
                  standalone={false}
                />
                {dataGraph.map((row, index) => {
                  return (
                    <VictoryLine
                      key={index}
                      style={{
                        data: { stroke: topColors[index] }
                      }}
                      scale={{ x: 'time', y: 'linear' }}
                      data={row}
                    />
                  );
                })}
              </VictoryChart>
            </div>
          </Paper>
        </div>
      ) : (
        <div />
      );
    } else return <div />;
  }
}

export default withAPISettings(Topgraph);
