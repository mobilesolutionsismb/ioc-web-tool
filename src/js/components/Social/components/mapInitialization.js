import { getMapSettings /* , attachCursorEvents */ } from 'js/components/MapView';
import { point, featureCollection } from '@turf/helpers';
import { variableCircleInMeters, DEFAULT_POSITION_CIRCLE_PAINT } from 'js/modules/map';
export const clusterMaxZoom = 10;

export function onMapLoad(event, doneCallback, hoverCallback, mouseLeaveCallback, rest) {
  const map = event.target;
  const coords = rest.geolocation.coords;
  const { longitude, latitude, accuracy } = coords;

  map.addSource('userposition', {
    type: 'geojson',
    data: point([longitude, latitude])
  });

  map.addSource('tweets', {
    type: 'geojson',
    data: featureCollection([]),
    cluster: true,
    clusterMaxZoom: clusterMaxZoom, // Max zoom to cluster points on
    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
  });

  map.addSource('hide-tweets', {
    type: 'geojson',
    data: featureCollection([])
  });

  map.addSource('spiderify-tweets', {
    type: 'geojson',
    data: featureCollection([])
  });

  map.addSource('center-spiderify-tweets', {
    type: 'geojson',
    data: featureCollection([])
  });

  map.addSource('event-detection', {
    type: 'geojson',
    data: null
  });

  map.addSource('event-detection-point', {
    type: 'geojson',
    data: featureCollection([])
  });

  map.addSource('bounding-box', {
    type: 'geojson',
    data: featureCollection([])
  });

  // Layers

  map.addLayer({
    id: 'hide-tweets-layer',
    type: 'circle',
    source: 'hide-tweets',
    paint: {
      'circle-radius': 20,
      'circle-opacity': 0
    }
  });

  //POSITION

  map.addLayer({
    id: 'position-marker-accuracy',
    type: 'circle',
    source: 'userposition',
    paint: { ...variableCircleInMeters(accuracy, latitude), 'circle-opacity': 0.25 }
  });

  map.addLayer({
    id: 'position-marker',
    type: 'circle',
    source: 'userposition',
    paint: DEFAULT_POSITION_CIRCLE_PAINT
  });

  //TWEETS
  map.addLayer({
    id: 'tweet-marker-click',
    type: 'circle',
    source: 'tweets',
    paint: {
      'circle-radius': 30,
      'circle-color': '#449fdb',
      'circle-translate': [0, -18],
      'circle-opacity': 0.5
    },
    filter: ['==', 'tweetId', '']
  });
  map.addLayer({
    id: 'spiderify-tweet-marker-click',
    type: 'circle',
    source: 'spiderify-tweets',
    paint: {
      'circle-radius': 30,
      'circle-color': '#449fdb',
      'circle-translate': [0, -18],
      'circle-opacity': 0.5
    },
    filter: ['==', 'tweetId', '']
  });

  map.loadImage(require('assets/twitter-icon/twitter.png'), function(error, image) {
    map.addImage('twiiter-icon', image);
    map.addLayer({
      id: 'tweet-marker',
      source: 'tweets',
      type: 'symbol',
      layout: {
        'icon-image': 'twiiter-icon',
        'icon-size': 0.5,
        'icon-anchor': 'bottom',
        'icon-allow-overlap': true
      },
      paint: {
        'text-halo-width': 2,
        'text-halo-color': '#fff',
        'text-color': '#449fdb'
      },
      filter: ['!has', 'point_count']
    });

    map.addLayer({
      id: 'spiderify-tweet-marker',
      source: 'spiderify-tweets',
      type: 'symbol',
      layout: {
        'icon-image': 'twiiter-icon',
        'icon-size': 0.5,
        'icon-anchor': 'bottom',
        'icon-allow-overlap': true
      },
      paint: {
        'text-halo-width': 2,
        'text-halo-color': '#fff',
        'text-color': '#449fdb'
      },
      filter: ['all', ['==', '$type', 'Point']]
    });
  });

  map.addLayer({
    id: 'spiderify-tweet-line',
    source: 'spiderify-tweets',
    type: 'line',
    paint: {
      'line-color': 'black',
      'line-dasharray': [0.2, 2],
      'line-width': 2
    },
    filter: ['==', '$type', 'LineString']
  });
  map.addLayer({
    id: 'center-spiderify',
    source: 'center-spiderify-tweets',
    type: 'circle',
    paint: {
      'circle-radius': 5,
      'circle-color': 'black'
    }
  });
  //CLUSTERING

  map.addLayer({
    id: 'clusters',
    type: 'circle',
    source: 'tweets',
    filter: ['has', 'point_count'],
    paint: {
      //   * Blue, 20px circles when point count is less than 10
      //   * Yellow, 30px circles when point count is between 10 and 50
      //   * Pink, 40px circles when point count is greater than or equal to 50
      'circle-color': ['step', ['get', 'point_count'], '#51bbd6', 10, '#f1f075', 50, '#f28cb1'],
      'circle-radius': ['step', ['get', 'point_count'], 20, 10, 30, 50, 40]
    }
  });

  map.addLayer({
    id: 'cluster-count',
    type: 'symbol',
    source: 'tweets',
    filter: ['has', 'point_count'],
    layout: {
      'text-field': '{point_count_abbreviated}',
      'text-size': 12,
      'text-font': ['Open Sans Bold'],
      'text-allow-overlap': true
    }
  });

  //EVENT DETECTION

  map.addLayer({
    id: 'events',
    type: 'fill',
    source: 'event-detection',
    paint: {
      'fill-color': '#ff6e40',
      'fill-outline-color': '#bf360c',
      'fill-opacity': 0.3
    }
  });

  map.addLayer({
    id: 'event-point',
    type: 'symbol',
    source: 'event-detection-point',
    layout: {
      'text-font': ['I-REACT_Pinpoints'],
      'text-field': '\ue91c',
      'text-size': {
        base: 20,
        stops: [[0, 26], [12, 36], [20, 40], [24, 46]]
      },
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 2,
      'text-halo-color': 'white',
      'text-color': '#bf360b'
    }
  });

  map.addLayer({
    id: 'event-point-symbol',
    type: 'symbol',
    source: 'event-detection-point',
    layout: {
      'text-font': ['I-REACT_Pinpoints'],
      'text-field': {
        type: 'categorical',
        property: 'hazard',
        stops: [
          ['none', ' '],
          ['wildfires', '\ue916'],
          ['floods', '\ue917'],
          ['extreme_weather_conditions', '\ue915'],
          ['drought', '\ue915'],
          ['storms', '\ue915'],
          ['snow', '\ue915'],
          ['landslide', '\ue913'],
          ['earthquake', '\ue914']
        ],
        default: '\ue916'
      },
      'text-size': {
        base: 20,
        stops: [[0, 26], [12, 36], [20, 40], [24, 46]]
      },
      'text-allow-overlap': true
    },
    paint: {
      'text-color': 'white'
    }
  });

  // BOUNDING BOX SELECTED

  map.addLayer({
    id: 'bounding-box',
    type: 'line',
    source: 'bounding-box',
    paint: {
      'line-color': '#000000',
      'line-dasharray': [0.5]
    }
  });

  if (typeof doneCallback === 'function') {
    doneCallback(map, getMapSettings(map));
  }
}
