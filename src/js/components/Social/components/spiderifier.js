import { lineString } from '@turf/helpers';
import destination from '@turf/destination';
import booleanEqual from '@turf/boolean-equal';

// DEFAULTS
const MAX_ITEMS = 8; // max item per spiral circle
const LD = 0.11; //0.001; // line radius in km
const LDU = 'kilometers'; // line radius distance

const DEFAULT_PARAMS = {
  maxItemsPerCircle: MAX_ITEMS,
  lineDistance: LD,
  distanceStep: LD * 0.5,
  lineDistanceUnit: LDU
};

export class Spiderifier {
  constructor(params = DEFAULT_PARAMS) {
    const { maxItemsPerCircle, lineDistance, distanceStep, lineDistanceUnit } = params;

    this._maxItemsPerCircle = maxItemsPerCircle;
    this._lineDistance = lineDistance;
    this._distanceStep = distanceStep;
    this._distanceOptions = { units: lineDistanceUnit };

    this.destination = destination;
    this.booleanEqual = booleanEqual;
    this.lineString = lineString;
  }

  _getDistance = (centerCoords, distance, alpha) => {
    return this.destination(centerCoords, distance, alpha, this._distanceOptions);
  };

  /**
   *
   * @param {GeoJSONPointGeometry} clusterCenter - coming from supercluster
   * @param {Array<GeoJSONPointFeature>} features - children of the cluster
   */
  _getSpiderifiedFeatures = (features, nPoints) => {
    const alpha = 360 / Math.min(this._maxItemsPerCircle, features.length); // degrees
    const center = features[0];
    let spider = [];
    const originalFeatures = features;
    //const clusterId = clusterCenter.properties.cluster_id; // comes from supercluster
    for (let i = 0; i < nPoints; i++) {
      const loopNumber = Math.floor(i / this._maxItemsPerCircle);
      const _alphaOffset = alpha > 0 && loopNumber > 0 ? alpha / (loopNumber + 1) : 0;
      const _alpha = (i * alpha + _alphaOffset) % 360;

      const _distance = this._lineDistance + loopNumber * this._distanceStep;
      const point = this._getDistance(center.geometry, _distance, _alpha);
      point.properties = features[i].properties;
      const line = this.lineString([center.geometry.coordinates, point.geometry.coordinates], {
        leg_id: `leg-${center.properties.tweetId}-${i}`,
        isLeg: true
      });
      spider = [...spider, point, line];
    }
    return { spider, originalFeatures, center: [center] };
  };

  _get;

  /**
   *
   * @param {Array<GeoJSONPointFeature>} features - features present on the visible bounding box
   * @return Array<Object> { spider: Array<GeoJSONPointFeature>, originalFeatures: Array<GeoJSONPointFeature>, center: Array<int>}
   */

  getSpiderifiedFeatures = features => {
    let areEqual = [];

    function* _getTweetsInSamePosition(newFe, areEqual, coordinates) {
      if (newFe.length > 0) {
        let equals = newFe.filter(
          f =>
            f.geometry.coordinates[0] === coordinates[0] &&
            f.geometry.coordinates[1] === coordinates[1]
        );
        newFe = newFe.filter(
          f =>
            f.geometry.coordinates[0] !== coordinates[0] ||
            f.geometry.coordinates[1] !== coordinates[1]
        );
        if (equals.length > 1) {
          areEqual.push(equals);
          if (newFe.length > 0)
            yield* _getTweetsInSamePosition(newFe, areEqual, newFe[0].geometry.coordinates);
          else yield areEqual;
        } else {
          if (newFe.length > 0)
            yield* _getTweetsInSamePosition(newFe, areEqual, newFe[0].geometry.coordinates);
          else yield areEqual;
        }
      } else {
        yield areEqual;
      }
    }

    var gen = _getTweetsInSamePosition(features, areEqual, features[0].geometry.coordinates);
    let res = gen.next();
    while (!res.done) {
      if (res.value !== null) {
        break;
      }
      res = gen.next();
    }
    const arrayEqualFeatures = res.value;
    // const arrayEqualFeatures = [...areEqual];
    if (arrayEqualFeatures.length > 0) {
      return arrayEqualFeatures.map(equalFeatures =>
        this._getSpiderifiedFeatures(equalFeatures, equalFeatures.length)
      );
    } else return null;
  };
}
