export function compareBbox(actualbbx, selectedbbx) {
  if (
    actualbbx[0][0].toFixed(2) === selectedbbx[0][0].toFixed(2) &&
    actualbbx[0][1].toFixed(2) === selectedbbx[0][1].toFixed(2) &&
    actualbbx[1][0].toFixed(2) === selectedbbx[1][0].toFixed(2) &&
    actualbbx[1][1].toFixed(2) === selectedbbx[1][1].toFixed(2) &&
    actualbbx[2][0].toFixed(2) === selectedbbx[2][0].toFixed(2) &&
    actualbbx[2][1].toFixed(2) === selectedbbx[2][1].toFixed(2) &&
    actualbbx[3][0].toFixed(2) === selectedbbx[3][0].toFixed(2) &&
    actualbbx[3][1].toFixed(2) === selectedbbx[3][1].toFixed(2)
  ) {
    return true;
  } else {
    return false;
  }
}
