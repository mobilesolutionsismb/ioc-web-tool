// const APP_BAR_HEIGHT = 64;
const APP_NAV_WEIGHT = 64;

// const FILTERS_BAR_HEIGHT = 140;
const STATUS_BAR_HEIGHT = 55;

// const CARDS_SECTION_HEIGHT = 250;

export const statusBarStyle = {
  marginLeft: APP_NAV_WEIGHT,
  height: STATUS_BAR_HEIGHT,
  width: `calc(100% - ${APP_NAV_WEIGHT}px)`,
  borderTop: '1px solid rgb(52, 52, 52)'
};

export const filtersBarStyle = {
  boxSizing: 'border-box',
  marginLeft: APP_NAV_WEIGHT,
  width: `calc(100% - ${APP_NAV_WEIGHT}px)`,
  borderBottom: '1px solid rgb(52, 52, 52)',
  paddingBottom: '10px',
  transition: 'all 0.5s'
};

export const mapStyle = {
  height: '290px'
};

export const cardsStyle = {
  padding: '10px 15px',
  boxSizing: 'border-box'
};

export const eventCardStyle = {
  padding: '10px 15px',
  maxHeight: '70px'
};

export const tweetsListStyle = {
  width: '26%',
  height: '100%',
  boxSizing: 'border-box',
  transition: 'all 0.5s'
};

export const threeColumns = {
  marginLeft: APP_NAV_WEIGHT,
  display: 'flex',
  flexDirection: 'row',
  width: `calc(100% - ${APP_NAV_WEIGHT}px)`,
  height: '100%'
};

export const middleColumn = {
  width: '50%',
  transition: 'all 1s',
  overflowY: 'auto'
};

const STYLE = {
  statusBarStyle,
  filtersBarStyle,
  mapStyle,
  tweetsListStyle,
  threeColumns,
  middleColumn,
  cardsStyle,
  eventCardStyle
};

export default STYLE;
