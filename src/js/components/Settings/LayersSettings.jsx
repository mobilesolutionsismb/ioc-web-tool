import React, { Component } from 'react';
import { compose } from 'redux';
import {
  /* List, ListItem, */ Paper,
  SelectField,
  MenuItem,
  Toggle,
  RaisedButton
} from 'material-ui';
// import { iReactTheme } from 'js/startup/iReactTheme';
import LayersSettingsSelector from './LayersSettingsSelector';

// import { Link } from 'react-router-dom';
// import { withDictionary } from 'ioc-localization';
import { withGeneralSetting /*, withApplicationSetting*/ } from 'ioc-api-interface';
import { withLoader } from 'js/modules/ui';
import { withUserSettings } from 'js/modules/UserSettings';
import { getDurationFormat } from 'js/utils/localizeDate';

const enhance = compose(
  /* withDictionary, */ withGeneralSetting,
  /*withApplicationSetting,*/ withLoader,
  withUserSettings
);

const STYLES = {
  card: {
    backgroundColor: '#454545',
    width: '100%',
    padding: '15px 25px',
    marginBottom: 10
  }
};

const HeaderItem = props => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }}
  >
    <div style={{ padding: '10px 0 20px 0', minWidth: 300 }} />
    <div style={{ padding: '10px 0 20px 0', minWidth: 150, color: '#c7c7c7' }}> TIME RANGE </div>
    <div style={{ padding: '10px 0 20px 0', minWidth: 80, color: '#c7c7c7' }}> HIDE/SHOW </div>
  </div>
);

const Header = props => (
  <div
    style={{
      fontWeight: 'bold',
      padding: '10px 0 20px 0',
      minWidth: 300,
      display: 'inline-flex',
      width: '100%'
    }}
  >
    <div style={{ width: '100%' }}>
      <h3 style={{ marginBottom: 0 }}>{props.title}</h3>
      <div style={{ fontSize: 12, color: '#6b6b6b' }} className="subtitle-settings">
        Subtitle component goes here
      </div>
    </div>
    <RaisedButton
      label="SAVE"
      primary={true}
      style={{ alignSelf: 'center' }}
      onClick={props.save}
    />
  </div>
);

class ItemToModify extends Component {
  changeRangeSetting(e, key, value) {
    this.props.updateLocalUserSettings(key, value);
  }

  changeShowLayer(e, key) {
    let value = '';
    if (this.props.allUserSettings[this.props.showHide.name]) {
      if (this.props.allUserSettings[this.props.showHide.name] === 'Show') value = 'Hide';
      else value = 'Show';
    } else {
      if (this.props.showHide.customData.defaultOption === 'Show') value = 'Hide';
      else value = 'Show';
    }
    this.props.updateLocalUserSettings(key, value);
  }
  render() {
    // let { dictionary } = this.props;

    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          minHeight: '48px'
        }}
        className="layers-settings-configuration-item"
      >
        <div
          style={{
            width: 300,
            color: '#c7c7c7',
            overflowX: 'hidden',
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis'
          }}
        >
          {' '}
          {this.props.displayName}{' '}
        </div>
        {this.props.leadTime ? (
          <SelectField
            hintText="Team List"
            onChange={(event, key, value) =>
              this.changeRangeSetting(event, this.props.leadTime.name, value)
            }
            value={
              this.props.allUserSettings[this.props.leadTime.name]
                ? this.props.allUserSettings[this.props.leadTime.name]
                : this.props.leadTime.customData.defaultOption
            }
          >
            {this.props.leadTime.customData.options.map((option, index) => (
              <MenuItem
                key={index}
                value={option}
                primaryText={<span>{getDurationFormat(option)}</span>}
              />
            ))}
          </SelectField>
        ) : (
          <div style={{ width: '150px' }} />
        )}
        {this.props.showHide ? (
          <Toggle
            style={{ maxWidth: 85 }}
            toggled={
              this.props.allUserSettings[this.props.showHide.name]
                ? this.props.allUserSettings[this.props.showHide.name] === 'Show'
                : this.props.showHide.customData.defaultOption === 'Show'
            }
            onToggle={e => this.changeShowLayer(e, this.props.showHide.name)}
            iconStyle={{ margin: 'auto' }}
          />
        ) : null}
      </div>
    );
  }
}

class LayersSettings extends Component {
  group = null;
  // constructor(props) {
  //   super(props);
  //   this.props.getSettingDefinitionsByGroup();
  //   this.props.getAllUserSettings();
  // }
  componentDidMount() {
    this._init(this.props);
  }

  _init = async props => {
    await props.getSettingDefinitionsByGroup();
    await props.getAllUserSettings();
  };

  reformatGroups() {
    this.group = this.props.generalSettings.groupSettingDefinition[this.getGroupByUrl()];

    let rows = [];
    let label,
      leadTime,
      showHide,
      displayName = null;
    if (this.group)
      this.group.forEach((setting, index) => {
        const type = this.capitalizeFirstLetter(
          setting.settingDefinition.customData.settingDefinitionType
        );
        const currentLabel = setting.settingDefinition.name.replace(type, '');

        if (label === null || label === currentLabel) {
          label = currentLabel;
          if (type === 'ShowHide')
            showHide = {
              customData: setting.settingDefinition.customData,
              name: setting.settingDefinition.name
            };
          else if (type === 'LeadTime')
            leadTime = {
              customData: setting.settingDefinition.customData,
              name: setting.settingDefinition.name
            };
          displayName = setting.settingDefinition.displayName;

          if (index + 1 === this.group.length) {
            rows.push({ displayName: displayName, leadTime: leadTime, showHide: showHide });
          }
        } else if (label !== currentLabel) {
          //ha habido cambio de fila,
          rows.push({ displayName: displayName, leadTime: leadTime, showHide: showHide }); //añado la fila, aunque algun campo valga null
          const type = this.capitalizeFirstLetter(
            setting.settingDefinition.customData.settingDefinitionType
          );
          const currentLabel = setting.settingDefinition.name.replace(type, '');

          label = currentLabel;
          leadTime = null;
          showHide = null;
          if (type === 'ShowHide')
            showHide = {
              customData: setting.settingDefinition.customData,
              name: setting.settingDefinition.name
            };
          else if (type === 'LeadTime')
            leadTime = {
              customData: setting.settingDefinition.customData,
              name: setting.settingDefinition.name
            };
          displayName = setting.settingDefinition.displayName;
        }
      });
    return rows;
  }

  saveSettings = () => {
    this.props.updateUserSettings(
      this.props.settingsToModify,
      this.props.loadingStart,
      this.props.loadingStop
    );
    this.props.resetUserSettingsToModify();
  };

  getGroupByUrl() {
    if (this.props.match.params.subpanel) {
      return `App.UserSettingsGroups.Layers.${this.props.match.params.subpanel.replace('-', '.')}`;
    }

    return Object.keys(this.props.generalSettings.groupSettingDefinition)[0];
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  getName() {
    let name = '';
    if (this.props.items) {
      this.props.items.forEach(child => {
        if (child.children.some(item => item.name === this.getGroupByUrl())) {
          name = child.children.find(item => item.name === this.getGroupByUrl()).displayName;
          console.log(name);
        }
      });
    }
    return name;
  }
  render() {
    // let { dictionary } = this.props;
    const selected = this.getGroupByUrl();

    return (
      <div className="panel-settings">
        <LayersSettingsSelector match={this.props.match} layersList={this.props.items} />

        <div className="panel-selected-settings">
          <Header {...this.group} {...this.props} title={this.getName()} save={this.saveSettings} />
          <Paper style={STYLES.card}>
            {this.props.generalSettings.groupSettingDefinition[selected] ? (
              <div>
                <HeaderItem {...this.props} />
                {this.reformatGroups().map((a, i) => (
                  <ItemToModify
                    key={i}
                    displayName={a.displayName}
                    leadTime={a.leadTime}
                    showHide={a.showHide}
                    {...this.props}
                  />
                ))}
              </div>
            ) : (
              'Settings not available'
            )}
          </Paper>
        </div>
      </div>
    );
  }
}
export default enhance(LayersSettings);
