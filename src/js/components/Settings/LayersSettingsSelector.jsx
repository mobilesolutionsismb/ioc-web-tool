import React, { Component } from 'react';
import { compose } from 'redux';

import { List, Subheader, ListItem, Divider } from 'material-ui';
// import { iReactTheme } from 'js/startup/iReactTheme';

import { Link } from 'react-router-dom';
// import { withDictionary } from 'ioc-localization';
import { withGeneralSetting /*, withApplicationSetting*/ } from 'ioc-api-interface';

const enhance = compose(/* withDictionary, */ withGeneralSetting /*, withApplicationSetting*/);

const LayerPanelHeaderGroup = props => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }}
  >
    <Subheader style={{ fontSize: '12px' }}> {props.title} </Subheader>
  </div>
);

class LayersSettingsSelector extends Component {
  buildUrl(layerName) {
    const urlName = layerName.split('.Layers.');
    return urlName[1].replace('.', '-');
  }
  getUrl() {
    if (this.props.match.params.subpanel) {
      return `App.UserSettingsGroups.Layers.${this.props.match.params.subpanel.replace('-', '.')}`;
    }

    return Object.keys(this.props.generalSettings.groupSettingDefinition)[0];
  }
  render() {
    // let { dictionary } = this.props;
    const selected = this.getUrl();

    return (
      <div className="list-section-settings">
        {this.props.layersList.map((layer, index) => (
          <div key={index}>
            {layer.displayName ? (
              <LayerPanelHeaderGroup
                title={layer.displayName}
                name={layer.name}
                history={this.props.history}
              />
            ) : null}
            <List style={{ padding: '0' }}>
              {layer.children.map((layerChild, i) => (
                <ListItem
                  key={i}
                  primaryText={
                    <div
                      style={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}
                    >
                      {layerChild.displayName}
                    </div>
                  }
                  className={selected === layerChild.name ? 'selected-settings' : ''}
                  containerElement={
                    <Link to={`/settings/layers/${this.buildUrl(layerChild.name)}`} />
                  }
                />
              ))}
            </List>
            <Divider />
          </div>
        ))}
      </div>
    );
  }
}
export default enhance(LayersSettingsSelector);
