import './style.scss';
import React, { Component } from 'react';
import { compose } from 'redux';
import { Link, Redirect } from 'react-router-dom';
import LanguageSettings from './LanguageSettings';
import OperationalProcedure from './OperationalProcedure/OperationalProcedure';
import {
  List,
  ListItem,
  Divider,
  Toolbar,
  ToolbarGroup,
  ToolbarTitle,
  FontIcon
} from 'material-ui';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import LayersSettings from './SubComponents/LayersSettings';

import { withLogin, withGeneralSetting } from 'ioc-api-interface';
import { withUserSettings } from 'js/modules/UserSettings';
import { withRouter } from 'react-router';

import { withDictionary } from 'ioc-localization';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';

const enhance = compose(
  withLogin,
  withDictionary,
  withGeneralSetting,
  withUserSettings,
  withLayersAndSettings
);

class Settings extends Component {
  top_level_settings = [
    { displayName: '_language', name: 'language' },
    { displayName: '_social_stream', name: 'social' },
    { displayName: '_layers', name: 'layers' },
    { displayName: '_system', name: 'system' },
    { displayName: '_operational_procedure', name: 'procedure' }
  ];

  constructor(props) {
    super(props);
    this.state = {
      from: props.location.state && props.location.state.from ? props.location.state.from : '/'
    };
  }

  close = () => {
    this.props.history.push(this.state.from);
  };

  componentDidMount() {
    if (Object.keys(this.props.remoteSettings).length === 0) {
      this.props.loadSettings();
    }
  }

  render() {
    if (!this.props.loggedIn) {
      return <Redirect to="/" />;
    }
    if (this.props.loggedIn && Object.keys(this.props.remoteSettings).length === 0) {
      return <div />;
    }
    const dict = this.props.dictionary;
    const selected = this.props.match.params.panel ? this.props.match.params.panel : 'language';

    let settingsPanel = null;
    switch (selected.toLowerCase()) {
      case 'layers':
        settingsPanel = (
          <LayersSettings
            dictionary={dict}
            match={this.props.match}
            categories={this.props.remoteSettings.children[0]}
          />
        );
        break;
      case 'language':
        settingsPanel = <LanguageSettings match={this.props.match} />;
        break;
      case 'procedure':
        settingsPanel = (
          <OperationalProcedure match={this.props.match} history={this.props.history} />
        );
        break;
      default:
        settingsPanel = <div />;
    }

    return (
      <div className="page settings">
        <div className="list-settings">
          <Toolbar className="header-settings">
            <ToolbarGroup>
              <ToolbarTitle text="Settings" style={{ color: 'white' }} />
            </ToolbarGroup>
            <ToolbarGroup style={{ color: 'white' }}>
              <FontIcon className="material-icons" style={{ color: 'white' }} onClick={this.close}>
                close
              </FontIcon>
            </ToolbarGroup>
          </Toolbar>
          <List>
            {this.top_level_settings.map((setting, i) => (
              <div className={selected === setting.name ? 'selected-settings' : ''} key={i}>
                <ListItem
                  containerElement={<Link to={`/settings/${setting.name.toLowerCase()}`} />}
                  primaryText={dict(setting.displayName)}
                  rightIcon={<ChevronRight />}
                />
                <Divider inset={false} />
              </div>
            ))}
          </List>
        </div>
        <div className="section-settings">
          <Toolbar>
            <ToolbarGroup>
              <ToolbarTitle text={dict(selected.displayName)} style={{ color: 'white' }} />
            </ToolbarGroup>
          </Toolbar>
          {settingsPanel}
        </div>
      </div>
    );
  }
}

export default withRouter(enhance(Settings));
