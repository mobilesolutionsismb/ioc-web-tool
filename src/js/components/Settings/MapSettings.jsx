import React, { Component } from 'react';
import { withMapPreferences, MAP_STYLES_NAMES } from 'js/modules/preferences';
import { withDictionary } from 'ioc-localization';
import { withRouter } from 'react-router';
import { iReactTheme } from 'js/startup/iReactTheme';

import { List, ListItem, Divider, Avatar, Subheader } from 'material-ui';

const STYLES = {
  list: { width: '100%', height: '100%', overflowX: 'hidden', overflowY: 'auto' },
  avatar: {
    borderColor: iReactTheme.palette.primary1Color,
    borderWidth: '2px',
    borderStyle: 'solid',
    boxSizing: 'border-box'
  },
  selectedAvatar: {
    borderColor: iReactTheme.palette.accent2Color,
    borderWidth: '2px',
    borderStyle: 'solid',
    boxSizing: 'border-box'
  },
  selectedText: { fontWeight: 'bold' }
};

const SUFFIX_RX = /-(gl|raster)$/i;

const MAP_PICS = MAP_STYLES_NAMES.reduce((pics, style) => {
  const imageName = style.replace(SUFFIX_RX, '');
  pics[style] = require(`assets/mapSettings/${imageName}.png`);
  return pics;
}, {});

class MapSettings extends Component {
  changeMapStyle(mapStyleName) {
    this.props.changeMapStyle(mapStyleName);
    //this.props.history.push('/home/map');
  }

  render() {
    const dict = this.props.dictionary;
    const selectedStyleName = this.props.preferences.mapStyleName;
    return (
      <div className="map-settings page">
        <List style={STYLES.list}>
          <Subheader>{dict('_map_style_list_subheader')}</Subheader>
          {MAP_STYLES_NAMES.map((mapStyleName, i) => {
            const selected = selectedStyleName === mapStyleName;
            const preview = MAP_PICS[mapStyleName];
            return (
              <div key={i}>
                <ListItem
                  onClick={this.changeMapStyle.bind(this, mapStyleName)}
                  primaryText={mapStyleName}
                  style={selected ? STYLES.selectedText : {}}
                  rightAvatar={
                    <Avatar
                      style={selected ? STYLES.selectedAvatar : STYLES.avatar}
                      src={preview}
                    />
                  }
                />
                <Divider inset={false} />
              </div>
            );
          })}
        </List>
      </div>
    );
  }
}

export default withRouter(withDictionary(withMapPreferences(MapSettings)));
