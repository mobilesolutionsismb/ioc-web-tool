import React, { Component } from 'react';
import { SelectField, MenuItem } from 'material-ui';
import { iReactTheme } from 'js/startup/iReactTheme';
import { loadDictionary } from 'js/utils/getAssets';

import { withDictionary } from 'ioc-localization';

const STYLES = {
  customWidth: {
    width: 150
  },
  button: {
    margin: 12
  },
  default: {
    margin: 8
  },
  header: {
    color: iReactTheme.palette.textColor
  }
};

class LanguageSettings extends Component {
  handleChange = async (event, index, lang) => {
    const params = await loadDictionary(lang);
    const { locale, translationLoader } = params;
    this.props.loadDictionary(locale, translationLoader);
    // this.props.loadDictionary(
    //   lang,
    //   require(`bundle-loader?lazy!locale/lang_${lang}/translation.json`)
    // );
  };

  _getLanguageMenuItem = lang => (
    <MenuItem key={lang} value={lang} primaryText={this.props.supportedLanguages[lang]} />
  );

  render() {
    const dict = this.props.dictionary;

    return (
      <div className="language-settings page">
        {/* <h1 style={STYLES.header}>{dict._language_settings}</h1> */}
        <h2 style={STYLES.header}>{dict._language}</h2>
        <SelectField
          style={STYLES.button}
          floatingLabelText={dict._language}
          value={this.props.locale}
          onChange={this.handleChange}
        >
          {Object.keys(this.props.supportedLanguages).map(this._getLanguageMenuItem)}
        </SelectField>
      </div>
    );
  }
}

export default withDictionary(LanguageSettings);
