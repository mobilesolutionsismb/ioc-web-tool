import './style.scss';
import React, { Component } from 'react';
import { Paper } from 'material-ui';
import MapSettings from '../MapSettings';

const STYLES = {
  card: {
    backgroundColor: '#454545',
    width: '100%',
    padding: '15px 25px',
    marginBottom: 10
  }
};

class BaseLayerSettings extends Component {
  render() {
    return (
      <div>
        <Paper style={STYLES.card}>
          <MapSettings />
        </Paper>
      </div>
    );
  }
}
export default BaseLayerSettings;
