import './style.scss';
import React, { Component } from 'react';
import { Paper } from 'material-ui';
import ItemToModify from './ItemToModify';

const STYLES = {
  card: {
    backgroundColor: '#454545',
    width: '100%',
    padding: '15px 25px',
    marginBottom: 10
  }
};

const HeaderItem = props => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }}
  >
    <div style={{ padding: '10px 0 20px 0', minWidth: 300 }} />
    <div style={{ padding: '10px 0 20px 0', minWidth: 150, color: '#c7c7c7' }}> TIME RANGE </div>
    <div style={{ padding: '10px 0 20px 0', minWidth: 80, color: '#c7c7c7' }}> HIDE/SHOW </div>
  </div>
);

class SettingLayerPanel extends Component {
  _reformatGroups(selected) {
    let rows = [],
      rowIndex = -1;
    let prevSetting = { settingDefinition: {} };
    if (selected)
      selected.settings.forEach((currentSetting, index) => {
        const componentType = currentSetting.settingDefinition.customData.settingDefinitionType;
        if (
          currentSetting.settingDefinition.displayName === prevSetting.settingDefinition.displayName
        ) {
          //already present
          let elementToEdit = { ...rows[rowIndex] };
          elementToEdit.settingDefinition[componentType] =
            currentSetting.settingDefinition.customData;
          elementToEdit.settingDefinition[componentType].value = currentSetting.value;
          elementToEdit.settingDefinition[componentType].key = this._computeKey(
            currentSetting.settingDefinition.name,
            componentType
          );
          rows.splice(rowIndex, 1, elementToEdit);
        } else {
          rowIndex++;
          currentSetting.settingDefinition[componentType] =
            currentSetting.settingDefinition.customData;
          currentSetting.settingDefinition[componentType].value = currentSetting.value;
          currentSetting.settingDefinition[componentType].key = this._computeKey(
            currentSetting.settingDefinition.name,
            componentType
          );
          prevSetting = currentSetting;
          rows.splice(rowIndex, 0, currentSetting);
        }
      });
    return rows;
  }

  _computeKey(name, type) {
    let array = name.split('.');
    if (type === 'showHide') array[array.length - 1] = 'ShowHide';
    else array[array.length - 1] = 'LeadTime';

    return array.join('.');
  }

  _capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    const { selected, itemEditedList } = this.props;
    const listItems = this._reformatGroups(selected);

    return (
      <div>
        <Paper style={STYLES.card}>
          {selected ? (
            <div>
              {selected.displayName}
              <HeaderItem {...this.props} />
              {listItems.map((a, i) => (
                <ItemToModify
                  itemEditedList={itemEditedList}
                  addSettingToBeUpdated={this.props.addSettingToBeUpdated}
                  key={i}
                  item={a}
                />
              ))}
            </div>
          ) : (
            <span>Settings not available</span>
          )}
        </Paper>
      </div>
    );
  }
}
export default SettingLayerPanel;
