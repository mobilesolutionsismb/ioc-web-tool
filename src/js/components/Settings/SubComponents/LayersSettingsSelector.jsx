import './style.scss';
import React, { Component } from 'react';
import { Subheader, Divider } from 'material-ui';
import SettingMenuItem from './SettingMenuItem';

const LayerPanelHeaderGroup = props => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }}
  >
    <Subheader style={{ fontSize: '12px' }}> {props.title} </Subheader>
  </div>
);

class LayersSettingsSelector extends Component {
  render() {
    const { menuItems, history, match } = this.props;

    return (
      <div className="list-section-settings">
        {menuItems.map((item, index) => (
          <div key={index}>
            {item.displayName ? (
              <LayerPanelHeaderGroup title={item.displayName} name={item.name} history={history} />
            ) : null}
            <SettingMenuItem menuItems={item.children} match={match} menuDisplayName={item.name} />
            <Divider />
          </div>
        ))}
      </div>
    );
  }
}
export default LayersSettingsSelector;
