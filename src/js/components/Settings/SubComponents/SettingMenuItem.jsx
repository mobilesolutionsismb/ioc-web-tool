import React, { Component } from 'react';
import { List, ListItem } from 'material-ui';
import { Link } from 'react-router-dom';

class SettingMenuItem extends Component {
  buildUrl(layerName) {
    const urlName = layerName.split('.Layers.');
    return urlName[1].replace('.', '-');
  }

  render() {
    const { menuItems, menuDisplayName } = this.props;

    return (
      <List style={{ padding: '0' }}>
        {menuItems.map((itemChild, i) => (
          <div key={i}>
            <ListItem
              key={i}
              //primaryText={<div style={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}>{itemChild.displayName}</div>} className={(match.params.panel === itemChild.name ? 'selected-settings' : '')}
              primaryText={
                <div style={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}>
                  {itemChild.displayName}
                </div>
              }
              containerElement={<Link to={`/settings/layers/${this.buildUrl(itemChild.name)}`} />}
            />
            {menuDisplayName === 'App.UserSettingsGroups.Layers.General' ? (
              <ListItem
                primaryText={
                  <div
                    style={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}
                  >
                    Base Layers
                  </div>
                }
                containerElement={<Link to="/settings/layers/General-BaseLayers" />}
              />
            ) : (
              <div />
            )}
          </div>
        ))}
      </List>
    );
  }
}
export default SettingMenuItem;
