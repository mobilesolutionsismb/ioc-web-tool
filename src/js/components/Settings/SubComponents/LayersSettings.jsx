import React, { Component } from 'react';
import LayersSettingsSelector from './LayersSettingsSelector';
import SettingLayerPanelList from './SettingLayerPanelList';
import { findInSettingsTree } from 'js/modules/LayersAndSettings/settingsTreeUtils';

/**
 * Return true if node.name ends with endString
 *
 * @param {any} endString
 * @param {any} node
 * @returns
 */
function findNodeByEndStringCallback(endString, node) {
  return typeof node.name === 'string' && node.name.endsWith(endString);
}

class LayersSettings extends Component {
  // Luca's version with warnings fixed
  // _findByName = (startingName, a) => a.name === startingName

  _getGroupByUrl(categories) {
    if (this.props.match.params.subpanel === 'General-BaseLayers') {
      return { children: [], isBaseLayersSelected: true };
    } else if (this.props.match.params.subpanel) {
      const subpanelRouteName = this.props.match.params.subpanel;
      const findCb = findNodeByEndStringCallback.bind(null, subpanelRouteName.replace('-', '.'));
      const result = findInSettingsTree(categories, findCb, true);

      if (result) {
        return result.group;
      } else {
        return { children: [] };
      }
      // Luca's version with warnings fixed
      // const subPanelRoutes = this.props.match.params.subpanel.split('-');
      // let selected = categories.children[0];
      // let startingName = 'App.UserSettingsGroups.Layers.';
      // for (let i = 0; i < subPanelRoutes.length; i++) {
      //   startingName = startingName + subPanelRoutes[i];
      //   selected = selected.children.find(this._findByName.bind(this, startingName));
      //   startingName = startingName + '.';
      // }
      // return selected;
    }

    return { children: [] };
  }

  render() {
    const { match, categories, dictionary } = this.props;
    const selected = this._getGroupByUrl(categories);

    return (
      <div className="panel-settings">
        {categories.children.map((child, index) => (
          <LayersSettingsSelector key={index} match={match} menuItems={child.children} />
        ))}

        <SettingLayerPanelList dictionary={dictionary} selected={selected} />
      </div>
    );
  }
}
export default LayersSettings;
