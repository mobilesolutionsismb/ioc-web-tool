import './style.scss';
import React, { Component } from 'react';
import { RaisedButton } from 'material-ui';
import { compose } from 'redux';
import SettingLayerPanel from './SettingLayerPanel';
import { withLayersAndSettings } from 'js/modules/LayersAndSettings';
import { withLoader } from 'js/modules/ui';
import PoiSettings from './PoiSettings';
import BaseLayerSettings from './BaseLayerSettings';
const enhance = compose(withLoader, withLayersAndSettings);

const Header = props => (
  <div
    style={{
      fontWeight: 'bold',
      padding: '0px 0 10px 0',
      minWidth: 300,
      display: 'inline-flex',
      width: '100%'
    }}
  >
    <div style={{ width: '100%' }}>
      <h3 style={{ marginBottom: 0 }}>{props.title}</h3>
    </div>
    {props.save && (
      <RaisedButton
        label="SAVE"
        primary={true}
        style={{ alignSelf: 'center' }}
        onClick={props.save}
      />
    )}
  </div>
);

class SettingLayerPanelList extends Component {
  _saveSettings = async () => {
    await this.props.updateSettingsOnServer(
      this.props.itemEditedList,
      this.props.loadingStart,
      this.props.loadingStop
    );
  };

  _addSettingToBeUpdated = selectedItem => {
    this.props.addSettingToBeUpdated(selectedItem);
  };

  render() {
    const { selected, itemEditedList, dictionary } = this.props;

    return (
      <div className="layer-setting-panel-list">
        {selected.isBaseLayersSelected ? (
          <Header {...this.props} title="Base Layers" />
        ) : (
          <Header {...this.props} title={selected.displayName} save={this._saveSettings} />
        )}
        {selected.settings ? (
          <SettingLayerPanel
            itemEditedList={itemEditedList}
            addSettingToBeUpdated={this._addSettingToBeUpdated}
            selected={selected}
          />
        ) : selected.children ? (
          selected.children.map((item, index) => (
            <SettingLayerPanel
              itemEditedList={itemEditedList}
              addSettingToBeUpdated={this._addSettingToBeUpdated}
              key={index}
              selected={item}
            />
          ))
        ) : (
          <div />
        )}
        {selected.name === 'App.UserSettingsGroups.Layers.General.Poi' && (
          <PoiSettings dictionary={dictionary} />
        )}
        {selected.isBaseLayersSelected && <BaseLayerSettings dictionary={dictionary} />}
      </div>
    );
  }
}
export default enhance(SettingLayerPanelList);
