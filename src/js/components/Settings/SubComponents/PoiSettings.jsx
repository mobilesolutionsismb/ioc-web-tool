import './style.scss';
import React, { Component } from 'react';
import { Paper, Toggle } from 'material-ui';
import { withMapLayerFilters } from 'js/modules/mapLayerFilters';

const STYLES = {
  card: {
    backgroundColor: '#454545',
    width: '100%',
    padding: '15px 25px',
    marginBottom: 10
  }
};

const MAP_LAYER_FILTER_NAMES = [
  '_is_layer_report_visible',
  '_is_layer_rr_visible',
  '_is_layer_emcomm_visible',
  '_is_layer_mission_visible',
  '_is_layer_agloc_visible',
  '_is_layer_maprequest_visible'
];

const HeaderItem = props => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }}
  >
    <div style={{ padding: '10px 0 20px 0', minWidth: 300 }} />
    <div style={{ padding: '10px 0 20px 0', minWidth: 150, color: '#c7c7c7' }} />
    <div style={{ padding: '10px 0 20px 0', minWidth: 80, color: '#c7c7c7' }}> HIDE/SHOW </div>
  </div>
);

class PoiSettings extends Component {
  _changeShowLayer(e, newToggleValue, layerSetting) {
    if (newToggleValue) {
      this.props.addMapLayerFilter('layersVisibility', layerSetting);
    } else {
      this.props.removeMapLayerFilter('layersVisibility', layerSetting);
    }
  }

  render() {
    const { dictionary } = this.props;

    return (
      <div>
        <Paper style={STYLES.card}>
          <div>
            Local Settings
            <HeaderItem {...this.props} />
            {MAP_LAYER_FILTER_NAMES.map((layerSetting, index) => (
              <div
                key={index}
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  minHeight: '48px'
                }}
                className="layers-settings-configuration-item"
              >
                <div
                  style={{
                    width: 300,
                    color: '#c7c7c7',
                    overflowX: 'hidden',
                    whiteSpace: 'nowrap',
                    textOverflow: 'ellipsis'
                  }}
                >
                  {' '}
                  {dictionary(layerSetting)}{' '}
                </div>
                <div />
                <Toggle
                  style={{ maxWidth: 85 }}
                  toggled={this.props.mapLayerFilters.layersVisibility.indexOf(layerSetting) >= 0}
                  onToggle={(e, isInputChecked) =>
                    this._changeShowLayer(e, isInputChecked, layerSetting)
                  }
                  iconStyle={{ margin: 'auto' }}
                />
              </div>
            ))}
          </div>
        </Paper>
      </div>
    );
  }
}
export default withMapLayerFilters(PoiSettings);
