import React, { Component } from 'react';
import { SelectField, MenuItem, Toggle } from 'material-ui';
import { getDurationFormat } from 'js/utils/localizeDate';

class ItemToModify extends Component {
  _getLeadTimeComponent(item) {
    let result = <div />;
    const editedItem = this.props.itemEditedList.find(a => a.key === item.key);
    let value = editedItem ? editedItem.option : item.value;

    result = (
      <SelectField
        onChange={(e, index, payload) => this._changeLeadTimeSetting(e, index, payload)}
        value={value}
      >
        {item.options.map((option, index) => (
          <MenuItem
            key={index}
            value={option}
            primaryText={<span>{getDurationFormat(option)}</span>}
          />
        ))}
      </SelectField>
    );

    return result;
  }

  _getShowHideComponent(item) {
    let result = <div />;
    const editedItem = this.props.itemEditedList.find(a => a.key === item.key);
    let value = editedItem ? editedItem.option : item.value;

    result = (
      <Toggle
        style={{ maxWidth: 85 }}
        toggled={value === 'Show'}
        onToggle={(e, isInputChecked) => this._changeShowHideSetting(e, isInputChecked)}
        iconStyle={{ margin: 'auto' }}
      />
    );

    return result;
  }

  _changeLeadTimeSetting(e, index, payload) {
    this.props.item.settingDefinition.leadTime.value = this.props.item.settingDefinition.leadTime.options[
      index
    ];
    let selectedItem = {
      key: this.props.item.settingDefinition.leadTime.key,
      option: this.props.item.settingDefinition.leadTime.value
    };
    this.props.addSettingToBeUpdated(selectedItem);
  }

  _changeShowHideSetting(e, isInputChecked) {
    this.props.item.settingDefinition.showHide.value = isInputChecked ? 'Show' : 'Hide';
    let selectedItem = {
      key: this.props.item.settingDefinition.showHide.key,
      option: isInputChecked ? 'Show' : 'Hide'
    };
    this.props.addSettingToBeUpdated(selectedItem);
  }

  render() {
    const { item } = this.props;
    const { displayName } = item.settingDefinition;

    const leadTimeComponent = item.settingDefinition.leadTime ? (
      this._getLeadTimeComponent(item.settingDefinition.leadTime)
    ) : (
      <div />
    );
    const showHideComponent = item.settingDefinition.showHide ? (
      this._getShowHideComponent(item.settingDefinition.showHide)
    ) : (
      <div />
    );

    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          minHeight: '48px'
        }}
        className="layers-settings-configuration-item"
      >
        <div
          style={{
            width: 300,
            color: '#c7c7c7',
            overflowX: 'hidden',
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis'
          }}
        >
          {' '}
          {displayName}{' '}
        </div>
        {leadTimeComponent}
        {showHideComponent}
      </div>
    );
  }
}

export default ItemToModify;
