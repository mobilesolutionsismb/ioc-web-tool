import React, { Component } from 'react';

import './style.scss';
import OperationalProcedureForm from './OperationalProcedureForm';
import { RaisedButton, CircularProgress } from 'material-ui';
import { localizeDate } from 'js/utils/localizeDate';
import { withDictionary } from 'ioc-localization';
import { withMessages } from 'js/modules/ui';
import { withOperationalPlan } from 'js/modules/operationalPlanApi';
import { withEnumsAndRoles, withLogin } from 'ioc-api-interface';
import { compose } from 'redux';
const enhace = compose(
  withEnumsAndRoles,
  withLogin,
  withDictionary,
  withOperationalPlan,
  withMessages
);

class OperationalProcedure extends Component {
  componentDidMount() {
    this.props.getMyProcedures(this.props.user.organizationUnits[0].id);
    this.props.getAntCons();
    this.props.getRoles();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.errorGet && this.props.errorGet) {
      this.props.pushMessage('Error loading data', 'error');
    }
  }
  componentWillUnmount() {
    this.props.restartOP();
  }
  _createOP = () => {
    this.props.history.push('/settings/procedure/new_procedure');
    //to={`/settings/layers/${this.buildUrl(itemChild.name)}`}
  };
  _editOP = id => {
    //await this.props.getProcedureById(id);
    this.props.history.push('/settings/procedure/edit_procedure');
  };
  render() {
    const dict = this.props.dictionary;
    var component;
    switch (this.props.match.params.subpanel) {
      case undefined:
        component = (
          <div className="mainContainer">
            <div className="divContainerTable">
              {this.props.lastProcedure ? (
                this.props.lastProcedure.created === '' ? (
                  <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    {dict._create_op_message}{' '}
                    <RaisedButton
                      style={{ marginLeft: 10, marginTop: 10 }}
                      label={dict._create}
                      onClick={this._createOP}
                      primary={true}
                    />
                  </div>
                ) : (
                  <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    {dict._edit_op_message}{' '}
                    {localizeDate(this.props.lastProcedure.created + '+00:00')}
                    <RaisedButton
                      style={{ marginLeft: 10, marginTop: 10 }}
                      label={dict._edit}
                      onClick={this._editOP}
                      primary={true}
                    />
                  </div>
                )
              ) : (
                !this.props.errorGet && (
                  <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <CircularProgress size={32} />
                  </div>
                )
              )}
            </div>
          </div>
        );
        break;
      case 'new_procedure':
        component = (
          <OperationalProcedureForm
            organization={this.props.user.organizationUnits[0].id}
            history={this.props.history}
          />
        );
        break;
      case 'edit_procedure':
        component = (
          <OperationalProcedureForm
            organization={this.props.user.organizationUnits[0].id}
            edit={true}
            history={this.props.history}
          />
        );
        break;
      default:
        break;
    }
    return component;
  }
}
export default enhace(OperationalProcedure);
