import React, { Component } from 'react';
import {
  SelectField,
  MenuItem,
  RaisedButton,
  TextField,
  IconButton,
  Divider,
  CircularProgress
} from 'material-ui';
import moment from 'moment';
import { withDictionary } from 'ioc-localization';
import { withMessages } from 'js/modules/ui';
import { withOperationalPlan } from 'js/modules/operationalPlanApi';
import { withEnumsAndRoles } from 'ioc-api-interface';
import { compose } from 'redux';
const enhace = compose(
  withEnumsAndRoles,
  withDictionary,
  withOperationalPlan,
  withMessages
);

const STYLES = {
  button: {
    margin: 12
  }
};

class OperationalProcedureForm extends Component {
  state = {
    creationLoading: false,
    isorgLabel: false,
    orgLabel: '',
    compare: [
      { field: '==' },
      { field: '!=' },
      { field: '<' },
      { field: '>' },
      { field: '<=' },
      { field: '>=' }
    ],

    numberOfObjects: {},
    rules: [
      {
        title: '',
        ant: [{ displayName: null, when: null, field: null, compare: '', value: '' }],
        cons: [
          {
            displayName: null,
            what: null,
            who: '',
            message: '',
            mode: null,
            message2: '',
            warningLevel: null
          }
        ]
      }
    ]
  };

  componentDidMount() {
    if (this.props.edit || this.props.template) {
      this.setRules();
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.antCons === undefined &&
      this.props.antCons !== undefined &&
      this.props.antCons.antecedent === undefined
    ) {
      this.props.getAntCons();
    }
  }
  changeTitle = (numRule, event) => {
    let rules = this.state.rules;
    rules[numRule].title = event.target.value;
    this.setState(rules);
  };
  changewhen = (numRule, numAnt, event, index, value) => {
    let rules = this.state.rules;
    rules[numRule].ant[numAnt].when = value;
    rules[numRule].ant[numAnt].field = null;
    rules[numRule].ant[numAnt].value = '';
    rules[numRule].ant[numAnt].compare = '';
    let validMethods = this.props.antCons.consecuent.filter((currentElement, when) => {
      return this._filterWhat(currentElement, rules[numRule].ant);
    });
    let numCons = rules[numRule].cons.length;
    for (let x = 0; x < numCons; x++) {
      let found = validMethods.find(y => y.name === rules[numRule].cons[x].what);
      if (found === undefined) {
        if (x === 0) {
          rules[numRule].cons[x] = {
            what: null,
            who: '',
            message: '',
            mode: null,
            message2: '',
            warningLevel: null
          };
        } else {
          rules[numRule].cons.splice(x, 1);
        }
      }
    }

    this.setState(rules);
  };
  changeField = (numRule, numAnt, event, index, value) => {
    let rules = this.state.rules;
    rules[numRule].ant[numAnt].field = value;

    this.setState(rules);
  };
  changeCompare = (numRule, numAnt, event, index, value) => {
    let rules = this.state.rules;
    rules[numRule].ant[numAnt].compare = value;
    this.setState(rules);
  };
  changewhat = (numRule, numCons, event, index, value) => {
    let rules = this.state.rules;
    rules[numRule].cons[numCons].what = value;
    this.setState(rules);
  };
  changeWith = (numRule, numAnt, event) => {
    let rules = this.state.rules;
    rules[numRule].ant[numAnt].value = event.target.value;
    this.setState(rules);
  };
  changeWho = (numRule, numCons, event, index, value) => {
    let rules = this.state.rules;
    rules[numRule].cons[numCons].who = value;
    this.setState(rules);
  };
  changeMessage = (numRule, numCons, event) => {
    let rules = this.state.rules;
    rules[numRule].cons[numCons].message = event.target.value;
    this.setState(rules);
  };
  changeMessage2 = (numRule, numCons, event) => {
    let rules = this.state.rules;
    rules[numRule].cons[numCons].message2 = event.target.value;
    this.setState(rules);
  };
  changeMode = (numRule, numCons, event, index, value) => {
    let rules = this.state.rules;
    rules[numRule].cons[numCons].mode = value;
    this.setState(rules);
  };
  changeWarningLevel = (numRule, numCons, event, index, value) => {
    let rules = this.state.rules;
    rules[numRule].cons[numCons].warningLevel = value;
    this.setState(rules);
  };

  _isValidRule(rule) {
    if (rule.ant[0].when != null && rule.cons[0] != null) {
      return true;
    } else {
      return false;
    }
  }
  _addRule = () => {
    const numberOfRules = this.state.rules.length - 1;
    if (this._isValidRule(this.state.rules[numberOfRules])) {
      let rules = this.state.rules;
      rules.push({
        title: '',
        ant: [{ when: null, field: null, compare: '', value: '' }],
        cons: [{ what: null, who: '', message: '', mode: null, message2: '', warningLevel: null }]
      });
      this.setState(rules);
    }
  };

  _isValidAnt(ant) {
    if (ant.when !== null && ant.field !== null && ant.compare !== '' && ant.value !== '')
      return true;
    else return false;
  }
  _validAntCharacters(ant) {
    if (ant.value.includes('@')) return false;
    else return true;
  }
  _isValidCons(cons) {
    let method = this.props.antCons.consecuent.find(x => x.name === cons.what);
    if (method !== undefined) {
      let num = method.parameters.length;
      let ret;
      if (cons.what !== null) {
        switch (num) {
          case 6:
            if (
              cons.who !== '' &&
              cons.message !== '' &&
              cons.mode !== null &&
              cons.message2 !== ''
            ) {
              ret = true;
            } else {
              ret = false;
            }
            break;
          case 5:
            if (
              cons.who !== '' &&
              cons.message !== '' &&
              cons.mode !== null &&
              cons.warningLevel !== null
            ) {
              ret = true;
            } else {
              ret = false;
            }
            break;
          case 4:
            if (cons.who !== '' && cons.message !== '' && cons.mode !== null) {
              ret = true;
            } else {
              ret = false;
            }
            break;
          case 3:
            if (cons.who !== '' && cons.mode !== null) {
              ret = true;
            } else {
              ret = false;
            }
            break;
          default:
            break;
        }
        return ret;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  _validConsCharacters(cons) {
    let method = this.props.antCons.consecuent.find(x => x.name === cons.what);
    if (method !== undefined) {
      let num = method.parameters.length;
      let ret;
      if (cons.what !== null) {
        switch (num) {
          case 6:
            if (cons.message.includes('@') || cons.message2.includes('@')) {
              ret = true;
            } else {
              ret = false;
            }
            break;
          case 5:
            if (cons.message.includes('@')) {
              ret = true;
            } else {
              ret = false;
            }
            break;
          case 4:
            if (cons.message.includes('@')) {
              ret = true;
            } else {
              ret = false;
            }
            break;
          case 3:
            ret = true;

            break;
          default:
            break;
        }
        return ret;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  _addAnt = numRule => {
    const numberOfAnts = this.state.rules[numRule].ant.length - 1;
    if (this._isValidAnt(this.state.rules[numRule].ant[numberOfAnts])) {
      let rules = this.state.rules;
      rules[numRule].ant.push({ when: null, field: null, compare: '', value: '' });
      this.setState(rules);
    }
  };
  _addCons = numRule => {
    const numberOfAnts = this.state.rules[numRule].cons.length - 1;
    if (this._isValidCons(this.state.rules[numRule].cons[numberOfAnts])) {
      let rules = this.state.rules;
      rules[numRule].cons.push({
        what: null,
        who: '',
        message: '',
        mode: null,
        message2: '',
        warningLevel: null
      });
      this.setState(rules);
    }
  };
  createObjectToSend() {
    return {
      package: ' com.drools.optimizer',
      import: [
        { library_0: ' com.drools.optimizer.AppExpertSystem;' },
        { library_1: ' com.drools.utils.Event;' },
        { library_2: ' com.drools.utils.Message;' },
        { library_3: ' com.answare.dataModel.*;' },
        { library_4: ' com.answare.ireact.datamodel.*;' },
        { library_5: ' java.util.Date;' },
        { library_6: ' com.answare.ireact.datamodel.Report;' },
        { library_7: ' com.answare.ireact.datamodel.Notification;' },
        { library_8: ' com.answare.ireact.datamodel.EmergencyEvent;' },
        { library_9: ' com.answare.ireact.datamodel.Receiver;' },
        { library_10: ' com.answare.ireact.datamodel.FWILayers;' },
        { library_11: ' com.answare.ireact.datamodel.ARPA_Hydrogeological_Alert;' },
        { library_12: ' com.answare.ireact.datamodel.UK_Flood_Risk;' },
        { library_13: ' answare.azureConnection.MessageBrokerNewExternalData;' },
        { library_14: ' com.drools.utils.PastEvent;' },
        { library_15: ' java.util.Queue;' },
        { library_16: ' java.util.LinkedList;' }
      ],
      global: ' com.drools.optimizer.DSSEngine dssEngine;',
      rules: []
    };
  }

  _validateform = () => {
    let ret = true;
    this.state.rules.forEach(element => {
      if (element.title === '') {
        this.props.pushMessage('Rules must be title', 'error');
        ret = false;
        return;
      }
      if (element.title.includes('@')) {
        this.props.pushMessage('character @ is not allowed on title', 'error');
        ret = false;
        return;
      }
      let errorFound = false;
      element.ant.forEach(ant => {
        if (!errorFound) {
          if (!this._isValidAnt(ant)) {
            this.props.pushMessage('Antecedents must be value', 'error');
            ret = false;
            errorFound = true;
            return;
          }
          if (!this._validAntCharacters(ant)) {
            this.props.pushMessage('character @ is not allowed on Antecedents', 'error');
            ret = false;
            errorFound = true;
            return;
          }
        }
      });
      if (errorFound) {
        return;
      }

      errorFound = false;
      element.cons.forEach(cons => {
        if (!errorFound) {
          if (!this._isValidCons(cons)) {
            this.props.pushMessage('Consequents must be value', 'error');
            ret = false;
            return;
          }
        }
        if (this._validConsCharacters(cons)) {
          this.props.pushMessage('character @ is not allowed on Consecuents', 'error');
          ret = false;
          errorFound = true;
          return;
        }
      });
    });

    return ret;
  };
  getClassName = string => {
    let type = string.substring(1);
    let lastCharacter = type.substring(type.length - 1);
    if (isNaN(lastCharacter)) {
      return type;
    } else {
      return type.slice(0, -1);
    }
  };
  setRules = () => {
    if (
      this.props.lastProcedure === undefined ||
      this.props.lastProcedure.value === undefined ||
      this.props.lastProcedure.value.rules === undefined
    ) {
      this.props.history.push('/settings/procedure');
    } else {
      let rules = this.props.lastProcedure.value.rules;
      let rulesToForm = [];
      rules.forEach(rule => {
        let auxRule = {
          ant: [],
          cons: []
        };
        auxRule.title = rule.rule.replace(/@/g, '');
        rule.when.forEach(when => {
          let auxAnt = {};
          let a = Object.getOwnPropertyNames(when);
          let split, type, auxwhen;
          if (when[a].split('@,').length === 2) {
            //one method with 2 things to compare: $uk_flood_risk: UK_Flood_Risk( area_name==@York@, rivers==@segura@)
            let auxAnt2 = {};
            split = when[a].split(':');
            type = this.getClassName(split[0]);
            auxwhen = this.props.antCons.antecedent.find(x =>
              this._getWhenKey(x)
                .toLocaleLowerCase()
                .includes(type)
            );
            auxAnt.when = this._getWhenKey(auxwhen);

            auxAnt2.when = this._getWhenKey(auxwhen);

            let part1 = when[a].split(' ')[2].split('@');

            auxAnt.compare = this._getCompare(part1[0]);
            auxAnt.field = ` ${part1[0].replace(auxAnt.compare, '')}`;
            auxAnt.value = part1[1];
            auxRule.ant.push(auxAnt);

            let part2 = when[a].split('@,')[1];

            auxAnt2.compare = this._getCompare(part2.split('@')[0]);
            auxAnt2.field = ` ${part2
              .split('@')[0]
              .split(auxAnt2.compare)[0]
              .substring(1)}`;
            auxAnt2.value = part2.split(auxAnt2.compare)[1].replace(/@/g, '');
            auxAnt2.value = auxAnt2.value.substring(0, auxAnt2.value.length - 1);
            auxRule.ant.push(auxAnt2);
          } else {
            split = when[a].split(':');

            type = this.getClassName(split[0]);
            auxwhen = this.props.antCons.antecedent.find(x =>
              this._getWhenKey(x)
                .toLocaleLowerCase()
                .includes(type)
            );
            auxAnt.when = this._getWhenKey(auxwhen);
            let parts = split[1].split('@');
            auxAnt.compare = this._getCompare(parts[0]);
            auxAnt.field = parts[0].split('(')[1].replace(auxAnt.compare, '');
            auxAnt.value = parts[1];
            auxRule.ant.push(auxAnt);
          }
        });
        rule.then.forEach(then => {
          let auxCons = {};
          let a = Object.getOwnPropertyNames(then);

          if (!then[a].includes('retract')) {
            let split = then[a].split('(');
            auxCons.what = ` ${split[0].split('.')[1]}`;
            let args = split[1].split(',@');
            switch (args.length) {
              case 6:
                auxCons.message = args[1].split('@')[0];
                auxCons.message2 = args[2].replace(/@/g, '');
                auxCons.mode = `@${args[4]}`;
                auxCons.who = this._getRoleIdByName(args[5].replace('@);', ''));
                break;
              case 5:
                if (args[1].substring(args[1].length - 1) === '@') {
                  //case 5
                  auxCons.message = args[1].replace(/@/g, '');
                  auxCons.mode = `@${args[2]}`;
                  auxCons.who = this._getRoleIdByName(args[3].replace(/@/g, ''));
                  auxCons.warningLevel = `@${args[4].replace(');', '')}`;
                  auxCons.message2 = '';
                } else {
                  //case 5
                  auxCons.message = args[1].split('@')[0];
                  auxCons.message2 = args[2].replace(/@/g, '');
                  auxCons.mode = `@${args[3]}`;
                  auxCons.who = this._getRoleIdByName(args[4].replace('@);', ''));
                }

                break;
              case 4:
                auxCons.message = args[1].replace(/@/g, '');
                auxCons.mode = `@${args[2]}`;
                auxCons.who = this._getRoleIdByName(args[3].replace('@);', ''));
                auxCons.message2 = '';
                break;
              case 3:
                break;
              default:
                break;
            }
            auxRule.cons.push(auxCons);
          }
        });
        rulesToForm.push(auxRule);
      });
      this.setState({ rules: rulesToForm });
    }
  };
  _getCompare = string => {
    let ret;
    if (string.includes('!=')) {
      ret = '!=';
    } else if (string.includes('==')) {
      ret = '==';
    } else if (string.includes('<=')) {
      ret = '<=';
    } else if (string.includes('>=')) {
      ret = '>=';
    } else if (string.includes('<')) {
      ret = '<';
    } else if (string.includes('>')) {
      ret = '>';
    }
    return ret;
  };
  _createActionPlan = async () => {
    if (this._validateform()) {
      this.setState({ creationLoading: true });
      try {
        let send = this.createObjectToSend();

        this.state.rules.forEach(rule => {
          let numberOfObjects = {};
          const _numberojObject = type => {
            let ret = '';
            if (numberOfObjects[type] !== undefined) {
              ret = numberOfObjects[type];
              numberOfObjects[type]++;
            } else {
              numberOfObjects[type] = 1;
            }
            return ret;
          };
          let objects = [];
          var arule = {};
          arule.rule = `@${rule.title}@`;
          arule.when = [];
          arule.then = [];
          let antIndex = 0;
          let consIndex = 0;
          let addValue = [];
          if (rule.ant.length > 1 && rule.ant[0].when === rule.ant[1].when) {
            let ante = '';
            let objaux;
            let classname = rule.ant[0].when.split('.')[4];
            objaux = `$${classname.toLowerCase()}${_numberojObject(rule.ant[0].when)}`;
            ante += `${objaux}: ${classname}(`;
            objects.push(objaux);

            ante += `${rule.ant[0].field}${rule.ant[0].compare}@${rule.ant[0].value}@,${
              rule.ant[1].field
            }${rule.ant[1].compare}@${rule.ant[1].value}@)`;

            let auxbj = {};
            auxbj[`clausula_${antIndex}`] = ante;
            arule.when.push(auxbj);
            antIndex++;
          } else {
            rule.ant.forEach(ant => {
              let ante = '';
              let objaux;
              let classname = ant.when.split('.')[4];
              objaux = `$${classname.toLowerCase()}${_numberojObject(ant.when)}`;
              ante += `${objaux}: ${classname}(`;
              objects.push(objaux);

              ante += `${ant.field}${ant.compare}@${ant.value}@)`;

              let auxbj = {};
              auxbj[`clausula_${antIndex}`] = ante;
              arule.when.push(auxbj);
              antIndex++;
            });
          }

          rule.cons.forEach(cons => {
            let selectedconsecuent = this.props.antCons.consecuent.find(x => x.name === cons.what);
            let conse = `dssEngine.${cons.what.substr(1)}(`;
            let selectobject;
            if (selectedconsecuent.addValue) addValue.push(selectedconsecuent.addValue);
            let classnamecons = selectedconsecuent.parameters[0].type.split('.')[4].toLowerCase();
            conse += `$${classnamecons}`;
            selectobject = `$${classnamecons}`;

            switch (selectedconsecuent.parameters.length) {
              case 3:
                conse += `,${cons.mode},@${this._getRoleNameById(cons.who)}@);`;
                break;
              case 4:
                conse += `,@${cons.message}@,${cons.mode},@${this._getRoleNameById(cons.who)}@);`;
                break;
              case 5:
                conse += `,@${cons.message}@,${cons.mode},@${this._getRoleNameById(cons.who)}@,${
                  cons.warningLevel
                });`;
                break;
              case 6:
                let sameObjects = objects.filter(x => x.indexOf(selectobject) > -1);
                conse += `,@${cons.message}@,${sameObjects[sameObjects.length - 1]},@${
                  cons.message2
                }@,${cons.mode},@${this._getRoleNameById(cons.who)}@);`;
                break;
              default:
                break;
            }

            let auxbj = {};
            auxbj[`action_${consIndex}`] = conse;

            arule.then.push(auxbj);

            consIndex++;
          });
          if (addValue.length === 2) {
            if (addValue[0] === addValue[1]) {
              let auxbj2 = {};
              auxbj2[`action_${consIndex}`] = addValue[0];
              arule.then.push(auxbj2);
            } else {
              let auxbj2 = {};
              auxbj2[`action_${consIndex}`] = addValue[0];
              arule.then.push(auxbj2);
              let auxbj3 = {};
              auxbj3[`action_${consIndex + 1}`] = addValue[1];
              arule.then.push(auxbj3);
            }
          } else if (addValue.length === 1) {
            let auxbj2 = {};
            auxbj2[`action_${consIndex}`] = addValue[0];
            arule.then.push(auxbj2);
          }

          send.rules.push(arule);
        });
        //console.log(JSON.stringify(send));
        await this.props.createRule(
          send,
          moment.utc().format('YYYY-MM-DD HH:mm:ss'),
          this.props.organization
        );
        this.setState({ creationLoading: false });
        this.props.pushMessage('Operational plan created succesfully', 'success', null, null);
        this.props.history.push('/settings/procedure');
      } catch (error) {
        this.setState({ creationLoading: false });
        this.props.pushMessage('Create operational plan Error', 'error');
      }
    }
  };
  _getWhen1MenuItem = id => {
    let key = this._getWhenKey(id);
    return <MenuItem key={key} value={key} primaryText={id.displayName} />;
  };
  _getFieldMenuItem = (currElement, index) => {
    return <MenuItem key={index} value={currElement.name} primaryText={currElement.name} />;
  };
  _getCompareMenuItem = id => {
    return (
      <MenuItem
        key={id}
        value={this.state.compare[id].field}
        primaryText={this.state.compare[id].field}
      />
    );
  };
  _getWhatMenuItem = obj => {
    return <MenuItem key={obj.name} value={obj.name} primaryText={obj.displayName} />;
  };
  _filterWhat = (obj, ants) => {
    if (obj.parameters.length > 0) {
      let argName = obj.parameters[0].type.split(' ')[1];
      let found = ants.find(x => x.when === argName);
      if (found !== undefined || obj.parameters[0].type === 'class java.lang.String') return obj;
    } else {
      return obj;
    }
  };
  _filterWhatToDisplay = (obj, ants, numRule, numCons) => {
    let consToCompare = numCons === 0 ? 1 : 0;

    if (obj.parameters.length > 0) {
      let argName = obj.parameters[0].type.split(' ')[1];
      let found = ants.find(x => x.when === argName);

      if (found !== undefined || obj.parameters[0].type === 'class java.lang.String') {
        if (
          this.state.rules[numRule].cons.length === 1 ||
          obj.name !== this.state.rules[numRule].cons[consToCompare].what
        )
          return obj;
      }
    } else {
      return obj;
    }
  };
  _getWhoMenuItem = (role, index) => {
    return <MenuItem key={index} value={role.id} primaryText={role.displayName} />;
  };
  _getRoleNameById = id => {
    let role = this.props.roles.find(x => x.id === id);
    return role.displayName;
  };
  _getRoleIdByName = name => {
    let role = this.props.roles.find(x => x.name === name || x.displayName === name);
    return role.id;
  };
  _deleteRule = numRule => {
    if (this.state.rules.length > 1) {
      let rules = this.state.rules;
      rules.splice(numRule, 1);
      this.setState(rules);
    }
  };
  _deleteAnt = (numRule, numAnt) => {
    if (this.state.rules[numRule].ant.length === 2) {
      let rules = this.state.rules;
      rules[numRule].ant.splice(numAnt, 1);
      this.setState(rules);
    } else if (this.state.rules[numRule].ant.length === 1) {
      let rules = this.state.rules;
      rules[numRule].ant[numAnt] = { when: null, field: null, compare: '', value: '' };
      this.setState(rules);
    }
  };
  _deleteCons = (numRule, numCons) => {
    if (this.state.rules[numRule].cons.length === 2) {
      let rules = this.state.rules;
      rules[numRule].cons.splice(numCons, 1);
      this.setState(rules);
    } else if (this.state.rules[numRule].cons.length === 1) {
      let rules = this.state.rules;
      rules[numRule].cons[numCons] = {
        what: null,
        who: '',
        message: '',
        mode: null,
        message2: '',
        warningLevel: null
      };
      this.setState(rules);
    }
  };
  _getWhenKey = antecedent => {
    let keys = Object.keys(antecedent);
    let key = keys.find(x => x !== 'displayName');
    return key;
  };
  _getClassfields = when => {
    return this.props.antCons.antecedent.find(x => this._getWhenKey(x) === when)[when];
  };
  _areMoreMethods = rule => {
    let validMethods = this.props.antCons.consecuent.filter((currentElement, when) => {
      return this._filterWhat(currentElement, rule.ant);
    });
    return rule.cons.length === 1 && validMethods.length > 1;
  };
  generateWhatSection(rule, numRule, numCons, what) {
    let num = this.props.antCons.consecuent.find(x => x.name === what).parameters.length;
    let field = null;
    switch (num) {
      case 3:
        field = (
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Who'}
              onChange={(event, index, value) =>
                this.changeWho(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].who}
            >
              {this.props.roles.map((currElement, index) => {
                return this._getWhoMenuItem(currElement, index);
              })}
            </SelectField>
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Mode'}
              onChange={(event, index, value) =>
                this.changeMode(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].mode}
            >
              <MenuItem key={0} value={'@suggested@'} primaryText={'suggested'} />
              <MenuItem key={1} value={'@validated@'} primaryText={'validated'} />
            </SelectField>
          </div>
        );
        break;
      case 4:
        field = (
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <TextField
              floatingLabelText="Message to send"
              style={STYLES.button}
              hintText="Message to send"
              value={rule.cons[numCons].message}
              onChange={event => this.changeMessage(numRule, numCons, event)}
            />
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Who'}
              onChange={(event, index, value) =>
                this.changeWho(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].who}
            >
              {this.props.roles.map((currElement, index) => {
                return this._getWhoMenuItem(currElement, index);
              })}
            </SelectField>
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Mode'}
              onChange={(event, index, value) =>
                this.changeMode(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].mode}
            >
              <MenuItem key={0} value={'@suggested@'} primaryText={'suggested'} />
              <MenuItem key={1} value={'@validated@'} primaryText={'validated'} />
            </SelectField>
          </div>
        );
        break;
      case 5:
        field = (
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <TextField
              floatingLabelText="Message to send"
              style={STYLES.button}
              hintText="Message to send"
              value={rule.cons[numCons].message}
              onChange={event => this.changeMessage(numRule, numCons, event)}
            />
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Who'}
              onChange={(event, index, value) =>
                this.changeWho(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].who}
            >
              {this.props.roles.map((currElement, index) => {
                return this._getWhoMenuItem(currElement, index);
              })}
            </SelectField>
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Mode'}
              onChange={(event, index, value) =>
                this.changeMode(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].mode}
            >
              <MenuItem key={0} value={'@suggested@'} primaryText={'suggested'} />
              <MenuItem key={1} value={'@validated@'} primaryText={'validated'} />
            </SelectField>
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Warning level'}
              onChange={(event, index, value) =>
                this.changeWarningLevel(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].warningLevel}
            >
              <MenuItem key={0} value={'@moderate@'} primaryText={'Moderate'} />
              <MenuItem key={1} value={'@severe@'} primaryText={'Severe'} />
              <MenuItem key={2} value={'@extreme@'} primaryText={'Extreme'} />
            </SelectField>
          </div>
        );
        break;
      case 6:
        field = (
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <TextField
              floatingLabelText="Message one to send"
              style={STYLES.button}
              hintText="Message one to send"
              value={rule.cons[numCons].message}
              onChange={event => this.changeMessage(numRule, numCons, event)}
            />
            <TextField
              floatingLabelText="Message two to send"
              style={STYLES.button}
              hintText="Message two to send"
              value={rule.cons[numCons].message2}
              onChange={event => this.changeMessage2(numRule, numCons, event)}
            />
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Who'}
              onChange={(event, index, value) =>
                this.changeWho(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].who}
            >
              {this.props.roles.map((currElement, index) => {
                return this._getWhoMenuItem(currElement, index);
              })}
            </SelectField>
            <SelectField
              style={STYLES.button}
              floatingLabelText={'Mode'}
              onChange={(event, index, value) =>
                this.changeMode(numRule, numCons, event, index, value)
              }
              value={rule.cons[numCons].mode}
            >
              <MenuItem key={0} value={'@suggested@'} primaryText={'suggested'} />
              <MenuItem key={1} value={'@validated@'} primaryText={'validated'} />
            </SelectField>
          </div>
        );
        break;

      default:
        break;
    }
    return field;
  }
  render() {
    const dict = this.props.dictionary;
    return this.props.antCons !== undefined && this.props.antCons.antecedent ? (
      <div className="operational-settings">
        {this.state.rules.map((rule, numRule) => (
          <div key={numRule} style={{ border: '1px gray solid' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <TextField
                floatingLabelText={dict._title}
                style={STYLES.button}
                hintText={dict._title}
                value={rule.title}
                onChange={event => this.changeTitle(numRule, event)}
              />
              <div>
                <IconButton>
                  <i
                    className="material-icons md-18 md-light"
                    onClick={event => this._deleteRule(numRule)}
                  >
                    close
                  </i>
                </IconButton>
              </div>
            </div>
            <div style={{ display: 'flex', marginLeft: 10 }}>
              <p>{dict._antecedents}</p>
              {rule.ant.length <= 1 && (
                <IconButton>
                  <i
                    className="material-icons md-18 md-light"
                    onClick={event => this._addAnt(numRule)}
                  >
                    add
                  </i>
                </IconButton>
              )}
            </div>
            <Divider style={{ marginLeft: 10, marginRight: 10 }} />
            <div style={{ marginLeft: 20 }}>
              {rule.ant.map((ant, numAnt) => (
                <div key={numAnt} style={{ display: 'flex' }}>
                  <SelectField
                    style={STYLES.button}
                    floatingLabelText={'When'}
                    onChange={(event, index, value) =>
                      this.changewhen(numRule, numAnt, event, index, value)
                    }
                    value={ant.when}
                  >
                    {this.props.antCons.antecedent.map(this._getWhen1MenuItem)}
                  </SelectField>

                  {ant.when !== null && (
                    <SelectField
                      style={STYLES.button}
                      floatingLabelText={'Field'}
                      onChange={(event, index, value) =>
                        this.changeField(numRule, numAnt, event, index, value)
                      }
                      value={ant.field}
                    >
                      {this._getClassfields(ant.when).map((currElement, index) => {
                        return this._getFieldMenuItem(currElement, index);
                      })}
                    </SelectField>
                  )}
                  {ant.field !== null &&
                    ant.field !== 'new' && (
                      <SelectField
                        style={{ margin: 12, width: 100 }}
                        floatingLabelText={'Compare'}
                        onChange={(event, index, value) =>
                          this.changeCompare(numRule, numAnt, event, index, value)
                        }
                        value={ant.compare}
                      >
                        {Object.keys(this.state.compare).map(this._getCompareMenuItem)}
                      </SelectField>
                    )}
                  {ant.field !== null &&
                    ant.field !== 'new' && (
                      <TextField
                        floatingLabelText="With"
                        style={STYLES.button}
                        hintText="With"
                        value={ant.value}
                        onChange={event => this.changeWith(numRule, numAnt, event)}
                      />
                    )}
                  <div>
                    <IconButton>
                      <i
                        className="material-icons md-18 md-light"
                        onClick={event => this._deleteAnt(numRule, numAnt)}
                      >
                        close
                      </i>
                    </IconButton>
                  </div>
                </div>
              ))}
            </div>
            <div style={{ display: 'flex', marginLeft: 10 }}>
              <p>{dict._consequents}</p>
              {rule.cons.length <= 1 &&
                this._areMoreMethods(rule) && (
                  <IconButton>
                    <i
                      className="material-icons md-18 md-light"
                      onClick={event => this._addCons(numRule)}
                    >
                      add
                    </i>
                  </IconButton>
                )}
            </div>
            <Divider style={{ marginLeft: 10, marginRight: 10 }} />
            <div style={{ marginLeft: 20 }}>
              {rule.cons.map((cons, numCons) => (
                <div key={numCons} style={{ display: 'flex' }}>
                  {rule.ant[0].when !== null ? (
                    <SelectField
                      style={STYLES.button}
                      floatingLabelText={'What to do'}
                      onChange={(event, index, value) =>
                        this.changewhat(numRule, numCons, event, index, value)
                      }
                      value={cons.what}
                    >
                      {this.props.antCons.consecuent
                        .filter((currentElement, when) => {
                          return this._filterWhatToDisplay(
                            currentElement,
                            rule.ant,
                            numRule,
                            numCons
                          );
                        })
                        .map(this._getWhatMenuItem)}
                    </SelectField>
                  ) : (
                    <SelectField
                      style={STYLES.button}
                      floatingLabelText={'What to do'}
                      disabled={true}
                      onChange={(event, index, value) =>
                        this.changewhat(numRule, numCons, event, index, value)
                      }
                      value={cons.what}
                    />
                  )}

                  {cons.what && this.generateWhatSection(rule, numRule, numCons, cons.what)}
                  <div>
                    <IconButton>
                      <i
                        className="material-icons md-18 md-light"
                        onClick={event => this._deleteCons(numRule, numCons)}
                      >
                        close
                      </i>
                    </IconButton>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ))}

        <div style={{ margin: 12, display: 'flex' }}>
          <RaisedButton
            style={{ marginRight: 10 }}
            label={dict._add_rule}
            onClick={this._addRule}
            primary={true}
          />
          <RaisedButton
            disabled={this.state.creationLoading}
            label={this.props.edit ? dict._edit_op : dict._create_op}
            onClick={this._createActionPlan}
            secondary={true}
            style={{ marginRight: 5 }}
          />
          {this.state.creationLoading && <CircularProgress size={32} />}
        </div>
      </div>
    ) : (
      <div />
    );
  }
}
export default enhace(OperationalProcedureForm);
