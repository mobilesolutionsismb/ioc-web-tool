import React, { Component } from 'react';
import { FlatButton, RaisedButton } from 'material-ui';
import { Step, Stepper, StepButton, StepContent, StepLabel } from 'material-ui/Stepper';
import { withReports, withReportRequests } from 'ioc-api-interface';
import { withLoader, withMessages } from 'js/modules/ui';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { compose } from 'redux';
import { withReportNewRequest } from 'js/modules/reportNewRequest';
import { InputAOIStep, TimeWindowStep, ContentTypeStep } from 'js/components/Steps';
import ReporterCategories from 'js/components/app/drawer/components/ReporterCategories';
import {
  AOILabel,
  TimeLabel,
  ContentTypeLabel,
  ReportersAllowedLabel
} from 'js/components/app/drawer/components/commons';
import EmCommVisibilityStep from 'js/components/Steps/EmCommVisibilityStep';
import { VisibilityLabel } from '../EmergencyCommunication/commons';

const enhance = compose(
  withReports,
  withLoader,
  withLeftDrawer,
  withMessages,
  withReportNewRequest,
  withReportRequests
);

class ReportNewRequest extends Component {
  state = {
    stepIndex: 0
  };

  componentDidMount() {
    if (!this.props.categories || this.props.categories.length === 0) {
      this.props.getMeasuresByCategory(this.props.loadingStart, this.props.loadingStop);
    }

    if (this.props.newReportRequest.id > 0)
      this.setState({
        stepIndex: 1
      });
  }

  togglePrimary = drawerType => {
    this.props.setOpenSecondary(false);
    this.props.setOpenPrimary(true, drawerType);
  };

  handleNext = () => {
    const { stepIndex } = this.state;
    if (stepIndex < 4) {
      this.setState({
        stepIndex: stepIndex + 1
      });
    }
  };

  handlePrev = () => {
    const { stepIndex } = this.state;
    if (stepIndex > 0) {
      this.setState({
        stepIndex: stepIndex - 1
      });
    }
  };

  renderStepActions(step, canBeDisabled) {
    return (
      <div style={{ margin: '12px 0' }}>
        {step < 3 && (
          <RaisedButton
            label="Next"
            disableTouchRipple={true}
            disableFocusRipple={true}
            primary={true}
            onClick={this.handleNext}
            style={{ marginRight: 12 }}
          />
        )}
        {step > 0 && (
          <FlatButton
            label="Back"
            disableTouchRipple={true}
            disableFocusRipple={true}
            onClick={this.handlePrev}
          />
        )}
      </div>
    );
  }

  //#region TimeWindow
  handleStartChange = (event, startDate) => {
    this.props.setReportNewRequestStartDate(startDate);
  };

  handleStartTimeChange = (event, startTime) => {
    this.props.setReportNewRequestStartTime(startTime);
  };

  handleEndChange = (event, endDate) => {
    this.props.setReportNewRequestEndDate(endDate);
  };

  handleEndTimeChange = (event, endTime) => {
    this.props.setReportNewRequestEndTime(endTime);
  };
  //#endregion

  //#region Content Type
  handleContentTypeChange = event => {
    this.props.setContentType(event.currentTarget.value);
  };

  handleContentTypeCategoryChange = (value, key) => {
    this.props.setContentCategory(value);
  };

  handleContentTypeCategoryRemove = () => {
    this.props.removeContentCategory();
  };

  handleContentTypeMeasureChange = (values, key) => {
    this.props.setContentMeasures(values);
  };
  //#endregion

  //#region Reporters Allowed
  handleReportersAllowedChange = event => {
    this.props.setReportersAllowed(parseInt(event.currentTarget.value, 10));
  };

  setReportRequestVisibility = visibility => {
    this.props.setVisibility(visibility);
  };

  handleReportersAllowedTeamChange = (event, key, values) => {
    this.props.setReportersAllowedTeams(values);
  };

  handleReportersAllowedLevelChange = (event, key, payload) => {
    this.props.setReportersAllowedLevel(payload);
  };
  //#endregion

  render() {
    let {
      dictionary,
      newReportRequest,
      roles,
      enums,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory,
      getMapDraw
    } = this.props;

    const drawProps = {
      getMapDraw,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory
    };

    const { stepIndex } = this.state;
    let categoriesDistinc = Array.from(new Set(this.props.categories.map(item => item.category)));
    let measureList = newReportRequest.categoryType
      ? this.props.categories
          .filter(item => item.category === newReportRequest.categoryType.value)
          .map(function(item) {
            item.value = item.id;
            return item;
          })
      : [];

    const address = newReportRequest.address
      ? newReportRequest.address
      : drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const StepAOIChild = <AOILabel text={address} dictionary={dictionary} />;
    const StepTimeChild = (
      <TimeLabel
        startDate={newReportRequest.startDate}
        startTime={newReportRequest.startTime}
        endDate={newReportRequest.endDate}
        endTime={newReportRequest.endTime}
        dictionary={dictionary}
        locale={this.props.locale}
      />
    );

    const timeWindowStep = (
      <TimeWindowStep
        isDisabledStart={newReportRequest.id !== 0}
        startDate={newReportRequest.startDate}
        startTime={newReportRequest.startTime}
        endDate={newReportRequest.endDate}
        endTime={newReportRequest.endTime}
        dictionary={dictionary}
        locale={this.props.locale}
        handleStartChange={this.handleStartChange}
        handleStartTimeChange={this.handleStartTimeChange}
        handleEndChange={this.handleEndChange}
        handleEndTimeChange={this.handleEndTimeChange}
        renderStepActions={this.renderStepActions(1)}
      />
    );

    const StepReporterAllowedChild = (
      <ReportersAllowedLabel
        roles={roles}
        reportersAllowed={newReportRequest.reportersAllowed}
        dictionary={dictionary}
      />
    );

    const visibilityStep = (
      <EmCommVisibilityStep
        visibility={newReportRequest.visibility}
        dictionary={dictionary}
        setEmCommVisibility={this.setReportRequestVisibility}
        visibilityList={enums.visibility}
        renderStepActions={this.renderStepActions(2)}
      />
    );
    const visibilityLabel = (
      <VisibilityLabel visibility={newReportRequest.visibility} dictionary={dictionary} />
    );

    const reportersAllowed = (
      <ReporterCategories
        roles={roles}
        isDisabled={newReportRequest.id !== 0}
        reportersAllowed={newReportRequest.reportersAllowed}
        teams={newReportRequest.teams}
        minimumLevel={newReportRequest.minimumLevel}
        handleReportersAllowedTeamChange={this.handleReportersAllowedTeamChange}
        handleReportersAllowedLevelChange={this.handleReportersAllowedLevelChange}
        handleReportersAllowedChange={this.handleReportersAllowedChange}
        dictionary={dictionary}
        levelTypes={this.props.enums.levelTypes}
        renderStepActions={this.renderStepActions(2, true)}
      />
    );

    const stepContentChild = (
      <InputAOIStep
        isDisabled={newReportRequest.id !== 0}
        category={this.props.category}
        address={address}
        renderStepActions={this.renderStepActions(0)}
        {...drawProps}
        resetCategory="base"
      />
    );

    const StepContentTypeChild = (
      <ContentTypeLabel contentType={newReportRequest.contentType} dictionary={dictionary} />
    );
    const contentTypeStep = (
      <ContentTypeStep
        isDisabled={newReportRequest.id !== 0}
        dictionary={dictionary}
        contentType={newReportRequest.contentType}
        renderStepActions={this.renderStepActions(3)}
        measures={newReportRequest.measures}
        handleContentTypeChange={this.handleContentTypeChange}
        categoryType={newReportRequest.categoryType}
        handleContentTypeMeasureChange={this.handleContentTypeMeasureChange}
        handleContentTypeCategoryChange={this.handleContentTypeCategoryChange}
        showCategories={true}
        showMeasures={true}
        handleContentTypeCategoryRemove={this.handleContentTypeCategoryRemove}
        categoryList={categoriesDistinc.map(function(item) {
          return {
            value: item,
            name: item.toUpperCase()
          };
        })}
        measureList={measureList}
      />
    );

    let isStep3Completed =
      (newReportRequest.contentType.indexOf('measure') >= 0 &&
        newReportRequest.measures.length > 0) ||
      (newReportRequest.contentType.indexOf('measure') === -1 &&
        newReportRequest.contentType.length > 0);

    return (
      <section>
        <Stepper activeStep={stepIndex} linear={false} orientation="vertical">
          <Step completed={stepIndex > 0 && drawPolygon && drawPolygon.id !== 0}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 0
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={StepAOIChild} />
            </StepButton>
            <StepContent children={stepContentChild} />
          </Step>
          <Step completed={stepIndex !== 1 && newReportRequest.startDate !== null}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 1
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={StepTimeChild} />
            </StepButton>
            <StepContent children={timeWindowStep} style={{ width: '300px' }} />
          </Step>
          <Step completed={stepIndex !== 2 && newReportRequest.visibility !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 2
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={visibilityLabel} />
            </StepButton>
            <StepContent children={visibilityStep} />
          </Step>
          <Step completed={stepIndex !== 3 && newReportRequest.reportersAllowed.length > 0}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 3
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={StepReporterAllowedChild} />
            </StepButton>
            <StepContent>{reportersAllowed}</StepContent>
          </Step>
          <Step completed={stepIndex !== 4 && isStep3Completed}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 4
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={StepContentTypeChild} />
            </StepButton>
            <StepContent>{contentTypeStep}</StepContent>
          </Step>
        </Stepper>
        <div style={{ position: 'absolute', bottom: 120, left: 20, color: 'grey', fontSize: 13 }}>
          * mandatory fields
        </div>
      </section>
    );
  }
}

export default enhance(ReportNewRequest);
