import React, { Component } from 'react';
import { FlatButton, RaisedButton } from 'material-ui';
import { Step, Stepper, StepButton, StepContent, StepLabel } from 'material-ui/Stepper';
import { withOrganizationUnits, withLogin } from 'ioc-api-interface';
import { withLoader, withMessages } from 'js/modules/ui';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { compose } from 'redux';
import { withMissionCreate } from 'js/modules/missionCreate';
import { TitleStep, InputAOIStep, AssignToStep, TasksStep } from 'js/components/Steps';
import {
  AOILabel,
  TitleLabel,
  AssignToLabel,
  TasksLabel
} from 'js/components/app/drawer/components/commons';
import { withTaskCreate } from 'js/modules/taskCreate';
import { drawerNames } from 'js/modules/AreaOfInterest/dataSources/categories';

const enhance = compose(
  withLoader,
  withLeftDrawer,
  withMessages,
  withMissionCreate,
  withOrganizationUnits,
  withTaskCreate,
  withLogin
);

class MissionCreateStepper extends Component {
  state = {
    stepIndex: 0
  };

  componentDidMount() {
    this._init();
  }

  _init = async () => {
    //TODO: set Teams for all org units of the user
    if (this.props.user.organizationUnits) {
      const ids = this.props.user.organizationUnits.map(item => item.id);
      await this.props.getOrganizationUnitTeams(ids);
    }
  };

  handleTitleChange = e => {
    this.props.setMissionTitle(e.currentTarget.value);
  };

  handleTeamChange = e => {
    this.props.setMissionTeamId(e.value);
  };

  handleTeamRemove = () => {
    this.props.setMissionTeamId(null);
  };

  togglePrimary = drawerType => {
    this.props.setOpenSecondary(false);
    this.props.setOpenPrimary(true, drawerType);
  };

  handleNext = () => {
    const { stepIndex } = this.state;
    if (stepIndex < 4) {
      this.setState({
        stepIndex: stepIndex + 1
      });
    }
  };

  handlePrev = () => {
    const { stepIndex } = this.state;
    if (stepIndex > 0) {
      this.setState({
        stepIndex: stepIndex - 1
      });
    }
  };

  _toggleSecondary = () => {
    this.props.setOpenSecondary(true, drawerNames.taskCreate);
  };

  renderStepActions(step, canBeDisabled) {
    return (
      <div style={{ margin: '12px 0' }}>
        {step < 3 && (
          <RaisedButton
            label="Next"
            disableTouchRipple={true}
            disableFocusRipple={true}
            primary={true}
            onClick={this.handleNext}
            style={{ marginRight: 12 }}
          />
        )}
        {step > 0 && (
          <FlatButton
            label="Back"
            disableTouchRipple={true}
            disableFocusRipple={true}
            onClick={this.handlePrev}
          />
        )}
      </div>
    );
  }

  render() {
    let {
      dictionary,
      newMission,
      teams,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawCategory,
      setDrawPoint,
      setDrawPolygon,
      getMapDraw
    } = this.props;

    const drawProps = {
      getMapDraw,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory
    };

    const { stepIndex } = this.state;

    const { teamId, firstResponderId } = newMission;

    const team = teams.find(team => team.id === teamId);
    const address =
      drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const titleLabel = <TitleLabel title={newMission.title} dictionary={dictionary} />;
    const titleStep = (
      <TitleStep
        title={newMission.title}
        handleTitleChange={this.handleTitleChange}
        dictionary={dictionary}
        renderStepActions={this.renderStepActions(0)}
      />
    );

    const aoiLabel = <AOILabel text={address} dictionary={dictionary} />;
    const aoiStep = (
      <InputAOIStep
        isDisabled={newMission.id !== 0}
        category={this.props.category}
        address={address}
        renderStepActions={this.renderStepActions(1)}
        {...drawProps}
        resetCategory="base"
      />
    );

    const assignToLabel = (
      <AssignToLabel teamName={team ? team.displayName : ''} dictionary={dictionary} />
    );
    const assignToStep = (
      <AssignToStep
        dictionary={dictionary}
        teams={teams}
        renderStepActions={this.renderStepActions(2)}
        team={team}
        firstResponderId={firstResponderId}
        handleTeamChange={this.handleTeamChange}
        handleTeamRemove={this.handleTeamRemove}
      />
    );

    const tasksLabel = <TasksLabel dictionary={dictionary} />;
    const tasksStep = (
      <TasksStep
        dictionary={dictionary}
        toggleTaskDialog={this._toggleSecondary}
        renderStepActions={this.renderStepActions(3)}
      />
    );

    return (
      <section>
        <Stepper activeStep={stepIndex} linear={false} orientation="vertical">
          <Step completed={stepIndex > 0 && newMission.title !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 0
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={titleLabel} />
            </StepButton>
            <StepContent children={titleStep} style={{ width: '300px' }} />
          </Step>
          <Step completed={stepIndex !== 1 && address !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 1
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={aoiLabel} />
            </StepButton>
            <StepContent children={aoiStep} />
          </Step>
          <Step completed={stepIndex !== 2 && (teamId !== null || firstResponderId !== null)}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 2
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={assignToLabel} />
            </StepButton>
            <StepContent children={assignToStep} />
          </Step>
          <Step completed={stepIndex !== 3 && newMission.tasks.length > 0}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 3
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={tasksLabel} />
            </StepButton>
            <StepContent children={tasksStep} />
          </Step>
        </Stepper>
        <div style={{ position: 'absolute', bottom: 120, left: 20, color: 'grey', fontSize: 13 }}>
          * mandatory fields
        </div>
      </section>
    );
  }
}

export default enhance(MissionCreateStepper);
