import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {
  PeopleIcon,
  LocationOnIcon,
  ScheduleIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import { Divider } from 'material-ui';
import { localizeDate } from 'js/utils/localizeDate';

const iconStyle = { fontSize: '18px', color: 'lightgrey' };
const fontStyle = { width: 24, height: 24, padding: 0 };
const memberStyle = { display: 'flex', alignItems: 'center', fontSize: 12, margin: 2 };

export const MissionDetailHeader = muiThemeable()(({ dictionary, missionProperties, locale }) => (
  <div
    style={{
      margin: 10,
      wordBreak: 'break-word',
      display: 'flex',
      paddingTop: '20px',
      flexDirection: 'column',
      fontSize: 13
    }}
  >
    <span style={{ marginBottom: 20 }}>{missionProperties.title}</span>
    <Divider />
    <div style={memberStyle}>
      <LocationOnIcon style={fontStyle} iconStyle={iconStyle} />
      {missionProperties.address}
    </div>
    <div style={memberStyle}>
      <ScheduleIcon style={fontStyle} iconStyle={iconStyle} />
      {missionProperties.start != null && missionProperties.end != null ? (
        <span>
          {dictionary._start}: {localizeDate(missionProperties.start, locale, 'L LT', true)} -{' '}
          {dictionary._end}: {localizeDate(missionProperties.end, locale, 'L LT', true)}
        </span>
      ) : (
        <span />
      )}
    </div>
    <div style={memberStyle}>
      <PeopleIcon style={fontStyle} iconStyle={iconStyle} />
      <span>{missionProperties.missionAssigneeLabel}</span>
    </div>
  </div>
));
