import React, { Component } from 'react';
import { FlatButton, RaisedButton } from 'material-ui';
import { Step, Stepper, StepButton, StepContent, StepLabel } from 'material-ui/Stepper';
import { withReports } from 'ioc-api-interface';
import { withLoader, withMessages } from 'js/modules/ui';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { compose } from 'redux';
import { withEmCommNew } from 'js/modules/emCommNew';
import {
  InputAOIStep,
  TimeWindowStep,
  HazardTypeStep,
  DescriptionStep,
  EmCommCapStatusStep,
  EmCommCapDetailsStep,
  EmCommResponseTypeStep,
  EmCommReceiversStep,
  TitleStep
} from 'js/components/Steps';
import { AOILabel, TimeLabel } from 'js/components/app/drawer/components/commons';
import {
  EmCommTypeLabel,
  HazardLabel,
  DescriptionLabel,
  TargetLabel,
  EmCommCapDetailsLabel,
  EmCommResponseTypeLabel,
  VisibilityLabel
} from './commons';
import { TitleLabel, WebLabel } from 'js/components/app/drawer/components/commons';
import EmCommVisibilityStep from 'js/components/Steps/EmCommVisibilityStep';

const enhance = compose(
  withReports,
  withLoader,
  withLeftDrawer,
  withMessages,
  withEmCommNew
);

class EmCommStepper extends Component {
  state = {
    stepIndex: 0,
    title: ''
  };

  handleAOIChange = e => {
    let currentStep = this.props.draw['notification_new'].currentStep;
    if (currentStep === 0) {
      this.props.pushMessage('Click on the center of AOI on the map', 'success', null);
      this.props.setDrawCurrentStep('notification_new', 1);
    }
  };

  togglePrimary = drawerType => {
    this.props.setOpenSecondary(false);
    this.props.setOpenPrimary(true, drawerType);
  };

  handleNext = () => {
    const { stepIndex } = this.state;
    if (stepIndex < 10) {
      this.setState({
        stepIndex: stepIndex + 1
      });
    }
  };

  handlePrev = () => {
    const { stepIndex } = this.state;
    if (stepIndex > 0) {
      this.setState({
        stepIndex: stepIndex - 1
      });
    }
  };

  renderStepActions(step, canBeDisabled) {
    return (
      <div style={{ margin: '12px 0' }}>
        {step < 10 && (
          <RaisedButton
            label="Next"
            disableTouchRipple={true}
            disableFocusRipple={true}
            primary={true}
            onClick={this.handleNext}
            style={{ marginRight: 12 }}
          />
        )}
        {step > 0 && (
          <FlatButton
            label="Back"
            disableTouchRipple={true}
            disableFocusRipple={true}
            onClick={this.handlePrev}
          />
        )}
      </div>
    );
  }

  //#region TimeWindow
  handleStartChange = (event, startDate) => {
    this.props.setEmCommStartDate(startDate);
  };

  handleStartTimeChange = (event, startTime) => {
    this.props.setEmCommStartTime(startTime);
  };

  handleEndChange = (event, endDate) => {
    this.props.setEmCommEndDate(endDate);
  };

  handleEndTimeChange = (event, endTime) => {
    this.props.setEmCommEndTime(endTime);
  };
  //#endregion

  setEmCommType = type => {
    this.props.setEmCommType(type);
  };

  setEmCommVisibility = visibility => {
    this.props.setEmCommVisibility(visibility);
  };

  setEmCommLevel = level => {
    this.props.setEmCommLevel(level);
  };

  setEmCommCapStatus = level => {
    this.props.setEmCommCapStatus(level);
  };

  setEmCommWarningLevel = warningLevel => {
    this.props.setEmCommCapWarningLevel(warningLevel);
  };

  setEmCommCapCertainty = capCertainty => {
    this.props.setEmCommCapCertainty(capCertainty);
  };

  setEmCommCapUrgency = capUrgency => {
    this.props.setEmCommCapUrgency(capUrgency);
  };

  setEmCommCapResponseType = capResponseType => {
    this.props.setEmCommCapResponseType(capResponseType);
  };

  setEmCommHazard = hazard => {
    this.props.setEmCommHazard(hazard);
  };
  setCapCategory = capCategory => {
    this.props.setCapCategory(capCategory);
  };

  handleDescriptionChange = event => {
    this.props.setEmCommDescription(event.currentTarget.value);
  };

  handleTitleChange = e => {
    this.props.setEmCommTitle(e.currentTarget.value);
  };

  handleWebChange = e => {
    this.props.setEmCommWeb(e.currentTarget.value);
  };

  handleInstructionChange = e => {
    this.props.setEmCommInstruction(e.currentTarget.value);
  };

  render() {
    let {
      dictionary,
      roles,
      newEmComm,
      drawPoint,
      drawPolygon,
      drawTargetPoint,
      drawTargetPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawCategory,
      setDrawPolygon,
      getMapDraw,
      setDrawTargetPoint,
      setDrawTargetPolygon,
      enums
    } = this.props;

    const drawProps = {
      getMapDraw,
      drawPoint,
      drawPolygon,
      resetHomeDrawState,
      setDrawPoint,
      setDrawPolygon,
      setDrawCategory,
      drawTargetPoint
    };

    const { stepIndex } = this.state;

    const address = newEmComm.address
      ? newEmComm.address
      : drawPoint && drawPoint.coordinates
        ? '[ ' +
          Math.round(drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    const targetAddress = newEmComm.targetAddress
      ? newEmComm.targetAddress
      : drawTargetPoint && drawTargetPoint.coordinates
        ? '[ ' +
          Math.round(drawTargetPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(drawTargetPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    if (!newEmComm.hazard) newEmComm.hazard = { label: '', value: '' };
    if (!newEmComm.capCategory) newEmComm.capCategory = { label: '', value: '' };
    if (!newEmComm.level) newEmComm.level = { label: '', value: '' };
    if (!newEmComm.capStatus) newEmComm.capStatus = { label: '', value: '' };
    if (!newEmComm.warningLevel) newEmComm.warningLevel = { label: '', value: '' };
    if (!newEmComm.capCertainty) newEmComm.capCertainty = { label: '', value: '' };
    if (!newEmComm.capUrgency) newEmComm.capUrgency = { label: '', value: '' };
    if (!newEmComm.capResponseType) newEmComm.capResponseType = { label: '', value: '' };

    const capStatusChild = (
      <EmCommCapStatusStep
        dictionary={dictionary}
        newEmComm={newEmComm}
        setCapStatus={this.setEmCommCapStatus}
        setEmCommType={this.setEmCommType}
        renderStepActions={this.renderStepActions(0)}
      />
    );
    const capStatusLabel = (
      <EmCommTypeLabel
        type={newEmComm.type}
        level={newEmComm.capStatus.label}
        dictionary={dictionary}
      />
    );

    const hazardTypeStep = (
      <HazardTypeStep
        dictionary={dictionary}
        setHazard={this.setEmCommHazard}
        setCapCategory={this.setCapCategory}
        hazard={newEmComm.hazard}
        capCategory={newEmComm.capCategory}
        renderStepActions={this.renderStepActions(1)}
      />
    );
    const hazardLabel = (
      <HazardLabel
        capCategory={newEmComm.capCategory.label}
        hazard={newEmComm.hazard.label}
        dictionary={dictionary}
      />
    );

    const aoiLabel = <AOILabel text={address} dictionary={dictionary} />;

    const aoiStep = (
      <InputAOIStep
        isDisabled={newEmComm.id !== 0}
        category={this.props.category}
        address={address}
        renderStepActions={this.renderStepActions(2)}
        {...drawProps}
        resetCategory="base"
      />
    );

    const StepTimeChild = (
      <TimeLabel
        startDate={newEmComm.startDate}
        startTime={newEmComm.startTime}
        endDate={newEmComm.endDate}
        endTime={newEmComm.endTime}
        dictionary={dictionary}
        locale={this.props.locale}
      />
    );

    const timeWindowStep = (
      <TimeWindowStep
        isDisabledStart={newEmComm.id !== 0}
        startDate={newEmComm.startDate}
        startTime={newEmComm.startTime}
        endDate={newEmComm.endDate}
        endTime={newEmComm.endTime}
        dictionary={dictionary}
        locale={this.props.locale}
        handleStartChange={this.handleStartChange}
        handleStartTimeChange={this.handleStartTimeChange}
        handleEndChange={this.handleEndChange}
        handleEndTimeChange={this.handleEndTimeChange}
        renderStepActions={this.renderStepActions(3)}
      />
    );

    const capDetailsChild = (
      <EmCommCapDetailsStep
        dictionary={dictionary}
        newEmComm={newEmComm}
        setCapUrgency={this.setEmCommCapUrgency}
        setCapCertainty={this.setEmCommCapCertainty}
        setWarningLevel={this.setEmCommWarningLevel}
        renderStepActions={this.renderStepActions(4)}
      />
    );
    const capDetailsLabel = <EmCommCapDetailsLabel newEmComm={newEmComm} dictionary={dictionary} />;
    const isCapDetailsStepCompleted =
      newEmComm.warningLevel.value !== '' &&
      newEmComm.capCertainty.value !== '' &&
      newEmComm.capUrgency.value !== '';

    const visibilityStep = (
      <EmCommVisibilityStep
        visibility={newEmComm.visibility}
        dictionary={dictionary}
        setEmCommVisibility={this.setEmCommVisibility}
        visibilityList={enums.visibility}
        renderStepActions={this.renderStepActions(5)}
      />
    );
    const visibilityLabel = (
      <VisibilityLabel visibility={newEmComm.visibility} dictionary={dictionary} />
    );

    const targetStep = (
      <EmCommReceiversStep
        getMapDraw={this.props.getMapDraw}
        isDisabled={newEmComm.id !== 0}
        category={drawerNames.targetAOI}
        address={targetAddress}
        renderStepActions={this.renderStepActions(6)}
        setDrawPoint={setDrawTargetPoint}
        setDrawPolygon={setDrawTargetPolygon}
        setDrawCategory={setDrawCategory}
        resetHomeDrawState={resetHomeDrawState}
        drawPoint={drawTargetPoint}
        drawPolygon={drawTargetPolygon}
        resetCategory="target"
        roles={roles}
        dictionary={dictionary}
        receivers={newEmComm.receivers}
        removeEmCommReceiver={this.props.removeEmCommReceiver}
        updateEmCommReceivers={this.props.updateEmCommReceivers}
      />
    );

    const targetLabel = (
      <TargetLabel
        receivers={newEmComm.receivers.map(item => item.label)}
        address={targetAddress}
        dictionary={dictionary}
      />
    );

    const titleLabel = <TitleLabel title={newEmComm.title} dictionary={dictionary} />;
    const titleStep = (
      <TitleStep
        title={newEmComm.title}
        handleTitleChange={this.handleTitleChange}
        dictionary={dictionary}
        renderStepActions={this.renderStepActions(7)}
      />
    );

    const capResponseType = (
      <EmCommResponseTypeStep
        dictionary={dictionary}
        newEmComm={newEmComm}
        setCapResponseType={this.setEmCommCapResponseType}
        handleInstructionChange={this.handleInstructionChange}
        renderStepActions={this.renderStepActions(8)}
      />
    );

    const capResponseTypeLabel = (
      <EmCommResponseTypeLabel newEmComm={newEmComm} dictionary={dictionary} />
    );

    const descriptionStep = (
      <DescriptionStep
        description={newEmComm.description}
        handleDescriptionChange={this.handleDescriptionChange}
        dictionary={dictionary}
        renderStepActions={this.renderStepActions(9)}
      />
    );

    const descriptionLabel = (
      <DescriptionLabel description={newEmComm.description} dictionary={dictionary} />
    );

    const webLabel = <WebLabel web={newEmComm.web} />;
    const webStep = (
      <TitleStep
        title={newEmComm.web}
        handleTitleChange={this.handleWebChange}
        dictionary={dictionary}
        renderStepActions={this.renderStepActions(10)}
      />
    );

    const isCapStatusStepCompleted =
      newEmComm.type === 'alert' ||
      (newEmComm.type === 'warning' && newEmComm.capStatus.value !== '');

    return (
      <section>
        <Stepper activeStep={stepIndex} linear={false} orientation="vertical">
          <Step completed={stepIndex > 0 && isCapStatusStepCompleted}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 0
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={capStatusLabel} />
            </StepButton>
            <StepContent children={capStatusChild} />
          </Step>
          <Step completed={stepIndex !== 1 && newEmComm.hazard.value !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 1
                })
              }
            >
              <StepLabel style={{ color: 'white', marginBottom: 20 }} children={hazardLabel} />
            </StepButton>
            <StepContent children={hazardTypeStep} style={{ width: '300px' }} />
          </Step>
          <Step completed={stepIndex !== 2 && address !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 2
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={aoiLabel} />
            </StepButton>
            <StepContent children={aoiStep} />
          </Step>
          <Step completed={stepIndex !== 3 && newEmComm.startDate !== null}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 3
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={StepTimeChild} />
            </StepButton>
            <StepContent children={timeWindowStep} style={{ width: '300px' }} />
          </Step>
          <Step completed={stepIndex !== 4 && isCapDetailsStepCompleted}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 4
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={capDetailsLabel} />
            </StepButton>
            <StepContent children={capDetailsChild} />
          </Step>
          <Step completed={stepIndex !== 5 && newEmComm.visibility !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 5
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={visibilityLabel} />
            </StepButton>
            <StepContent children={visibilityStep} />
          </Step>
          <Step completed={stepIndex !== 6 && targetAddress !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 6
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={targetLabel} />
            </StepButton>
            <StepContent children={targetStep} />
          </Step>
          <Step completed={stepIndex !== 7 && newEmComm.title !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 7
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={titleLabel} />
            </StepButton>
            <StepContent children={titleStep} />
          </Step>
          <Step
            completed={
              stepIndex !== 8 &&
              newEmComm.capResponseType !== null &&
              newEmComm.capResponseType.value !== ''
            }
          >
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 8
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={capResponseTypeLabel} />
            </StepButton>
            <StepContent children={capResponseType} />
          </Step>
          <Step completed={stepIndex !== 9 && newEmComm.description !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 9
                })
              }
            >
              <StepLabel style={{ color: 'white', marginBottom: 20 }} children={descriptionLabel} />
            </StepButton>
            <StepContent children={descriptionStep} />
          </Step>
          <Step completed={stepIndex !== 10 && newEmComm.web !== ''}>
            <StepButton
              onClick={() =>
                this.setState({
                  stepIndex: 10
                })
              }
            >
              <StepLabel style={{ color: 'white' }} children={webLabel} />
            </StepButton>
            <StepContent children={webStep} />
          </Step>
        </Stepper>
        <div style={{ position: 'absolute', bottom: 120, left: 20, color: 'grey', fontSize: 13 }}>
          * mandatory fields
        </div>
      </section>
    );
  }
}

export default enhance(EmCommStepper);
