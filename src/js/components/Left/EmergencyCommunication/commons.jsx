import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';

const STYLE = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  justifyContent: 'flex-start',
  fontSize: '10px'
};

const ITEM_STYLE = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: 1
};

export const EmCommTypeLabel = muiThemeable()(({ type = '', level = '', dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._em_comm_type} *</span>
    </div>
    {type !== '' ? (
      <div style={ITEM_STYLE}>
        <span>{dictionary[type]}</span>
        {level !== '' ? (
          <div style={ITEM_STYLE}>
            <span>, {dictionary[level]}</span>
          </div>
        ) : (
          <div />
        )}
      </div>
    ) : (
      <div />
    )}
  </div>
));

export const HazardLabel = muiThemeable()(({ capCategory = '', hazard = '', dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._hazard} *</span>
    </div>
    {capCategory !== '' ? (
      <div style={ITEM_STYLE}>
        <span>{dictionary[capCategory]}</span>
        {hazard !== '' ? (
          <div style={ITEM_STYLE}>
            <span>, {dictionary[hazard]}</span>
          </div>
        ) : (
          <div />
        )}
      </div>
    ) : (
      <div />
    )}
  </div>
));

export const DescriptionLabel = muiThemeable()(({ description = '', dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._description}</span>
    </div>
    {description !== '' ? (
      <div style={ITEM_STYLE}>
        <span>{description.length > 65 ? description.substring(0, 65) + '...' : description}</span>
      </div>
    ) : (
      <div />
    )}
  </div>
));

export const TargetAOILabel = muiThemeable()(({ address = '', dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._target_aoi}</span>
    </div>
    {address !== '' ? (
      <div style={ITEM_STYLE}>
        <span>{address}</span>
      </div>
    ) : (
      <div />
    )}
  </div>
));

export const TargetLabel = muiThemeable()(({ address = '', receivers = [], dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._target_aoi}</span>
    </div>
    <span>{receivers.join(', ')}</span>
    <span>{address}</span>
  </div>
));

export const VisibilityLabel = muiThemeable()(({ visibility = '', dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._visibility}</span>
    </div>
    <span>{visibility}</span>
  </div>
));

export const EmCommCapDetailsLabel = muiThemeable()(({ newEmComm, dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._emcomm_cap_details}</span>
    </div>
    <div style={{ display: 'flex' }}>
      {newEmComm.warningLevel.label !== '' ? (
        <div style={ITEM_STYLE}>
          <span>{dictionary[newEmComm.warningLevel.label]}</span>
        </div>
      ) : (
        <div />
      )}
      {newEmComm.capCertainty !== '' ? (
        <div style={ITEM_STYLE}>
          <span>{dictionary[newEmComm.capCertainty.label]}</span>
        </div>
      ) : (
        <div />
      )}
      {newEmComm.capUrgency !== '' ? (
        <div style={ITEM_STYLE}>
          <span>{dictionary[newEmComm.capUrgency.label]}</span>
        </div>
      ) : (
        <div />
      )}
    </div>
  </div>
));

export const EmCommResponseTypeLabel = muiThemeable()(({ newEmComm, dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._instructions}</span>
    </div>
    <div style={{ display: 'flex' }}>
      {newEmComm.capResponseType.label !== '' ? (
        <div style={ITEM_STYLE}>
          <span>{dictionary[newEmComm.capResponseType.label]}</span>
        </div>
      ) : (
        <div />
      )}
      {newEmComm.instruction && newEmComm.instruction !== '' ? (
        <div style={ITEM_STYLE}>
          <span>
            {newEmComm.instruction.length > 65
              ? newEmComm.instruction.substring(0, 65) + '...'
              : newEmComm.instruction}
          </span>
        </div>
      ) : (
        <div />
      )}
    </div>
  </div>
));
