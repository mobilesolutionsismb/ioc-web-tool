import React, { Component } from 'react';
import { IconButton, FontIcon } from 'material-ui';
import { withReports } from 'ioc-api-interface';
import { withMessages } from 'js/modules/ui';
import './style.scss';
import muiThemeable from 'material-ui/styles/muiThemeable';

const STYLES = {
  buttonsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignContents: 'center',
    alignItems: 'center',
    height: 48
  },
  likesCounter: {
    width: '100%',
    maxWidth: 120,
    height: 'auto',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  likeIcon: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  smallIcon: {
    fontSize: '12px'
  }
};

const IconSizes = {
  xs: {
    padding: 2,
    fontSize: '12px'
  },
  s: {
    padding: 3,
    fontSize: '16px'
  },
  m: {
    padding: 4,
    fontSize: '18px'
  },
  l: {
    padding: 6,
    fontSize: '24px'
  },
  xl: {
    padding: 8,
    fontSize: '32px'
  }
};

export const LikeButton = ({ buttonType, count, disabled }) => (
  <span>
    <IconButton disabled={disabled} style={{ width: 'auto', padding: '5px' }}>
      <i className="material-icons md-18">{buttonType === 'up' ? 'thumb_up' : 'thumb_down'}</i>
    </IconButton>
    <span>{count}</span>
  </span>
);

export const LikeIcon = ({ buttonType, color, backgroundColor, size }) => (
  <FontIcon
    className="material-icons"
    color={color}
    style={{ ...IconSizes[size || 'm'], backgroundColor, borderRadius: '100%' }}
  >
    {buttonType === 'up' ? 'thumb_up' : 'thumb_down'}
  </FontIcon>
);

export const LikeIconWithCount = ({ buttonType, count, color, backgroundColor, size = 'm' }) => (
  <div style={STYLES.likeIcon}>
    <LikeIcon size={size} buttonType={buttonType} color={color} backgroundColor={backgroundColor} />
    <span className="like-icon-counter" style={IconSizes[size || 'm']}>
      {typeof count === 'number' ? count : 0}
    </span>
  </div>
);

export const LikesCounter = muiThemeable()(
  ({ upvotes, downvotes, muiTheme, size, style, alreadyVoted }) => (
    <div className="likes-counter" style={{ ...STYLES.likesCounter, ...style }}>
      <LikeIconWithCount
        buttonType="up"
        size={size || 'm'}
        count={upvotes}
        color={alreadyVoted === 'up' ? muiTheme.palette.canvasColor : muiTheme.palette.textColor}
        backgroundColor={
          alreadyVoted === 'up' ? muiTheme.palette.textColor : muiTheme.palette.canvasColor
        }
      />
      <LikeIconWithCount
        size={size || 'm'}
        buttonType="down"
        count={downvotes}
        color={alreadyVoted === 'down' ? muiTheme.palette.canvasColor : muiTheme.palette.textColor}
        backgroundColor={
          alreadyVoted === 'down' ? muiTheme.palette.textColor : muiTheme.palette.canvasColor
        }
      />
    </div>
  )
);

class LikeButtons extends Component {
  render() {
    const { report, containerStyle } = this.props;
    if (report.voteEnabled) {
      return (
        <div className="controls" style={{ ...STYLES.buttonsContainer, ...containerStyle }}>
          <LikeButton buttonType="up" disabled={true} count={report.upvoteCount} />
          <LikeButton buttonType="down" disabled={true} count={report.downvoteCount} />
        </div>
      );
    } else {
      return <div className="controls" style={{ ...STYLES.buttonsContainer, ...containerStyle }} />;
    }
  }
}

export default withMessages(withReports(LikeButtons));
