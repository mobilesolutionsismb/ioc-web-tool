import './style.scss';
import React, { Component } from 'react';
import { compose } from 'redux';
import { Card, CardMedia, CardTitle, CardText } from 'material-ui';
import { withMissions, withLogin } from 'ioc-api-interface';
import { ReportDetailButtons } from 'js/components/DetailCard/commons';
import {
  TaskDetailCardInfo
  //TaskDetailCardFooter
} from './commons';
import { withLoader, withMessages } from 'js/modules/ui';
import moment from 'moment';

const enhance = compose(
  withMissions,
  withLoader,
  withMessages,
  withLogin
);

class TaskDetailCard extends Component {
  closeCard = () => {
    this.props.deselectMissionTask();
  };

  _onDeleteButtonClick = async () => {
    if (!this.props.deleteSelectedTask) return;
    if (this.props.taskDescription)
      //when the task has no ID
      this.props.deleteSelectedTask(this.props.taskDescription);
    //task has to be deleted server-side
    else this.props.deleteSelectedTask(this.props.task.id);
  };

  render() {
    const { feature, dictionary, locale, user } = this.props;
    const { description, end, isTookOver, isCompleted, taskUsers } = feature.properties;
    const mission = this.props.mission != null ? this.props.mission.properties : {};
    const taskEnd = new moment(end);
    const remainingTime = moment.duration(new moment().diff(taskEnd));
    const parsedTaskUsers = typeof taskUsers === 'string' ? JSON.parse(taskUsers) : taskUsers;

    const assignee =
      parsedTaskUsers && (isTookOver || isCompleted)
        ? parsedTaskUsers
            .map(tu => (tu.user.id === user.id ? dictionary._me : tu.user.userName))
            .join(', ')
        : null;

    return (
      <Card style={{ minHeight: 500 }}>
        <CardMedia>
          <ReportDetailButtons onCloseClick={this.closeCard} />
        </CardMedia>
        <CardTitle
          title="TASK"
          titleStyle={{ fontSize: 15 }}
          style={{ paddingBottom: 0, paddingTop: 8 }}
        />
        <CardText style={{ padding: 0, height: 350 }}>
          <div className="task-detail-description">{description}</div>
          <div className="task-detail-info">
            <TaskDetailCardInfo
              user={this.props.user}
              dictionary={dictionary}
              locale={locale}
              task={feature.properties}
              mission={mission}
              remainingTime={remainingTime != null ? remainingTime._data.days : ''}
              assignee={assignee}
            />
          </div>
          {/* <hr className="horizontal-line-style" />
          <div className="task-detail-info">
            <TaskDetailCardFooter />
          </div> */}
        </CardText>
      </Card>
    );
  }
}

export default enhance(TaskDetailCard);
