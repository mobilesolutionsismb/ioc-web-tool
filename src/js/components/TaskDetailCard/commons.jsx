import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { localizeDate } from 'js/utils/localizeDate';
import {
  DeleteIcon,
  PersonIcon,
  DoneIcon,
  LocationOnIcon,
  ScheduleIcon,
  BuildIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import { MISSION_COLORS } from 'js/startup/iReactTheme';
import { capitalize } from 'js/utils/stringUtils';
import styled from 'styled-components';

const TookOverByChip = styled.span`
  margin-left: auto;
  margin-right: 8px;
  text-transform: uppercase;
  font-weight: 300;
  padding: 2px 4px;
  height: 16px;
  line-height: 16px;
  font-size: 14px;
  border-radius: 3px;
`;

export const ChipTookOverBy = styled(TookOverByChip)`
  background-color: ${props => props.theme.palette.textColor};
  color: ${props => props.theme.palette.backgroundColor};
`;

export const ChipNoneTookOver = styled(TookOverByChip)`
  background-color: ${props => props.theme.palette.backgroundColor};
  color: ${props => props.theme.palette.textColor};
  border: 1px dashed;
  box-sizing: border-box;
`;

const memberStyle = { display: 'flex', alignItems: 'center', fontSize: 14, margin: 2 };
const fontStyle = { width: 24, height: 24, padding: 0 };
const iconStyle = { fontSize: '18px', color: 'lightgrey' };

const taskDetailIconStyle = { borderRadius: 200, fontSize: 18 };
const customStyle = { padding: 0, height: 0 };
const assigneeBadgeStyle = { margin: 0, height: 18, lineHeight: '16px' };

export const TaskDetailCardInfo = muiThemeable()(
  ({ dictionary, task, locale, mission, remainingTime, assignee }) => (
    <div>
      <div style={memberStyle}>
        <PersonIcon style={fontStyle} iconStyle={iconStyle} />
        <span>
          {dictionary._missiontask_acceptedBy}
          :&nbsp;
        </span>
        {assignee ? (
          <ChipTookOverBy style={assigneeBadgeStyle}>{assignee}</ChipTookOverBy>
        ) : (
          <ChipNoneTookOver style={assigneeBadgeStyle}>{dictionary._nobody}</ChipNoneTookOver>
        )}
      </div>
      <div style={memberStyle}>
        <LocationOnIcon style={fontStyle} iconStyle={iconStyle} />
        {task.address}
      </div>
      <div style={{ ...memberStyle, display: 'flex', alignItems: 'center' }}>
        <ScheduleIcon style={fontStyle} iconStyle={iconStyle} />
        <span>
          {localizeDate(task.start, locale, 'L LT', true)} -{' '}
          {localizeDate(task.end, locale, 'L LT', true)}
        </span>
        <span>
          <TaskColorDot status={task.temporalStatus} dictionary={dictionary} />
        </span>
        <span>
          <TaskStatusIcon isCompleted={task.isCompleted} isTookOver={task.isTookOver} />
        </span>
        {/* {task.temporalStatus !== 'expired' && task.temporalStatus !== 'completed' ? (
          <span>-{remainingTime} days</span>
        ) : (
          <div />
        )} */}
      </div>
      <div style={memberStyle}>
        <PersonIcon style={fontStyle} iconStyle={iconStyle} />
        <span>
          {dictionary._assigned_to}
          :&nbsp;
        </span>
        <span>{mission ? mission.missionAssigneeLabel : ''}</span>
      </div>
      <div style={memberStyle}>
        <PersonIcon style={fontStyle} iconStyle={iconStyle} />
        <span>
          {dictionary._agents_needed}
          :&nbsp;
        </span>
        <span>{task.numberOfNecessaryPeople}</span>
      </div>
      <div style={memberStyle}>
        <BuildIcon style={fontStyle} iconStyle={iconStyle} />
        {task.taskTools}
      </div>
    </div>
  )
);

export function TaskStatusIcon({ isCompleted, isTookOver }) {
  return (
    <div>
      {isCompleted ? (
        <DoneIcon
          style={customStyle}
          iconStyle={{ ...taskDetailIconStyle, color: 'black', backgroundColor: 'lawngreen' }}
        />
      ) : isTookOver ? (
        <PersonIcon
          style={customStyle}
          iconStyle={{ ...taskDetailIconStyle, color: 'black', backgroundColor: 'white' }}
        />
      ) : (
        <PersonIcon
          style={customStyle}
          iconStyle={{ ...taskDetailIconStyle, color: 'white', backgroundColor: 'black' }}
        />
      )}
    </div>
  );
}

export const TaskDetailCardFooter = () => (
  <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
    <span>MISSION DETAIL</span>
    {/* <DeleteIcon style={fontStyle} iconStyle={iconStyle} /> */}
  </div>
);

export function TaskColorDot({ status, dictionary }) {
  let color = '';
  switch (status) {
    case 'completed':
      color = MISSION_COLORS.temporalStatus.completed;
      break;
    case 'expired':
      color = MISSION_COLORS.temporalStatus.expired;
      break;
    case 'ongoing':
      color = MISSION_COLORS.temporalStatus.ongoing;
      break;
    case 'open':
      color = MISSION_COLORS.temporalStatus.open;
      break;
    case 'tookOver':
      color = MISSION_COLORS.temporalStatus.open;
      break;
    default:
      color = 'black';
      break;
  }

  return (
    <div className={`status-task ${status}`} style={{ background: color }}>
      <div className="tooltiptext">{dictionary(`_mission_filter_is${capitalize(status)}`)}</div>
    </div>
  );
}

export const TaskDetailActions = muiThemeable()(({ dictionary, status, onDeleteClick }) => (
  <div className="task-detail-card-actions">
    <DeleteIcon
      iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
      tooltip={dictionary._delete_task}
      onClick={onDeleteClick}
    />
    <span>
      <TaskBottomIcon status={status} />
    </span>
  </div>
));

export const TaskBottomIcon = ({ status }) => {
  let icon = <div />;
  const iconStyle = { fontSize: 16 };
  switch (status) {
    case 'isCompleted':
      icon = (
        <DoneIcon
          iconStyle={{
            ...iconStyle,
            color: 'black',
            backgroundColor: 'lawngreen',
            borderRadius: 10
          }}
        />
      );
      break;
    case 'isTookOver':
      icon = (
        <PersonIcon
          iconStyle={{ ...iconStyle, color: 'black', backgroundColor: 'white', borderRadius: 10 }}
        />
      );
      break;
    default:
      icon = <PersonIcon iconStyle={iconStyle} />;
      break;
  }

  return icon;
};
