import React, { Component } from 'react';
import { API } from 'ioc-api-interface';
import qs from 'qs';
import { passwordRule } from 'js/utils/fieldValidation';
import { logMain, logError } from 'js/utils/log';
import {
  FormComponent,
  StyledForm,
  FormContentWrapper,
  StyledFormActions
} from 'js/components/FormComponent';
import { RaisedButton } from 'material-ui';
// import { withDictionary } from 'ioc-localization';
// import { withLoader, withMessages } from 'js/modules/ui';
// import { compose } from 'redux';
import { ErrorContainer } from '../app/commons';
import { getErrorMessage } from 'js/utils/getErrorMessage';

// const enhance = compose(
//   withLoader,
//   withMessages,
//   withDictionary
// );

const api = API.getInstance();

const resetPassworFormConfig = (userId, resetCode) => [
  {
    name: 'userId',
    inputType: 'hidden',
    defaultValue: userId || ''
  },
  {
    name: 'resetCode',
    inputType: 'hidden',
    defaultValue: resetCode || ''
  },
  {
    name: 'password',
    trim: true,
    inputType: 'password',
    label: '_cp_form_newpassword',
    rules: [passwordRule],
    defaultValue: ''
  }
];

export class ResetPasswordComponent extends Component {
  state = {
    error: null
  };

  _getParams = search => {
    const params = qs.parse(search.replace(/^\?/, ''));
    return {
      userId: parseInt(params.userId, 10),
      resetCode: params.resetCode,
      tenantId: parseInt(params.tenantId, 10)
    };
  };

  _getErrorMessage = error => getErrorMessage(error);

  _resetPassword = async (userId, resetCode, password) => {
    try {
      this.props.loadingStart();
      const response = await api.account.resetPassword(userId, resetCode, password);
      logMain('Password reset successfully', response.data);
      this.props.pushMessage('Password reset successfully', 'success');
    } catch (err) {
      this.setState({ error: err });
      logError(err);
      this.props.pushError(err);
    } finally {
      this.props.loadingStop();
    }
  };

  _onSubmit = formData => {
    logMain('FD', formData);
    const { userId, resetCode, password } = formData;
    this._resetPassword(userId, resetCode, password);
  };

  componentDidMount() {
    const { userId, resetCode } = this._getParams(this.props.location.search);
    if (!userId || !resetCode) {
      const message =
        !userId && !resetCode
          ? 'Missing arguments'
          : !userId
          ? 'Missing userId'
          : 'Missing resetCode';
      const err = new Error(message);
      this.setState({ error: err });

      this.props.pushError(err);
    }
  }

  render() {
    const { location, dictionary } = this.props;
    const params = this._getParams(location.search);
    const { userId, resetCode } = params;
    logMain('params', params);
    return (
      <div className="reset-password page">
        <h1>{dictionary._prof_edit_change_password}</h1>
        {this.state.error === null && (
          <FormComponent
            className="reset-password-form"
            style={{ height: 120 }}
            dictionary={dictionary}
            config={resetPassworFormConfig(userId, resetCode)}
            onSubmit={this._onSubmit}
            formComponent={StyledForm}
            innerComponent={FormContentWrapper}
            actionComponent={StyledFormActions}
            actions={() => [
              <RaisedButton
                key="login"
                type="submit"
                className="ui-button"
                primary={true}
                // style={style.button}
                label={dictionary ? dictionary._submit : 'submit'}
              />
            ]}
          />
        )}
        {this.state.error !== null && (
          <ErrorContainer>
            <h2>Error</h2>
            <p>{this._getErrorMessage(this.state.error)}</p>
          </ErrorContainer>
        )}
      </div>
    );
  }
}

export default ResetPasswordComponent;
