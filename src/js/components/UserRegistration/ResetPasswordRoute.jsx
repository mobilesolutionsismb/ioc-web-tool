import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router';
import { withDictionary } from 'ioc-localization';
import { withLoader, withMessages } from 'js/modules/ui';
import { compose } from 'redux';

const enhance = compose(
  withLoader,
  withMessages,
  withDictionary
);

const ResetPasswordComponent = lazy(() => import('js/components/UserRegistration/ResetPassword'));

const ResetPassword = enhance(props => (
  <Suspense fallback={<div>Loading...</div>}>
    <ResetPasswordComponent {...props} />
  </Suspense>
));

export const ResetPasswordRoute = () => (
  <Route render={routeProps => <ResetPassword {...routeProps} />} />
);

export default ResetPasswordRoute;
