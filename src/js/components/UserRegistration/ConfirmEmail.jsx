import React, { Component } from 'react';
// import { Route } from 'react-router';
import { API } from 'ioc-api-interface';
import qs from 'qs';
import { logMain, logError, logWarning } from 'js/utils/log';
// import { withDictionary } from 'ioc-localization';
// import { withLoader, withMessages } from 'js/modules/ui';
// import { compose } from 'redux';
import { ErrorContainer } from '../app/commons';
import { getErrorMessage } from 'js/utils/getErrorMessage';
// const enhance = compose(
//   withLoader,
//   withMessages,
//   withDictionary
// );

const api = API.getInstance();

export class ConfirmEmailComponent extends Component {
  state = {
    emailConfirmed: false
  };

  _getParams = search => {
    const params = qs.parse(search.replace(/^\?/, ''));
    return {
      userId: parseInt(params.userId, 10),
      confirmationCode: params.confirmationCode,
      tenantId: parseInt(params.tenantId, 10)
    };
  };

  _getErrorMessage = error => getErrorMessage(error);

  _confirmEmail = async (userId, confirmationCode) => {
    try {
      this.props.loadingStart();
      const response = await api.account.activateEmail(userId, confirmationCode);
      logMain('Email confirmed successfully', response.data);
      this.setState({ emailConfirmed: true });
    } catch (err) {
      if (
        err.details.status === 400 &&
        err.details.serverDetails &&
        err.details.serverDetails.error.message === 'Email already activated'
      ) {
        this.setState({ emailConfirmed: true });
        logWarning(err);
      } else {
        logError(err);
        this.setState({ emailConfirmed: err });
        this.props.pushError(err);
      }
    } finally {
      this.props.loadingStop();
    }
  };

  componentDidMount() {
    const { userId, confirmationCode } = this._getParams(this.props.location.search);
    if (userId && confirmationCode) {
      this._confirmEmail(userId, confirmationCode);
    } else {
      const message =
        !userId && !confirmationCode
          ? 'Missing arguments'
          : !userId
          ? 'Missing userId'
          : 'Missing confirmationCode';
      const err = new Error(message);
      this.setState({ emailConfirmed: err });

      this.props.pushError(err);
    }
  }

  render() {
    const { emailConfirmed } = this.state;
    return (
      <div className="confirm-email page">
        {emailConfirmed === true && <p>Your e-mail address has been verified successfully!</p>}
        {emailConfirmed === true && (
          <p>You can close this window and login using the I-REACT Mobile App</p>
        )}
        {emailConfirmed === false && <p>Please wait while your email is being verified...</p>}
        {emailConfirmed instanceof Error && (
          <ErrorContainer>
            <h2>An error occurred while verifying your email</h2>
            <p>{this._getErrorMessage(emailConfirmed)}</p>
          </ErrorContainer>
        )}
      </div>
    );
  }
}

// export const ConfirmEmail = enhance(ConfirmEmailComponent);
export default ConfirmEmailComponent;

// export const ConfirmEmailRoute = () => (
//   <Route render={routeProps => <ConfirmEmail {...routeProps} />} />
// );
// export default ConfirmEmailRoute;
