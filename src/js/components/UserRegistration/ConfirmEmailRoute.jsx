import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router';
import { withDictionary } from 'ioc-localization';
import { withLoader, withMessages } from 'js/modules/ui';
import { compose } from 'redux';
const enhance = compose(
  withLoader,
  withMessages,
  withDictionary
);
const ConfirmEmailComponent = lazy(() => import('js/components/UserRegistration/ConfirmEmail'));
export const ConfirmEmail = enhance(props => (
  <Suspense fallback={<div>Loading...</div>}>
    <ConfirmEmailComponent {...props} />
  </Suspense>
));

export const ConfirmEmailRoute = () => (
  <Route render={routeProps => <ConfirmEmail {...routeProps} />} />
);

export default ConfirmEmailRoute;
