import React, { Component } from 'react';
import styled from 'styled-components';
import { ColumnPage } from 'js/components/app/commons';
import { RefreshIndicator } from 'material-ui';
import { uiLoaderStyle /* , loadingOverlayStyle */ } from './styles';
import { withLoader } from 'js/modules/ui';

const OverlayBackground = styled(ColumnPage)`
  background-color: rgba(0, 0, 0, 0.4);
  position: fixed;
  z-index: 99999;
`;

class LoadingOverlay extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.loading.status !== this.props.loading.status;
  }

  render() {
    return this.props.loading.status ? (
      <OverlayBackground className="loading-overlay">
        <RefreshIndicator size={40} left={10} top={0} status={'loading'} style={uiLoaderStyle} />
      </OverlayBackground>
    ) : (
      <div />
    );
  }
}

export default withLoader(LoadingOverlay);
