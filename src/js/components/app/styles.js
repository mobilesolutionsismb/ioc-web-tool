/**
 * Global app components style
 */
import styled from 'styled-components';
const APP_BAR_HEIGHT = 64;
const APP_NAV_WEIGHT = 64;

export const layoutStyle = {
  width: '100vw',
  height: '100vh',
  position: 'fixed',
  margin: 0,
  top: 0,
  left: 0
};

export const pageStyle = {
  position: 'relative',
  left: 0,
  top: APP_BAR_HEIGHT,
  height: `calc(100% - ${APP_BAR_HEIGHT}px)`
};

export const statusBarStyle = {
  position: 'fixed',
  left: APP_NAV_WEIGHT,
  top: APP_BAR_HEIGHT,
  zIndex: 1,
  height: APP_BAR_HEIGHT,
  width: `calc(100% - ${APP_NAV_WEIGHT}px)` /*,
    background: 'rgba(255,255,255, 0.8)',
    color: '#808080'*/
};

export const uiLoaderStyle = {
  display: 'inline-block',
  position: 'relative'
};

export const loadingOverlayStyle = {
  width: '100%',
  height: '100%',
  position: 'fixed',
  top: 0,
  left: 0,
  zIndex: 9999,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'rgba(0,0,0,0.5)'
};

export const leftDrawerStyle = {
  position: 'absolute',
  boxSizing: 'border-box',
  boxShadow: 'none',
  borderRight: '1px solid #F0F0F0',
  height: `calc(100vh - ${APP_BAR_HEIGHT}px)`,
  width: '380px',
  overflow: 'hidden',
  left: '64px'
};

const STYLE = {
  layoutStyle,
  pageStyle,
  uiLoaderStyle,
  loadingOverlayStyle,
  statusBarStyle,
  leftDrawerStyle
};

export default STYLE;

export const LoaderContainer = styled.div`
  position: fixed;
  width: calc(100% - 62px);
  top: 128px;
  left: 64px;
  z-index: 10;
  overflow: hidden;
`;
