import React, { Component } from 'react';
import { /* Avatar, */ MenuItem, Paper, Menu, Divider, FontIcon } from 'material-ui';

class RightMenu extends Component {
  render() {
    let { dictionary, openMenu, toggleOpenMenu } = this.props;

    return (
      <Paper className={openMenu ? '_h_nearx' : '_h_farx'}>
        <Menu onClick={toggleOpenMenu}>
          <MenuItem
            primaryText={dictionary._tr_popup_settings}
            leftIcon={<FontIcon className="material-icons">settings</FontIcon>}
          />
          <MenuItem
            primaryText={dictionary._tr_popup_users}
            leftIcon={<FontIcon className="material-icons">person_add</FontIcon>}
          />
          <Divider />
          <MenuItem
            primaryText={dictionary._tr_popup_account}
            leftIcon={<FontIcon className="material-icons">account_circle</FontIcon>}
          />
        </Menu>
      </Paper>
    );
  }
}

export { RightMenu };
export default RightMenu;
