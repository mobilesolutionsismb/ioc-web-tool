import React, { Component } from 'react';
import { white } from 'material-ui/styles/colors';

import { appInfo } from 'js/utils/getAppInfo';
import { API } from 'ioc-api-interface';
import { UserProfileIconMenu } from 'js/components/UserProfile/UserProfileIconMenu';
import { IconButton, FontIcon } from 'material-ui';
import { Link } from 'react-router-dom';

import { withLogin } from 'ioc-api-interface';
import { withLoader, withMessages } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { withUserSettings } from 'js/modules/UserSettings';
import { withRouter } from 'react-router';
import { localizeDate } from 'js/utils/localizeDate';
import { compose } from 'redux';

const enhance = compose(
  withLoader,
  withMessages,
  withLogin,
  withDictionary,
  withUserSettings
);
const api = API.getInstance();

const BUILD_INFO_KEY_MAP = {
  title: '',
  packageName: 'Package Name:',
  description: 'Description:',
  version: 'Version:',
  buildDate: 'Build Date:',
  environment: 'Bundle Environment:',
  backendEnvironment: 'Backend Environment:',
  geoServerURL: 'Geoserver URL:',
  rasterLayersMode: 'Raster Layers Mode'
};

const BUILD_INFO_VALUES_MAP = {
  title: v => <h4>{v}</h4>,
  packageName: v => <span>&nbsp;{v}</span>,
  description: v => <span>&nbsp;{v}</span>,
  version: v => <span>&nbsp;{v}</span>,
  buildDate: (v, locale) => <span>&nbsp;{localizeDate(v, locale, 'LLLL Z')}</span>,
  environment: v => <span>&nbsp;{v}</span>,
  backendEnvironment: v => <span>&nbsp;{v}</span>,
  geoServerURL: v => <span>&nbsp;{v}</span>,
  rasterLayersMode: v => <span>&nbsp;{v}</span>
};

const getEntryKey = key => (BUILD_INFO_KEY_MAP.hasOwnProperty(key) ? BUILD_INFO_KEY_MAP[key] : key);

const getEntryValue = (key, value, locale) =>
  BUILD_INFO_VALUES_MAP.hasOwnProperty(key) ? BUILD_INFO_VALUES_MAP[key](value, locale) : value;

const About = ({ locale, dictionary }) => {
  const style = { textTransform: 'capitalize' };
  return (
    <div>
      {Object.entries(appInfo).map((entry, i) => (
        <div key={i}>
          <b style={style}>{getEntryKey(entry[0])}</b>
          {getEntryValue(entry[0], entry[1], locale)}
        </div>
      ))}
      <h4>API Module version</h4>
      <div>
        <b style={style}>{getEntryKey('packageName')}</b>
        {getEntryValue('packageName', api.moduleVersion.pkgName, locale)}
      </div>
      <div>
        <b style={style}>{getEntryKey('version')}</b>
        {getEntryValue('version', api.moduleVersion.version, locale)}
      </div>
      <div>
        <b style={style}>{getEntryKey('buildDate')}</b>
        {getEntryValue('buildDate', api.moduleVersion.buildDate, locale)}
      </div>
      <div>
        <b style={style}>{getEntryKey('environment')}</b>
        {getEntryValue('environment', api.moduleVersion.environment, locale)}
      </div>
    </div>
  );
};

class RightElement extends Component {
  onLogoutClick = async props => {
    await this.props.logout(this.props.loadingStart, this.props.loadingStop);
    this.props.history.replace('/logout');
  };

  onSettingsClick = () => {
    this.props.history.push('/settings', { from: this.props.location.pathname });
  };

  showInfo = () => {
    this.props.pushModalMessage(
      'Build Info',
      <About locale={this.props.locale} dictionary={this.props.dictionary} />,
      {
        _close: null
      }
    );
  };

  render() {
    return this.props.user ? (
      <div key={'0'}>
        {!this.props.location.pathname.match(/settings/) && (
          <IconButton
            onClick={this.onSettingsClick}
            tooltip={this.props.dictionary._tr_popup_settings}
          >
            <FontIcon className="material-icons" color={white}>
              settings
            </FontIcon>
          </IconButton>
        )}
        <UserProfileIconMenu history={this.props.history} user={this.props.user} />
        <IconButton tooltip="About" onClick={this.showInfo}>
          <FontIcon className="material-icons" color={white}>
            info_outline
          </FontIcon>
        </IconButton>
        {this.props.user.userName === 'admin' && !this.props.location.pathname.match(/admin/) && (
          <Link to="/admin">
            <IconButton tooltip="Admin Panel">
              <FontIcon className="material-icons" color={white}>
                settings_applications
              </FontIcon>
            </IconButton>
          </Link>
        )}

        <IconButton onClick={this.onLogoutClick} tooltip={this.props.dictionary._signout}>
          <FontIcon className="material-icons" color={white}>
            exit_to_app
          </FontIcon>
        </IconButton>
      </div>
    ) : (
      <span key={'0'} />
    );
  }
}
export default withRouter(enhance(RightElement));
