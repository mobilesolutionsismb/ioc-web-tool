import React from 'react';
import { IconButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';

export const HeaderButton = iconText =>
  muiThemeable()(({ muiTheme, onClick }) => (
    <IconButton className="menu-button" onClick={onClick}>
      <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
        {iconText}
      </FontIcon>
    </IconButton>
  ));

export const BackButton = HeaderButton('arrow_back');
export const CloseButton = HeaderButton('clear');
export const MenuButton = HeaderButton('menu');
