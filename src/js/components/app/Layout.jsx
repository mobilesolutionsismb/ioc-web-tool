import React, { Component } from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { Navigator } from 'js/components/Navigator';
import { Router, Route } from 'react-router-dom';

import { withMessages } from 'js/modules/ui';

import { Header, RightElement } from './header2';
import Modal from './Modal';
import SnackBar from './SnackBar';
import LoadingOverlay from './LoadingOverlay';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import styled from 'styled-components';

const LayoutContainer = styled.div`
  width: 100vw;
  height: 100vh;
  position: fixed;
  margin: 0;
  top: 0;
  left: 0;
  color: ${props => props.theme.palette.textColor};
  background-color: ${props => props.theme.palette.canvasColor};
`;

class Layout extends Component {
  // Read https://reactjs.org/blog/2017/07/26/error-handling-in-react-16.html
  componentDidCatch(error, info) {
    console.error('Top Level Component error', error, info);
    // Here all exception are critical because it means unhandled by other views
    appInsights.trackException(error, 'Layout::componentDidCatch', {}, {}, SeverityLevel.Critical);
    this.props.pushError(error);
  }

  render() {
    const { onRouterError, history } = this.props;
    return (
      <Router onRouterError={onRouterError} history={history}>
        <LayoutContainer className="layout">
          <Header showLogo={true} rightButton={<RightElement />} />
          <Navigator style={{ height: 'calc(100% - 64px)', left: 0, top: 64 }} />
          <Route render={routerProps => <Modal {...routerProps} />} />
          <SnackBar />
          <LoadingOverlay />
        </LayoutContainer>
      </Router>
    );
  }
}

export default muiThemeable()(withMessages(Layout));
