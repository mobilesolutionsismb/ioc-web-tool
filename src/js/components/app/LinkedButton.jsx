import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class LinkedButton extends Component {
  static defaultProps = {
    to: '/'
  };
  render() {
    const { to, children, replace, ...props } = this.props;
    return (
      <button {...props}>
        <Link to={to} replace={replace || true}>
          {children}
        </Link>
      </button>
    );
  }
}
