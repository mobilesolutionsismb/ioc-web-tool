import React, { Component } from 'react';
import { compose } from 'redux';
import ReactMaterialUiNotifications from './ReactMaterialUiNotifications';
import Flag from 'material-ui/svg-icons/content/flag';
import { deepOrange500 } from 'material-ui/styles/colors';
import moment from 'moment';
import { withNotifications } from 'ioc-api-interface';
import { withMessages } from 'js/modules/ui';
const enhance = compose(withMessages, withNotifications);

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  closeNot(not) {
    this.props.setNotificationRead(not);
  }
  showNotification = not => {
    ReactMaterialUiNotifications.showNotification({
      id: not.id,
      title: 'Title',
      additionalText: not.notification.data.message,
      icon: <Flag />,
      iconBadgeColor: deepOrange500,
      overflowText: not.notification.data.properties.customProp,
      timestamp: moment().format('h:mm A')
    });
    // update notifications count
    this.setState({
      count: ++this.state.count
    });
  };
  render() {
    return (
      <div />
      // <ReactMaterialUiNotifications
      //   desktop={true}
      //   transitionName={{
      //     leave: 'dummy',
      //     leaveActive: 'fadeOut',
      //     appear: 'dummy',
      //     appearActive: 'zoomInUp'
      //   }}
      //   transitionAppear={true}
      //   transitionLeave={true}
      //   onRef={ref => (this.notificationsList = ref)}
      //   closeNot = {(id) =>this.closeNot(id)}
      // />
    );
  }
}

export default enhance(Notification);
