import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { avatarLoader } from 'js/utils/getAssets';

async function loadPicture(i, callback) {
  const avatarPicB64 = await avatarLoader(i);
  callback(avatarPicB64);
}

const AvatarWrapper = styled.div.attrs(() => ({ className: 'avatar' }))`
  background-image: ${props =>
    props.backgroundImageB64 ? `url(${props.backgroundImageB64})` : ''};
  background-repeat: no-repeat;
  background-position: center;
  width: ${props => props.size || 40}px;
  height: ${props => props.size || 40}px;
  box-sizing: border-box;
  border-radius: 100%;
  border: 2px solid
    ${props =>
      props.isPro ? props.theme.palette.authorityColor : props.theme.palette.citizensColor};
`;

export function SmallAvatar({ avatarIndex = 0, isPro = false, style, size }) {
  const [avatarPicB64, setAvatarPicB64] = useState('');

  useEffect(() => {
    loadPicture(avatarIndex, setAvatarPicB64);
  }, [avatarIndex]);

  return (
    <AvatarWrapper style={style} isPro={isPro} backgroundImageB64={avatarPicB64} size={size} />
  );
}
