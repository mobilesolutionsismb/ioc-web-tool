import React, { Component } from 'react';
import { compose } from 'redux';
import {
  IconButton,
  FontIcon,
  MenuItem,
  DropDownMenu,
  Toolbar,
  ToolbarGroup,
  ToolbarTitle,
  Toggle
} from 'material-ui';
import { withUserSettings } from 'js/modules/UserSettings';
import { withMapLayerFilters } from 'js/modules/mapLayerFilters';

const enhance = compose(withUserSettings, withMapLayerFilters);

const styles = {
  thumbOff: {
    backgroundColor: 'lightgrey'
  },
  trackOff: {
    backgroundColor: 'grey'
  },
  thumbSwitched: {
    backgroundColor: 'red'
  },
  trackSwitched: {
    backgroundColor: 'grey'
  }
};
class DrawerTitle extends Component {
  rightAction = () => {
    this.props.rightAction(
      this.props.secondDrawerName
        ? this.props.secondDrawerName
        : this.props.category
          ? this.props.category
          : ''
    );
  };

  changeShowLayer(e, newToggleValue) {
    if (newToggleValue) {
      this.props.addMapLayerFilter('layersVisibility', this.props.mapLayerFilterName);
    } else {
      this.props.removeMapLayerFilter('layersVisibility', this.props.mapLayerFilterName);
    }
  }

  handleChange = (event, index, value) => {
    this.setState({
      value
    });
    this.props.onSelectClick(value);
  };

  render() {
    const rightButton = (
      <IconButton>
        <FontIcon className="material-icons" onClick={this.rightAction}>
          {this.props.rightIcon}
        </FontIcon>
      </IconButton>
    );

    let leftButton = null;
    if (this.props.leftIcon && this.props.leftAction) {
      leftButton = (
        <IconButton>
          <FontIcon
            className="material-icons"
            onClick={this.props.leftAction.bind(this, this.props.backDrawerType)}
          >
            {this.props.leftIcon}
          </FontIcon>
        </IconButton>
      );
    }

    let title = null;
    if (this.props.multiple) {
      const listTitles = this.props.title.map((a, i) => (
        <MenuItem value={this.props.drawerNames[i]} primaryText={a} label={a} key={i} />
      ));
      title = (
        <DropDownMenu
          value={this.props.category}
          onChange={this.handleChange}
          children={listTitles}
        />
      );
    } else {
      title = <ToolbarTitle style={{ color: 'white' }} text={this.props.title} />;
    }

    const layerSwitch = this.props.mapLayerFilterName ? (
      <Toggle
        defaultToggled={true}
        toggled={
          this.props.mapLayerFilters.layersVisibility.indexOf(this.props.mapLayerFilterName) >= 0
        }
        onToggle={(e, value) => this.changeShowLayer(e, value)}
        thumbStyle={styles.thumbOff}
        trackStyle={styles.trackOff}
        thumbSwitchedStyle={styles.thumbSwitched}
        trackSwitchedStyle={styles.trackSwitched}
      />
    ) : (
      <div />
    );

    return (
      <Toolbar style={{ height: 64 }}>
        <ToolbarGroup>
          {leftButton}
          {title}
        </ToolbarGroup>
        <ToolbarGroup style={{ display: 'flex', justifyContent: 'flex-end' }}>
          {layerSwitch}
          {rightButton}
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

export default enhance(DrawerTitle);
