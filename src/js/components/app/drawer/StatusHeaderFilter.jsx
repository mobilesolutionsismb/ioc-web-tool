import React, { Component } from 'react';
import { IconButton, DropDownMenu, MenuItem } from 'material-ui';
import { withDictionary } from 'ioc-localization';
import { withReportFilters, availableSorters } from 'js/modules/reportFilters';
import { withMissionFilters, availableMissionSorters } from 'js/modules/missionFilters';
import {
  withReportRequestFilters,
  availableReportRequestSorters
} from 'js/modules/reportRequestFilters';
import {
  withEmergencyCommunicationFilters,
  availableEmcommSorters
} from 'js/modules/emcommFilters';
import { withMapRequestFilters, availableMapReqSorters } from 'js/modules/mapRequestFilters';
import {
  withAgentLocationFilters,
  availableSorters as availableAgentLocationSorters
} from 'js/modules/agentLocationFilters';
import { withEmergencyEvents } from 'ioc-api-interface';
import { withLoader, withMessages } from 'js/modules/ui';
import { compose } from 'redux';

const enhance = compose(
  withLoader,
  withMessages,
  withDictionary,
  withReportFilters,
  withReportRequestFilters,
  withEmergencyEvents,
  withEmergencyCommunicationFilters,
  withMissionFilters,
  withMapRequestFilters,
  withAgentLocationFilters
);

export const RefreshButton = ({ onClick, tooltip, disabled }) => (
  <IconButton
    iconClassName="material-icons"
    style={{ padding: '0 12px' }}
    onClick={onClick}
    disabled={disabled}
    tooltip={tooltip}
  >
    refresh
  </IconButton>
);

class StatusHeaderFilter extends Component {
  handleChange = (event, index, value) => {
    this.setState({
      value
    });
    this._setSorter(value);
  };

  _setSorter = async sorterName => {
    try {
      switch (this.props.headerType) {
        case 'report':
          await this.props.setReportSorter(sorterName);
          break;
        case 'event':
          await this.props.setEventSorter(sorterName);
          break;
        case 'reportRequest':
          await this.props.setReportRequestSorter(sorterName);
          break;
        case 'notifications':
          await this.props.setEmcommSorter(sorterName);
          break;
        case 'mission':
          await this.props.setMissionSorter(sorterName);
          break;
        case 'mapRequest':
          await this.props.setMapRequestSorter(sorterName);
          break;
        case 'agentLocation':
          await this.props.setAgentLocationSorter(sorterName);
          break;
        default:
          break;
      }
    } catch (e) {
      console.log('%cError', 'color: white; background: darkRed', e);
      this.props.pushError(e);
    }
  };

  _getMenuItems = () => {
    switch (this.props.headerType) {
      case 'report':
        return availableSorters.map((fv, i) => (
          <MenuItem key={i} value={fv} primaryText={this.props.dictionary(fv)} />
        ));
      case 'event':
        return this.props.availableEventSorters.map((fv, i) => (
          <MenuItem key={i} value={fv} primaryText={this.props.dictionary(fv)} />
        ));
      case 'reportRequest':
        return availableReportRequestSorters.map((fv, i) => (
          <MenuItem key={i} value={fv} primaryText={this.props.dictionary(fv)} />
        ));
      case 'notifications':
        return availableEmcommSorters.map((fv, i) => (
          <MenuItem key={i} value={fv} primaryText={this.props.dictionary(fv)} />
        ));
      case 'mission':
        return availableMissionSorters.map((fv, i) => (
          <MenuItem key={i} value={fv} primaryText={this.props.dictionary(fv)} />
        ));
      case 'mapRequest':
        return availableMapReqSorters.map((fv, i) => (
          <MenuItem key={i} value={fv} primaryText={this.props.dictionary(fv)} />
        ));
      case 'agentLocation':
        return availableAgentLocationSorters.map((fv, i) => (
          <MenuItem key={i} value={fv} primaryText={this.props.dictionary(fv)} />
        ));
      default:
        break;
    }
  };

  render() {
    let { dictionary } = this.props;

    let currentSorter = '';
    switch (this.props.headerType) {
      case 'report':
        currentSorter = this.props.reportSorter;
        break;
      case 'event':
        currentSorter = this.props.eventSorter;
        break;
      case 'reportRequest':
        currentSorter = this.props.reportRequestSorter;
        break;
      case 'notifications':
        currentSorter = this.props.emcommSorter;
        break;
      case 'mission':
        currentSorter = this.props.missionSorter;
        break;
      case 'mapRequest':
        currentSorter = this.props.mapRequestSorter;
        break;
      case 'agentLocation':
        currentSorter = this.props.agentLocationSorter;
        break;
      default:
        currentSorter = '';
        break;
    }

    return (
      <div
        style={{
          backgroundColor: 'black',
          fontSize: '12px',
          display: 'inline-flex',
          alignItems: 'center',
          width: '100%',
          padding: '10px',
          boxSizing: 'border-box'
        }}
      >
        {typeof this.props.refreshItems === 'function' && (
          <RefreshButton tooltip={dictionary('_refresh')} onClick={this.props.refreshItems} />
        )}
        <div style={{ width: '100%' }}>
          <span>
            {this.props.numberOfResults} {this.props.nameOfResults}
          </span>
        </div>
        <div
          style={{
            display: 'inline-flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            whiteSpace: 'noWrap'
          }}
        >
          <div style={{ paddingRight: 5 }} />
          <div>
            <DropDownMenu
              value={currentSorter}
              onChange={this.handleChange}
              autoWidth={true}
              labelStyle={{ fontSize: '12px', paddingLeft: '0px' }}
              listStyle={{ paddingLeft: '0px' }}
              underlineStyle={{ margin: '0' }}
            >
              {this._getMenuItems()}
            </DropDownMenu>
          </div>
        </div>
      </div>
    );
  }
}

export default enhance(StatusHeaderFilter);
