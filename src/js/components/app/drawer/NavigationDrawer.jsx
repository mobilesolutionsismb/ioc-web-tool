import './style.scss';
import React, { Component } from 'react';
import { FontIcon, Paper } from 'material-ui';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withReports, withEmergencyEvents, withMissions } from 'ioc-api-interface';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { drawerNames } from 'js/modules/AreaOfInterest';
const enhance = compose(
  withLeftDrawer,
  withReports,
  withEmergencyEvents,
  withMissions,
  withDictionary
);

const STYLES = {
  disabledLabel: {
    color: 'rgba(255,255,255,0.3)'
  }
};

const NAV_CONFIG = [
  {
    id: drawerNames.event,
    tooltip: '_drawer_label_events',
    disabled: false,
    togglesDrawer: true,
    iconText: 'flag',
    label: '_drawer_label_events',
    className: 'materialdesignicons mdi mdi-flag'
  },
  {
    id: drawerNames.report,
    tooltip: '_drawer_label_reports',
    disabled: false,
    togglesDrawer: true,
    iconText: 'view_list',
    label: '_drawer_label_reports',
    className: 'materialdesignicons mdi mdi-view-list'
  },
  {
    id: drawerNames.notification,
    tooltip: '_drawer_label_alerts_warning',
    disabled: false,
    togglesDrawer: true,
    iconText: 'notifications',
    label: '_drawer_label_alerts_warning',
    className: 'materialdesignicons mdi mdi-bell'
  },
  {
    id: drawerNames.mission,
    tooltip: '_drawer_label_missions',
    disabled: false,
    togglesDrawer: true,
    iconText: 'directions',
    label: '_drawer_label_missions',
    className: 'materialdesignicons mdi mdi-directions'
  },
  {
    id: drawerNames.mapRequests,
    tooltip: '_drawer_label_map_requests',
    disabled: false,
    togglesDrawer: true,
    iconText: 'map',
    label: '_drawer_label_map_requests',
    className: 'materialdesignicons mdi mdi-map'
  },
  {
    id: drawerNames.agentLocations,
    tooltip: '_drawer_label_agent_locations',
    disabled: false,
    togglesDrawer: true,
    iconText: 'person',
    label: '_drawer_label_agent_locations',
    className: 'materialdesignicons mdi mdi-account'
  },
  {
    id: drawerNames.social,
    tooltip: '_drawer_label_social',
    disabled: false,
    togglesDrawer: false,
    iconText: 'people',
    label: '_drawer_label_social',
    className: 'materialdesignicons mdi mdi-twitter'
  }
];

class NavigationDrawer extends Component {
  //1 --> first drawer
  //2 --> second drawer
  //undefined --> unknown drawer type
  _isPrimaryOrSecondaryType(drawerType) {
    const primaryType = [
      drawerNames.report,
      drawerNames.reportRequest,
      drawerNames.reportNewRequest,
      drawerNames.event,
      drawerNames.eventCreate,
      drawerNames.eventEdit,
      drawerNames.mission,
      drawerNames.missionCreate,
      drawerNames.notification,
      drawerNames.social,
      drawerNames.map,
      drawerNames.mapRequests,
      drawerNames.agentLocations
    ];
    const secondaryType = [
      drawerNames.reportRequestFilter,
      drawerNames.reportFilter,
      drawerNames.eventFilter,
      drawerNames.misisonFilter
    ];

    return primaryType.indexOf(drawerType) > -1
      ? 1
      : secondaryType.indexOf(drawerType) > -1
        ? 2
        : undefined;
  }

  componentDidMount() {
    const activeItem = this.props.match.params.activeItem;
    if (activeItem && activeItem !== drawerNames.map) {
      const drawerType = this._isPrimaryOrSecondaryType(activeItem);
      if (drawerType === 1) {
        this.props.setOpenPrimary(true, activeItem);
      } else {
        this.props.setOpenPrimary(false, '');
      }
    } else if (activeItem === drawerNames.map) {
      this.props.setOpenPrimary(false, '');
    }
  }

  componentDidUpdate(prevProps) {
    const netxActiveItem = this.props.match.params.activeItem;
    const prevActiveItem = prevProps.match.params.activeItem;
    if (netxActiveItem !== prevActiveItem) {
      this._setActiveDrawer(netxActiveItem, prevActiveItem, this.props);
    }
  }

  _setActiveDrawer = (nextDrawerType, prevDrawerType, props) => {
    if (nextDrawerType === prevDrawerType) {
      props.setOpenPrimary(false);
      return;
    }
    switch (nextDrawerType) {
      case drawerNames.event:
        props.match.params.activeItem = drawerNames.event;
        props.setOpenPrimary(true, drawerNames.event);
        break;
      case drawerNames.report:
        props.match.params.activeItem = drawerNames.report;
        props.setOpenPrimary(true, drawerNames.report);
        break;
      case drawerNames.mission:
        props.match.params.activeItem = drawerNames.mission;
        props.setOpenPrimary(true, drawerNames.mission);
        break;
      case drawerNames.map:
        props.setOpenPrimary(false);
        props.setOpenSecondary(false);
        props.match.params.activeItem = '';
        break;
      case drawerNames.notification:
        props.setOpenSecondary(false);
        props.match.params.activeItem = drawerNames.notification;
        props.setOpenPrimary(true, drawerNames.notification);
        break;
      case drawerNames.mapRequests:
        props.setOpenSecondary(false);
        props.match.params.activeItem = drawerNames.mapRequests;
        props.setOpenPrimary(true, drawerNames.mapRequests);
        break;
      case drawerNames.agentLocations:
        props.setOpenSecondary(false);
        props.match.params.activeItem = drawerNames.agentLocations;
        props.setOpenPrimary(true, drawerNames.agentLocations);
        break;
      default:
        props.match.params.activeItem = '';
        break;
    }
  };

  // TODO delegate loading to component
  getMissions = async props => {
    try {
      await props.getMissions(props.loadingStart, props.loadingStop);
    } catch (e) {
      console.error('Error', e);
      //throw (e);
    }
  };

  // Return the destination link depending on the active item
  _getDestLink(activeItem, itemId) {
    const dest = activeItem === itemId ? 'map' : itemId;
    return `home/${dest}`;
  }

  // Remap config to items
  _getItems = (palette, dictionary, activeItem) => {
    return NAV_CONFIG.map((nc, i) => {
      const destLink = nc.togglesDrawer ? this._getDestLink(activeItem, nc.id) : nc.id;
      const isLinkActive = nc.togglesDrawer ? (match, location) => activeItem === nc.id : undefined;
      const activeStyle = {
        color: palette.canvasColor,
        backgroundColor: palette.accent1Color
      };
      const style = nc.disabled ? STYLES.disabledLabel : { color: palette.textColor };
      const label = dictionary(nc.label);

      return (
        <div key={i} className={`navigation-drawer-item${nc.disabled ? ' disabled' : ''}`}>
          <NavLink
            exact
            strict
            activeClassName="selected"
            activeStyle={activeStyle}
            style={style}
            isActive={isLinkActive}
            to={`/${destLink}`}
          >
            <FontIcon
              className={nc.className}
              color={
                nc.disabled
                  ? STYLES.disabledLabel.color
                  : activeItem === nc.id
                    ? palette.canvasColor
                    : palette.textColor
              }
            >
              {/* {nc.iconText}  */}
            </FontIcon>
            {/* <FontIcon className="materialdesignicons mdi mdi-twitter" /> */}
            <span
              className="navigation-drawer-label"
              style={nc.disabled ? STYLES.disabledLabel : {}}
            >
              {label}
            </span>
          </NavLink>
        </div>
      );
    });
  };

  render() {
    const { dictionary, muiTheme, match } = this.props;
    const activeItem = match.path === '/social' ? drawerNames.social : match.params.activeItem;

    const palette = muiTheme.palette;
    const style = {
      backgroundColor: palette.canvasColor,
      color: palette.textColor
    };

    return (
      <Paper rounded={false} zDepth={2} className="navigation-drawer" style={style}>
        {this._getItems(palette, dictionary, activeItem)}
      </Paper>
    );
  }
}

export default withRouter(muiThemeable()(enhance(NavigationDrawer)));
