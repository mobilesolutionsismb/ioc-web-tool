import React, { Component } from 'react';
import { Divider, RaisedButton, List, ListItem } from 'material-ui';

class DrawerFooter extends Component {
  render() {
    const { customStyle, entityId } = this.props;
    return (
      <div className="drawerFilterButton" style={customStyle}>
        <Divider />
        <List>
          <ListItem disabled={true}>
            <div>
              {this.props.clear && (
                <RaisedButton
                  disabled={entityId !== 0}
                  label={this.props.leftLabel}
                  onClick={this.props.clear}
                  style={{ position: 'relative', left: '5%' }}
                />
              )}
              {this.props.apply && (
                <RaisedButton
                  primary={true}
                  label={this.props.rightLabel}
                  onClick={this.props.apply}
                  style={{ position: 'relative', left: '30%' }}
                />
              )}
            </div>
          </ListItem>
        </List>
      </div>
    );
  }
}

export default DrawerFooter;
