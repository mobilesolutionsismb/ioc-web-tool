import React, { Component } from 'react';
import { IconButton } from 'material-ui';
import { withDictionary } from 'ioc-localization';
import styled, { css, keyframes } from 'styled-components';

const spin = keyframes`
{
  0% {
    transform: rotateZ(0);
  }
  100% {
    transform: rotateZ(360deg);
  }
}`;

const spinAnimation = props =>
  css`
    ${spin} 1s linear infinite;
  `;

const StyledIconButton = styled(IconButton)`
  padding: 0 12px;

  @keyframes spin {
    0% {
      transform: rotateZ(0);
    }
    100% {
      transform: rotateZ(360deg);
    }
  }

  & span.material-icons {
    color: ${props =>
      props.loading === 'true' ? props.theme.palette.accent1Color : ''} !important;
    transition: color 0.3s cubic-bezier(0.075, 0.82, 0.165, 1);
    animation: ${props => (props.loading === 'true' ? spinAnimation : 'none')};
  }
`;

class RefreshButton extends Component {
  static defaultProps = {
    loading: false,
    tooltip: '',
    refreshItems: () => new Promise(succ => setTimeout(succ, 1000))
  };

  _manualReload = async () => {
    try {
      await this.props.refreshItems();
      console.log('Refresh success');
    } catch (e) {
      console.error(e);
    }
  };

  render() {
    const { dictionary, tooltip, disabled, style, loading } = this.props;
    return (
      <StyledIconButton
        loading={loading + ''}
        iconClassName="material-icons"
        style={style}
        onClick={this._manualReload}
        disabled={disabled || loading}
        tooltip={dictionary(tooltip)}
      >
        refresh
      </StyledIconButton>
    );
  }
}

export default withDictionary(RefreshButton);
