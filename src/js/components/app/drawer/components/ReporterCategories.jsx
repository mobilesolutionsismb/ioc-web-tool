import React, { Component } from 'react';
import { /* SelectField, MenuItem, */ ListItem, Checkbox, List } from 'material-ui';

class ReporterCategories extends Component {
  render() {
    const { /* dictionary, */ roles } = this.props;

    // let statusChildren = (
    // <div key={ 0 }>
    //   { this.props.reportersAllowed.filter(e => e === dictionary._is_citizen).length > 0 &&
    //     <SelectField disabled={ this.props.isDisabled } floatingLabelText="with status higher than" value={ this.props.minimumLevel } onChange={ this.props.handleReportersAllowedLevelChange } style={ { paddingTop: 0 } }>
    //       { Object.keys(this.props.levelTypes).map((element, index) => <MenuItem key={ index } value={ element } primaryText={ dictionary('_' + element) } />
    //         )  }
    //     </SelectField> }
    // </div>
    // );

    // let reporterChild = (
    //   <div style={{ display: 'flex', alignItems: 'center' }}>
    //     <div>
    //       <Checkbox
    //         disabled={this.props.isDisabled}
    //         value="Team"
    //         checked={this.props.reportersAllowed.filter(e => e === 'Team').length > 0}
    //         onClick={this.props.handleReportersAllowedChange}
    //       />
    //     </div>
    //     <div>
    //       <SelectField
    //         disabled={
    //           this.props.isDisabled ||
    //           this.props.reportersAllowed.filter(e => e === 'Team').length === 0
    //         }
    //         multiple={true}
    //         style={{ paddingTop: 0, width: 215 }}
    //         hintText="Team List"
    //         onChange={this.props.handleReportersAllowedTeamChange}
    //         value={this.props.teams}
    //       >
    //         <MenuItem key={1} value="Team 1" primaryText="Team 1" />
    //         <MenuItem key={2} value="Team 2" primaryText="Team 2" />
    //         <MenuItem key={3} value="Team 3" primaryText="Team 3" />
    //         <MenuItem key={4} value="Team 4" primaryText="Team 4" />
    //         <MenuItem key={5} value="Team 5" primaryText="Team 5" />
    //       </SelectField>
    //     </div>
    //   </div>
    // );

    return (
      <div>
        <div>
          <List style={{ whiteSpace: 'noWrap' }}>
            {/* <ListItem
              disabled={true}
              style={{ marginBottom: '20px' }}
              leftCheckbox={reporterChild}
            /> */}
            {roles ? (
              roles
                .filter(
                  role =>
                    role.displayName !== 'Admin' &&
                    role.displayName !== 'User' &&
                    role.displayName !== 'Citizen'
                )
                .map((role, i) => (
                  <ListItem
                    key={i}
                    disabled={true}
                    leftCheckbox={
                      <Checkbox
                        disabled={this.props.isDisabled}
                        checked={
                          this.props.reportersAllowed.filter(e => {
                            if (e.id) return e.id === role.id;
                            else return e === role.id;
                          }).length > 0
                        }
                        value={role.id}
                        label={role.displayName}
                        onClick={this.props.handleReportersAllowedChange}
                      />
                    }
                  />
                ))
            ) : (
              <div />
            )}
            {/* <ListItem disabled={ true } children={ statusChildren } /> */}
          </List>
        </div>
        {this.props.renderStepActions}
      </div>
    );
  }
}

export default ReporterCategories;
