import React from 'react';
import { FlatButton, Dialog } from 'material-ui';
import { withAreaOfInterest } from 'js/modules/AreaOfInterest';
import { withTaskCreate } from 'js/modules/taskCreate';

const customContentStyle = {
  position: 'absolute',
  left: '25%',
  bottom: '10%'
};

class ConfirmDialog extends React.Component {
  handleCancel = () => {
    //TODO: find a way to cancel just the correct AOI
    this.props.resetDrawState(this.props.draw.activeDraw);
    this.props.resetDrawState('target_aoi');

    const mapDraw = this.props.mapBoxDraw;
    if (mapDraw !== null) mapDraw.deleteAll();
  };

  handleSubmit = () => {
    if (this.props.draw[this.props.draw.activeDraw].currentStep !== 0)
      this.props.setDrawCurrentStep(this.props.draw.activeDraw, 0);
    else this.props.setDrawCurrentStep('target_aoi', 0);

    if (this.props.draw.activeDraw === 'task_create') this.props.toggleTaskPopup(true);
  };

  render() {
    const { dictionary } = this.props;

    const actions = [
      <FlatButton label={dictionary._cancel} primary={true} onClick={this.handleCancel} />,
      <FlatButton label={dictionary._submit} primary={true} onClick={this.handleSubmit} />
    ];

    return (
      <div>
        <Dialog
          title={dictionary._aoi}
          actions={actions}
          modal={true}
          contentStyle={customContentStyle}
          open={this.props.visible}
        >
          {dictionary._dialogConfirmMessage}
        </Dialog>
      </div>
    );
  }
}

export default withTaskCreate(withAreaOfInterest(ConfirmDialog));
