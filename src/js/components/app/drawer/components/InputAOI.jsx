import React, { Component } from 'react';
import { TextField, IconButton } from 'material-ui';
//import { withReportFilters } from 'js/modules/reportFilters';
//import { withAreaOfInterest } from 'js/modules/AreaOfInterest';
import { withMessages } from 'js/modules/ui';
import { compose } from 'redux';
const enhance = compose(withMessages);

class InputAOI extends Component {
  handleAOIChange = e => {
    if (!this.props.isDisabled) {
      const mapDraw = this.props.getMapDraw();
      if (mapDraw) {
        mapDraw.changeMode('point_mode', {
          category: this.props.category,
          setDrawPoint: drawPoint => this.props.setDrawPoint(drawPoint),
          setDrawPolygon: drawPolygon => this.props.setDrawPolygon(drawPolygon),
          setDrawCategory: drawCategory => this.props.setDrawCategory(drawCategory)
        });
      }
    }
  };

  resetState = () => {
    const mapDraw = this.props.getMapDraw();
    if (this.props.drawPoint.id && this.props.drawPolygon.id) {
      const ids = [this.props.drawPoint.id, this.props.drawPolygon.id];
      if (mapDraw) mapDraw.delete(ids);
    } else {
      if (mapDraw) mapDraw.deleteAll();
    }
    this.props.resetHomeDrawState(this.props.resetCategory);
  };

  render() {
    const text =
      this.props.drawPoint && this.props.drawPoint.coordinates
        ? '[ ' +
          Math.round(this.props.drawPoint.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(this.props.drawPoint.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    return (
      <div>
        <div
          style={{
            fontSize: 14,
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center'
          }}
        >
          {text ? (
            <div
              style={{
                fontSize: 14,
                display: 'flex',
                justifyContent: 'flex-start',
                alignItems: 'center'
              }}
            >
              <div
                style={{
                  fontSize: 14,
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'flex-start'
                }}
              >
                {text}
              </div>
              <div>
                <IconButton>
                  <i className="material-icons md-18 md-light" onClick={this.resetState}>
                    close
                  </i>
                </IconButton>
              </div>
            </div>
          ) : (
            <TextField disabled={true} hintText="AOI center" />
          )}
        </div>
        <div
          style={{
            fontSize: 10,
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center'
          }}
        >
          <span>Draw Area on map</span>
          <IconButton>
            <i className="material-icons md-18 md-light" onClick={e => this.handleAOIChange(e)}>
              mode_edit
            </i>
          </IconButton>
        </div>
      </div>
    );
  }
}

export default enhance(InputAOI);
