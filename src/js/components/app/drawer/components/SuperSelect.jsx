import React, { Component } from 'react';
import { Chip, Avatar, FlatButton } from 'material-ui';
import SuperSelectField from 'material-ui-superselectfield';

const menuItemStyle = {
  whiteSpace: 'normal',
  display: 'flex',
  justifyContent: 'space-between',
  lineHeight: 'normal'
};

class SuperSelect extends Component {
  handleCheck = labels => {
    console.log(labels);
    let args = {};
    if (Array.isArray(labels)) {
      for (let i = 0; i < labels.length; i++) {
        args[labels[i].value] = true;
      }
    } else {
      args = labels;
    }

    this.props.updateFilters(this.props.targetEnumKey, args);
  };

  handleCustomDisplaySelections = labels => values =>
    Array.isArray(labels) ? (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        {labels.length > 0 ? (
          labels.map(({ label, value }, index) => (
            <div key={index}>
              {this.props.hideIcons ? (
                <Chip
                  key={index}
                  style={{ margin: 5 }}
                  onRequestDelete={this.onRequestDelete(labels, label, value)}
                >
                  <div>
                    <span>{label}</span>
                  </div>
                </Chip>
              ) : (
                <Chip
                  key={index}
                  style={{ margin: 5 }}
                  onRequestDelete={this.onRequestDelete(labels, label, value)}
                >
                  <div>
                    <Avatar size={30}>{this.props.arrayIcons[label]}</Avatar>
                    <span>{label}</span>
                  </div>
                </Chip>
              )}
            </div>
          ))
        ) : (
          <div style={{ minHeight: 42, lineHeight: '42px' }}>{this.props.hintText}</div>
        ) // advice: use one of <option>s' default height as min-height
        }
      </div>
    ) : (
      <div>
        {labels.value === 0 ? (
          <div style={{ minHeight: 42, lineHeight: '42px' }}>{this.props.hintText}</div>
        ) : this.props.hideIcons ? (
          <Chip
            style={{ margin: 5 }}
            onRequestDelete={this.onRequestDelete(labels, labels.label, labels.value)}
          >
            <div>
              <span>{labels.label}</span>
            </div>
          </Chip>
        ) : (
          <Chip
            style={{ margin: 5 }}
            onRequestDelete={this.onRequestDelete(labels, labels.label, labels.value)}
          >
            <div>
              <Avatar size={30}>{this.props.arrayIcons[labels.label]}</Avatar>
              <span>{labels.label}</span>
            </div>
          </Chip>
        )}
      </div>
    );

  onRequestDelete = (allLabels, label, value) => event => {
    let args = {};
    let labels;
    if (Array.isArray(allLabels)) {
      labels = allLabels.filter((v, i) => v.value !== value);
      for (let i = 0; i < labels.length; i++) {
        args[labels[i].value] = true;
      }
    } else {
      args = {};
    }

    this.props.updateFilters(this.props.targetEnumKey, args);
  };

  render() {
    const { targetEnum, currentFilterValue, multipleChoice, dictionary } = this.props;
    let labels;
    if (multipleChoice) {
      labels = new Array(currentFilterValue.length);
      for (let i = 0; i < currentFilterValue.length; i++) {
        labels[i] = {};
        labels[i].value = currentFilterValue[i];
        labels[i].label = dictionary[currentFilterValue[i]];
      }
    } else {
      if (currentFilterValue.value && currentFilterValue.value !== 0) {
        labels = {};
        labels.value = currentFilterValue.value;
        labels.label = currentFilterValue.label;
      } else
        labels = {
          value: 0
        };
    }

    const dataSourceNodes = Object.keys(targetEnum).map((e, i) => {
      let ob = targetEnum[e];
      return (
        <div key={i} value={ob.value} label={dictionary[ob.label]} style={menuItemStyle}>
          <div style={{ marginRight: 10 }}>
            <span style={{ fontWeight: 'bold' }}>{dictionary[ob.label]}</span>
            <br />
          </div>
        </div>
      );
    });

    return (
      <div style={{ display: 'flex', alignItems: 'flex-end' }}>
        {multipleChoice ? (
          <SuperSelectField
            name="Select"
            multiple
            checkPosition="left"
            onChange={this.handleCheck}
            value={labels}
            elementHeight={40}
            selectionsRenderer={this.handleCustomDisplaySelections(labels)}
            style={{ width: 250 }}
            menuCloseButton={<FlatButton label="close" hoverColor={'rgb(77, 182, 172)'} />}
          >
            {dataSourceNodes}
          </SuperSelectField>
        ) : (
          <SuperSelectField
            name="Select"
            checkPosition="left"
            onChange={this.handleCheck}
            value={labels}
            elementHeight={40}
            selectionsRenderer={this.handleCustomDisplaySelections(labels)}
            style={{ width: 250 }}
          >
            {dataSourceNodes}
          </SuperSelectField>
        )}
      </div>
    );
  }
}

export default SuperSelect;
