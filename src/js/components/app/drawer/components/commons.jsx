import React from 'react';
// import { LocationOnIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import FontIcon from 'material-ui/FontIcon';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { localizeDate } from 'js/utils/localizeDate';
// import { TimePicker, DatePicker } from 'material-ui';

const STYLE = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  justifyContent: 'flex-start',
  fontSize: '10px'
};

const ITEM_STYLE = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: 1
};

export const AOILabel = muiThemeable()(({ text, dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._aoi} *</span>
    </div>
    {text && text !== '' ? (
      <div style={ITEM_STYLE}>
        <FontIcon className="material-icons" style={{ fontSize: 18 }}>
          location_on
        </FontIcon>
        <span>{text}</span>
      </div>
    ) : (
      <div />
    )}
  </div>
));

export const TimeLabel = muiThemeable()(
  ({ startDate, startTime, endDate, endTime, dictionary, locale }) => (
    <div style={STYLE}>
      <div style={{ fontSize: 14 }}>
        <span>{dictionary._event_time} *</span>
      </div>
      <div>
        {startDate !== null || startTime !== null ? (
          <div style={ITEM_STYLE}>
            <FontIcon className="material-icons" style={{ fontSize: 18 }}>
              schedule
            </FontIcon>
            <div style={{ paddingRight: 2, paddingLeft: 2 }}>{dictionary._start}:</div>{' '}
            {startDate ? (
              <div style={{ paddingRight: 2 }}>
                {localizeDate(startDate, locale, 'L LT', true)}{' '}
              </div>
            ) : (
              <div />
            )}
            {startTime ? <div> {localizeDate(startTime, locale, 'L LT', true)} </div> : <div />}
          </div>
        ) : (
          <div />
        )}
        {endDate !== null || endDate !== null ? (
          <div style={ITEM_STYLE}>
            <div style={{ paddingRight: 2, paddingLeft: 20 }}>{dictionary._end}:</div>{' '}
            {endDate ? (
              <div style={{ paddingRight: 2 }}>{localizeDate(endDate, locale, 'L LT', true)} </div>
            ) : (
              <div />
            )}
            {endTime ? <div> {localizeDate(endTime, locale, 'L LT', true)} </div> : <div />}
          </div>
        ) : (
          <div />
        )}
      </div>
    </div>
  )
);

export const ContentTypeLabel = muiThemeable()(({ contentType, dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._content_type} *</span>
    </div>
    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
      {contentType.map((content, i) => {
        let displayName = dictionary['_' + content];
        if (i !== contentType.length - 1) displayName += ',';

        return (
          <span key={i} style={{ fontSize: 10, marginRight: 2 }}>
            {displayName}
          </span>
        );
      })}
    </div>
  </div>
));

export const ReportersAllowedLabel = muiThemeable()(({ roles, reportersAllowed, dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._reporter_cat_allowed} *</span>
    </div>
    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
      {reportersAllowed.map((reporterId, i) => {
        let displayName = roles
          .filter(item => item.id === reporterId)
          .map(item => item.displayName)[0];
        if (i !== reportersAllowed.length - 1) displayName += ',';

        return (
          <span key={i} style={{ fontSize: 10, marginRight: 2 }}>
            {displayName}
          </span>
        );
      })}
    </div>
  </div>
));

export const TitleLabel = muiThemeable()(({ title, dictionary }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._title} *</span>
    </div>
    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
      <span style={{ fontSize: 10, marginRight: 2 }}>
        {title ? (title.length > 65 ? title.substring(0, 65) + '...' : title) : ''}
      </span>
    </div>
  </div>
));

export const WebLabel = muiThemeable()(({ web }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>Link</span>
    </div>
    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
      <span style={{ fontSize: 10, marginRight: 2 }}>
        {web && web.length > 65 ? web.substring(0, 65) + '...' : web}
      </span>
    </div>
  </div>
));

export const AssignToLabel = muiThemeable()(({ dictionary, teamName }) => (
  <div style={STYLE}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._assign_to} *</span>
    </div>
    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
      <span style={{ fontSize: 10, marginRight: 2 }}> {teamName}</span>
    </div>
  </div>
));

export const TasksLabel = muiThemeable()(({ dictionary }) => (
  <div style={{ ...STYLE, marginBottom: 10 }}>
    <div style={{ fontSize: 14 }}>
      <span>{dictionary._tasks_creation} *</span>
    </div>
    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
      <span style={{ fontSize: 10, marginRight: 2 }} />
    </div>
  </div>
));
