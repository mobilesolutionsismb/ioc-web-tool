import React, { Component } from 'react';
import { compose } from 'redux';
import { Checkbox } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { withReportFilters } from 'js/modules/reportFilters';
import { withMissionFilters } from 'js/modules/missionFilters';
import { withEventFilters } from 'js/modules/clientSideEventFilters';
import { withReportRequestFilters } from 'js/modules/reportRequestFilters';
import { withEmergencyCommunicationFilters } from 'js/modules/emcommFilters';
import { withMapRequestFilters } from 'js/modules/mapRequestFilters';
import { withAgentLocationFilters } from 'js/modules/agentLocationFilters';
import { withDictionary } from 'ioc-localization';
//import { withIREACTFeatures } from 'ioc-api-interface';
// import CheckboxOutline from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import CheckboxChecked from 'material-ui/svg-icons/toggle/check-box';
import RefreshButton from './RefreshButton';

const ColoredCheckbox = ({ color }) => <CheckboxChecked color={color} />;
const enhance = compose(
  withDictionary,
  withReportFilters,
  withReportRequestFilters,
  withEmergencyCommunicationFilters,
  withEventFilters,
  withMissionFilters,
  withAgentLocationFilters,
  /* withIREACTFeatures, */ withMapRequestFilters
);

const STYLES = {
  radioButton: {
    width: 'auto',
    maxWidth: 'calc(33% - 4px)',
    padding: '0 2px'
  },
  cbxLabel: {
    float: 'none',
    left: -10,
    fontSize: 11,
    lineHeight: '24px',
    width: '100%'
  }
};

class LeftDrawerFilterHeader extends Component {
  onCheck = (event, isInputChecked) => {
    const filterCategory = event.currentTarget.name;
    const filterName = event.currentTarget.value;
    // The "Validated only" filter is equivalent to remove all status filters but _is_status_validated
    switch (this.props.headerType) {
      case 'report':
        if (filterName === '_is_status_validated') {
          if (isInputChecked) {
            this.props.removeReportFilter(filterCategory, '_is_status_inappropriate');
            this.props.removeReportFilter(filterCategory, '_is_status_inaccurate');
            this.props.removeReportFilter(filterCategory, '_is_status_submitted');
            this.props.addReportFilter(filterCategory, '_is_status_validated');
          } else {
            this.props.removeReportFilter(filterCategory, '_is_status_validated');
          }
        } else {
          if (isInputChecked) {
            this.props.addReportFilter(filterCategory, filterName);
          } else {
            this.props.removeReportFilter(filterCategory, filterName);
          }
        }
        break;
      case 'reportRequest':
        if (isInputChecked) this.props.addReportRequestFilter(filterCategory, filterName);
        else this.props.removeReportRequestFilter(filterCategory, filterName);
        break;
      case 'notifications':
        if (isInputChecked) this.props.addEmcommFilter(filterCategory, filterName);
        else this.props.removeEmcommFilter(filterCategory, filterName);
        break;
      case 'event':
        if (isInputChecked) this.props.addEventFilter(filterCategory, filterName);
        else this.props.removeEventFilter(filterCategory, filterName);
        break;
      case 'mission':
        if (isInputChecked) this.props.addMissionFilter(filterCategory, filterName);
        else this.props.removeMissionFilter(filterCategory, filterName);
        break;
      case 'mapRequest':
        if (isInputChecked) this.props.addMapRequestFilter(filterCategory, filterName);
        else this.props.removeMapRequestFilter(filterCategory, filterName);
        break;
      case 'agentLocation':
        if (isInputChecked) this.props.addAgentLocationFilter(filterCategory, filterName);
        else this.props.removeAgentLocationFilter(filterCategory, filterName);
        break;
      default:
        break;
    }

    //Disable selected item when a fliter is activated
    if (isInputChecked) {
      if (this.props.headerType !== 'event') this.props.deselectFeature();
      else this.props.deselectEvent();
    }
  };

  _isActiveFilter = (filterCategory, filterName, nextProps = null) => {
    const props = nextProps ? nextProps : this.props;

    switch (props.headerType) {
      case 'report':
        if (filterCategory !== 'status') {
          return (
            props.reportFilters.hasOwnProperty(filterCategory) &&
            Array.isArray(props.reportFilters[filterCategory]) &&
            props.reportFilters[filterCategory].indexOf(filterName) > -1
          );
        } else
          return (
            props.reportFilters.status.length === 1 &&
            props.reportFilters.status.indexOf('_is_status_validated') > -1
          );
      case 'reportRequest':
        return (
          props.reportRequestFilters.hasOwnProperty(filterCategory) &&
          Array.isArray(props.reportRequestFilters[filterCategory]) &&
          props.reportRequestFilters[filterCategory].indexOf(filterName) > -1
        );
      case 'notifications':
        return (
          props.emcommFilters.hasOwnProperty(filterCategory) &&
          Array.isArray(props.emcommFilters[filterCategory]) &&
          props.emcommFilters[filterCategory].indexOf(filterName) > -1
        );
      case 'event':
        return (
          props.feEventFilters.hasOwnProperty(filterCategory) &&
          Array.isArray(props.feEventFilters[filterCategory]) &&
          props.feEventFilters[filterCategory].indexOf(filterName) > -1
        );
      case 'mission':
        return (
          props.missionFilters.hasOwnProperty(filterCategory) &&
          Array.isArray(props.missionFilters[filterCategory]) &&
          props.missionFilters[filterCategory].indexOf(filterName) > -1
        );
      case 'mapRequest':
        return (
          props.mapRequestFilters.hasOwnProperty(filterCategory) &&
          Array.isArray(props.mapRequestFilters[filterCategory]) &&
          props.mapRequestFilters[filterCategory].indexOf(filterName) > -1
        );
      case 'agentLocation':
        return (
          props.agentLocationFilters.hasOwnProperty(filterCategory) &&
          Array.isArray(props.agentLocationFilters[filterCategory]) &&
          props.agentLocationFilters[filterCategory].indexOf(filterName) > -1
        );
      default:
        return false;
    }
  };

  render() {
    const { muiTheme, dictionary, refreshItems, loading } = this.props;
    const palette = muiTheme.palette;
    //const authChecked = this._isActiveFilter('userType', '_is_authority');
    //const citizenChecked = this._isActiveFilter('userType', '_is_citizen');
    //const validatedChecked = filters.status.length === 1 && filters.status.indexOf('_is_status_validated') > -1;

    return (
      <div className="reports-list-filters">
        <RefreshButton tooltip={'_refresh'} refreshItems={refreshItems} loading={loading} />
        {this.props.checkboxes.map((item, index) => (
          <Checkbox
            key={index}
            checkedIcon={
              this._isActiveFilter(item.type, item.value) ? (
                <ColoredCheckbox color={palette[item.paletteColor || 'textColor']} />
              ) : (
                undefined
              )
            }
            className="report-filter-cbx"
            value={item.value}
            name={item.type}
            label={<span className="filter-cbx-label">{dictionary(item.label)}</span>}
            checked={this._isActiveFilter(item.type, item.value)}
            labelStyle={STYLES.cbxLabel}
            style={STYLES.radioButton}
            onCheck={this.onCheck}
          />
        ))}
      </div>
    );
  }
}

export default enhance(muiThemeable()(LeftDrawerFilterHeader));
