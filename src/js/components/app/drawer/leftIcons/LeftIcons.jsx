import './style.scss';
import React from 'react';
import { FontIcon, IconButton } from 'material-ui';

const iconStyles = {
  fontSize: 36
};

function iconFactory(iconText) {
  return props => (
    <IconButton {...props}>
      <FontIcon className="material-icons" style={iconStyles}>
        {iconText}
      </FontIcon>
    </IconButton>
  );
}

export const WarningIcon = iconFactory('warning');
export const LocationOnIcon = iconFactory('location_on');
export const LocalSeeIcon = iconFactory('local_see');
export const PersonIcon = iconFactory('person');
export const ModeEditIcon = iconFactory('mode_edit');
export const KeyboardBackspaceIcon = iconFactory('keyboard_backspace');
export const DonutLargeIcon = iconFactory('donut_large');
export const ReplyIcon = iconFactory('reply');
export const CloseIcon = iconFactory('close');
export const ShareIcon = iconFactory('share');
export const PhoneIcon = iconFactory('phone');
export const ScheduleIcon = iconFactory('schedule');
export const FullScreenIcon = iconFactory('fullscreen');
export const EventIcon = iconFactory('event');
export const DateRangeIcon = iconFactory('date_range');
export const MoneyIcon = iconFactory('attach_money');
export const VerticalDotsIcon = iconFactory('more_vert');
export const FlightIcon = iconFactory('flight');
export const PlaceIcon = iconFactory('place');
export const PeopleIcon = iconFactory('people');
export const ResourceIcon = iconFactory('local_hospital');
export const MeasureIcon = iconFactory('multiline_chart');
export const DeleteIcon = iconFactory('delete');
export const DoneIcon = iconFactory('done');
export const ArrowIcon = iconFactory('trending_flat');
export const PowerIcon = iconFactory('power_settings_new');
export const AccessibilityIcon = iconFactory('accessibility');
export const TollIcon = iconFactory('toll');
export const EmailIcon = iconFactory('email');
export const CopyrightIcon = iconFactory('copyright');
export const SmileIcon = iconFactory('sentiment_satisfied_alt');
export const BatteryIcon = iconFactory('battery_std');
export const DirectionWalkIcon = iconFactory('directions_walk');
export const DropIcon = iconFactory('opacity');
export const AirConditionedIcon = iconFactory('ac_unit');
export const BuildIcon = iconFactory('build');
