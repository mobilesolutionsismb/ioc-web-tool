import React, { Component } from 'react';
import { FloatingActionButton } from 'material-ui';
import ContentAdd from 'material-ui/svg-icons/content/add';

const floatingButtonStyle = {
  position: 'absolute',
  left: 300,
  bottom: 10
};

class AddActionButton extends Component {
  state = {};

  render() {
    return (
      <div className="addActionButton">
        <FloatingActionButton
          onClick={this.props.addAction}
          mini={true}
          style={floatingButtonStyle}
        >
          <ContentAdd />
        </FloatingActionButton>
      </div>
    );
  }
}

export default AddActionButton;
