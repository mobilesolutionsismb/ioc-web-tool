import './styles.scss';
import React, { Component } from 'react';
import { DatePicker, FontIcon, TimePicker } from 'material-ui';
import { compose } from 'redux';
import { RefreshIndicator } from 'material-ui';
import styled from 'styled-components';
// import areIntlLocalesSupported from 'intl-locales-supported';
import { localizeDate } from 'js/utils/localizeDate';

import { withLoader, withMessages } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import {
  withEmergencyEvents,
  withAPISettings /* , withIREACTFeatures */
} from 'ioc-api-interface';
// import { withTopBarDate } from 'js/modules/topBarDate';

const enhance = compose(
  withLoader,
  withMessages,
  withDictionary,
  withEmergencyEvents,
  withAPISettings /* , withIREACTFeatures */
);

const SmallLoaderWrapper = styled.div`
  position: relative;
  height: 100%;
`;

const SmallLoader = () => (
  <SmallLoaderWrapper>
    <RefreshIndicator size={40} top={-20} left={10} status={'loading'} />
  </SmallLoaderWrapper>
);

class DoubleDatePicker extends Component {
  _changeDateOrTimes = async (dateStart, dateEnd) => {
    try {
      if (new Date(dateStart) > new Date(dateEnd)) {
        const e = new Error('Start date must be smaller than End date');
        this.props.pushError(e);
        return;
      }
      // this.props.setLiveTimeWindowWithDates(dateStart, dateEnd, this.props.loadingStart, this.props.loadingStop);
      await this.props.setLiveTimeWindowWithDates(
        dateStart,
        dateEnd /* , this.updateStart, this.updateEnd */
      );
      // Provisional for demo of 12/01/2018
      this.props.pushMessage('All data updated successfully', 'success');
    } catch (err) {
      console.error('Error changing live time window', err);
      this.props.pushError(err);
    }
  };

  // Valid for both time and date
  _handleStartDateChange = (event, newDateStart) => {
    const { liveTW } = this.props;
    console.log('New START date', newDateStart, 'Old START date', new Date(liveTW.dateStart));
    this._changeDateOrTimes(newDateStart, liveTW.dateEnd);
  };

  // Valid for both time and date
  _handleEndDateChange = (event, newDateEnd) => {
    const { liveTW } = this.props;
    console.log('New END date', newDateEnd, 'Old END date', new Date(liveTW.dateEnd));
    this._changeDateOrTimes(liveTW.dateStart, newDateEnd);
  };

  _formatDate = date => {
    return localizeDate(date, this.props.locale, 'll');
  };

  render() {
    const { dictionary, activeEventTW, liveTW, ireactFeaturesIsUpdating } = this.props;
    const eventMode = activeEventTW !== null;
    const disablePicker = eventMode || ireactFeaturesIsUpdating === true;
    const dateStart = new Date(eventMode ? activeEventTW.dateStart : liveTW.dateStart);
    const dateEnd = new Date(eventMode ? activeEventTW.dateEnd : liveTW.dateEnd);

    return (
      <div>
        <span>{dictionary._from.toUpperCase()} </span>
        <DatePicker
          id="date-start"
          className="datePicker"
          value={dateStart}
          disabled={disablePicker}
          formatDate={this._formatDate}
          // DateTimeFormat={DateTimeFormat}
          okLabel={dictionary._ok}
          cancelLabel={dictionary._cancel}
          onChange={this._handleStartDateChange}
        />

        <TimePicker
          id="time-start"
          className="timePicker"
          value={dateStart}
          format={'24hr'}
          disabled={disablePicker} //ASK Qhnu
          okLabel={dictionary._ok}
          cancelLabel={dictionary._cancel}
          onChange={this._handleStartDateChange}
        />
        <FontIcon className="material-icons" style={{ fontSize: '15px' }}>
          arrow_forward
        </FontIcon>
        <span>{dictionary._to.toUpperCase()} </span>
        <DatePicker
          id="date-end"
          className="datePicker"
          value={dateEnd}
          disabled={disablePicker}
          formatDate={this._formatDate}
          // DateTimeFormat={DateTimeFormat}
          okLabel={dictionary._ok}
          cancelLabel={dictionary._cancel}
          onChange={this._handleEndDateChange}
        />
        <TimePicker
          id="time-end"
          className="timePicker"
          value={dateEnd}
          format={'24hr'} //ASK Qhnu
          disabled={disablePicker}
          textFieldStyle={{ color: 'gold' }}
          okLabel={dictionary._ok}
          cancelLabel={dictionary._cancel}
          onChange={this._handleEndDateChange}
        />
        {ireactFeaturesIsUpdating && <SmallLoader />}
      </div>
    );
  }
}

export default enhance(DoubleDatePicker);
