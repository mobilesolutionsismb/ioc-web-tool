import React, { useEffect, useRef } from 'react';
import { useIREACTFeatures } from 'js/hooks/use-ireactfeatures.hook';
import { logMain } from 'js/utils/log';

/**
 * @typedef {React.Props} ReloaderProps
 * @property {Number} frequency=30000
 *
 * @param {ReloaderProps} props
 * @returns {React.Component}
 */
export function LiveReloader({ frequency = 30000 }) {
  const [
    { ireactFeaturesLastUpdated, ireactFeaturesIsUpdating },
    { updateIREACTFeatures }
  ] = useIREACTFeatures();

  const intervalRef = useRef(null);

  useEffect(() => {
    intervalRef.current = setInterval(() => {
      if (!ireactFeaturesIsUpdating && Date.now() - ireactFeaturesLastUpdated > frequency) {
        logMain('Updating features...');
        updateIREACTFeatures();
      }
    }, frequency);
    return () => {
      clearInterval(intervalRef.current);
      intervalRef.current = null;
    };
  });

  return <span>LIVE</span>;
}
