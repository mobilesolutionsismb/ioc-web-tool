import React, { Component } from 'react';
import FontIcon from 'material-ui/FontIcon';
import { withMap } from 'js/modules/map';
import { withDictionary } from 'ioc-localization';
import { compose } from 'redux';

const enhance = compose(
  withDictionary,
  withMap
);
const PRECISION = 3;
class ExtraInformation extends Component {
  static defaultProps = {
    clickedPoint: null,
    center: null
  };

  _toFixed(point) {
    if (Array.isArray(point) && typeof point[0] === 'number' && typeof point[1] === 'number') {
      return `Lat: ${point[1].toFixed(PRECISION)}, Lng: ${point[0].toFixed(PRECISION)}`;
    } else return '';
  }

  _getDisplayPoint = () => {
    if (this.props.clickedPoint) {
      return this._toFixed(this.props.clickedPoint);
    } else if (this.props.center) {
      return `Map Center ${this._toFixed(this.props.center)}`;
    } else {
      return '';
    }
  };

  render() {
    return (
      <div style={{ width: 350, minWidth: '127px' }}>
        <FontIcon className="material-icons" style={{ fontSize: '15px' }}>
          room
        </FontIcon>
        <span>{this._getDisplayPoint()}</span>
      </div>
    );
  }
}

export default enhance(ExtraInformation);
