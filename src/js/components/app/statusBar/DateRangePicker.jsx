import './styles.scss';
import React, { Component } from 'react';
import { DateRange } from 'react-date-range';
import { Card, FlatButton, TimePicker } from 'material-ui';
import { localizeDate } from 'js/utils/localizeDate';
import { withLoader, withMessages } from 'js/modules/ui';
import { withAPISettings } from 'ioc-api-interface';
import { compose } from 'redux';
import DateRangePickerTheme from './DateRangePickerTheme';
import { getCompleteDateTime } from 'js/utils/getCompleteDateTime';
import { grey800, grey900, redA700, white } from 'material-ui/styles/colors';

const enhance = compose(
  withLoader,
  withMessages,
  withAPISettings
);

class DateRangePicker extends Component {
  handleSelect(range) {
    this.props.setRange(range);
  }

  _closeDateTimePicker = () => {
    this.props.closeDateTimePicker();
  };

  _handleStartTimeChange = startTime => {
    this.props.handleStartTimeChange(startTime);
  };

  _handleEndTimeChange = endTime => {
    this.props.handleEndTimeChange(endTime);
  };

  _applyDateRange = async () => {
    try {
      const startDate = new Date(
        getCompleteDateTime(
          this.props.range.startDate,
          this.props.times.startTime,
          this.props.locale
        )
      );
      const endDate = new Date(
        getCompleteDateTime(this.props.range.endDate, this.props.times.endTime, this.props.locale)
      );
      if (startDate > endDate) {
        const e = new Error('Start date must be smaller than End date');
        this.props.pushError(e);
        return;
      }

      await this.props.setLiveTimeWindowWithDates(
        startDate,
        endDate,
        this.props.loadingStart,
        this.props.loadingStop
      );
      // Provisional for demo of 12/01/2018
      this.props.pushMessage('All data updated successfully', 'success');
      this._closeDateTimePicker();
    } catch (err) {
      console.error('Error changing live time window', err);
      this.props.pushError(err);
    }
  };

  render() {
    const {
      isDatePickerVisible,
      dictionary,
      locale,
      startDate,
      endDate,
      ranges,
      range,
      times
    } = this.props;

    const dateStartedString = startDate ? localizeDate(startDate, locale, 'L LT') : '';
    const dateFinishedString = endDate ? localizeDate(endDate, locale, 'L LT') : '';

    return (
      <div>
        <div style={{ textTransform: 'uppercase' }}>
          <span>{dateStartedString}</span>
          <span> - </span>
          <span>{dateFinishedString}</span>
        </div>
        <div
          style={
            isDatePickerVisible
              ? { display: 'block', position: 'absolute', zIndex: 5000, left: '15%', top: '100%' }
              : { display: 'none' }
          }
        >
          <Card
            style={{
              width: '100%',
              zIndex: 99,
              padding: 0,
              backgroundColor: grey900
            }}
          >
            <DateRange
              onChange={range => this.handleSelect(range)}
              linkedCalendars={true}
              twoStepChange={true}
              theme={DateRangePickerTheme}
              ranges={ranges}
              startDate={range.startDate}
              endDate={range.endDate}
              lang={locale}
            />
            <div className="time-picker-line">
              <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                <span style={{ fontSize: 12, paddingRight: 10 }}>{dictionary._startTime}</span>
                <TimePicker
                  value={times.startTime}
                  id="startTime"
                  textFieldStyle={{ fontSize: 12, width: '60px' }}
                  onChange={(e, startTime) => this._handleStartTimeChange(startTime)}
                />
              </div>
              <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
                <span style={{ fontSize: 12, paddingRight: 10 }}>{dictionary._endTime}</span>
                <TimePicker
                  value={times.endTime}
                  id="endTime"
                  textFieldStyle={{ fontSize: 12, width: '60px' }}
                  onChange={(e, endTime) => this._handleEndTimeChange(endTime)}
                />
              </div>
            </div>
            <hr style={{ borderColor: grey800 }} />
            <div className="buttons-line">
              <FlatButton
                label={dictionary._cancel}
                labelStyle={{ fontSize: 15 }}
                onClick={() => this._closeDateTimePicker()}
              />
              <FlatButton
                style={{ color: white, backgroundColor: redA700 }}
                label={dictionary._apply}
                labelStyle={{ fontSize: 15 }}
                onClick={() => this._applyDateRange()}
              />
            </div>
          </Card>
        </div>
      </div>
    );
  }
}

export default enhance(DateRangePicker);
