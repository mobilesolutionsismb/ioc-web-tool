import React, { Component } from 'react';
import { fade } from 'material-ui/utils/colorManipulator';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { fullBlack, red500 } from 'material-ui/styles/colors';
import RightButtons from './RightButtons';
import Hazard from './Hazard';
import ExtraInformation from './ExtraInformation';
import DateRangePicker from './DateRangePicker';
import { withEmergencyEvents, withAPISettings } from 'ioc-api-interface';
import { withLoader, withMessages } from 'js/modules/ui';
import { compose } from 'redux';
import { DateRangeIcon, CloseIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import moment from 'moment';
import { teal400 } from 'material-ui/styles/colors';
import { withDictionary } from 'ioc-localization';
//import { _listOrganizationUnits } from '../../../modules/QuickUserAdd/dataSources/apiDefinitions';
const FIRST_OPENED_WIDTH = 380; //px
const SECOND_OPENED_WIDTH = 665; //px

const enhance = compose(
  withEmergencyEvents,
  withAPISettings,
  withLeftDrawer,
  withLoader,
  withMessages,
  withDictionary
);

class StatusBar extends Component {
  static defaultProps = {
    clickedPoint: null
  };

  state = {
    isDatePickerVisible: false,
    ranges: {},
    range: {
      startDate: moment(),
      endDate: moment()
    },
    times: {
      startTime: moment(),
      endTime: moment()
    }
  };

  _setRange = range => {
    this.setState({ range });
  };

  componentDidMount() {
    this._init(this.props);
  }

  _init = props => {
    const { dictionary } = props;
    let ranges = {};
    ranges[dictionary._today] = {
      startDate: moment().startOf('day'),
      endDate: moment().endOf('day')
    };

    ranges[dictionary._yesterday] = {
      startDate: moment()
        .subtract(1, 'days')
        .startOf('day'),
      endDate: moment()
        .subtract(1, 'days')
        .endOf('day')
    };

    ranges[dictionary._last7Days] = {
      startDate: now => {
        return now.add(-7, 'days');
      },
      endDate: now => {
        return now;
      }
    };

    ranges[dictionary._last30Days] = {
      startDate: now => {
        return now.add(-30, 'days');
      },
      endDate: now => {
        return now;
      }
    };

    const { liveTW, activeEvent } = this.props;
    const isEventActive = this.props.activeEventId !== -1;
    const startDate = moment(isEventActive ? activeEvent.properties.start : liveTW.dateStart);
    const endDate = moment(isEventActive ? activeEvent.properties.end : liveTW.dateEnd);

    const range = {
      startDate,
      endDate
    };
    const times = {
      startTime: new Date(startDate),
      endTime: new Date(endDate)
    };
    this.setState({ ranges, range, times });
    if (this.props.deselectFeature) this.props.deselectFeature();
  };

  _handleStartTimeChange = startTime => {
    let times = this.state.times;
    times.startTime = startTime;
    this.setState({ times });
  };

  _handleEndTimeChange = endTime => {
    let times = this.state.times;
    times.endTime = endTime;
    this.setState({ times });
  };

  checkOpenTabs = () => {
    if (this.props.socialPanel) {
      return {};
    } else {
      if (this.props.isPrimaryOpen) {
        if (this.props.isSecondaryOpen) {
          return {
            marginLeft: `${SECOND_OPENED_WIDTH}px`,
            width: `calc(100% - ${SECOND_OPENED_WIDTH}px)`
          };
        } else {
          return {
            marginLeft: `${FIRST_OPENED_WIDTH}px`,
            width: `calc(100% - ${SECOND_OPENED_WIDTH}px)`
          };
        }
      } else {
        return {};
      }
    }
  };

  setTransparenceBackground = () => {
    if (this.props.socialPanel) {
      return { backgroundColor: fullBlack };
    } else {
      if (
        this.props.activeEvent !== null &&
        this.props.activeEvent !== undefined &&
        this.isEventOpen(this.props.activeEvent)
      ) {
        return { backgroundColor: fade(red500, 0.54) };
      } else {
        return { backgroundColor: fade(fullBlack, 0.54) };
      }
    }
  };

  isEventOpen = event => {
    let active = false;

    if (
      event.end === undefined ||
      event.end === null ||
      new Date(event.end).getTime() > new Date().getTime()
    ) {
      active = true;
    }
    return active;
  };

  _openDateRangePicker = () => {
    this.setState({ isDatePickerVisible: true });
  };

  _closeDateTimePicker = () => {
    setTimeout(() => {
      this.setState({ isDatePickerVisible: false });
    });
  };

  _onMouseEnterIcon = event => {
    event.currentTarget.style['background'] = this.props.muiTheme.palette.tabsColor;
  };

  _onMouseLeaveIcon = (event, isLiveMode, isRangeIcon) => {
    if (isLiveMode || !isRangeIcon) event.currentTarget.style['background'] = 'transparent';
    else event.currentTarget.style['background'] = teal400;
  };

  _setLiveMode = async () => {
    try {
      const today = new Date(moment().endOf('day'));
      const last7Days = new Date(
        moment(today)
          .subtract(7, 'days')
          .startOf('day')
      );

      await this.props.setLiveTimeWindowWithDates(
        last7Days,
        today,
        this.props.loadingStart,
        this.props.loadingStop
      );
      // Provisional for demo of 12/01/2018
      this.props.pushMessage('All data updated successfully', 'success');
      this._closeDateTimePicker();
      this._init(this.props);
    } catch (err) {
      console.error('Error changing live time window', err);
      this.props.pushError(err);
    }
  };

  render() {
    const { style, muiTheme, locale, liveTW, clickedPoint, activeEvent, dictionary } = this.props;
    const isEventActive = this.props.activeEventId !== -1;
    const startDate = moment(isEventActive ? activeEvent.properties.start : liveTW.dateStart);
    const endDate = moment(isEventActive ? activeEvent.properties.end : liveTW.dateEnd);
    const tmpValue = moment(endDate).subtract(7, 'days');
    const isLiveMode = moment(tmpValue).isSame(startDate, 'day');

    const aspectStyle = {
      color: muiTheme.palette.textColor
    };

    const iconStyle = {
      color: 'white',
      padding: 0
    };

    const iconButtonStyle = {
      borderRadius: '50%',
      marginLeft: 10
    };

    const calendarButtonStyle = {
      background: isLiveMode ? 'transparent' : teal400
    };

    return (
      <section style={{ ...style, ...aspectStyle, ...this.setTransparenceBackground() }}>
        <div className="statusBarInfo" style={this.checkOpenTabs()}>
          <Hazard />
          {/* <DoubleDatePicker socialPanel={this.props.socialPanel} /> */}
          <div>
            <DateRangePicker
              isDatePickerVisible={this.state.isDatePickerVisible}
              closeDateTimePicker={this._closeDateTimePicker}
              locale={locale}
              startDate={startDate}
              endDate={endDate}
              setRange={this._setRange}
              ranges={this.state.ranges}
              range={this.state.range}
              times={this.state.times}
              handleStartTimeChange={this._handleStartTimeChange}
              handleEndTimeChange={this._handleEndTimeChange}
              dictionary={dictionary}
            />
            {!isEventActive && (
              <DateRangeIcon
                onMouseEnter={event => this._onMouseEnterIcon(event)}
                onMouseLeave={event => this._onMouseLeaveIcon(event, isLiveMode, true)}
                style={{ ...iconButtonStyle, ...calendarButtonStyle }}
                iconStyle={iconStyle}
                onClick={() => this._openDateRangePicker()}
              />
            )}
            {isLiveMode ? (
              <div />
            ) : (
              !isEventActive && (
                <CloseIcon
                  onMouseEnter={event => this._onMouseEnterIcon(event)}
                  onMouseLeave={event => this._onMouseLeaveIcon(event)}
                  style={iconButtonStyle}
                  iconStyle={iconStyle}
                  onClick={this._setLiveMode}
                />
              )
            )}
          </div>
          <ExtraInformation clickedPoint={clickedPoint} />
          {this.props.activeEvent !== null && this.props.activeEvent !== undefined ? (
            <RightButtons />
          ) : null}
        </div>
      </section>
    );
  }
}

export default enhance(muiThemeable()(StatusBar));
