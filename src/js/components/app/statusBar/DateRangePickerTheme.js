import { grey900, grey800, redA700, white, teal400, teal700 } from 'material-ui/styles/colors';

const DateRangePickerTheme = {
  DateRange: {
    background: grey900,
    height: '265px'
  },
  Calendar: {
    background: grey900,
    color: white,
    marginRight: '40px'
  },
  MonthAndYear: {
    background: grey900,
    color: white,
    fontSize: '15px'
  },
  MonthButton: {
    background: redA700
  },
  MonthArrowPrev: {
    borderRightColor: white
  },
  MonthArrowNext: {
    borderLeftColor: white
  },
  Weekday: {
    background: grey900,
    color: white,
    fontWeight: '400px'
  },
  Day: {
    transition: 'transform .1s ease, box-shadow .1s ease, background .1s ease'
  },
  DaySelected: {
    background: teal400
  },
  DayActive: {
    background: teal400,
    boxShadow: 'none'
  },
  DayPassive: {
    visibility: 'hidden'
  },
  DayInRange: {
    background: teal700,
    color: white
  },
  DayHover: {
    background: grey800,
    color: white,
    transform: 'scale(1.1) translateY(-10%)',
    boxShadow: '0 2px 4px rgba(0, 0, 0, 0.4)'
  },
  PredefinedRanges: {
    marginLeft: 10,
    marginTop: 10,
    marginRight: 40,
    background: grey900
  },
  PredefinedRangesItem: {
    background: grey900,
    color: white,
    borderStyle: 'solid',
    borderColor: white,
    borderWidth: '1px',
    textAlign: 'left'
  },
  PredefinedRangesItemActive: {
    color: teal400
  }
};

export default DateRangePickerTheme;
