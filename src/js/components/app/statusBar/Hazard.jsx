import React, { Component } from 'react';
import { compose } from 'redux';
import { Chip, Avatar } from 'material-ui';
import moment from 'moment';
import { withDictionary } from 'ioc-localization';
import { withEmergencyEvents, withAPISettings } from 'ioc-api-interface';
import { HAZARD_TYPES_LETTERS } from 'js/components/ReportsCommons';
import './styles.scss';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { LiveReloader } from './LiveReloader';
const enhance = compose(
  withDictionary,
  withEmergencyEvents,
  withAPISettings
);
class HazardBar extends Component {
  render() {
    let { activeEvent, liveTW, muiTheme } = this.props;
    const isLiveMode =
      moment(liveTW.dateEnd).isSame(moment(), 'day') &&
      moment(liveTW.dateStart).isSame(moment().subtract(7, 'd'), 'day');

    return (
      <div>
        {/* <div>{dictionary._hazard}: {selectedEvent ? selectedEvent.hazard : 'Live Mode'}</div> */}
        {activeEvent ? (
          <Chip style={{ height: 24, backgroundColor: 'rgba(0,0,0,0)' }}>
            <Avatar
              className="ireact-pinpoint-icons"
              size={24}
              style={{ height: 24, width: 24 }}
              backgroundColor={'white'}
              color={'red'}
            >
              {HAZARD_TYPES_LETTERS[activeEvent.properties.hazard]}
            </Avatar>
            <div className="eventCardHazzard">
              <span>
                {activeEvent.properties.eventCode
                  ? `${activeEvent.properties.eventCode}: ${activeEvent.properties.displayName}`
                  : `${activeEvent.properties.displayName}`}
              </span>
            </div>
          </Chip>
        ) : isLiveMode ? (
          <div className="mode-div">
            <LiveReloader />
            <div
              className="mode-dot"
              style={{ background: muiTheme.palette.completedOrAccomplishedColor }}
            />
          </div>
        ) : (
          <div className="mode-div">
            <span style={{ color: muiTheme.palette.liveModeDisabledColor }}>LIVE</span>
            <div
              className="mode-dot"
              style={{ background: muiTheme.palette.liveModeDisabledColor }}
            />
          </div>
        )}
      </div>
    );
  }
}

export default muiThemeable()(enhance(HazardBar));
