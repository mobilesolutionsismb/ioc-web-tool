import React, { Component } from 'react';
import { withEmergencyEvents } from 'ioc-api-interface';
import { withFeaturePopup } from 'js/modules/featurePopup';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { IconButton, FontIcon } from 'material-ui';
import { drawerNames } from 'js/modules/AreaOfInterest';
import { compose } from 'redux';

const enhance = compose(withLeftDrawer, withEmergencyEvents, withFeaturePopup);

class RightButtons extends Component {
  deactivateEvent = () => {
    this.props.toggleDialog(false);
    this.props.deactivateEvent();
  };
  editEvent = () => {
    this.props.toggleDialog(false);
    this.props.history.replace(`/home/${drawerNames.event}?event_edit`);
  };
  render() {
    let actEvent = this.props.activeEvent;
    return (
      <div>
        {actEvent !== null && actEvent !== undefined ? (
          <div style={{ borderLeft: '1px white solid', position: 'relative', right: 50 }}>
            <IconButton
              disabled={
                this.props.activeEventId !== null && this.props.activeEventId !== undefined
                  ? false
                  : true
              }
              style={{ float: 'right' }}
              onClick={this.deactivateEvent}
            >
              <FontIcon className="material-icons">close</FontIcon>
            </IconButton>
          </div>
        ) : null}
      </div>
    );
  }
}
export default enhance(RightButtons);
