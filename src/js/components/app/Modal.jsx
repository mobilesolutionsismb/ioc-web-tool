import React, { Component } from 'react';
import { Dialog, FlatButton } from 'material-ui';
import isPojo from 'is-pojo';
import { compose } from 'redux';

import { withRouter } from 'react-router-dom';

import { withDictionary } from 'ioc-localization';
import { withLogin } from 'ioc-api-interface';
import { withMessages } from 'js/modules/ui';

const enhance = compose(withLogin, withDictionary, withMessages);

class Modal extends Component {
  unlock = null;

  handleRequestClose = () => {
    let isMessage = this.props.modal !== null;
    if (isMessage) {
      this.props.popModalMessage();
      if (this.unlock !== null) {
        this.unlock();
        this.unlock = null;
      }
    }
  };

  //This component accepts only actions in the format { actionName:actionCallback }
  //This methods remap them to flat buttons
  validateActions = actionName => {
    if (!actionName) {
      return [];
    } else {
      if (isPojo(actionName)) {
        const actionKeys = Object.keys(actionName);
        if (actionKeys.length > 1) {
          return actionKeys
            .map(name => {
              const isValid = typeof name === 'string' && typeof actionName[name] === 'function';
              return isValid
                ? {
                    name,
                    action: () => {
                      actionName[name]();
                      this.handleRequestClose();
                    }
                  }
                : null;
            })
            .filter(a => a !== null)
            .map((a, index) => (
              <FlatButton
                label={this.props.dictionary(a.name)}
                primary={index > 0}
                onClick={a.action}
              />
            ));
        } else {
          //just one action
          const name = actionKeys[0];
          const isValid = typeof name === 'string' && typeof actionName[name] === 'function';
          if (isValid) {
            const a = actionName[name];
            if (name === '_close') {
              const closeButton = (
                <FlatButton
                  label={this.props.dictionary._close}
                  primary={false}
                  onClick={() => {
                    actionName[name]();
                    this.handleRequestClose();
                  }}
                />
              );
              return [closeButton];
            } else {
              const actionButton = (
                <FlatButton
                  label={this.props.dictionary(a.name)}
                  primary={true}
                  onClick={() => {
                    actionName[name]();
                    this.handleRequestClose();
                  }}
                />
              );
              const closeButton = (
                <FlatButton
                  label={this.props.dictionary._close}
                  primary={false}
                  onClick={this.handleRequestClose}
                />
              );
              return [closeButton, actionButton];
            }
          } else {
            return [
              <FlatButton
                label={this.props.dictionary._close}
                primary={false}
                onClick={this.handleRequestClose}
              />
            ];
          }
        }
      } else return [];
    }
  };

  render() {
    const isMessage = this.props.modal !== null && this.props.modal.type === 'modal';
    let title = ' ';
    let text = ' ';
    let actions = [];

    if (isMessage) {
      const dict = this.props.dictionary;

      const message = this.props.modal;
      if (typeof message.text !== 'string' && React.isValidElement(message.text)) {
        text = message.text;
      } else {
        const args = [message.text, ...message.params];
        text = dict(...args);
      }
      title = dict(message.title);
      const actionName = message.actionName;
      actions = this.validateActions(actionName);
      if (this.unlock === null) {
        this.unlock = this.props.history.block();
      }
    }

    return (
      <Dialog
        title={title}
        actions={actions}
        modal={false}
        open={isMessage}
        onRequestClose={this.handleRequestClose}
      >
        {text}
      </Dialog>
    );
  }
}

export default withRouter(enhance(Modal));
