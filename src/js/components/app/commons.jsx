import styled from 'styled-components';

export const Page = styled.div`
  width: 100%;
  height: 100%;
`;

export default Page;

export const FlexPage = styled(Page)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const RowPage = styled(FlexPage)`
  flex-direction: row;
`;

export const ColumnPage = styled(FlexPage)`
  flex-direction: column;
`;

export const Container = Page;

export const NavigatorBody = styled.div.attrs(() => ({ className: 'navigator' }))`
  position: absolute;
  /* left: 0;
  right: 0;
  top: 0;
  bottom: 0; */
  width: 100%;
  height: 100%;
  max-width: 100%;
  max-height: 100%;
  overflow: hidden;
`;

// Use in input when adornment is empty
export const AdornmentIconPlaceholder = styled.span`
  width: 24px;
`;

export const FlexContainer = styled(Container)`
  display: flex;
  align-items: ${props => props.alignItems || 'center'};
  justify-content: ${props => props.justifyContent || 'center'};
  flex-direction: ${props => props.direction || 'column'};
  background-color: ${props => props.theme.palette.backgroundColor || 'inherit'};
  color: ${props => props.theme.palette.textColor || 'inherit'};
`;

export const PageContainer = styled(Container).attrs(() => ({
  className: 'page-container'
}))`
  width: ${props =>
    props.tabBarHeight
      ? `calc(100% - ${props.expandX ? props.offsetOpenX : props.offsetClosedX}px)`
      : '100%'};
  right: 0;
  height: ${props => (props.tabBarHeight ? `calc(100% - ${props.tabBarHeight}px)` : '100%')};
  position: absolute;
  top: ${props => (props.tabBarHeight ? `${props.tabBarHeight}px` : '0')};
  user-select: none;
`;

export const ErrorContainer = styled(FlexContainer).attrs(() => ({
  className: 'error-container'
}))`
  background-color: darkred;
  color: white;
  overflow: auto;
  padding: 16px;
  box-sizing: border-box;
`;

export const StyledInputWrapper = styled.div.attrs(() => ({ className: 'input-wrapper' }))`
  border: none;
  padding: 0;
  width: ${256 + 48}px;
  height: 72px;
  min-width: 48px;
  min-height: 48px;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: space-between;
  overflow: visible;
  position: relative;
`;
