import React, { lazy, Suspense } from 'react';
import { NavigatorBody } from 'js/components/app/commons';
import { Route, Switch, Redirect } from 'react-router-dom';
const Login = lazy(() => import('js/components/auth/components/Login'));
const Logout = lazy(() => import('js/components/auth/components/Logout'));
const ToS = lazy(() => import('js/components/ToS/ToS'));
const Home = lazy(() => import('js/components/Home2/Home'));
const Settings = lazy(() => import('js/components/Settings/Settings'));
const AdminPanel = lazy(() => import('js/components/AdminPanel'));
const UAVImage = lazy(() => import('js/components/UAV/UAVImage'));
const Social = lazy(() => import('js/components/Social/Social'));
const ConfirmEmail = lazy(() => import('js/components/UserRegistration/ConfirmEmailRoute'));
const ResetPassword = lazy(() => import('js/components/UserRegistration/ResetPasswordRoute'));
const UserProfile = lazy(() => import('js/components/UserProfile/UserProfile'));
const PasswordChange = lazy(() => import('js/components/UserProfile/PasswordChange'));

export const DEFAULT_PAGE_AUTHENTICATED = '/home/map';
export const DEFAULT_PAGE_UNAUTHENTICATED = '/';

export function Navigator({ style }) {
  return (
    <NavigatorBody style={style}>
      <Suspense fallback={<div>Loading...</div>}>
        <Route
          render={({ location }) => (
            <Switch location={location}>
              <Route
                path={'/home'}
                strict={false}
                exact={false}
                render={({ location }) => (
                  <Switch location={location}>
                    <Route exact={true} strict={true} path="/home/:activeItem">
                      <Home />
                    </Route>
                    <Redirect
                      exact={true}
                      strict={true}
                      path="/home"
                      to={DEFAULT_PAGE_AUTHENTICATED}
                    />
                    <Redirect
                      exact={true}
                      strict={true}
                      path="/home/"
                      to={DEFAULT_PAGE_AUTHENTICATED}
                    />
                  </Switch>
                )}
              />

              <Route path="/settings/:panel?/:subpanel?">
                <Settings />
              </Route>

              <Route path="/social" exact={true}>
                <Social />
              </Route>

              <Route path="/uavReport/:id" exact={true}>
                <UAVImage />
              </Route>

              <Route path="/admin" exact={true}>
                <AdminPanel />
              </Route>

              <Route
                path="/terms"
                exact={true}
                render={({ location }) => <ToS location={location} />}
              />

              <Route
                path="/privacy"
                exact={true}
                render={({ location }) => <ToS location={location} />}
              />

              <Route path="/reset-password" exact={true}>
                <ResetPassword />
              </Route>

              <Route path="/confirm-email" exact={true}>
                <ConfirmEmail />
              </Route>

              <Route path="/editprofile" exact={true}>
                <UserProfile />
              </Route>

              <Route
                path="/editpassword"
                exact={true}
                render={({ history }) => <PasswordChange history={history} />}
              />

              <Route path="/logout" exact={true}>
                <Logout />
              </Route>

              <Redirect path="/login" to={DEFAULT_PAGE_UNAUTHENTICATED} push={false} />
              <Redirect path="/index.html" to={DEFAULT_PAGE_UNAUTHENTICATED} push={false} />

              <Route path={DEFAULT_PAGE_UNAUTHENTICATED} exact={true}>
                <Login />
              </Route>

              <Route render={() => <h1>Not Found :( </h1>} />
            </Switch>
          )}
        />
      </Suspense>
    </NavigatorBody>
  );
}
