import React /* , { Component } */ from 'react';
import { Chip, Avatar, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { yellowA200, deepOrange500, green800, redA700 } from 'material-ui/styles/colors';
import styled from 'styled-components';
import { CATEGORY_MAP, CONTENT_TYPES_LETTERS } from 'js/components/ReportsCommons';
import { getTranslationByhazardName } from 'js/utils/hazardMappings';
import EMCOMM_HAZARD_ICONS from './ireact_pinpoint_emcomm_hazard_map';

export function EmCommLanguage({ language, size = 24 }) {
  return (
    <Avatar className="emcomm-language" size={size} style={{ height: size, width: size }}>
      <FontIcon style={{ width: size }} className={`flag-icon flag-icon-${language}`} />
    </Avatar>
  );
}

export function EmCommLanguageChip({ language, palette, dictionary, size = 24 }) {
  return (
    <Chip className="language-chip" style={{ backgroundColor: palette.backgroundColor }}>
      <Avatar className="emcomm-language" size={size} style={{ height: size, width: size }}>
        <FontIcon style={{ width: size }} className={`flag-icon flag-icon-${language}`} />
      </Avatar>
      <div className="language-label">
        <span>
          {dictionary._language}: {language.toUpperCase()}
        </span>
      </div>
    </Chip>
  );
}

export const EmCommChip = ({ emComm, palette, dictionary }) => {
  return (
    <Chip className="hazard-chip" style={{ backgroundColor: palette.backgroundColor }}>
      <Avatar
        className="ireact-pinpoint-icons"
        size={24}
        style={{ height: 24, width: 24 }}
        color={palette.canvasColor}
        backgroundColor={palette.textColor}
      >
        {EMCOMM_HAZARD_ICONS[emComm.hazard]}
      </Avatar>
      <div className="hazard-label">
        <span>{dictionary[getTranslationByhazardName(emComm.hazard)]}</span>
      </div>
    </Chip>
  );
};

export const LevelBox = styled.div`
  margin-right: 4px;
  margin-left: 2px;
  width: ${props => props.size || '18px'};
  height: ${props => props.size || '18px'};
  background-color: ${props =>
    props.theme.palette.emcommSeverity[props.severity] ||
    props.theme.palette.emcommSeverity.unknown};
`;

export const Level = ({ level, type, palette, isLarge, dictionary }) => {
  if (type === 'warning') {
    return (
      <div className={`level warning${isLarge ? ' large' : ''}`}>
        <div
          className="box"
          style={{
            borderColor: palette.bePreparedEmCommColor,
            color: isLarge
              ? level === 1
                ? palette.canvasColor
                : palette.textColor
              : palette.bePreparedEmCommColor,
            backgroundColor: level === 1 ? palette.bePreparedEmCommColor : palette.canvasColor
          }}
        >
          {isLarge ? dictionary._emcomm_be_prepared : ''}
        </div>
        <div
          className="box"
          style={{
            borderColor: palette.actionRequiredEmCommColor,
            color: isLarge
              ? level === 2
                ? palette.canvasColor
                : palette.textColor
              : palette.actionRequiredEmCommColor,
            backgroundColor: level === 2 ? palette.actionRequiredEmCommColor : palette.canvasColor
          }}
        >
          {isLarge ? dictionary._emcomm_action_required : ''}
        </div>
        <div
          className="box"
          style={{
            borderColor: palette.dangerToLifeEmCommColor,
            color: isLarge ? palette.textColor : palette.dangerToLifeEmCommColor,
            backgroundColor: level === 3 ? palette.dangerToLifeEmCommColor : palette.canvasColor
          }}
        >
          {isLarge ? dictionary._emcomm_danger_to_life : ''}
        </div>
      </div>
    );
  } else if (type === 'alert') {
    return (
      <div
        className={`level alert${isLarge ? ' large' : ''}`}
        style={{
          borderColor: palette.dangerToLifeEmCommColor,
          backgroundColor: palette.dangerToLifeEmCommColor,
          color: palette.textColor
        }}
      >
        {isLarge ? dictionary._emcomm_danger_to_life : ''}
      </div>
    );
  } else {
    return (
      <div className="level" style={{ background: palette.accent1Color, color: palette.textColor }}>
        {`Unknown Emergency Communication type ${type}`}
      </div>
    );
  }
};

export const CategoryDotIcon = ({ category, palette }) => (
  <Avatar
    className="category-icon material-icons"
    size={24}
    style={{ height: 24, width: 24 }}
    color={palette.textColor}
    backgroundColor={palette.backgroundColor}
  >
    {CATEGORY_MAP[category]}
  </Avatar>
);

export const ContentTypeChip = ({ contentType, palette }) => {
  return (
    <Chip className="content-type-chip" style={{ backgroundColor: palette.canvasColor }}>
      <Avatar
        className="material-icons"
        size={24}
        style={{ height: 24, width: 24 }}
        color={palette.textColor}
        backgroundColor={palette.canvasColor}
      >
        {CONTENT_TYPES_LETTERS[contentType]}
      </Avatar>
      <div className="content-type-chip-label">
        <span>{contentType.substr(0, 3)}</span>
      </div>
    </Chip>
  );
};

export const EmCommColorSquare = muiThemeable()(({ level, muiTheme }) => (
  <div
    className={`level-em-comm level-${level}`}
    style={{
      width: 15,
      height: 15,
      backgroundColor:
        level === 'bePrepared'
          ? muiTheme.palette.bePreparedEmCommColor
          : level === 'actionRequired'
          ? muiTheme.palette.actionRequiredEmCommColor
          : muiTheme.palette.dangerToLifeEmCommColor
    }}
  />
));

export const WarningLevelColorSquare = ({ warningLevel, palette }) => {
  let customStyle = {
    width: 15,
    height: 15,
    marginRight: 10
  };
  switch (warningLevel) {
    case 'unknown':
      customStyle.backgroundColor = 'white';
      break;
    case 'minor':
      customStyle.backgroundColor = green800;
      break;
    case 'moderate':
      customStyle.backgroundColor = yellowA200;
      break;
    case 'severe':
      customStyle.backgroundColor = deepOrange500;
      break;
    case 'extreme':
      customStyle.backgroundColor = redA700;
      break;
    default:
      break;
  }

  return <div style={customStyle} />;
};
