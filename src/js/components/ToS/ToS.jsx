import React from 'react';
import { withDictionary } from 'ioc-localization';
// import { Route } from 'react-router-dom';
import qs from 'qs';
import { Page } from 'js/components/app/commons';
import styled from 'styled-components';

const TOSPage = styled(Page)`
  padding: 0px;
  overflow: hidden;
  box-sizing: border-box;

  iframe {
    width: 100%;
    height: 100%;
    border: none;
  }

  a,
  a:visited {
    color: ${props => props.theme.palette.primary1Color};
  }
`;

function ToSComponent({ location, locale }) {
  const searchParams = qs.parse(location.search.replace(/^\?/, ''));

  const type = location.pathname.endsWith('privacy') ? 'privacy' : 'terms';
  const title = type === 'privacy' ? 'Privacy' : 'Terms & Conditions';
  const _lang = searchParams.lang || locale;
  const url = `${PUBLIC_PATH}tos.html?type=${type}&lang=${_lang}`;
  return (
    <TOSPage className="tos">
      <iframe title={title} src={url} />
      <div className="v-spacer" />
    </TOSPage>
  );
}

export const ToS = withDictionary(ToSComponent);
export default ToS;
//  const ToSRoute = () => (
//   <Route
//     render={({ location }) => {
//       const searchParams = qs.parse(location.search.replace(/^\?/, ''));

//       return <ToS location={location} {...searchParams} />;
//     }}
//   />
// );

//  export default ToSRoute;
