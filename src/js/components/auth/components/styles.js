export const logoStyle = {
  width: '80%',
  maxWidth: 304,
  height: 100,
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'contain',
  backgroundImage: `url(${require('assets/logo/logo.png')})`
};

export const selectedFlagStyle = {
  borderBottomWidth: '1px',
  borderBottomStyle: 'solid'
};

export const fullWidthStyle = {
  width: '100%',
  marginTop: 8,
  marginBottom: 8
};

export const checkboxWrapperStyle = {
  width: '70%',
  maxWidth: 304,
  height: 32
};

export const formStyle = {
  width: '70%',
  maxWidth: 304
};
