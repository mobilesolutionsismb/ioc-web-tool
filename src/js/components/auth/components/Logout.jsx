import React, { Component } from 'react';
import { withLogin } from 'ioc-api-interface';
import { appInsights } from 'js/startup/errorHandlingAndTracking';
import { withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';

class Logout extends Component {
  _clearAll = async props => {
    console.warn('Clearing everything in storage');
    if (this.props.loggedIn) {
      await props.logout();
    }
    appInsights.clearAuthenticatedUserContext();
  };

  componentDidMount() {
    this._clearAll(this.props);
  }

  render() {
    return this.props.loggedIn ? <div /> : <Redirect push={false} to="/" from="/logout" />;
  }
}
export default withRouter(withLogin(Logout));
