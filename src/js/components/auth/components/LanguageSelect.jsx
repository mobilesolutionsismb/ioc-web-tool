import React, { Component } from 'react';
import { IconButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { loadDictionary } from 'js/utils/getAssets';

import { selectedFlagStyle } from './styles';

class LanguageSelect extends Component {
  selectLanguage = async lang => {
    const params = await loadDictionary(lang);
    const { locale, translationLoader } = params;
    this.props.changeLanguage(locale, translationLoader);
    // this.props.changeLanguage(
    //   lang,
    //   require(`bundle-loader?lazy!locale/lang_${lang}/translation.json`)
    // );
  };

  render() {
    let langCodes = Object.keys(this.props.supportedLanguages);
    let iconButtons = langCodes.map(lang => {
      let selected = lang === this.props.selectedLanguage;
      let highlighColor = this.props.muiTheme.palette.accent1Color;
      let selectedStyle = selected
        ? {
            borderBottomColor: highlighColor,
            ...selectedFlagStyle
          }
        : {
            borderBottomStyle: 'none'
          };
      return (
        <IconButton
          key={lang}
          className={selected ? 'selected' : null}
          tooltip={this.props.supportedLanguages[lang]}
          onClick={this.selectLanguage.bind(this, lang)}
          style={selectedStyle}
        >
          <FontIcon className={`flag-icon flag-icon-${lang}`} />
        </IconButton>
      );
    });

    return <div className="login-language-select">{iconButtons}</div>;
  }
}

export default muiThemeable()(LanguageSelect);
