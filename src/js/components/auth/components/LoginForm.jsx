import React, { Component } from 'react';
import {
  FormComponent,
  StyledForm,
  FormContentWrapper,
  StyledFormActions
} from 'js/components/FormComponent';
import { RaisedButton /* , FlatButton */ } from 'material-ui';
import { emailRule, passwordRule } from 'js/utils/fieldValidation';
// import { LinkedButton } from 'js/components/app/LinkedButton';
import { Link } from 'react-router-dom';

const loginFormConfig = [
  {
    name: 'user',
    trim: 'all',
    inputType: 'text',
    label: '_email',
    rules: [emailRule],
    defaultValue: ''
  },
  {
    name: 'password',
    trim: true,
    inputType: 'password',
    label: '_password',
    rules: [passwordRule],
    defaultValue: ''
  }
];

const style = {
  button: {
    width: 304
  }
};

export class LoginForm extends Component {
  render() {
    const { dictionary } = this.props;
    return (
      <FormComponent
        className="login-form"
        config={loginFormConfig}
        onSubmit={this.props.onSubmit}
        formComponent={StyledForm}
        dictionary={dictionary}
        innerComponent={FormContentWrapper}
        actionComponent={StyledFormActions}
        actions={() => [
          <RaisedButton
            key="login"
            type="submit"
            className="ui-button"
            primary={true}
            style={style.button}
            label={dictionary._signin}
          />,
          <div className="link" key="privacy-links">
            <p>
              <Link to="/privacy">Privacy Policy</Link>
            </p>
            <p>
              <Link to="/terms">Terms And Conditions</Link>
            </p>
          </div>

          // TODO add links to GDPR and other cr**
          // <FlatButton
          //   key="password-forgot"
          //   className="ui-button"
          //   disableTouchRipple={true}
          //   containerElement={<LinkedButton to="/passwordForgot" />}
          //   label={dictionary._password_forgot}
          // />,
          // <FlatButton
          //   key="register"
          //   className="ui-button"
          //   disableTouchRipple={true}
          //   containerElement={<LinkedButton to="/terms" />}
          //   label={dictionary._signup}
          // />
        ]}
      />
    );
  }
}

export default LoginForm;
