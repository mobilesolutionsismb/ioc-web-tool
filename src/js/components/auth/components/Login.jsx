import './login.scss';
import React, { Component } from 'react';
import { withGeolocation } from 'ioc-geolocation';
import { withMap } from 'js/modules/map';
import { Redirect } from 'react-router-dom';

import { withLogin, withAPISettings, withEnumsAndRoles } from 'ioc-api-interface';
import { withMessages, withLoader } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { compose } from 'redux';
import LoginForm from './LoginForm';
import LanguageSelect from './LanguageSelect';
import { Checkbox } from 'material-ui';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import centroid from '@turf/centroid';
import { logoStyle, checkboxWrapperStyle, formStyle } from './styles';
import { loadDictionary } from 'js/utils/getAssets';

const enhance = compose(
  withGeolocation,
  withMap,
  withDictionary,
  withLogin,
  withAPISettings,
  withEnumsAndRoles,
  withMessages,
  withLoader
);

class Login extends Component {
  onLoginFormSubmit = async formData => {
    this.props.loadingStart();
    try {
      // TODO check this with the real api
      let user = await this.props.login(formData.user, formData.password);
      if (user.userType !== 'authority') {
        const e = 'Invalid User Type';
        appInsights.trackException(
          e,
          'Login::onLoginFormSubmit',
          { api: 'login', userType: user.userType },
          {},
          SeverityLevel.Verbose
        );
        this.props.logout();
        this.props.pushError(e);
      } else {
        appInsights.setAuthenticatedUserContext(`${user.id}`, user.userName, true);
        try {
          const aoi = await this.props.setAuthorityUserAreaOfInterest();
          this.props.updateMapSettings({
            maxBounds: aoi.bounds,
            center: centroid(aoi.feature).geometry.coordinates
            // center: geolocationPos
            //   ? [geolocationPos.coords.longitude, geolocationPos.coords.latitude]
            //   : centroid(aoi.feature).geometry.coordinates
          });
          await this.props.getEnums();
          await this.props.getRoles();
        } catch (e) {
          console.error(
            '%cLoad configuration data Failure',
            'color: whitesmoke; background: darkred',
            e
          );
          appInsights.trackException(
            e,
            'Login::onLoginFormSubmit',
            { api: 'configuration' },
            {},
            SeverityLevel.Critical
          );
        }

        let geolocationPos = null;
        try {
          geolocationPos = await this.props.updatePosition();
          this.props.updateMapSettings({
            center: [geolocationPos.coords.longitude, geolocationPos.coords.latitude]
          });
          console.log('%c geolocationPos', 'color: lightgreen;', geolocationPos);
        } catch (e) {
          console.error('%cCannot update position', 'color: whitesmoke; background: darkgray', e);
          appInsights.trackException(
            e,
            'Login::onLoginFormSubmit',
            { api: 'geolocation' },
            {},
            SeverityLevel.Information
          );
        }
        this.props.pushMessage('_logged_in', 'success', null, user.userName);
        console.log('%cSuccess', 'color: white; background: lime', user);
      }
      this.props.loadingStop();
    } catch (e) {
      await this._checkLanguageIsLoaded();
      console.log('%cError', 'color: white; background: darkRed', e);
      appInsights.trackException(
        e,
        'Login::onLoginFormSubmit',
        { api: 'login' },
        {},
        SeverityLevel.Error
      );
      this.props.pushError(e);
      this.props.loadingStop();
    }
  };

  storePassword = (e, isChecked) => {
    this.props.storeCredentials(isChecked);
  };

  _checkLanguageIsLoaded = async () => {
    if (this.props.dictIsEmpty) {
      const params = await loadDictionary(this.props.locale);
      const { locale, translationLoader } = params;
      this.props.loadDictionary(locale, translationLoader);
    }
    return;
  };

  componentDidUpdate(prevProps) {
    if (this.props.dictIsEmpty && !prevProps.dictIsEmpty) {
      this._checkLanguageIsLoaded();
    }
  }

  componentDidMount() {
    this._checkLanguageIsLoaded();
  }

  render() {
    if (this.props.loggedIn) {
      return <Redirect to="/home" from="/" />;
    } else {
      return (
        <div className="login page">
          <div style={logoStyle} />
          <LoginForm
            style={formStyle}
            onSubmit={this.onLoginFormSubmit}
            dictionaryIsEmpty={this.props.dictionary.isEmpty}
            dictionary={this.props.dictionary}
            locale={this.props.locale}
          />
          <div style={checkboxWrapperStyle}>
            <Checkbox
              onCheck={this.storePassword}
              label={this.props.dictionary('_remember_password')}
            />
          </div>
          <LanguageSelect
            selectedLanguage={this.props.locale}
            changeLanguage={this.props.loadDictionary}
            supportedLanguages={this.props.supportedLanguages}
          />
        </div>
      );
    }
  }
}

export default enhance(Login);
