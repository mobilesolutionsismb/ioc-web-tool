import React from 'react';
import { Redirect } from 'react-router-dom';
import { withLogin } from 'ioc-api-interface';

/**
 * Wraps the specified component into a connected Lock component that renders the original
 * component only if the `user` property is truthy, redirecting the user to the `lockScreenPath`
 * otherwise.
 *
 * @param {any} Component the component to lock
 * @param {any} unauthenticatedLandingPage the path to the unauthenticated landing page
 * @param {any} authenticatedLandingPage the path to the authenticated landing page
 * @returns
 */
const UserRedirectLock = (
  Component,
  unauthenticatedLandingPage = null,
  authenticatedLandingPage = null
) => {
  const Lock = ({
    // Login stuff
    loggedIn,
    user,
    rememberCredentials,
    login,
    logout,
    passwordForgot,
    register,
    storeCredentials,
    // Other stuff that should be passed on
    ...rest
  }) => {
    // console.debug('UserRedirectLock', user ? user.username : 'null', rest.location.pathname);
    //may be a more in-depth check such has token is valid
    if (loggedIn) {
      if (authenticatedLandingPage === null) {
        // const NotificationLocked = NotificationHOC(Component, loggedIn);
        return <Component {...rest} />;
      } else {
        return <Redirect to={authenticatedLandingPage} />;
      }
    } else {
      if (unauthenticatedLandingPage === null) {
        // const NotificationLocked = NotificationHOC(Component, loggedIn);
        return <Component {...rest} />;
      } else {
        return <Redirect to={unauthenticatedLandingPage} />;
      }
    }
  };

  return withLogin(Lock);
};

export default UserRedirectLock;
