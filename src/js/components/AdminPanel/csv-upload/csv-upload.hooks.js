import { useState, useEffect, useRef } from 'react';
import { API } from 'ioc-api-interface';
import { _listTeams } from 'js/modules/QuickUserAdd/dataSources/apiDefinitions';
import axios from 'axios';

const api = API.getInstance();
const axiosInstance = api._axios;

async function getTeams(ids) {
  if (!Array.isArray(ids) || ids.length === 0) {
    return [];
  }
  try {
    const response = await axiosInstance.request(_listTeams(ids));
    return response.data.result;
  } catch (err) {
    throw err;
  }
}

export function useOrganizationTeams(organizations) {
  const ids = organizations.map(o => o.id);
  const [loading, setLoading] = useState(false);
  const [orgTeams, setOrgTeams] = useState([]);
  const cancelRef = useRef(null);
  const isMounted = useRef(null);

  function onLoad(loading) {
    if (isMounted.current) {
      setLoading(loading);
    }
  }

  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = null;
    };
  }, []);

  async function _getOrgTeams(ids) {
    onLoad(true);
    const source = axios.CancelToken.source();
    cancelRef.current = source;
    try {
      const teams = await getTeams(ids, cancelRef.current.token);
      if (isMounted.current) {
        setOrgTeams(teams);
      }
    } catch (err) {
      if (axios.isCancel(err)) {
        console.log('Request canceled', err.message);
      }
    } finally {
      onLoad(false);
    }
  }

  async function getOrgTeams(ids) {
    if (loading && cancelRef.current) {
      cancelRef.current.cancel();
      cancelRef.current = null;
      _getOrgTeams(ids);
    } else {
      _getOrgTeams(ids);
    }
  }
  useEffect(() => {
    if (ids.length > 0) {
      getOrgTeams(ids);
    } else {
      setOrgTeams([]);
    }
  }, [organizations]);
  return orgTeams;
}

export function useUploadProgress() {
  const [count, setCount] = useState(0);
  const [total, setTotal] = useState(0);
  // const [uploadProgress, setUploadProgress] = useState({ count: 0, total: 0 });

  function initUploadProgress(t) {
    setCount(0);
    setTotal(t);
  }

  function resetProgress() {
    setCount(0);
    setTotal(0);
  }

  function incrementUploadProgress() {
    if (total > 0) {
      const _count = count + 1;
      if (_count < total) {
        setCount(_count);
      } else {
        resetProgress();
      }
    } else {
      resetProgress();
    }
  }
  const uploadProgress = { count, total };
  return [uploadProgress, initUploadProgress, incrementUploadProgress];
}
