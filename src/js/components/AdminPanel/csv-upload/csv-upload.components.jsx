import styled from 'styled-components';
import { ColumnPage } from 'js/components/app/commons';

export const CSVUploadContainer = styled(ColumnPage).attrs(() => ({ className: 'csv-uploads' }))`
  justify-content: space-evenly;
  align-items: flex-start;
  padding: 16px;
  box-sizing: border-box;
  position: relative;
`;

export const ErrorBox = styled.div.attrs(() => ({ className: 'error-box' }))`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  border: 2px solid;
  padding: 32px;
  min-width: 520px;
  background-color: darkred;
  overflow: auto;
  margin: 8;
`;

export const Errors = styled.div.attrs(() => ({ className: 'errors' }))`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  z-index: 10;
  display: flex;
  flex-direction: column;
  max-height: 100%;
  overflow: auto;
`;
