import { getGeom } from '@turf/invariant';
import wellknown from 'wellknown';
import rewind from '@turf/rewind';
import { feature } from '@turf/helpers';
import {
  emailRule,
  firstnameRule,
  lastnameRule,
  mobilePhoneRule,
  usernameRule
} from 'js/utils/fieldValidation';
import { normalizeEmail, trim, isEmpty } from 'validator';

const IReactOrganizationUnitType = {
  Pro: 1,
  PublicAuthority: 2,
  TechnicalService: 4,
  FirstResponder: 8,
  UtilitiesProvider: 16,
  Volunteer: 32,
  ExternalProvider: 64,
  PrivateCompany: 128
};

const IReactOrganizationUnitTypes = Object.keys(IReactOrganizationUnitType);

const IReactUserRole = {
  Admin: 2,
  User: 3,
  Pro: 4,
  Citizen: 5,
  ExternalProvider: 6,
  PublicAuthority: 7,
  TechnicalService: 8,
  FirstResponder: 9,
  UtilitiesProvider: 10,
  Volunteer: 11,
  TeamLeader: 12,
  OrganizationResponsible: 13,
  DecisionMaker: 14,
  InfieldAgent: 15
};

const IReactUserRoles = Object.keys(IReactUserRole);

const VALID_GEOMS = ['Polygon', 'MultiPolygon'];

function isValidAOI(wktString) {
  let errorText = '';
  try {
    const geoj = wellknown.parse(wktString);
    if (!geoj) {
      errorText = 'Invalid WKT';
    } else {
      const geom = getGeom(geoj);
      if (VALID_GEOMS.indexOf(geom.type) === -1) {
        errorText = 'AOI must be a valid Polygon/Multipolygon';
      }
    }
  } catch (err) {
    errorText = err.message || err;
  }
  return errorText;
}

export function validateOrganizationUnit({
  OrganizationType = '',
  AreaOfInterest = '',
  DisplayName = '',
  organizationsList = [],
  index = -1
}) {
  const rowIndex = index > -1 ? index + 1 : -1;
  const indexString = rowIndex > -1 ? `Line [${rowIndex}]: ` : '';
  let errors = [];
  let organization = {
    OrganizationType: trim(OrganizationType),
    AreaOfInterest: trim(AreaOfInterest),
    DisplayName: trim(DisplayName)
  };
  try {
    if (isEmpty(organization.DisplayName)) {
      errors.push(new Error(`${indexString}DisplayName cannot be empty`));
    }
    const currentDisplayNames = organizationsList.map(org => org.displayName);
    if (currentDisplayNames.includes(organization.DisplayName)) {
      errors.push(
        new Error(
          `${indexString}An organization with name: "${organization.DisplayName}" already exists`
        )
      );
    }

    if (isEmpty(organization.OrganizationType)) {
      errors.push(new Error(`${indexString}OrganizationType Cannot be empty`));
    }
    // Validate OrganizationType
    if (!IReactOrganizationUnitTypes.includes(organization.OrganizationType)) {
      errors.push(
        new Error(`${indexString}Invalid organziation type: "${organization.OrganizationType}"`)
      );
    }

    if (organization.AreaOfInterest.length === 0) {
      errors.push(new Error(`${indexString}AreaOfInterest cannot be empty`));
    }
    // Validate and Fix aoi
    const wkt = organization.AreaOfInterest;
    const _errorAOI = isValidAOI(wkt);
    if (_errorAOI.length > 0) {
      errors.push(new Error(_errorAOI));
    }
    const geoj = wellknown.parse(wkt);
    const aoi = rewind(feature(geoj, {}));
    organization.AreaOfInterest = wellknown.stringify(aoi);
  } catch (err) {
    errors.push(err);
  }
  return {
    rowIndex,
    isValid: errors.length === 0,
    errors,
    organization
  };
}

export function validateTeam({
  DisplayName = '',
  ParentOrganization = '',
  AreaOfInterest = null,
  organizationsList = [],
  index = -1
}) {
  const rowIndex = index > -1 ? index + 1 : -1;
  const indexString = rowIndex > -1 ? `Line [${rowIndex}]: ` : '';

  let errors = [];
  let team = {
    ParentOrganization: trim(ParentOrganization),
    AreaOfInterest: trim(AreaOfInterest),
    DisplayName: trim(DisplayName)
  };
  try {
    for (const teamFieldName of Object.keys(team)) {
      const teamValue = team[teamFieldName];
      if (teamFieldName !== 'AreaOfInterest' && isEmpty(teamValue)) {
        errors.push(new Error(`${indexString}${teamFieldName} cannot be empty`));
        break;
      }
    }
    const parent =
      team.ParentOrganization.length > 0
        ? organizationsList.find(org => org.displayName === team.ParentOrganization)
        : undefined;
    if (!parent) {
      errors.push(
        new Error(
          `${indexString}Invalid Parent Organization name: "${
            team.ParentOrganization
          }". Such organization does not exist.`
        )
      );
      team['parentId'] = -1;
    } else {
      team['parentId'] = parent.id;
    }
    if (typeof team.AreaOfInterest === 'string' && team.AreaOfInterest.length !== 0) {
      // Validate and Fix aoi
      const wkt = team.AreaOfInterest;
      const _errorAOI = isValidAOI(wkt);
      if (_errorAOI.length > 0) {
        errors.push(new Error(_errorAOI));
      }
      const geoj = wellknown.parse(wkt);
      const aoi = rewind(feature(geoj, {}));
      team.AreaOfInterest = wellknown.stringify(aoi);
    } else if (parent) {
      team.AreaOfInterest = parent.areaOfInterest;
    }
  } catch (err) {
    errors.push(err);
  }
  return { team, errors, isValid: errors.length === 0, rowIndex };
}

export function validateProfessional({
  Username = '',
  FirstName = '',
  Surname = '',
  PhoneNumber = '',
  EmailAddress = '',
  City = '-',
  Country = '',
  Organization = '',
  Team = '',
  Roles = '',
  organizationsList = [],
  teamsList = [],
  index = -1,
  dictionary = t => t
}) {
  const rowIndex = index > -1 ? index + 1 : -1;
  const indexString = rowIndex > -1 ? `Line [${rowIndex}]: ` : '';

  let errors = [];
  const user = {
    Username: trim(Username),
    FirstName: trim(FirstName),
    Surname: trim(Surname),
    PhoneNumber: trim(PhoneNumber),
    EmailAddress: normalizeEmail(trim(EmailAddress), { all_lowercase: true }),
    City: trim(City) || '-',
    Country: trim(Country),
    Organization: trim(Organization),
    Team: trim(Team),
    Roles
  };
  try {
    for (const userFieldName of Object.keys(user)) {
      const userValue = user[userFieldName];
      if (isEmpty(userValue)) {
        errors.push(new Error(`${indexString}${userFieldName} cannot be empty`));
        break;
      }
      if (userFieldName === 'Username') {
        const validationErr = usernameRule(userValue);
        if (validationErr !== null) {
          errors.push(
            new Error(
              `${indexString}${userFieldName}: ${dictionary(validationErr.text, userValue)}`
            )
          );
          break;
        }
      }
      if (userFieldName === 'FirstName') {
        const validationErr = firstnameRule(userValue);
        if (validationErr !== null) {
          errors.push(
            new Error(
              `${indexString}${userFieldName}: ${dictionary(validationErr.text, userValue)}`
            )
          );
          break;
        }
      }
      if (userFieldName === 'Surname') {
        const validationErr = lastnameRule(userValue);
        if (validationErr !== null) {
          errors.push(
            new Error(
              `${indexString}${userFieldName}: ${dictionary(validationErr.text, userValue)}`
            )
          );
          break;
        }
      }
      if (userFieldName === 'EmailAddress') {
        const validationErr = emailRule(userValue);
        if (validationErr !== null) {
          errors.push(
            new Error(
              `${indexString}${userFieldName}: ${dictionary(validationErr.text, userValue)}`
            )
          );
          break;
        }
      }
      if (userFieldName === 'PhoneNumber') {
        const validationErr = mobilePhoneRule(userValue);
        if (validationErr !== null) {
          errors.push(
            new Error(
              `${indexString}${userFieldName}: ${dictionary(validationErr.text, userValue)}`
            )
          );
          break;
        }
      }
    }

    const org =
      Organization.length > 0
        ? organizationsList.find(org => org.displayName === Organization)
        : undefined;
    if (!org) {
      errors.push(
        new Error(
          `${indexString}Invalid Organization: "${Organization}". Such organization does not exist.`
        )
      );
      user['orgId'] = -1;
    } else {
      user['orgId'] = org.id;
    }
    const team = Team.length > 0 ? teamsList.find(t => t.displayName === Team) : undefined;
    if (!team) {
      errors.push(
        new Error(
          `${indexString}Invalid Team: "${Team}". Such team does not exist for organization: ${Organization}.`
        )
      );
      user['teamIds'] = [];
    } else {
      user['teamIds'] = [team.id];
    }

    const userRoles = Roles.replace(/;$/, '').split(';');
    const isValidRole = Roles.length > 0 && userRoles.every(r => IReactUserRoles.includes(r));
    if (!isValidRole) {
      errors.push(new Error(`${indexString}Invalid Roles: ${Roles}`));
      user['roles'] = [];
    } else {
      user['roles'] = userRoles;
    }
  } catch (err) {
    errors.push(err);
  }
  return { errors, isValid: errors.length === 0, user, rowIndex };
}

export function fakeUpload(el) {
  return new Promise(s =>
    setTimeout(() => {
      console.log('Uploaded', el);
      s();
    }, 2500)
  );
}
