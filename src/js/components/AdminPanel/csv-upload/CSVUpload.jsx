import React, { useEffect, useState } from 'react';
import { CSVUploadContainer, ErrorBox, Errors } from './csv-upload.components';
import { useLogin } from 'js/hooks/use-login.hook';
import { DropLine } from './UploadCsvButton';
import { logMain } from 'js/utils/log';
import { useCSV } from 'js/hooks/csv-hooks';
import { useQuickUserAdd } from 'js/hooks/use-admin.hook';
import { useUploadProgress, useOrganizationTeams } from './csv-upload.hooks';
import {
  validateOrganizationUnit,
  validateProfessional,
  validateTeam,
  fakeUpload
} from './admin.utils';
import { useLocale } from 'js/hooks/use-locale.hook';

export function CSVUpload() {
  const [{ dictionary }] = useLocale();

  const [{ user }] = useLogin();
  const [serversideErrors, setServersideErrors] = useState([]);
  const [
    { organizationsList, /* teamsList, */ error, loading },
    { registerOrganizationUnit, registerTeam, registerProfessional, cleanState }
  ] = useQuickUserAdd();
  const teamsList = useOrganizationTeams(organizationsList);
  const [organizations, loadOrganizationsCSV, resetOrganizations] = useCSV('');
  const [teams, loadTeamsCSV, resetTeams] = useCSV('');
  const [users, loadUsersCSV, resetUsers] = useCSV('');
  const [parsingErrors, setParsingErrors] = useState([]);
  const [validationErrors, setValidationErrors] = useState([]);
  const [uploadProgress, initUploadProgress, incrementUploadProgress] = useUploadProgress();

  function clearAll() {
    resetOrganizations();
    resetTeams();
    resetUsers();
    initUploadProgress(0);
    cleanState();
  }

  function queueServerSideError(apiErrors) {
    if (apiErrors.length) {
      setServersideErrors(apiErrors);
    }
  }

  useEffect(() => {
    if (error !== null) {
      setServersideErrors([...serversideErrors, error.error]);
    }
  }, [error]);

  function resetServerSideErrors() {
    setServersideErrors([]);
    clearAll();
  }

  function resetParsingErrors() {
    setParsingErrors('');
    clearAll();
  }

  function resetValidationErrors() {
    setValidationErrors('');
    clearAll();
  }

  async function uploadElements(type, elements) {
    let uploader = fakeUpload;
    switch (type) {
      case 'organizations':
        uploader = el =>
          registerOrganizationUnit(
            el.organization.OrganizationType,
            el.organization.AreaOfInterest,
            el.organization.DisplayName
          );
        break;
      case 'teams':
        uploader = el =>
          registerTeam(el.team.AreaOfInterest, el.team.DisplayName, el.team.parentId);
        break;
      case 'users':
        uploader = el =>
          registerProfessional(
            el.user.FirstName,
            el.user.Surname,
            el.user.Username,
            el.user.PhoneNumber,
            el.user.EmailAddress,
            el.user.City,
            el.user.Country,
            el.user.orgId,
            el.user.teamIds,
            el.user.roles
          );
        break;
      default:
        break;
    }
    // todo switch type and get selector
    initUploadProgress(elements.length);
    let apiErrors = [];
    for (const el of elements) {
      try {
        await uploader(el);
        incrementUploadProgress();
      } catch (err) {
        let _err = new Error(`Line [${el.rowIndex}] - ${err.message}`);
        apiErrors = [...apiErrors, _err];
      }
    }
    queueServerSideError(apiErrors);
  }

  useEffect(() => {
    if (organizations.data.length > 0) {
      const orgs = organizations.data.map((co, index) =>
        validateOrganizationUnit({ ...co, organizationsList, index })
      );
      const validOrgs = orgs.filter(o => o.isValid);
      console.log('Organizations', orgs);
      const _valErrors = orgs.filter(o => !o.isValid).map(o => o.errors.map(e => e.message));
      setValidationErrors(_valErrors);
      uploadElements('organizations', validOrgs);
    }
    if (organizations.errors.length > 0) {
      setParsingErrors(organizations.errors.map(e => JSON.stringify(e)));
    }
  }, [organizations]);

  useEffect(() => {
    if (teams.data.length > 0) {
      const vteams = teams.data.map((tm, index) =>
        validateTeam({ ...tm, organizationsList, index })
      );
      console.log('Teams', vteams);
      const validTeams = vteams.filter(t => t.isValid);
      const _valErrors = vteams.filter(o => !o.isValid).map(o => o.errors.map(e => e.message));
      setValidationErrors(_valErrors);
      uploadElements('teams', validTeams);
    }
    if (teams.errors.length > 0) {
      setParsingErrors(teams.errors.map(e => JSON.stringify(e)));
    }
  }, [teams]);

  useEffect(() => {
    if (users.data.length > 0 && teamsList.length > 0) {
      const vusers = users.data.map((user, index) =>
        validateProfessional({ ...user, organizationsList, teamsList, index, dictionary })
      );
      console.log('Users', vusers);
      const validUsers = vusers.filter(t => t.isValid);
      const _valErrors = vusers.filter(o => !o.isValid).map(o => o.errors.map(e => e.message));
      setValidationErrors(_valErrors);
      uploadElements('users', validUsers);
    }
    if (users.errors.length > 0) {
      setParsingErrors(teams.errors.map(e => JSON.stringify(e)));
    }
  }, [users, teamsList]);

  function onOrgFileContentLoaded(result) {
    logMain('ORG FILE UPL', result);
    loadOrganizationsCSV(result.content);
  }

  function onTeamFileContentLoaded(result) {
    logMain('TEAM FILE UPL', result);
    loadTeamsCSV(result.content);
  }

  function onUserFileContentLoaded(result) {
    logMain('USER FILE UPL', result);
    loadUsersCSV(result.content);
  }

  const hasErrors =
    parsingErrors.length > 0 || validationErrors.length > 0 || serversideErrors.length > 0;

  if (user.userName !== 'admin') {
    return null;
  } else {
    return (
      <CSVUploadContainer className="upload-csv">
        <h4>CSVUPLOAD {user.name}</h4>
        {uploadProgress.total > 0 && (
          <h6>
            Uploading {uploadProgress.count}/{uploadProgress.total}
          </h6>
        )}
        <DropLine
          id="org"
          onFileContentLoaded={onOrgFileContentLoaded}
          label="Upload Organizations"
          description="Drag here or use this button for loading organizations"
          fileAccept="text/csv"
          disabled={loading}
        />
        <DropLine
          id="teams"
          onFileContentLoaded={onTeamFileContentLoaded}
          label="Upload Teams"
          description="Drag here or use this button for loading teams"
          fileAccept="text/csv"
          disabled={loading}
        />
        <DropLine
          id="user"
          onFileContentLoaded={onUserFileContentLoaded}
          label="Upload Users"
          description="Drag here or use this button for loading users (professionals)"
          fileAccept="text/csv"
          disabled={loading}
        />
        {hasErrors && (
          <Errors>
            {parsingErrors.length > 0 && (
              <ErrorBox onClick={resetParsingErrors}>
                <h5>CSV Parsing errors</h5>
                <small>Click this box to dismiss</small>
                {parsingErrors.map((pe, i) => (
                  <div key={i} style={{ marginBottom: 8 }}>
                    {pe}
                  </div>
                ))}
              </ErrorBox>
            )}
            {validationErrors.length > 0 && (
              <ErrorBox onClick={resetValidationErrors}>
                <h5>Validation errors</h5>
                <small>Click this box to dismiss</small>
                {validationErrors.map((ve, i) => (
                  <div key={i} style={{ marginBottom: 8 }}>
                    {ve.map((veMsg, j) => (
                      <div key={`${i}-${j}`}>{veMsg}</div>
                    ))}
                  </div>
                ))}
              </ErrorBox>
            )}
            {serversideErrors.length > 0 && (
              <ErrorBox onClick={resetServerSideErrors}>
                <h5>Server or upload errors</h5>
                <small>Click this box to dismiss</small>
                {serversideErrors.map((e, i) => (
                  <div key={i} style={{ marginBottom: 8 }}>
                    {JSON.stringify(e.message)}
                  </div>
                ))}
              </ErrorBox>
            )}
          </Errors>
        )}
      </CSVUploadContainer>
    );
  }
}

export default CSVUpload;
