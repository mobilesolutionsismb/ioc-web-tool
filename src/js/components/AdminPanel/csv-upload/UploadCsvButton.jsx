import React from 'react';
import { useFileReaders } from 'js/hooks/upload-csv.hook';
import RaisedButton from 'material-ui/RaisedButton';
import { useMessages, useLoader } from 'js/hooks/use-ui.hooks';
import FileDrop from 'react-file-drop';

import styled from 'styled-components';
// fileAccept: '*', // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
// fileReadMode: 'text', // possible values are 'dataurl', 'binarystring', 'buffer' https://developer.mozilla.org/en-US/docs/Web/API/FileReader#Methods

const DropLineContainer = styled.div.attrs(() => ({ className: 'drop-line' }))`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  border: 2px solid;
  padding: 32px;
  min-width: 520px;
  position: relative;

  .file-drop {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;

    &.disabled {
      background-color: #544;
      &::after {
        content: 'Drop disabled, please wait';
        position: relative;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        display: flex;
        text-align: center;
        justify-content: center;
        align-content: center;
      }
    }
  }

  p {
    background-color: ${props => props.theme.paper.backgroundColor};
    margin-left: -180px;
    margin-top: -86px;
  }
`;

function _nullClick(event) {
  event.target.value = null;
}

function UploadCsvButtonInner({
  id,
  fileAccept,
  fileMultiple,
  FileInputProps,
  onFileUpload,
  loading,
  label,
  style,
  disabled = false,
  buttonStyle = { width: 180, color: '#fff' }
}) {
  return (
    <RaisedButton
      disabled={disabled}
      primary={true}
      style={{ ...style, position: 'relative' }}
      buttonStyle={buttonStyle}
      labelStyle={{ width: '100%', height: '100%' }}
    >
      <input
        disabled={disabled}
        key="file-input"
        accept={fileAccept}
        style={{
          visibility: 'hidden',
          position: 'absolute',
          left: 0,
          top: 0,
          width: '100%',
          height: '100%'
        }}
        id={id}
        type="file"
        multiple={fileMultiple}
        {...FileInputProps}
        onInput={onFileUpload}
        onClick={_nullClick}
      />
      <label key="file-input-label" htmlFor={id} style={{ display: 'block', flexGrow: 1 }}>
        {loading ? 'Loading...' : label}
      </label>
    </RaisedButton>
  );
}

export function UploadCsvButton({
  id, //MANDATORY
  onFileContentLoaded, //MANDATORY
  label = 'Upload stuff',
  fileAccept = '*',
  fileReadMode = 'text',
  fileMultiple = false,
  FileInputProps = {},
  style,
  disabled = false,
  buttonStyle
}) {
  const [{ pushError }] = useMessages();
  const [{ loading }, { loadingStart, loadingStop }] = useLoader();
  const { onFileUpload } = useFileReaders(
    fileReadMode,
    fileMultiple,
    onFileContentLoaded,
    loadingStart,
    loadingStop,
    pushError
  );

  const innerButtonProps = {
    id,
    label,
    loading,
    fileAccept,
    fileMultiple,
    onFileUpload,
    FileInputProps,
    style,
    buttonStyle
  };

  return <UploadCsvButtonInner {...innerButtonProps} />;
}

export function DropLine({
  onFileContentLoaded,
  id,
  label,
  description,
  fileAccept = '*',
  fileReadMode = 'text',
  fileMultiple = false,
  disabled = false,
  ...rest
}) {
  const [{ pushError }] = useMessages();
  const [{ loading }, { loadingStart, loadingStop }] = useLoader();
  const { onFileUpload, readFiles } = useFileReaders(
    fileReadMode,
    fileMultiple,
    onFileContentLoaded,
    loadingStart,
    loadingStop,
    pushError
  );

  function _handleDrop(files) {
    if (files.length > 0) {
      const filesArray = Array.from(files);
      if (fileAccept !== '*' && filesArray.some(f => f.type !== fileAccept)) {
        pushError(new Error(`File of type other than ${fileAccept} are not accepted`));
      } else {
        readFiles(files);
      }
    }
  }

  const innerButtonProps = {
    id,
    label,
    loading,
    fileAccept,
    fileMultiple,
    onFileUpload,
    ...rest
  };

  return (
    <DropLineContainer>
      {disabled ? <div className="file-drop disabled" /> : <FileDrop onDrop={_handleDrop} />}
      <UploadCsvButtonInner {...innerButtonProps} disabled={disabled} />
      <p>{loading ? 'Loading file...' : description}</p>
    </DropLineContainer>
  );
}
