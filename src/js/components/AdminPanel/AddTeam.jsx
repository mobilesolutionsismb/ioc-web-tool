import React, { Component } from 'react';
import { /* SelectField, MenuItem, */ TextField, RaisedButton, AutoComplete } from 'material-ui';
import { getGeom } from '@turf/invariant';
import wellknown from 'wellknown';
import rewind from '@turf/rewind';
import { feature } from '@turf/helpers';

const VALID_GEOMS = ['Polygon', 'MultiPolygon'];

const STYLE = {
  input: {
    marginLeft: 32
  }
};

class AddTeam extends Component {
  state = {
    AreaOfInterest: '',
    DisplayName: '',
    parentId: -1,
    orgSearchText: ''
  };

  static defaultProps = {
    organizationsList: []
  };

  handleTextFieldChange = (fieldname, event) => this.setState({ [fieldname]: event.target.value });

  _clear = () => {
    this.setState({
      AreaOfInterest: '',
      DisplayName: '',
      parentId: -1,
      orgSearchText: ''
    });
    this.props.cleanState();
    this.props.listOrganizations();
  };

  _isValidState(state) {
    const { AreaOfInterest, DisplayName, parentId } = state;
    return (
      parentId >= -1 &&
      this._isValidAOI(AreaOfInterest.trim()).length === 0 &&
      DisplayName.trim().length > 0
    );
  }

  _submit = async () => {
    try {
      const { AreaOfInterest, DisplayName, parentId } = this.state;
      await this.props.registerTeam(AreaOfInterest, DisplayName, parentId);
    } catch (err) {
      console.error('Org creation error', err);
    }
  };

  _getRewindedAOI = wkt => {
    if (wkt.length && this._isValidAOI(wkt) === '') {
      try {
        const geoj = wellknown.parse(wkt);
        const rew = rewind(feature(geoj, {}));
        return wellknown.stringify(rew);
      } catch (err) {
        throw err;
      }
    } else {
      return this.state.AreaOfInterest;
    }
  };

  onOrganizationChange = async (orgSearchText, dataSource) => {
    const selectedOrg = dataSource.find(org => org.displayName === orgSearchText);
    this.setState({
      parentId: typeof selectedOrg === 'undefined' ? -1 : selectedOrg.id,
      orgSearchText
    });
  };

  _isValidAOI = wkt => {
    let errorText = '';
    if (wkt.length) {
      try {
        const geoj = wellknown.parse(wkt);
        if (!geoj) {
          errorText = 'Invalid WKT';
        } else {
          const geom = getGeom(geoj);
          if (VALID_GEOMS.indexOf(geom.type) === -1) {
            errorText = 'AOI must be a valid Polygon/Multipolygon';
          }
        }
      } catch (err) {
        console.log('ERR', errorText);
        errorText = err.message || err;
      }
    }
    return errorText;
  };

  render() {
    const { organizationsList } = this.props;

    return (
      <div className="team-add form">
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="Team Display Name"
            id="DisplayName"
            value={this.state.DisplayName}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'DisplayName')}
          />
        </div>
        <div className="input">
          <AutoComplete
            fullWidth={true}
            style={STYLE.input}
            hintText="Search Organization"
            floatingLabelText="Search Organization"
            filter={AutoComplete.fuzzyFilter}
            dataSource={organizationsList}
            dataSourceConfig={{
              text: 'displayName',
              value: 'id'
            }}
            maxSearchResults={10}
            searchText={this.state.orgSearchText}
            onUpdateInput={this.onOrganizationChange}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            errorText={this._isValidAOI(this.state.AreaOfInterest.trim())}
            floatingLabelText="Area of Interest (WKT STRING)"
            id="AreaOfInterest"
            value={this._getRewindedAOI(this.state.AreaOfInterest.trim())}
            fullWidth={true}
            multiLine={true}
            rows={4}
            rowsMax={4}
            onChange={this.handleTextFieldChange.bind(this, 'AreaOfInterest')}
          />
        </div>
        <div className="input">
          <RaisedButton
            disabled={!this._isValidState(this.state)}
            onClick={this._submit}
            label="Submit"
            primary={true}
            style={STYLE.input}
          />
          <RaisedButton onClick={this._clear} label="Clear" style={STYLE.input} />
        </div>
      </div>
    );
  }
}

export default AddTeam;
