import React, { Component } from 'react';
import { SelectField, MenuItem, TextField, RaisedButton } from 'material-ui';
import { getGeom } from '@turf/invariant';
import wellknown from 'wellknown';
import rewind from '@turf/rewind';
import { feature } from '@turf/helpers';

const IReactOrganizationUnitType = {
  Pro: 1,
  PublicAuthority: 2,
  TechnicalService: 4,
  FirstResponder: 8,
  UtilitiesProvider: 16,
  Volunteer: 32,
  ExternalProvider: 64,
  PrivateCompany: 128
};

const IReactOrganizationUnitTypeValues = Object.keys(IReactOrganizationUnitType);

const VALID_GEOMS = ['Polygon', 'MultiPolygon'];

const STYLE = {
  input: {
    marginLeft: 32
  }
};

class AddOrganization extends Component {
  state = {
    OrganizationType: 'Not Selected',
    AreaOfInterest: '',
    DisplayName: ''
  };

  onOrganizationTypeChange = (event, index, value) => this.setState({ OrganizationType: value });

  handleTextFieldChange = (fieldname, event) => this.setState({ [fieldname]: event.target.value });

  _clear = () => {
    this.setState({
      OrganizationType: 'Not Selected',
      AreaOfInterest: '',
      DisplayName: ''
    });
    this.props.cleanState();
    this.props.listOrganizations();
  };

  _isValidState(state) {
    const { OrganizationType, AreaOfInterest, DisplayName } = state;
    return (
      IReactOrganizationUnitTypeValues.indexOf(OrganizationType) >= 0 &&
      this._isValidAOI(AreaOfInterest.trim()).length === 0 &&
      DisplayName.trim().length > 0
    );
  }

  _submit = async () => {
    try {
      const { OrganizationType, AreaOfInterest, DisplayName } = this.state;
      await this.props.registerOrganizationUnit(OrganizationType, AreaOfInterest, DisplayName);
    } catch (err) {
      console.error('Org creation error', err);
    }
  };

  _getRewindedAOI = wkt => {
    if (wkt.length && this._isValidAOI(wkt) === '') {
      try {
        const geoj = wellknown.parse(wkt);
        const rew = rewind(feature(geoj, {}));
        return wellknown.stringify(rew);
      } catch (err) {
        throw err;
      }
    } else {
      return this.state.AreaOfInterest;
    }
  };

  _isValidAOI = wkt => {
    let errorText = '';
    if (wkt.length) {
      try {
        const geoj = wellknown.parse(wkt);
        if (!geoj) {
          errorText = 'Invalid WKT';
        } else {
          const geom = getGeom(geoj);
          if (VALID_GEOMS.indexOf(geom.type) === -1) {
            errorText = 'AOI must be a valid Polygon/Multipolygon';
          }
        }
      } catch (err) {
        errorText = err.message || err;
      }
    }
    console.log('ERR', errorText);
    return errorText;
  };

  render() {
    return (
      <div className="organization-add form">
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="Organization Display Name"
            id="DisplayName"
            value={this.state.DisplayName}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'DisplayName')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            errorText={this._isValidAOI(this.state.AreaOfInterest.trim())}
            floatingLabelText="Area of Interest (WKT STRING)"
            id="AreaOfInterest"
            value={this._getRewindedAOI(this.state.AreaOfInterest.trim())}
            fullWidth={true}
            multiLine={true}
            rows={4}
            rowsMax={4}
            onChange={this.handleTextFieldChange.bind(this, 'AreaOfInterest')}
          />
        </div>
        <div className="input">
          <SelectField
            floatingLabelText="Select Organization Type"
            style={STYLE.input}
            value={this.state.OrganizationType}
            onChange={this.onOrganizationTypeChange}
          >
            <MenuItem value={-1} primaryText="Not Selected" />
            {IReactOrganizationUnitTypeValues.map((type, index) => (
              <MenuItem key={index} value={type} primaryText={type} />
            ))}
          </SelectField>
        </div>
        <div className="input">
          <RaisedButton
            disabled={!this._isValidState(this.state)}
            onClick={this._submit}
            label="Submit"
            primary={true}
            style={STYLE.input}
          />
          <RaisedButton onClick={this._clear} label="Clear" style={STYLE.input} />
        </div>
      </div>
    );
  }
}

export default AddOrganization;
