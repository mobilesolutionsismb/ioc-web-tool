import './styles.scss';
import React, { Component, lazy, Suspense } from 'react';
import { withDictionary } from 'ioc-localization';
import { withRouter } from 'react-router-dom';
import { withAdminQuickUserAdd } from 'js/modules/QuickUserAdd';
import { SelectField, MenuItem, CircularProgress, IconButton } from 'material-ui';
import { Redirect } from 'react-router-dom';
import { withLogin } from 'ioc-api-interface';
// import AddOrganization from './AddOrganization';
// import AddTeam from './AddTeam';
// import AddProfessional from './AddProfessional';
// import AddCitizen from './AddCitizen';
// import ShowOrganization from './ShowOrganization';

const AddOrganization = lazy(() => import('./AddOrganization'));
const AddTeam = lazy(() => import('./AddTeam'));
const AddProfessional = lazy(() => import('./AddProfessional'));
const AddCitizen = lazy(() => import('./AddCitizen'));
const ShowOrganization = lazy(() => import('./ShowOrganization'));

// import { CSVUpload } from './csv-upload/CSVUpload';
const CSVUpload = lazy(() => import('./csv-upload/CSVUpload'));

const Loading = () => <div>Loading...</div>;

const ITEM_TYPES = [
  { type: 'show organization', label: 'Show Organization', icon: '👁' },
  { type: 'organization', label: 'Add Organization', icon: '⛑️' },
  { type: 'team', label: 'Add Team', icon: '⛏️' },
  { type: 'professional', label: 'Add Professional', icon: '👨‍🚒' },
  { type: 'citizen', label: 'Add Citizen', icon: '👤' }
];
class AdminPanel extends Component {
  state = {
    itemTypeIndex: -1
  };

  static defaultProps = {
    organizationsList: [],
    teamsList: [],
    orgAddState: 0,
    citizenAddState: 0,
    professionalState: 0,
    error: null
  };

  componentDidMount() {
    this.props.listOrganizations();
  }

  componentDidUpdate(nextProps, nextState) {
    if (nextState.itemTypeIndex !== this.state.itemTypeIndex) {
      if (
        nextState.itemTypeIndex > -1 &&
        ITEM_TYPES[nextState.itemTypeIndex].type === 'professional'
      ) {
        nextProps.listOrganizations();
      }
    }
  }

  onItemTypeChange = (event, index, value) => this.setState({ itemTypeIndex: value });

  _isLoading = () => {
    const { organization, team, citizen, professional } = this.props;
    return (
      organization === 'loading' ||
      team === 'loading' ||
      citizen === 'loading' ||
      professional === 'loading'
    );
  };

  _isSuccess = () => {
    const { organization, team, citizen, professional } = this.props;
    return (
      organization === 'success' ||
      team === 'success' ||
      citizen === 'success' ||
      professional === 'success'
    );
  };

  _isError = () => {
    const { organization, team, citizen, professional } = this.props;
    return (
      organization === 'error' ||
      team === 'error' ||
      citizen === 'error' ||
      professional === 'error'
    );
  };

  _reset = () => {
    this.setState({ itemTypeIndex: -1 });
    this.props.resetUserAndOrganizationData();
    this.props.listOrganizations();
  };

  render() {
    const {
      loggedIn,
      /* dictionary,  */
      cleanState,
      listOrganizations,
      organization,
      orgUsers,
      citizen,
      professional,
      team,
      organizationsList,
      teamsList,
      clearTeams,
      listTeamsForOrganizationId,
      error,
      registerOrganizationUnit,
      registerTeam,
      registerCitizen,
      registerProfessional
    } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" />;
    }
    const itemTypeIndex = this.state.itemTypeIndex;
    const itemType = itemTypeIndex >= 0 ? ITEM_TYPES[itemTypeIndex].type : null;
    return (
      <div className="quick-user-add page">
        {this._isLoading() && <CircularProgress size={80} thickness={5} style={{ top: '30%' }} />}
        <h3>Quick User and Organizations Add Panel</h3>
        <div className="select-op">
          <SelectField
            disabled={error !== null || this._isLoading()}
            floatingLabelText="Select operation"
            value={this.state.itemTypeIndex}
            onChange={this.onItemTypeChange}
          >
            <MenuItem
              value={-1}
              primaryText="CSV Upload"
              label={'📄 CSV Upload'}
              leftIcon={
                <span role="img" aria-label="icon">
                  📄
                </span>
              }
            />
            {ITEM_TYPES.map((it, index) => (
              <MenuItem
                key={index}
                value={index}
                label={`${it.icon} ${it.label}`}
                primaryText={it.label}
                leftIcon={
                  <span role="img" aria-label="icon">
                    {it.icon}
                  </span>
                }
              />
            ))}
          </SelectField>
          {this.state.itemTypeIndex !== -1 && (
            <IconButton tooltip="Reset" onClick={this._reset}>
              <i className="material-icons md-18 md-light">clear</i>
            </IconButton>
          )}
        </div>
        {itemType === 'show organization' && (
          <Suspense fallback={<Loading />}>
            <ShowOrganization
              organizationsList={organizationsList}
              orgUsers={orgUsers}
              cleanState={cleanState}
              listOrganizationsUsers={value => {
                this.props.listOrganizationsUsers(value);
              }}
            />
          </Suspense>
        )}
        {itemType === 'organization' && (
          <Suspense fallback={<Loading />}>
            <AddOrganization
              listOrganizations={listOrganizations}
              organizationsList={organizationsList}
              cleanState={cleanState}
              registerOrganizationUnit={registerOrganizationUnit}
            />
          </Suspense>
        )}
        {itemType === 'team' && (
          <Suspense fallback={<Loading />}>
            <AddTeam
              listOrganizations={listOrganizations}
              organizationsList={organizationsList}
              cleanState={cleanState}
              registerTeam={registerTeam}
            />
          </Suspense>
        )}
        {itemType === 'professional' && (
          <Suspense fallback={<Loading />}>
            <AddProfessional
              listOrganizations={listOrganizations}
              organizationsList={organizationsList}
              teamsList={teamsList}
              listTeamsForOrganizationId={listTeamsForOrganizationId}
              clearTeams={clearTeams}
              cleanState={cleanState}
              registerProfessional={registerProfessional}
            />
          </Suspense>
        )}
        {itemType === 'citizen' && (
          <Suspense fallback={<Loading />}>
            <AddCitizen
              listOrganizations={listOrganizations}
              cleanState={cleanState}
              registerCitizen={registerCitizen}
            />
          </Suspense>
        )}
        {itemType === null && (
          <Suspense fallback={<Loading />}>
            <CSVUpload />
          </Suspense>
        )}
        {this._isError() && (
          <div style={{ color: 'red' }}>
            {<span>{`${error.errorType}: ${error.error.message}`}</span>}
          </div>
        )}
        {organization === 'success' && (
          <div style={{ color: 'chartreuse' }}>
            {<span>{'Organization added successfully'}</span>}
          </div>
        )}
        {team === 'success' && (
          <div style={{ color: 'chartreuse' }}>{<span>{'Team added successfully'}</span>}</div>
        )}
        {professional === 'success' && (
          <div style={{ color: 'chartreuse' }}>
            {<span>{'Professional user added successfully'}</span>}
          </div>
        )}
        {citizen === 'success' && (
          <div style={{ color: 'chartreuse' }}>
            {<span>{'Citizen user added successfully'}</span>}
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(withAdminQuickUserAdd(withDictionary(withLogin(AdminPanel))));
