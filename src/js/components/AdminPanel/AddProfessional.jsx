import React, { Component } from 'react';
import { SelectField, AutoComplete, MenuItem, TextField, RaisedButton } from 'material-ui';
import { isValidEmail, isValidPhone } from 'js/utils/isValidEmail';

const STYLE = {
  input: {
    marginLeft: 32
  }
};

class AddProfessional extends Component {
  state = {
    name: '',
    surname: '',
    username: '',
    phone: '',
    email: '',
    city: '',
    country: '',
    organizationUnitId: -1,
    orgSearchText: '',
    selectedTeams: []
  };

  static defaultProps = {
    organizationsList: []
  };

  onOrganizationChange = async (orgSearchText, dataSource) => {
    const selectedOrg = dataSource.find(org => org.displayName === orgSearchText);
    if (typeof selectedOrg !== 'undefined') {
      this.setState({ organizationUnitId: selectedOrg.id, orgSearchText });
      await this.props.listTeamsForOrganizationId(selectedOrg.id);
    } else {
      this.props.clearTeams();
      this.setState({ organizationUnitId: -1, selectedTeams: [], orgSearchText });
    }
  };

  onTeamChange = (event, index, selectedTeams) => {
    console.log('onTeamChange', index, selectedTeams);
    this.setState({ selectedTeams });
  };

  handleTextFieldChange = (fieldname, event) => this.setState({ [fieldname]: event.target.value });

  _isValidState(state) {
    const {
      name,
      surname,
      username,
      phone,
      email,
      city,
      country,
      organizationUnitId,
      selectedTeams
    } = state;
    return (
      name.trim().length > 0 &&
      surname.trim().length > 0 &&
      username.trim().length > 0 &&
      phone.trim().length > 0 &&
      email.trim().length > 0 &&
      city.trim().length > 0 &&
      country.trim().length > 0 &&
      (organizationUnitId !== -1 || selectedTeams.length > 0)
    );
  }

  _clear = () => {
    this.setState({
      orgItemIndex: -1,
      name: '',
      surname: '',
      username: '',
      phone: '',
      email: '',
      city: '',
      country: '',
      orgSearchText: '',
      organizationUnitId: -1,
      selectedTeams: []
    });
    this.props.cleanState();
    this.props.listOrganizations();
  };

  _submit = async () => {
    try {
      const {
        name,
        surname,
        username,
        phone,
        email,
        city,
        country,
        organizationUnitId,
        selectedTeams
      } = this.state;
      await this.props.registerProfessional(
        name,
        surname,
        username,
        phone,
        email.toLowerCase(),
        city,
        country,
        selectedTeams.length === 0 ? organizationUnitId : -1,
        selectedTeams
      );
    } catch (err) {
      console.error('Submit professional error', err);
    }
  };

  _menuItems = values => {
    return this.props.teamsList.map((team, i) => (
      <MenuItem
        key={team.id}
        insetChildren={true}
        checked={values && values.indexOf(team.id) > -1}
        value={team.id}
        primaryText={team.displayName}
      />
    ));
  };

  render() {
    const { organizationsList } = this.props;
    return (
      <div className="professional-add form">
        <div className="input">
          <AutoComplete
            fullWidth={true}
            style={STYLE.input}
            hintText="Search Organization"
            floatingLabelText="Search Organization"
            filter={AutoComplete.fuzzyFilter}
            dataSource={organizationsList}
            dataSourceConfig={{
              text: 'displayName',
              value: 'id'
            }}
            maxSearchResults={10}
            searchText={this.state.orgSearchText}
            onUpdateInput={this.onOrganizationChange}
          />
        </div>
        <div className="input">
          <SelectField
            floatingLabelText="Select Teams"
            hintText="Select one or more Team"
            style={STYLE.input}
            fullWidth={true}
            multiple={true}
            value={this.state.selectedTeams}
            onChange={this.onTeamChange}
          >
            {this._menuItems(this.state.selectedTeams)}
          </SelectField>
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="Username"
            id="username"
            value={this.state.username}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'username')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="First Name"
            id="name"
            value={this.state.name}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'name')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="Surname"
            id="surname"
            value={this.state.surname}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'surname')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            errorText={
              this.state.phone.length && !isValidPhone(this.state.phone)
                ? 'Invalid Phone Number'
                : ''
            }
            floatingLabelText="phone number"
            id="email"
            value={this.state.phone.toLowerCase()}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'phone')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            errorText={
              this.state.email.length && !isValidEmail(this.state.email) ? 'Invalid Email' : ''
            }
            floatingLabelText="e-mail address"
            id="email"
            value={this.state.email.toLowerCase()}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'email')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="City"
            id="city"
            value={this.state.city}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'city')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="Country"
            id="country"
            value={this.state.country}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'country')}
          />
        </div>
        <div className="input">
          <RaisedButton
            disabled={!this._isValidState(this.state)}
            onClick={this._submit}
            label="Submit"
            primary={true}
            style={STYLE.input}
          />
          <RaisedButton onClick={this._clear} label="Clear" style={STYLE.input} />
        </div>
      </div>
    );
  }
}

export default AddProfessional;
