import React, { Component } from 'react';
import { TextField, RaisedButton } from 'material-ui';
import { isValidEmail, isValidPhone } from 'js/utils/isValidEmail';

const STYLE = {
  input: {
    marginLeft: 32
  }
};

class AddCitizen extends Component {
  state = {
    name: '',
    surname: '',
    username: '',
    phone: '',
    email: '',
    city: '',
    country: ''
  };

  handleTextFieldChange = (fieldname, event) => this.setState({ [fieldname]: event.target.value });

  _isValidState(state) {
    const { name, surname, username, phone, email, city, country, organizationUnitId } = state;
    return (
      name.trim().length > 0 &&
      surname.trim().length > 0 &&
      username.trim().length > 0 &&
      phone.trim().length > 0 &&
      email.trim().length > 0 &&
      city.trim().length > 0 &&
      country.trim().length > 0 &&
      organizationUnitId !== -1
    );
  }

  _clear = () => {
    this.setState({
      orgItemIndex: -1,
      name: '',
      surname: '',
      username: '',
      phone: '',
      email: '',
      city: '',
      country: '',
      organizationUnitId: null
    });
    this.props.cleanState();
    this.props.listOrganizations();
  };

  _submit = async () => {
    try {
      const {
        name,
        surname,
        username,
        phone,
        email,
        city,
        country,
        organizationUnitId
      } = this.state;
      await this.props.registerCitizen(
        name,
        surname,
        username,
        phone,
        email.toLocaleLowerCase(),
        city,
        country,
        organizationUnitId
      );
    } catch (err) {
      console.error('Submit citizen error', err);
    }
  };

  render() {
    return (
      <div className="citizen-add form">
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="Username"
            id="username"
            value={this.state.username}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'username')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="First Name"
            id="name"
            value={this.state.name}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'name')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="Surname"
            id="surname"
            value={this.state.surname}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'surname')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            errorText={
              this.state.phone.length && !isValidPhone(this.state.phone)
                ? 'Invalid Phone Number'
                : ''
            }
            floatingLabelText="phone number"
            id="email"
            value={this.state.phone.toLowerCase()}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'phone')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            errorText={
              this.state.email.length && !isValidEmail(this.state.email) ? 'Invalid Email' : ''
            }
            floatingLabelText="e-mail address"
            id="email"
            value={this.state.email.toLowerCase()}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'email')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="City"
            id="city"
            value={this.state.city}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'city')}
          />
        </div>
        <div className="input">
          <TextField
            style={STYLE.input}
            floatingLabelText="Country"
            id="country"
            value={this.state.country}
            fullWidth={true}
            onChange={this.handleTextFieldChange.bind(this, 'country')}
          />
        </div>
        <div className="input">
          <RaisedButton
            disabled={!this._isValidState(this.state)}
            onClick={this._submit}
            label="Submit"
            primary={true}
            style={STYLE.input}
          />
          <RaisedButton onClick={this._clear} label="Clear" style={STYLE.input} />
        </div>
      </div>
    );
  }
}

export default AddCitizen;
