import React, { Component } from 'react';
import {
  SelectField,
  MenuItem,
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  TextField
} from 'material-ui';
import { getSimplifiedDateTime } from 'js/utils/getSimplifiedDateTime';
import { withDictionary } from 'ioc-localization';
const STYLE = {
  tableHeader: {
    color: 'black',
    fontWeight: 'bold',
    borderRight: '1px gray solid'
  }
};

class ShowOrganization extends Component {
  state = {
    selectedOrg: -1,
    quickSearch: ''
  };

  onItemTypeChange = (event, index, value) => {
    this.setState({ selectedOrg: value });
    this.props.listOrganizationsUsers(value);
  };
  _printName = id => {
    const org = this.props.organizationsList.find(x => x.id === id);
    return org !== undefined ? `${org.displayName}` : '-';
  };

  handleTextFieldChange = (event, value) => {
    this.setState({ quickSearch: value });
  };

  searchRole = (roles, chars) => {
    let ret = false;
    roles.forEach(element => {
      if (element.roleName.toLowerCase().includes(chars)) {
        ret = true;
      }
    });
    return ret;
  };

  filterUsers = () => {
    let search = this.state.quickSearch.toLowerCase();
    return this.props.orgUsers.items.filter(user => {
      return (
        user.name.toLowerCase().includes(search) ||
        user.surname.toLowerCase().includes(search) ||
        user.emailAddress.toLowerCase().includes(search) ||
        this.searchRole(user.roles, search)
      );
    });
  };

  printUserRoles = roles => {
    let string = '';
    roles.forEach(role => {
      string = string + `${role.roleName}, `;
    });
    return string.slice(0, -2);
  };
  render() {
    const { dictionary } = this.props;
    const selectedOrg = this.props.organizationsList.find(x => x.id === this.state.selectedOrg);
    const numberOfPeople =
      this.props.orgUsers.totalCount !== undefined ? this.props.orgUsers.totalCount : '-';
    return (
      <div className="organization-add form">
        <div className="select-org">
          <SelectField
            //disabled={error !== null || this._isLoading()}
            style={{ width: '100%' }}
            floatingLabelText={dictionary._search_organization}
            value={selectedOrg !== undefined ? selectedOrg.id : -1}
            onChange={this.onItemTypeChange}
          >
            <MenuItem value={-1} primaryText="None" label={dictionary._drawer_label_organization} />
            {this.props.organizationsList.map((it, index) => (
              <MenuItem key={index} value={it.id} label={it.label} primaryText={it.displayName} />
            ))}
          </SelectField>
        </div>
        {selectedOrg && (
          <div className="orgTable">
            <Table selectable={false}>
              <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
                style={{ backgroundColor: 'white' }}
              >
                <TableRow>
                  <TableHeaderColumn style={STYLE.tableHeader}>
                    {dictionary._org_display_name}
                  </TableHeaderColumn>
                  <TableHeaderColumn style={STYLE.tableHeader}>{dictionary._aoi}</TableHeaderColumn>
                  <TableHeaderColumn style={STYLE.tableHeader}>
                    {dictionary._type}
                  </TableHeaderColumn>
                  <TableHeaderColumn style={STYLE.tableHeader}>
                    {dictionary._people}
                  </TableHeaderColumn>
                  <TableHeaderColumn style={STYLE.tableHeader}>
                    {dictionary._responsible}
                  </TableHeaderColumn>
                  <TableHeaderColumn style={STYLE.tableHeader}>
                    {dictionary._creation_date}
                  </TableHeaderColumn>
                  <TableHeaderColumn style={STYLE.tableHeader}>
                    {dictionary._son_of}
                  </TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                <TableRow>
                  <TableRowColumn style={{ whiteSpace: 'normal' }}>
                    {selectedOrg.displayName}
                  </TableRowColumn>
                  <TableRowColumn>{selectedOrg.areaOfInterest}</TableRowColumn>
                  <TableRowColumn>{selectedOrg.organizationType}</TableRowColumn>
                  <TableRowColumn>{numberOfPeople}</TableRowColumn>
                  <TableRowColumn>
                    {selectedOrg.responsible !== undefined ? selectedOrg.responsible : '-'}
                  </TableRowColumn>
                  <TableRowColumn>{getSimplifiedDateTime(selectedOrg.creationTime)}</TableRowColumn>
                  <TableRowColumn>
                    {selectedOrg.parentId ? this._printName(selectedOrg.parentId) : '-'}
                  </TableRowColumn>
                </TableRow>
              </TableBody>
            </Table>
          </div>
        )}
        {this.props.orgUsers.totalCount !== undefined && (
          <div className="orgUsersContainer">
            <div className="orgUsersTitle">{dictionary._registered_professionals}</div>
            <div className="orgUsersSummary">
              <div>
                {dictionary._total}: {this.props.orgUsers.totalCount}
              </div>
              <TextField
                style={{ maxWidth: 150 }}
                floatingLabelText={dictionary._quick_search}
                id="DisplayName"
                value={this.state.quickSearch}
                fullWidth={true}
                onChange={this.handleTextFieldChange}
              />
            </div>
            <div>
              <Table selectable={false} bodyStyle={{ height: '30vh' }}>
                <TableHeader
                  displaySelectAll={false}
                  adjustForCheckbox={false}
                  style={{ backgroundColor: 'white' }}
                >
                  <TableRow>
                    <TableHeaderColumn style={STYLE.tableHeader}>
                      {dictionary._name}
                    </TableHeaderColumn>
                    <TableHeaderColumn style={STYLE.tableHeader}>
                      {dictionary._surname}
                    </TableHeaderColumn>
                    <TableHeaderColumn style={STYLE.tableHeader}>
                      {dictionary._username}
                    </TableHeaderColumn>
                    <TableHeaderColumn style={STYLE.tableHeader}>
                      {dictionary._email}
                    </TableHeaderColumn>
                    <TableHeaderColumn style={STYLE.tableHeader}>
                      {dictionary._role}
                    </TableHeaderColumn>
                    <TableHeaderColumn style={STYLE.tableHeader}>
                      {dictionary._creation_date}
                    </TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                  {this.filterUsers().map((user, idx) => {
                    return (
                      <TableRow key={idx}>
                        <TableRowColumn style={{ whiteSpace: 'normal' }}>
                          {user.name}
                        </TableRowColumn>
                        <TableRowColumn>{user.surname}</TableRowColumn>
                        <TableRowColumn>{user.userName}</TableRowColumn>
                        <TableRowColumn>{user.emailAddress}</TableRowColumn>
                        <TableRowColumn>{this.printUserRoles(user.roles)}</TableRowColumn>
                        <TableRowColumn>{getSimplifiedDateTime(user.creationTime)}</TableRowColumn>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default withDictionary(ShowOrganization);
