import './style.scss';
import React, { Component } from 'react';
import { Card, CardText, CardTitle, CardActions, TextField } from 'material-ui';
import { withTaskCreate } from 'js/modules/taskCreate';
import { withMissionCreate } from 'js/modules/missionCreate';
import { logWarning } from 'js/utils/log';
import { withLeftDrawer } from 'js/modules/LeftDrawer';
import { withAreaOfInterest } from 'js/modules/AreaOfInterest';
import { withMessages } from 'js/modules/ui';
import { CloseIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import wellknown from 'wellknown';
import styled from 'styled-components';
import { getCompleteDateTime } from 'js/utils/getCompleteDateTime';
import { compose } from 'redux';
import ReactDOM from 'react-dom';
import {
  TimeWindowTask,
  AgentsNeeded,
  CardActionButtons,
  TaskCardTitle,
  TaskTools
} from 'js/components/TaskCreateDialog/commons';
import { point } from '@turf/helpers';
const enhance = compose(
  withTaskCreate,
  withLeftDrawer,
  withAreaOfInterest,
  withMissionCreate,
  withMessages
);

const bodyLineStyle = {
  display: 'flex',
  alignItems: 'center',
  width: '100%'
};

const SelectionPopupContainer = styled.div`
  width: 500px;
  height: 400px;
`;

const { Popup } = mapboxgl;

class TaskCreateDialog extends Component {
  mapPopup = new Popup({
    closeButton: false
  });
  element = null;

  constructor(props) {
    super(props);
    this.element = document.createElement('div');
  }

  state = {
    description: '',
    taskTools: ''
  };

  componentDidUpdate(prevProps) {
    if (!this.props.mapView) {
      return; // no mapview no party
    }

    if (
      this.props.isTaskPopupOpen &&
      !prevProps.isTaskPopupOpen &&
      this.props.drawTask &&
      this.props.drawTask.coordinates
    )
      this._addPopup(this.props.mapView, this.props.drawTask);
    else if (prevProps.isTaskPopupOpen && !this.props.isTaskPopupOpen) this._removePopup();
  }

  _addPopup = (mapView, feature) => {
    if (
      !feature ||
      (!Array.isArray(feature.coordinates) && !Array.isArray(feature.geometry.coordinates))
    ) {
      logWarning('Invalid feature', feature);
      return;
    }

    const map = mapView.getMap();
    const coordinates = feature.coordinates ? feature.coordinates : feature.geometry.coordinates;
    this.mapPopup
      .setDOMContent(this.element)
      .setLngLat(coordinates)
      .addTo(map);
  };

  _removePopup = () => {
    this.mapPopup.remove();
  };

  _onCloseClick = () => {
    const mapDraw = this.props.getMapDraw();
    if (this.props.drawTask.id) {
      if (mapDraw) mapDraw.delete(this.props.drawTask.id);
    } else {
      if (mapDraw) mapDraw.deleteAll();
    }

    this._clearTask();
    this.props.toggleTaskPopup(false);
  };

  _addTaskToMission = () => {
    if (this.props.newTask.description.length === 0) {
      this.props.pushError('Description of the task mandatory');
      return;
    }

    if (this.props.newTask.taskTools && this.props.newTask.taskTools.length > 60) {
      this.props.pushError('Tools field too long');
      return;
    }

    if (this.props.newTask.numberOfNecessaryPeople === 0) {
      this.props.pushError('Set the number of Agents needed');
      return;
    }

    if (!this.props.newTask.startDate || this.props.newTask.startDate == null) {
      this.props.pushError('Start Date mandatory');
      return;
    } else
      this.props.newTask.start = getCompleteDateTime(
        this.props.newTask.startDate,
        this.props.newTask.startTime,
        this.props.locale
      );

    if (!this.props.newTask.endDate || this.props.newTask.endDate == null) {
      this.props.pushError('End Date mandatory');
      return;
    } else
      this.props.newTask.end = getCompleteDateTime(
        this.props.newTask.endDate,
        this.props.newTask.endTime,
        this.props.locale
      );

    if (!this.props.newTask.start.isBefore(this.props.newTask.end, 'second')) {
      this.props.pushError('Start Date must be before the End Date');
      return;
    }

    this.props.newTask.address =
      this.props.drawTask && this.props.drawTask.coordinates
        ? '[ ' +
          Math.round(this.props.drawTask.coordinates[0] * 10000) / 10000 +
          ', ' +
          Math.round(this.props.drawTask.coordinates[1] * 10000) / 10000 +
          ' ]'
        : '';
    this.props.newTask.location = wellknown.stringify(point(this.props.drawTask.coordinates));
    this.props.newTask.drawTask = {
      coordinates: this.props.drawTask.coordinates,
      id: this.props.drawTask.id
    };

    this.props.AddTaskToMission(this.props.newTask);
    this._clearTask();
    this.props.setDrawTask({});
    setTimeout(() => this.props.toggleTaskPopup(false), 0);
  };

  _clearTask = () => {
    this.props.resetTask();
    this._resetDescription();
    this._resetTaskTools();
  };

  _handleTaskDescriptionChange = event => {
    this.props.setTaskDescription(this.state.description);
  };

  _updateDescriptionState = event => {
    this.setState({ description: event.currentTarget.value });
  };

  _handleAgentsNeededChange = event => {
    this.props.setTaskAgentsNeeded(event.currentTarget.value);
  };

  _updateTaskToolsState = event => {
    this.setState({ taskTools: event.currentTarget.value });
  };

  _handleTaskToolsChange = event => {
    this.props.setTaskTools(this.state.taskTools);
  };

  //#region TimeWindow
  _handleStartChange = (event, startDate) => {
    this.props.setTaskStartDate(startDate);
  };

  _handleStartTimeChange = (event, startTime) => {
    this.props.setTaskStartTime(startTime);
  };

  _handleEndChange = (event, endDate) => {
    this.props.setTaskEndDate(endDate);
  };

  _handleEndTimeChange = (event, endTime) => {
    this.props.setTaskEndTime(endTime);
  };
  //#endregion

  //#region Reset Region
  _resetDescription = () => {
    this.setState({ description: '' });
  };

  _resetTaskTools = () => {
    this.setState({ taskTools: '' });
  };

  _resetStart = () => {
    this.props.resetTaskStart();
  };

  _resetEnd = () => {
    this.props.resetTaskEnd();
  };

  //#endregion

  render() {
    const { dictionary, locale, newTask } = this.props;
    const cardTitle = <TaskCardTitle dictionary={dictionary} onCloseClick={this._onCloseClick} />;

    const timeWindowStep = (
      <TimeWindowTask
        startDate={newTask.startDate}
        startTime={newTask.startTime}
        endDate={newTask.endDate}
        endTime={newTask.endTime}
        dictionary={dictionary}
        locale={locale}
        handleStartChange={this._handleStartChange}
        handleStartTimeChange={this._handleStartTimeChange}
        handleEndChange={this._handleEndChange}
        handleEndTimeChange={this._handleEndTimeChange}
        resetStart={this._resetStart}
        resetEnd={this._resetEnd}
      />
    );

    const agentsNeeded = (
      <AgentsNeeded
        dictionary={dictionary}
        numberOfNecessaryPeople={newTask.numberOfNecessaryPeople}
        handleAgentsNeededChange={this._handleAgentsNeededChange}
      />
    );

    const cardActions = (
      <CardActionButtons
        onClearClick={this._clearTask}
        onSaveClick={() => this._addTaskToMission()}
        dictionary={dictionary}
      />
    );

    const descriptionField = (
      <div style={bodyLineStyle}>
        <TextField
          style={{ width: '82%', marginRight: 35 }}
          name="description"
          onChange={this._updateDescriptionState}
          onBlur={this._handleTaskDescriptionChange}
          value={this.state.description}
          multiLine={false}
        />
        <CloseIcon
          iconStyle={{ fontSize: '10px', color: 'lightgrey' }}
          onClick={e => this._resetDescription()}
        />
      </div>
    );

    const taskTools = (
      <TaskTools
        dictionary={dictionary}
        taskTools={this.state.taskTools}
        handleTaskToolsChange={this._handleTaskToolsChange}
        updateTaskToolsState={this._updateTaskToolsState}
      />
    );

    let Container = SelectionPopupContainer;

    return ReactDOM.createPortal(
      <Container>
        <Card style={{ minHeight: '100%' }}>
          <CardTitle
            titleStyle={{ fontSize: 15 }}
            style={{ paddingBottom: 0, paddingTop: 8 }}
            title={cardTitle}
          />
          <hr className="horizontalLineStyle" />
          <CardText style={{ display: 'flex', padding: 0, minHeight: 300 }}>
            <div className="task-detail-body">
              {descriptionField}
              {timeWindowStep}
              {agentsNeeded}
              {taskTools}
            </div>
          </CardText>
          <hr className="horizontalLineStyle" />
          <CardActions children={cardActions} />
        </Card>
      </Container>,
      this.element
    );
  }
}
export default enhance(TaskCreateDialog);
