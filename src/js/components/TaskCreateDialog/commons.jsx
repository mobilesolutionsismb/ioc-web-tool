import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { TimePicker, DatePicker, TextField, FlatButton } from 'material-ui';
import moment from 'moment';
import {
  ScheduleIcon,
  PersonIcon,
  CloseIcon,
  BuildIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';

export const TimeWindowTask = muiThemeable()(
  ({
    dictionary,
    startDate,
    startTime,
    endDate,
    endTime,
    handleStartChange,
    handleStartTimeChange,
    handleEndChange,
    handleEndTimeChange,
    resetStart,
    resetEnd
  }) => (
    <div style={{ width: '100%' }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
          alignItems: 'center',
          paddingTop: '20px'
        }}
      >
        <ScheduleIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
        />
        <div style={{ width: '25%' }}>{dictionary._start + '*'}</div>
        <DatePicker
          value={startDate ? startDate : moment()}
          id="startDate"
          textFieldStyle={{ width: '100px' }}
          style={{ flexGrow: 1 }}
          onChange={handleStartChange}
        />
        <TimePicker
          value={startTime ? startTime : moment()}
          id="startTime"
          textFieldStyle={{ width: '100px' }}
          style={{ flexGrow: 1 }}
          onChange={handleStartTimeChange}
        />
        <CloseIcon iconStyle={{ fontSize: '10px', color: 'lightgrey' }} onClick={resetStart} />
      </div>
      <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
        <div style={{ width: '25%', paddingLeft: 24 }}>{dictionary._end + '*'}</div>
        <DatePicker
          value={endDate ? endDate : moment()}
          id="endDate"
          textFieldStyle={{ width: '100px' }}
          style={{ flexGrow: 1 }}
          onChange={handleEndChange}
        />
        <TimePicker
          value={endTime ? endTime : moment()}
          id="endTime"
          textFieldStyle={{ width: '100px' }}
          style={{ flexGrow: 1 }}
          onChange={handleEndTimeChange}
        />
        <CloseIcon iconStyle={{ fontSize: '10px', color: 'lightgrey' }} onClick={resetEnd} />
      </div>
    </div>
  )
);

export const AgentsNeeded = muiThemeable()(
  ({ dictionary, numberOfNecessaryPeople, handleAgentsNeededChange }) => (
    <div
      style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: '20px'
      }}
    >
      <PersonIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
      />
      <div style={{ width: '25%' }}>{dictionary._agents_needed}</div>
      <TextField
        style={{ width: '20%' }}
        id="number"
        label="Number"
        value={numberOfNecessaryPeople}
        onChange={handleAgentsNeededChange}
        type="number"
        min="0"
        margin="normal"
      />
    </div>
  )
);

export const TaskTools = muiThemeable()(
  ({ dictionary, taskTools, handleTaskToolsChange, updateTaskToolsState }) => (
    <div
      style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: '20px'
      }}
    >
      <BuildIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
      />
      <div style={{ width: '25%' }}>{dictionary._task_tools}</div>
      <TextField
        id="taskTools"
        label="Task tools"
        value={taskTools}
        onChange={updateTaskToolsState}
        onBlur={handleTaskToolsChange}
        type="text"
        margin="normal"
      />
    </div>
  )
);

export const CardActionButtons = muiThemeable()(({ dictionary, onClearClick, onSaveClick }) => (
  <div style={{ display: 'flex', position: 'absolute', right: 20 }}>
    <FlatButton label={dictionary._clear} onClick={onClearClick} />
    <FlatButton label={dictionary._add} onClick={onSaveClick} />
  </div>
));

export const TaskCardTitle = muiThemeable()(({ dictionary, onCloseClick }) => (
  <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
    <span>TASK</span>
    <CloseIcon
      style={{ width: 24, height: 24, padding: 0 }}
      iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
      onClick={onCloseClick}
    />
  </div>
));
