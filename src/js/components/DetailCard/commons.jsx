import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { CloseIcon, ShareIcon, FullScreenIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { IconButton, FontIcon } from 'material-ui';

export const ReportDetailButtons = muiThemeable()(
  ({ onCloseClick, onShareCLick, onFullScreenClick }) => (
    <div style={{ position: 'absolute', right: 5, top: 5, zIndex: 1 }}>
      {onFullScreenClick && (
        <FullScreenIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
          onClick={onFullScreenClick}
        />
      )}
      {onShareCLick && (
        <ShareIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
        />
      )}
      {onCloseClick && (
        <CloseIcon
          style={{ width: 24, height: 24, padding: 0 }}
          iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
          onClick={onCloseClick}
        />
      )}
    </div>
  )
);

export const ChangeReportStatusButtons = muiThemeable()(
  ({
    dictionary,
    muiTheme,
    isValidated,
    onValidateClick,
    onRejectClick,
    status,
    iconStyle,
    isDisabled
  }) => (
    <div>
      {!isValidated ? (
        <div
          style={{
            width: '40%',
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center',
            fontSize: 11,
            position: 'absolute',
            bottom: 0,
            right: 5
          }}
        >
          <span>{dictionary._validate_report}: </span>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'center',
              marginBottom: 5
            }}
          >
            <IconButton
              className="menu-button"
              onClick={onRejectClick}
              disabled={isDisabled}
              style={{
                marginRight: 5,
                padding: 5,
                backgroundColor: muiTheme.palette.rejectedColor,
                overflow: 'hidden',
                borderRadius: '100%',
                fontSize: 36,
                width: 36,
                height: 36,
                margin: 0,
                lineHeight: '28px'
              }}
              iconStyle={{ fontSize: 24, ...iconStyle }}
              touch={true}
              tooltipStyles={{ top: 0 }}
              tooltipPosition="top-left"
            >
              <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
                clear
              </FontIcon>
            </IconButton>
            <IconButton
              className="menu-button"
              disabled={isDisabled}
              style={{
                padding: 5,
                backgroundColor: muiTheme.palette.validatedColor,
                overflow: 'hidden',
                borderRadius: '100%',
                fontSize: 36,
                width: 36,
                height: 36,
                margin: 0,
                lineHeight: '28px'
              }}
              iconStyle={{ fontSize: 24, ...iconStyle }}
              touch={true}
              onClick={onValidateClick}
              tooltipStyles={{ top: 0 }}
              tooltipPosition="top-left"
            >
              <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
                done
              </FontIcon>
            </IconButton>
          </div>
        </div>
      ) : status !== null ? (
        status === 'validated' ? (
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-around',
              alignItems: 'center',
              fontSize: 11,
              position: 'absolute',
              bottom: 5,
              right: 5
            }}
          >
            <IconButton
              className="menu-button"
              disabled={isDisabled}
              style={{
                marginRight: 5,
                padding: 5,
                backgroundColor: muiTheme.palette.validatedColor,
                overflow: 'hidden',
                borderRadius: '100%',
                fontSize: 36,
                width: 36,
                height: 36,
                margin: 0,
                lineHeight: '28px'
              }}
              iconStyle={{ fontSize: 24, ...iconStyle }}
              touch={true}
              tooltipStyles={{ top: 0 }}
              tooltipPosition="top-left"
            >
              <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
                done
              </FontIcon>
            </IconButton>
          </div>
        ) : status === 'inappropriate' ? (
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-around',
              alignItems: 'center',
              fontSize: 11,
              position: 'absolute',
              bottom: 5,
              right: 5
            }}
          >
            <IconButton
              className="menu-button"
              disabled={isDisabled}
              style={{
                marginRight: 5,
                padding: 5,
                backgroundColor: muiTheme.palette.rejectedColor,
                overflow: 'hidden',
                borderRadius: '100%',
                fontSize: 36,
                width: 36,
                height: 36,
                margin: 0,
                lineHeight: '28px'
              }}
              iconStyle={{ fontSize: 24, ...iconStyle }}
              touch={true}
              tooltipStyles={{ top: 0 }}
              tooltipPosition="top-left"
            >
              <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
                clear
              </FontIcon>
            </IconButton>
          </div>
        ) : status === 'inaccurate' ? (
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-around',
              alignItems: 'center',
              fontSize: 11,
              position: 'absolute',
              bottom: 5,
              right: 5
            }}
          >
            <IconButton
              className="menu-button"
              disabled={isDisabled}
              style={{
                marginRight: 5,
                padding: 5,
                backgroundColor: muiTheme.palette.inaccurateColor,
                overflow: 'hidden',
                borderRadius: '100%',
                fontSize: 36,
                width: 36,
                height: 36,
                margin: 0,
                lineHeight: '28px'
              }}
              iconStyle={{ fontSize: 24, ...iconStyle }}
              touch={true}
            >
              <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
                clear
              </FontIcon>
            </IconButton>
          </div>
        ) : (
          <div />
        )
      ) : (
        <div />
      )}
    </div>
  )
);

export const ReportDetailBoxedLine = muiThemeable()(
  ({ customStyle, leftIcon, secondElement, thirdElement, content, lineWidth, lineHeight }) => (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        borderStyle: 'ridge',
        borderColor: 'grey',
        borderWidth: 1,
        width: lineWidth,
        minHeight: 30,
        padding: 0,
        borderRight: 'none',
        ...customStyle
      }}
    >
      <span style={{ paddingLeft: 10, paddingRight: 20 }}>
        {isNaN(leftIcon) ? (
          <FontIcon className="ireact-icons" style={{ fontSize: 18 }}>
            {leftIcon}
          </FontIcon>
        ) : (
          <span style={{ padding: 5 }}>{leftIcon}</span>
        )}
      </span>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          width: '100%',
          height: '100%'
        }}
      >
        <span>{secondElement}</span>
        <span style={{ paddingRight: 20 }}>{thirdElement}</span>
      </div>
    </div>
  )
);
