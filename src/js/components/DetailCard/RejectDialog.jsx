import React from 'react';
import { Dialog, RadioButtonGroup, RadioButton, RaisedButton } from 'material-ui';
import './style.scss';

class RejectDialog extends React.Component {
  state = {
    motivation: ''
  };

  setMotivation = (event, motivation) => {
    this.setState({
      motivation
    });
  };

  handleClose = () => {
    this.props.closeRejectDialog(null);
  };

  submit = () => {
    this.props.closeRejectDialog(this.state.motivation);
  };

  render() {
    const dictionary = this.props.dictionary;
    return (
      <Dialog open={this.props.rejectDialogVisible} onClose={this.handleClose}>
        <div className="reject-dialog-body">
          <div style={{ marginBottom: 10 }}>
            <span>{dictionary._select_reason}</span>
          </div>
          <div>
            <RadioButtonGroup
              name="reportRejection"
              valueSelected={this.state.motivation}
              onChange={this.setMotivation}
            >
              <RadioButton value="inaccurate" label={dictionary._is_status_inaccurate} />
              <RadioButton value="inappropriate" label={dictionary._is_status_inappropriate} />
            </RadioButtonGroup>
          </div>
        </div>
        <div className="reject-dialog-footer">
          <RaisedButton onClick={this.handleClose} label={dictionary._cancel} />
          <RaisedButton
            style={{ marginLeft: 10 }}
            onClick={this.submit}
            label={dictionary._save}
            primary={true}
            disabled={!this.state.motivation}
          />
        </div>
      </Dialog>
    );
  }
}

export default RejectDialog;
