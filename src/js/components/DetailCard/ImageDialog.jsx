import React from 'react';
import Dialog from 'material-ui/Dialog';
// import { CloseIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import './style.scss';
class ImageDialog extends React.Component {
  handleClose = () => {
    this.props.closeImageDialog();
  };

  render() {
    const image = (
      <div>
        <img alt="" src={this.props.imageUrl ? this.props.imageUrl : undefined} />
        {/* <CloseIcon style={{ position: 'absolute', top: 20, right: 20 }} iconStyle={{color:'black'}} onClick={ this.handleClose } /> */}
      </div>
    );

    var imagine = new Image();
    imagine.src = this.props.imageUrl ? this.props.imageUrl : undefined;

    let customContentStyle = {};
    if (imagine) {
      customContentStyle = {
        width: imagine.width + 25,
        maxWidth: 'none',
        height: imagine.height + 25
      };
    }

    return (
      <div className="report-image-dialog" id="report-image-dialog">
        <Dialog
          open={this.props.imageDialogVisible}
          children={image}
          onRequestClose={this.handleClose}
          contentStyle={customContentStyle}
          paperClassName="paperClass"
          bodyStyle={{ borderBottom: 0, borderTop: 0 }}
          autoDetectWindowHeight={true}
        />
      </div>
    );
  }
}

export default ImageDialog;
