import './style.scss';
import React, { Component } from 'react';
import { Card, CardText, CardTitle, CardMedia, RaisedButton } from 'material-ui';
import { LikesCounter } from 'js/components/LikeButtons';
import { withReports } from 'ioc-api-interface';
import { withRouter } from 'react-router';
import { ReportDetailButtons, ReportDetailBoxedLine, ChangeReportStatusButtons } from './commons';
import { localizeDate } from 'js/utils/localizeDate';
import { ReportContentStats, RoleColorDot } from 'js/components/ReportsCommons/commons';
import {
  PhoneIcon,
  LocationOnIcon,
  ScheduleIcon
} from 'js/components/app/drawer/leftIcons/LeftIcons';
import {
  RESOURCE_ICONS,
  DAMAGE_ICONS,
  PEOPLE_ICONS,
  MeasureCategoryIcon
} from 'js/components/ReportsCommons';
import { withLoader, withMessages } from 'js/modules/ui';
import { HAZARD_VALUES_BLACKLIST } from 'js/components/ReportsCommons/commons';
import { ImageDialog } from 'js/components/DetailCard';
import { RejectDialog } from 'js/components/DetailCard';
import { withLogin } from 'ioc-api-interface';
import { compose } from 'redux';
import { SmallAvatar } from 'js/components/app/SmallAvatar';
import { teal300, white } from 'material-ui/styles/colors';
const enhance = compose(
  withReports,
  withLoader,
  withMessages,
  withRouter,
  withLogin
);

class DetailCard extends Component {
  state = {
    imageDialogVisible: false,
    rejectDialogVisible: false,
    showPhoneNumber: false
  };

  toggleShowPhoneNumber = () => {
    this.setState({ showPhoneNumber: !this.state.showPhoneNumber });
  };

  closeCard = () => {
    this.props.deselectFeature();
  };

  validateReport = async () => {
    await this.props.validateReport(
      this.props.feature.properties.id,
      this.props.loadingStart,
      this.props.loadingStop
    );

    this.props.pushMessage(this.props.dictionary._report_validated_success, 'success', null);
  };

  setReportToInappropriate = async () => {
    await this.props.setInappropriateReport(
      this.props.feature.properties.id,
      this.props.loadingStart,
      this.props.loadingStop
    );

    this.props.pushMessage(this.props.dictionary._report_inappropriate_updated, 'success', null);
  };

  setReportToInaccurate = async () => {
    await this.props.setInaccurateReport(
      this.props.feature.properties.id,
      this.props.loadingStart,
      this.props.loadingStop
    );

    this.props.pushMessage(this.props.dictionary._report_inaccurate_updated, 'success', null);
  };

  openImageDialog = () => {
    setTimeout(() => {
      this.setState({
        imageDialogVisible: true
      });
    });
  };

  openRejectDialog = () => {
    setTimeout(() => {
      this.setState({
        rejectDialogVisible: true
      });
    });
  };

  _openUAVReport = id => {
    this.props.history.push(`/uavReport/${id}`);
  };

  closeImageDialog = () => {
    setTimeout(() => {
      this.setState({
        imageDialogVisible: false
      });
    });
  };

  closeRejectDialog = value => {
    if (value === 'inappropriate') this.setReportToInappropriate();
    else if (value === 'inaccurate') this.setReportToInaccurate();
    setTimeout(() => {
      this.setState({
        rejectDialogVisible: false
      });
    });
  };

  render() {
    const dict = this.props.dictionary;
    let report =
      this.props.feature && this.props.feature !== null ? this.props.feature.properties : null;
    const likeCounter =
      report === null || report.status !== 'submitted' ? (
        <div />
      ) : (
        <LikesCounter
          size="xs"
          style={{ fontSize: 10 }}
          upvotes={report.upvoteCount}
          downvotes={report.downvoteCount}
          alreadyVoted={report.alreadyVoted}
        />
      );
    const reportCategory =
      report === null ? (
        <div />
      ) : (
        <ReportContentStats reportProperties={report} hideMeasureIcons={true} />
      );

    let reporter = {};
    let proReporter = false;
    let avatarContainer = <div />;
    if (report !== null && report.user && report.user !== null) {
      reporter = report.user;
      proReporter = proReporter = report.organization !== null && report.organization !== undefined;
      avatarContainer = (
        <SmallAvatar
          avatarIndex={report.user.userPicture}
          isPro={proReporter}
          style={{ marginLeft: 8 }}
        />
      );
    }

    const locationIcon = (
      <LocationOnIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
      />
    );
    const scheduleIcon = (
      <ScheduleIcon
        style={{ width: 24, height: 24, padding: 0 }}
        iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
      />
    );

    const changeReportStatusButtons = (
      <ChangeReportStatusButtons
        dictionary={dict}
        isValidated={report !== null && report.status !== 'submitted'}
        status={report !== null ? report.status : null}
        onValidateClick={this.validateReport}
        onRejectClick={this.openRejectDialog}
        isDisabled={!this.props.user.canValidateReport}
      />
    );
    const displayOrganization =
      report != null && report.organization != null
        ? report.organization.length > 40
          ? report.organization.substring(0, 40) + '...'
          : report.organization
        : '';
    const scrollableStyle = { overflowX: 'hidden', maxHeight: 130 };
    const listOfItems =
      report !== null ? (
        report.measures.length > 0 ? (
          //measures
          report.measures.length <= 4 ? (
            report.measures.map((e, i) => (
              <ReportDetailBoxedLine
                key={i}
                leftIcon={<MeasureCategoryIcon category={e.category} color={'#FFFFFF'} />}
                secondElement={dict('_' + e.title)}
                thirdElement={e.valueLabel ? dict(e.valueLabel) : e.value + ' ' + e.measureUnit}
              />
            ))
          ) : (
            <div style={scrollableStyle}>
              {report.measures.map((e, i) => (
                <ReportDetailBoxedLine
                  key={i}
                  leftIcon={<MeasureCategoryIcon category={e.category} color={'#FFFFFF'} />}
                  secondElement={dict('_' + e.title)}
                  thirdElement={e.valueLabel ? e.valueLabel : e.value + ' ' + e.measureUnit}
                />
              ))}
            </div>
          )
        ) : report.damages.length > 0 ? (
          //damages
          report.damages.length <= 4 ? (
            report.damages.map((e, i) => (
              <ReportDetailBoxedLine
                key={i}
                leftIcon={DAMAGE_ICONS[e.label.replace('_', '')]}
                secondElement={dict(e.label)}
                thirdElement={e.degree}
              />
            ))
          ) : (
            <div style={scrollableStyle}>
              {report.damages.map((e, i) => (
                <ReportDetailBoxedLine
                  key={i}
                  leftIcon={DAMAGE_ICONS[e.label.replace('_', '')]}
                  secondElement={dict(e.label)}
                  thirdElement={e.degree}
                />
              ))}
            </div>
          )
        ) : report.resources.length > 0 ? (
          //resources
          report.resources.length <= 4 ? (
            report.resources.map((e, i) => (
              <ReportDetailBoxedLine
                key={i}
                leftIcon={RESOURCE_ICONS[e.label.replace('_', '')]}
                secondElement={dict(e.label)}
              />
            ))
          ) : (
            <div style={scrollableStyle}>
              {report.resources.map((e, i) => (
                <ReportDetailBoxedLine
                  key={i}
                  leftIcon={RESOURCE_ICONS[e.label.replace('_', '')]}
                  secondElement={dict(e.label)}
                />
              ))}
            </div>
          )
        ) : report.people.length > 0 ? (
          //People
          report.people.length <= 4 ? (
            report.people.map((e, i) => (
              <ReportDetailBoxedLine
                key={i}
                leftIcon={PEOPLE_ICONS[e.label]}
                secondElement={dict(e.label)}
                thirdElement={e.quantity}
                l
              />
            ))
          ) : (
            <div style={scrollableStyle}>
              {report.people.map((e, i) => (
                <ReportDetailBoxedLine
                  key={i}
                  leftIcon={PEOPLE_ICONS[e.label]}
                  secondElement={dict(e.label)}
                  thirdElement={e.quantity}
                />
              ))}
            </div>
          )
        ) : null
      ) : null;

    const lastRowSecondColumn = (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <RoleColorDot
          style={{ marginTop: 'inherit' }}
          userRole={proReporter ? 'authority' : 'pro'}
        />
        <div style={{ fontSize: 12, paddingLeft: 5 }}>
          <div>
            {proReporter
              ? `${displayOrganization}, ${reporter.employeeId}`
              : `${reporter.level || 'Novice'}, ${reporter.score}`}
          </div>
          <div>
            {this.state.showPhoneNumber ? `${dict._phoneNumber}: ${reporter.phoneNumber}` : ''}
          </div>
        </div>
        {/* <span style={{ fontSize: 12, paddingLeft: 5 }}>{proReporter ? `${report.organization}, ${reporter.employeeId}` : `${reporter.nickname}, ${reporter.level || 'Novice'}, ${reporter.score}`}</span> */}
      </div>
    );

    const overlayStyle = {
      background: 'red',
      height: 20,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    };

    const hazardMustBeShown =
      report !== null && HAZARD_VALUES_BLACKLIST.indexOf(report.hazard) === -1;

    return (
      <div className="detail-popup" style={{ padding: 0 }}>
        {report === null ? (
          <div />
        ) : (
          <Card>
            <CardMedia
              overlay={
                <CardTitle
                  title={hazardMustBeShown ? this.props.dictionary(`_${report.hazard}`) : ''}
                  titleStyle={{ fontSize: 15, textTransform: 'uppercase' }}
                />
              }
              overlayContentStyle={hazardMustBeShown ? overlayStyle : { visibility: 'hidden' }}
              style={{ paddingBottom: 0 }}
            >
              <div
                className="cardMedia"
                style={{
                  backgroundImage: 'url(' + report.picture + ')',
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'contain',
                  backgroundPosition: 'center',
                  height: 300,
                  maxHeight: 300,
                  display: 'flex'
                }}
              >
                <img
                  alt=""
                  className="detail-img"
                  src={report.picture}
                  style={{ visibility: 'hidden' }}
                />
                <ReportDetailButtons
                  onCloseClick={this.closeCard}
                  onFullScreenClick={this.openImageDialog}
                />
              </div>
            </CardMedia>
            <hr className="horizontalLineStyle" />
            <CardText style={{ display: 'flex', padding: 0, minHeight: 200 }}>
              <div style={{ width: '60%' }}>
                <ReportDetailBoxedLine
                  customStyle={{ borderTop: 'none' }}
                  secondElement={reportCategory}
                  thirdElement={likeCounter}
                />
                {listOfItems}
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    position: 'absolute',
                    bottom: 0,
                    width: '60%',
                    justifyContent: 'space-between'
                  }}
                >
                  {avatarContainer}
                  {lastRowSecondColumn}
                  <PhoneIcon
                    iconStyle={{ color: this.state.showPhoneNumber ? teal300 : white }}
                    onClick={this.toggleShowPhoneNumber}
                  />
                </div>
                {/* <ReportDetailBoxedLine customStyle={{ borderTop: 'none', borderBottom: 'none', position: 'absolute', bottom: 5 }} leftIcon={proReporter ? <div /> : avatarContainer} secondElement={lastRowSecondColumn} thirdElement={phoneIcon} /> */}
              </div>
              <div className="verticalLine" />
              <div style={{ width: '40%', minHeight: 140 }}>
                <div className="reportDetailSecondColumnInfo">
                  <span>{locationIcon}</span>
                  <span>{report.address}</span>
                </div>
                <div className="reportDetailSecondColumnInfo">
                  <span>{scheduleIcon}</span>
                  <span>{localizeDate(report.userCreationTime, this.props.locale, 'L LT')}</span>
                </div>
                {report.type === 'misc' && (
                  <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <RaisedButton
                      label="See details"
                      primary={true}
                      onClick={() => this._openUAVReport(report.id)}
                    />
                  </div>
                )}
                <div>{changeReportStatusButtons}</div>
              </div>
            </CardText>
          </Card>
        )}
        <ImageDialog
          imageDialogVisible={this.state.imageDialogVisible}
          imageUrl={report ? report.picture : undefined}
          closeImageDialog={this.closeImageDialog}
        />
        <RejectDialog
          rejectDialogVisible={this.state.rejectDialogVisible}
          closeRejectDialog={this.closeRejectDialog}
          dictionary={dict}
        />
      </div>
    );
  }
}
export default enhance(DetailCard);
