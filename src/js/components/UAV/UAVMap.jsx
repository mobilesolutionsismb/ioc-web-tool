import React, { Component } from 'react';
import { compose } from 'redux';

import { point } from '@turf/helpers';
import { MapView } from 'js/components/MapView';
import { onMapLoad } from './mapInitialization';
import { withGeolocation } from 'ioc-geolocation';
import { withMapPreferences } from 'js/modules/preferences';
import { withTopBarDate } from 'js/modules/topBarDate';
import { withDictionary } from 'ioc-localization';
import { withLoader } from 'js/modules/ui';
import { withAPISettings } from 'ioc-api-interface';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';

const enhance = compose(
  withAPISettings,
  withMapPreferences,
  withGeolocation,
  withTopBarDate,
  withDictionary,
  withLoader
);

const MAX_ZOOM = 16;

class UAVMap extends Component {
  mapView = null;

  state = {
    dialogVisible: false
  };

  componentDidUpdate(prevProps) {
    if (!areFeaturesEqual(prevProps.report, this.props.report)) {
      if (this.mapView && this.props.report) {
        const map = this.mapView.getMap();
        const reportPosition = map.getSource('reportPosition');
        if (reportPosition) {
          reportPosition.setData(point(this.props.report.geometry.coordinates));
        }
      }
    }
  }

  render() {
    if (!this.props.report || this.props.report.properties.type !== 'misc') {
      return <div>No UAV Report to be displayed here</div>;
    } else {
      const mapSettings = {
        style: this.props.preferences.mapStyle,
        center: this.props.report.geometry.coordinates,
        zoom: MAX_ZOOM,
        pitch: 0,
        bearing: 0,
        interactive: false,
        minZoom: MAX_ZOOM,
        maxZoom: MAX_ZOOM
      };
      return (
        <div style={{ height: '100%', position: 'relative' }}>
          <MapView
            ref={mapView => (this.mapView = mapView)}
            mapSettings={mapSettings}
            reportFeature={this.props.report}
            onMapLoad={onMapLoad}
            onMapHover={() => {}}
            onMapMouseLeave={() => {}}
          />
          <div
            className="position-controls"
            style={{
              position: 'absolute',
              zIndex: '99',
              bottom: 0,
              right: 0,
              marginRight: 5,
              marginBottom: 5
            }}
          />
        </div>
      );
    }
  }
}
export default enhance(UAVMap);
