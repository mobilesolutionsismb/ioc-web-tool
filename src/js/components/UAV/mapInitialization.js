import { featureCollection } from '@turf/helpers';
import { getMapSettings } from 'js/components/MapView';
import { darken } from 'material-ui/utils/colorManipulator';

import { REPORT_COLOR } from 'js/components/Home2/components/Map/mapboxStyles/featureColors';
import {
  DEFAULT_ICON_LAYOUT,
  DEFAULT_ICON_PAINT
} from 'js/components/Home2/components/Map/mapboxStyles/singleFeaturesLayers';
export function onMapLoad(event, doneCallback, hoverCallback, mouseLeaveCallback, rest) {
  const map = event.target;

  const features = rest.reportFeature ? [rest.reportFeature] : [];
  const source = 'reportPosition';
  map.addSource(source, {
    type: 'geojson',
    data: featureCollection(features)
  });

  // Layers
  map.addLayer({
    id: 'report-position',
    type: 'symbol',
    source,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-field': '\ue90a',
      'text-size': ['interpolate', ['linear'], ['zoom'], 0, 2, 4, 7, 7, 12, 24, 32],
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 2,
      'text-color': REPORT_COLOR,
      'text-halo-color': darken(REPORT_COLOR, 0.15)
    }
  });

  map.addLayer({
    id: 'report-icons',
    type: 'symbol',
    source,
    layout: DEFAULT_ICON_LAYOUT,
    paint: DEFAULT_ICON_PAINT
  });

  if (typeof doneCallback === 'function') {
    doneCallback(map, getMapSettings(map));
  }
}
