import React, { Component } from 'react';
import './styles.scss';

class RegionCard extends Component {
  state = {
    clicked: false,
    hovered: false
  };

  render() {
    const backgroundColor = this.state.clicked
      ? 'rgb(151,151,151)'
      : this.state.hovered
        ? 'rgb(110,110,110)'
        : 'rgb(77,77,77)';
    return (
      <div
        className="regionCard"
        style={{ backgroundColor: backgroundColor }}
        onClick={() => {
          if (this.state.clicked) {
            this.setState({ clicked: false });
            this.props.hideRegion(this.props.number);
          } else {
            this.setState({ clicked: true });
            this.props.showRegion(this.props.number);
          }
        }}
        onMouseOver={() => {
          this.setState({ hovered: true });
        }}
        onMouseLeave={() => {
          this.setState({ hovered: false });
        }}
      >
        <div style={{ width: '40%', height: '100%' }}>
          <div
            style={{
              width: '100%',
              height: '100%',
              backgroundImage: `url(${this.props.item.Uri})`,
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'contain',
              backgroundPosition: 'center'
            }}
          />
        </div>
        <div className="cardName">{`Region ${this.props.number + 1}`}</div>
      </div>
    );
  }
}

export default RegionCard;
