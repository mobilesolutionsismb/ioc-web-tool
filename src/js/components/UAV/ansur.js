that.previewImg = document.createElement('img');
that.previewImg.src = that.observation.preview_url;

// Wait for preview to load then call addCanvasImage:

ObservationSlidein.prototype.addCanvasImage = function() {
  var img = new createjs.Bitmap(this.previewImg);
  img.x = 0;
  img.y = 0;
  this.stage.addChild(img);
  return this;
};

this.regionsContainer = new createjs.Container();
this.stage.addChild(this.regionsContainer);

// For each region

ImageLoader.load(region.url).then(function(imgElement) {
  region.imgEl = imgElement;

  that.regions.push(that.addRegion(region));
  that.regionsContainer.addChild(region.canvasImg);

  that.stage.update();
});

ObservationSlidein.prototype.addRegion = function(region) {
  // Converting region coordinates and dimensions from the originals image coordinate system to the preview image coordinate system
  // Preview scale can be found by: previewImg.naturalWidth / observation.meta.Source.width;

  regien.previewCoords = { x: region.x * previewScale, y: region.y * previewScale };
  region.previewDimensions = {
    width: region.width * previewScale,
    height: region.height * previewScale
  };

  var img = new createjs.Bitmap(region.imgEl);
  img.x = region.previewCoords.x;
  img.y = region.previewCoords.y;
  img.scaleX = this.previewScale;
  img.scaleY = this.previewScale;

  region.canvasImg = img;
  return region;
};
