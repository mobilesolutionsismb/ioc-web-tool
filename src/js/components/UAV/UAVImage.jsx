import './styles.scss';
import React, { Component } from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { GeoJSONFeaturesProvider } from 'js/components/GeoJSONFeaturesProvider';
import { UAVMap } from './';
import RegionCard from './RegionCard';
import { BackButton as BackButtonInner } from 'js/components/app/HeaderButton';
import axios from 'axios';
import 'yuki-createjs';
import 'video-react/dist/video-react.css';
import { Player, BigPlayButton } from 'video-react';
import { withRouter } from 'react-router';
import { withDictionary } from 'ioc-localization';
import { withReports, withLogin } from 'ioc-api-interface';
import { localizeDate } from 'js/utils/localizeDate';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';

const enhance = compose(
  withRouter,
  withReports,
  withLogin,
  withDictionary
);
const BackButton = withRouter(({ history }) => (
  <BackButtonInner
    onClick={() => {
      history.goBack();
    }}
  />
));
// console.warn("ImageLoader",createjs.ImageLoader);
// const previewScale = 1.0;

const createjs = window.createjs;

class UAVImage extends Component {
  state = {
    showRegions: false,
    uavData: null,
    isVideo: false,
    hovered: -1,
    notFound: false,
    drag: false,
    lastX: 0,
    lastY: 0,
    videoSelected: 0
  };
  zoom = 1.0;
  img = null;
  previewImg = null;
  canvas = null;
  videoPlayer = null;
  regionsContainer = [];
  stage = null;
  regions = [];
  video = [];
  poster = null;
  _mapcomponent = null;

  _initAfterMount = async () => {
    const uavData = await this.props.getReportExtensionData(this.props.match.params.id);

    if (uavData.length > 0) {
      const video = uavData[0].extensionData.Details.filter(o => o.FileType === 4);
      const poster = uavData[0].extensionData.Details.find(o => o.FileType === 3);
      this.reportId = parseInt(this.props.match.params.id, 10) || -1;
      if (video.length === 0) {
        this.setState({ uavData: uavData });
      } else {
        this.poster = poster ? poster.Uri : undefined;
        this.video = video.map(v => v.Uri);
        this.setState({ isVideo: true, uavData: uavData, videoSelected: 0 });
      }
    } else {
      this.setState({ notFound: true });
    }
  };

  componentDidMount() {
    this._initAfterMount();
  }

  _onRegionsToggle = (e, isChecked) => {
    this.setState({ showRegions: isChecked });
  };

  _showRegions = () => {
    for (let x = 0; x < this.regionsContainer.length; x++) {
      this.regionsContainer[x].visible = true;
    }

    this.stage.update();
  };
  _showRegion = index => {
    this.regionsContainer[index].visible = true;

    this.stage.update();
  };

  _hideRegions = () => {
    for (let x = 0; x < this.regionsContainer.length; x++) {
      this.regionsContainer[x].visible = false;
    }

    this.stage.update();
  };
  _hideRegion = index => {
    this.regionsContainer[index].visible = false;

    this.stage.update();
  };

  _searchReportFromList = async () => {
    if (this.props.reportFeaturesURL) {
      let features = null;
      const response = await axios.get(this.props.reportFeaturesURL);
      features =
        response.data.type === 'FeatureCollection' ? response.data.features : response.data;
      const report = features.find(f => f.properties.id === this.reportId);
      if (!report) {
        this.props.history.replace('/home');
      }
    } else {
      this.props.history.replace('/home');
    }
  };
  _getBackgroundImage = () => {
    const data = this.state.uavData[0];
    const backgroundImage = data.extensionData.Details.find(o => o.FileType === 1);
    return backgroundImage
      ? backgroundImage
      : {
          Uri: '',
          FileType: 1,
          Created: '',
          Metadata: {
            Source: {
              camera_make: null,
              camera_model: null,
              filename: '',
              height: 1,
              size: 2,
              width: 1
            }
          },
          Hash: null
        };
  };

  _setCanvasScale = () => {
    if (this.stage.canvas) {
      let canvasBounds = this.stage.getChildAt(0).getBounds();
      let boundsRatio = canvasBounds.width / canvasBounds.height;
      let canvasRatio = this.stage.canvas.width / this.stage.canvas.height;
      let scale = 1;
      if (boundsRatio >= canvasRatio) {
        scale = this.stage.canvas.width / canvasBounds.width;
      } else if (canvasRatio > boundsRatio) {
        scale = this.stage.canvas.height / canvasBounds.height;
        this.stage.x = (this.stage.canvas.width - canvasBounds.width * scale) / 2;
      }
      this.stage.scaleX = this.stage.scaleY = scale;
      this.stage.update();
    }

    return this;
  };

  _updateStage = () => {
    this.stage.update();
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this._updateStage);
    this.canvas = null;
    this.videoPlayer = null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.showRegions !== this.state.showRegions) {
      this.state.showRegions === true ? this._showRegions() : this._hideRegions();
    }
    if (prevState.uavData === null && this.state.uavData !== null) {
      this._searchReportFromList();
    }
    if (prevState.videoSelected !== this.state.videoSelected) {
      this.videoPlayer.load();
      this.videoPlayer.play();
    }

    if (!this.state.isVideo && prevState.uavData == null && this.state.uavData != null) {
      this.canvas.width = this.canvas.parentElement.offsetWidth;
      this.canvas.height = this.canvas.parentElement.offsetHeight;

      this.previewImg = new Image();
      this.previewImg.onload = () => {
        console.log('IMG LOADED', this.previewImg.width, this.previewImg.height);
        // this.canvas.width = this.previewImg.width;
        // this.canvas.height = this.previewImg.height;
        this.stage = new createjs.Stage(this.canvas);
        this._addCanvasImage()
          ._initRegions()
          ._setCanvasScale();
      };
      // Start loading image
      const backgroundImage = this._getBackgroundImage();
      this.previewImg.src = backgroundImage.Uri;
      window.addEventListener('resize', this._updateStage);
    }
    if (this.state.notFound && this.setState.notFound !== prevState.notFound) {
      setTimeout(() => {
        this.props.history.replace('/home');
      }, 3000);
    }
  }

  _setCanvasRef = node => {
    this.canvas = node;
  };
  _setVideoRef = node => {
    this.videoPlayer = node;
  };
  playVideo = id => {
    if (this.state.videoSelected === id) {
      this.videoPlayer.play();
    } else {
      this.setState({ videoSelected: id });
    }
  };
  _addCanvasImage = () => {
    //recorrer uav
    const data = this.state.uavData[0];
    const numberOFRegions = data.extensionData.Details.filter(det => det.FileType === 2).length;

    this.img = new createjs.Bitmap(this.previewImg);
    this.img.x = 0;
    this.img.y = 0;

    this.stage.addChild(this.img);

    for (let x = 0; x < numberOFRegions; x++) {
      this.regionsContainer[x] = new createjs.Container();
      this.regionsContainer[x].visible = false;
      this.stage.addChild(this.regionsContainer[x]);
    }
    this.stage.update();
    return this;
  };

  _parseRegion(regionItem, imgEl) {
    const width = parseInt(regionItem.width, 10);
    const height = parseInt(regionItem.height, 10);
    const x = parseInt(regionItem.x, 10);
    const y = parseInt(regionItem.y, 10);
    const scale = parseInt(regionItem.scale, 10);
    const quality = parseInt(regionItem.quality, 10);
    return {
      x,
      y,
      width,
      height,
      scale,
      quality,
      imgEl
    };
  }

  _initRegions = () => {
    const data = this.state.uavData[0];
    if (data && data.extensionData) {
      const Details = data.extensionData.Details.filter(det => det.FileType !== 1);
      Details.forEach((detail, index) => {
        const imageLoader = new createjs.ImageLoader({ src: detail.Uri });
        imageLoader.on('complete', e => {
          const imgElement = e.target.getResult();
          console.log('imgElement', imgElement, detail.Uri);

          const region = this._addRegion(this._parseRegion(detail.Metadata.Region, imgElement));
          this.regions.push(region);
          this.regionsContainer[index].addChild(region.canvasImg);
          this.regionsContainer[index].addChild(region.rect);
          this.stage.update();
        });
        imageLoader.on('error', e => {
          console.error('ImageLoader error', e);
        });
        imageLoader.load();
      });
    }
    return this;
  };
  _dragImage = e => {
    e.preventDefault();
    if (this.state.drag) {
      var local = this.stage.globalToLocal(this.stage.mouseX, this.stage.mouseY);

      this.stage.regX = local.x;
      this.stage.regY = local.y;

      this.stage.x = this.stage.mouseX + (this.stage.mouseX - this.state.lastX);
      this.stage.y = this.stage.mouseY + (this.stage.mouseY - this.state.lastY);

      // make sure to redraw the stage to show the change:
      this.stage.update();
      this.setState({ lastX: this.stage.mouseX, lastY: this.stage.mouseY });
    }
  };
  makeZoom = e => {
    if (Math.max(-1, Math.min(1, e.deltaY || -e.detail)) <= 0) {
      this.zoom = 1.1;
    } else {
      this.zoom = 1 / 1.1;
    }

    var local = this.stage.globalToLocal(this.stage.mouseX, this.stage.mouseY);
    this.stage.regX = local.x;
    this.stage.regY = local.y;
    this.stage.x = this.stage.mouseX;
    this.stage.y = this.stage.mouseY;
    this.stage.scaleX =
      this.stage.scaleX > 1 || this.zoom > 1 ? (this.stage.scaleY *= this.zoom) : this.stage.scaleX;

    this.stage.update();
  };

  _addRegion = region => {
    // Converting region coordinates and dimensions from the originals image coordinate system to the preview image coordinate system
    // Preview scale can be found by: previewImg.naturalWidth / observation.meta.Source.width;
    const bgImage = this._getBackgroundImage();
    const observationScale = parseInt(bgImage.Metadata.Source.width, 10);
    const { x, y, width, height } = region;
    const previewScale = this.previewImg.naturalWidth / observationScale; //this.state.scaleFactor; // using width does not work!
    region.previewCoords = { x: x * previewScale, y: y * previewScale };
    region.previewDimensions = { width: width * previewScale, height: height * previewScale };

    const img = new createjs.Bitmap(region.imgEl);
    img.x = region.previewCoords.x;
    img.y = region.previewCoords.y;
    img.scaleX = previewScale;
    img.scaleY = previewScale;

    region.canvasImg = img;
    const rect = new createjs.Shape();
    rect.graphics
      .beginStroke('red')
      .setStrokeStyle(1, 'round')
      .setStrokeDash([10, 10], 0)
      .drawRect(
        region.previewCoords.x,
        region.previewCoords.y,
        region.previewDimensions.width,
        region.previewDimensions.height
      )
      .endStroke();
    region.rect = rect;
    console.log(
      'Region loaded',
      { x, y },
      '==>',
      region.previewCoords,
      'AND',
      { width, height },
      '==>',
      region.previewDimensions,
      'SCALE',
      previewScale
    );
    return region;
  };
  mouseUp = () => {
    this.setState({ drag: false });
  };
  mouseDown = () => {
    if (this.isClickInsideImage()) {
      this.setState({
        drag: true,
        lastX: this.stage.mouseX,
        lastY: this.stage.mouseY
      });
    }
  };
  isClickInsideImage = () => {
    if (this.stage) {
      let local = this.stage.globalToLocal(this.stage.mouseX, this.stage.mouseY);
      let width = this.stage.children[0]._rectangle.width;
      let height = this.stage.children[0]._rectangle.height;
      if (local.x < 0 || local.y < 0 || local.x > width || local.y > height) return false;
      else return true;
    } else {
      return false;
    }
  };
  render() {
    if (!this.props.loggedIn) {
      return <Redirect to="/" />;
    }
    const palette = this.props.muiTheme.palette;
    return !this.state.notFound ? (
      <GeoJSONFeaturesProvider
        style={{ height: '100%' }}
        sourceURL={this.props.reportFeaturesURL}
        itemType={'report'}
      >
        {features => {
          const report = features.find(f => f.properties.id === this.reportId);
          /*if (!report) {
                  this.props.history.replace('/home');
                }*/
          return (
            <div className="uav-main-container" style={{ height: 'calc(100vh - 64px)' }}>
              <div className="uav-image page" style={{ color: palette.liveModeDisabledColor }}>
                <header>
                  <div className="UAVheaderContainer">
                    <BackButton />
                    <h2>
                      {`${this.props.dictionary._uav_report} - `}
                      {report &&
                        localizeDate(report.properties.userCreationTime, this.props.locale, 'L LT')}
                    </h2>
                    <div />
                  </div>
                </header>
                <main>
                  {this.state.isVideo ? (
                    <div
                      style={{
                        width: '70%',
                        maxHeight: '100%',
                        display: 'flex',
                        justifyContent: 'center'
                      }}
                    >
                      <Player
                        playsInline
                        fluid={false}
                        height={500}
                        ref={this._setVideoRef}
                        poster={this.poster}
                      >
                        <BigPlayButton position="center" />
                        <source src={this.video[this.state.videoSelected]} />
                      </Player>
                    </div>
                  ) : (
                    <canvas
                      ref={this._setCanvasRef}
                      onWheel={this.makeZoom}
                      onMouseDown={this.mouseDown}
                      onMouseUp={this.mouseUp}
                      onMouseMove={this._dragImage}
                    />
                  )}
                </main>
              </div>
              <div className="rigthContainer">
                <div className="mapContainer">
                  <UAVMap report={report || null} />
                </div>
                <div className="imagesContainer">
                  {!this.state.isVideo &&
                    this.state.uavData &&
                    this.state.uavData[0].extensionData.Details.filter(o => o.FileType === 2).map(
                      (it, index) => (
                        <RegionCard
                          key={index}
                          number={index}
                          item={it}
                          showRegion={this._showRegion}
                          hideRegion={this._hideRegion}
                        />
                      )
                    )}
                  {this.state.isVideo &&
                    this.state.uavData &&
                    this.video.map((v, idx) => {
                      return (
                        <div
                          key={idx}
                          className="regionCard video"
                          style={{ backgroundColor: this.state.hovered === idx && 'gray' }}
                          onClick={() => {
                            this.playVideo(idx);
                          }}
                          onMouseOver={() => {
                            this.setState({ hovered: idx });
                          }}
                          onMouseLeave={() => {
                            this.setState({ hovered: -1 });
                          }}
                        >
                          <div style={{ width: '40%', height: '100%' }}>
                            <div
                              style={{
                                width: '100%',
                                height: '100%',
                                backgroundImage:
                                  this.poster &&
                                  `url(${
                                    this.state.uavData[0].extensionData.Details.find(
                                      o => o.FileType === 3
                                    ).Uri
                                  })`,
                                backgroundRepeat: 'no-repeat',
                                backgroundSize: 'contain',
                                backgroundPosition: 'center'
                              }}
                            />
                          </div>
                          <div className="cardName">UAV Video {idx + 1}</div>
                        </div>
                      );
                    })}
                </div>
              </div>
            </div>
          );
        }}
      </GeoJSONFeaturesProvider>
    ) : (
      <div className="UAVNotFound">
        <h1>UAV report {this.props.match.params.id} not found</h1>
      </div>
    );
  }
}

export default enhance(muiThemeable()(UAVImage));
