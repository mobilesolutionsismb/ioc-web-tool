import './style.scss';
import React, { Component } from 'react';
import { Paper } from 'material-ui';
import { DeleteIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { LocationOnIcon, EventIcon } from 'js/components/app/drawer/leftIcons/LeftIcons';
import { localizeDate } from 'js/utils/localizeDate';
import { BuildIcon } from '../app/drawer/leftIcons/LeftIcons';
const MAX_TITLE_LENGTH_CARD = 35;

const memberStyle = { display: 'flex', alignItems: 'center', fontSize: 11 };
const iconStyle = { fontSize: '18px', color: 'lightgrey' };
const fontStyle = { width: 24, height: 24, padding: 0 };

class TaskNewCard extends Component {
  _onDeleteButtonClick = e => {
    this.props.deleteSelectedTask(this.props.feature.description);
  };

  render() {
    const { feature, dictionary, locale } = this.props;
    const { description, address, taskTools } = feature;

    const truncatedDescription =
      description && description.length > MAX_TITLE_LENGTH_CARD
        ? description.substring(0, MAX_TITLE_LENGTH_CARD) + '...'
        : description;

    return (
      <Paper>
        <div className="task-new-card">
          <div className="task-new-card-title">
            {truncatedDescription}
            <DeleteIcon
              iconStyle={{ fontSize: '18px', color: 'lightgrey' }}
              tooltip={dictionary._delete_task}
              onClick={e => this._onDeleteButtonClick(e)}
            />
          </div>
          <div className="task-new-card-info">
            <div style={memberStyle}>
              <LocationOnIcon style={fontStyle} iconStyle={iconStyle} />
              {address}
            </div>
            <div style={memberStyle}>
              <EventIcon style={fontStyle} iconStyle={iconStyle} />
              <span>
                {dictionary._start}: {localizeDate(feature.startDate, locale, 'L LT', true)} -{' '}
                {dictionary._end}:{' '}
                {feature.endDate ? localizeDate(feature.endDate, locale, 'L LT', true) : ''}
              </span>
            </div>
            <div style={memberStyle}>
              <BuildIcon style={fontStyle} iconStyle={iconStyle} />
              {taskTools}
            </div>
          </div>
        </div>
      </Paper>
    );
  }
}

export default TaskNewCard;
