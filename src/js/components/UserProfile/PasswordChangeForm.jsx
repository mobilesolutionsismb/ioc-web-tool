import React from 'react';
import { FormComponent } from 'js/components/FormComponent';
import { RaisedButton, FlatButton } from 'material-ui';
import { passwordRule, confirmRule } from 'js/utils/fieldValidation';
import { LinkedButton } from 'js/components/app/LinkedButton';
import styled from 'styled-components';
import { useLocale } from 'js/hooks/use-locale.hook';

const passwordConfirmRule = confirmRule('newPassword', 'Password do not match');

const StyledForm = styled.form.attrs(() => ({ className: 'password-change-form' }))`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-evenly;
`;

const FormContentWrapper = styled.div.attrs(() => ({ className: 'password-change-form__wrapper' }))`
  width: 100%;
  height: auto;
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
  overflow-y: auto;
  overflow-x: hidden;

  & .input-wrapper {
    &:last-child {
      margin-bottom: 64px;
    }

    .form-input {
      & div:last-child {
        white-space: nowrap;
      }
    }
  }
`;

const StyledFormAction = styled.div.attrs(() => ({ className: 'password-change-form__action' }))`
  width: 100%;
  height: auto;
  flex-grow: 2;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  padding: 8px 0;
`;

const pfFormConfig = [
  {
    name: 'currentPassword',
    trim: true,
    inputType: 'password',
    label: '_cp_form_oldpassword',
    rules: [passwordRule],
    defaultValue: ''
  },
  {
    name: 'newPassword',
    trim: true,
    inputType: 'password',
    label: '_cp_form_newpassword',
    rules: [passwordRule],
    defaultValue: ''
  },
  {
    name: 'newPassword-confirm',
    trim: true,
    inputType: 'password',
    label: '_cp_form_newpassword_conf',
    rules: [passwordRule, passwordConfirmRule],
    defaultValue: ''
  }
];

const style = {
  button: {
    width: 304
  }
};

export function PasswordChangeForm({ onSubmit }) {
  const [{ dictionary }] = useLocale();
  console.log('dictionary', dictionary);

  return (
    <FormComponent
      className="password-change-form"
      config={pfFormConfig}
      dictionary={dictionary}
      onSubmit={onSubmit}
      formComponent={StyledForm}
      innerComponent={FormContentWrapper}
      actionComponent={StyledFormAction}
      actions={() => [
        <RaisedButton
          key="submit"
          type="submit"
          className="ui-button"
          primary={true}
          style={style.button}
          label={dictionary._submit}
        />,
        <FlatButton
          key="cancel"
          className="ui-button"
          secondary={true}
          disableTouchRipple={true}
          containerElement={<LinkedButton to="/" replace={true} />}
          label={dictionary._cancel}
        />
      ]}
    />
  );
}

export default PasswordChangeForm;
