import React, { Component } from 'react';
import { compose } from 'redux';
import { withMessages } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { withLogin } from 'ioc-api-interface';
import { Redirect } from 'react-router-dom';

import { ColumnPage } from 'js/components/app/commons';

import { lighten } from 'material-ui/utils/colorManipulator';
import { avatarLoader } from 'js/utils/getAssets';

import styled from 'styled-components';
import { logError } from 'js/utils/log';

const AvatarPreview = styled.div.attrs(() => ({ className: 'avatar-preview' }))`
  width: 100%;
  height: 30%;
  background-color: ${props => props.theme.palette.primary1Color};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const UserAvatar = styled.div.attrs(() => ({ className: 'user-avatar' }))`
  width: ${props => props.size || 100}px;
  height: ${props => props.size || 100}px;
  min-width: ${props => props.size || 100}px;
  min-height: ${props => props.size || 100}px;
  background-image: ${props => (props.src ? `url(${props.src})` : '')};
  background-position: center top;
  background-size: 85%;
  background-repeat: no-repeat;
  background-color: rgba(0, 0, 0, 0.25);
  border: 4px solid ${props => (props.selected ? props.theme.palette.accent1Color : 'transparent')};
  box-sizing: border-box;
  border-radius: 100%;
`;

const AvatarChoice = styled.div.attrs(() => ({ className: 'avatar-choice' }))`
  width: 100%;
  height: 70%;
  overflow-x: hidden;
  overflow-y: auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  align-content: space-evenly;

  .avatar-list-item {
    width: ${props => props.size || ((window.innerWidth - 6) / 3) * 0.85}px;
    height: ${props => props.size || ((window.innerWidth - 6) / 3) * 0.85}px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    border: 1px solid transparent;
    box-sizing: border-box;
    border-radius: 100%;
    padding: 0;
    font-size: 16px;
    text-transform: uppercase;
    cursor: pointer;
    color: ${props => props.theme.palette.textColor};
    background-color: ${props => props.theme.palette.canvasColor};
    outline: none;
    -webkit-appearance: none;
    /* margin: 16px 32px; */

    &.selected.button {
      position: relative;
      &::before {
        position: absolute;
        content: 'checkmark';
        font-family: Material Icons;
        top: 0;
        left: 25%;
        color: ${props => props.theme.palette.accent1Color};
        line-height: ${props => props.size || ((window.innerWidth - 6) / 3) * 0.85}px;
        font-size: ${props => (props.size || ((window.innerWidth - 6) / 3) * 0.85) * 0.5}px;
        font-weight: normal;
        font-style: normal;
        text-transform: none;
        -webkit-font-smoothing: antialiased;
        text-rendering: optimizeLegibility;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-feature-settings: 'liga';
        font-feature-settings: 'liga';
        text-align: center;
        font-weight: bold;
        z-index: 1;
      }

      &::after {
        position: absolute;
        content: '';
        top: 0;
        left: 0;
        color: ${props => props.theme.palette.accent1Color};
        background-color: rgba(255, 255, 255, 0.3);
        border-radius: 100%;
        width: ${props => props.size || ((window.innerWidth - 6) / 3) * 0.85}px;
        height: ${props => props.size || ((window.innerWidth - 6) / 3) * 0.85}px;
        box-sizing: border-box;
        border: 4px solid;
      }
    }

    &.ripple {
      background-position: center;
      transition: background 0.8s;
    }
    &.ripple:hover {
      background: ${props => {
        const color = lighten(props.theme.palette.canvasColor, 0.2);
        return `${color} radial-gradient(circle, transparent 1%, ${color} 1%) center/15000%`;
      }};
    }
    &.ripple:active {
      background-color: ${props => lighten(props.theme.palette.canvasColor, 0.35)};
      background-size: 100%;
      transition: background 0s;
    }
  }
`;

const enhance = compose(
  withLogin,
  withDictionary,
  withMessages
);

class UserProfileComponent extends Component {
  state = {
    userAvatar: '',
    avatarPics: new Array(AVATAR_FILE_NAMES.length).fill('')
  };

  _loadAvatars = async () => {
    try {
      const userAvatar = await avatarLoader(this.props.user.userPicture);
      const avatarPics = await Promise.all(AVATAR_FILE_NAMES.map((n, i) => avatarLoader(i)));
      this.setState({
        userAvatar,
        avatarPics
      });
    } catch (err) {
      logError('cannot load avatars', err);
    }
  };

  componentDidUpdate(prevProps) {
    if (
      (this.props.user && !prevProps.user) ||
      (this.props.user &&
        prevProps.user &&
        this.props.user.userPicture !== prevProps.user.userPicture)
    ) {
      this._loadAvatars();
    }
  }

  componentDidMount() {
    if (this.props.user) {
      this._loadAvatars();
    }
  }

  render() {
    const { userAvatar, avatarPics } = this.state;
    const { dictionary, user, updateUserProfilePicture } = this.props;
    if (!user) {
      return <Redirect to="/" push={false} />;
    }

    const { userPicture } = user;
    const buttonSize = ((window.innerWidth - 6) / 3) * 0.33;
    return (
      <ColumnPage>
        <AvatarPreview>
          <UserAvatar src={userAvatar} size={window.innerHeight * 0.2} selected={true} />
          <div>{dictionary('_prof_avatar_select_message')}</div>
        </AvatarPreview>
        <AvatarChoice size={buttonSize}>
          {avatarPics.map((availableAvatar, i) => {
            return (
              <button
                onClick={() => updateUserProfilePicture(i)}
                key={i}
                className={`avatar-list-item ripple${userPicture === i ? ' selected button' : ''}`}
              >
                <UserAvatar
                  src={availableAvatar}
                  size={buttonSize}
                  // size={window.innerWidth * 0.32 * 0.8}
                  selected={userPicture === i}
                />
              </button>
            );
          })}
        </AvatarChoice>
      </ColumnPage>
    );
  }
}

const UserProfile = enhance(UserProfileComponent);
export default UserProfile;
// const UserProfileRoute = () => (
//   <Route
//     render={() => {
//       return <UserProfile />;
//     }}
//   />
// );
// export default UserProfileRoute;
