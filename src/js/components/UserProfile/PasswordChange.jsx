import React, { useEffect, useState } from 'react';
import { ColumnPage } from 'js/components/app/commons';
// import { Header, Content } from 'js/components/app';
import { PasswordChangeForm } from './PasswordChangeForm';
import { API } from 'ioc-api-interface';
import { useLoader, useMessages } from 'js/hooks/use-ui.hooks';
import { logMain } from 'js/utils/log';
import { useLogin } from 'js/hooks/use-login.hook';
import { Redirect } from 'react-router-dom';

// export function PasswordChange() {
//   return <div>PASSW CHANGE</div>;
// }
async function changePassword(
  currentPassword,
  newPassword,
  setLoading,
  onResponse,
  onError,
  emailAddress,
  login
) {
  setLoading(true);
  try {
    const api = API.getInstance();
    const response = await api.profile.changePassword(currentPassword, newPassword);
    await login(emailAddress, newPassword, null, null);
    onResponse(response.data);
  } catch (err) {
    onError(err);
  } finally {
    setLoading(false);
  }
}

export function PasswordChange({ history }) {
  const [{ user, loggedIn, rememberCredentials }, { login, storeCredentials }] = useLogin();
  const [userData, setUserData] = useState({ currentPassword: '', newPassword: '' });
  const [{ loading }, { loadingStart, loadingStop }] = useLoader();
  const [{ pushMessage, pushError }] = useMessages();
  function setLoading(loading) {
    loading ? loadingStart() : loadingStop();
  }

  function onResponse(responseData) {
    logMain('PasswordChange::onResponse', responseData);
    if (rememberCredentials === true) {
      storeCredentials(true);
    }
    pushMessage('_password_update_ok', 'success');
    if (history) {
      history.replace('/');
    }
  }

  function onError(err) {
    pushError(err);
  }

  useEffect(() => {
    if (!loading && userData.currentPassword && userData.newPassword) {
      changePassword(
        userData.currentPassword,
        userData.newPassword,
        setLoading,
        onResponse,
        onError,
        user.emailAddress,
        login
      );
    }
  }, [userData]);

  function onSubmit(data) {
    console.log('click', data);
    const { currentPassword, newPassword } = data;
    setUserData({ currentPassword, newPassword });
  }

  if (!loggedIn) {
    return <Redirect to="/" push={false} />;
  }
  return (
    <ColumnPage className="password-change">
      <PasswordChangeForm onSubmit={onSubmit} />
    </ColumnPage>
  );
}

export default PasswordChange;
