import React from 'react';
import { useLocale } from 'js/hooks/use-locale.hook';
import { IconMenu, MenuItem, IconButton, Divider } from 'material-ui';
import { SmallAvatar } from 'js/components/app/SmallAvatar';
import styled from 'styled-components';

const UserResume = styled.div.attrs(() => ({ className: 'user-resume' }))`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  padding: 8px;
  opacity: 0.65;
  & div {
    padding: 8px;
  }
`;

const ROLES_MAP = {
  Admin: 'Admin',
  User: 'User',
  Pro: 'Pro',
  Citizen: 'Citizen',
  ExternalProvider: 'External Provider',
  PublicAuthority: 'Public Authority',
  TechnicalService: 'Technical Service',
  FirstResponder: 'First Responder',
  UtilitiesProvider: 'Utilities Provider',
  Volunteer: 'Volunteer',
  TeamLeader: 'Team Leader',
  OrganizationResponsible: 'Organization Responsible',
  DecisionMaker: 'Decision Maker',
  InfieldAgent: 'Infield Agent'
};

const MASKED_ROLES = ['User', 'Pro', 'Citizen']; // keys of ROLES_MAP

function getUserRolestring(userRoles) {
  return userRoles
    .filter(r => !MASKED_ROLES.includes(r))
    .map(r => ROLES_MAP[r])
    .join(', ');
}

export function UserProfileIconMenu({ closeDrawer = null, history, user }) {
  const [{ dictionary }] = useLocale();
  const { userPicture, name, surname, organizationUnits, roles } = user;
  const orgs = organizationUnits.map(ou => ou.displayName).join(', ');
  const rolesString = getUserRolestring(roles);
  const tooltip = `${name} ${surname} - ${orgs}`;
  return (
    <IconMenu
      useLayerForClickAway={true}
      className="edit-icon-menu"
      iconButtonElement={
        <IconButton tooltip={tooltip} tooltipPosition="bottom-center">
          <SmallAvatar avatarIndex={userPicture} isPro={true} size={24} />
        </IconButton>
      }
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      targetOrigin={{ horizontal: 'right', vertical: 'top' }}
      onClick={e => {
        e.preventDefault();
        e.stopPropagation();
      }}
    >
      <UserResume>
        <div>
          {name} {surname}
        </div>
        <div>
          <i>{orgs}</i>
        </div>
        <div>{rolesString}</div>
      </UserResume>
      <Divider />
      <MenuItem
        primaryText={dictionary('_prof_edit_change_avatar')}
        onClick={e => {
          e.preventDefault();
          e.stopPropagation();
          if (typeof closeDrawer === 'function') {
            closeDrawer();
          }
          if (history) {
            history.push('/editprofile');
          }
        }}
      />

      <MenuItem
        primaryText={dictionary('_prof_edit_change_password')}
        onClick={e => {
          e.preventDefault();
          e.stopPropagation();
          if (typeof closeDrawer === 'function') {
            closeDrawer();
          }
          if (history) {
            history.push('/editpassword');
          }
        }}
      />
    </IconMenu>
  );
}
