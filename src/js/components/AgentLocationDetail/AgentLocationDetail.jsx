import './style.scss';
import React, { Component } from 'react';
import {
  CloseIcon,
  EmailIcon,
  PhoneIcon,
  PlaceIcon,
  ScheduleIcon,
  PeopleIcon
} from '../app/drawer/leftIcons/LeftIcons';
import { localizeDate } from 'js/utils/localizeDate';
import WearableDetails from './WearableDetails';

class AgentLocationDetail extends Component {
  closeCard = () => {
    this.props.deselectFeature();
  };

  render() {
    const { locale, feature, dictionary } = this.props;
    const {
      name,
      surname,
      emailAddress,
      phoneNumber,
      lastModified,
      organizationUnitName,
      wearableInfo,
      userName,
      positionType,
      teams
    } = feature.properties;

    const teamNames =
      teams && teams.length > 0 ? teams.map(item => item.displayName).join(', ') : '';
    const coordinatesString = feature.geometry.coordinates.map(c => c.toFixed(2)).join(', ');
    const iconStyle = {
      fontSize: 18,
      paddingRight: 0
    };
    const columnWidth = wearableInfo !== null ? '55%' : '100%';
    return (
      <div className="agent-location-detail" style={{ minHeight: 500 }}>
        <div className="ag-loc-title">
          <span>
            {surname} {name}
          </span>
          <span>
            <CloseIcon onClick={this.closeCard} />
          </span>
        </div>
        <div className="ag-loc-container">
          <div className="ag-loc-body" style={{ width: columnWidth }}>
            <div className="ag-loc-first-col" style={{ width: '100%' }}>
              <div className="ag-loc-empl">Agent id: {userName}</div>
              <div className="ag-loc-general">
                <div>
                  <EmailIcon iconStyle={iconStyle} />
                  <span>{emailAddress}</span>
                </div>
                <div>
                  <PhoneIcon iconStyle={iconStyle} />
                  <span>{phoneNumber}</span>
                </div>
                <div>
                  <PeopleIcon iconStyle={iconStyle} />
                  <span>{teamNames}</span>
                </div>
              </div>
              <div style={{ padding: 10 }}>
                <div className="ag-loc-general-position">
                  <div>
                    <PlaceIcon iconStyle={iconStyle} />
                    <span>Last Position</span>
                  </div>
                  <span style={{ paddingLeft: 50 }}>{coordinatesString}</span>
                  <span style={{ paddingLeft: 50, fontStyle: 'italic' }}>{positionType}</span>
                </div>
                <div className="ag-loc-general-lastupdate">
                  <div>
                    <ScheduleIcon iconStyle={iconStyle} />
                    <span>Last update</span>
                  </div>
                  <span style={{ paddingLeft: 50 }}>
                    {localizeDate(lastModified, locale, 'L LT')}
                  </span>
                </div>
              </div>
              <div className="ag-loc-organisation" style={{ width: columnWidth }}>
                <span>{organizationUnitName}</span>
              </div>
            </div>
          </div>
          {wearableInfo !== null && (
            <div style={{ display: 'flex' }}>
              <div className="agent-verticalLine" />
              <WearableDetails dictionary={dictionary} features={wearableInfo} />
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default AgentLocationDetail;
