import './style.scss';
import React, { Component } from 'react';
import {
  PowerIcon,
  SmileIcon,
  BatteryIcon,
  DirectionWalkIcon,
  DropIcon,
  AirConditionedIcon,
  TollIcon,
  AccessibilityIcon
} from '../app/drawer/leftIcons/LeftIcons';
import { lightGreenA400, redA700 } from 'material-ui/styles/colors';

const iconStyle = {
  fontSize: 18,
  paddingRight: 0
};

class WearableDetails extends Component {
  render() {
    const { features } = this.props;
    const { deviceStatus, oxygenLevel, fallDetection } = features;
    const deviceColor = deviceStatus === 'off' ? redA700 : lightGreenA400;
    const hasOxygen = oxygenLevel && oxygenLevel.type;
    const oxygenColor =
      deviceStatus === 'off'
        ? 'grey'
        : hasOxygen
          ? oxygenLevel.type === 'low'
            ? redA700
            : lightGreenA400
          : '#fff';
    const fallDownColor =
      deviceStatus === 'off' ? 'grey' : fallDetection !== 'fallModerate' ? redA700 : lightGreenA400;

    return (
      <div className="wearable-detail">
        <div className="wearable-row">
          <PowerIcon iconStyle={{ ...iconStyle, color: deviceColor }} />
          <span>Wearable: {features.deviceStatus}</span>
        </div>
        <div className="wearable-row">
          {deviceStatus === 'off' ? (
            <div>
              <TollIcon iconStyle={{ ...iconStyle, color: oxygenColor }} />
              <span>Oxygen level: -</span>
            </div>
          ) : hasOxygen ? (
            <div>
              <TollIcon iconStyle={{ ...iconStyle, color: oxygenColor }} />
              <span>Oxygen level: {oxygenLevel.type}</span>
            </div>
          ) : (
            <span />
          )}
        </div>
        <div className="wearable-row">
          {deviceStatus === 'off' ? (
            <div>
              <DirectionWalkIcon iconStyle={{ ...iconStyle, color: 'grey' }} />
              <span>Movement: -</span>
            </div>
          ) : (
            <div>
              <DirectionWalkIcon iconStyle={{ ...iconStyle }} />
              <span>Movement: {features.movingState ? features.movingState : '-'}</span>
            </div>
          )}
        </div>
        <div className="wearable-row">
          {deviceStatus === 'off' ? (
            <div>
              <SmileIcon iconStyle={{ ...iconStyle, color: 'grey' }} />
              <span>Panic: -</span>
            </div>
          ) : (
            <div>
              <SmileIcon iconStyle={{ ...iconStyle }} />
              <span>Panic: {features.panicDetection ? features.panicDetection : '-'}</span>
            </div>
          )}
        </div>
        <div className="wearable-row">
          {deviceStatus === 'off' ? (
            <div>
              <AccessibilityIcon iconStyle={{ ...iconStyle, color: 'grey' }} />
              <span>Fall down: -</span>
            </div>
          ) : (
            <div>
              <AccessibilityIcon iconStyle={{ ...iconStyle, color: fallDownColor }} />
              <span>Fall down: {fallDetection ? fallDetection : '-'}</span>
            </div>
          )}
        </div>
        <div className="wearable-row">
          {deviceStatus === 'off' ? (
            <div>
              <AirConditionedIcon iconStyle={{ ...iconStyle, color: 'grey' }} />
              <span>Temperature: -</span>
            </div>
          ) : (
            <div>
              <AirConditionedIcon iconStyle={{ ...iconStyle }} />
              <span>Temperature: {features.temperature.toFixed(1)}° C</span>
            </div>
          )}
        </div>
        <div className="wearable-row">
          {deviceStatus === 'off' ? (
            <div>
              <AirConditionedIcon iconStyle={{ ...iconStyle, color: 'grey' }} />
              <span>Pressure: -</span>
            </div>
          ) : (
            <div>
              <AirConditionedIcon iconStyle={{ ...iconStyle }} />
              <span>Pressure: {features.pressure.toFixed(2)} mbar</span>
            </div>
          )}
        </div>
        <div className="wearable-row">
          {deviceStatus === 'off' ? (
            <div>
              <DropIcon iconStyle={{ ...iconStyle, color: 'grey' }} />
              <span>Humidity: -</span>
            </div>
          ) : (
            <div>
              <DropIcon iconStyle={{ ...iconStyle }} />
              <span>Humidity: {features.humidityPercentage.toFixed(1)}%</span>
            </div>
          )}
        </div>
        <div className="wearable-row">
          {deviceStatus === 'off' ? (
            <div>
              <BatteryIcon iconStyle={{ ...iconStyle, color: 'grey' }} />
              <span>Battery: -</span>
            </div>
          ) : (
            <div>
              <BatteryIcon iconStyle={{ ...iconStyle }} />
              <span>Battery: {features.batteryPercentage}%</span>
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default WearableDetails;
