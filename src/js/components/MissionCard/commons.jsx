import styled from 'styled-components';
import { featureFilter } from '@mapbox/mapbox-gl-style-spec';
import { darken } from 'material-ui/utils/colorManipulator';
import { MISSION_TASK_COLOR, MISSION_COLORS } from 'js/startup/iReactTheme';

export const HEADER_COLOR = darken(MISSION_TASK_COLOR, 0.7);

export function getMissionStatusColor(ts) {
  return ts && MISSION_COLORS.temporalStatus[ts] ? MISSION_COLORS.temporalStatus[ts] : 'black';
}

export const MissionColorDot = styled.div.attrs(() => ({
  className: props => `status-mission ${props.temporalStatus}`
}))`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  margin: 0 8px;
  background-color: ${props => getMissionStatusColor(props.temporalStatus)};
  position: relative;

  & .tooltiptext {
    visibility: hidden;
    opacity: 0;
    width: 120px;
    background-color: ${props => props.theme.palette.canvasColor};
    color: ${props => props.theme.palette.textColor};
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    box-shadow: 4px 4px 4px -1px rgba(0, 0, 0, 0.75);
    transition: opacity 300ms cubic-bezier(0.39, 0.575, 0.565, 1);

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
    right: 8px;
    top: -18px;
  }

  &:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
  }
`;

export function getCompletedTaskIdsFilterFn(taskIds) {
  return featureFilter([
    'all',
    ['==', 'temporalStatus', 'completed'],
    ['in', 'id', ...taskIds]
  ]).bind(null, null);
}

const actionsWidth = 24;

export const CardContainer = styled.div.attrs(() => ({ className: 'card-container' }))`
  padding: 10px 20px;
  width: 100%;
  height: calc(100% - 20px);
  display: flex;
  font-size: 12px;
`;

export const InfoContainer = styled.div.attrs(() => ({ className: 'info-container' }))`
  display: flex;
  flex-direction: column;
  width: calc(100% - ${actionsWidth}px);
  height: 100%;
  justify-content: space-around;
`;

export const BarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 24px;
  width: 100%;
  font-weight: bold;
`;

export const DescriptionContainer = styled.div`
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;

  .mission-description-title {
    text-transform: uppercase;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow-x: hidden;
    width: calc(100% - 24px);
  }
`;

export const DetailsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  width: 100%;
  height: calc(100% - 24px - 16px);
`;

export const DateStringContainer = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  align-items: baseline;
  justify-content: space-between;
`;

const TookOverByChip = styled.span`
  margin-left: auto;
  margin-right: 8px;
  text-transform: uppercase;
  font-weight: 300;
  padding: 2px 4px;
  height: 16px;
  line-height: 16px;
  font-size: 14px;
  border-radius: 3px;
`;

export const ChipTookOverBy = styled(TookOverByChip)`
  background-color: ${props => props.muiTheme.palette.textColor};
  color: ${props => props.theme.palette.backgroundColor};
`;

export const ChipNoneTookOver = styled(TookOverByChip)`
  background-color: ${props => props.theme.palette.backgroundColor};
  color: ${props => props.theme.palette.textColor};
  border: 1px dashed;
  box-sizing: border-box;
`;

export const ChipCompleted = styled(TookOverByChip)`
  background-color: ${props => props.theme.palette.backgroundColor};
  color: ${props => props.theme.palette.textColor};
  border: 1px dashed;
  box-sizing: border-box;
`;
