import 'style/tos-style.scss';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import React, { Component, lazy, Suspense } from 'react';
import { render } from 'react-dom';
import { iReactTheme } from 'js/startup/iReactTheme';
import { MuiThemeProvider, getMuiTheme } from 'material-ui/styles';
import qs from 'qs';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import styled, { ThemeProvider } from 'styled-components';
import { loadDictionary } from 'js/utils/getAssets';
import { makeDictionary } from 'ioc-localization';
const ConfirmEmailComponent = lazy(() => import('js/components/UserRegistration/ConfirmEmail'));
const ResetPasswordComponent = lazy(() => import('js/components/UserRegistration/ResetPassword'));

/*
 * Handle user account actions such as reset password
 * and confirm email
 */
const _SUPPORTED_LANGUAGES =
  typeof SUPPORTED_LANGUAGES === 'undefined'
    ? require('locale/languages.json')
    : SUPPORTED_LANGUAGES;

const supportedLanguages = Object.keys(_SUPPORTED_LANGUAGES);
const FALLBACK_LANGUAGE = supportedLanguages[0] || 'en';
const SYSTEM_LANGUAGE =
  typeof window.navigator.language === 'string'
    ? window.navigator.language.substr(0, 2).toLowerCase()
    : null;

const DEFAULT_LOCALE =
  supportedLanguages.indexOf(SYSTEM_LANGUAGE) === -1 ? FALLBACK_LANGUAGE : SYSTEM_LANGUAGE;
const DefaultDictionary = {
  isEmpty: true,
  locale: DEFAULT_LOCALE,
  translations: {},
  supportedLanguages
};

const location = window.location;

const searchParams = qs.parse(location.search.replace(/^\?/, ''));
const { action, ...rest } = searchParams;

const Base = styled.div`
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const MainContainer = styled(Base)`
  width: 100vw;
  height: 100vh;
  position: fixed;
  z-index: 1;
`;

const OverlayBackground = styled(Base)`
  background-color: rgba(0, 0, 0, 0.4);
  position: fixed;
  z-index: 99999;
`;

const pushError = err => console.error(err);
const pushMessage = message => {
  window.alert(message);
  window.location.replace('/account.html?action=ok');
};

const commonProps = {
  pushError,
  pushMessage
};

const P404 = () => (
  <div>
    <h1>{action ? `${action} not valid` : '404: not found'}</h1>
  </div>
);

const ACTION_OK = 'ok';
const ACTION_CONFIRM = 'confirm';
const ACTION_RESET = 'reset';

const dictionary = makeDictionary(DefaultDictionary);

class PasswordManagementApp extends Component {
  _dictionary = dictionary;

  state = {
    dictIsEmpty: true,
    locale: dictionary.language.locale,
    loading: false
  };

  loadingStart = () => {
    console.log('LOADING START');
    this.setState({ loading: true });
  };

  loadingStop = () => {
    console.log('LOADING STOP');
    this.setState({ loading: false });
  };

  _checkLanguageIsLoaded = async () => {
    if (this.state.dictIsEmpty) {
      const params = await loadDictionary(this.state.locale);
      const { locale, translationLoader } = params;
      translationLoader(translations => {
        console.log('Loaded transl', locale, translations);
        this._dictionary.language = { translations, locale };
        this.setState({
          dictIsEmpty: Object.keys(translations).length === 0,
          locale
        });
      });
    }
    return;
  };

  componentDidMount() {
    document.querySelector('body').style.backgroundColor = iReactTheme.palette.canvasColor;

    this._checkLanguageIsLoaded();
  }

  onRouterError = err => {
    console.error('Router error', err);
  };

  render() {
    const theme = getMuiTheme(iReactTheme);
    let InnerComponent = null;
    switch (action) {
      case ACTION_CONFIRM:
        InnerComponent = ConfirmEmailComponent;
        break;
      case ACTION_RESET:
        InnerComponent = ResetPasswordComponent;
        break;
      case ACTION_OK:
        InnerComponent = () => <div>Please close this window</div>;
        break;
      default:
        InnerComponent = P404;
        break;
    }

    return (
      <MuiThemeProvider muiTheme={theme}>
        <ThemeProvider theme={theme}>
          <MainContainer className="main-container">
            <Suspense fallback={<div>Loading...</div>}>
              <InnerComponent
                location={window.location}
                {...commonProps}
                loadingStart={this.loadingStart}
                loadingStop={this.loadingStop}
                locale={this.state.locale}
                dictionary={this._dictionary}
              />
            </Suspense>
            {this.state.loading && (
              <OverlayBackground>
                <RefreshIndicator
                  size={40}
                  left={10}
                  top={0}
                  status={'loading'}
                  style={{ position: 'relative' }}
                />
              </OverlayBackground>
            )}
          </MainContainer>
        </ThemeProvider>
      </MuiThemeProvider>
    );
  }
}

export default function launch() {
  const element = document.getElementById('passw');
  if (element) {
    render(<PasswordManagementApp />, element);
    console.log('PasswordManagementApp', action, rest);
  }
}
