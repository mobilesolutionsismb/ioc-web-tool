import supercluster from 'supercluster';
import bbox from '@turf/bbox';
import { featureCollection } from '@turf/helpers';
import axios from 'axios';
// const CLUSTER_INNER_PROPS = ['cluster', 'cluster_id', 'point_count', 'point_count_abbreviated', 'bounds'];

// Polygons and missionTasks must not be clusterd
// const toBeClusterd = f => f.geometry.type === 'Point' && f.properties.itemType !== 'missionTask';
// const notToBeClustered = f => f.geometry.type !== 'Point' || f.properties.itemType === 'missionTask';

let workerFeatures = [];
let clusteredFeaturesURL = null;

function _makeDataURL(urlPointer, data) {
  _clearDataURL(urlPointer);
  const blob = new Blob([JSON.stringify(data)], { type: 'application/json' });
  urlPointer = URL.createObjectURL(blob);
  return urlPointer;
}

function _clearDataURL(urlPointer) {
  if (urlPointer !== null) {
    URL.revokeObjectURL(urlPointer);
    urlPointer = null;
  }
}

/**
 * Get cluster configuration
 * @param {Object} stats
 * @param {Number} nPoints
 */
function getClusterConfiguration(stats, nPoints) {
  const population = stats.point_count;
  const entries = Object.entries(stats.categories);

  const _stops = entries.map(e => Math.round(e[1] * (nPoints - entries.length) / population));
  const totalStops = _stops.reduce((t, n) => {
    t += n;
    return t;
  }, 0);
  if (totalStops + entries.length !== nPoints) {
    const minIndex = _stops.indexOf(Math.min(..._stops));
    _stops[minIndex] += 1;
  }
  const stops = Array(nPoints).fill('_pause_el');
  let current = 0;
  for (let entry of entries) {
    const index = entries.findIndex(e => e[0] === entry[0]);
    const _start = current;
    const _end = current + _stops[index];
    stops.fill(entry[0], _start, _end);
    current = _end + 1;
  }

  const props = stops
    .map((sv, i) => ({
      [`_border-${i}`]: sv
    }))
    .reduce((o, n) => {
      o = { ...n, ...o };
      return o;
    }, {});
  return { ...stats, ...props };
}

let index = null;
// let nonClusteredFeatures = [];

/** Inital configuration */
function initial() {
  return {
    categories: {}
  };
}

/**
 * Map Feature properties to categories
 * Mapping of clustered itemTypes to categories
 * report --> report
 * emergencyCommunication --> {reportRequest || alertOrWarning}
 * mission --> mission
 * agentLocation --> agentLocation
 * mapRequest --> mapRequest
 * other --> unknown
 */

/**
 * Categorize each feature properties
 * @see https://github.com/mapbox/supercluster#property-mapreduce-options
 * @param {Object} properties
 * @returns {Object} categories
 */
function mapProperties(properties) {
  let categories = {};
  const itemType = properties.itemType;
  switch (itemType) {
    case 'emergencyCommunication':
      if (properties.type === 'warning' || properties.type === 'alert') {
        categories['alertOrWarning'] = 1;
      } else if (properties.type === 'reportRequest') {
        categories['reportRequest'] = 1;
      } else {
        // SHOULD NEVER HAPPEN
        console.warn('WORKER::Unknown category found', itemType, properties);
        categories['unknown'] = 1;
      }
      break;
    case 'report':
    case 'mission':
    case 'agentLocation':
    case 'mapRequest':
      categories[itemType] = 1;
      break;
    default:
      // SHOULD NEVER HAPPEN
      console.warn('WORKER::Unknown category found', itemType, properties);
      categories['unknown'] = 1;
      break;
  }
  return { categories };
}

/**
 * Reduce properties to stats
 * @see https://github.com/mapbox/supercluster#property-mapreduce-options
 * @param {Object} accumulated
 * @param {Object} properties
 */
function reduceProperties(accumulated, properties) {
  for (const id in properties.categories) {
    accumulated.categories[id] = (accumulated.categories[id] || 0) + properties.categories[id];
  }
}

function init(features = []) {
  // const points = features.filter(toBeClusterd);
  // nonClusteredFeatures = features.filter(notToBeClustered);
  index = supercluster({
    log: false,
    radius: 40, //double the radius of the circles - TODO pass as a param to INIT
    extent: 512,
    maxZoom: 15, // Max zoom - TODO pass as a param to INIT
    // initial properties of a cluster (before running the reducer)
    initial: initial,
    // properties to use for individual points when running the reducer
    map: mapProperties,
    // a reduce function for calculating custom cluster properties
    reduce: reduceProperties
  }).load(features);

  postMessage({ ready: true, messageType: 'init' });
}

function clusterize(boundingBox, zoom, pointsPerBorder) {
  /* eslint-disable */
  self._clusterizing = true;
  /* eslint-enable */
  const _zoom = Math.round(zoom); // must be round integer
  const features = index.getClusters(boundingBox, _zoom);
  // console.log('%c Worker::clusterize', 'background: gold; color: rebeccapurple', boundingBox, zoom, features.length);
  const result = features.reduce(
    (result, f) => {
      if (f.properties.cluster === true) {
        f.properties = getClusterConfiguration(f.properties, pointsPerBorder);
        const children = index.getLeaves(f.properties.cluster_id, f.properties.point_count); //index.getChildren(f.properties.cluster_id, _zoom + 1); // points and clusters, not just points
        const bounds = bbox(featureCollection(children));
        f.properties.bounds = bounds;
        result.clusteredFeatures = [f, ...result.clusteredFeatures];
      } else {
        result.singleFeatures = [f, ...result.singleFeatures];
      }
      return result;
    },
    { singleFeatures: [], clusteredFeatures: [] }
  );

  const data = featureCollection([...result.singleFeatures, ...result.clusteredFeatures]);
  postMessage({
    clusteredFeaturesURL: _makeDataURL(clusteredFeaturesURL, data),
    // featureCollection: featureCollection([...result.singleFeatures, ...result.clusteredFeatures]),
    messageType: 'clusterize'
  });
  /* eslint-disable */
  self._clusterizing = false;
  /* eslint-enable */
}

/**
 * Retrieve position (lat,lng) for given feature id, bounding box, zoom
 * @param {*} featureId
 * @param {*} boundingBox
 * @param {*} zoom
 * @param {*} type
 */
function getFeaturePosition(featureId, boundingBox, zoom, type = 'report') {
  const _zoom = Math.round(zoom);

  const findPredicate = f =>
    f.properties.id === featureId &&
    // Currenty we filter reports by having "aleradyVoted: boolean"
    type === 'report'
      ? typeof f.properties.alreadyVoted !== 'undefined'
      : f.properties.type === type;

  const featuresAtCurrentZoomLevel = index.getClusters(boundingBox, _zoom);
  // Divide in cluster and single features
  const data = featuresAtCurrentZoomLevel.reduce(
    (result, f) => {
      if (f.properties.cluster === true) {
        result.clusteredFeatures = [f, ...result.clusteredFeatures];
      } else {
        result.singleFeatures = [f, ...result.singleFeatures];
      }
      return result;
    },
    { singleFeatures: [], clusteredFeatures: [] }
  );
  let position = null;

  // Search first in single features
  let singleFeature = data.singleFeatures.find(findPredicate);
  if (typeof singleFeature !== 'undefined') {
    // Is single feature: return position
    if (singleFeature.geometry.type === 'Point') {
      position = singleFeature.geometry.coordinates;
    }
    postMessage({
      messageType: 'getFeaturePosition',
      position
    });
  } else {
    // find in clusters
    for (let clusterFeature of data.clusteredFeatures) {
      let childFeatures = index.getLeaves(clusterFeature.properties.cluster_id, _zoom, Infinity);
      let singleFeature = childFeatures.find(findPredicate);
      if (typeof singleFeature !== 'undefined') {
        position = clusterFeature.geometry.coordinates;
        break;
      }
    }
    postMessage({
      messageType: 'getFeaturePosition',
      position
    });
  }
}

// Works with the chunked transfer version
// function addFeatures(features, chunks, chunkNo) {
//     console.log(`Worker: add ${features.length} features, chunk: ${chunkNo}/${chunks}`);
//     if (chunkNo === 0) {
//         workerFeatures = Array.isArray(features) ? features : [];
//     }
//     else {
//         workerFeatures = [...workerFeatures, ...features];
//     }
//     if (chunkNo === chunks - 1) {
//         init(workerFeatures);
//     }
// }

// Retrieve data using a blob url
async function fetchFeatures(featuresURL) {
  try {
    const response = await axios.get(featuresURL);
    workerFeatures = response.data;
    init(workerFeatures);
  } catch (err) {
    console.error('Cannot parse features', err);
  }
}

/* eslint-disable */
self._clusterizing = false;

self.addEventListener('error', error => {
  console.error('Report cluster worker error', error);
  self.postMessage({ error });
});

self.addEventListener('message', event => {
  if (event.data.action) {
    // console.log('%c Worker received action:', 'background: gold; color: rebeccapurple', event.data.action);
    switch (event.data.action) {
      case 'updatedata':
      case 'init':
        const { featuresURL } = event.data;
        fetchFeatures(featuresURL);
        // const { features, chunks, chunkNo } = event.data;
        // addFeatures(features, chunks, chunkNo);

        // init(event.data.features);
        break;
      case 'clusterize':
        if (!self._clusterizing) {
          /* eslint-enable */
          clusterize(event.data.bbox, event.data.zoom, event.data.pointsPerBorder);
        }
        break;
      case 'getFeaturePosition':
        getFeaturePosition(event.data.featureId, event.data.bbox, event.data.zoom, event.data.type);
        break;
      default:
        break;
    }
  }
});
