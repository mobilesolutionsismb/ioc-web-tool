import supercluster from 'supercluster';
import bbox from '@turf/bbox';
import { featureCollection } from '@turf/helpers';

//console.log('%c Worker Spawned', 'background: gold; color: orange', supercluster);

let index = null;

function initial() {
  return {
    categories: {}
  };
}
function mapProperties(properties) {
  return { sum: properties.myValue };
}
function reduceProperties(accumulated, properties) {
  accumulated.sum += properties.sum;
}

function init(features = []) {
  index = supercluster({
    log: false,
    radius: 50,
    extent: 512,
    maxZoom: 16,
    // initial properties of a cluster (before running the reducer)
    initial: initial,
    // properties to use for individual points when running the reducer
    map: mapProperties,
    // a reduce function for calculating custom cluster properties
    reduce: reduceProperties
  }).load(features);

  postMessage({ ready: true });
}

function clusterize(boundingBox, zoom) {
  let features = index.getClusters(boundingBox, zoom);
  let result = features.reduce(
    (result, f) => {
      if (f.properties.cluster === true) {
        const children = index.getLeaves(f.properties.cluster_id, zoom);
        const bounds = bbox(featureCollection(children));
        f.properties.bounds = bounds;
        result.clusteredFeatures = [f, ...result.clusteredFeatures];
      } else {
        result.singleFeatures = [f, ...result.singleFeatures];
      }
      return result;
    },
    { singleFeatures: [], clusteredFeatures: [] }
  );
  postMessage(result);
}

/* eslint-disable */
self.addEventListener('error', error => {
  console.error('Report cluster worker error', error);
  self.postMessage({ error });
});

self.addEventListener('message', event => {
  /* eslint-enable */
  if (event.data.action) {
    // console.log('%c Worker received action:', 'background: gold; color: rebeccapurple', event.data.action);
    switch (event.data.action) {
      case 'updatedata':
      case 'init':
        init(event.data.features);
        break;
      case 'clusterize':
        clusterize(event.data.bbox, event.data.zoom);
        break;
      default:
        break;
    }
  }
});
