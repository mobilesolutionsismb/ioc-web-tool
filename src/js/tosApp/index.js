import qs from 'qs';
import remark from 'remark';
import remarkHtml from 'remark-html';
import axios from 'axios';
import 'style/tos-style.scss';

/*
 * This file contains the logic for displaying the terms of services
 * or the privacy policy to external clients _without_ loading
 * the whole I-REACT frontend (thus much faster)
 */

const PRIVACY_FILE = 'privacy';
const TERMS_FILE = 'tos_citizen';
const location = window.location;
const searchParams = qs.parse(location.search.replace(/^\?/, ''));
const { lang, type } = searchParams;
const _lang = lang || navigator.language.slice(0, 2);
const filename = type === 'privacy' ? PRIVACY_FILE : TERMS_FILE;

const rootDirectoryProvider = {};

Object.defineProperty(rootDirectoryProvider, 'rootDirectory', {
  get: () => PUBLIC_PATH
});

async function getMarkdownTextAsHTML(filename, locale = 'en') {
  const pathname = `${rootDirectoryProvider.rootDirectory}locales/lang_${locale}/${filename}.md`;
  let markdownResponse;
  try {
    markdownResponse = await axios.get(pathname);
  } catch (err) {
    if (err.response && err.response.status === 404) {
      markdownResponse = await axios.get(
        `${rootDirectoryProvider.rootDirectory}locales/lang_en/${filename}.md`
      );
    } else {
      throw err;
    }
  }
  const text = markdownResponse.data;
  return remark()
    .use(remarkHtml)
    .processSync(text).contents;
}

export default async function launch() {
  const tosNode = document.querySelector('#tos');
  const text = await getMarkdownTextAsHTML(filename, _lang);
  tosNode.innerHTML = text;
}
