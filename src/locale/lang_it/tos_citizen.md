# TERMINI DEI SERVIZI E CONDIZIONI PER I CITTADINI

I presenti Termini di servizio ("Termini") regolano l'accesso e l'uso dei servizi I-REACT (sito web, app per dispositivi mobili, API, e-mail e notifiche push) e qualsiasi altro servizio coperto da I-REACT che collega a questi Termini (collettivamente, "Servizi") e qualsiasi informazione, testo, link, grafica, foto, video o altro materiale o disposizione di materiali caricati, scaricati o visualizzati sui Servizi (collettivamente denominati "Contenuti"). Utilizzando i Servizi accetti di essere vincolato da questi Termini.

1.  Chi può utilizzare i servizi
1.  Contenuto sui servizi
1.  Utilizzo dei servizi
1.  Limitazioni di responsabilità e limitazioni di responsabilità / limitazioni di responsabilità
1.  Generale

Nel caso in cui ci sia una discrepanza tra la [versione in lingua inglese di questo documento](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=en) e qualsiasi copia tradotta dei Termini di Servizio, la versione inglese prevarrà.

## CHI PUÒ UTILIZZARE I SERVIZI

È possibile utilizzare i Servizi solo se si accettano i Termini di servizio e l'Informativa sulla privacy di I-REACT. Se accetti i presenti Termini e utilizzi i Servizi per conto di una società, organizzazione, governo o altra entità legale, dichiari e garantisci di essere autorizzato a farlo.

### CONTENUTI SUI SERVIZI

L'utente è responsabile dell'utilizzo dei Servizi e di qualsiasi Contenuto (immagini, video, posizione, testo e metadati) forniti dall'utente, inclusa la conformità a leggi, norme e regolamenti applicabili. Dovresti fornire solo contenuti che ti è comodo condividere con gli altri. I contenuti inviati nei rapporti saranno convalidati dalle autorità e potrebbero anche essere soggetti a una procedura di peer-review.
Qualsiasi uso o affidamento su qualsiasi Contenuto o materiale pubblicato tramite i Servizi o ottenuto dall'utente attraverso i Servizi è a proprio rischio.
I-REACT rispetta i diritti di proprietà intellettuale altrui e si aspetta che gli utenti dei Servizi facciano lo stesso. Ci riserviamo il diritto di rimuovere il Contenuto presumibilmente in violazione senza preavviso, a nostra esclusiva discrezione e senza responsabilità nei confronti dell'utente.

## I TUOI DIRITTI

Conservi i tuoi diritti su qualsiasi Contenuto che invii, pubblichi o mostri su o attraverso i Servizi. Qual è il tuo è il tuo - tu possiedi il tuo Contenuto (e le tue foto e video sono parte del Contenuto).

Inviando, pubblicando o visualizzando Contenuti su o attraverso i Servizi, ci concedi una licenza mondiale, non esclusiva e priva di royalty (con il diritto di sub-licenza) di utilizzare, copiare, riprodurre, elaborare, adattare, modificare, pubblicare, trasmettere , visualizzare e distribuire tali contenuti in tutti i media o metodi di distribuzione (ora conosciuti o sviluppati successivamente). Questa licenza ci autorizza a rendere disponibili i tuoi contenuti per il resto del mondo e a consentire agli altri di fare lo stesso. Accetti che questa licenza includa il diritto per I-REACT di fornire, promuovere e migliorare i Servizi e rendere il Contenuto inviato ao tramite i Servizi disponibile ad altre società, organizzazioni o individui per la diffusione, diffusione, distribuzione, promozione o pubblicazione di tali Contenuti su altri media e servizi, soggetti ai nostri termini e condizioni per l'uso di tali Contenuti. Tali ulteriori usi da parte di I-REACT, o di altre società, organizzazioni o singoli individui, possono essere effettuati senza alcun compenso a te corrisposto in relazione al Contenuto che l'Utente invia, pubblica, trasmette o rende altrimenti disponibile attraverso i Servizi.

I-REACT stabilirà una serie di regole in continua evoluzione su come i partner dell'ecosistema possono interagire con i tuoi contenuti sui servizi. Queste regole esistono per abilitare un ecosistema aperto tenendo conto dei tuoi diritti. Comprendi che noi, o qualsiasi altra organizzazione rilevante, possiamo modificare o adattare il tuo Contenuto in quanto è distribuito, sindacato, pubblicato o trasmesso da noi e dai nostri partner e / o apportare modifiche al Contenuto per convalidarlo, adattarlo a diversi media e per allinearlo a fini I-REACT. L'utente dichiara e garantisce di disporre di tutti i diritti, il potere e l'autorità necessari per concedere i diritti concessi nel presente documento a qualsiasi Contenuto inviato.

## UTILIZZO DEI SERVIZI

È possibile utilizzare i Servizi solo in conformità con i presenti Termini e tutte le leggi, le norme e i regolamenti applicabili.

I nostri servizi si evolvono costantemente. Come tale, i Servizi possono cambiare di volta in volta, a nostra discrezione. Potremmo interrompere (in modo permanente o temporaneo) la fornitura dei Servizi o di qualsiasi funzionalità all'interno dei Servizi all'utente o agli utenti in generale. Inoltre, ci riserviamo il diritto di creare limiti all'uso e allo stoccaggio a nostra esclusiva discrezione in qualsiasi momento. Potremmo anche rimuovere o rifiutare di distribuire qualsiasi Contenuto sui Servizi, sospendere o chiudere utenti e rivendicare nomi utente senza responsabilità nei tuoi confronti. Inoltre, a seconda dello sviluppo del modello di business della piattaforma I-REACT, e in considerazione per I-REACT che concede l'accesso e l'uso dei Servizi, l'utente accetta che I-REACT e i suoi fornitori e partner terzi possano inserire pubblicità sui Servizi o in connessione con la visualizzazione di Contenuti o informazioni dai Servizi, sia che vengano presentati da voi o da altri.

Accetti di non abusare dei nostri Servizi, ad esempio, interferendo con essi o accedendoli utilizzando un metodo diverso dall'interfaccia e le istruzioni che forniamo. Durante l'accesso o l'utilizzo dei Servizi, non è possibile effettuare alcuna delle seguenti operazioni: (i) accedere, manomettere o utilizzare aree non pubbliche dei Servizi, i sistemi informatici di I-REACT oi sistemi di consegna tecnica dei fornitori di I-REACT; (ii) analizzare, analizzare o testare la vulnerabilità di qualsiasi sistema o rete o violare o eludere eventuali misure di sicurezza o di autenticazione; (iii) accedere o cercare o tentare di accedere o cercare i Servizi con qualsiasi mezzo (automatico o di altro tipo) diverso dalle nostre interfacce pubblicate attualmente disponibili fornite da I-REACT (e solo in conformità ai termini e alle condizioni applicabili), a meno che non sia stato specificamente autorizzato a farlo in un accordo separato con I-REACT o (iv) interferire con, o interrompere, (o tentare di farlo), l'accesso di qualsiasi utente, host o rete, incluso, senza limitazioni, invio di virus, sovraccarico, allagamento, spamming, bombardamento di posta elettronica dei Servizi o scripting della creazione di Contenuti in modo tale da interferire o creare un onere eccessivo per i Servizi. Ci riserviamo inoltre il diritto di accedere, leggere, conservare e divulgare qualsiasi informazione che riteniamo ragionevole sia necessaria per (i) soddisfare qualsiasi legge, regolamento, processo legale o richiesta governativa applicabile, (ii) applicare i Termini, compresa la ricerca di potenziali violazioni del presente, (iii) rilevare, prevenire o altrimenti affrontare frodi, sicurezza o problemi tecnici, (iv) rispondere alle richieste di supporto degli utenti, o (v) proteggere i diritti, la proprietà o la sicurezza di I-REACT, dei suoi utenti e del pubblico . I-REACT non divulga le informazioni di identificazione personale a terzi se non in conformità con la nostra Informativa sulla privacy.

## IL TUO ACCOUNT

Potrebbe essere necessario creare un account per utilizzare alcuni dei nostri servizi. Sei responsabile della salvaguardia del tuo account, quindi utilizza una password complessa e limita il suo utilizzo a questo account. Non possiamo e non saremo responsabili per eventuali perdite o danni derivanti dal mancato rispetto di quanto sopra.

Per poter utilizzare I-REACT Services, è necessario aggiungere un numero di smartphone al proprio account. È possibile controllare la maggior parte delle comunicazioni dai Servizi. Potremmo aver bisogno di fornirti determinate comunicazioni, come annunci di servizio e messaggi amministrativi. Queste comunicazioni sono considerate parte dei Servizi e del tuo account e potresti non essere in grado di rifiutare di riceverle. Se in seguito cambi o disattivi il tuo numero di telefono, devi aggiornare le informazioni del tuo account per impedirci di comunicare con chiunque acquisisca il tuo vecchio numero e per poter mantenere attivo il Servizio per il tuo account.

## LA TUA LICENZA PER UTILIZZARE I SERVIZI

Per testare la soluzione, I-REACT offre una licenza personale, mondiale, esente da royalty, non assegnabile e non esclusiva per utilizzare il software fornito all'utente come parte dei Servizi. Questa licenza ha l'unico scopo di consentire all'utente di utilizzare e godere dei vantaggi dei Servizi forniti da I-REACT, secondo le modalità consentite dalle presenti Condizioni. Una volta che la piattaforma è completamente sviluppata e funzionante e il modello di business è stato definito, le condizioni della licenza potrebbero cambiare.

I Servizi possono essere protetti da copyright, marchi commerciali e altre leggi sia in Europa che all'estero, questo sarà ulteriormente definito lungo il percorso. Nulla nei Termini conferisce all'utente il diritto di utilizzare il nome I-REACT o alcuno dei marchi, loghi, nomi di dominio e altre caratteristiche distintive del marchio I-REACT. Tutti i diritti, titoli e interessi relativi ai Servizi (esclusi i Contenuti forniti dagli utenti) sono e resteranno di proprietà esclusiva di I-REACT e del suo Consorzio. Eventuali commenti, commenti o suggerimenti che potresti fornire su I-REACT o sui Servizi sono interamente volontari e saremo liberi di utilizzare tali commenti, commenti o suggerimenti come riteniamo opportuno e senza alcun obbligo nei tuoi confronti.

## TERMINARE IL SERVIZIO

È possibile terminare il contratto legale con I-REACT in qualsiasi momento disattivando il proprio account e interrompendo l'utilizzo dei Servizi. Sul sito web I-REACT puoi trovare le istruzioni su come disattivare il tuo account e sulla politica sulla privacy di I-REACT puoi trovare maggiori informazioni su cosa succede alle tue informazioni.

Possiamo sospendere o chiudere il tuo account o interrompere la fornitura di tutti o parte dei Servizi in qualsiasi momento per qualsiasi o nessuna ragione, incluso, ma non limitato a, se ragionevolmente crediamo: (i) hai violato questi Termini o l'I -REACT Services, (ii) si crea rischio o potenziale esposizione legale per noi; (iii) il tuo account deve essere rimosso a causa di condotta illecita, (iv) il tuo account deve essere rimosso a causa di una prolungata inattività; o (v) la nostra fornitura dei Servizi all'utente non è più commercialmente valida. Faremo ogni ragionevole sforzo per informarti attraverso l'indirizzo email associato al tuo account o la prossima volta che tenterai di accedere al tuo account, a seconda delle circostanze. In tutti questi casi, i Termini cesseranno, inclusa, senza limitazione, la licenza per utilizzare i Servizi. Se ritieni che il tuo account sia stato chiuso per errore, puoi presentare un ricorso seguendo i passaggi indicati nella nostra pagina web.

## DISCLAIMERS E LIMITAZIONI DI RESPONSABILITÀ

I servizi sono disponibili "COSÌ COME SONO"
L'accesso e l'utilizzo dei Servizi o di qualsiasi Contenuto sono a rischio dell'utente. Comprendi e accetti che i Servizi ti sono forniti "COSÌ COME SONO" e "COME DISPONIBILI". Le "entità I-REACT" si riferiscono a I-REACT, ai suoi genitori, affiliati, società collegate, funzionari, direttori, dipendenti, agenti, rappresentanti, partner e licenzianti. Senza limitare quanto sopra, nella misura massima consentita dalla legge applicabile, THE I-REACT ENTITIES NON RICONOSCE ALCUNA GARANZIA E CONDIZIONE, SIA ESPRESSA O IMPLICITA, DI COMMERCIABILITÀ, IDONEITÀ PER UN PARTICOLARE SCOPO O NON VIOLAZIONE. Le Entità I-REACT non rilasciano alcuna garanzia o dichiarazione e declinano ogni responsabilità per eventuali danni al proprio sistema informatico, perdita di dati o altri danni derivanti dall'accesso o dall'uso dei Servizi o di qualsiasi Contenuto. Nessun consiglio o informazione, sia orale che scritta, ottenuta dalle Entità I-REACT o attraverso i Servizi, creerà qualsiasi garanzia o rappresentazione non espressamente fatta nel presente documento.

## LIMITAZIONE DI RESPONSABILITÀ

Utilizzando i Servizi, l'utente accetta che I-REACT, i suoi genitori, affiliati, società collegate, funzionari, direttori, dipendenti, rappresentanti di agenti, partner e licenzianti, la responsabilità è limitata nella misura massima consentita nel proprio paese di residenza.

## GENERALE

Il Consorzio I-REACT può rivedere le presenti Condizioni di volta in volta. Le modifiche saranno retroattive e la versione più aggiornata dei Termini, che sarà sempre disponibile nel sito web del progetto, regolerà il nostro rapporto con te. A parte le modifiche che riguardano nuove funzioni o fatte per ragioni legali, ti notificheremo 30 giorni in anticipo (ancora da approvare come termine) di apportare modifiche effettive ai presenti Termini che influiscono sui diritti o gli obblighi di qualsiasi parte di questi Termini, per esempio tramite una notifica di servizio o un'email per l'e-mail associata al tuo account. Continuando ad accedere o utilizzare i Servizi dopo l'entrata in vigore di tali revisioni, l'utente accetta di essere vincolato dai Termini modificati.

Nel caso in cui qualsiasi disposizione di questi Termini sia ritenuta non valida o non applicabile, tale disposizione sarà limitata o eliminata nella misura minima necessaria e le restanti disposizioni di questi Termini rimarranno in vigore a tutti gli effetti. La mancata applicazione da parte di I-REACT di qualsiasi diritto o disposizione dei presenti Termini non sarà considerata una rinuncia a tale diritto o disposizione.

_Queste condizioni sono un accordo tra te e il consorzio I-REACT che rappresenta il progetto I-REACT, che ha ricevuto finanziamenti dal programma di ricerca e innovazione Horizon 2020 dell'Unione Europea nell'ambito della convenzione di sovvenzione n. 700256. Se hai domande su questi Termini, per favore Contattaci._
