# POLITICA SULLA RISERVATEZZA

_I-REACT sistema di risposta alle emergenze fornisce una gestione delle emergenze più efficace e più veloce._
_Informativa sulla privacy di I-REACT descrive come gestiamo le informazioni che ci fornite quando utilizzate i nostri Servizi. L'utente riconosce che attraverso l'utilizzo dei Servizi l'utente acconsente alla raccolta e all'utilizzo delle Informazioni personali definite, come in questa Informativa sulla privacy._

Questa Informativa sulla privacy descrive come e quando raccogliamo, usiamo e condividiamo le informazioni degli utenti attraverso il sito web I-REACT, l'app mobile, le API, il sistema di back-end e qualsiasi altro servizio coperto che si collega a questa politica (collettivamente, i "Servizi") e dai nostri partner e altre terze parti. Ad esempio, ci invii informazioni quando usi i nostri Servizi sul web o dall'app mobile di I-REACT. Quando si utilizza uno dei nostri servizi, l'utente acconsente alla raccolta, al trasferimento, alla memorizzazione, alla divulgazione e all'utilizzo delle proprie informazioni come descritto nella presente Informativa sulla privacy. Questo include tutte le informazioni che si sceglie di fornire che è ritenuto sensibile ai sensi della legge applicabile.

_** I servizi I-REACT Gli utenti target principali sono i cittadini e i primi soccorritori.**_ I cittadini e i primi soccorritori sono i principali utenti finali delle soluzioni e dei servizi I-REACT, essendo responsabili della gestione delle emergenze sul campo e di produrre un rapporto che contenga informazioni accurate e quantitative. Oltre a queste due categorie di utenti, ci sono altri potenziali utenti della soluzione I-REACT, tali esperti tecnici (ad esempio analisti, amministratori di sistema, ecc.) Che potrebbero dover eseguire un particolare tipo di analisi dei dati, per esaminare serie temporali di dati , ecc. I cittadini sono impegnati attraverso crowdsourcing, approcci di gamification e social media per essere attivi nel segnalare eventi e disastri naturali che possono verificarsi.

_** I-REACT sarà il cosiddetto "Controller" dei dati personali che ci fornisci.**_ Raccogliamo le informazioni fornite dagli utenti e siamo responsabili della politica di protezione dei dati e di qualsiasi cosa relativa al elaborazione di dati personali. I-REACT ha un responsabile della protezione dei dati (DPO) il cui ruolo è di consigliare le regole necessarie per garantire la conformità alle leggi sulla protezione dei dati.

Quando questa politica menziona "noi" o "noi", fa riferimento al responsabile del trattamento delle informazioni in base a questa politica: il consorzio I-REACT (maggiori informazioni su www.i-react.eu). Nonostante ciò, sei l'unico responsabile delle informazioni inviate tramite i Servizi, come descritto nei Termini di servizio di I-REACT.

Indipendentemente dal paese in cui vivi, ci autorizzi a trasferire, archiviare e utilizzare le tue informazioni in qualsiasi paese in cui operiamo. In alcuni di questi paesi, le leggi sulla privacy e sulla protezione dei dati e le norme relative al momento in cui le autorità governative possono accedere ai dati possono variare rispetto a quelle del paese in cui si vive.

Nel caso in cui ci sia una discrepanza tra la [versione in lingua inglese di questo documento](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=en) e qualsiasi copia tradotta dell'Informativa sulla privacy, la versione inglese prevarrà.

## RACCOLTA DI INFORMAZIONI

Durante l'utilizzo dei nostri Servizi (consultare Termini e condizioni), potremmo chiederti di fornirci alcune Informazioni personali che possono essere utilizzate per contattarti o identificarti. Tali informazioni includono il tuo nome, cognome, nome utente, e-mail, numero di telefono cellulare, insieme al paese, provincia e città in cui di solito vivi (collettivamente, Profilo utente).
Raccogliamo, elaboriamo e usiamo le tue informazioni di seguito per fornire e migliorare i nostri Servizi. Gli utenti dei cittadini possono utilizzare I-REACT Services attraverso l'app mobile.

**Informazioni base sull'account:** Se scegli di creare un account I-REACT, devi fornirci alcune informazioni personali, come nome, cognome, nome utente (nickname), password, email, numero di cellulare, insieme a la nazione, la regione e la città in cui vivi. Raccogliamo le suddette informazioni personali durante la procedura di registrazione allo scopo di creare un utente unico, necessario per il corretto funzionamento dei Servizi I-REACT. È consentito creare e gestire un solo account I-REACT per numero di telefono e utente.
Il trattamento dei dati personali può avvenire in qualsiasi paese dell'UE per il trasferimento, la conservazione e l'utilizzo da parte di I-REACT. Raccogliamo informazioni aggiuntive quando gli utenti registrati interagiscono con i nostri Servizi. È possibile fare riferimento alla sezione Registro per i dettagli.

**Informazioni di contatto:** Fornendoci il tuo numero di telefono e la tua email, accetti di ricevere messaggi di testo ed e-mail da noi. Possiamo utilizzare le informazioni di contatto per inviarti informazioni sui nostri Servizi, per aiutarti a prevenire spam, frodi o abusi.

**Ulteriori informazioni:** Puoi anche scegliere di fornirci ulteriori informazioni per migliorare e personalizzare la tua esperienza nei nostri Servizi. Se colleghi il tuo account sui nostri Servizi al tuo account su un altro servizio, l'altro servizio potrebbe inviarci le informazioni che autorizzi per l'uso nei Servizi. Queste informazioni possono consentire la pubblicazione incrociata o in altro modo aiutarci a migliorare i Servizi, e vengono eliminate dai nostri Servizi entro alcune settimane dalla disconnessione dai nostri Servizi dal tuo account sull'altro servizio.

**Informazioni sulla posizione:** L'applicazione mobile I-REACT consente agli utenti registrati di creare rapporti (consultare Informazioni per dettagli) sulle condizioni ambientali e sull'emergenza in corso. Solo quando un report viene inviato a I-REACT, la posizione del dispositivo viene inviata e memorizzata nel sistema I-REACT. La posizione viene determinata automaticamente dal tuo telefono utilizzando le informazioni dal GPS, le informazioni sulle reti wireless o le torri cellulari vicino al tuo dispositivo mobile o il tuo indirizzo IP. I-REACT non traccia le posizioni degli utenti e non consente ad altri utenti di ottenere la posizione di un report insieme a qualsiasi informazione inclusa nel profilo utente. Possiamo utilizzare e memorizzare le informazioni sulla tua posizione per fornire funzionalità dei nostri servizi.

**Dati di registro:** Come molti operatori di siti, registriamo le interazioni dei nostri utenti durante l'utilizzo dei nostri servizi. Pertanto, ogni azione relativa al recupero dei dati, la fornitura che il tuo fare attraverso i Servizi è registrato. Registriamo i seguenti dati: ID utente (generato internamente), timestamp e durata dell'azione, l'indirizzo IP e il tipo di client utilizzato, il carico utile dell'azione (dati richiesti o inviati).

## USO DELLE INFORMAZIONI

I-REACT è progettato principalmente per aiutarti a condividere le informazioni con altri utenti per consentire il monitoraggio e la gestione delle emergenze. La maggior parte delle informazioni che ci fornite attraverso I-REACT Services sono informazioni che ci state chiedendo di rendere pubbliche. I-REACT raccoglie anche altre informazioni pubbliche derivate dalla combinazione di altri set di dati; o dedotto utilizzando algoritmi per analizzare una varietà di dati, come i social media, i dati di localizzazione al fine di mappare i rischi e le previsioni precedenti e prevenirne di nuovi.

### REPORT

L'applicazione mobile I-REACT consente agli utenti registrati di creare report. I-REACT tratta diversi tipi di rapporti e informazioni al loro interno (ad esempio testo, foto, video) e ad essi associati (ad esempio informazioni geolocalizzate e visualizzazione basata su mappe), in base al tipo di utente che li invia:

**REPORT CITTADINI.** Un utente cittadino, già registrato e autenticato sulla piattaforma I-REACT, può creare un rapporto su un evento naturale che si sta verificando nella sua posizione attuale. Questo rapporto dovrebbe fornire informazioni qualitative per monitorare una condizione o descrivere la situazione reale, ad esempio la presenza di acqua / fiamme, danni alle strade e agli edifici che circondano la sua posizione. Una volta completato il rapporto, viene inviato alla piattaforma I-REACT. Le segnalazioni presentate dai cittadini potrebbero essere convalidate dalle autorità. I report rifiutati non verranno più forniti dai Servizi. Il report inviato sarà condiviso con altri utenti e potrebbe anche essere soggetto a un processo di peer review (downvote, upvote).

**REPORT DALLE AUTORITÀ.** Un primo utente risponditore è incaricato di produrre un rapporto che contenga informazioni accurate e quantitative. I loro rapporti sono convalidati per impostazione predefinita e sono di primaria importanza per le sale di controllo di emergenza in modo che possano essere intraprese azioni per gestire l'emergenza.

**ANALISI DEI DATI** Dopo un'emergenza, o durante una fase di pre-allarme, gli utenti ufficiali potrebbero aver bisogno di accedere a una particolare serie di dati storici (ad esempio i livelli dell'acqua registrati per un lago, ecc.), Unendoli ad altri fonti di informazione, per creare un modello predittivo. Potrebbe anche essere possibile analizzare le informazioni che sono state create dagli utenti tramite i loro rapporti, precedenti e in corso e di emergenza, e utilizzarle per creare altri livelli di informazione, ad es. aree di delimitazione delle inondazioni. Utilizzando i servizi di I-REACT e inviando una segnalazione, acconsenti alla raccolta e all'elaborazione di qualsiasi dato personale correlato.
Tutte le segnalazioni raccolte, fornite da voi e da tutti gli altri utenti, vengono elaborate e quindi utilizzate per condividere informazioni (previa elaborazione interna) con altri utenti al fine di avvisare e avvisare sui rischi imminenti (in base alle loro posizioni) e anche per attività di gestione (scambio di dati e informazioni tra PA (sala operativa) e First Responders nel campo).
I-REACT diffonde le informazioni dei report a tutti gli utenti di I-REACT, che possono includere società di ricerche di mercato che analizzano le informazioni per tendenze e approfondimenti.

### QUANDO SI USA I NOSTRI SERVIZI

Riceviamo informazioni quando invii i rapporti, visualizzi il contenuto o altrimenti interagisca con i nostri Servizi, anche se non hai creato un account ("Dati di registro"). Il team di sviluppatori di I-REACT può utilizzare questi dati esclusivamente per scoprire bug nel software e migliorare l'efficacia dei nostri servizi.

### CONDIVISIONE E DIVULGAZIONE DELLE INFORMAZIONI PERSONALI

Non divulghiamo le tue informazioni personali private se non nelle limitate circostanze descritte qui.

**Legge e danni:** Nonostante quanto diversamente stabilito nella presente Informativa sulla privacy, possiamo conservare o divulgare le tue informazioni se riteniamo che sia ragionevolmente necessario per ottemperare a una legge, a un regolamento, a un procedimento legale oa una richiesta governativa; per proteggere la sicurezza di qualsiasi persona; affrontare le frodi, la sicurezza o le questioni tecniche; o per proteggere i nostri diritti o proprietà dei nostri utenti. Tuttavia, nulla in questa Informativa sulla privacy è inteso a limitare le eventuali difese o obiezioni legali che potresti avere nei confronti di una terza parte, inclusa la richiesta di un governo, di divulgare le tue informazioni.

**Trasferimenti aziendali e affiliati:** Nel caso in cui siamo coinvolti in un fallimento, fusione, acquisizione, riorganizzazione o vendita di beni, le tue informazioni possono essere vendute o trasferite come parte di tale transazione. Questa Informativa sulla privacy si applicherà alle tue informazioni come trasferite alla nuova entità. Potremmo anche divulgare informazioni su di voi alle nostre società affiliate al fine di aiutare a fornire, comprendere e migliorare i nostri Servizi, questo può includere la consegna di annunci pubblicitari.

**Informazioni pubbliche:** caso le autorità pubbliche e le agenzie responsabili della sicurezza pubblica e delle procedure di gestione delle emergenze (ad esempio protezione civile, polizia, croce rossa) aderiscono ai servizi I-REACT, saranno autorizzati a vedere il numero di telefono del utente che genera un rapporto. Questo per consentire la verifica delle informazioni, analogamente a ciò che accade già quando si chiama il 112.

**Informazioni non personali, aggregate o a livello di dispositivo:** Potremmo condividere o divulgare informazioni anonime, aggregate o a livello di dispositivo, come il numero totale di volte in cui le persone hanno interagito con un rapporto o il numero di utenti che hanno fatto clic su un particolare collegamento / rapporto (anche se ne è stato fatto uno solo), gli eventi di emergenza che si verificano in una determinata località o rapporti aggregati o a livello di dispositivo per gli inserzionisti sugli utenti che hanno visto o fatto clic sui propri annunci. Queste informazioni non includono il tuo nome, password, indirizzo email, numero di telefono. Tuttavia, potremmo condividere informazioni non personali, aggregate o a livello di dispositivo tramite partnership con altre entità.

## COMUNICAZIONI

Potremmo utilizzare le vostre informazioni personali per contattarvi con newsletter, materiale promozionale o di marketing e altre informazioni correlate alle modifiche relative ai servizi.

## SECURITY

La sicurezza delle tue informazioni personali è importante per noi, quindi I-REACT utilizza gli standard e i protocolli più recenti per il trasferimento sicuro dei dati e l'archiviazione su Internet. Ma ricorda che nessun metodo di trasmissione su Internet, o metodo di archiviazione elettronica, è sicuro al 100%. Mentre ci sforziamo di utilizzare mezzi commercialmente accettabili per proteggere le tue informazioni personali, non possiamo garantire la sua assoluta sicurezza.

## ACCESSO, MODIFICA, ESPORTAZIONE, ELIMINAZIONE DELLE INFORMAZIONI PERSONALI

Per offrirti i servizi, operiamo a livello globale. I-REACT è conforme alla Direttiva UE 95/46 / CE e al Regolamento generale sulla protezione dei dati dell'UE - GDPR (i "Principi") riguardanti la raccolta, l'uso, la condivisione e la conservazione di informazioni personali dall'Unione Europea.
Se hai un reclamo relativo alla Privacy, ti preghiamo di contattarci all'indirizzo [(info@i-react.eu)] (mailto: info@i-react.eu). Se in qualsiasi momento ritieni che le informazioni che elaboriamo su di te siano errate, puoi richiedere di vedere queste informazioni e di correggerle o cancellarle. Se desideri presentare un reclamo su come abbiamo gestito i tuoi dati personali, puoi contattarci per risolvere la questione entro il tempo indicato nel GDPR.

## MODIFICHE ALLA PRESENTE POLITICA

La presente Informativa sulla privacy è efficace dal (16/09/2018) e rimarrà in vigore salvo per quanto riguarda eventuali modifiche delle sue disposizioni in futuro. Potremmo rivedere la presente Informativa sulla privacy di volta in volta. La versione più aggiornata della politica disciplinerà il nostro utilizzo delle informazioni dell'utente e sarà sempre disponibile sul sito Web di I-REACT [http://www.i-react.eu/](http://www.i-react.eu /). Se apportiamo una modifica a questa politica che, a nostra esclusiva discrezione, è materiale, ti invieremo una notifica via email all'indirizzo email associato al tuo account I-REACT. Continuando ad accedere o utilizzare i Servizi dopo l'entrata in vigore di tali modifiche, l'utente accetta di essere vincolato dall'Informativa sulla privacy rivista.

## CONTATTACI

_Se hai qualche idea o domanda sulla presente Informativa sulla privacy, ti preghiamo di farcelo sapere contattandoci attraverso il nostro sito web: [http://www.i-react.eu/contact/](http://www.i-react .eu / contatto /)_
